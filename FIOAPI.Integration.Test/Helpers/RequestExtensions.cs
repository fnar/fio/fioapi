﻿//#define SHOULD_BREAK_ON_ASSERT

using FIOAPI.Integration.Test;
using System.Net;

namespace FIOAPI.Integration.Test.Helpers
{
    public static class RequestExtensions
    {
        public static void Break()
        {
#if SHOULD_BREAK_ON_ASSERT
            if (Debugger.IsAttached)
            {
                Debugger.Break();
            }
#endif
        }

        private static void AssertBreak(bool Condition)
        {
            if (!Condition) Break();
        }

        private static void AssertBaseline(this Request request)
        {
            AssertBreak(request != null);
            Assert.That(request, Is.Not.Null);

            AssertBreak(request.StatusCode != null);
            Assert.That(request.StatusCode, Is.Not.Null, request.PayloadFilePath);

            if (request.StatusCode == HttpStatusCode.InternalServerError)
            {
                string PayloadFilePathStr = request.PayloadFilePath != null ? $" ({request.PayloadFilePath})" : "";
                Assert.Fail($"InternalServerError{PayloadFilePathStr}: {request.ResponsePayload}");
            }
        }

        private static void NotExpectingBadRequest(this Request request)
        {
            if (request.StatusCode == HttpStatusCode.BadRequest && !string.IsNullOrEmpty(request.ResponsePayload))
            {
                Break();
                Assert.Fail($"Did not expect BadRequest.\nBadRequest Payload: {request.ResponsePayload}");
            }
        }

        public static void AssertInternalServerError(this Request request)
        {
            AssertBreak(request != null);
            Assert.That(request, Is.Not.Null);

            AssertBreak(request.StatusCode != null);
            Assert.That(request.StatusCode, Is.Not.Null, request.PayloadFilePath);

            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.InternalServerError));
        }

        public static void AssertOK(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK), request.PayloadFilePath);

            if (request.ResponseHeaders != null)
            {
                Assert.That(!request.ResponseHeaders.Any(rh => rh.Key == ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutHeader), Is.True);
            }
        }

        public static void AssertBadRequest(this Request request)
        {
            request.AssertBaseline();
            AssertBreak(HttpStatusCode.BadRequest == request.StatusCode);
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest), request.PayloadFilePath);
        }

        public static void AssertNotAcceptable(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            AssertBreak(HttpStatusCode.NotAcceptable == request.StatusCode);
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotAcceptable), request.PayloadFilePath);
        }

        public static void AssertNotFound(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            AssertBreak(HttpStatusCode.NotFound == request.StatusCode);
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound), request.PayloadFilePath);
        }

        public static void AssertForbidden(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            AssertBreak(HttpStatusCode.Forbidden == request.StatusCode);
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden), request.PayloadFilePath);
        }

        public static void AssertUnauthorized(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            AssertBreak(HttpStatusCode.Unauthorized == request.StatusCode);
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized), request.PayloadFilePath);
        }

        public static void AssertNoContent(this Request request)
        {
            request.AssertBaseline();
            request.NotExpectingBadRequest();
            AssertBreak(HttpStatusCode.NoContent == request.StatusCode);
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NoContent), request.PayloadFilePath);
        }

        public static void AssertHydrationTimeout(this Request request)
        {
            request.NotExpectingBadRequest();
            AssertBreak(HttpStatusCode.OK == request.StatusCode);
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK), request.PayloadFilePath);
            AssertBreak(request.ResponseHeaders != null);
            Assert.That(request.ResponseHeaders, Is.Not.Null, request.PayloadFilePath);
            AssertBreak(request.ResponseHeaders.Any(h => h.Key == ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutHeader));
            Assert.That(request.ResponseHeaders.Any(h => h.Key == ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutHeader), Is.True, request.PayloadFilePath);
        }

        public static void AssertStatusCode(this Request request, int StatusCode)
        {
            request.AssertBaseline();
            if (StatusCode != 400)
            {
                request.NotExpectingBadRequest();
            }
            AssertBreak(StatusCode == (int)request.StatusCode!);
            Assert.That((int)request.StatusCode!, Is.EqualTo(StatusCode), request.PayloadFilePath);
        }

        public static async Task AssertEnforceAuth(this Request request)
        {
            AssertBreak(request.AuthDef == null);
            Assert.That(request.AuthDef, Is.Null, request.PayloadFilePath);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();
        }

        public static async Task AssertEnforceWrite(this Request request, bool RequireAdmin = false)
        {
            var userAccount = RequireAdmin ? TestUserAccount.MakeAdminAccount() : TestUserAccount.MakeAccount();
            request.AuthDef = userAccount.GetReadDef();
            await request.GetNoResultResponseAsync();
            AssertBreak(HttpStatusCode.Forbidden == request.StatusCode);
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden), request.PayloadFilePath);

            // Also test user-write
            if (RequireAdmin)
            {
                userAccount = TestUserAccount.MakeAccount();
                request.AuthDef = userAccount.GetWriteDef();
                await request.GetNoResultResponseAsync();
                AssertBreak(HttpStatusCode.Forbidden == request.StatusCode);
                Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden), request.PayloadFilePath);
            }
        }
    }
}
