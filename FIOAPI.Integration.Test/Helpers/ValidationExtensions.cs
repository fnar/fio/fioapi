﻿namespace FIOAPI.Integration.Test.Helpers
{
    public static class ValidationExtensions
    {
        public static void AssertPassesValidation<T>(this T? validationObj) where T : IValidation
        {
            Assert.That(validationObj, Is.Not.Null);

            List<string> Errors = new();
            if (!validationObj.Validate(ref Errors))
            {
#if DEBUG
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }
#endif
                Assert.Fail(string.Join(Environment.NewLine, Errors));
            }
        }
    }
}
