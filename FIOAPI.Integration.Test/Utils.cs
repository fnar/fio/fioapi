﻿namespace FIOAPI.Integration.Test
{
    public enum CharacterSet
    {
        UserNameCharacters,
        PasswordCharacters
    }

    public static class Utils
    {
        private const string UserNameCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-.";
        private const string PasswordCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890!@#$%^&*()_-=+/\\'\"<>,.`~";

        private static Random Rnd = new Random((int)DateTime.Now.Ticks);

        private static string GetCharacterSet(CharacterSet set)
        {
            switch (set)
            {
                case CharacterSet.UserNameCharacters:
                    return UserNameCharacters;
                case CharacterSet.PasswordCharacters:
                    return PasswordCharacters;
                default:
                    throw new InvalidOperationException();
            }
        }

        public static string GenerateRandomString(int Length, CharacterSet set)
        {
            string ValidCharacters = GetCharacterSet(set);

            var ResultChars = new char[Length];
            for (int i = 0; i < Length; ++i)
            {
                ResultChars[i] = ValidCharacters[Rnd.Next(ValidCharacters.Length)];
            }

            return new string(ResultChars);
        }

        public static string GetCSProjDirectory()
        {
            return Directory.GetParent(Environment.CurrentDirectory)!.Parent!.Parent!.FullName;
        }

        public static List<string> GetFiles(string path)
        {
            return new DirectoryInfo(path)
                .GetFiles()
                .OrderBy(di => di.LastWriteTime)
                .Select(di => di.FullName)
                .ToList();
        }

        public static List<T> DeserializeFromCSV<T>(string Contents) where T : FIOAPI.Utils.ICsvObject, new()
        {
            using (var reader = new StringReader(Contents))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {                
                return csv.GetRecords<T>().ToList();
            }
        }

        public static T DeserializeFromFile<T>(string FilePath) where T : new()
        {
            Assert.That(File.Exists(FilePath), Is.True);

            var FileContents = File.ReadAllText(FilePath);
            Assert.That(FileContents, Is.Not.Null);
            Assert.That(FileContents, Is.Not.Empty);

            T? Payload = default;
            try
            {
                Payload = JsonSerializer.Deserialize<T>(FileContents);

            }
            catch (Exception ex)
            {
                Assert.Fail($"Failed to deserialize object from file {FilePath}. Error: {ex}");
            }

            Assert.That(Payload, Is.Not.Null);
            return Payload;
        }

        public static async Task AssertPutDataCommon<T>(this HttpClient client, string Endpoint, string TestFilesPath, AuthDef writeDef, bool RequireAdmin = false) where T : IValidation, new()
        {
            var TestFiles = Utils.GetFiles(TestFilesPath);
            Assert.That(TestFiles, Is.Not.Empty);

            var request = new Request(client, HttpMethod.Put, Endpoint);
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite(RequireAdmin);
            request.AuthDef = writeDef;

            T FirstPayloadObj = DeserializeFromFile<T>(TestFiles.First());

            // Find a string property and find an equals-string property
            PropertyInfo? StringPropInfo = null;
            PropertyInfo? EqualsPropInfo = null;
            var PropInfos = FirstPayloadObj.GetType().GetProperties()
                .Where(pi => !pi.GetCustomAttributes().Any(attr => attr is JsonIgnoreAttribute))
                .ToList();
            StringPropInfo = PropInfos.Where(p => p.PropertyType == typeof(string) && p.Name != nameof(IValidation.HashCodeKey)).LastOrDefault();
            EqualsPropInfo = PropInfos.Where(p => p.PropertyType == typeof(string) && p.GetCustomAttributes(true).Any(a => a is EqualsAttribute)).LastOrDefault();

            // Test Hydration timeout>	FIOAPI.Integration.Test.dll!FIOAPI.Integration.Test.Utils.AssertPutDataCommon<FIOAPI.Payloads.Planet.PATH_PLANETS>(System.Net.Http.HttpClient client, string Endpoint, string TestFilesPath, FIOAPI.Integration.Test.AuthDef writeDef, bool RequireAdmin) Line 106	C#

            if (StringPropInfo != null)
            {
                StringPropInfo.SetValue(FirstPayloadObj, ControllerFilters.HydrationTimeoutFilter.HydrationTimeoutStr);
                request.SetPayload(FirstPayloadObj);
                request.SetPayloadFilePath(TestFiles.First());
                await request.GetNoResultResponseAsync();
                request.AssertHydrationTimeout();
            }

            // Test BadRequest
            FirstPayloadObj = DeserializeFromFile<T>(TestFiles.First());
            if (EqualsPropInfo != null)
            {
                EqualsPropInfo.SetValue(FirstPayloadObj, Guid.NewGuid().ToString());
                request.SetPayload(FirstPayloadObj);
                await request.GetNoResultResponseAsync();
                request.AssertBadRequest();
            }

            foreach (var TestFile in TestFiles)
            {
                Assert.That(File.Exists(TestFile), Is.True);
                var FileContents = File.ReadAllText(TestFile);
                Assert.That(FileContents, Is.Not.Null);
                Assert.That(FileContents, Is.Not.Empty);

                T? PayloadObj = null;
                try
                {
                    PayloadObj = JsonSerializer.Deserialize<T>(FileContents);
                }
                catch (Exception ex)
                {
                    Assert.Fail($"Failed to deserialize object on {TestFile}. Error: {ex}");
                }

                Assert.That(PayloadObj, Is.Not.Null);

                List<string> Errors = new();
                if (!PayloadObj.Validate(ref Errors))
                {
                    string ErrorMsg = string.Join(Environment.NewLine, Errors);
                    Assert.Fail($"Failed to validate payload for deserialized object for {TestFile}. Errors:\n {ErrorMsg}");
                }

                request.SetPayload(PayloadObj);
                request.SetPayloadFilePath(TestFile);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }
    }
}
