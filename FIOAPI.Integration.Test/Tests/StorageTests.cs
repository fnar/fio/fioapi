﻿using FIOAPI.Payloads.Storage;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class StorageTests : FIOAPIIntegrationTest
    {
        private static TestUserAccount Account = TestUserAccount.MakeAccount();

        private static string StorageTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Storage");

        [Test, Order(0)]
        public async Task PutPlanets()
        {
            PlanetTests pt = new();
            await pt.PutPlanets();
        }

        [Test, Order(0)]
        public async Task PutStations()
        {  
            GlobalTests gt = new();
            await gt.PutStations();
        }

        [Test, Order(0)]
        public async Task PutShips()
        {
            ShipTests st = new();
            await st.PutShips();
        }

        [Test, Order(1)]
        public async Task PutPlanetSites()
        {
            PlanetTests pt = new();
            await pt.PutSitesData();
        }

        [Test, Order(2)]
        public async Task PutStorages()
        {
            var TestDataPath = Path.Combine(StorageTestDataFolder, "MESG_STORAGE_STORAGES");
            await client.AssertPutDataCommon<STORAGE_STORAGES>("/storage", TestDataPath, Account.GetWriteDef());

            TestDataPath = Path.Combine(StorageTestDataFolder, "ROOT_STORAGE_CHANGE");
            await client.AssertPutDataCommon<ROOT_STORAGE_CHANGE>("/storage/change", TestDataPath, Account.GetWriteDef());
        }

        [Test, Order(3)]
        public async Task PutWarehouses()
        {
            var TestDataPath = Path.Combine(StorageTestDataFolder, "MESG_WAREHOUSE_STORAGES");
            await client.AssertPutDataCommon<WAREHOUSE_STORAGES>("/storage/warehouses", TestDataPath, Account.GetWriteDef());
        }

        [Test, Order(4)]
        public async Task GetStorages()
        {
            request = new Request(client, HttpMethod.Get, $"/storage?username={Account.UserName.ToUpper()}", Account.GetReadDef());
            var response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<DB.Model.Storage>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            Assert.That(response.UserNameToData.Keys.Count, Is.EqualTo(1));
            var firstStorage = response.UserNameToData.First().Value.Data[0];
            Assert.That(firstStorage.StorageItems, Is.Empty);
            Assert.That(firstStorage.LocationNaturalId, Is.Not.Null);

            request.EndPoint = "/storage?include_items=true";
            response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<DB.Model.Storage>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            Assert.That(response.UserNameToData.Keys.Count, Is.EqualTo(1));
            Assert.That(response.UserNameToData.Values.SelectMany(s => s.Data).SelectMany(s => s.StorageItems).Any(), Is.True);
            Assert.That(response.UserNameToData.Values.SelectMany(s => s.Data).Select(s => s.LocationNaturalId).Any(a => a != null), Is.True);
        }

        [Test, Order(4)]
        public async Task GetWarehouseInfo()
        {
            request = new Request(client, HttpMethod.Get, $"/storage/warehouseinfo?username={Account.UserName.ToUpper()}", Account.GetReadDef());
            var response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<DB.Model.UserWarehouse>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            Assert.That(response.UserNameToData.Keys.Count, Is.EqualTo(1));
            Assert.That(response.UserNameToData.First().Value.Data.Count, Is.GreaterThan(1));
        }
    }
}
