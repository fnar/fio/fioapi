﻿namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class ChatTests : FIOAPIIntegrationTest
    {
        private static string TestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Chat");

        [Test, Order(0)]
        public async Task PutMesgChannelData()
        {
            var Account = TestUserAccount.MakeAccount();

            var ChannelPayloadsFolder = Path.Combine(TestDataFolder, "MESG_CHANNEL_DATA");
            await client.AssertPutDataCommon<Payloads.Chat.MESG_CHANNEL_DATA>("/chat/channel", ChannelPayloadsFolder, Account.GetWriteDef());
        }

        [Test, Order(0)]
        public async Task PutRootChannelData()
        {
            var Account = TestUserAccount.MakeAccount();

            var ChannelPayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_DATA");
            await client.AssertPutDataCommon<Payloads.Chat.ROOT_CHANNEL_DATA>("chat/channel/root", ChannelPayloadsFolder, Account.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task PutMessageAddedSelf()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "MESG_CHANNEL_MESSAGE_ADDED");
            await client.AssertPutDataCommon<Payloads.Chat.MESG_CHANNEL_MESSAGE_ADDED>("/chat/messageaddedself", MessagePayloadsFolder, Account.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task PutMessageAdded()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_MESSAGE_ADDED");
            await client.AssertPutDataCommon<Payloads.Chat.ROOT_CHANNEL_MESSAGE_ADDED>("/chat/messageadded", MessagePayloadsFolder, Account.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task PutMessageList()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "MESG_CHANNEL_MESSAGE_LIST");
            await client.AssertPutDataCommon<Payloads.Chat.CHANNEL_MESSAGE_LIST>("/chat/messagelist", MessagePayloadsFolder, Account.GetWriteDef());
        }

        [Test, Order(2)]
        public async Task PutMessageDeleted()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_MESSAGE_DELETED");
            await client.AssertPutDataCommon<Payloads.Chat.CHANNEL_MESSAGE_DELETED>("/chat/messagedeleted", MessagePayloadsFolder, Account.GetWriteDef());
        }

        [Test, Order(3)]
        public async Task PutUserJoined()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_USER_JOINED");
            await client.AssertPutDataCommon<Payloads.Chat.CHANNEL_USER_JOINED>("/chat/userjoined", MessagePayloadsFolder, Account.GetWriteDef());
        }

        [Test, Order(3)]
        public async Task PutUserLeft()
        {
            var Account = TestUserAccount.MakeAccount();
            var MessagePayloadsFolder = Path.Combine(TestDataFolder, "ROOT_CHANNEL_USER_LEFT");
            await client.AssertPutDataCommon<Payloads.Chat.CHANNEL_USER_LEFT>("/chat/userleft", MessagePayloadsFolder, Account.GetWriteDef());
        }

        [Test, Order(4)]
        public async Task GetChannelList()
        {
            // Force a bad request
            request = new Request(client, HttpMethod.Get, "/chat/list?types=abcdefghjijklmnopqrstuvwxyz");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Force a non-existent type
            request = new Request(client, HttpMethod.Get, "/chat/list?types=OMGWTF");
            var chatChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.That(chatChannels, Is.Not.Null);
            Assert.That(chatChannels, Is.Empty);

            // Grab only public
            request = new Request(client, HttpMethod.Get, "/chat/list?types=pUbLiC");
            chatChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.That(chatChannels, Is.Not.Null);
            Assert.That(chatChannels.Count, Is.EqualTo(3)); // 3: Global, Help, UFO

            // Grab public and group
            request = new Request(client, HttpMethod.Get, "/chat/list?types=PUBLIC&types=GROUP");
            chatChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.That(chatChannels, Is.Not.Null);
            Assert.That(chatChannels.Count > 3, Is.True);

            // Test no params
            request = new Request(client, HttpMethod.Get, "/chat/list");
            chatChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.That(chatChannels, Is.Not.Null);
            Assert.That(chatChannels, Is.Not.Empty);
        }

        [Test, Order(4)]
        public async Task GetMessages()
        {
            request = new Request(client, HttpMethod.Get, "/chat/list?types=PUBLIC");
            var publicChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.That(publicChannels, Is.Not.Null);
            Assert.That(publicChannels, Is.Not.Empty);

            Assert.That(publicChannels.Count == 3, Is.True);
            var global = publicChannels.First(pc => pc.DisplayName != null && pc.DisplayName.Contains("Global"));
            var help = publicChannels.First(pc => pc.DisplayName != null && pc.DisplayName.Contains("Help"));
            var ufo = publicChannels.First(pc => pc.DisplayName != null && pc.DisplayName.Contains("Operations"));

            // Bad request testing
            request = new Request(client, HttpMethod.Get, "/chat/messages?top=1001");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            request = new Request(client, HttpMethod.Get, "/chat/messages?skip=-1");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            request = new Request(client, HttpMethod.Get, $"/chat/messages?channel_names={global.DisplayName}&channel_names={help.DisplayName}&channel_names={ufo.DisplayName}");
            var messages = await request.GetResponseAsync<List<DB.Model.ChatMessage>>();
            request.AssertOK();
            Assert.That(messages, Is.Not.Null);
            Assert.That(messages, Is.Not.Empty);
            Assert.That(messages.Count, Is.LessThanOrEqualTo(1000));
        }

        [Test, Order(4)]
        public async Task GetUserMessages()
        {
            request = new Request(client, HttpMethod.Get, "/chat/list?types=PUBLIC");
            var publicChannels = await request.GetResponseAsync<List<DB.Model.ChatChannel>>();
            request.AssertOK();
            Assert.That(publicChannels, Is.Not.Null);
            Assert.That(publicChannels, Is.Not.Empty);

            Assert.That(publicChannels.Count == 3, Is.True);
            var help = publicChannels.First(pc => pc.DisplayName != null && pc.DisplayName.Contains("Help"));

            // Bad request testing
            request = new Request(client, HttpMethod.Get, "/chat/user/Saganaki/1234");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            request = new Request(client, HttpMethod.Get, "/chat/user/S/00000000000000000000000000000001");
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            request = new Request(client, HttpMethod.Get, $"/chat/user/Saganaki/{help.ChatChannelId}");
            var messages = await request.GetResponseAsync<List<DB.Model.ChatMessage>>();
            request.AssertOK();
            Assert.That(messages, Is.Not.Null);
            Assert.That(messages, Is.Not.Empty);
            Assert.That(messages.Count, Is.LessThanOrEqualTo(1000));
        }
    }
}
