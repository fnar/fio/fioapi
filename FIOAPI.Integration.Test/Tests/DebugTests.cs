﻿namespace FIOAPI.Integration.Test.Tests
{
    public class DebugTests : FIOAPIIntegrationTest
    {
        [Test]
        public async Task TestException()
        {
            request = new Request(client, HttpMethod.Get, "/debug/exception");
            await request.GetNoResultResponseAsync();
            request.AssertInternalServerError();
        }
    }
}
