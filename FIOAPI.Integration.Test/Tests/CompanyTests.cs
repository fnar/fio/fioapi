﻿using FIOAPI.Payloads;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class CompanyTests : FIOAPIIntegrationTest
    {
        private static TestUserAccount Account = TestUserAccount.MakeAccount();

        private static string TestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Company");

        [Test, Order(0)]
        public async Task PutCompanies()
        {
            var TestDataPath = Path.Combine(TestDataFolder, "PATH_companies-_");
            await client.AssertPutDataCommon<Payloads.Company.PATH_COMPANY>("/company", TestDataPath, Account.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task PutCompanyOffices()
        {
            var TestDataPath = Path.Combine(TestDataFolder, "PATH_companies-_-offices");
            await client.AssertPutDataCommon<Payloads.Company.PATH_COMPANIES_OFFICES>("/company/offices", TestDataPath, Account.GetWriteDef());
        }

        [Test, Order(2)]
        public async Task PutSelfData()
        {
            var TestDataPath = Path.Combine(TestDataFolder, "MESG_COMPANY_DATA");
            await client.AssertPutDataCommon<Payloads.Company.MESG_COMPANY_DATA>("/company/selfmsg", TestDataPath, Account.GetWriteDef());
        }

        [Test, Order(2)]
        public async Task PutPathSelfData()
        {
            var TestDataPath = Path.Combine(TestDataFolder, "ROOT_COMPANY_DATA");
            await client.AssertPutDataCommon<Payloads.Company.ROOT_COMPANY_DATA>("/company/selfroot", TestDataPath, Account.GetWriteDef());
        }

        [Test, Order(3)]
        public async Task PutBalanceData()
        {
            var TestDataPath = Path.Combine(TestDataFolder, "MESG_ACCOUNTING_CASH_BALANCES");
            await client.AssertPutDataCommon<Payloads.Company.MESG_ACCOUNTING_CASH_BALANCES>("/company/accounting", TestDataPath, Account.GetWriteDef());
        }

        [Test, Order(4)]
        public async Task GetCompany()
        {
            var companyCode = "ZZZZ";
            request = new Request(client, HttpMethod.Get, $"/company/lookup?company={companyCode}");
            var result = await request.GetResponseAsync<List<Company?>>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Count.EqualTo(1));
            Assert.That(result.First(), Is.Null);

            companyCode = "PC";
            request.EndPoint = $"/company/lookup?company={companyCode}";
            result = await request.GetResponseAsync<List<Company?>>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Count.EqualTo(1));
            Assert.That(result.First(), Is.Not.Null);
            Assert.That(result.First().Planets, Is.Empty);
            Assert.That(result.First().Offices, Is.Empty);

            request.EndPoint = $"/company/lookup?company={companyCode}&include_planets=true&include_offices=true";
            result = await request.GetResponseAsync<List<Company?>>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Count.EqualTo(1));
            Assert.That(result.First(), Is.Not.Null);
            Assert.That(result.First().Planets, Is.Not.Empty);
            Assert.That(result.First().Offices, Is.Not.Empty);

            var companyId = result.First().CompanyId;

            // Give it a bad company id
            request.EndPoint = $"/company/lookup?company={companyId[0..31]}&include_planets=true&include_offices=true";
            result = await request.GetResponseAsync<List<Company?>>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Count.EqualTo(1));
            Assert.That(result!.First(), Is.Null);

            // Fix the company company and ensure it works properly
            request.EndPoint = $"/company/lookup?company={companyId}&include_planets=true&include_offices=true";
            result = await request.GetResponseAsync<List<Company?>>();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Count.EqualTo(1));
            Assert.That(result.First(), Is.Not.Null);
            Assert.That(result.First().Planets, Is.Not.Empty);
            Assert.That(result.First().AICLiquid, Is.Null);

            request.EndPoint = $"/company/lookup?company=SAGA&include_planets=true&include_offices=true";

            // Without auth
            result = await request.GetResponseAsync<List<Company?>>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Has.Count.EqualTo(1));
            Assert.That(result.First(), Is.Not.Null);
            Assert.That(result.First().Planets, Is.Not.Empty);
            Assert.That(result.First().AICLiquid, Is.Null);

            // With auth
            request = new Request(client, HttpMethod.Get, $"/company?username={Account.UserName}&include_planets=true&include_offices=true", Account.GetReadDef());
            var retreivalResult = await request.GetResponseAsync<RetrievalResponse<Company>>();
            request.AssertOK();
            Assert.That(retreivalResult, Is.Not.Null);
            Assert.That(retreivalResult.UserNameToData, Is.Not.Empty);
            Assert.That(retreivalResult.UserNameToData.Values.First(), Is.Not.Null);
            var companyData = retreivalResult.UserNameToData.Values.First().Data;
            Assert.That(companyData.AICLiquid, Is.Not.Null);
            Assert.That(companyData.AICLiquid > 0.0, Is.True);
            Assert.That(companyData.RepresentationCostNextLevel, Is.Not.Null);
            Assert.That(companyData.RepresentationCostNextLevel > 0.0, Is.True);
            Assert.That(companyData.RatingContractCount > 0, Is.True);
        }

        [Test, Order(4)]
        public async Task GetBalances()
        {
            var apikey = await Account.GetAPIKey();
            request = new Request(client, HttpMethod.Get, $"/csv/balances?apikey={apikey}&username={Account.UserName}&include_header=true");
            // Test csv retrieval twice to confirm both API key paths work properly
            var res = await request.GetResponseAsync<string>();
            request.AssertOK();
            res = await request.GetResponseAsync<string>();
            request.AssertOK();
        }
    }
}
