﻿using FIOAPI.Payloads.Group;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class GroupTests : FIOAPIIntegrationTest
    {
        [Test]
        public async Task Create()
        {
            var accounts = TestUserAccount.MakeAccounts(5);

            // BadRequest
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = -1,
                GroupName = "AB",
            };
            Assert.False(createPayload.PassesValidation());
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Group name too large
            createPayload.RequestedId = 1234;
            createPayload.GroupName = "1234567890ABCDEFG";
            Assert.That(createPayload.GroupName.Length > 16, Is.True);
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Success
            createPayload.RequestedId = 1234;
            createPayload.GroupName = "API Test";
            createPayload.AssertPassesValidation();
            request.SetPayload(createPayload);
            var createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createRes, Is.Not.Null);
            Assert.That(createRes.GroupId, Is.EqualTo(1234));

            // Same Id specified
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotAcceptable));

            // Success with generated id
            createPayload.RequestedId = 0;
            Assert.That(createPayload.PassesValidation(), Is.True);
            request.SetPayload(createPayload);
            createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createRes, Is.Not.Null);
            Assert.That(createRes.GroupId, Is.InRange(1, DB.Model.Group.LargestGroupId));

            // Success with user invites.  Intentional duplicates (should be removed)
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[0].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[0].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[1].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[1].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[2].DisplayUserName,
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[3].DisplayUserName,
                Admin = true
            });
            createPayload.Invites.Add(new Payloads.Group.UserInvite
            {
                UserName = accounts[4].DisplayUserName,
                Admin = true
            });
            Assert.That(createPayload.PassesValidation(), Is.True);
            request.SetPayload(createPayload);
            createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createRes, Is.Not.Null);
            Assert.That(createRes.GroupId, Is.InRange(1, DB.Model.Group.LargestGroupId));

            var groupId = createRes.GroupId;

            // At this point, we've created 3 groups.  Create GroupController.MaxGroupCount - 3 more groups
            for (int i = 0; i < Controllers.GroupController.MaxGroupCount - 3; ++i)
            {
                createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
                Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
                Assert.That(createRes, Is.Not.Null);
                Assert.That(createRes.GroupId, Is.InRange(1, DB.Model.Group.LargestGroupId));
            }

            // Attempt to create one more--this should faild
            createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotAcceptable));

            request = new Request(client, HttpMethod.Get, $"/group/{groupId}", accounts[0].GetReadDef());
            var groupRes = await request.GetResponseAsync<DB.Model.Group>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(groupRes, Is.Not.Null);
            Assert.That(groupRes.PendingInvites.Any(), Is.True);
        }

        [Test]
        public async Task Delete()
        {
            var account = TestUserAccount.MakeAccount();
            var ReadDef = account.GetReadDef();
            var WriteDef = account.GetWriteDef();

            // Create a group
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Delete Test"
            };
            Assert.That(createPayload.PassesValidation(), Is.True);
            request = new Request(client, HttpMethod.Post, "/group/create", WriteDef);
            request.SetPayload(createPayload);
            var createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createRes, Is.Not.Null);
            Assert.That(createRes.GroupId, Is.InRange(1, DB.Model.Group.LargestGroupId));

            // Try to use a read def
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createRes.GroupId}", ReadDef);
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden));

            // Try to specify a bad id
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{DB.Model.Group.LargestGroupId + 2}", WriteDef);
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Try to specify an id that we don't own
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createRes.GroupId + 1}", WriteDef);
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // Actually delete the correct group
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createRes.GroupId}", WriteDef);
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Try to delete again and fail
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
        }

        [Test]
        public async Task List()
        {
            var account = TestUserAccount.MakeAccount();
            var ReadDef = account.GetReadDef();
            var WriteDef = account.GetWriteDef();

            request = new Request(client, HttpMethod.Get, "/group/list", ReadDef);
            var listResult = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.That(listResult, Is.Not.Null);
            Assert.That(listResult, Is.Empty);

            // Create GroupController.MaxGroupCount groups
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "List Test"
            };
            request = new Request(client, HttpMethod.Post, "/group/create", WriteDef);
            request.SetPayload(createPayload);
            for (int i = 0; i < Controllers.GroupController.MaxGroupCount; ++i)
            {
                await request.GetNoResultResponseAsync();
                Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            }

            // Make sure we have MaxGroupCount entries
            request = new Request(client, HttpMethod.Get, "/group/list", ReadDef);
            listResult = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.That(listResult, Is.Not.Null);
            Assert.That(listResult, Is.Not.Empty);
            Assert.That(listResult.Count, Is.EqualTo(Controllers.GroupController.MaxGroupCount));
        }

        [Test]
        public async Task ListOwner()
        {
            var account = TestUserAccount.MakeAccount();
            var ReadDef = account.GetReadDef();
            var WriteDef = account.GetWriteDef();

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "List Test"
            };
            request = new Request(client, HttpMethod.Post, "/group/create", WriteDef);
            request.SetPayload(createPayload);
            var createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createRes, Is.Not.Null);
            var groupId1 = createRes.GroupId;

            createRes = await request.GetResponseAsync<Payloads.Group.CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createRes, Is.Not.Null);
            var groupId2 = createRes.GroupId;

            request = new Request(client, HttpMethod.Get, "/group/list/owner", ReadDef);
            var listOwner = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(listOwner, Is.Not.Null);
            Assert.That(listOwner.Count, Is.EqualTo(2));
            Assert.That(listOwner.Any(lo => lo.GroupId == groupId1), Is.True);
            Assert.That(listOwner.Any(lo => lo.GroupId == groupId2), Is.True);
            Assert.That(listOwner.Count(lo => lo.GroupName == createPayload.GroupName), Is.EqualTo(2));
        }

        [Test]
        public async Task ListAdmin()
        {
            var accounts = TestUserAccount.MakeAccounts(3);

            int Group1Id = 0;
            int Group2Id = 0;

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "List Admin",
                Invites = new List<UserInvite>()
                {
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                        Admin = true
                    }
                }
            };

            // Create first group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);
            Group1Id = createResponse.GroupId;

            // Create second group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[1].GetWriteDef());
            request.SetPayload(createPayload);
            createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);
            Group2Id = createResponse.GroupId;

            // Accept first admin invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group1Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Accept second admin invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group2Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Make sure list admin returns 2 results
            request = new Request(client, HttpMethod.Get, "/group/list/admin", accounts[2].GetReadDef());
            var adminList = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(adminList, Is.Not.Null);
            Assert.That(adminList.Count, Is.EqualTo(2));
        }

        [Test]
        public async Task ListMember()
        {
            var accounts = TestUserAccount.MakeAccounts(3);

            int Group1Id = 0;
            int Group2Id = 0;

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "List Admin",
                Invites = new List<UserInvite>()
                {
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                    }
                }
            };

            // Create first group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);
            Group1Id = createResponse.GroupId;

            // Create second group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[1].GetWriteDef());
            request.SetPayload(createPayload);
            createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);
            Group2Id = createResponse.GroupId;

            // Accept first invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group1Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Accept second invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group2Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Make sure member list returns 2 results
            request = new Request(client, HttpMethod.Get, "/group/list/member", accounts[2].GetReadDef());
            var adminList = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(adminList, Is.Not.Null);
            Assert.That(adminList.Count, Is.EqualTo(2));
        }

        [Test]
        public async Task InviteAcceptRejectList()
        {
            var accounts = TestUserAccount.MakeAccounts(3);

            int Group1Id;
            int Group2Id;

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Acc/Rej/List",
                Invites = new List<UserInvite>()
                {
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                        Admin = true
                    }
                }
            };

            // Create first group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);
            Group1Id = createResponse.GroupId;

            // Create second group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[1].GetWriteDef());
            request.SetPayload(createPayload);
            createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);
            Group2Id = createResponse.GroupId;

            // Verify we have 2 invites
            request = new Request(client, HttpMethod.Get, "/group/list/invite", accounts[2].GetReadDef());
            var inviteList = await request.GetResponseAsync<List<InviteResponse>>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(inviteList, Is.Not.Null);
            Assert.That(inviteList.Count, Is.EqualTo(2));

            // Accept the first invite
            request = new Request(client, HttpMethod.Put, $"group/invite/accept/{Group1Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Verify we only have 1 invite
            request = new Request(client, HttpMethod.Get, "/group/list/invite", accounts[2].GetReadDef());
            inviteList = await request.GetResponseAsync<List<InviteResponse>>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(inviteList, Is.Not.Null);
            Assert.That(inviteList, Has.Count.EqualTo(1));
            Assert.That(inviteList[0].GroupId, Is.EqualTo(Group2Id));

            // Reject the remaining invite
            request = new Request(client, HttpMethod.Put, $"group/invite/reject/{Group2Id}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            request = new Request(client, HttpMethod.Get, "/group/list/invite", accounts[2].GetReadDef());
            inviteList = await request.GetResponseAsync<List<InviteResponse>>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(inviteList, Is.Not.Null);
            Assert.That(inviteList, Is.Empty);
        }

        [Test]
        public async Task Invite()
        {
            var accounts = TestUserAccount.MakeAccounts(3);

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Invite",
            };

            // Create the group
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            request.AssertOK();
            Assert.That(createResponse, Is.Not.Null);

            // Invite self
            var invitePayload = new Invite()
            {
                GroupId = createResponse.GroupId,
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[0].UserName
                    }
                }
            };

            // Test wrong groupid
            var badInvitePayload = new Invite()
            {
                GroupId = createResponse.GroupId + 1,
                Invites = invitePayload.Invites
            };
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[0].GetWriteDef());
            request.SetPayload(badInvitePayload);
            await request.GetNoResultResponseAsync();
            request.AssertNotFound();

            // Test read-only def
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[0].GetReadDef());
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Ensure we get BadRequest when inviting self
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[0].GetWriteDef());
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Add first user
            invitePayload.Invites.Add(new UserInvite { UserName = accounts[1].UserName, Admin = true });
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Try again, we should get BadRequest
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Have first user check their invites and ensure it has one
            request = new Request(client, HttpMethod.Get, "/group/list/invite", accounts[1].GetReadDef());
            var inviteList = await request.GetResponseAsync<List<InviteResponse>>();
            request.AssertOK();
            Assert.That(inviteList, Is.Not.Null);
            Assert.That(inviteList.Count, Is.EqualTo(1));
            inviteList[0].AssertPassesValidation();

            // Have first user accept
            request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Should still fail, despite an accepted invite
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[0].GetWriteDef());
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Try to invite last user as admin (from an admin). This isn't allowed (BadRequest)
            request = new Request(client, HttpMethod.Post, "/group/invite", accounts[1].GetWriteDef());
            invitePayload.Invites.Add(new UserInvite { UserName = accounts[2].UserName, Admin = true });
            request.SetPayload(invitePayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();
        }

        [Test]
        public async Task Kick()
        {
            var accounts = TestUserAccount.MakeAccounts(6);

            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Kick",
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[1].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[3].UserName
                    },
                    new UserInvite
                    {
                        UserName = accounts[4].UserName
                    }
                }
            };

            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);

            // Have each user accept the invite
            for (int UserIdx = 1; UserIdx < 5; ++UserIdx)
            {
                request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[UserIdx].GetWriteDef());
                await request.GetNoResultResponseAsync();
                Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            }

            // Test input validation - too large an int
            request = new Request(client, HttpMethod.Put, $"/group/kick/{DB.Model.Group.LargestGroupId + 1}/Foo", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Test input validation - too small a username
            request = new Request(client, HttpMethod.Put, $"/group/kick/1/AB", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // User not found
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/ABC", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // Non-members return NotFound
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[3].UserName}", accounts[5].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // Members can't kick
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[3].UserName}", accounts[4].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // Can't kick the GroupOwner
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[0].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Can't kick another Admin
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[2].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Can't kick yourself
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[1].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Can kick member
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[4].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Owner can kick admin
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[1].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Kicked admin has no power now
            request = new Request(client, HttpMethod.Put, $"/group/kick/{createResponse.GroupId}/{accounts[3].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));
        }

        [Test]
        public async Task Leave()
        {
            var accounts = TestUserAccount.MakeAccounts(3);
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Leave",
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[1].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                    },
                }
            };

            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);

            // Each user should accept
            for (int UserIdx = 1; UserIdx < 3; ++UserIdx)
            {
                request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[UserIdx].GetWriteDef());
                await request.GetNoResultResponseAsync();
                Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            }

            // Specify an invite GroupId
            request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{DB.Model.Group.LargestGroupId + 1}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Try to leave w/o auth
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}");
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));

            // Try to leave with a read only key
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}", accounts[2].GetReadDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden));

            // Have the owner try to leave
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Have user leave
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}", accounts[2].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Have the user try to leave again
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // Have the admin leave
            request = new Request(client, HttpMethod.Put, $"/group/leave/{createResponse.GroupId}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Ensure there's zero admins and zero users
            request = new Request(client, HttpMethod.Get, $"/group/{createResponse.GroupId}", accounts[0].GetReadDef());
            var group = await request.GetResponseAsync<DB.Model.Group>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(group, Is.Not.Null);
            Assert.That(group.Admins, Is.Empty);
            Assert.That(group.Users, Is.Empty);
        }

        [Test]
        public async Task Promote()
        {
            var accounts = TestUserAccount.MakeAccounts(3);
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Leave",
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[1].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                    },
                }
            };
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);

            // Have each user accept
            for (int UserIdx = 1; UserIdx < 3; ++UserIdx)
            {
                request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[UserIdx].GetWriteDef());
                await request.GetNoResultResponseAsync();
                Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            }

            // Try to promote unauth'd
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[2].UserName}");
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));

            // Try to use read auth
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[2].UserName}", accounts[0].GetReadDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden));

            // Try to specify a GroupId we're not owner of
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId + 1}/{accounts[2].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // Specify a bad GroupId
            request = new Request(client, HttpMethod.Put, $"/group/promote/{DB.Model.Group.LargestGroupId + 1}/{accounts[2].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Make sure admin can't promote another user
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[2].UserName}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // Try to promote an existing admin
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[1].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Try to promote an non-existent user
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/FooBar", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // Successfully promote
            request = new Request(client, HttpMethod.Put, $"/group/promote/{createResponse.GroupId}/{accounts[2].UserName}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Try again to make sure it returns BadRequest
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Make sure the group has 2 admins
            request = new Request(client, HttpMethod.Get, $"/group/{createResponse.GroupId}", accounts[0].GetReadDef());
            var group = await request.GetResponseAsync<DB.Model.Group>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(group, Is.Not.Null);
            Assert.That(group.Admins.Count, Is.EqualTo(2));
            Assert.That(group.Users, Is.Empty);
        }

        [Test]
        public async Task DeleteGroup()
        {
            var accounts = TestUserAccount.MakeAccounts(4);
            var createPayload = new Payloads.Group.Create()
            {
                RequestedId = 0,
                GroupName = "Leave",
                Invites = new List<UserInvite>
                {
                    new UserInvite
                    {
                        UserName = accounts[1].UserName,
                        Admin = true
                    },
                    new UserInvite
                    {
                        UserName = accounts[2].UserName,
                    },
                    new UserInvite
                    {
                        UserName = accounts[3].UserName,
                    }
                }
            };
            request = new Request(client, HttpMethod.Post, "/group/create", accounts[0].GetWriteDef());
            request.SetPayload(createPayload);
            var createResponse = await request.GetResponseAsync<CreateResponse>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(createResponse, Is.Not.Null);

            // Have UserIdx 1 & 2 accept
            for (int UserIdx = 1; UserIdx < 3; ++UserIdx)
            {
                request = new Request(client, HttpMethod.Put, $"/group/invite/accept/{createResponse.GroupId}", accounts[UserIdx].GetWriteDef());
                await request.GetNoResultResponseAsync();
                Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            }

            // Try to delete unauthorized
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createResponse.GroupId}");
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));

            // Try to delete w/ a read def
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createResponse.GroupId}", accounts[0].GetReadDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden));

            // Give a bad id
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{DB.Model.Group.LargestGroupId + 1}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.BadRequest));

            // Have an admin try to delete the group
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createResponse.GroupId}", accounts[1].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // Have the owner actually delete the group
            request = new Request(client, HttpMethod.Delete, $"/group/delete/{createResponse.GroupId}", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));

            // Try to delete it again
            await request.GetNoResultResponseAsync();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.NotFound));

            // We shouldn't be able to retrieve the group
            request = new Request(client, HttpMethod.Get, $"/group/{createResponse.GroupId}", accounts[0].GetWriteDef());
            var group = await request.GetResponseAsync<DB.Model.Group>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.Forbidden));

            // User 1 shouldn't be an admin for any group
            request = new Request(client, HttpMethod.Get, "/group/list/admin", accounts[1].GetReadDef());
            var adminList = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(adminList, Is.Not.Null);
            Assert.That(adminList, Is.Empty);

            // User 2 shouldn't be a user for any group
            request = new Request(client, HttpMethod.Get, "/group/list/member", accounts[2].GetReadDef());
            var memberList = await request.GetResponseAsync<List<DB.Model.Group>>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(memberList, Is.Not.Null);
            Assert.That(memberList, Is.Empty);

            // User 3 shouldn't have any invites
            request = new Request(client, HttpMethod.Get, "/group/list/invite", accounts[2].GetReadDef());
            var inviteList = await request.GetResponseAsync<List<InviteResponse>>();
            Assert.That(request.StatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.That(inviteList, Is.Not.Null);
            Assert.That(inviteList, Is.Empty);
        }
    }
}
