﻿using FIOAPI.Payloads.Permission;
using FIOAPI.Payloads.Ship;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class ShipTests : FIOAPIIntegrationTest
    {
        private static List<TestUserAccount> Accounts = TestUserAccount.MakeAccounts(3);

        private static string ShipTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Ship");

        [Test, Order(0)]
        public async Task PutShips()
        {
            var TestDataFolder = Path.Combine(ShipTestDataFolder, "MESG_SHIP_SHIPS");
            await client.AssertPutDataCommon<MESG_SHIP_SHIPS>("/ships", TestDataFolder, Accounts[0].GetWriteDef());
        }

        [Test, Order(1)]
        public async Task PutFlights()
        {
            var TestDataFolder = Path.Combine(ShipTestDataFolder, "MESG_SHIP_FLIGHT_FLIGHTS");
            await client.AssertPutDataCommon<MESG_SHIP_FLIGHT_FLIGHTS>("/ships/flights", TestDataFolder, Accounts[0].GetWriteDef());
        }

        [Test, Order(2)]
        public async Task GetShips()
        {
            request = new Request(client, HttpMethod.Get, $"/ships?username={Accounts[0].UserName.ToUpper()}", Accounts[0].GetReadDef());
            var response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<Ship>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            var firstShip = response.UserNameToData.First().Value.Data[0];
            Assert.That(firstShip.RepairMaterials, Is.Empty);

            // Not allowed
            request.AuthDef = Accounts[1].GetReadDef();
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Grant...
            request = new Request(client, HttpMethod.Put, "/permission/grant", Accounts[0].GetWriteDef());
            request.SetPayload(new Grant()
            {
                UserName = Accounts[1].UserName
            });
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Updating the permissions is asynchronous--could take time
            await Task.Delay(100);

            // Works
            request = new Request(client, HttpMethod.Get, $"/ships?username={Accounts[0].UserName.ToUpper()}", Accounts[1].GetReadDef());
            response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<Ship>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            firstShip = response.UserNameToData.First().Value.Data[0];
            Assert.That(firstShip.RepairMaterials, Is.Empty);

            // Grant _just_ information
            request = new Request(client, HttpMethod.Put, "/permission/grant", Accounts[0].GetWriteDef());
            request.SetPayload(new Grant()
            {
                UserName = Accounts[1].UserName,
                Permissions = new Permissions()
                {
                    ShipPermissions = new ShipPermissions()
                    {
                        Information = true,
                        Repair = false,
                        Flight = false,
                        Inventory = false,
                        FuelInventory = false
                    }
                }
            });
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Updating the permissions is asynchronous--could take time
            await Task.Delay(100);

            // Try again, but make sure fields are nulled/cleared
            request = new Request(client, HttpMethod.Get, $"/ships?username={Accounts[0].UserName.ToUpper()}&include_repair_materials=true", Accounts[1].GetReadDef());
            response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<Ship>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            firstShip = response.UserNameToData.First().Value.Data[0];
            Assert.That(firstShip.RepairMaterials, Is.Empty);
            Assert.That(firstShip.FlightId, Is.Null);
            Assert.That(firstShip.CurrentLocationId, Is.Null);
            Assert.That(firstShip.CurrentLocationName, Is.Null);
            Assert.That(firstShip.CurrentLocationNaturalId, Is.Null);
            Assert.That(firstShip.StoreId, Is.EqualTo(Constants.InvalidAPEXID));
            Assert.That(firstShip.STLFuelStoreId, Is.EqualTo(Constants.InvalidAPEXID));
            Assert.That(firstShip.FTLFuelStoreId, Is.EqualTo(Constants.InvalidAPEXID));
        }

        [Test, Order(2)]
        public async Task GetFlights()
        {
            var Account0UserName = Accounts[0].UserName;
            var Account1UserName = Accounts[1].UserName;
            var Account2UserName = Accounts[2].UserName;

            request = new Request(client, HttpMethod.Get, $"/ships/flights?username={Account0UserName.ToUpper()}", Accounts[0].GetReadDef());
            var response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<Flight>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            Assert.That(response.UserNameToData[Account0UserName].Data, Is.Not.Empty);
            Assert.That(response.UserNameToData[Account0UserName].Errors, Is.Empty);

            // Test forbidden
            request.AuthDef = Accounts[2].GetReadDef();
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Test badusername
            request.AuthDef = Accounts[0].GetReadDef();
            request.EndPoint = $"/ships/flights?username=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Test not having read to one of the users
            request.EndPoint = $"/ships/flights?username={Account0UserName}&username={Account2UserName}";
            response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<Flight>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);

            Assert.That(response.UserNameToData[Account0UserName].Data, Is.Not.Empty);
            Assert.That(response.UserNameToData[Account0UserName].Errors, Is.Empty);

            Assert.That(response.UserNameToData[Account2UserName].Data, Is.Empty);
            Assert.That(response.UserNameToData[Account2UserName].Errors, Is.Not.Empty);

            // Test not having read to either of the users
            request.EndPoint = $"/ships/flights?username={Account1UserName}&username={Account2UserName}";
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();
        }
    }
}
