﻿using FIOAPI.Payloads;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class ContractTests : FIOAPIIntegrationTest
    {
        private static string TestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Contract");

        [Test]
        public async Task TestContracts()
        {
            var Account = TestUserAccount.MakeAccount();

            var TestFilesPath = Path.Combine(TestDataFolder, "MESG_CONTRACTS_CONTRACTS");
            await client.AssertPutDataCommon<Payloads.Contract.MESG_CONTRACTS_CONTRACTS>("/contracts", TestFilesPath, Account.GetWriteDef());

            TestFilesPath = Path.Combine(TestDataFolder, "MESG_CONTRACTS_CONTRACT");
            await client.AssertPutDataCommon<Payloads.Contract.MESG_CONTRACTS_CONTRACT>("/contracts/contract", TestFilesPath, Account.GetWriteDef());

            request = new Request(client, HttpMethod.Get, $"/contracts?UserName={Account.UserName}", Account.GetReadDef());
            var response = await request.GetResponseAsync<RetrievalResponse<List<DB.Model.Contract>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData, Is.Not.Empty);
            Assert.That(response.UserNameToData.ContainsKey(Account.UserName), Is.True);
            Assert.That(response.UserNameToData[Account.UserName].Errors, Is.Empty);
            Assert.That(response.UserNameToData[Account.UserName].Data, Is.Not.Empty);
        }
    }
}
