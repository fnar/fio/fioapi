﻿namespace FIOAPI.Integration.Test.Tests
{
    public class AuthSharedState
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public Guid RegistrationGuid { get; set; }
        public string? JWTToken { get; set; }
        public Guid ReadOnlyAPIKey { get; set; }
        public Guid WriteAPIKey { get; set; }

        public AuthSharedState()
        {
            UserName = Utils.GenerateRandomString(16, CharacterSet.UserNameCharacters);
            Password = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
        }
    }

    [Parallelizable]
    public class AuthTests : FIOAPIIntegrationTest
    {
        private AuthSharedState _state = new();

        private static string UserName = "";
        private static string Password = "";
        private static Guid RegistrationGuid;
        private static string? JWTToken;
        private static Guid ReadOnlyAPIKey;
        private static Guid WriteAPIKey;

        public AuthTests()
        {
            UserName = _state.UserName;
            Password = _state.Password;
            RegistrationGuid = _state.RegistrationGuid;
            JWTToken = _state.JWTToken;
            ReadOnlyAPIKey = _state.ReadOnlyAPIKey;
            WriteAPIKey = _state.WriteAPIKey;
        }

        private void StoreSharedState()
        {
            _state.UserName = UserName;
            _state.Password = Password;
            _state.RegistrationGuid = RegistrationGuid;
            _state.JWTToken = JWTToken;
            _state.ReadOnlyAPIKey = ReadOnlyAPIKey;
            _state.WriteAPIKey = WriteAPIKey;
        }

        [Test, Order(0)]
        public async Task AutoRequestRegister()
        {
            var autoRequestRegisterPayload = new Payloads.Auth.AutoRequestRegister();
            autoRequestRegisterPayload.payload = new Payloads.Auth.AutoRequestRegisterPayload();
            autoRequestRegisterPayload.payload.username = UserName + " - Bot";
            autoRequestRegisterPayload.payload.admin = false;
            Assert.That(autoRequestRegisterPayload.PassesValidation(), Is.False); // Ensure you can't auto-request a bot

            // Make it work properly now
            autoRequestRegisterPayload.payload.username = UserName;
            autoRequestRegisterPayload.AssertPassesValidation();
            
            request = new Request(client, HttpMethod.Post, "/auth/autorequestregister");
            Assert.That(request.ContentType, Is.EqualTo("text/plain"));
            request.SetPayload(autoRequestRegisterPayload);
            Assert.That(request.ContentType, Is.EqualTo("application/json"));

            var response = await request.GetResponseAsync<Payloads.Auth.AutoRequestRegisterResponse>();
            Assert.That(response, Is.Not.Null);
            request.AssertOK();
            Assert.Multiple(() =>
            {
                Assert.That(response.PassesValidation(), Is.True);
                Assert.That(UserName, Is.EqualTo(response.UserName));
                Assert.That(response.RegistrationGuid, Is.Not.EqualTo(Guid.Empty));
                Assert.That(response.RegistrationTime, Is.Not.EqualTo(DateTime.MinValue));
                Assert.That(response.RegistrationTime, Is.Not.EqualTo(DateTime.MaxValue));
            });

            Assert.That(response.PassesValidation(), Is.True);

            RegistrationGuid = response.RegistrationGuid;
            StoreSharedState();
        }

        [Test, Order(1)]
        public async Task Register()
        {
            
            request = new Request(client, HttpMethod.Post, "/auth/register");

            var registerPayload = new Payloads.Auth.Register();
            registerPayload.UserName = UserName;
            registerPayload.Password = Password;

            // Intentionally not including Guid
            Assert.That(registerPayload.PassesValidation(), Is.False);
            request.SetPayload(registerPayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Not the right guid
            registerPayload.RegistrationGuid = Guid.NewGuid();
            registerPayload.AssertPassesValidation();
            request.SetPayload(registerPayload);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Success, the correct guid
            registerPayload.RegistrationGuid = RegistrationGuid;
            registerPayload.AssertPassesValidation();
            request.SetPayload(registerPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }

        [Test, Order(2)]
        public async Task Login()
        {
            
            request = new Request(client, HttpMethod.Post, "/auth/login");

            var loginPayload = new Payloads.Auth.Login();
            loginPayload.UserName = "ab";
            loginPayload.Password = "cd";
            Assert.That(loginPayload.PassesValidation(), Is.False);

            // Intentional bad request
            request.SetPayload(loginPayload);
            var response = await request.GetResponseAsync<Payloads.Auth.LoginResponse>();
            request.AssertBadRequest();

            // Intentional bad username & password
            loginPayload.UserName = Utils.GenerateRandomString(16, CharacterSet.UserNameCharacters);
            loginPayload.Password = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
            loginPayload.AssertPassesValidation();
            request.SetPayload(loginPayload);
            response = await request.GetResponseAsync<Payloads.Auth.LoginResponse>();
            request.AssertUnauthorized();

            // Good username, bad password
            loginPayload.UserName = UserName;
            loginPayload.AssertPassesValidation();
            request.SetPayload(loginPayload);
            response = await request.GetResponseAsync<Payloads.Auth.LoginResponse>();
            request.AssertUnauthorized();

            // Good username & password
            loginPayload.Password = Password;
            loginPayload.AssertPassesValidation();
            request.SetPayload(loginPayload);
            response = await request.GetResponseAsync<Payloads.Auth.LoginResponse>();
            Assert.That(response, Is.Not.Null);
            request.AssertOK();
            response.AssertPassesValidation();
            JWTToken = response!.Token;
            StoreSharedState();
        }

        [Test, Order(3)]
        public async Task CreateAPIKey()
        {
            Assert.That(JWTToken, Is.Not.Null);
            AuthDef authDef = new()
            {
                AuthType = AuthType.Bearer,
                Token = JWTToken
            };

            
            request = new Request(client, HttpMethod.Post, "/auth/createapikey", authDef);

            var createAPIKeyPayload = new Payloads.Auth.CreateAPIKey();
            createAPIKeyPayload.UserName = "ab";
            createAPIKeyPayload.Password = "cd";
            createAPIKeyPayload.ApplicationName = "Integration Test";
            createAPIKeyPayload.AllowWrites = false;
            Assert.That(createAPIKeyPayload.PassesValidation(), Is.False);

            // Intentional bad request
            request.SetPayload(createAPIKeyPayload);
            var response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            request.AssertBadRequest();

            // Bad password
            createAPIKeyPayload.UserName = UserName;
            createAPIKeyPayload.Password = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
            createAPIKeyPayload.AssertPassesValidation();
            request.SetPayload(createAPIKeyPayload);
            response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            request.AssertUnauthorized();

            // Good username & password
            createAPIKeyPayload.Password = Password;
            createAPIKeyPayload.AssertPassesValidation();
            request.SetPayload(createAPIKeyPayload);
            response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            Assert.That(response, Is.Not.Null);
            request.AssertOK();
            response.AssertPassesValidation();
            ReadOnlyAPIKey = response!.APIKey;

            createAPIKeyPayload.AllowWrites = true;
            createAPIKeyPayload.AssertPassesValidation();
            request.SetPayload(createAPIKeyPayload);
            response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            response.AssertPassesValidation();
            Assert.That(response.APIKey, Is.Not.EqualTo(Guid.Empty));
            WriteAPIKey = response.APIKey;
            StoreSharedState();
        }

        [Test, Order(4)]
        public async Task LoginCheck()
        {
            // Test JWTToken
            Assert.That(JWTToken, Is.Not.Null);
            AuthDef authDef = new()
            {
                AuthType = AuthType.Bearer,
                Token = JWTToken
            };
            
            request = new Request(client, HttpMethod.Get, "/auth", authDef);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Test APIKey
            Assert.That(ReadOnlyAPIKey, Is.Not.EqualTo(Guid.Empty));
            authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = ReadOnlyAPIKey.ToString("N")
            };
            request = new Request(client, HttpMethod.Get, "/auth", authDef);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Test not logged in
            request = new Request(client, HttpMethod.Get, "/auth");
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();
        }

        [Test, Order(5)]
        public async Task CreateAPIKeyWithReadOnlyAPIKey()
        {
            Assert.That(ReadOnlyAPIKey, Is.Not.EqualTo(Guid.Empty));
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = ReadOnlyAPIKey.ToString("N")
            };

            
            request = new Request(client, HttpMethod.Post, "/auth/createapikey", authDef);

            var createAPIKeyPayload = new Payloads.Auth.CreateAPIKey();
            createAPIKeyPayload.UserName = UserName;
            createAPIKeyPayload.Password = Password;
            createAPIKeyPayload.ApplicationName = "This Shouldn't Work";
            createAPIKeyPayload.AllowWrites = true;
            createAPIKeyPayload.AssertPassesValidation();

            // This should not work because the APIKey we're using for auth doesn't have write
            _ = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
            request.AssertForbidden();
        }

        [Test, Order(6)]
        public async Task CreateTonsOfAPIKeys()
        {
            Assert.That(WriteAPIKey, Is.Not.EqualTo(Guid.Empty));
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = WriteAPIKey.ToString("N")
            };

            
            request = new Request(client, HttpMethod.Post, "/auth/createapikey", authDef);

            var createAPIKeyPayload = new Payloads.Auth.CreateAPIKey();
            createAPIKeyPayload.UserName = UserName;
            createAPIKeyPayload.Password = Password;
            createAPIKeyPayload.AllowWrites = false;
            createAPIKeyPayload.AssertPassesValidation();

            // Keep creating APIKeys until we're no longer allowed to do so

            int APIKeyCount = 1;
            Payloads.Auth.CreateAPIKeyResponse? response;
            do
            {
                createAPIKeyPayload.ApplicationName = $"Iteration {APIKeyCount}";
                request.SetPayload(createAPIKeyPayload);
                response = await request.GetResponseAsync<Payloads.Auth.CreateAPIKeyResponse>();
                Assert.That(request.StatusCode, Is.AnyOf(HttpStatusCode.OK, HttpStatusCode.NotAcceptable));
            } while (HttpStatusCode.OK == request.StatusCode);
        }

        [Test, Order(7)]
        public async Task ListAPIKeys()
        {
            Assert.That(WriteAPIKey, Is.Not.EqualTo(Guid.Empty));
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = WriteAPIKey.ToString("N")
            };

            
            request = new Request(client, HttpMethod.Get, "/auth/listapikeys", authDef);
            var response = await request.GetResponseAsync<List<APIKey>>();
            Assert.That(response, Is.Not.Null);
            Assert.That(response, Has.Count.EqualTo(Controllers.AuthController.MaxAPIKeyCount));
        }

        [Test, Order(8)]
        public async Task RevokeAPIKey()
        {
            Assert.That(ReadOnlyAPIKey, Is.Not.EqualTo(Guid.Empty));
            Assert.That(WriteAPIKey, Is.Not.EqualTo(Guid.Empty));
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = ReadOnlyAPIKey.ToString("N")
            };

            request = new Request(client, HttpMethod.Post, "/auth/revokeapikey", authDef);

            var payload = new Payloads.Auth.RevokeAPIKey();
            payload.UserName = UserName;
            payload.Password = Password;
            payload.APIKeyToRevoke = ReadOnlyAPIKey;
            payload.AssertPassesValidation();

            // Attempt to use read-only APIKey
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Set the correct APIKey now
            authDef.Token = WriteAPIKey.ToString("N");
            request = new Request(client, HttpMethod.Post, "/auth/revokeapikey", authDef);

            // Intentional bad request
            payload.UserName = "ab";
            Assert.That(payload.PassesValidation(), Is.False);
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Intentionally bad password
            payload.UserName = UserName;
            payload.Password = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
            payload.APIKeyToRevoke = ReadOnlyAPIKey;
            payload.AssertPassesValidation();
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Intentionally bad APIKey
            payload.Password = Password;
            payload.APIKeyToRevoke = Guid.NewGuid();
            payload.AssertPassesValidation();
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            request.AssertNoContent();

            payload.APIKeyToRevoke = ReadOnlyAPIKey;
            payload.AssertPassesValidation();
            request.SetPayload(payload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();
            ReadOnlyAPIKey = Guid.Empty;
            StoreSharedState();
        }

        [Test, Order(9)]
        public async Task ChangePassword()
        {
            Assert.That(WriteAPIKey, Is.Not.EqualTo(Guid.Empty));
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = WriteAPIKey.ToString("N")
            };

            var newPassword = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);

            request = new Request(client, HttpMethod.Post, "/auth/changepassword", authDef);

            var changePasswordPayload = new Payloads.Auth.ChangePassword();
            changePasswordPayload.OldPassword = "ab";
            changePasswordPayload.NewPassword = newPassword;
            Assert.That(changePasswordPayload.PassesValidation(), Is.False);

            // Bad request
            request.SetPayload(changePasswordPayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Bad password
            changePasswordPayload.OldPassword = Utils.GenerateRandomString(32, CharacterSet.PasswordCharacters);
            changePasswordPayload.AssertPassesValidation();
            request.SetPayload(changePasswordPayload);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Success
            changePasswordPayload.OldPassword = Password;
            changePasswordPayload.AssertPassesValidation();
            request.SetPayload(changePasswordPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();
            Password = newPassword;
            StoreSharedState();
        }

        [Test, Order(10)]
        public async Task DeleteAccount()
        {
            Assert.That(WriteAPIKey, Is.Not.EqualTo(Guid.Empty));
            AuthDef authDef = new()
            {
                AuthType = AuthType.APIKey,
                Token = WriteAPIKey.ToString("N")
            };

            // Bad request (password must be >= 3 chars)
            var deletePayload = new Payloads.Auth.DeleteAccount { Password = "f" };
            Assert.That(deletePayload.PassesValidation(), Is.False);
            request = new Request(client, HttpMethod.Post, "/auth/deleteaccount", authDef);
            request.SetPayload(deletePayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Bad password
            deletePayload.Password = "abcdefghij";
            request.SetPayload(deletePayload);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Ok
            deletePayload.Password = Password;
            request.SetPayload(deletePayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }
    }
}
