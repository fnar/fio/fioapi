﻿using FIOAPI.Payloads.Production;
using NUnit.Framework.Internal;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class ProductionLineTests : FIOAPIIntegrationTest
    {
        private static string ProdLineTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Production");

        private static TestUserAccount Account = TestUserAccount.MakeAccount();

        [Test, Order(0)]
        public async Task PutProdLines()
        {
            var TestDataPath = Path.Combine(ProdLineTestDataFolder, "MESG_PRODUCTION_SITE_PRODUCTION_LINES");
            await client.AssertPutDataCommon<SITE_PRODUCTION_LINES>("/productionline", TestDataPath, Account.GetWriteDef());

            TestDataPath = Path.Combine(ProdLineTestDataFolder, "ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED");
            await client.AssertPutDataCommon<ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED>("/productionline/updated", TestDataPath, Account.GetWriteDef());

            TestDataPath = Path.Combine(ProdLineTestDataFolder, "MESG_PRODUCTION_PRODUCTION_LINE");
            await client.AssertPutDataCommon<MESG_PRODUCTION_PRODUCTION_LINE>("/productionline/msg_updated", TestDataPath, Account.GetWriteDef());

            TestDataPath = Path.Combine(ProdLineTestDataFolder, "ROOT_PRODUCTION_ORDER_ADDED");
            await client.AssertPutDataCommon<ROOT_PRODUCTION_ORDER_ADDED>("/productionline/order/added", TestDataPath, Account.GetWriteDef());

            TestDataPath = Path.Combine(ProdLineTestDataFolder, "ROOT_PRODUCTION_ORDER_UPDATED");
            await client.AssertPutDataCommon<ROOT_PRODUCTION_ORDER_UPDATED>("/productionline/order/updated", TestDataPath, Account.GetWriteDef());

            TestDataPath = Path.Combine(ProdLineTestDataFolder, "ROOT_PRODUCTION_ORDER_REMOVED");
            await client.AssertPutDataCommon<ROOT_PRODUCTION_ORDER_REMOVED>("/productionline/order/removed", TestDataPath, Account.GetWriteDef());

            TestDataPath = Path.Combine(ProdLineTestDataFolder, "MESG_PRODUCTION_ORDER_ADDED");
            await client.AssertPutDataCommon<MESG_PRODUCTION_ORDER_ADDED>("/productionline/order/msg_added", TestDataPath, Account.GetWriteDef());

            TestDataPath = Path.Combine(ProdLineTestDataFolder, "MESG_PRODUCTION_ORDER_REMOVED");
            await client.AssertPutDataCommon<MESG_PRODUCTION_ORDER_REMOVED>("/productionline/order/msg_removed", TestDataPath, Account.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task PutGarbageProdLines()
        {
            var TestDataPath = Path.Combine(ProdLineTestDataFolder, "ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_GARBAGE");
            await client.AssertPutDataCommon<ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED>("/productionline/updated", TestDataPath, Account.GetWriteDef());
            await Task.Delay(TimeSpan.FromSeconds(6.1));

            var TestFiles = Utils.GetFiles(TestDataPath);

            ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED FirstPayloadObj = Utils.DeserializeFromFile<ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED>(TestFiles.First());
            string ProductionLineId = FirstPayloadObj.payload.id;

            request = new Request(client, HttpMethod.Get, $"/productionline?username={Account.UserName.ToUpper()}", Account.GetReadDef());
            var prodLinesResponse = await request.GetResponseAsync<Payloads.RetrievalResponse<List<DB.Model.ProductionLine>>>();
            request.AssertOK();
            Assert.That(prodLinesResponse, Is.Not.Null);

            var prodLineToFind = prodLinesResponse.UserNameToData.First().Value.Data.FirstOrDefault(pl => pl.ProductionLineId == ProductionLineId);
            Assert.That(prodLineToFind, Is.Not.Null);
            Assert.That(prodLineToFind.Efficiency, Is.EqualTo(1.0));
        }

        [Test, Order(2)]
        public async Task GetProdLines()
        {
            request = new Request(client, HttpMethod.Get, $"/productionline?username={Account.UserName.ToUpper()}", Account.GetReadDef());
            var response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<DB.Model.ProductionLine>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            Assert.That(response.UserNameToData.Keys.Count, Is.EqualTo(1));
            var firstSite = response.UserNameToData.First().Value.Data[0];
            Assert.That(firstSite.Orders, Is.Empty);

            request.EndPoint = "/productionline?include_orders=true";
            response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<DB.Model.ProductionLine>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            Assert.That(response.UserNameToData.Keys.Count, Is.EqualTo(1));
            Assert.That(response.UserNameToData.First().Value.Data.Any(pl => pl.Orders.Count != 0), Is.True);
        }
    }
}
