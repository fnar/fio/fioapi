﻿namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class CurrencyTests : FIOAPIIntegrationTest
    {
        private static string CurrencyTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Currency", "MESG_FOREX_BROKER_DATA");

        [Test, Order(0)]
        public async Task PutBrokerData()
        {
            var Account = TestUserAccount.MakeAccount();
            await client.AssertPutDataCommon<Payloads.Currency.MESG_FOREX_BROKER_DATA>("/fx", CurrencyTestDataFolder, Account.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task GetBrokerData()
        {
            // We should only have n*(n-1) total groupings
            int TotalFXTickers = Constants.ValidCurrencies.Count * (Constants.ValidCurrencies.Count - 1);

            request = new Request(client, HttpMethod.Get, "/fx?include_buys=false&include_sells=false");
            var results = await request.GetResponseAsync<List<FX>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results.Count, Is.EqualTo(TotalFXTickers));

            // Make sure with a plain query we don't have buys and sells
            results.ForEach(result =>
            {
                Assert.That(result.BuyOrders, Is.Empty);
                Assert.That(result.SellOrders, Is.Empty);
            });

            // Test including buys
            request = new Request(client, HttpMethod.Get, "/fx?include_sells=false");
            results = await request.GetResponseAsync<List<FX>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results.Count, Is.EqualTo(TotalFXTickers));

            bool HasAtLeastOneBuy = false;
            foreach (var result in results)
            {
                Assert.That(result.SellOrders, Is.Empty);
                HasAtLeastOneBuy |= result.BuyOrders.Any();
            }
            Assert.That(HasAtLeastOneBuy, Is.True);

            // Test including sells
            request = new Request(client, HttpMethod.Get, "/fx?include_buys=false");
            results = await request.GetResponseAsync<List<FX>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results.Count, Is.EqualTo(TotalFXTickers));

            bool HasAtLeastOneSell = false;
            foreach (var result in results)
            {
                Assert.That(result.BuyOrders, Is.Empty);
                HasAtLeastOneSell |= result.SellOrders.Any();
            }
            Assert.That(HasAtLeastOneSell, Is.True);

            // Test including both buys and sells
            request = new Request(client, HttpMethod.Get, "/fx");
            results = await request.GetResponseAsync<List<FX>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results.Count, Is.EqualTo(TotalFXTickers));

            HasAtLeastOneBuy = false;
            HasAtLeastOneSell = false;
            foreach (var result in results)
            {
                HasAtLeastOneBuy |= result.BuyOrders.Any();
                HasAtLeastOneSell |= result.SellOrders.Any();
            }
            Assert.That(HasAtLeastOneBuy, Is.True);
            Assert.That(HasAtLeastOneSell, Is.True);
        }
    }
}
