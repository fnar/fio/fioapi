﻿using FIOAPI.Payloads.CSV;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class BuildingTests : FIOAPIIntegrationTest
    {
        private static string TestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Building", "MESG_WORLD_REACTOR_DATA");

        private List<string> TestFiles
        {
            get
            {
                if (_TestFiles.Count == 0)
                {
                    _TestFiles = Utils.GetFiles(TestDataFolder);
                }

                return _TestFiles;
            }
        }
        private List<string> _TestFiles = new List<string>();

        [Test, Order(0)]
        public async Task PutAll()
        {
            var account = TestUserAccount.MakeAccount();

            
            await client.AssertPutDataCommon<Payloads.Building.MESG_WORLD_REACTOR_DATA>("/building", TestDataFolder, account.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task GetAll()
        {
            
            request = new Request(client, HttpMethod.Get, "/building");
            var results = await request.GetResponseAsync<List<DB.Model.Building>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
            Assert.That(results.Count, Is.EqualTo(TestFiles.Count));

            request = new Request(client, HttpMethod.Get, "/building");
            results = await request.GetResponseAsync<List<DB.Model.Building>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
            Assert.That(results.Count, Is.EqualTo(TestFiles.Count));
            foreach (var result in results)
            {
                Assert.That(result.Costs, Is.Empty);
                Assert.That(result.Recipes, Is.Empty);
            }

            request.EndPoint = "/building?include_costs=true&include_recipes=true";
            results = await request.GetResponseAsync<List<DB.Model.Building>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
            Assert.That(results.Count, Is.EqualTo(TestFiles.Count));

            // Not all buildings have costs and recipes (some don't)
            bool FoundCosts = false;
            bool FoundRecipes = false;
            foreach (var result in results)
            {
                FoundCosts |= result.Costs.Any();
                FoundRecipes |= result.Recipes.Any();
            }

            Assert.That(FoundCosts, Is.True);
            Assert.That(FoundRecipes, Is.True);
        }

        [Test, Order(2)]
        public async Task TestCSV()
        {
            request = new Request(client, HttpMethod.Get, "/csv/buildings?include_header=true");
            var result = await request.GetStringResponseAsync();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Not.Empty);

            var csvResults = Utils.DeserializeFromCSV<CsvBuilding>(result);
            Assert.That(csvResults, Is.Not.Null);
            Assert.That(csvResults, Is.Not.Empty);
        }
    }
}
