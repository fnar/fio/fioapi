﻿using FIOAPI.Payloads.LocalMarket;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class LocalMarketTests : FIOAPIIntegrationTest
    {
        private static string LocalMarketTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "LocalMarket", "PATH_localmarkets-_-ads");

        [Test, Order(0)]
        public async Task PutLocalMarketAds()
        {
            var Account = TestUserAccount.MakeAccount();
            await client.AssertPutDataCommon<PATH_LOCALMARKET_AD>("/localmarket", LocalMarketTestDataFolder, Account.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task GetAllLocalMarketAds()
        {
            request = new Request(client, HttpMethod.Get, "/localmarket");
            var results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
            Assert.That(results.Select(r => r.PlanetId).Distinct().Count(), Is.GreaterThan(1));
            Assert.That(results.Select(r => r.Type).Distinct().Count(), Is.EqualTo(3));

            request = new Request(client, HttpMethod.Get, "/localmarket?include_buys=false");
            results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
            Assert.That(results.Select(r => r.PlanetId).Distinct().Count(), Is.GreaterThan(1));
            Assert.That(results.Select(r => r.Type).Distinct().Count(), Is.EqualTo(2));
        }

        [Test, Order(1)]
        public async Task GetLocalMarketAds()
        {
            var TestFiles = Utils.GetFiles(LocalMarketTestDataFolder);
            Assert.That(TestFiles, Is.Not.Empty);

            var FirstTestFile = TestFiles.First();
            var localMarketData = JsonSerializer.Deserialize<Payloads.LocalMarket.PATH_LOCALMARKET_AD>(File.ReadAllText(FirstTestFile));
            Assert.That(localMarketData, Is.Not.Null);
            Assert.That(localMarketData.PassesValidation(), Is.True);

            //var lmNaturalId = localMarketData.payload.message.payload.body[0].address.lines.Last().entity.naturalId;
            var lmNaturalId = "KW-688c";
            request = new Request(client, HttpMethod.Get, $"/localmarket/{lmNaturalId}");
            var results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);

            request = new Request(client, HttpMethod.Get, $"/localmarket/{lmNaturalId}?include_buys=false&include_sells=false");
            results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);

            results.ForEach(result =>
            {
                Assert.That(result.Type == "COMMODITY_SHIPPING", Is.True);
            });

            request = new Request(client, HttpMethod.Get, $"/localmarket/{lmNaturalId}?include_buys=false&include_sells=false&include_shipments=false");
            results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Empty);
        }

        [Test, Order(1)]
        public async Task GetSingleLocalMarketAd()
        {
            // Known example values
            const string LocalMarketIdentifier = "ANT";
            const int LocalMarketAdId = 8024;

            // Not found
            request = new Request(client, HttpMethod.Get, $"/localmarket/{LocalMarketIdentifier}/1234");
            await request.GetNoResultResponseAsync();
            request.AssertNotFound();

            // Valid
            request = new Request(client, HttpMethod.Get, $"/localmarket/{LocalMarketIdentifier}/{LocalMarketAdId}");
            var result = await request.GetResponseAsync<LocalMarketAd>();
            request.AssertOK();
        }

        [Test, Order(1)]
        public async Task GetCompanyAds()
        {
            // Known example values
            const string CompanyId = "f412e3c0305c121aebc43ce8643418d8";
            const string CompanyName = "Ascaron";
            const string CompanyCode = "ASCA";

            // Not found
            request = new Request(client, HttpMethod.Get, "/localmarket/company/AAAA");
            var results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Empty);

            // Found - CompanyId
            request = new Request(client, HttpMethod.Get, $"/localmarket/company/{CompanyId}");
            var companyIdResults = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(companyIdResults, Is.Not.Null);
            Assert.That(companyIdResults, Is.Not.Empty);

            // Found - CompanyName
            request = new Request(client, HttpMethod.Get, $"/localmarket/company/{CompanyName}");
            var companyNameResults = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(companyNameResults, Is.Not.Null);
            Assert.That(companyNameResults, Is.Not.Empty);

            // Found - CompanyCode
            request = new Request(client, HttpMethod.Get, $"/localmarket/company/{CompanyCode}");
            var companyCodeResults = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(companyCodeResults, Is.Not.Null);
            Assert.That(companyCodeResults, Is.Not.Empty);

            Assert.That(companyNameResults.Count, Is.EqualTo(companyIdResults.Count));
            Assert.That(companyCodeResults.Count, Is.EqualTo(companyIdResults.Count));
        }

        [Test, Order(1)]
        public async Task GetShippingAds()
        {
            // Test values
            const string OriginNaturalId = "ANT";
            const string DestinationNaturalId = "SE-110c";

            // Not found
            request = new Request(client, HttpMethod.Get, "/localmarket/shipping?origin=BEN&destination=SE-110a");
            var results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Empty);

            // Found
            request = new Request(client, HttpMethod.Get, $"/localmarket/shipping?origin={OriginNaturalId}&destination={DestinationNaturalId}");
            results = await request.GetResponseAsync<List<LocalMarketAd>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
        }
    }
}
