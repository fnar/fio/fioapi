﻿using FIOAPI.Payloads;
using FIOAPI.Payloads.CSV;
using FIOAPI.Payloads.Sites;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class SiteTests : FIOAPIIntegrationTest
    {
        private static TestUserAccount Account = TestUserAccount.MakeAdminAccount();

        private static string SiteTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Site");

        [Test, Order(0)]
        public async Task PutSites()
        {
            var MesgSiteSitesTestDataPath = Path.Combine(SiteTestDataFolder, "MESG_SITE_SITES");
            await client.AssertPutDataCommon<MESG_SITE_SITES>("/sites/msg_sites", MesgSiteSitesTestDataPath, Account.GetWriteDef());

            var PathSitePlatformUpdatedTestDataPath = Path.Combine(SiteTestDataFolder, "ROOT_SITE_PLATFORM_UPDATED");
            await client.AssertPutDataCommon<ROOT_SITE_PLATFORM_UPDATED>("/sites/updated", PathSitePlatformUpdatedTestDataPath, Account.GetWriteDef());

            var RootSiteSitesTestDataPath = Path.Combine(SiteTestDataFolder, "ROOT_SITE_SITES");
            await client.AssertPutDataCommon<ROOT_SITE_SITES>("/sites/sites", RootSiteSitesTestDataPath, Account.GetWriteDef());

            var RootPlatformBuiltTestDataPath = Path.Combine(SiteTestDataFolder, "ROOT_SITE_PLATFORM_BUILT");
            await client.AssertPutDataCommon<ROOT_SITE_PLATFORM_BUILT>("/sites/built", RootPlatformBuiltTestDataPath, Account.GetWriteDef());

            var RootPlatformUpdatedTestDataPath = Path.Combine(SiteTestDataFolder, "ROOT_SITE_PLATFORM_UPDATED");
            await client.AssertPutDataCommon<ROOT_SITE_PLATFORM_UPDATED>("/sites/updated", RootPlatformUpdatedTestDataPath, Account.GetWriteDef());

            var MesgPlatformRemovedTestDataPath = Path.Combine(SiteTestDataFolder, "MESG_SITE_PLATFORM_REMOVED");
            await client.AssertPutDataCommon<MESG_SITE_PLATFORM_REMOVED>("sites/msg_removed", MesgPlatformRemovedTestDataPath, Account.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task GetSites()
        {
            request = new Request(client, HttpMethod.Get, $"/sites?username={Account.UserName.ToUpper()}", Account.GetReadDef());
            var response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<DB.Model.Site>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            Assert.That(response.UserNameToData.Keys.Count, Is.EqualTo(1));
            var firstSite = response.UserNameToData.First().Value.Data[0];
            Assert.That(firstSite.Buildings, Is.Empty);

            request.EndPoint = "/sites?include_buildings=true";
            response = await request.GetResponseAsync<Payloads.RetrievalResponse<List<DB.Model.Site>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData.Keys, Is.Not.Empty);
            Assert.That(response.UserNameToData.Keys.Count, Is.EqualTo(1));
            firstSite = response.UserNameToData.First().Value.Data[0];
            Assert.That(firstSite.Buildings, Is.Not.Empty);

            var apiKey = await Account.GetAPIKey();
            request = new Request(client, HttpMethod.Get, $"/csv/sites?UserName={Account.UserName}&APIKey={apiKey}&include_header=true");
            var csvResponse = await request.GetStringResponseAsync();
            request.AssertOK();
            Assert.That(csvResponse, Is.Not.Null);
            Assert.That(csvResponse, Is.Not.Empty);

            var csvResults = Utils.DeserializeFromCSV<CsvSite>(csvResponse);
            Assert.That(csvResults, Is.Not.Null);
            Assert.That(csvResults, Is.Not.Empty);
        }

        [Test, Order(2)]
        public async Task PutExperts()
        {
            var ExpertTestFiles = Utils.GetFiles(Path.Combine(SiteTestDataFolder, "MESG_EXPERTS_EXPERTS"));
            Assert.That(ExpertTestFiles, Is.Not.Empty);

            request = new Request(client, HttpMethod.Put, "/sites/experts");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();
            request.AuthDef = Account.GetWriteDef();

            foreach (var ExpertTestFile in ExpertTestFiles)
            {
                var expertPayload = JsonSerializer.Deserialize<MESG_EXPERTS_EXPERTS>(File.ReadAllText(ExpertTestFile));
                Assert.That(expertPayload, Is.Not.Null);
                Assert.That(expertPayload.PassesValidation_Throw(), Is.True, ExpertTestFile);

                request.SetPayloadFromFile(ExpertTestFile);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Test, Order(3)]
        public async Task GetExperts()
        {
            request = new Request(client, HttpMethod.Get, $"/sites/experts?UserName={Account.UserName}", Account.GetReadDef());
            var response = await request.GetResponseAsync<RetrievalResponse<List<Expert>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData, Is.Not.Empty);
            Assert.That(response.UserNameToData.ContainsKey(Account.UserName), Is.True);
            Assert.That(response.UserNameToData[Account.UserName].Errors, Is.Empty);
            Assert.That(response.UserNameToData[Account.UserName].Data, Is.Not.Empty);
        }

        [Test, Order(2)]
        public async Task PutWorkforce()
        {
            var TestDataPath = Path.Combine(SiteTestDataFolder, "MESG_WORKFORCE_WORKFORCES");
            await client.AssertPutDataCommon<MESG_WORKFORCE_WORKFORCES>("/sites/workforces", TestDataPath, Account.GetWriteDef());

            TestDataPath = Path.Combine(SiteTestDataFolder, "ROOT_WORKFORCE_WORKFORCES_UPDATED");
            await client.AssertPutDataCommon<ROOT_WORKFORCE_WORKFORCES_UPDATED>("/sites/workforces/updated", TestDataPath, Account.GetWriteDef());
        }

        [Test, Order(3)]
        public async Task GetWorkforce()
        {
            request = new Request(client, HttpMethod.Get, $"/sites/workforces?UserName={Account.UserName}", Account.GetReadDef());
            var response = await request.GetResponseAsync<RetrievalResponse<List<Workforce>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData, Is.Not.Empty);
            Assert.That(response.UserNameToData.ContainsKey(Account.UserName), Is.True);
            Assert.That(response.UserNameToData[Account.UserName].Errors, Is.Empty);
            Assert.That(response.UserNameToData[Account.UserName].Data, Is.Not.Empty);
            Assert.That(response.UserNameToData[Account.UserName].Data.Any(w => w.LastTickTime != null), Is.True);
        }
    }
}
