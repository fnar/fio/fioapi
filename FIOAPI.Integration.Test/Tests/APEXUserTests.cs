﻿namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class APEXUserTests : FIOAPIIntegrationTest
    {
        private static TestUserAccount Account = TestUserAccount.MakeAccount();

        private string TestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "User");

        public APEXUserTests()
        {
        }

        [Test, Order(0)]
        public async Task PutUsers()
        {
            var UserTestFiles = Utils.GetFiles(Path.Combine(TestDataFolder, "PATH_users-_"));
            Assert.That(UserTestFiles, Is.Not.Empty);

            request = new Request(client, HttpMethod.Put, "/apexuser");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();
            request.AuthDef = Account.GetWriteDef();

            foreach (var UserTestFile in UserTestFiles)
            {
                var data = JsonSerializer.Deserialize<Payloads.User.PATH_USERS>(File.ReadAllText(UserTestFile));
                Assert.That(data, Is.Not.Null);
                Assert.That(data.PassesValidation_Throw(), Is.True, UserTestFile);

                request.SetPayload(data);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }
        }

        [Test, Order(1)]
        public async Task PutPresence()
        {
            var PresenceTestFilePath = Path.Combine(TestDataFolder, "ROOT_PRESENCE_LIST");
            await client.AssertPutDataCommon<Payloads.User.ROOT_PRESENCE_LIST>("/apexuser/presence", PresenceTestFilePath, Account.GetWriteDef());
        }

        [Test, Order(2)]
        public async Task GetUsers()
        {
            request = new Request(client, HttpMethod.Get, "/apexuser");
            var APEXUsers = await request.GetResponseAsync<List<APEXUser>>();
            request.AssertOK();
            Assert.That(APEXUsers, Is.Not.Null);
            Assert.That(APEXUsers, Is.Not.Empty);
            Assert.That(APEXUsers.Any(u => u.LastOnlineTimestamp != null), Is.True);
        }
    }
}
