﻿using FIOAPI.Payloads.Global;
using FIOAPI.Payloads.Sites;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class GlobalTests : FIOAPIIntegrationTest
    {
        public static string GlobalTestFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Global");

        [Test]
        public async Task PutCountry()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();
            var CountryRegistryFolder = Path.Combine(GlobalTestFolder, "MESG_COUNTRY_REGISTRY_COUNTRIES");
            await client.AssertPutDataCommon<MESG_COUNTRY_REGISTRY_COUNTRIES>("/global/countries", CountryRegistryFolder, adminAccount.GetWriteDef(), RequireAdmin: true);

            // Successful retrieve
            request = new Request(client, HttpMethod.Get, "/global/countries");
            var countryList = await request.GetResponseAsync<List<DB.Model.Country>>();
            request.AssertOK();
            Assert.That(countryList, Is.Not.Null);
            Assert.That(countryList, Is.Not.Empty);
        }

        [Test]
        public async Task PutSimulationData()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();
            var SimulationDataFolder = Path.Combine(GlobalTestFolder, "MESG_SIMULATION_DATA");
            await client.AssertPutDataCommon<MESG_SIMULATION_DATA>("/global/simulationdata", SimulationDataFolder, adminAccount.GetWriteDef(), RequireAdmin: true);

            // Test GET Success
            request = new Request(client, HttpMethod.Get, "/global/simulationdata");
            var result = await request.GetResponseAsync<DB.Model.SimulationData>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public async Task PutComexExchanges()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();
            var ComexExchangeDataFolder = Path.Combine(GlobalTestFolder, "PATH_commodityexchanges");
            await client.AssertPutDataCommon<PATH_COMMODITY_EXCHANGES>("/global/comexexchanges", ComexExchangeDataFolder, adminAccount.GetWriteDef(), RequireAdmin: true);

            // Success retrieve
            request = new Request(client, HttpMethod.Get, "/global/comexexchanges");
            var result = await request.GetResponseAsync<List<DB.Model.ComexExchange>>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Not.Empty);
        }

        [Test]
        public async Task PutStations()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();
            var StationsDataFolder = Path.Combine(GlobalTestFolder, "PATH_stations-_");
            await client.AssertPutDataCommon<PATH_STATION>("/global/station", StationsDataFolder, adminAccount.GetWriteDef(), RequireAdmin: true);

            // Success retrieve
            const int TotalNumStations = 6;
            request = new Request(client, HttpMethod.Get, "/global/stations");
            var result = await request.GetResponseAsync<List<DB.Model.Station>>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Not.Empty);
            Assert.That(result.Count, Is.EqualTo(TotalNumStations));
        }

        [Test]
        public async Task WorkforceRequirements()
        {
            var account = TestUserAccount.MakeAccount();
            var adminAccount = TestUserAccount.MakeAdminAccount();

            // Do normal account first
            var WorkforceRequirementsFolder = Path.Combine(GlobalTestFolder, "MESG_WORKFORCE_WORKFORCES");
            await client.AssertPutDataCommon<MESG_WORKFORCE_WORKFORCES>("/sites/workforces", WorkforceRequirementsFolder, account.GetWriteDef());

            // Make sure it's empty
            request = new Request(client, HttpMethod.Get, "/global/workforceneeds");
            var result = await request.GetResponseAsync<List<DB.Model.WorkforceRequirement>>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Empty);

            // Use admin now
            await client.AssertPutDataCommon<MESG_WORKFORCE_WORKFORCES>("/sites/workforces", WorkforceRequirementsFolder, adminAccount.GetWriteDef());

            // Successful retrieve
            result = await request.GetResponseAsync<List<DB.Model.WorkforceRequirement>>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result, Is.Not.Empty);
            Assert.That(result[0].Needs, Is.Not.Empty);
        }
    }
}
