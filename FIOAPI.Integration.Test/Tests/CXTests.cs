﻿using FIOAPI.Payloads;
using FIOAPI.Payloads.CSV;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class CXTests : FIOAPIIntegrationTest
    {
        public static string ExchangeTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Exchange");

        [Test]
        public async Task CXEntryTests()
        {
            var Account = TestUserAccount.MakeAccount();
            var TestDataPath = Path.Combine(ExchangeTestDataFolder, "MESG_COMEX_BROKER_DATA");
            await client.AssertPutDataCommon<Payloads.CX.CX_PRICES>("/cx", TestDataPath, Account.GetWriteDef());

            // See if grabbing all data works
            request = new Request(client, HttpMethod.Get, "/cx/prices");
            var allPrices = await request.GetResponseAsync<List<CXEntry>>();
            Assert.That(allPrices, Is.Not.Null);
            Assert.That(allPrices, Is.Not.Empty);
            Assert.That(allPrices.SelectMany(p => p.BuyOrders).Any(), Is.False);
            Assert.That(allPrices.SelectMany(p => p.SellOrders).Any(), Is.False);

            request.EndPoint = "/cx/prices?include_buy_orders=true";
            allPrices = await request.GetResponseAsync<List<CXEntry>>();
            Assert.That(allPrices, Is.Not.Null);
            Assert.That(allPrices, Is.Not.Empty);
            Assert.That(allPrices.SelectMany(p => p.BuyOrders).Any(), Is.True);
            Assert.That(allPrices.SelectMany(p => p.SellOrders).Any(), Is.False);

            request.EndPoint = "/cx/prices?include_buy_orders=true&include_sell_orders=true";
            allPrices = await request.GetResponseAsync<List<CXEntry>>();
            Assert.That(allPrices, Is.Not.Null);
            Assert.That(allPrices, Is.Not.Empty);
            Assert.That(allPrices.SelectMany(p => p.BuyOrders).Any(), Is.True);
            Assert.That(allPrices.SelectMany(p => p.SellOrders).Any(), Is.True);

            // Test bad ticker
            request.EndPoint = "/cx/prices/ABCDE.CI1";
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Test bad exchange code
            request.EndPoint = "/cx/prices/DW.CII3";
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Test invalid ticker (no content)
            request.EndPoint = "/cx/prices/AAA.CI1";
            await request.GetNoResultResponseAsync();
            request.AssertNoContent();

            // Test known example
            request.EndPoint = "/cx/prices/DW.CI1";
            var price = await request.GetResponseAsync<CXEntry>();
            Assert.That(price, Is.Not.Null);
            Assert.That(price.BuyOrders, Is.Empty);
            Assert.That(price.SellOrders, Is.Empty);
            Assert.That(price.MMBuy, Is.Not.Null);
            Assert.That(price.MMSell, Is.Not.Null);

            request.EndPoint = "/cx/prices/DW.CI1?include_sell_orders=true";
            price = await request.GetResponseAsync<CXEntry>();
            Assert.That(price, Is.Not.Null);
            Assert.That(price.BuyOrders, Is.Empty);
            Assert.That(price.SellOrders, Is.Not.Empty);

            request.EndPoint = "/cx/prices/DW.CI1?include_buy_orders=true";
            price = await request.GetResponseAsync<CXEntry>();
            Assert.That(price, Is.Not.Null);
            Assert.That(price.BuyOrders, Is.Not.Empty);
            Assert.That(price.SellOrders, Is.Empty);

            request = new Request(client, HttpMethod.Get, "/csv/prices?include_header=true");
            var csvResponse = await request.GetStringResponseAsync();
            Assert.That(csvResponse, Is.Not.Null);
            Assert.That(csvResponse, Is.Not.Empty);

            var csvResults = Utils.DeserializeFromCSV<CsvPrices>(csvResponse);
            Assert.That(csvResults, Is.Not.Null);
            Assert.That(csvResults, Is.Not.Empty);
        }

        [Test]
        public async Task CXPCTests()
        {
            var Account = TestUserAccount.MakeAccount();

            var CXEntryDataFilePath = Path.Combine(ExchangeTestDataFolder, "CXPC_TEST_DATA", "MESG_COMEX_BROKER_DATA");
            await client.AssertPutDataCommon<Payloads.CX.CX_PRICES>("/cx", CXEntryDataFilePath, Account.GetWriteDef());

            var CXPCDataFiles = Utils.GetFiles(Path.Combine(ExchangeTestDataFolder, "CXPC_TEST_DATA", "MESG_COMEX_BROKER_PRICES"));
            CXPCDataFiles.Reverse(); // We want it in reverse because the CXPC data is in order of 1d, 3d, 7d, 30d, etc (start with longest time-frame, go to narrowest to ensure data isn't lost)
            Assert.That(CXPCDataFiles, Is.Not.Empty);

            request = new Request(client, HttpMethod.Put, "/cx/cxpc");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();
            request.AuthDef = Account.GetWriteDef();

            foreach (var CXPCDataFile in CXPCDataFiles)
            {
                var CXPCData = JsonSerializer.Deserialize<Payloads.CX.MESG_COMEX_BROKER_PRICES>(File.ReadAllText(CXPCDataFile));
                Assert.That(CXPCData, Is.Not.Null);
                Assert.That(CXPCData.PassesValidation(), Is.True);

                request.SetPayload(CXPCData);
                await request.GetNoResultResponseAsync();
                request.AssertOK();
            }

            const string ticker = "OVE.CI1";
            request = new Request(client, HttpMethod.Get, $"/cx/cxpc?ticker={ticker}");
            var response = await request.GetResponseAsync<List<CXPC>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response, Is.Not.Empty);
            Assert.That(response.First().Entries, Is.Not.Empty);
            Assert.That(response.First().Entries.Any(e => e.Interval == Constants.ValidCXPCIntervals.Last()), Is.True);

            const string interval_test_1 = "DAY_ONE";
            const string interval_test_2 = "HOUR_FOUR";
            request.EndPoint = $"/cx/cxpc?ticker={ticker}&interval={interval_test_1}&interval={interval_test_2}";
            response = await request.GetResponseAsync<List<CXPC>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response, Is.Not.Empty);
            Assert.That(response.SelectMany(cxpc => cxpc.Entries).All(e => e.Interval == interval_test_1 || e.Interval == interval_test_2), Is.True);
        }

        [Test]
        public async Task CXOSTests()
        {
            var Account = TestUserAccount.MakeAccount();
            var TestDataPath = Path.Combine(ExchangeTestDataFolder, "MESG_COMEX_TRADER_ORDERS");
            await client.AssertPutDataCommon<Payloads.CX.MESG_COMEX_TRADER_ORDER>("/cx/cxos", TestDataPath, Account.GetWriteDef());

            request = new Request(client, HttpMethod.Get, $"/cx/cxos?UserName={Account.UserName}");
            await request.AssertEnforceAuth();
            request.AuthDef = Account.GetReadDef();

            var response = await request.GetResponseAsync<RetrievalResponse<List<CXOS>>>();
            request.AssertOK();
            Assert.That(response, Is.Not.Null);
            Assert.That(response.UserNameToData, Is.Not.Empty);
            Assert.That(response.UserNameToData.ContainsKey(Account.UserName), Is.True);
            Assert.That(response.UserNameToData[Account.UserName].Errors, Is.Empty);
            Assert.That(response.UserNameToData[Account.UserName].Data, Is.Not.Empty);
        }
    }
}
