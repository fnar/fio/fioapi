﻿namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class MaterialTests : FIOAPIIntegrationTest
    {
        private static string MaterialTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Material", "MESG_WORLD_MATERIAL_DATA");

        private static string MaterialCategoryTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Material", "MESG_WORLD_MATERIAL_CATEGORIES");

        [Test, Order(0)]
        public async Task MaterialTest()
        {
            // No results initially
            request = new Request(client, HttpMethod.Get, "/material");
            var materials = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.That(materials, Is.Not.Null);
            Assert.That(materials, Is.Empty);

            var account = TestUserAccount.MakeAccount();
            await client.AssertPutDataCommon<Payloads.Material.MESG_WORLD_MATERIAL_DATA>("/material", MaterialTestDataFolder, account.GetWriteDef());

            // Make sure it works
            request = new Request(client, HttpMethod.Get, "/material");
            materials = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.That(materials, Is.Not.Null);
            Assert.That(materials, Is.Not.Empty);

            // Test ticker
            request.EndPoint = "/material?ticker=RAT&ticker=H2O";
            materials = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.That(materials, Is.Not.Null);
            Assert.That(materials, Has.Count.EqualTo(2));

            // Test combination
            request.EndPoint = "/material?ticker=RAT&name=water&id=b9640b0d66e7d0ca7e4d3132711c97fc&category=3f047ec3043bdd795fd7272d6be98799";
            materials = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.That(materials, Is.Not.Null);
            Assert.That(materials, Is.Not.Empty);
        }

        [Test, Order(0)]
        public async Task MaterialCategoryTest()
        {
            // No results initially
            request = new Request(client, HttpMethod.Get, "/material/categories");
            var categories = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.That(categories, Is.Not.Null);
            Assert.That(categories, Is.Empty);

            var account = TestUserAccount.MakeAccount();
            await client.AssertPutDataCommon<Payloads.Material.MESG_WORLD_MATERIAL_CATEGORIES>("/material/category", MaterialCategoryTestDataFolder, account.GetWriteDef());

            // Make sure it works
            request = new Request(client, HttpMethod.Get, "/material/categories");
            categories = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.That(categories, Is.Not.Null);
            Assert.That(categories, Is.Not.Empty);
        }

        [Test, Order(1)]
        public async Task EnsureSFAndFFMatchTest()
        {
            request = new Request(client, HttpMethod.Get, "material?ticker=SF&ticker=FF");
            var materials = await request.GetResponseAsync<List<DB.Model.Material>>();
            request.AssertOK();
            Assert.That(materials, Is.Not.Null);
            Assert.That(materials, Has.Count.EqualTo(2));

            var sf = materials.FirstOrDefault(m => m.Ticker == "SF");
            Assert.That(sf, Is.Not.Null);
            Assert.That(sf.Weight, Is.EqualTo(Constants.STLFuelWeight));
            Assert.That(sf.Volume, Is.EqualTo(Constants.STLFuelVolume));

            var ff = materials.FirstOrDefault(m => m.Ticker == "FF");
            Assert.That(ff, Is.Not.Null);
            Assert.That(ff.Weight, Is.EqualTo(Constants.FTLFuelWeight));
            Assert.That(ff.Volume, Is.EqualTo(Constants.FTLFuelVolume));
        }
    }
}
