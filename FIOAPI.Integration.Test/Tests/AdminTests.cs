﻿using FIOAPI.Payloads.Auth;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class AdminTests : FIOAPIIntegrationTest
    {
        [Test]
        public async Task IsAdmin()
        {
            var userAccount = TestUserAccount.MakeAccount();
            var adminAccount = TestUserAccount.MakeAdminAccount();

            // Not logged in
            request = new Request(client, HttpMethod.Get, "/admin");
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // User attempt
            request = new Request(client, HttpMethod.Get, "/admin", userAccount.GetWriteDef());
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Good
            request = new Request(client, HttpMethod.Get, "/admin", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }


        [Test]
        public async Task IsUser()
        {
            var userAccount = TestUserAccount.MakeAccount();
            var adminAccount = TestUserAccount.MakeAdminAccount();

            var userAccountName = userAccount.UserName.ToUpper(); // Upper should be fine

            // Not logged in
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{userAccountName}");
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // User attempt
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{userAccountName}", userAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // User account not found
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{userAccountName + "X"}", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertNoContent();

            // User account found
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{userAccountName}", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }

        [Test]
        public async Task CreateAccount()
        {
            var userAccount = TestUserAccount.MakeAccount();
            var adminAccount = TestUserAccount.MakeAdminAccount();

            var createPayload = new Payloads.Admin.CreateAccount()
            {
                UserName = "AB",
                Password = "CD",
                Admin = true
            };
            Assert.That(createPayload.PassesValidation(), Is.False);

            // Not logged in
            request = new Request(client, HttpMethod.Post, "/admin/createaccount");
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // Using admin read def
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", adminAccount.GetReadDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // User attempt
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", userAccount.GetWriteDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Too small of username/password
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", adminAccount.GetWriteDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // New account creation
            createPayload.UserName = "ABC";
            createPayload.Password = "DEF";
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Confirm new account exists
            request = new Request(client, HttpMethod.Get, "/admin/isuser/ABC", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Existing account reset/upgrade
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", adminAccount.GetWriteDef());
            createPayload.UserName = userAccount.UserName.ToUpper();
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Make sure new useraccount is now admin
            request = new Request(client, HttpMethod.Get, "/admin", userAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }

        [Test]
        public async Task CreateBotAccount()
        {
            var adminAccount = TestUserAccount.MakeAdminAccount();
            var createPayload = new Payloads.Admin.CreateAccount()
            {
                UserName = "ABC",
                Password = "DEF",
                Admin = true,
                Bot = true
            };

            request = new Request(client, HttpMethod.Post, "/admin/createaccount", adminAccount.GetWriteDef());

            // Can't have an admin bot
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Successfully create the bot
            createPayload.Admin = false;
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Ensure you can login w/ the bot
            var loginPayload = new Login()
            {
                UserName = "ABC - Bot",
                Password = "DEF"
            };
            request = new Request(client, HttpMethod.Post, "/auth/login");
            request.SetPayload(loginPayload);
            var response = await request.GetResponseAsync<LoginResponse>();
            request.AssertOK();
        }

        [Test]
        public async Task DeleteAccount()
        {
            var userAccount = TestUserAccount.MakeAccount();
            var adminAccount = TestUserAccount.MakeAdminAccount();
            var createPayload = new Payloads.Admin.CreateAccount()
            {
                UserName = "DeleteTest",
                Password = "DeleteTest",
                Admin = true
            };
            Assert.That(createPayload.PassesValidation(), Is.True);

            // Create the account
            request = new Request(client, HttpMethod.Post, "/admin/createaccount", adminAccount.GetWriteDef());
            request.SetPayload(createPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Not logged in
            request = new Request(client, HttpMethod.Delete, $"/admin/deleteaccount/{createPayload.UserName}");
            await request.GetNoResultResponseAsync();
            request.AssertUnauthorized();

            // User account attempt
            request = new Request(client, HttpMethod.Delete, $"/admin/deleteaccount/{createPayload.UserName}", userAccount.GetWriteDef());
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // Admin read def attempt
            request = new Request(client, HttpMethod.Delete, $"/admin/deleteaccount/{createPayload.UserName}", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertForbidden();

            // User deleted
            request = new Request(client, HttpMethod.Delete, $"/admin/deleteaccount/{createPayload.UserName}", adminAccount.GetWriteDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Ensure the user is no longer found
            request = new Request(client, HttpMethod.Get, $"/admin/isuser/{createPayload.UserName}", adminAccount.GetReadDef());
            await request.GetNoResultResponseAsync();
            request.AssertNoContent();
        }
    }
}
