﻿using FIOAPI.Payloads.Planet;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class PlanetTests : FIOAPIIntegrationTest
    {
        private static string PlanetTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Planet", "PATH_planets-_");
        private static string PlanetSitesTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Planet", "PATH_planets-_-sites");
        private static string PlanetCOGCTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Planet", "PATH_planets-_-cogc-_");
        private static string PlanetPopulationTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Planet", "PATH_populations-_");
        private static string PlanetPopulationProjectsTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Planet", "PATH_populations-_-projects-_");


        [Test, Order(0)]
        public async Task PutPlanets()
        {
            var Account = TestUserAccount.MakeAccount();
            await client.AssertPutDataCommon<PATH_PLANETS>("/planet", PlanetTestDataFolder, Account.GetWriteDef());
        }

        [Test, Order(0)]
        public async Task PutSitesData()
        {
            var Account = TestUserAccount.MakeAccount();
            await client.AssertPutDataCommon<PLANET_SITES>("/planet/sites", PlanetSitesTestDataFolder, Account.GetWriteDef());
        }

        [Test, Order(0)]
        public async Task PutDependentMapData()
        {
            var mt = new MapTests();
            await mt.PutMapData();
            await mt.PutStationAndPlanetData();

            var gt = new GlobalTests();
            await gt.PutStations();
        }

        [Test, Order(0)]
        public async Task PutDependentMapStationAndPlanetData()
        {
            var mt = new MapTests();
            await mt.PutStationAndPlanetData();
        }

        [Test, Order(0)]
        public async Task PutDependentGlobalData()
        {
            var gt = new GlobalTests();
            await gt.PutStations();
        }

        [Test, Order(1)]
        public async Task PutCOGCData()
        {
            var Account = TestUserAccount.MakeAccount();
            await client.AssertPutDataCommon<PLANET_COGC_HISTORY>("/planet/cogc", PlanetCOGCTestDataFolder, Account.GetWriteDef());
        }

        [Test, Order(2)]
        public async Task PutPopulationData()
        {
            var Account = TestUserAccount.MakeAccount();
            await client.AssertPutDataCommon<PLANET_POPULATION_REPORT>("/planet/population", PlanetPopulationTestDataFolder, Account.GetWriteDef());
        }

        [Test, Order(3)]
        public async Task PutPopulationProjects()
        {
            var Account = TestUserAccount.MakeAccount();
            await client.AssertPutDataCommon<PATH_POPULATION_PROJECTS>("/planet/populationproject", PlanetPopulationProjectsTestDataFolder, Account.GetWriteDef());
        }

        [Test, Order(4)]
        public async Task GetPlanet()
        {
            request = new Request(client, HttpMethod.Get, "/planet/Griffonstone");
            var result = await request.GetResponseAsync<DB.Model.Planet>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.WorkforceFees, Is.Empty);

            request = new Request(client, HttpMethod.Get, "/planet/Griffonstone?include_resources=false&include_workforce_fees=true");
            result = await request.GetResponseAsync<DB.Model.Planet>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Resources, Is.Empty);
            Assert.That(result.WorkforceFees, Is.Not.Empty);

            await Task.Delay(100);

            request = new Request(client, HttpMethod.Get, "/planet/Griffonstone?include_workforce_fees=true&include_cogc_programs=true");
            result = await request.GetResponseAsync<DB.Model.Planet>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.Resources, Is.Not.Empty);
            Assert.That(result.WorkforceFees, Is.Not.Empty);
            Assert.That(result.COGCPrograms, Is.Not.Empty);

            request = new Request(client, HttpMethod.Get, "/planet/Proxion?include_population_projects=true");
            result = await request.GetResponseAsync<DB.Model.Planet>();
            request.AssertOK();
            Assert.That(result, Is.Not.Null);
            Assert.That(result.PopulationProjects, Is.Not.Empty);
        }

        [Test, Order(4)]
        public async Task GetAllPlanets()
        {
            request = new Request(client, HttpMethod.Get, "/planet/all_planets");
            var results = await request.GetResponseAsync<List<DB.Model.Planet>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
            Assert.That(results[0].WorkforceFees, Is.Empty);

            request = new Request(client, HttpMethod.Get, "/planet/all_planets?include_workforce_fees=true");
            results = await request.GetResponseAsync<List<DB.Model.Planet>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
            Assert.That(results[0].WorkforceFees, Is.Not.Empty);
        }

        [Test, Order(4)]
        public async Task GetSiteCounts()
        {
            request = new Request(client, HttpMethod.Get, "/planet/sitecount");
            var results = await request.GetResponseAsync<List<PlanetSiteCount>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
            Assert.That(results, Has.Count.GreaterThan(1));
            Assert.That(results.First(), Has.Count.GreaterThan(1));
        }

        [Test, Order(4)]
        public async Task SearchPlanets()
        {
            PlanetSearch search = new()
            {
                IncludeRocky = true,
                IncludeGaseous = true,
                IncludeLowGravity = true,
                IncludeHighGravity = true,
                IncludeLowPressure = true,
                IncludeHighPressure = true,
                IncludeLowTemperature = true,
                IncludeHighTemperature = true,
                DistanceChecks = { "Benten", "Antares I" }
            };

            request = new Request(client, HttpMethod.Post, "/planet/search");
            request.SetPayload(search);
            var results = await request.GetResponseAsync<List<PlanetSearchResult>>();
            request.AssertOK();
            Assert.That(results, Is.Not.Null);
            Assert.That(results, Is.Not.Empty);
            Assert.That(results.All(r => r.DistanceResults.Count == search.DistanceChecks.Count), Is.True);
        }

        [Test, Order(4)]
        public async Task GetPlanetSites()
        {
            const string GriffonstonePlanetId = "7EF6CAA148AA38C0E3966FDF47EE1A6B";
            const string GriffonstoneName = "Griffonstone";
            const string GriffonstoneNaturalId = "LS-300C";

            request = new Request(client, HttpMethod.Get, "/planet/sites/AB0123ACBASd1123");
            var planetSites = await request.GetResponseAsync<List<PlanetSite>>();
            request.AssertBadRequest();
            Assert.That(planetSites, Is.Null);

            request.EndPoint = $"/planet/sites/{GriffonstonePlanetId}";
            planetSites = await request.GetResponseAsync<List<PlanetSite>>();
            request.AssertOK();
            Assert.That(planetSites, Is.Not.Null);
            Assert.That(planetSites, Is.Not.Empty);

            request.EndPoint = $"/planet/sites/{GriffonstoneName}";
            planetSites = await request.GetResponseAsync<List<PlanetSite>>();
            request.AssertOK();
            Assert.That(planetSites, Is.Not.Null);
            Assert.That(planetSites, Is.Not.Empty);

            request.EndPoint = $"/planet/sites/{GriffonstoneNaturalId}";
            planetSites = await request.GetResponseAsync<List<PlanetSite>>();
            request.AssertOK();
            Assert.That(planetSites, Is.Not.Null);
            Assert.That(planetSites, Is.Not.Empty);
        }

        [Test, Order(5)]
        public async Task TestSearchAfterRefresh()
        {
            var mt = new MapTests();
            await mt.PutMapData();

            PlanetSearch search = new()
            {
                IncludeRocky = true,
                IncludeGaseous = true,
                IncludeLowGravity = true,
                IncludeHighGravity = true,
                IncludeLowPressure = true,
                IncludeHighPressure = true,
                IncludeLowTemperature = true,
                IncludeHighTemperature = true,
                DistanceChecks = { "BEN", "ANT" }
            };

            request = new Request(client, HttpMethod.Post, "/planet/search");
            request.SetPayload(search);
            var res = await request.GetResponseAsync<List<PlanetSearchResult>>();
            request.AssertOK();
            Assert.That(res, Is.Not.Null);
            Assert.That(res, Is.Not.Empty);
        }
    }
}
