﻿namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class MapTests : FIOAPIIntegrationTest
    {
        private static string MapTestDataFolder => Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Map");

        [Test, Order(0)]
        public async Task PutMapData()
        {
            var Account = TestUserAccount.MakeAccount();
            var AdminAccount = TestUserAccount.MakeAdminAccount();

            var TestDataPath = Path.Combine(MapTestDataFolder, "MESG_WORLD_SECTORS");
            await client.AssertPutDataCommon<Payloads.Map.MAP_WORLD_SECTORS>("/map/sectors", TestDataPath, AdminAccount.GetWriteDef(), RequireAdmin: true);

            TestDataPath = Path.Combine(MapTestDataFolder, "MESG_SYSTEM_STARS_DATA");
            await client.AssertPutDataCommon<Payloads.Map.SYSTEM_STARS>("/map/systemstars", TestDataPath, AdminAccount.GetWriteDef(), RequireAdmin: true);

            TestDataPath = Path.Combine(MapTestDataFolder, "PATH_systems-_");
            await client.AssertPutDataCommon<Payloads.Map.MAP_SYSTEM>("/map/system", TestDataPath, AdminAccount.GetWriteDef(), RequireAdmin: false);
        }

        // Further down tests require station and planet data
        [Test, Order(0)]
        public async Task PutStationAndPlanetData()
        {
            // Add station data
            var AdminAccount = TestUserAccount.MakeAdminAccount();
            var GlobalTestFolder = Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Global");
            var StationsDataFolder = Path.Combine(GlobalTestFolder, "PATH_stations-_");

            await client.AssertPutDataCommon<Payloads.Global.PATH_STATION>("/global/station", StationsDataFolder, AdminAccount.GetWriteDef(), RequireAdmin: true);

            // Add planet data
            var PlanetTestDataFolder = Path.Combine(Utils.GetCSProjDirectory(), "SamplePayloads", "Planet", "PATH_planets-_");
            await client.AssertPutDataCommon<Payloads.Planet.PATH_PLANETS>("/planet", PlanetTestDataFolder, AdminAccount.GetWriteDef());
        }

        [Test, Order(1)]
        public async Task GetSectors()
        {
            request = new Request(client, HttpMethod.Get, "/map/sectors");
            var sectors = await request.GetResponseAsync<List<DB.Model.Sector>>();
            request.AssertOK();
            Assert.That(sectors, Is.Not.Null);
            Assert.That(sectors, Is.Not.Empty);
            Assert.That(sectors[0].SubSectors, Is.Empty);

            request.EndPoint = "/map/sectors?include_subsectors=true";
            sectors = await request.GetResponseAsync<List<DB.Model.Sector>>();
            request.AssertOK();
            Assert.That(sectors, Is.Not.Null);
            Assert.That(sectors, Is.Not.Empty);
            sectors.ForEach(s =>
            {
                Assert.That(s.SubSectors, Is.Not.Empty);
            });

            request.EndPoint = "/map/sectors/AJ";
            sectors = await request.GetResponseAsync<List<DB.Model.Sector>>();
            request.AssertOK();
            Assert.That(sectors, Is.Not.Null);
            Assert.That(sectors.Count, Is.EqualTo(1));
        }

        [Test, Order(1)]
        public async Task GetSystems()
        {
            request = new Request(client, HttpMethod.Get, "/map/systems");
            var systems = await request.GetResponseAsync<List<DB.Model.System>>();
            request.AssertOK();
            Assert.That(systems, Is.Not.Null);
            Assert.That(systems, Is.Not.Empty);
            systems.ForEach(s =>
            {
                Assert.That(s.Connections, Is.Not.Empty);
            });

            request.EndPoint = "/map/systems/OF";
            systems = await request.GetResponseAsync<List<DB.Model.System>>();
            request.AssertOK();
            Assert.That(systems, Is.Not.Null);
            Assert.That(systems, Is.Not.Empty);
            Assert.That(systems.Count > 1, Is.True);
        }

        [Test, Order(1)]
        public async Task GetStars()
        {
            request = new Request(client, HttpMethod.Get, "/map/stars");
            var systems = await request.GetResponseAsync<List<DB.Model.Star>>();
            request.AssertOK();
            Assert.That(systems, Is.Not.Null);
            Assert.That(systems, Is.Not.Empty);

            request.EndPoint = "/map/stars/OF";
            systems = await request.GetResponseAsync<List<DB.Model.Star>>();
            request.AssertOK();
            Assert.That(systems, Is.Not.Null);
            Assert.That(systems, Is.Not.Empty);
            Assert.That(systems.Count > 1, Is.True);
        }

        [Test, Order(1)]
        public async Task GetJumpRoutes()
        {
            // Test system name to system name
            request = new Request(client, HttpMethod.Get, "/map/jumproute/Benten/Moria");
            var routeResult = await request.GetResponseAsync<List<JumpCacheRouteJump>>();
            request.AssertOK();
            Assert.That(routeResult, Is.Not.Null);
            Assert.That(routeResult, Is.Not.Empty);

            // Test system naturalid to system naturalid
            const int JumpDistanceBetweenAntares3AndHortus = 8;
            request = new Request(client, HttpMethod.Get, "map/jumpcount/ZV-194/VH-331");
            var jumpCountResult = await request.GetResponseAsync<int>();
            request.AssertOK();
            Assert.That(jumpCountResult, Is.EqualTo(JumpDistanceBetweenAntares3AndHortus));

            // Test system name to system name
            request.EndPoint = "/map/jumpcount/Hortus/Antares III";
            jumpCountResult = await request.GetResponseAsync<int>();
            request.AssertOK();
            Assert.That(jumpCountResult, Is.EqualTo(JumpDistanceBetweenAntares3AndHortus));

            // Test system id to system id
            const string HortusSystemId = "F2F57766EBACA9D69EFAE41CCF4D8853";
            const string AntaresIIISystemId = "AC35DD2A980E53FEDD1A4FE87A909972";
            request.EndPoint = $"/map/jumpcount/{HortusSystemId}/{AntaresIIISystemId}";
            Assert.That(jumpCountResult, Is.EqualTo(JumpDistanceBetweenAntares3AndHortus));

            // Test station naturalid to station naturalid
            const int JumpDistanceBetweenBentenAndAntares = 9;
            request.EndPoint = "/map/jumpcount/ben/ant";
            jumpCountResult = await request.GetResponseAsync<int>();
            request.AssertOK();
            Assert.That(jumpCountResult, Is.EqualTo(JumpDistanceBetweenBentenAndAntares));

            // Test station name to station name
            request.EndPoint = "/map/jumpcount/benten station/antares station";
            jumpCountResult = await request.GetResponseAsync<int>();
            request.AssertOK();
            Assert.That(jumpCountResult, Is.EqualTo(JumpDistanceBetweenBentenAndAntares));

            // Test StationId to StationId
            const string ANTStationId = "1DECA369A92788B8079E7AC245BE66F7";
            const string BENStationId = "3CB9C89BFDF03513A91023E07C90DC08";
            request.EndPoint = $"/map/jumpcount/{ANTStationId}/{BENStationId}";
            jumpCountResult = await request.GetResponseAsync<int>();
            request.AssertOK();
            Assert.That(jumpCountResult, Is.EqualTo(JumpDistanceBetweenBentenAndAntares));

            // Test planet naturalid to planet naturalid
            request.EndPoint = "/map/jumpcount/vh-331a/zv-194a";
            jumpCountResult = await request.GetResponseAsync<int>();
            request.AssertOK();
            Assert.That(jumpCountResult, Is.EqualTo(JumpDistanceBetweenAntares3AndHortus));

            // test planet name to planet name
            request.EndPoint = "/map/jumpcount/nike/promitor";
            jumpCountResult = await request.GetResponseAsync<int>();
            request.AssertOK();
            Assert.That(jumpCountResult, Is.EqualTo(JumpDistanceBetweenAntares3AndHortus));

            // test planet id to planet id
            const string NikePlanetId = "B11D2E328F4BE01EA51088A42C783426";
            const string PromitorPlanetId = "99EB57FEFD18101662BF834CBEEA34ED";
            request.EndPoint = $"/map/jumpcount/{NikePlanetId}/{PromitorPlanetId}";
            request.AssertOK();
            Assert.That(jumpCountResult, Is.EqualTo(JumpDistanceBetweenAntares3AndHortus));
        }
    }
}
