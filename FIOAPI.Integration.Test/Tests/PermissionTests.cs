﻿using FIOAPI.Payloads.Permission;

namespace FIOAPI.Integration.Test.Tests
{
    [Parallelizable]
    public class PermissionTests : FIOAPIIntegrationTest
    {
        [Test]
        public async Task Grant()
        {
            var accounts = TestUserAccount.MakeAccounts(2);

            var grantPayload = new Grant()
            {
                UserName = "BD",
            };
            Assert.False(grantPayload.PassesValidation());

            // Not logged in & write-apikey
            request = new Request(client, HttpMethod.Put, "/permission/grant");
            await request.AssertEnforceAuth();
            await request.AssertEnforceWrite();

            // Bad payload (username too short)
            request = new Request(client, HttpMethod.Put, "/permission/grant", accounts[0].GetWriteDef());
            request.SetPayload(grantPayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Grantee and Grantor at the same, bad request
            grantPayload.UserName = accounts[0].UserName;
            request.SetPayload(grantPayload);
            await request.GetNoResultResponseAsync();
            request.AssertBadRequest();

            // Grantee not found
            grantPayload.UserName = "UserNotFound";
            request.SetPayload(grantPayload);
            await request.GetNoResultResponseAsync();
            request.AssertNotFound();

            // Success
            grantPayload.UserName = accounts[1].UserName;
            request.SetPayload(grantPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Try again, with slightly modified permissions
            grantPayload.Permissions.CompanyPermissions.LiquidCurrency = false;
            request.SetPayload(grantPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }

        [Test]
        public async Task GetPermissions()
        {
            var accounts = TestUserAccount.MakeAccounts(2);

            // Not logged in
            request = new Request(client, HttpMethod.Get, "/permission");
            await request.AssertEnforceAuth();

            // Empty list
            request = new Request(client, HttpMethod.Get, "/permission", accounts[0].GetReadDef());
            var perms = await request.GetResponseAsync<List<PermissionResponse>>();
            request.AssertOK();
            Assert.That(perms, Is.Not.Null);
            Assert.That(perms, Is.Empty);

            var grantPayload = new Grant()
            {
                UserName = accounts[1].UserName,
            };
            Assert.That(grantPayload.PassesValidation(), Is.True);

            // Create the permission
            request = new Request(client, HttpMethod.Put, "/permission/grant", accounts[0].GetWriteDef());
            request.SetPayload(grantPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Create another for all users
            grantPayload.UserName = "*";
            request.SetPayload(grantPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Grant the same guy again, to ensure we don't get a third
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Should have two entries
            request = new Request(client, HttpMethod.Get, "/permission", accounts[0].GetReadDef());
            perms = await request.GetResponseAsync<List<PermissionResponse>>();
            request.AssertOK();
            Assert.That(perms, Is.Not.Null);
            Assert.That(perms.Count, Is.EqualTo(2));

            perms.ForEach(p => Assert.That(p.PassesValidation(), Is.True));

            // Revoke the `*` permission
            request = new Request(client, HttpMethod.Delete, "/permission/revoke/*", accounts[0].GetWriteDef());
            await request.GetNoResultResponseAsync();
            request.AssertOK();
        }

        [Test]
        public async Task GetGranted()
        {
            var accounts = TestUserAccount.MakeAccounts(2);

            // No auth
            request = new Request(client, HttpMethod.Get, "/permission/granted");
            await request.AssertEnforceAuth();

            // No entries
            request = new Request(client, HttpMethod.Get, "/permission/granted", accounts[1].GetReadDef());
            var perms = await request.GetResponseAsync<List<PermissionResponse>>();
            request.AssertOK();
            Assert.That(perms, Is.Not.Null);
            Assert.That(perms, Is.Empty);

            var grantPayload = new Grant()
            {
                UserName = accounts[1].UserName,
            };
            Assert.That(grantPayload.PassesValidation(), Is.True);

            // Create the permission
            request = new Request(client, HttpMethod.Put, "/permission/grant", accounts[0].GetWriteDef());
            request.SetPayload(grantPayload);
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // Re-run the grant to make sure no duplicates show
            await request.GetNoResultResponseAsync();
            request.AssertOK();

            // One entry
            request = new Request(client, HttpMethod.Get, "/permission/granted", accounts[1].GetReadDef());
            perms = await request.GetResponseAsync<List<PermissionResponse>>();
            request.AssertOK();
            Assert.That(perms, Is.Not.Null);
            Assert.That(perms.Count, Is.EqualTo(1));
            Assert.That(perms.First().PassesValidation(), Is.True);
        }
    }
}
