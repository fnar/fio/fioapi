﻿namespace FIOAPI.Internal.Test
{
    [Parallelizable]
    public class ControllerTests
    {
        [Test]
        public void VerifyGetsHaveAnonymousOrPolicy()
        {
            var AllControllers = ReflectionHelpers.AllControllerClassTypes;

            var Errors = new List<string>();
            foreach (var Controller in AllControllers)
            {
                var Methods = Controller.GetMethods();
                foreach (var Method in Methods)
                {
                    var MethodAttrs = Method.GetCustomAttributes(true);
                    bool HasHttpGetAttribute = MethodAttrs.Any(ma => ma is HttpGetAttribute);
                    bool HasAuthorizeAttribute = MethodAttrs.Any(ma => ma is AuthorizeAttribute);
                    bool HasAllowAnonymousAttribute = MethodAttrs.Any(ma => ma is AllowAnonymousAttribute);

                    if (HasHttpGetAttribute)
                    {
                        if (!HasAuthorizeAttribute && !HasAllowAnonymousAttribute)
                        {
                            Errors.Add($"{Controller.Name}.{Method.Name} has HttpGet but doesn't specify Authorize or AllowAnonymous");
                        }
                        else if (HasAllowAnonymousAttribute && HasAuthorizeAttribute)
                        {
                            Errors.Add($"{Controller.Name}.{Method.Name} is HttpGet and has both Authorize and AllowAnonymous, which isn't allowed.");
                        }
                    }

                }
            }

            if (Errors.Count > 0)
            {
                var ErrorMsg = string.Join(Environment.NewLine, Errors);
                Assert.Fail(ErrorMsg);
            }
        }

        [Test]
        public void VerifyAllHaveControllerClassAttributesAreCorrect()
        {
            List<string> Errors = new();
            var AllControllers = ReflectionHelpers.AllControllerClassTypes;
            foreach (var Controller in AllControllers)
            {
                var routeAttr = Controller.GetCustomAttribute<RouteAttribute>();
                var apiControllerAttr = Controller.GetCustomAttribute<ApiControllerAttribute>();

                if (routeAttr == null)
                {
                    Errors.Add($"Controller {Controller.Name} is missing the [Route] attribute");
                }

                if (apiControllerAttr == null)
                {
                    Errors.Add($"Controller {Controller.Name} is missing the [ApiController] attribute");
                }
            }

            if (Errors.Count > 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, Errors));
            }
        }
    }
}
