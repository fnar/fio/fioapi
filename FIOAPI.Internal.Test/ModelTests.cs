﻿namespace FIOAPI.Internal.Test
{
    [Parallelizable]
    public class ModelTests
    {
        private const string UserNameSubmittedName = "UserNameSubmitted";
        private const int UserNameSubmittedMinLength = 3;
        private const int UserNameSubmittedMaxLength = 32;

        private const string TimestampName = "Timestamp";

        private readonly Dictionary<string, Tuple<int, int>> StringAttributeToMinMax = new()
        {
            { nameof(APEXIDAttribute), new Tuple<int, int>(Constants.APEXIDLength, Constants.APEXIDLength) },
            { nameof(BuildingCategoryAttribute), new Tuple<int, int>(Constants.BuildingCategoryLengthMin, Constants.BuildingCategoryLengthMax) },
            { nameof(CurrencyCodeAttribute), new Tuple<int, int>(Constants.CurrencyLengthMin, Constants.CurrencyLengthMax) },
            { nameof(FXTickerAttribute), new Tuple<int, int>(7, 7) },
            { nameof(NaturalIDAttribute), new Tuple<int, int>(Constants.NaturalIDLengthMin, Constants.NaturalIDLengthMax) },
            { nameof(ResourceTypeAttribute), new Tuple<int, int>(Constants.PlanetResourceTypeLengthMin, Constants.PlanetResourceTypeLengthMax) },
            { nameof(TickerAttribute), new Tuple<int, int>(Constants.TickerLengthMin, Constants.TickerLengthMax) },
            { nameof(WorkforceLevelAttribute), new Tuple<int, int>(Constants.WorkforceLevelLengthMin, Constants.WorkforceLevelLengthMax) }
        };

        [Test]
        public void VerifyUserNameSubmittedAndTimestampHaveCorrectAttribute()
        {
            var AllModels = Utils.ReflectionHelpers.AllModelClassTypes;

            List<string> FailureMessages = new();
            foreach (var Model in AllModels)
            {
                var ModelPropInfos = Model
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .ToList();

                var UserNameSubmittedPropInfo = ModelPropInfos
                    .Where(pi => pi.PropertyType == typeof(string))
                    .Where(pi => pi.Name == UserNameSubmittedName)
                    .FirstOrDefault();
                if (UserNameSubmittedPropInfo != null)
                {
                    // Verify correct attributes
                    var attrs = UserNameSubmittedPropInfo.GetCustomAttributes(true);
                    if (!attrs.Any(a => a is LowercaseAttribute))
                    {
                        FailureMessages.Add($"{Model.Name}.{UserNameSubmittedName} is missing the [Lowercase] attribute.");
                    }

                    if (attrs.FirstOrDefault(a => a is StringLengthAttribute) is StringLengthAttribute StringLengthAttr)
                    {
                        if (StringLengthAttr.MinimumLength != UserNameSubmittedMinLength)
                        {
                            FailureMessages.Add($"{Model.Name}.{UserNameSubmittedName}'s [StringLength] attribute should have a minimum length of {UserNameSubmittedMinLength}");
                        }

                        if (StringLengthAttr.MaximumLength != UserNameSubmittedMaxLength)
                        {
                            FailureMessages.Add($"{Model.Name}.{UserNameSubmittedName}'s [StringLength] attribute should have a maximum length of {UserNameSubmittedMaxLength}");
                        }
                    }

                }

                var TimestampPropInfo = ModelPropInfos
                    .Where(pi => pi.Name == TimestampName)
                    .Where(pi => pi.PropertyType == typeof(DateTime))
                    .FirstOrDefault();
                if (TimestampPropInfo != null)
                {
                    var attrs = TimestampPropInfo.GetCustomAttributes(true);
                    if (!attrs.Any(a => a is ValidTimestampAttribute))
                    {
                        FailureMessages.Add($"{Model.Name}.{TimestampName} is missing the [ValidTimestamp] attribute");
                    }
                }
            }

            if (FailureMessages.Count != 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, FailureMessages));
            }
        }

        [Test]
        public void VerifyAllModelsAreIValidation()
        {
            var FailureMessages = Utils.ReflectionHelpers.AllModelClassTypes
                .Where(t => !t.IsSubclassOf(typeof(IValidation)))
                .Where(t => !t.GetCustomAttributes(true).Any(a => a is NotMappedAttribute))
                .Select(t => $"{t.Name} does not inherit from IValidation")
                .ToList();

            if (FailureMessages.Count > 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, FailureMessages));
            }
        }

        [Test]
        public void VerifyAllModelStringsHaveStringLength()
        {
            var AllModels = Utils.ReflectionHelpers.AllModelClassTypes
                .Where(t => t.IsSubclassOf(typeof(IValidation)))
                .ToList();
            Assert.That(AllModels, Is.Not.Null);
            Assert.That(AllModels, Is.Not.Empty);

            List<string> FailureMessages = new();
            foreach (var Model in AllModels)
            {
                var ModelPropInfos = Model.GetProperties(BindingFlags.Instance | BindingFlags.Public);
                foreach (var ModelPropInfo in ModelPropInfos)
                {
                    bool IsStringType = ModelPropInfo.PropertyType == typeof(string);
                    if (IsStringType)
                    {
                        var attrs = ModelPropInfo.GetCustomAttributes(true);
                        bool IsNotMappedOrNoValidate = attrs.Any(attr => attr is NotMappedAttribute || attr is NoValidateAttribute);
                        if (IsNotMappedOrNoValidate)
                        {
                            continue;
                        }

                        bool HasGetterAndSetter = ModelPropInfo.GetGetMethod() != null && ModelPropInfo.GetSetMethod() != null;
                        if (HasGetterAndSetter)
                        {
                            // Make sure we have a string length attribute
                            bool HasStringLengthAttribute = attrs.Any(attr => attr is StringLengthAttribute);
                            if (!HasStringLengthAttribute)
                            {
                                FailureMessages.Add($"{Model.Name}.{ModelPropInfo.Name} does not have a StringLength attribute");
                            }
                        }
                    }
                }
            }

            if (FailureMessages.Count != 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, FailureMessages));
            }
        }

        [Test]
        public void VerifyAllStringAttributesHaveProperStringLengths()
        {
            var AllModels = Utils.ReflectionHelpers.AllModelClassTypes
                .Where(t => t.IsSubclassOf(typeof(IValidation)))
                .ToList();
            Assert.That(AllModels, Is.Not.Null & Is.Not.Empty);

            List<string> FailureMessages = new();
            foreach (var Model in AllModels)
            {
                var StringPropInfos = Model.GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(pi => pi.PropertyType == typeof(string))
                    .ToList();
                foreach (var StringPropInfo in StringPropInfos)
                {
                    var attrs = StringPropInfo.GetCustomAttributes(true);
                    bool IsNotMappedOrNoValidate = attrs.Any(attr => attr is NotMappedAttribute || attr is NoValidateAttribute || attr is CustomStringLengthAttribute);
                    if (IsNotMappedOrNoValidate)
                    {
                        continue;
                    }

                    bool HasGetterAndSetter = StringPropInfo.GetGetMethod() != null && StringPropInfo.GetSetMethod() != null;
                    if (HasGetterAndSetter)
                    {
                        foreach (var StringAttrEntry in StringAttributeToMinMax)
                        {
                            string AttrTypeName = StringAttrEntry.Key;
                            int Min = StringAttrEntry.Value.Item1;
                            int Max = StringAttrEntry.Value.Item2;

                            if (attrs.FirstOrDefault(a => a is StringLengthAttribute) is StringLengthAttribute strLenAttr && attrs.Any(a => a.GetType().Name == AttrTypeName))
                            {
                                if (strLenAttr.MinimumLength != Min || strLenAttr.MaximumLength != Max)
                                {
                                    FailureMessages.Add($"{Model.Name}.{StringPropInfo.Name} is has attribute {AttrTypeName} but the StringLength attribute Min/Max is '{strLenAttr.MinimumLength}, {strLenAttr.MaximumLength}' but should be '{Min}, {Max}'");
                                }
                            }
                        }
                    }
                }
            }
            if (FailureMessages.Count != 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, FailureMessages));
            }
        }

        [Test]
        public void VerifyModelListsContainIValidationObjectsAndAreVirtualAndInitialized()
        {
            var AllModels = Utils.ReflectionHelpers.AllModelClassTypes
                .Where(t => t.IsSubclassOf(typeof(IValidation)))
                .ToList();
            Assert.That(AllModels, Is.Not.Null & Is.Not.Empty);

            foreach (var Model in AllModels)
            {
                var ModelPropInfos = Model.GetProperties();
                foreach (var ModelPropInfo in ModelPropInfos)
                {
                    var PropertyType = ModelPropInfo.PropertyType;
                    if (PropertyType.IsGenericType && PropertyType.GetGenericTypeDefinition() == typeof(List<>))
                    {
                        var Attrs = ModelPropInfo.GetCustomAttributes(true);
                        bool IsMapped = !Attrs.Any(a => a is NotMappedAttribute);
                        if (IsMapped)
                        {
                            var ElementType = PropertyType.GetGenericArguments()[0]!;
                            if (!ElementType.IsSubclassOf(typeof(IValidation)))
                            {
                                Assert.Fail($"{Model.Name}.{ModelPropInfo.Name} isn't a List of IValidation objects");
                            }

                            if (!ModelPropInfo.GetMethod!.IsVirtual)
                            {
                                Assert.Fail($"{Model.Name}.{ModelPropInfo.Name} isn't virtual");
                            }

                            var TestObj = Activator.CreateInstance(Model);
                            object? value = ModelPropInfo.GetValue(TestObj);
                            if (value == null)
                            {
                                Assert.Fail($"{Model.Name}.{ModelPropInfo.Name} is initialized to null, when it should be a new list");
                            }
                        }
                    }
                }
            }
        }
    }
}
