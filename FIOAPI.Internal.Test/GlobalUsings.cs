global using System.Collections;
global using System.ComponentModel.DataAnnotations.Schema;
global using System.ComponentModel.DataAnnotations;
global using System.Reflection;

global using FIOAPI.Attributes;
global using FIOAPI.Caches;
global using FIOAPI.DB.Model;
global using FIOAPI.Extensions;
global using FIOAPI.Utils;

global using CsvHelper.Configuration.Attributes;

global using Microsoft.AspNetCore.Authorization;
global using Microsoft.AspNetCore.Mvc;

global using NUnit.Framework;

global using RangeAttribute = System.ComponentModel.DataAnnotations.RangeAttribute;