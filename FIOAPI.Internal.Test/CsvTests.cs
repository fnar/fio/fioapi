﻿namespace FIOAPI.Internal.Test
{
    [Parallelizable]
    public class CsvTests
    {
        [Test]
        public void EnsureAllCsvClassesInheritICSVObject()
        {
            var FailureMessages = ReflectionHelpers.AllCsvClassTypes
                .Where(t => !t.GetInterfaces().Contains(typeof(ICsvObject)))
                .Select(t => $"{t.Name} does not inherit from {nameof(ICsvObject)}")
                .ToList();

            if (FailureMessages.Count > 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, FailureMessages));
            }
        }

        [Test]
        public void EnsureAllCsvClassPropertiesHaveIndexAndIndexIsValid()
        {
            List<string> FailureMessages = new();

            foreach (var CsvType in ReflectionHelpers.AllCsvClassTypes)
            {
                if (CsvType.GetFields().Any())
                {
                    FailureMessages.Add($"{CsvType.Name} has fields when it should only have properties.");
                }

                var CsvPropInfos = CsvType
                    .GetProperties(BindingFlags.Public | BindingFlags.Instance)
                    .ToList();

                List<int> ValidIndices = Enumerable
                    .Range(0, CsvPropInfos.Count)
                    .ToList();

                foreach (var CsvPropInfo in CsvPropInfos)
                {
                    if (CsvPropInfo.GetGetMethod() == null || CsvPropInfo.GetSetMethod() == null)
                    {
                        FailureMessages.Add($"{CsvType.Name}.{CsvPropInfo.Name} must have a getter and setter.");
                    }

                    var indexAttr = CsvPropInfo.GetCustomAttributes(true).FirstOrDefault(a => a is IndexAttribute) as IndexAttribute;
                    if (indexAttr == null)
                    {
                        FailureMessages.Add($"{CsvType.Name}.{CsvPropInfo.Name} is missing the CsvHelper Index Attribute.");
                    }
                    else
                    {
                        if (!ValidIndices.Contains(indexAttr.Index))
                        {
                            FailureMessages.Add($"{CsvType.Name}.{CsvPropInfo.Name} is using an invalid index of {indexAttr.Index} (out of range or duplicate).");

                        }
                        else
                        {
                            ValidIndices.Remove(indexAttr.Index);
                        }
                    }
                }

                if (ValidIndices.Count > 0)
                {
                    var missingIndices = string.Join(",", ValidIndices);
                    FailureMessages.Add($"{CsvType.Name} is missing the following indices: {missingIndices}");
                }
            }

            if (FailureMessages.Count > 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, FailureMessages));
            }
        }
    }
}
