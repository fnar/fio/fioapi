﻿using FIOAPI.DB.Model;

namespace FIOAPI.Internal.Test
{
    [Parallelizable]
    public class ExtensionTests
    {
        [Test]
        public void VerifySplitExtensions()
        {
            var res = "a_b$c@d a_b$c@d a_b$c@d a_b$c@d".SplitOnMultiple(" ", "_", "$", "@");
            Assert.That(res.Count == 4, Is.True);
            Assert.That(res.All(r => r[0] == "a" && r[1] == "b" && r[2] == "c" && r[3] == "d"), Is.True);

            var res2 = "a,b,c,d".SplitOn(",");
            Assert.That(res2.Count == 4, Is.True);
            Assert.That(res2.All(r => r.Length == 1), Is.True);
        }

        public class HashCodeTestA : IValidation
        {
            public HashCodeTestA()
            {
                x = new Random().Next();
            }

            public int x { get; set; } = 1;
            public int y { get; set; } = 2;
            public int? z { get; set; } = 3;
            public int? w { get; set; } = null;

            public override string HashCodeKey => x.ToString();
        }

        public class HashCodeTestB : IValidation
        {
            public HashCodeTestA a { get; set; } = new();
            public HashCodeTestA b { get; set; } = new();
            public HashCodeTestA? c { get; set; } = null;

            public List<HashCodeTestA> list { get; set; } = new()
            {
                new HashCodeTestA(),
                new HashCodeTestA()
            };

            public HashCodeTestA[] arr { get; set; } = new HashCodeTestA[5];

            public List<string> strList { get; set; } = new()
            {
                "bar", "omg", "wtf"
            };

            public string[] strArr { get; set; } = new string[2]
            {
                "a", "b"
            };

            public string str { get; set; } = "geez";

            public override string HashCodeKey => str;
        }

        public class HashCodeTestC : IValidation
        {
            [FIOHashIgnore]
            public int dummy { get; set; } = 42;

            public override string HashCodeKey => dummy.ToString();
        }

        public class HashCodeTestD : IValidation
        {
            public int a { get; set; } = 42;
            public int x { get; set; } = 42;

            public override string HashCodeKey => a.ToString();
            public override int HashCode => new HashCode().ToHashCode();
        }

        [Test]
        public void TestHashCode()
        {
            int EmptyHashCode = new HashCode().ToHashCode();

            HashCodeTestA a = new();
            Assert.That(a.HashCode, Is.Not.EqualTo(EmptyHashCode));

            HashCodeTestB b = new();
            Assert.That(b.HashCode, Is.Not.EqualTo(EmptyHashCode));

            HashCodeTestC c = new();
            Assert.That(c.HashCode, Is.EqualTo(new HashCode().ToHashCode()));

            HashCodeTestD d = new();
            Assert.That(d.HashCode, Is.EqualTo(EmptyHashCode));
        }
    }
}
