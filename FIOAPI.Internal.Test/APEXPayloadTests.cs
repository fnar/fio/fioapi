﻿using System.Text.Json.Serialization;

#if false
[Parallelizable]
namespace FIOAPI.Internal.Test
{
    public class APEXPayloadTests
    {
        private static List<Type> ControllerExclusions = new()
        {
            typeof(Controllers.DataController),
            typeof(Controllers.AuthController),
            typeof(Controllers.PermissionController),
#if DEBUG
            typeof(Controllers.DebugController),
#endif
        };

        [Test]
        public void EnsureAllFromBodyPUTObjectsAreInheritedFromIAPEXPayload()
        {
            List<string> Failures = new();

            var ControllerClassTypes = ReflectionHelpers.AllControllerClassTypes
                .Where(t => !ControllerExclusions.Contains(t));
            foreach (var ControllerClassType in ControllerClassTypes)
            {
                var routeAttr = ControllerClassType.GetCustomAttribute<RouteAttribute>();
                if (routeAttr == null)
                {
                    // This is addressed in another test
                    continue;
                }

                foreach (var ControllerMethod in ControllerClassType.GetMethods())
                {
                    var httpPutAttr = ControllerMethod.GetCustomAttribute<HttpPutAttribute>();
                    if (httpPutAttr == null)
                    {
                        continue;
                    }

                    var fromBodyParam = ControllerMethod.GetParameters()
                        .Where(p => p.GetCustomAttribute<FromBodyAttribute>() != null)
                        .FirstOrDefault();
                    if (fromBodyParam == null)
                    {
                        continue;
                    }

                    var className = ControllerClassType.Name;
                    var methodName = ControllerMethod.Name;
                    var httpPutEndpoint = $"{routeAttr.Template}/{httpPutAttr.Template}";
                    var fromBodyType = fromBodyParam.ParameterType;

                    if (!typeof(IAPEXPayload).IsAssignableFrom(fromBodyType))
                    {
                        Failures.Add($"{className}.{methodName} ({httpPutEndpoint}) has [FromBody] parameter {fromBodyType} that does not inherit from {typeof(IAPEXPayload).Name}.");
                    }
                }
            }

            if (Failures.Count > 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, Failures));
            }
        }

        class WalkEntry
        {
            public Type classType { get; set; }
            public PropertyInfo propertyInfo { get; set; }

            public WalkEntry(Type classType, PropertyInfo propertyInfo)
            {
                this.classType = classType;
                this.propertyInfo = propertyInfo;
            }
        }

        private void WalkClass(Type type, ref List<WalkEntry> messageTypes, ref List<WalkEntry> paths)
        {
            foreach (var propInfo in type.GetProperties())
            {
                if (propInfo.Name == "messageType" && propInfo.PropertyType == typeof(string))
                {
                    messageTypes.Add(new WalkEntry(type, propInfo));
                    continue;
                }

                if (propInfo.Name == "path" && !propInfo.PropertyType.IsArray)
                {
                    int x = 0; ;
                    ++x;
                }

                if (propInfo.Name == "path" && (propInfo.PropertyType.IsArray && propInfo.PropertyType.GetElementType() == typeof(string) || (typeof(ICollection).IsAssignableFrom(propInfo.PropertyType) && propInfo.PropertyType.GetGenericArguments()[0] == typeof(string))))
                {
                    paths.Add(new WalkEntry(type, propInfo));
                    continue;
                }

                if (propInfo.PropertyType.IsClass)
                {
                    WalkClass(propInfo.PropertyType, ref messageTypes, ref paths);
                }
            }
        }

        [Test]
        public void EnsureAllIAPEXPayloadsAreValid()
        {
            List<string> Failures = new();

            foreach (var IAPEXPayloadClassType in ReflectionHelpers.AllIAPEXPayloadClassTypes)
            {
                // Make sure the "Endpoint" property has JsonIgnore
                var EndpointProp = IAPEXPayloadClassType.GetProperties()
                    .First(p => p.Name == "Endpoint");
                if (EndpointProp.GetCustomAttribute<JsonIgnoreAttribute>() == null)
                {
                    Failures.Add($"{IAPEXPayloadClassType.Name}.{EndpointProp.Name} does not have the [JsonIgnore] attribute.");
                }

                List<WalkEntry> messageTypes = new();
                List<WalkEntry> paths = new();
                WalkClass(IAPEXPayloadClassType, ref messageTypes, ref paths);

                if (messageTypes.Count == 0 && paths.Count == 0)
                {
                    Failures.Add("Encountered an IAPEXPayload class that doesn't have either a messageType or path specified.");
                }

                foreach (var messageType in messageTypes)
                {
                    if (messageType.propertyInfo.GetCustomAttribute<EqualsAttribute>() == null)
                    {
                        Failures.Add($"{messageType.classType.Name}.{messageType.propertyInfo.Name} is missing an [Equals] attribute");
                    }
                }

                foreach(var path in paths)
                {
                    if (path.propertyInfo.GetCustomAttribute<ContainerLengthAttribute>() == null)
                    {
                        Failures.Add($"{path.classType.Name}.{path.propertyInfo.Name} is missing an [ContainerLength] attribute");
                    }

                    if (!path.propertyInfo.GetCustomAttributes<EqualsAttribute>().Any())
                    {
                        Failures.Add($"{path.classType.Name}.{path.propertyInfo.Name} requires at least one [Equals] attribute");
                    }
                }
            }

            if (Failures.Count > 0)
            {
                Assert.Fail(string.Join(Environment.NewLine, Failures));
            }
        }

        [Fact]
        public void Foo()
        {
            List<string> Errors = new();


        }
    }
}
#endif