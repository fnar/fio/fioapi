﻿namespace FIOAPI.Internal.Test
{
    [Parallelizable]
    public class ConstantTests
    {
        private readonly Dictionary<string, string> ContainerMappings = new()
        {
            { "ValidCurrencies", "CurrencyLength" },
            { "ValidPlanetResourceTypes", "PlanetResourceTypeLength" },
            { "ValidLocalMarketTypes", "LocalMarketTypeLength" },
            { "ValidRatings", "RatingLength" },
            { "ValidCXPCIntervals", "CXPCIntervalLength" },
            { "StoreTypeEnumToStoreType", "StoreTypeLength" },
            { "BuildingCategoryToType", "BuildingCategoryLength" },
            { "ValidWorkforceLevels", "WorkforceLevelLength" },
            { "ValidCOGCProgramTypes", "COGCProgramLength" },
            { "ValidWorkforceNeedCategories", "WorkforceNeedCategoryLength" },
            { "TypeToPlanetaryProject", "PlanetaryProjectLength" },
            { "ValidPlanetaryInfrastructureProjectTypes", "PlanetaryInfrastructureProjectLength" },
            { "ValidLeaderboardTypes", "LeaderboardTypesLength" },
            { "ValidLeaderboardRanges", "LeaderboardRangesLength" }
        };

        public readonly List<string> Exclusions = new()
        {
            "SupportedPositiveNumberTypes"
        };

        private bool IsListOfString(FieldInfo fieldInfo)
        {
            var FieldType = fieldInfo.FieldType;
            if (FieldType.IsGenericType && FieldType.GetGenericTypeDefinition() == typeof(List<>))
            {
                var ElementType = FieldType.GetGenericArguments()[0];
                return ElementType == typeof(string);
            }

            return false;
        }

        private bool IsDictionaryWithString(FieldInfo fieldInfo)
        {
            var FieldType = fieldInfo.FieldType;
            if (FieldType.IsGenericType && FieldType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
            {
                var KeyType = FieldType.GetGenericArguments()[0];
                var ValueType = FieldType.GetGenericArguments()[1];
                return KeyType == typeof(string) || ValueType == typeof(string);
            }

            return false;
        }

        [Test]
        public void VerifyKnownContainerConstants()
        {
            var ConstantClassType = typeof(FIOAPI.Constants);

            // Get candidates
            var FieldInfoCandidates = ConstantClassType
                .GetFields()
                .Where(p => IsListOfString(p) || IsDictionaryWithString(p))
                .ToList();

            var MissingFieldInfoNames = FieldInfoCandidates
                .Select(p => p.Name)
                .ToList();

            // Remove knowns
            MissingFieldInfoNames = MissingFieldInfoNames
                .Except(ContainerMappings.Keys)
                .ToList();

            // Remove exclusions
            MissingFieldInfoNames = MissingFieldInfoNames
                .Except(Exclusions)
                .ToList();

            Assert.That(MissingFieldInfoNames, Is.Empty, "Missing mapping in ContainerMappings");

            // Make sure all PropCandidates are readonly
            foreach (var FieldInfoCandidate in FieldInfoCandidates)
            {
                Assert.That(FieldInfoCandidate.IsStatic && FieldInfoCandidate.IsInitOnly, Is.True, $"{FieldInfoCandidate.Name} is not readonly");
            }
        }

        [Test]
        public void VerifyMinAndMaxLengthsExistAndAreConst()
        {
            var ConstantClassType = typeof(Constants);

            var FieldCandidates = ConstantClassType
                .GetFields()
                .Where(fi => IsListOfString(fi) || IsDictionaryWithString(fi))
                .Where(fi => ContainerMappings.Keys.Contains(fi.Name))
                .ToList();

            // ListMappingKeys
            var ListMappingValues = FieldCandidates
                .Select(fi => fi.Name)
                .Select(n => ContainerMappings[n])
                .ToList();

            // Mapping MinNames
            var MappingMinNames = ListMappingValues
                .Select(n => $"{n}Min")
                .ToList();

            // Mapping MaxNames
            var MappingMaxNames = ListMappingValues
                .Select(n => $"{n}Max")
                .ToList();

            var IntFieldInfoNames = ConstantClassType
                .GetFields()
                .Where(fi => fi.FieldType == typeof(int))
                .Select(p => p.Name)
                .ToList();

            // Property MinNames
            var FieldInfoMinNames = IntFieldInfoNames
                .Where(p => p.EndsWith("Min"))
                .ToList();

            // Property MaxNames
            var FieldInfoMaxNames = IntFieldInfoNames
                .Where(p => p.EndsWith("Max"))
                .ToList();

            List<string> MissingNamesList = new();

            if (MappingMinNames.Count > FieldInfoMinNames.Count)
            {
                MissingNamesList.AddRange(MappingMinNames
                   .Except(FieldInfoMinNames)
                   .ToList());
            }

            if (MappingMaxNames.Count > FieldInfoMaxNames.Count)
            {
                MissingNamesList.AddRange(MappingMaxNames
                    .Except(FieldInfoMaxNames)
                    .ToList());
            }

            if (MissingNamesList.Count != 0)
            {
                var MissingNamesStr = string.Join(", ", MissingNamesList);
                Assert.Fail($"The following constants are missing: {MissingNamesStr}");
            }

            // Make sure each min/max int FieldInfo is const
            var IntFieldInfos = ConstantClassType
                .GetFields()
                .Where(fi => fi.FieldType == typeof(int))
                .Where(fi => fi.Name.EndsWith("Min") || fi.Name.EndsWith("Max"))
                .Where(fi => MappingMinNames.Contains(fi.Name) || MappingMaxNames.Contains(fi.Name))
                .ToList();

            foreach (var IntFieldInfo in IntFieldInfos)
            {
                Assert.That(IntFieldInfo.IsLiteral && !IntFieldInfo.IsInitOnly, Is.True, $"{IntFieldInfo.Name} is not const");
            }
        }

        [Test]
        public void VerifyMinAndMaxLengthsAreCorrect()
        {
            var ContainerFieldInfos = typeof(Constants)
                .GetFields()
                .Where(p => IsListOfString(p) || IsDictionaryWithString(p))
                .Where(p => ContainerMappings.Keys.Contains(p.Name))
                .ToList();

            var MinMaxFieldInfos = typeof(Constants)
                .GetFields()
                .Where(fi => fi.FieldType == typeof(int))
                .Where(fi => fi.Name.EndsWith("Min") || fi.Name.EndsWith("Max"))
                .ToList();

            Dictionary<FieldInfo, List<FieldInfo>> ContainerToMinMax = new();
            foreach (var ContainerFieldInfo in ContainerFieldInfos)
            {
                // Min & Max field info
                var MinFieldInfo = MinMaxFieldInfos.FirstOrDefault(fi => fi.Name == $"{ContainerMappings[ContainerFieldInfo.Name]}Min");
                Assert.NotNull(MinFieldInfo);

                var MaxFieldInfo = MinMaxFieldInfos.FirstOrDefault(fi => fi.Name == $"{ContainerMappings[ContainerFieldInfo.Name]}Max");
                Assert.NotNull(MaxFieldInfo);

                List<FieldInfo> MinMax = new() { MinFieldInfo, MaxFieldInfo };
                ContainerToMinMax.Add(ContainerFieldInfo, MinMax);
            }

            // Verify the values are correct
            foreach (var ContainerKVP in ContainerToMinMax)
            {
                var ContainerFieldInfo = ContainerKVP.Key;
                var MinFieldInfo = ContainerKVP.Value[0];
                var MaxFieldInfo = ContainerKVP.Value[1];

                int ContainerMinLength = int.MaxValue;
                int ContainerMaxLength = 0;

                // Get the min and max string lengths from the container
                Assert.True(ContainerFieldInfo.FieldType.IsGenericType);
                if (ContainerFieldInfo.FieldType.GetGenericTypeDefinition() == typeof(List<>))
                {
                    Assert.True(ContainerFieldInfo.GetValue(null) is List<string>);
                    List<string> values = (ContainerFieldInfo.GetValue(null) as List<string>)!;
                    foreach (var value in values)
                    {
                        if (value.Length < ContainerMinLength)
                        {
                            ContainerMinLength = value.Length;
                        }
                        if (value.Length > ContainerMaxLength)
                        {
                            ContainerMaxLength = value.Length;
                        }
                    }
                }
                else
                {
                    Assert.True(ContainerFieldInfo.FieldType.GetGenericTypeDefinition() == typeof(Dictionary<,>));

                    // Figure out if the key or value is string
                    Type KeyType = ContainerFieldInfo.FieldType.GetGenericArguments()[0]!;
                    Type ValueType = ContainerFieldInfo.FieldType.GetGenericArguments()[1]!;

                    var DictionaryObject = ContainerFieldInfo.GetValue(null)!;

                    foreach (DictionaryEntry kvp in (IDictionary)DictionaryObject)
                    {
                        string value = (KeyType == typeof(string)) ? ((string)kvp.Key) : ((string)kvp.Value!)!;
                        if (value.Length < ContainerMinLength)
                        {
                            ContainerMinLength = value.Length;
                        }
                        if (value.Length > ContainerMaxLength)
                        {
                            ContainerMaxLength = value.Length;
                        }
                    }
                }

                int MinFieldInfoValue = (int)MinFieldInfo.GetValue(null)!;
                int MaxFieldInfoValue = (int)MaxFieldInfo.GetValue(null)!;

                if (ContainerMinLength != MinFieldInfoValue)
                {
                    Assert.Fail($"{MinFieldInfo.Name}'s value is '{MinFieldInfoValue}' but should be '{ContainerMinLength}'");
                }

                if (ContainerMaxLength != MaxFieldInfoValue)
                {
                    Assert.Fail($"{MaxFieldInfo.Name}'s value is '{MaxFieldInfoValue}' but should be '{ContainerMaxLength}'");
                }
            }
        }
    }
}
