﻿namespace FIOAPI.Internal.Test
{
    [Parallelizable]
    public class ValidationTests
    {
        class StringValidateTest : IValidation
        {
            [StringLength(4, MinimumLength = 2)]
            public string Value { get; set; } = "";
        }

        [Test]
        public void StringValidation()
        {
            var test = new StringValidateTest();
            Assert.False(test.PassesValidation());

            test.Value = "AB";
            Assert.True(test.PassesValidation());

            test.Value = "ABAB";
            Assert.True(test.PassesValidation());

            test.Value = "ABABA";
            Assert.False(test.PassesValidation());
        }

        class GuidValidateTest : IValidation
        {
            [GuidValid]
            public Guid Value { get; set; } = Guid.Empty;
        }

        [Test]
        public void GuidValidate()
        {
            var test = new GuidValidateTest();
            Assert.False(test.PassesValidation());

            test.Value = Guid.NewGuid();
            Assert.True(test.PassesValidation());
        }

        class RangeValidateTest : IValidation
        {
            [Range(1, 2)]
            public int Value { get; set; } = 0;
        }

        [Test]
        public void RangeValidate()
        {
            var test = new RangeValidateTest();
            Assert.False(test.PassesValidation());

            test.Value = 1;
            Assert.True(test.PassesValidation());

            test.Value = 2;
            Assert.True(test.PassesValidation());

            test.Value = 3;
            Assert.False(test.PassesValidation());
        }

        class ListValidateTest : IValidation
        {
            [NotEmpty]
            [Equals("Foo", OffsetIndex: 0)]
            [Equals("Bar", OffsetIndex: 1)]
            [Equals("Bbq", OffsetIndex: 3)]
            public List<string> Value { get; set; } = new List<string>();

            [Equals(1, OffsetIndex: 0)]
            [Equals(2, OffsetIndex: 1)]
            [Equals(3, OffsetIndex: 2)]
            [ContainerLength(5, minimumLength: 4)]
            public int[] Ints { get; set; } = null!;

            [Equals(42.42)]
            public double Exact { get; set; } = 42.42;
        }

        [Test]
        public void ListValidate()
        {
            var test = new ListValidateTest();
            Assert.False(test.PassesValidation());

            test.Value.Add("Foo");
            test.Value.Add("Bar");
            test.Value.Add("AnyString");
            test.Value.Add("Bbq");

            test.Ints = new int[3];
            test.Ints[0] = 1;
            test.Ints[1] = 2;
            test.Ints[2] = 3;
            Assert.False(test.PassesValidation());

            test.Ints = new int[5];
            test.Ints[0] = 1;
            test.Ints[1] = 2;
            test.Ints[2] = 3;
            test.Ints[3] = 4;
            test.Ints[4] = 5;
            Assert.True(test.PassesValidation_Throw());

            test.Value[2] = "AnotherString";
            Assert.True(test.PassesValidation_Throw());

            test.Value[0] = "Bar";
            Assert.False(test.PassesValidation());

            test.Value.Clear();
            Assert.False(test.PassesValidation());
        }

        class RequiredValidateTest : IValidation
        {
            public List<string> Value { get; set; } = null!;
        }

        [Test]
        public void RequiredValidate()
        {
            var test = new RequiredValidateTest();
            Assert.False(test.PassesValidation());

            test.Value = new List<string>();
            Assert.True(test.PassesValidation());
        }

        class CustomValidationTest : IValidation
        {
            public int Value { get; set; }

            public override void CustomValidation(ref List<string> Errors, string Context)
            {
                base.CustomValidation(ref Errors, Context);
                if (Value > 5 && Value < 10)
                {
                    Errors.Add("Value is > 5 and < 10");
                }
            }
        }

        [Test]
        public void CustomValidate()
        {
            var test = new CustomValidationTest();
            Assert.True(test.PassesValidation());

            test.Value = 5;
            Assert.True(test.PassesValidation());

            test.Value = 9;
            Assert.False(test.PassesValidation());
        }

        class RequiredAndNotEmptyTest : IValidation
        {
            [NotEmpty]
            public List<string> Value { get; set; } = null!;
        }

        [Test]
        public void RequiredAndNotEmptyValidate()
        {
            var test = new RequiredAndNotEmptyTest();
            Assert.False(test.PassesValidation());

            test.Value = new List<string>();
            Assert.False(test.PassesValidation());

            test.Value.Add("A");
            Assert.True(test.PassesValidation());
        }

        class ComplexTest : IValidation
        {
            [NotEmpty]
            public List<RequiredAndNotEmptyTest>? Test1 { get; set; }

            public CustomValidationTest Test2 { get; set; } = new CustomValidationTest();

            public RequiredValidateTest? Test3 { get; set; } = null;
        }

        [Test]
        public void ComplexValidate()
        {
            var test = new ComplexTest();

            test.Test1 = new List<RequiredAndNotEmptyTest>
            {
                new RequiredAndNotEmptyTest()
            };
            test.Test1[0].Value = new();
            test.Test1[0].Value!.Add("A");

            test.Test3 = new RequiredValidateTest();
            test.Test3.Value = new();
            Assert.True(test.PassesValidation());

            test.Test3.Value = null!;
            Assert.False(test.PassesValidation());

            test.Test3.Value = new();
            test.Test2.Value = 6;
            Assert.False(test.PassesValidation());

            test.Test2.Value = 0;
            test.Test1[0].Value!.Clear();
            Assert.False(test.PassesValidation());

            test.Test1[0].Value = null!;
            Assert.False(test.PassesValidation());
        }

        class RecursiveInnerTestClass : IValidation
        {
            [StringLength(10, MinimumLength = 5)]
            public string test { get; set; } = null!;

            public RecursiveTestClass Parent { get; set; } = null!;
        }

        class RecursiveTestClass : IValidation
        {
            public RecursiveInnerTestClass Inner { get; set; } = null!;
        }

        [Test]
        public void RecursiveTest()
        {
            var test = new RecursiveTestClass();
            var inner = new RecursiveInnerTestClass()
            {
                test = "FAIL",
                Parent = test
            };
            test.Inner = inner;

            Assert.False(test.PassesValidation());

            test.Inner.test = "PASSES";
            Assert.True(test.PassesValidation());
        }

        class APEXIDTestClass : IValidation
        {
            [APEXID]
            public string? test { get; set; }
        }

        [Test]
        public void APEXIDTest()
        {
            var test = new APEXIDTestClass();

            // null should succeed since it's not required
            test.test = null;
            Assert.True(test.PassesValidation());

            // Test not 32 characters
            test.test = new String('0', 33);
            Assert.False(test.PassesValidation());
            test.test = new String('0', 31);
            Assert.False(test.PassesValidation());

            // Test not uppercase
            test.test = new String('F', 32);
            Assert.False(test.PassesValidation());

            // Test invalid character
            test.test = new String('f', 31) + "g";
            Assert.False(test.PassesValidation());

            test.test = "0123456789abcdef0123456789abcdef";
            Assert.True(test.PassesValidation());
        }

        class NaturalIdTestClass : IValidation
        {
            [NaturalID]
            public string? naturalId { get; set; }
        }

        [Test]
        public void NaturalIdTest()
        {
            var test = new NaturalIdTestClass();

            // null should succeed since it's not required
            test.naturalId = null;
            Assert.True(test.PassesValidation());

            test.naturalId = "1";
            Assert.False(test.PassesValidation());

            test.naturalId = "123";
            Assert.False(test.PassesValidation());

            test.naturalId = "HRT-123";
            Assert.False(test.PassesValidation());

            test.naturalId = "UV-123A";
            Assert.False(test.PassesValidation());

            test.naturalId = "ABC";
            Assert.True(test.PassesValidation());

            test.naturalId = "UV-123";
            Assert.True(test.PassesValidation());

            test.naturalId = "UV-123z";
            Assert.True(test.PassesValidation());
        }

        class TickerTestClass : IValidation
        {
            [Ticker]
            public string? test { get; set; }
        }

        [Test]
        public void TickerTest()
        {
            var test = new TickerTestClass();

            // null should succeed since it's not required
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string should fail
            test.test = "";
            Assert.False(test.PassesValidation());

            // Lowercase anything should fail
            test.test = "rat";
            Assert.False(test.PassesValidation());
            test.test = "w";
            Assert.False(test.PassesValidation());
            test.test = "cogc";
            Assert.False(test.PassesValidation());

            // Anything more than 4 characters should fail
            test.test = "LMATCX";
            Assert.False(test.PassesValidation());

            // Anything between 1 and 4 characters uppercase should succeed
            test.test = "W";
            Assert.True(test.PassesValidation());
            test.test = "DW";
            Assert.True(test.PassesValidation());
            test.test = "RAT";
            Assert.True(test.PassesValidation());
            test.test = "LOCM";
            Assert.True(test.PassesValidation());
        }

        public class LowercaseTestClass : IValidation
        {
            [Lowercase]
            public string? test { get; set; }
        }

        [Test]
        public void LowercaseTest()
        {
            var test = new LowercaseTestClass();

            // Null is fine
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string is fine
            test.test = "";
            Assert.True(test.PassesValidation());

            test.test = "Hello";
            Assert.False(test.PassesValidation());

            test.test = "hello";
            Assert.True(test.PassesValidation());
        }

        public class UppercaseTestClass : IValidation
        {
            [Uppercase]
            public string? test { get; set; }
        }

        [Test]
        public void UppercaseTest()
        {
            var test = new UppercaseTestClass();

            // Null is fine
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string is fine
            test.test = "";
            Assert.True(test.PassesValidation());

            test.test = "hELLO";
            Assert.False(test.PassesValidation());

            test.test = "HELLO";
            Assert.True(test.PassesValidation());
        }

        public class CurrencyCodeTestClass : IValidation
        {
            [CurrencyCode]
            public string? test { get; set; }
        }

        [Test]
        public void CurrencyCodeTest()
        {
            var test = new CurrencyCodeTestClass();

            // Null is fine
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string is bad
            test.test = "";
            Assert.False(test.PassesValidation());

            // Single char
            test.test = "E";
            Assert.False(test.PassesValidation());

            // 3 characters, but not a valid one
            test.test = "AAA";
            Assert.False(test.PassesValidation());

            // Check against each currency
            FIOAPI.Constants.ValidCurrencies.ForEach(vc =>
            {
                test.test = vc;
                Assert.True(test.PassesValidation());
            });
        }

        public class FXTickerTestClass : IValidation
        {
            [FXTicker]
            public string? test { get; set; }
        }

        [Test]
        public void FXTickerTest()
        {
            var test = new FXTickerTestClass();

            // null is fine
            test.test = null;
            Assert.True(test.PassesValidation());

            // Empty string is bad
            test.test = "";
            Assert.False(test.PassesValidation());

            // Wrong number of chars
            test.test = "ABCABCABCABC";
            Assert.False(test.PassesValidation());

            // Invalid ticker both sides
            test.test = "AAA/BBB";
            Assert.False(test.PassesValidation());

            // Invalid ticker left side
            test.test = $"AAA/{Constants.ValidCurrencies[0]}";
            Assert.False(test.PassesValidation());

            // Invalid ticker right-side
            test.test = $"{Constants.ValidCurrencies[0]}/AAA";
            Assert.False(test.PassesValidation());

            // dash instead of /
            test.test = $"{Constants.ValidCurrencies[0]}-{Constants.ValidCurrencies[0]}";
            Assert.False(test.PassesValidation());

            // Match tickers on both sides
            test.test = $"{Constants.ValidCurrencies[0]}/{Constants.ValidCurrencies[0]}";
            Assert.False(test.PassesValidation());

            // Test all other combinations
            foreach (var left in Constants.ValidCurrencies)
            {
                foreach (var right in Constants.ValidCurrencies)
                {
                    test.test = $"{left}/{right}";
                    if (left == right)
                    {
                        Assert.False(test.PassesValidation());
                    }
                    else
                    {
                        Assert.True(test.PassesValidation());
                    }
                }
            }
        }

        [Test]
        public void RunValidationAsIfTypeAndPropertyName()
        {
            List<string> Errors = new();

            var TestUserName = "Saganaki";
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserName, typeof(User), nameof(User.UserName), ref Errors, "TestUserName");
            Assert.IsNotEmpty(Errors);

            Errors.Clear();
            TestUserName = "saganaki";
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserName, typeof(User), nameof(User.UserName), ref Errors, "TestUserName");
            Assert.IsEmpty(Errors);

            Errors.Clear();
            var TestUserNames = new List<string>
            {
                "Saganaki",
                "Kovus"
            };
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserNames, typeof(User), nameof(User.UserName), ref Errors, "TestUserNames");
            Assert.That(Errors, Has.Count.EqualTo(2));

            Errors.Clear();
            TestUserNames = TestUserNames.ConvertAll(u => u.ToLower());
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserNames, typeof(User), nameof(User.UserName), ref Errors, "TestUserNames");
            Assert.IsEmpty(Errors);

            var TestUserNamesArray = new string[]
            {
                "Saganaki",
                "Kovus"
            };
            Errors.Clear();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserNamesArray, typeof(User), nameof(User.UserName), ref Errors, "TestUserNamesArray");
            Assert.That(Errors, Has.Count.EqualTo(2));

            TestUserNamesArray[0] = "saganaki";
            TestUserNamesArray[1] = "kovus";
            Errors.Clear();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(TestUserNamesArray, typeof(User), nameof(User.UserName), ref Errors, "TestUserNamesArray");
            Assert.IsEmpty(Errors);
        }

        public class APEXTimestampTestClass : IValidation
        {
            [APEXTimestamp]
            public long test { get; set; }
        }

        [Test]
        public void RunAPEXTimestampValidations()
        {
            var test = new APEXTimestampTestClass();
            test.test = Constants.MinimumAPEXTimestamp - 1;
            Assert.False(test.PassesValidation());

            test.test = Constants.MaximumAPEXTimestamp + 1;
            Assert.False(test.PassesValidation());

            test.test = Constants.MinimumAPEXTimestamp;
            Assert.True(test.PassesValidation());

            test.test = Constants.MaximumAPEXTimestamp;
            Assert.True(test.PassesValidation());

            test.test = (Constants.MaximumAPEXTimestamp + Constants.MinimumAPEXTimestamp) / 2;
            Assert.True(test.PassesValidation());
        }

        public class InnerNoValidateTestClass : IValidation
        {
            [Range(1, 5)]
            public int x { get; set; }
        }

        public class NoValidateTestClass : IValidation
        {
            [NoValidate]
            public InnerNoValidateTestClass test { get; set; } = null!;
        }

        [Test]
        public void RunNoValidateTest()
        {
            var test = new NoValidateTestClass();
            test.test = new InnerNoValidateTestClass();
            test.test.x = 42;

            Assert.False(test.test.PassesValidation());
            Assert.True(test.PassesValidation());
        }

        public class PositiveNumberTestClass : IValidation
        {
            [PositiveNumber]
            public double a { get; set; } = 1.0;

            [PositiveNumber]
            public float b { get; set; } = 1.0f;

            [PositiveNumber]
            public int c { get; set; } = 1;

            [PositiveNumber]
            public long d { get; set; } = 1;

            [PositiveNumber]
            public short e { get; set; } = 1;

            [PositiveNumber]
            public sbyte f { get; set; } = 1;
        }

        [Test]
        public void RunPositiveNumberTest()
        {
            var test = new PositiveNumberTestClass();
            Assert.True(test.PassesValidation());

            test.a = 0;
            Assert.False(test.PassesValidation());

            test.a = -1.0;
            Assert.False(test.PassesValidation());
            test.a = 1.0;

            test.b = -1.0f;
            Assert.False(test.PassesValidation());
            test.b = 1.0f;

            test.c = -1;
            Assert.False(test.PassesValidation());
            test.c = 1;

            test.d = -1;
            Assert.False(test.PassesValidation());
            test.d = 1;

            test.e = -1;
            Assert.False(test.PassesValidation());
            test.e = 1;

            test.f = -1;
            Assert.False(test.PassesValidation());
            test.f = 1;

            Assert.True(test.PassesValidation());
        }

        public class ResourceTypeTestClass : IValidation
        {
            [ResourceType]
            public string test { get; set; } = null!;
        }

        [Test]
        public void RunResourceTypeTests()
        {
            var test = new ResourceTypeTestClass();

            Constants.ValidPlanetResourceTypes.ForEach(vrt =>
            {
                test.test = vrt;
                Assert.True(test.PassesValidation());

                test.test = vrt.ToLower();
                Assert.False(test.PassesValidation());
            });
        }

        public class BuildingCategoryTestClass : IValidation
        {
            [BuildingCategory]
            public string test { get; set; } = null!;
        }

        [Test]
        public void BuildingCategoryTests()
        {
            var test = new BuildingCategoryTestClass()
            {
                test = "foo"
            };
            Assert.False(test.PassesValidation());

            Constants.BuildingCategoryToType.Keys.ToList().ForEach(vbc =>
            {
                test.test = vbc;
                Assert.True(test.PassesValidation());

                test.test = vbc.ToLower();
                Assert.False(test.PassesValidation());
            });
        }

        public class WorkforceLevelTestClass : IValidation
        {
            [WorkforceLevel]
            public string test { get; set; } = null!;
        }

        [Test]
        public void WorkforceLevelTests()
        {
            var test = new WorkforceLevelTestClass
            {
                test = "foo"
            };
            Assert.False(test.PassesValidation());

            Constants.ValidWorkforceLevels.ForEach(vwl =>
            {
                test.test = vwl;
                Assert.True(test.PassesValidation());

                test.test = vwl.ToLower();
                Assert.False(test.PassesValidation());
            });
        }

        public class EqualsFailExpectedNull : IValidation
        {
            [Equals(null)]
            public string? Value { get; set; } = "Foo";
        }

        public class EqualsFailExpectedNotNull : IValidation
        {
            [Equals("Foo")]
            public string? Value { get; set; }
        }

        public class EqualsFailWrongType : IValidation
        {
            [Equals(1)]
            public string Value { get; set; } = "1";
        }

        public class EqualsFailNumberMismatchType : IValidation
        {
            [Equals(42)]
            public double ValueA { get; set; } = 42.0;

            [Equals(42)]
            public long ValueB { get; set; } = 42;

            [Equals(15.0f)]
            public double ValueC { get; set; } = 15.0;

            [Equals(42.42)]
            public int ValueD { get; set; } = 42;
        }

        public class EqualsFailNotMatchExpectedValue : IValidation
        {
            [Equals("Bar")]
            public string ValueA { get; set; } = "Foo";

            [Equals(42.0)]
            public double ValueB { get; set; } = 2.0;
        }

        public class EqualsWorking : IValidation
        {
            [Equals(42, typeof(double))]
            public double ValueA { get; set; } = 42.0;

            [Equals((double)15)]
            public double ValueB { get; set; } = 15.0;

            [Equals("Foo")]
            public string ValueC { get; set; } = "Foo";

            [Equals(5.0)]
            public double ValueD { get; set; } = 5;
        }

        [Test]
        public void TestEquals()
        {
            bool validateResult;
            List<string> Errors = new();

            var testObjA = new EqualsFailExpectedNull();
            validateResult = testObjA.Validate(ref Errors);
            Assert.False(validateResult);
            Assert.That(Errors, Has.Count.EqualTo(1));
            Assert.That(Errors.Any(e => e.Contains("was expected to be null")), Is.True);
            Errors.Clear();

            var testObjB = new EqualsFailExpectedNotNull();
            validateResult = testObjB.Validate(ref Errors);
            Assert.False(validateResult);
            Assert.That(Errors, Has.Count.EqualTo(1));
            Assert.That(Errors.Any(e => e.Contains("is null but is expected")), Is.True);
            Errors.Clear();

            var testObjC = new EqualsFailWrongType();
            validateResult = testObjC.Validate(ref Errors);
            Assert.False(validateResult);
            Assert.That(Errors, Has.Count.EqualTo(1));
            Assert.That(Errors.Any(e => e.Contains("could not be converted")), Is.True);
            Errors.Clear();

            var testObjD = new EqualsFailNumberMismatchType();
            validateResult = testObjD.Validate(ref Errors);
            Assert.False(validateResult);
            Assert.That(Errors, Has.Count.EqualTo(4));
            Assert.True(Errors.All(e => e.Contains("could not be converted")));
            Errors.Clear();

            var testObjE = new EqualsFailNotMatchExpectedValue();
            validateResult = testObjE.Validate(ref Errors);
            Assert.False(validateResult);
            Assert.That(Errors, Has.Count.EqualTo(2));
            Assert.True(Errors.All(e => e.Contains("does not match the expected")));
            Errors.Clear();

            var testObjF = new EqualsWorking();
            validateResult = testObjF.Validate(ref Errors);
            Assert.True(validateResult);
        }
    }
}