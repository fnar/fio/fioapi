﻿namespace FIOAPI.Internal.Test
{
    [Parallelizable]
    public class APEXEntityTests
    {
        private static void CheckCloneable(Type t, ref List<string> FailureMessages)
        {
            var PropInfos = t
                    .GetProperties(BindingFlags.Instance | BindingFlags.Public)
                    .Where(p => p.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)))
                    .Where(p => p.PropertyType.GetGenericArguments().Any())
                    .Where(p => p.PropertyType.GetGenericArguments()[0].IsSubclassOf(typeof(IValidation)))
                    .ToList();
            foreach (var PropInfo in PropInfos)
            {
                var attrs = PropInfo.GetCustomAttributes(true);
                var type = PropInfo.PropertyType.GetGenericArguments()[0];

                bool IsNotMapped = attrs.Any(attr => attr is NotMappedAttribute);
                if (IsNotMapped)
                {
                    continue;
                }

                bool IsNoValidate = attrs.Any(attr => attr is NoValidateAttribute);
                if (IsNoValidate)
                {
                    continue;
                }

                bool HasGetterAndSetter = PropInfo.GetGetMethod() != null && PropInfo.GetSetMethod() != null;
                if (HasGetterAndSetter)
                {
                    // Make sure this property also is subclass of ICloneable
                    bool IsCloneable = typeof(ICloneable).IsAssignableFrom(type);
                    if (IsCloneable)
                    {
                        CheckCloneable(type, ref FailureMessages);
                    }
                    else
                    {
                        FailureMessages.Add($"{t.Name}.{PropInfo.Name} is not ICloneable");
                    }
                }
            }
        }

        [Test]
        public void VerifyAllAPEXEntitiesAndChildrenAreCloneable()
        {
            var ApexEntityType = typeof(IAPEXEntity);
            var ModelNamespace = ApexEntityType.Namespace;
            var FIOAPIAssembly = Assembly.GetAssembly(ApexEntityType);
            Assert.That(FIOAPIAssembly, Is.Not.Null);
            var AllAPEXEntities = FIOAPIAssembly.GetTypes()
                .Where(t => ApexEntityType.IsAssignableFrom(t))
                .ToList();
            Assert.That(AllAPEXEntities, Is.Not.Null);
            Assert.That(AllAPEXEntities, Is.Not.Empty);

            var ValidationType = typeof(IValidation);
            List<string> FailureMessages = new();

            foreach (var APEXEntity in AllAPEXEntities)
            {
                CheckCloneable(APEXEntity, ref FailureMessages);
            }

            if (FailureMessages.Any())
            {
                Assert.Fail(string.Join(Environment.NewLine, FailureMessages));
            }
        }

        // @TODO: CHECK contents? Sample data? We need a way to verify that an IAPEXEntity implements clone PROPERLY
    }
}
