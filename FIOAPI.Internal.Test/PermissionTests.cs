﻿namespace FIOAPI.Internal.Test
{
    [Parallelizable]
    public class PermissionTests
    {
        [Test]
        public void VerifyPermissionCounts()
        {
            // Grab the number of bools on the DB model
            var dbPermission = new DB.Model.Permission();
            var numDBPermissions = dbPermission
                .GetType()
                .GetProperties()
                .Where(p => p.PropertyType == typeof(bool))
                .Count();

            // Grab the number of bools in the permissions payload
            int numPermissionsPayload = 0;
            var permissionsPayload = new Payloads.Permission.Permissions();

            // Check top-level bools (there really shouldn't be any, but just in case)
            numPermissionsPayload += permissionsPayload
                .GetType()
                .GetProperties()
                .Where(p => p.PropertyType == typeof(bool))
                .Count();

            // Check sub-object bools
            var subObjects = permissionsPayload
                .GetType()
                .GetProperties()
                .ToList();
            foreach (var subObject in subObjects)
            {
                if (!subObject.PropertyType.IsPrimitive)
                {
                    numPermissionsPayload += subObject.PropertyType
                        .GetProperties()
                        .Where(p => p.PropertyType == typeof(bool))
                        .Count();
                }
            }

            Assert.That(numPermissionsPayload, Is.EqualTo(numDBPermissions));

            // Now check Perm enum (-2: Minus one for None. Minus one for All)
            var permCount = Enum.GetNames(typeof(Perm)).Count() - 2;
            Assert.That(numDBPermissions, Is.EqualTo(permCount));
        }
    }
}
