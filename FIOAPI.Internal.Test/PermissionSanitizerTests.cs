﻿namespace FIOAPI.Internal.Test
{
    [Parallelizable]
    public class PermissionSantizerTests
    {
        [RequiredPermission(Perm.Company_LiquidCurrency)]
        public class PermissionTestParent
        {
            [APEXID]
            [RequiredPermission(Perm.Ship_Information)]
            public string TestAPEXID { get; set; } = null!;

            [RequiredPermission(Perm.Ship_Repair)]
            public string TestNormalString { get; set; } = null!;

            [RequiredPermission(Perm.Ship_Flight)]
            public string? TestNullableString { get; set; } = null;

            [RequiredPermission(Perm.Ship_Inventory)]
            public List<PermissionTestChild> TestChildren { get; set; } = new();

            [RequiredPermission(Perm.Ship_FuelInventory)]
            public List<string> TestOnlyGetter
            {
                get
                {
                    return new List<string> { "Foo", "Bar" };
                }
            }
        }

        [RequiredPermission(Perm.Misc_ShipmentTracking)]
        public class PermissionTestChild
        {
            [RequiredPermission(Perm.Sites_Location)]
            public DateTime TestDateTime { get; set; }

            [RequiredPermission(Perm.Sites_Workforces)]
            public Guid TestGuid { get; set; }

            [RequiredPermission(Perm.Sites_Experts)]
            public int TestInt { get; set; }

            [RequiredPermission(Perm.Sites_Buildings)]
            public double TestDouble { get; set; }

            [RequiredPermission(Perm.Sites_Repair)]
            public List<string> TestListString { get; set; } = new();

            [RequiredPermission(Perm.Sites_Reclaimable)]
            public List<string>? NullableList { get; set; } = new();
        }

        private PermissionTestParent GetNewTestObj()
        {
            return new PermissionTestParent
            {
                TestAPEXID = "11111111111111111111111111111111",
                TestNormalString = "Foo",
                TestNullableString = "Bar",
                TestChildren = new List<PermissionTestChild>()
                {
                    new PermissionTestChild
                    {
                        TestDateTime = DateTime.UtcNow,
                        TestGuid = Guid.NewGuid(),
                        TestInt = 42,
                        TestDouble = 42.0,
                        TestListString = new List<string>() { "A", "B", "C" },
                        NullableList = new List<string>() { "X", "Y", "Z" }
                    },
                    new PermissionTestChild
                    {
                        TestDateTime = DateTime.UtcNow,
                        TestGuid = Guid.NewGuid(),
                        TestInt = 84,
                        TestDouble = 84.0,
                        TestListString = new List<string>() { "AA", "BB", "CC" },
                        NullableList = new List<string>() { "XX", "YY", "ZZ" }
                    }
                }
            };
        }

        [Test]
        public void TestPermissionSanitizer()
        {
            PermissionSanitizer.Initialize();

            var Test = GetNewTestObj();

            List<string> Errors = new();
            bool ApplyResult = PermissionSanitizer.ApplyPermissionSanitizations(Perm.All, ref Test, ref Errors);
            Assert.Multiple(() =>
            {
                Assert.That(ApplyResult, Is.True);
                Assert.That(Errors, Is.Empty);
                Assert.That(Test.TestAPEXID, Is.EqualTo("11111111111111111111111111111111"));
                Assert.That(Test.TestNormalString, Is.EqualTo("Foo"));
                Assert.That(Test.TestNullableString!, Is.EqualTo("Bar"));
                Assert.That(Test.TestChildren, Has.Count.EqualTo(2));
            });
            var Child0 = Test.TestChildren[0];
            Assert.That(Child0.TestDateTime, Is.Not.EqualTo(DateTime.MinValue));
            Assert.That(Child0.TestGuid, Is.Not.EqualTo(Guid.Empty));
            Assert.That(Child0.TestInt, Is.EqualTo(42));
            Assert.That(Child0.TestDouble, Is.EqualTo(42.0));
            Assert.That(Child0.TestListString, Has.Count.EqualTo(3));
            Assert.That(Child0.TestListString, Has.Count.EqualTo(3));

            // Test no permission on parent object
            Errors.Clear();
            Test = GetNewTestObj();
            ApplyResult = PermissionSanitizer.ApplyPermissionSanitizations(Perm.All & ~Perm.Company_LiquidCurrency, ref Test, ref Errors);
            Assert.That(ApplyResult, Is.False);
            Assert.That(Errors, Is.Not.Empty);

            // Test no APEXID
            Errors.Clear();
            Test = GetNewTestObj();
            ApplyResult = PermissionSanitizer.ApplyPermissionSanitizations(Perm.All & ~Perm.Ship_Information, ref Test, ref Errors);
            Assert.That(ApplyResult, Is.True);
            Assert.That(Test.TestAPEXID, Is.EqualTo(Constants.InvalidAPEXID));
            Assert.That(Test.TestNormalString, Is.EqualTo("Foo"));
            Assert.That(Test.TestNullableString!, Is.EqualTo("Bar"));
            Assert.That(Errors, Is.Not.Empty);

            // Test no TestChildren - Attr on parent class property
            Errors.Clear();
            Test = GetNewTestObj();
            ApplyResult = PermissionSanitizer.ApplyPermissionSanitizations(Perm.All & ~Perm.Ship_Inventory, ref Test, ref Errors);
            Assert.That(ApplyResult, Is.True);
            Assert.That(Test.TestAPEXID, Is.EqualTo("11111111111111111111111111111111"));
            Assert.That(Test.TestNormalString, Is.EqualTo("Foo"));
            Assert.That(Test.TestNullableString!, Is.EqualTo("Bar"));
            Assert.That(Errors, Is.Not.Empty);
            Assert.That(Test.TestChildren, Is.Empty);

            // Test no TestChildren - Attr on child class
            Errors.Clear();
            Test = GetNewTestObj();
            ApplyResult = PermissionSanitizer.ApplyPermissionSanitizations(Perm.All & ~Perm.Misc_ShipmentTracking, ref Test, ref Errors);
            Assert.That(ApplyResult, Is.True);
            Assert.That(Test.TestAPEXID, Is.EqualTo("11111111111111111111111111111111"));
            Assert.That(Test.TestNormalString, Is.EqualTo("Foo"));
            Assert.That(Test.TestNullableString!, Is.EqualTo("Bar"));
            Assert.That(Errors, Is.Not.Empty);
            Assert.That(Test.TestChildren, Is.Empty);

            // Test no subobject list
            Errors.Clear();
            Test = GetNewTestObj();
            ApplyResult = PermissionSanitizer.ApplyPermissionSanitizations(Perm.All & ~Perm.Sites_Repair, ref Test, ref Errors);
            Assert.That(ApplyResult, Is.True);
            Assert.That(Test.TestAPEXID, Is.EqualTo("11111111111111111111111111111111"));
            Assert.That(Test.TestNormalString, Is.EqualTo("Foo"));
            Assert.That(Test.TestNullableString!, Is.EqualTo("Bar"));
            Assert.That(Errors, Is.Not.Empty);
            Assert.That(Test.TestChildren, Is.Not.Empty);
            Assert.That(Test.TestChildren[0].TestListString, Is.Empty);

            // Test nullable
            Errors.Clear();
            Test = GetNewTestObj();
            ApplyResult = PermissionSanitizer.ApplyPermissionSanitizations(Perm.All & ~Perm.Ship_Flight, ref Test, ref Errors);
            Assert.That(ApplyResult, Is.True);
            Assert.That(Test.TestAPEXID, Is.EqualTo("11111111111111111111111111111111"));
            Assert.That(Test.TestNormalString, Is.EqualTo("Foo"));
            Assert.That(Test.TestNullableString, Is.Null);
            Assert.That(Errors, Is.Not.Empty);
            Assert.That(Test.TestChildren, Is.Not.Empty);
            Assert.That(Test.TestNullableString, Is.Null);

            // Test empty string
            Errors.Clear();
            Test = GetNewTestObj();
            ApplyResult = PermissionSanitizer.ApplyPermissionSanitizations(Perm.All & ~Perm.Ship_Repair, ref Test, ref Errors);
            Assert.That(ApplyResult, Is.True);
            Assert.That(Test.TestAPEXID, Is.EqualTo("11111111111111111111111111111111"));
            Assert.That(Test.TestNormalString, Is.EqualTo(""));
            Assert.That(Test.TestNullableString!, Is.EqualTo("Bar"));
            Assert.That(Errors, Is.Not.Empty);
            Assert.That(Test.TestChildren, Is.Not.Empty);
            Assert.That(Test.TestNormalString, Is.Empty);
        }
    }
}
