﻿using System.Diagnostics;

namespace FIOAPI
{
    /// <summary>
    /// DatabaseType
    /// </summary>
    public enum DatabaseType
    {
        /// <summary>
        /// None
        /// </summary>
        None,

        /// <summary>
        /// SQLite
        /// </summary>
        Sqlite,

        /// <summary>
        /// Postgres
        /// </summary>
        Postgres
    }

    /// <summary>
    /// Globals
    /// </summary>
    public static class Globals
    {
        /// <summary>
        /// The configuration root with the given appsettings.json hierarchy properly resolved
        /// </summary>
        public static ConfigurationRoot ConfigRoot
        {
            get
            {
                if (_ConfigRoot == null)
                {
                    _ConfigRoot = (ConfigurationRoot)new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile("appsettings.json")
                        .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "production"}.json", true)
                        .Build();
                }

                return _ConfigRoot!;
            }
        }
        private static ConfigurationRoot? _ConfigRoot { get; set; } = null;

        /// <summary>
        /// The 'database' section within ConfigRoot
        /// </summary>
        private static IConfigurationSection DBConf
        {
            get
            {
                if (_DBConf == null)
                {
                    _DBConf = ConfigRoot.GetRequiredSection("database");
                }

                return _DBConf;
            }
        }
        private static IConfigurationSection? _DBConf { get; set; } = null;

        /// <summary>
        /// If we should apply a migration to the database
        /// </summary>
        public static bool ApplyMigration { get; set; } = false;

        /// <summary>
        /// If we should run after applying a migration
        /// </summary>
        public static bool RunAfterMigrate { get; set; } = false;

        /// <summary>
        /// If we should emit debug database logging
        /// </summary>
        public static bool DebugDatabase { get; set; } = false;

        /// <summary>
        /// If we're testing
        /// </summary>
        public static bool IsTesting { get; set; } = false;

        /// <summary>
        /// An override database type.  Options: [sqlite, postgres]
        /// </summary>
        public static string OverrideDBType { get; set; } = "";

        /// <summary>
        /// The ExceptionsPath
        /// </summary>
        public static string ExceptionsPath { get; set; } = "RuntimeExceptions";

        /// <summary>
        /// The database type to use
        /// </summary>
        public static DatabaseType DatabaseType { get; set; } = DatabaseType.Sqlite;
        /// <summary>
        /// The command timeout of database operations in seconds
        /// </summary>
        public static int CommandTimeout = 300;

        /// <summary>
        /// The database connection string to use
        /// </summary>
        public static string DatabaseConnectionString
        {
            get
            {
                if (_DatabaseConnectionString == null)
                {
                    string DBType = OverrideDBType;
                    if (DBType.Length == 0)
                    {
                        DBType = DBConf.MustGetValue<string>("type").ToLowerInvariant();
                    }

                    switch (DBType)
                    {
                        case "sqlite":
                            DatabaseType = DatabaseType.Sqlite;
                            _DatabaseConnectionString = SqliteServerConnectionString;
                            break;
                        case "postgres":
                        case "postgresql":
                            DatabaseType = DatabaseType.Postgres;
                            _DatabaseConnectionString = PostgresServerConnectionString;
                            break;
                        default:
                            throw new InvalidOperationException("Invalid database type requested");
                    }
                }

                return _DatabaseConnectionString;
            }
        }
        private static string? _DatabaseConnectionString = null;

        /// <summary>
        /// The sqlite server connection string
        /// </summary>
        public static string SqliteServerConnectionString
        {
            get
            {
                if (_SqliteServerConnectionString == null)
                {
                    var filename = DBConf.MustGetValue<string>("filename").QuoteWrap();
                    _SqliteServerConnectionString = $"Data Source={filename}";
                }

                return _SqliteServerConnectionString;
            }
        }
        private static string? _SqliteServerConnectionString = null;

        /// <summary>
        /// The postgres server connection string
        /// </summary>
        public static string PostgresServerConnectionString
        {
            get
            {
                if (_PostgresServerConnectionString == null)
                {
                    var KeyValuePairs = new List<string>();

                    var hostname = DBConf.MustGetValue<string>("hostname").QuoteWrap();
                    KeyValuePairs.Add($"Host={hostname}");

                    var database = DBConf.MustGetValue<string>("database").QuoteWrap();
                    KeyValuePairs.Add($"Database={database}");

                    var username = DBConf.MustGetValue<string>("username").QuoteWrap();
                    KeyValuePairs.Add($"Username={username}");

                    var password = DBConf.MustGetValue<string>("password").QuoteWrap();
                    KeyValuePairs.Add($"Password={password}");

                    int CommandTimeoutSecs = 30;
                    if (Debugger.IsAttached)
                    {
                        CommandTimeoutSecs = 300;
                    }

                    KeyValuePairs.Add($"CommandTimeout={CommandTimeoutSecs}");

                    var debugdatabase = $"Include Error Detail={DebugDatabase}";
                    KeyValuePairs.Add(debugdatabase);

                    _PostgresServerConnectionString = string.Join(";", KeyValuePairs.ToArray());
                }

                return _PostgresServerConnectionString;
            }
        }
        private static string? _PostgresServerConnectionString = null;

        /// <summary>
        /// A helper extension method to throw if a given key doesn't exist in a given section
        /// </summary>
        /// <typeparam name="T">The type to retrieve</typeparam>
        /// <param name="section">the section of the configuration file</param>
        /// <param name="key">the key of the value to retrieve</param>
        /// <returns>T</returns>
        /// <exception cref="InvalidOperationException">If the key in the provided section isn't found</exception>
        private static T MustGetValue<T>(this IConfigurationSection section, string key)
        {
            T? result = section.GetValue<T>(key);
            if (result == null)
            {
                throw new InvalidOperationException($"'{key}' in section '{section.Key}' not resolved");
            }

            return result;
        }

        /// <summary>
        /// Reloads the configuration
        /// </summary>
        public static void ReloadConfiguration()
        {
            _ConfigRoot = null;
            _DBConf = null;
            _DatabaseConnectionString = null;
        }
    }
}
