﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Planet
{
    public class PATH_POPULATION_PROJECTS : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PATH_POPULATION_PROJECTS_PAYLOAD payload { get; set; } = null!;
    }

    public class PATH_POPULATION_PROJECTS_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PATH_POPULATION_PROJECTS_MESSAGE message { get; set; } = null!;
    }

    public class PATH_POPULATION_PROJECTS_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PATH_POPULATION_PROJECTS_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PATH_POPULATION_PROJECTS_PAYLOAD_INNER : IValidation
    {
        public PATH_POPULATION_PROJECTS_BODY body { get; set; } = null!;

        [ContainerLength(4)]
        [Equals("populations", OffsetIndex:0)]
        [Equals("projects", OffsetIndex:2)]
        public string[] path { get; set; } = null!;
    }

    public class PATH_POPULATION_PROJECTS_BODY : IValidation
    {
        [StringLength(Constants.PlanetaryInfrastructureProjectLengthMax, MinimumLength = Constants.PlanetaryInfrastructureProjectLengthMin)]
        public string type { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string projectIdentifier { get; set; } = null!;

        [Range(0, 10)]
        public int level { get; set; }

        [Range(-1, 10)]
        public int activeLevel { get; set; }

        [Range(0, 10)]
        public int currentLevel { get; set; }

        public List<PATH_POPULATION_PROJECTS_UPGRADE_COST>? upgradeCosts { get; set; }

        [Range(0.0, 1.0)]
        public double upgradeStatus { get; set; }

        public List<PATH_POPULATION_PROJECTS_UPKEEP> upkeeps { get; set; } = null!;

        public List<PATH_POPULATION_PROJECTS_CONTRIBUTION> contributions { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;
    }

    public class PATH_POPULATION_PROJECTS_UPGRADE_COST : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int amount { get; set; }

        [Range(0, int.MaxValue)]
        public int currentAmount { get; set; }
    }

    public class PATH_POPULATION_PROJECTS_UPKEEP : IValidation
    {
        [Range(0, int.MaxValue)]
        public int stored { get; set; }

        [Range(0, int.MaxValue)]
        public int storeCapacity { get; set; }

        [Range(0, int.MaxValue)]
        public int duration { get; set; }

        public APEX_COMMON_TIMESTAMP nextTick { get; set; } = null!;

        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int amount { get; set; }

        [Range(0, int.MaxValue)]
        public int currentAmount { get; set; }
    }


    public class PATH_POPULATION_PROJECTS_CONTRIBUTION : IValidation
    {
        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> materials { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE contributor { get; set; } = null!;
    }
}
#pragma warning restore 1591