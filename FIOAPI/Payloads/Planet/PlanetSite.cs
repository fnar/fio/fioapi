﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Planet
{
    public class PLANET_SITES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PLANET_SITE_PAYLOAD payload { get; set; } = null!;
    }

    public class PLANET_SITE_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PLANET_SITE_MESSAGE message { get; set; } = null!;
    }

    public class PLANET_SITE_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PLANET_SITE_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PLANET_SITE_PAYLOAD_INNER : IValidation
    {
        public List<PLANET_SITE_BODY> body { get; set; } = null!;

        [ContainerLength(3)]
        [Equals("planets", OffsetIndex:0)]
        [Equals("sites", OffsetIndex:2)]
        public List<string> path { get; set; } = null!;
    }

    public class PLANET_SITE_BODY : IValidation
    {
        [APEXID]
        public string planetId { get; set; } = null!;

        [APEXID]
        public string ownerId { get; set; } = null!;

        public PLANET_SITE_ENTITY entity { get; set; } = null!;

        public string type { get; set; } = null!;

        public int plot { get; set; }

        [APEXID]
        public string id { get; set; } = null!;
    }

    public class PLANET_SITE_ENTITY : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(32)]
        public string name { get; set; } = null!;

        [StringLength(4)]
        public string? code { get; set; }

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;

        [NaturalID]
        public string? naturalId { get; set; } // This is set for planetary projects
    }
}
#pragma warning restore 1591