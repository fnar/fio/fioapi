﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Planet
{
    public class PLANET_COGC_HISTORY : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PLANET_COGC_HISTORY_PAYLOAD payload { get; set; } = null!;
    }

    public class PLANET_COGC_HISTORY_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PLANET_COGC_HISTORY_MESSAGE message { get; set; } = null!;
    }

    public class PLANET_COGC_HISTORY_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PLANET_COGC_HISTORY_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PLANET_COGC_HISTORY_PAYLOAD_INNER : IValidation
    {
        public PLANET_COGC_HISTORY_BODY body { get; set; } = null!;

        [ContainerLength(4)]
        [Equals("planets", OffsetIndex:0)]
        [Equals("cogc", OffsetIndex:2)]
        public List<string> path { get; set; } = null!;
    }

    public class PLANET_COGC_HISTORY_BODY : IValidation
    {
        public APEX_COMMON_ADDRESS_ENTITY planet { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP completionDate { get; set; } = null!;

        public string status { get; set; } = null!;

        public List<PLANET_COGC_PROGRAM> programs { get; set; } = null!;

        public List<PLANET_COGC_VOTE> votes { get; set; } = null!;

        public List<string> programTypes { get; set; } = null!;

        public PLANET_COGC_UPKEEP upkeep { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;
    }

    public class PLANET_COGC_UPKEEP : IValidation
    {
        public List<PLANET_COGC_BILL_OF_MATERIAL> billOfMaterial { get; set; } = null!;

        public List<PLANET_COGC_CONTRIBUTION> contributions { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP dueDate { get; set; } = null!;
    }

    public class PLANET_COGC_BILL_OF_MATERIAL : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int amount { get; set; }

        [Range(0, int.MaxValue)]
        public int currentAmount { get; set; }
    }

    public class PLANET_COGC_CONTRIBUTION : IValidation
    {
        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> materials { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE contributor { get; set; } = null!;
    }

    public class PLANET_COGC_PROGRAM : IValidation
    {
        public string? type { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP start { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP end { get; set; } = null!;
    }

    public class PLANET_COGC_VOTE : IValidation
    {
        public APEX_COMMON_USER_AND_CODE voter { get; set; } = null!;

        public double influence { get; set; }

        public string type { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;
    }
}
#pragma warning restore 1591