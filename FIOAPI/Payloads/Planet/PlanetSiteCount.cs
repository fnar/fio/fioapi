﻿namespace FIOAPI.Payloads.Planet
{
    /// <summary>
    /// PlanetSiteCount
    /// </summary>
    public class PlanetSiteCount
    {
        /// <summary>
        /// PlanetId
        /// </summary>
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// Counter of sites
        /// </summary>
        public int Count { get; set; } = 0;
    }
}
