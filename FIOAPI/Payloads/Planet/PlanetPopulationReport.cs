﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Planet
{
    public class PLANET_POPULATION_REPORT : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PLANET_POPULATION_REPORT_PAYLOAD payload { get; set; } = null!;
    }

    public class PLANET_POPULATION_REPORT_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PLANET_POPULATION_REPORT_MESSAGE message { get; set; } = null!;
    }

    public class PLANET_POPULATION_REPORT_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PLANET_POPULATION_REPORT_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PLANET_POPULATION_REPORT_PAYLOAD_INNER : IValidation
    {
        public PLANET_POPULATION_REPORT_BODY body { get; set; } = null!;

        [ContainerLength(2)]
        [Equals("populations", OffsetIndex:0)]
        public List<string> path { get; set; } = null!;
    }

    public class PLANET_POPULATION_REPORT_BODY : IValidation
    {
        public List<PLANET_POPULATION_REPORT_REPORT> reports { get; set; } = null!;

        public List<PLANET_POPULATION_REPORT_INFRASTRUCTURE> infrastructure { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;
    }

    public class PLANET_POPULATION_REPORT_REPORT : IValidation
    {
        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int simulationPeriod { get; set; }

        public bool explorersGraceEnabled { get; set; }

        public PLANET_POPULATION_REPORT_NEXT_POPULATION nextPopulation { get; set; } = null!;

        public PLANET_POPULATION_REPORT_POPULATION_DIFFERENCE populationDifference { get; set; } = null!;

        public PLANET_POPULATION_REPORT_POPULATION_CHANGE populationChange { get; set; } = null!;

        public PLANET_POPULATION_REPORT_POPULATION_SHIFT populationShift { get; set; } = null!;

        public PLANET_POPULATION_REPORT_AVERAGE_HAPPINESS averageHappiness { get; set; } = null!;

        public PLANET_POPULATION_REPORT_UNEMPLOYMENT_RATE unemploymentRate { get; set; } = null!;

        public PLANET_POPULATION_REPORT_OPEN_JOBS openJobs { get; set; } = null!;

        public PLANET_POPULATION_REPORT_NEED_FULFILLMENT needFulfillment { get; set; } = null!;

        public List<PLANET_POPULATION_REPORT_PER_PROJECT_NEED_FULFILLMENT> needFulfillments { get; set; } = null!;

        [StringLength(64)]
        public string? governmentProgramType { get; set; } = null!;
    }

    public class PLANET_POPULATION_REPORT_NEXT_POPULATION : IValidation
    {
        [Range(0, int.MaxValue)]
        public int PIONEER { get; set; }

        [Range(0, int.MaxValue)]
        public int SETTLER { get; set; }

        [Range(0, int.MaxValue)]
        public int TECHNICIAN { get; set; }

        [Range(0, int.MaxValue)]
        public int ENGINEER { get; set; }

        [Range(0, int.MaxValue)]
        public int SCIENTIST { get; set; }
    }

    public class PLANET_POPULATION_REPORT_POPULATION_DIFFERENCE : IValidation
    {
        public int PIONEER { get; set; }

        public int SETTLER { get; set; }

        public int TECHNICIAN { get; set; }

        public int ENGINEER { get; set; }

        public int SCIENTIST { get; set; }
    }

    public class PLANET_POPULATION_REPORT_POPULATION_CHANGE : IValidation
    {
        public int PIONEER { get; set; }

        public int SETTLER { get; set; }

        public int TECHNICIAN { get; set; }

        public int ENGINEER { get; set; }

        public int SCIENTIST { get; set; }
    }

    public class PLANET_POPULATION_REPORT_POPULATION_SHIFT : IValidation
    {
        public int PIONEER { get; set; }

        public int SETTLER { get; set; }

        public int TECHNICIAN { get; set; }

        public int ENGINEER { get; set; }

        public int SCIENTIST { get; set; }
    }

    public class PLANET_POPULATION_REPORT_AVERAGE_HAPPINESS : IValidation
    {
        [Range(0.0, 1.0)]
        public double PIONEER { get; set; }

        [Range(0.0, 1.0)]
        public double SETTLER { get; set; }

        [Range(0.0, 1.0)]
        public double TECHNICIAN { get; set; }

        [Range(0.0, 1.0)]
        public double ENGINEER { get; set; }

        [Range(0.0, 1.0)]
        public double SCIENTIST { get; set; }
    }

    public class PLANET_POPULATION_REPORT_UNEMPLOYMENT_RATE : IValidation
    {
        [Range(0.0, 1.0)]
        public double PIONEER { get; set; }

        [Range(0.0, 1.0)]
        public double SETTLER { get; set; }

        [Range(0.0, 1.0)]
        public double TECHNICIAN { get; set; }

        [Range(0.0, 1.0)]
        public double ENGINEER { get; set; }

        [Range(0.0, 1.0)]
        public double SCIENTIST { get; set; }
    }

    public class PLANET_POPULATION_REPORT_OPEN_JOBS : IValidation
    {
        [Range(0, int.MaxValue)]
        public int PIONEER { get; set; }

        [Range(0, int.MaxValue)]
        public int SETTLER { get; set; }

        [Range(0, int.MaxValue)]
        public int TECHNICIAN { get; set; }

        [Range(0, int.MaxValue)]
        public int ENGINEER { get; set; }

        [Range(0, int.MaxValue)]
        public int SCIENTIST { get; set; }
    }

    public class PLANET_POPULATION_REPORT_NEED_FULFILLMENT : IValidation
    {
        [Range(0.0, 1.0)]
        public double LIFE_SUPPORT { get; set; }

        [Range(0.0, 1.0)]
        public double SAFETY { get; set; }

        [Range(0.0, 1.0)]
        public double HEALTH { get; set; }

        [Range(0.0, 1.0)]
        public double COMFORT { get; set; }

        [Range(0.0, 1.0)]
        public double CULTURE { get; set; }

        [Range(0.0, 1.0)]
        public double EDUCATION { get; set; }
    }

    public class PLANET_POPULATION_REPORT_PER_PROJECT_NEED_FULFILLMENT : IValidation
    {
        [StringLength(64)]
        public string infrastructureType { get; set; } = null!;

        [StringLength(64)]
        public string needType { get; set; } = null!;

        [Range(0.0, 1.0)]
        public double fulfillment { get; set; }
    }

    public class PLANET_POPULATION_REPORT_INFRASTRUCTURE : IValidation
    {
        [StringLength(64)]
        public string type { get; set; } = null!;

        [Ticker]
        public string ticker { get; set; } = null!;

        [APEXID]
        public string projectId { get; set; } = null!;

        [StringLength(64)]
        public string projectName { get; set; } = null!;

        [Range(0, 10)]
        public int level { get; set; }

        [Range(-1, 10)]
        public int activeLevel { get; set; }

        [Range(0, 10)]
        public int currentLevel { get; set; }

        [Range(0.0, 1.0)]
        public double upkeepStatus { get; set; }

        [Range(0.0, 1.0)]
        public double upgradeStatus { get; set; }
    }
}
#pragma warning restore 1591