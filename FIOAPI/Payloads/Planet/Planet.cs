﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Planet
{
    public class PATH_PLANETS : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PATH_PLANETS_PAYLOAD payload { get; set; } = null!;
    }

    public class PATH_PLANETS_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PATH_PLANETS_MESSAGE message { get; set; } = null!;
    }

    public class PATH_PLANETS_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PATH_PLANETS_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PATH_PLANETS_PAYLOAD_INNER : IValidation
    {
        public PATH_PLANETS_BODY body { get; set; } = null!;

        [ContainerLength(2)]
        [Equals("planets", OffsetIndex:0)]
        public List<string> path { get; set; } = null!;
    }

    public class PATH_PLANETS_BODY : IValidation
    {
        [APEXID]
        public string planetId { get; set; } = null!;

        [NaturalID]
        public string naturalId { get; set; } = null!;

        [StringLength(64, MinimumLength = 1)]
        public string? name { get; set; } = null;

        public APEX_COMMON_USER? namer { get; set; } = null;

        public APEX_COMMON_TIMESTAMP? namingDate { get; set; } = null;

        public bool nameable { get; set; }

        public List<object> celestialBodies { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public PATH_PLANETS_DATA data { get; set; } = null!;

        [APEXID]
        public string? adminCenterId { get; set; }

        public PATH_PLANETS_BUILD_OPTIONS buildOptions { get; set; } = null!;

        public List<PATH_PLANETS_PROJECT> projects { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE? country { get; set; } = null;

        [APEXID]
        public string? governingEntity { get; set; }

        public PATH_PLANETS_LOCAL_RULES localRules { get; set; } = null!;

        [APEXID]
        public string populationId { get; set; } = null!;

        [APEXID]
        public string cogcId { get; set; } = null!;

        public bool supportsGhostSites { get; set; }

        [APEXID]
        public string id { get; set; } = null!;
    }
    public class PATH_PLANETS_DATA : IValidation
    {
        [PositiveNumber]
        public double gravity { get; set; }

        [PositiveNumber]
        public double magneticField { get; set; }

        [PositiveNumber]
        public double mass { get; set; }

        [PositiveNumber]
        public double massEarth { get; set; }

        public PATH_PLANETS_ORBIT orbit { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int orbitIndex { get; set; }

        [PositiveNumber]
        public double pressure { get; set; }

        [PositiveNumber]
        public double radiation { get; set; }

        [PositiveNumber]
        public double radius { get; set; }

        public List<PATH_PLANETS_RESOURCE> resources { get; set; } = null!;

        [PositiveNumber]
        public double sunlight { get; set; }

        public bool surface { get; set; }

        public double temperature { get; set; }

        [PositiveNumber]
        public int plots { get; set; }

        [Range(-1.0, 1.0)]
        public double? fertility { get; set; }
    }

    public class PATH_PLANETS_ORBIT : IValidation
    {
        public double semiMajorAxis { get; set; }

        public double eccentricity { get; set; }

        public double inclination { get; set; }

        public double rightAscension { get; set; }

        public double periapsis { get; set; }
    }

    public class PATH_PLANETS_RESOURCE : IValidation
    {
        [APEXID]
        public string materialId { get; set; } = null!;

        [ResourceType]
        public string type { get; set; } = null!;

        [Range(0.00001, 1)]
        public double factor { get; set; }
    }

    public class PATH_PLANETS_BUILD_OPTIONS : IValidation
    {
        [NotEmpty]
        public List<PATH_PLANETS_BUILD_OPTION> options { get; set; } = null!;
    }

    public class PATH_PLANETS_BUILD_OPTION : IValidation
    {
        public string siteType { get; set; } = null!;

        [APEXID]
        public string? feeReceiver { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? costs { get; set; }

        public PATH_PLANETS_BILL_OF_MATERIAL billOfMaterial { get; set; } = null!;
    }

    public class PATH_PLANETS_BILL_OF_MATERIAL : IValidation
    {
        [NotEmpty]
        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> quantities { get; set; } = null!;
    }

    public class PATH_PLANETS_COLLECTOR : IValidation
    {
        [APEXID]
        public string _proxy_key { get; set; } = null!;

        public string _type { get; set; } = null!;

        public APEX_COMMON_CURRENCY? currency { get; set; }
    }

    public class PATH_PLANETS_LOCAL_RULES : IValidation
    {
        public APEX_COMMON_CURRENCY? currency { get; set; }

        public PATH_PLANETS_COLLECTOR? collector { get; set; }

        public PATH_PLANETS_PRODUCTION_FEES? productionFees { get; set; }

        [NotEmpty]
        public List<int> productionFeeLimitFactors { get; set; } = null!;

        public PATH_PLANETS_LOCAL_MARKET_FEE localMarketFee { get; set; } = null!;

        public PATH_PLANETS_ESTABLISHMENT_FEE? siteEstablishmentFee { get; set; }

        public PATH_PLANETS_WAREHOUSE_FEE warehouseFee { get; set; } = null!;
    }

    public class PATH_PLANETS_PRODUCTION_FEES : IValidation
    {
        [NotEmpty]
        public List<PATH_PLANETS_FEE> fees { get; set; } = null!;
    }

    public class PATH_PLANETS_FEE : IValidation
    {
        [BuildingCategory]
        public string category { get; set; } = null!;

        [WorkforceLevel]
        public string workforceLevel { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY? fee { get; set; }
    }

    public class PATH_PLANETS_LOCAL_MARKET_FEE : IValidation
    {
        public int @base { get; set; }

        public int timeFactor { get; set; }
    }

    public class PATH_PLANETS_ESTABLISHMENT_FEE : IValidation
    {
        public int fee { get; set; }
    }

    public class PATH_PLANETS_WAREHOUSE_FEE : IValidation
    {
        public int fee { get; set; }
    }

    public class PATH_PLANETS_PROJECT : IValidation
    {
        public string type { get; set; } = null!;

        public string entityId { get; set; } = null!;
    }

}
#pragma warning restore 1591