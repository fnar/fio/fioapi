﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.LocalMarket
{
    public class PATH_LOCALMARKET_AD : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PATH_LOCALMARKET_AD_PAYLOAD payload { get; set; } = null!;
    }

    public class PATH_LOCALMARKET_AD_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PATH_LOCALMARKET_AD_MESSAGE message { get; set; } = null!;
    }

    public class PATH_LOCALMARKET_AD_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PATH_LOCALMARKET_AD_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PATH_LOCALMARKET_AD_PAYLOAD_INNER : IValidation
    {
        public List<PATH_LOCALMARKET_AD_BODY> body { get; set; } = null!;

        [ContainerLength(3)]
        [Equals("localmarkets", OffsetIndex:0)]
        [Equals("ads", OffsetIndex:2)]
        public List<string> path { get; set; } = null!;
    }

    public class PATH_LOCALMARKET_AD_BODY : IValidation
    {
        public APEX_COMMON_ADDRESS? origin { get; set; } = null!;

        public APEX_COMMON_ADDRESS? destination { get; set; } = null!;

        [Range(0.00001, 100000)]
        public double? cargoWeight { get; set; }

        [Range(0.00001, 100000)]
        public double? cargoVolume { get; set; }

        [APEXID]
        public string localMarketId { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int naturalId { get; set; }

        [Uppercase]
        [StringLength(64, MinimumLength = 3)]
        public string status { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE creator { get; set; } = null!;

        [Uppercase]
        [StringLength(20, MinimumLength = 3)]
        public string type { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public APEX_COMMON_MATERIAL_AND_AMOUNT? quantity { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY price { get; set; } = null!;

        [Range(1, 100)]
        public int advice { get; set; } // @note: Fulfill within {advice} days

        public APEX_COMMON_TIMESTAMP creationTime { get; set; } = null!;

        [StringLength(7, MinimumLength = 1)]
        public string minimumRating { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP expiry { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;

        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!Constants.ValidLocalMarketTypes.Values.Contains(type))
            {
                Errors.Add($"'{Context}' has an invalid type '{type}' specified.");
            }

            if (!Constants.ValidRatings.Contains(minimumRating))
            {
                Errors.Add($"'{Context}' has an invalid rating '{minimumRating}' specified.");
            }
        }
    }
}
#pragma warning restore 1591