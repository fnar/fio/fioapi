﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.User
{
    public class ROOT_PRESENCE_LIST : IValidation
    {
        [Equals("PRESENCE_LIST")]
        public string messageType { get; set; } = null!;

        public ROOT_PRESENCE_LIST_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_PRESENCE_LIST_PAYLOAD : IValidation
    {
        public List<ROOT_PRESENCE_LIST_USER> users { get; set; } = null!;
    }

    public class ROOT_PRESENCE_LIST_USER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string username { get; set; } = null!;

        public long time { get; set; }
    }
}
#pragma warning restore 1591