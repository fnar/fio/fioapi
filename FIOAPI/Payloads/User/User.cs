﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.User
{
    public class PATH_USERS : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PATH_USERS_PAYLOAD payload { get; set; } = null!;
    }

    public class PATH_USERS_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }
        
        public PATH_USERS_MESSAGE message { get; set; } = null!;
    }

    public class PATH_USERS_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PATH_USERS_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PATH_USERS_PAYLOAD_INNER : IValidation
    {
        public PATH_USERS_BODY body { get; set; } = null!;

        [ContainerLength(2)]
        [Equals("users", OffsetIndex:0)]
        public string[] path { get; set; } = null!;
    }

    public class PATH_USERS_BODY : IValidation
    {
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string username { get; set; } = null!;

        [StringLength(5, MinimumLength = 3)]
        public string subscriptionLevel { get; set; } = null!;

        [StringLength(32)]
        public string? highestTier { get; set; }

        public bool pioneer { get; set; }

        public bool moderator { get; set; }

        public bool team { get; set; }

        public APEX_COMMON_TIMESTAMP created { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE? company { get; set; }

        public int activeDaysPerWeek { get; set; }

        [APEXID]
        public string id { get; set; } = null!;
    }
}
#pragma warning restore 1591