﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Sites
{
    public class ROOT_SITE_PLATFORM_BUILT : IValidation
    {
        [Equals("SITE_PLATFORM_BUILT")]
        public string messageType { get; set; } = null!;

        public ROOT_SITE_PLATFORM_BUILT_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_SITE_PLATFORM_BUILT_PAYLOAD : IValidation
    {
        [APEXID]
        public string siteId { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;

        public ROOT_SITE_PLATFORM_BUILT_MODULE module { get; set; } = null!;

        public int area { get; set; }

        public APEX_COMMON_TIMESTAMP creationTime { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> reclaimableMaterials { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> repairMaterials { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY bookValue { get; set; } = null!;

        public double condition { get; set; }

        public APEX_COMMON_TIMESTAMP? lastRepair { get; set; }
    }

    public class ROOT_SITE_PLATFORM_BUILT_MODULE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string platformId { get; set; } = null!;

        [APEXID]
        public string reactorId { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string reactorName { get; set; } = null!;

        [Ticker]
        public string reactorTicker { get; set; } = null!;

        public string type { get; set; } = null!;
    }
}
#pragma warning restore 1591
