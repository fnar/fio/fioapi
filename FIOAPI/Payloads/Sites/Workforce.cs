﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Sites
{
    public class MESG_WORKFORCE_WORKFORCES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_WORKFORCE_WORKFORCES_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_WORKFORCE_WORKFORCES_MESSAGE message { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_MESSAGE : IValidation
    {
        [Equals("WORKFORCE_WORKFORCES")]
        public string messageType { get; set; } = null!;

        public MESG_WORKFORCE_WORKFORCES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_PAYLOAD_INNER : IValidation
    {
        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string siteId { get; set; } = null!;

        public List<MESG_WORKFORCE_WORKFORCES_WORKFORCE> workforces { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_WORKFORCE : IValidation
    {
        [WorkforceLevel]
        [StringLength(10, MinimumLength = 7)]
        public string level { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int population { get; set; }

        [Range(0, int.MaxValue)]
        public int reserve { get; set; }

        [Range(0, int.MaxValue)]
        public int capacity { get; set; }

        [Range(0, int.MaxValue)]
        public int required { get; set; }

        [Range(0.0, double.MaxValue)]
        public double satisfaction { get; set; }

        public List<MESG_WORKFORCE_WORKFORCES_WORKFORCE_NEEDS> needs { get; set; } = null!;
    }

    public class MESG_WORKFORCE_WORKFORCES_WORKFORCE_NEEDS : IValidation
    {
        [StringLength(32, MinimumLength = 1)]
        public string category { get; set; } = null!;

        public bool essential { get; set; }

        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0.0, double.MaxValue)]
        public double satisfaction { get; set; }

        [Range(0.0, double.MaxValue)]
        public double unitsPerInterval { get; set; }

        [Range(0.0, double.MaxValue)]
        public double unitsPer100 { get; set; }
    }
}
#pragma warning restore 1591