﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Sites
{
    public class MESG_SITE_SITES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_SITE_SITES_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_SITE_SITES_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public ROOT_SITE_SITES message { get; set; } = null!;
    }

    public class ROOT_SITE_SITES : IValidation
    {
        [Equals("SITE_SITES")]
        public string messageType { get; set; } = null!;

        public SITE_SITES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class SITE_SITES_PAYLOAD_INNER : IValidation
    {
        public List<SITE_SITES_SITE> sites { get; set; } = null!;
    }

    public class SITE_SITES_SITE : IValidation
    {
        [APEXID]
        public string siteId { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP founded { get; set; } = null!;

        public List<SITE_SITES_PLATFORM> platforms { get; set; } = null!;

        public SITE_SITES_OPTIONS buildOptions { get; set; } = null!;

        [PositiveNumber]
        public int area { get; set; }

        [PositiveNumber]
        public int investedPermits { get; set; }

        [PositiveNumber]
        public int maximumPermits { get; set; }
    }


    public class SITE_SITES_OPTIONS : IValidation
    {
        [NotEmpty]
        public List<SITE_SITES_OPTION> options { get; set; } = null!;
    }

    public class SITE_SITES_OPTION : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        [PositiveNumber]
        public int area { get; set; }

        [Ticker]
        public string ticker { get; set; } = null!;

        [BuildingCategory]
        public string? expertiseCategory { get; set; } = null!;

        public bool needsFertileSoil { get; set; }

        [StringLength(32)]
        public string type { get; set; } = null!;

        public List<SITE_SITES_WORKFORCE_CAPACITY> workforceCapacities { get; set; } = null!;

        public SITE_SITES_OPTION_MATERIALS materials { get; set; } = null!;
    }

    public class SITE_SITES_OPTION_MATERIALS : IValidation
    {
        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> quantities { get; set; } = null!;
    }

    public class SITE_SITES_WORKFORCE_CAPACITY : IValidation
    {
        [WorkforceLevel]
        public string level { get; set; } = null!;

        [PositiveNumber]
        public int capacity { get; set; }
    }

    public class SITE_SITES_PLATFORM : IValidation
    {
        [APEXID]
        public string siteId { get; set; } = null!; // The id for the site (all buildings on a site share same siteId)

        [APEXID]
        public string id { get; set; } = null!; // Platform id

        public SITE_SITES_PLATFORM_MODULE module { get; set; } = null!;

        [PositiveNumber]
        public int area { get; set; }

        public APEX_COMMON_TIMESTAMP creationTime { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> reclaimableMaterials { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> repairMaterials { get; set; } = null!;

        public SITE_SITES_BOOK_VALUE bookValue { get; set; } = null!;

        [Range(0.0, 1.0)]
        public float condition { get; set; }

        public APEX_COMMON_TIMESTAMP? lastRepair { get; set; }
    }

    public class SITE_SITES_PLATFORM_MODULE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!; // Instance building id

        [APEXID]
        public string platformId { get; set; } = null!; // Points to SITE_SITES_PLATFORM.id (parent)

        [APEXID]
        public string reactorId { get; set; } = null!;  // BuildingId

        [StringLength(64, MinimumLength = 3)]
        public string reactorName { get; set; } = null!;

        [Ticker]
        public string reactorTicker { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string type { get; set; } = null!;
    }

    public class SITE_SITES_BOOK_VALUE : IValidation
    {
        public double amount { get; set; }

        [CurrencyCode]
        public string currency { get; set; } = null!;
    }
}
#pragma warning restore 1591