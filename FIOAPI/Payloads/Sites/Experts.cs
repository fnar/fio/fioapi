﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Sites
{
    public class MESG_EXPERTS_EXPERTS : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_EXPERTS_EXPERTS_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_EXPERTS_EXPERTS_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_EXPERTS_EXPERTS_MESSAGE message { get; set; } = null!;
    }

    public class MESG_EXPERTS_EXPERTS_MESSAGE : IValidation
    {
        [Equals("EXPERTS_EXPERTS")]
        public string messageType { get; set; } = null!;

        public MESG_EXPERTS_EXPERTS_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_EXPERTS_EXPERTS_PAYLOAD_INNER : IValidation
    {
        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string siteId { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        [Range(0, 100)]
        public int total { get; set; }

        [Range(0, 5)]
        public int active { get; set; }

        [Range(0, 6)]
        public int totalActiveCap { get; set; }

        public List<MESG_EXPERTS_EXPERTS_EXPERT> experts { get; set; } = null!;
    }

    public class MESG_EXPERTS_EXPERTS_EXPERT : IValidation
    {
        [BuildingCategory]
        [StringLength(19, MinimumLength = 9)]
        public string category { get; set; } = null!;

        public MESG_EXPERTS_EXPERTS_EXPERT_ENTRY entry { get; set; } = null!;
    }

    public class MESG_EXPERTS_EXPERTS_EXPERT_ENTRY : IValidation
    {
        [BuildingCategory]
        [StringLength(19, MinimumLength = 9)]
        public string category { get; set; } = null!;

        [Range(0, 5)]
        public int current { get; set; }

        [Range(0, 5)]
        public int limit { get; set; }

        [Range(0, 5)]
        public int available { get; set; }

        [Range(0.0, 29.0)]
        public double efficiencyGain { get; set; }
    }
}
#pragma warning restore 1591