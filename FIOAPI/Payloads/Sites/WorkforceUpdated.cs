﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Sites
{
    public class ROOT_WORKFORCE_WORKFORCES_UPDATED : IValidation
    {
        [Equals("WORKFORCE_WORKFORCES_UPDATED")]
        public string messageType { get; set; } = null!;

        public ROOT_WORKFORCE_WORKFORCES_UPDATED_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_WORKFORCE_WORKFORCES_UPDATED_PAYLOAD : IValidation
    {
        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        [APEXID]
        public string siteId { get; set; } = null!;

        public List<MESG_WORKFORCE_WORKFORCES_WORKFORCE> workforces { get; set; } = null!;
    }
}
#pragma warning restore 1591