﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Sites
{
    public class MESG_SITE_PLATFORM_REMOVED : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_SITE_PLATFORM_REMOVED_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_SITE_PLATFORM_REMOVED_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public ROOT_SITE_PLATFORM_REMOVED message { get; set; } = null!;
    }

    public class ROOT_SITE_PLATFORM_REMOVED : IValidation
    {
        [Equals("SITE_PLATFORM_REMOVED")]
        public string messageType { get; set; } = null!;

        public ROOT_SITE_PLATFORM_REMOVED_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_SITE_PLATFORM_REMOVED_PAYLOAD : IValidation
    {
        [APEXID]
        public string siteId { get; set; } = null!;

        [APEXID]
        public string platformId { get; set; } = null!;
    }
}
#pragma warning restore 1591