﻿namespace FIOAPI.Payloads.Permission
{
    /// <summary>
    /// Response representing a Permission Model
    /// </summary>
    /// <remarks>This doesn't perfectly match the DB Model on purpose</remarks>
    public class PermissionResponse : IValidation
    {
        /// <summary>
        /// The grantor of permissions
        /// </summary>
        public string GrantorUserName { get; set; } = "";

        /// <summary>
        /// The grantee of permissions
        /// </summary>
        public string GranteeUserName { get; set; } = "";

        /// <summary>
        /// GroupId of the permission
        /// </summary>
        /// <remarks>This is 0 if it's not a Group permission</remarks>
        public int GroupId { get; set; }

        /// <summary>
        /// The permissions object definition
        /// </summary>
        public Permissions Permissions { get; set; } = new Permissions();

        /// <summary>
        /// Constructos a PermissionResponse from a DB Permission obj
        /// </summary>
        /// <param name="perm">DB permission obj</param>
        /// <returns>PermissionResponse</returns>
        public static PermissionResponse FromDBPermission(DB.Model.Permission perm)
        {
            return new PermissionResponse
            {
                GrantorUserName = perm.GrantorUserName,
                GranteeUserName = perm.GranteeUserName,
                GroupId = perm.GroupId,
                Permissions = Permissions.FromDBPermission(perm)
            };
        }

        /// <summary>
        /// Custom validation
        /// </summary>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="Context">The context where this validation is being run</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);
        }
    }
}
