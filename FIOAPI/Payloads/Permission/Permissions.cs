﻿namespace FIOAPI.Payloads.Permission
{
    /// <summary>
    /// Ship Permissions
    /// </summary>
    public class ShipPermissions : IValidation
    {
        /// <summary>
        /// Information such as Ship name, registration, specs (acceleration, mass, etc)
        /// </summary>
        [DefaultValue(true)]
        public bool Information { get; set; } = true;

        /// <summary>
        /// Information such as ship condition, last repair, and materials needed to repair
        /// </summary>
        [DefaultValue(true)]
        public bool Repair { get; set; } = true;

        /// <summary>
        /// Ship flight information
        /// </summary>
        [DefaultValue(true)]
        public bool Flight { get; set; } = true;

        /// <summary>
        /// Ship Inventory
        /// </summary>
        [DefaultValue(true)]
        public bool Inventory { get; set; } = true;

        /// <summary>
        /// Ship fuel inventory
        /// </summary>
        [DefaultValue(true)]
        public bool FuelInventory { get; set; } = true;
    }

    /// <summary>
    /// Site Permissions
    /// </summary>
    public class SitePermissions : IValidation
    {
        /// <summary>
        /// Where your sites are located
        /// </summary>
        [DefaultValue(true)]
        public bool Location { get; set; } = true;

        /// <summary>
        /// Workforce data (worker counts and capacity)
        /// </summary>
        [DefaultValue(true)]
        public bool Workforces { get; set; } = true;

        /// <summary>
        /// Experts
        /// </summary>
        [DefaultValue(true)]
        public bool Experts { get; set; } = true;

        /// <summary>
        /// The general building information (effectively, building ticker)
        /// </summary>
        [DefaultValue(true)]
        public bool Buildings { get; set; } = true;

        /// <summary>
        /// Repair costs
        /// </summary>
        [DefaultValue(true)]
        public bool Repair { get; set; } = true;

        /// <summary>
        /// Reclaimables
        /// </summary>
        [DefaultValue(true)]
        public bool Reclaimable { get; set; } = true;

        /// <summary>
        /// Production line information. This includes all active and pending orders
        /// </summary>
        [DefaultValue(true)]
        public bool ProductionLines { get; set; } = true;
    }

    /// <summary>
    /// Storage permissions
    /// </summary>
    public class StoragePermissions : IValidation
    {
        /// <summary>
        /// The location of all your storage (inventory and warehouse)
        /// </summary>
        [DefaultValue(true)]
        public bool Location { get; set; } = true;

        /// <summary>
        /// Information about the storage (Weight/Volume capacity, warehouse or base storage, etc)
        /// </summary>
        [DefaultValue(true)]
        public bool Information { get; set; } = true;

        /// <summary>
        /// Inventory items. This also implies actual weight/volume permissions
        /// </summary>
        [DefaultValue(true)]
        public bool Items { get; set; } = true;
    }

    /// <summary>
    /// Trade permissions
    /// </summary>
    public class TradePermissions : IValidation
    {
        /// <summary>
        /// Contract information
        /// </summary>
        [DefaultValue(true)]
        public bool Contract { get; set; } = true;

        /// <summary>
        /// CXOS information
        /// </summary>
        [DefaultValue(true)]
        public bool CXOS { get; set; } = true;
    }

    /// <summary>
    /// Misc permissions
    /// </summary>
    public class MiscPermissions : IValidation
    {
        /// <summary>
        /// If shipment tracking is allowed
        /// </summary>
        [DefaultValue(true)]
        public bool ShipmentTracking { get; set; } = true;
    }

    /// <summary>
    /// Company permissions
    /// </summary>
    public class CompanyPermissions : IValidation
    {
        /// <summary>
        /// Info
        /// </summary>
        [DefaultValue(true)]
        public bool Info { get; set; } = true;

        /// <summary>
        /// LiquidCurrency
        /// </summary>
        [DefaultValue(true)]
        public bool LiquidCurrency { get; set; } = true;

        /// <summary>
        /// Headquarters
        /// </summary>
        [DefaultValue(true)]
        public bool Headquarters { get; set; } = true;
    }

    /// <summary>
    /// Permissions
    /// </summary>
    public class Permissions : IValidation
    {
        /// <summary>
        /// ShipPermissions
        /// </summary>
        public ShipPermissions ShipPermissions { get; set; } = new();

        /// <summary>
        /// SitePermissions
        /// </summary>
        public SitePermissions SitesPermissions { get; set; } = new();

        /// <summary>
        /// StoragePermissions
        /// </summary>
        public StoragePermissions StoragePermissions { get; set; } = new();

        /// <summary>
        /// TradePermissions
        /// </summary>
        public TradePermissions TradePermissions { get; set; } = new();

        /// <summary>
        /// CompanyPermissions
        /// </summary>
        public CompanyPermissions CompanyPermissions { get; set; } = new();

        /// <summary>
        /// MiscPermissions
        /// </summary>
        public MiscPermissions MiscPermissions { get; set; } = new();

        /// <summary>
        /// Converts this to a DB Permission object
        /// </summary>
        /// <returns>A new DB Permission object</returns>
        public Model.Permission ToDBPermission()
        {
            return new Model.Permission
            {
                ShipInformation = ShipPermissions.Information,
                ShipRepair = ShipPermissions.Repair,
                ShipFlight = ShipPermissions.Flight,
                ShipInventory = ShipPermissions.Inventory,
                ShipFuelInventory = ShipPermissions.FuelInventory,
                SitesLocation = SitesPermissions.Location,
                SitesWorkforces = SitesPermissions.Workforces,
                SitesExperts = SitesPermissions.Experts,
                SitesBuildings = SitesPermissions.Buildings,
                SitesRepair = SitesPermissions.Repair,
                SitesReclaimable = SitesPermissions.Reclaimable,
                SitesProductionLines = SitesPermissions.ProductionLines,
                StorageLocation = StoragePermissions.Location,
                StorageInformation = StoragePermissions.Information,
                StorageItems = StoragePermissions.Items,
                TradeContract = TradePermissions.Contract,
                TradeCXOS = TradePermissions.CXOS,
                CompanyInfo = CompanyPermissions.Info,
                CompanyLiquidCurrency = CompanyPermissions.LiquidCurrency,
                CompanyHeadquarters = CompanyPermissions.Headquarters,
                MiscShipmentTracking = MiscPermissions.ShipmentTracking,
            };
        }

        /// <summary>
        /// Constructs a new Permissions object from a DB Permission
        /// </summary>
        /// <param name="dbPerm"></param>
        /// <returns></returns>
        public static Permissions FromDBPermission(Model.Permission dbPerm)
        {
            var perm = new Permissions();
            perm.ShipPermissions.Information = dbPerm.ShipInformation;
            perm.ShipPermissions.Repair = dbPerm.ShipRepair;
            perm.ShipPermissions.Flight = dbPerm.ShipFlight;
            perm.ShipPermissions.Inventory = dbPerm.ShipInventory;
            perm.ShipPermissions.FuelInventory = dbPerm.ShipFuelInventory;

            perm.SitesPermissions.Location = dbPerm.SitesLocation;
            perm.SitesPermissions.Workforces = dbPerm.SitesWorkforces;
            perm.SitesPermissions.Experts = dbPerm.SitesExperts;
            perm.SitesPermissions.Buildings = dbPerm.SitesBuildings;
            perm.SitesPermissions.Repair = dbPerm.SitesRepair;
            perm.SitesPermissions.Reclaimable = dbPerm.SitesReclaimable;
            perm.SitesPermissions.ProductionLines = dbPerm.SitesProductionLines;

            perm.StoragePermissions.Location = dbPerm.StorageLocation;
            perm.StoragePermissions.Information = dbPerm.StorageInformation;
            perm.StoragePermissions.Items = dbPerm.StorageItems;

            perm.TradePermissions.Contract = dbPerm.TradeContract;
            perm.TradePermissions.CXOS = dbPerm.TradeCXOS;

            perm.CompanyPermissions.Info = dbPerm.CompanyInfo;
            perm.CompanyPermissions.LiquidCurrency = dbPerm.CompanyLiquidCurrency;
            perm.CompanyPermissions.Headquarters = dbPerm.CompanyHeadquarters;

            perm.MiscPermissions.ShipmentTracking = dbPerm.MiscShipmentTracking;

            return perm;
        }
    }
}
