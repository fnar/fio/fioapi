﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvLMAd
    /// </summary>
    public class CsvLMAd : ICsvObject // @TODO: Shipments!
    {
        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [Csv.Index(0)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [Csv.Index(1)]
        public string PlanetName { get; set; } = null!;

        /// <summary>
        /// AdType
        /// </summary>
        [Csv.Index(2)]
        public string AdType { get; set; } = null!;

        /// <summary>
        /// AdNaturalId
        /// </summary>
        [Csv.Index(3)]
        public int AdNaturalId { get; set; }

        /// <summary>
        /// CreatorCompanyName
        /// </summary>
        [Csv.Index(4)]
        public string CreatorCompanyName { get; set; } = null!;

        /// <summary>
        /// CreatorCompanyCode
        /// </summary>
        [Csv.Index(5)]
        public string CreatorCompanyCode { get; set; } = null!;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Csv.Index(6)]
        public string? MaterialTicker { get; set; }

        /// <summary>
        /// MaterialAmount
        /// </summary>
        [Csv.Index(7)]
        public int? MaterialAmount { get; set; }

        /// <summary>
        /// Weight
        /// </summary>
        [Csv.Index(8)]
        public double? Weight { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        [Csv.Index(9)]
        public double? Volume { get; set; }

        /// <summary>
        /// OriginNaturalId
        /// </summary>
        [Csv.Index(10)]
        public string? OriginNaturalId { get; set; }

        /// <summary>
        /// DestinationNaturalId
        /// </summary>
        [Csv.Index(11)]
        public string? DestinationNaturalId { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        [Csv.Index(12)]
        public double Price { get; set; }

        /// <summary>
        /// PriceCurrency
        /// </summary>
        [Csv.Index(13)]
        public string Currency { get; set; } = null!;

        /// <summary>
        /// FulfillmentDays
        /// </summary>
        [Csv.Index(14)]
        public int FulfillmentDays { get; set; }

        /// <summary>
        /// CreationTimeEpochMs
        /// </summary>
        [Csv.Index(15)]
        public long CreationTimeEpochMs { get; set; }

        /// <summary>
        /// ExpiryTimeEpochMs
        /// </summary>
        [Csv.Index(16)]
        public long ExpiryTimeEpochMs { get; set; }

        /// <summary>
        /// MinimumRating
        /// </summary>
        [Csv.Index(17)]
        public string MinimumRating { get; set; } = null!;

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvLMAd() { }

        /// <summary>
        /// LocalMarketAd constructor
        /// </summary>
        /// <param name="localMarketAd">localMarketAd</param>
        public CsvLMAd(Model.LocalMarketAd localMarketAd)
        {
            PlanetNaturalId = localMarketAd.PlanetNaturalId;
            PlanetName = localMarketAd.PlanetName;
            AdType = localMarketAd.Type;
            AdNaturalId = localMarketAd.AdNaturalId;
            CreatorCompanyName = localMarketAd.CreatorName;
            CreatorCompanyCode = localMarketAd.CreatorCode;
            MaterialTicker = localMarketAd.MaterialTicker;
            MaterialAmount = localMarketAd.MaterialAmount;
            Weight = localMarketAd.Weight;
            Volume = localMarketAd.Volume;
            OriginNaturalId = localMarketAd.OriginNaturalId;
            DestinationNaturalId = localMarketAd.DestinationNaturalId;
            Price = localMarketAd.Price;
            Currency = localMarketAd.Currency;
            FulfillmentDays = localMarketAd.FulfillmentDays;
            CreationTimeEpochMs = localMarketAd.CreationTime.ToEpochMs();
            ExpiryTimeEpochMs = localMarketAd.Expiry.ToEpochMs();
            MinimumRating = localMarketAd.MinimumRating;
        }
    }
}
