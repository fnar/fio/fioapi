﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvBalance
    /// </summary>
    public class CsvBalance : ICsvObjectWithUserName
    {
        /// <summary>
        /// Currency
        /// </summary>
        [Csv.Index(1)]
        public string Currency { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Csv.Index(2)]
        public double? Amount { get; set; }
    }
}
