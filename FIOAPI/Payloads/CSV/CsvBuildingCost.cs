﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvBuildingCost
    /// </summary>
    public class CsvBuildingCost : ICsvObject
    {
        /// <summary>
        /// Key in the form `BUI-MAT`
        /// </summary>
        [Csv.Index(0)]
        public string Key { get; set; } = null!;

        /// <summary>
        /// Building Ticker
        /// </summary>
        [Csv.Index(1)]
        public string Building { get; set; } = null!;

        /// <summary>
        /// Material Ticker
        /// </summary>
        [Csv.Index(2)]
        public string Material { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Csv.Index(3)]
        public int Amount { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvBuildingCost() { }

        /// <summary>
        /// Building constructor
        /// </summary>
        /// <param name="buildingCost">BuildingCost</param>
        public CsvBuildingCost(Model.BuildingCost buildingCost)
        {
            Key = $"{buildingCost.Building.Ticker}-{buildingCost.Ticker}";
            Building = buildingCost.Building.Ticker;
            Material = buildingCost.Ticker;
            Amount = buildingCost.Amount;
        }
    }
}
