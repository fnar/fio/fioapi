﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvPlanet
    /// </summary>
    public class CsvPlanet : ICsvObject
    {
        /// <summary>
        /// PlanetId
        /// </summary>
        [Csv.Index(0)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [Csv.Index(1)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [Csv.Index(2)]
        public string? PlanetName { get; set; } = null!;

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvPlanet() { }

        /// <summary>
        /// Planet constructor
        /// </summary>
        /// <param name="planet"></param>
        public CsvPlanet(Model.Planet planet)
        {
            PlanetId = planet.PlanetId;
            PlanetNaturalId = planet.NaturalId;
            PlanetName = planet.Name;
        }
    }
}
