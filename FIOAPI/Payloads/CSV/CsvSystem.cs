﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvSystem
    /// </summary>
    public class CsvSystem : ICsvObject
    {
        /// <summary>
        /// Naturalid
        /// </summary>
        [Csv.Index(0)]
        public string NaturalId { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        [Csv.Index(1)]
        public string? Name { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [Csv.Index(2)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// PositionX
        /// </summary>
        [Csv.Index(3)]
        public double PositionX { get; set; }

        /// <summary>
        /// PositionY
        /// </summary>
        [Csv.Index(4)]
        public double PositionY { get; set; }

        /// <summary>
        /// PositionZ
        /// </summary>
        [Csv.Index(5)]
        public double PositionZ { get; set; }

        /// <summary>
        /// SectorId
        /// </summary>
        [Csv.Index(6)]
        public string SectorId { get; set; } = null!;

        /// <summary>
        /// SubSectorId
        /// </summary>
        [Csv.Index(7)]
        public string SubSectorId { get; set; } = null!;

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public CsvSystem() { }

        /// <summary>
        /// System Constructor
        /// </summary>
        /// <param name="system"></param>
        public CsvSystem(Model.System system)
        {
            NaturalId = system.NaturalId;
            Name = system.Name;
            Type = system.Type;
            PositionX = system.PositionX;
            PositionY = system.PositionY;
            PositionZ = system.PositionZ;
            SectorId = system.SectorId;
            SubSectorId = system.SubSectorId;
        }
    }
}
