﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvWorkforceNeed
    /// </summary>
    public class CsvWorkforceNeed : ICsvObjectWithUserName
    {
        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [Csv.Index(1)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(2)]
        public string? PlanetName { get; set; }

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Csv.Index(3)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// DailyAmount
        /// </summary>
        [Csv.Index(4)]
        public double DailyAmount { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvWorkforceNeed() { }

        /// <summary>
        /// WorkforceNeed constructor
        /// </summary>
        /// <param name="userName">userName</param>
        /// <param name="workforceNeed">workforceNeed</param>
        /// <param name="sites">sites</param>
        public CsvWorkforceNeed(string userName, Model.WorkforceNeed workforceNeed, List<Model.Site> sites)
        {
            UserName = userName;

            var site = sites.FirstOrDefault(s => s.SiteId == workforceNeed.Workforce.SiteId);

            Model.Planet? planet = null;
            if (site != null)
            {
                planet = EntityCaches.PlanetCache.Get(site.LocationNaturalId);
            }

            if (planet != null)
            {
                PlanetNaturalId = planet.NaturalId;
                PlanetName = planet.Name;
            }
            else
            {
                PlanetNaturalId = site != null ? site.LocationNaturalId : workforceNeed.Workforce.SiteId;
                PlanetName = site != null ? site.LocationName : workforceNeed.Workforce.SiteId;
            }
            
            MaterialTicker = workforceNeed.MaterialTicker;
            DailyAmount = workforceNeed.UnitsPerInterval.RoundForDisplay();
        }
    }
}
