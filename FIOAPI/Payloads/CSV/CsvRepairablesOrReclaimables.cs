﻿using FIOAPI.DB.Model;
using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvRepairablesOrReclaimables
    /// </summary>
    public class CsvRepairablesOrReclaimables : ICsvObjectWithUserName
    {
        /// <summary>
        /// LocationNaturalId
        /// </summary>
        [Csv.Index(1)]
        public string LocationNaturalId { get; set; } = null!;

        /// <summary>
        /// LocationName
        /// </summary>
        [Csv.Index(2)]
        public string? LocationName { get; set; }

        /// <summary>
        /// BuildingId
        /// </summary>
        [Csv.Index(3)]
        public string BuildingId { get; set; } = null!;

        /// <summary>
        /// BuildingTicker
        /// </summary>
        [Csv.Index(4)]
        public string BuildingTicker { get; set; } = null!;

        /// <summary>
        /// Material
        /// </summary>
        [Csv.Index(5)]
        public string Material { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Csv.Index(6)]
        public int Amount { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvRepairablesOrReclaimables() { }

        /// <summary>
        /// SiteBuilding constructor
        /// </summary>
        /// <param name="siteBuilding">siteBuilding</param>
        /// <param name="matTickerAndAmount">MatTickerAndAmount object</param>
        public CsvRepairablesOrReclaimables(SiteBuilding siteBuilding, MaterialTickerAndAmount matTickerAndAmount)
        {
            UserName = siteBuilding.Site.UserNameSubmitted;
            LocationNaturalId = siteBuilding.Site.LocationNaturalId;
            LocationName = siteBuilding.Site.LocationName;
            BuildingId = siteBuilding.SiteBuildingId;
            BuildingTicker = siteBuilding.BuildingTicker;
            Material = matTickerAndAmount.Ticker;
            Amount = matTickerAndAmount.Amount;
        }
    }
}
