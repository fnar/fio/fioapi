﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvGlobalWorkforceNeed - Non-user specific workforce needs (global)
    /// </summary>
    public class CsvGlobalWorkforceNeed : ICsvObject
    {
        /// <summary>
        /// WorkforceType
        /// </summary>
        [Csv.Index(0)]
        public string WorkforceType { get; set; } = null!;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Csv.Index(1)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// MaterialCategory
        /// </summary>
        [Csv.Index(2)]
        public string MaterialCategory { get; set; } = null!;

        /// <summary>
        /// AmountPerOneHundred
        /// </summary>
        [Csv.Index(3)]
        public double AmountPerOneHundred { get; set; }
    }
}
