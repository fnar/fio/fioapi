﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvSite
    /// </summary>
    public class CsvSite : ICsvObjectWithUserName
    {
        /// <summary>
        /// SiteId
        /// </summary>
        [Csv.Index(1)]
        public string SiteId { get; set; } = null!;

        /// <summary>
        /// LocationNaturalId
        /// </summary>
        [Csv.Index(2)]
        public string LocationNaturalId { get; set; } = null!;

        /// <summary>
        /// LocationName
        /// </summary>
        [Csv.Index(3)]
        public string? LocationName { get; set; }

        /// <summary>
        /// BuildingId
        /// </summary>
        [Csv.Index(4)]
        public string BuildingId { get; set; } = null!;

        /// <summary>
        /// BuildingTicker
        /// </summary>
        [Csv.Index(5)]
        public string BuilderTicker { get; set; } = null!;

        /// <summary>
        /// BuildingCondition
        /// </summary>
        [Csv.Index(6)]
        public double BuildingCondition { get; set; }

        /// <summary>
        /// BuildingLastRepairEpochMs
        /// </summary>
        [Csv.Index(7)]
        public long? BuildingLastRepairEpochMs { get; set; }

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public CsvSite() { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="userName">userName</param>
        /// <param name="siteBuilding">siteBuilding</param>
        public CsvSite(string userName, Model.SiteBuilding siteBuilding)
        {
            UserName = userName;
            SiteId = siteBuilding.Site.SiteId;
            LocationNaturalId = siteBuilding.Site.LocationNaturalId;
            LocationName = siteBuilding.Site.LocationName;
            BuildingId = siteBuilding.SiteBuildingId;
            BuildingCondition = siteBuilding.Condition;
            BuildingLastRepairEpochMs = siteBuilding.LastRepair != null ? ((DateTime)siteBuilding.LastRepair).ToEpochMs() : siteBuilding.CreationTime.ToEpochMs();
        }
    }
}
