﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvBuilding
    /// </summary>
    public class CsvBuilding : ICsvObject
    {
        /// <summary>
        /// Building ticker
        /// </summary>
        [Csv.Index(0)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        [Csv.Index(1)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Area
        /// </summary>
        [Csv.Index(2)]
        public int Area { get; set; }
        
        /// <summary>
        /// Expertise
        /// </summary>
        [Csv.Index(3)]
        public string? Expertise { get; set; } = null!;

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public CsvBuilding() { }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="building">building</param>
        public CsvBuilding(Model.Building building)
        {
            Ticker = building.Ticker;
            Name = building.Name;
            Area = building.AreaCost;
            Expertise = building.Expertise;
        }
    }
}
