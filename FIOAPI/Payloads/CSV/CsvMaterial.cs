﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvMaterial
    /// </summary>
    public class CsvMaterial : ICsvObject
    {
        /// <summary>
        /// Ticker
        /// </summary>
        [Csv.Index(0)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        [Csv.Index(1)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Weight
        /// </summary>
        [Csv.Index(2)]
        public double Weight { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        [Csv.Index(3)] 
        public double Volume { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvMaterial() { }

        /// <summary>
        /// Material constructor
        /// </summary>
        /// <param name="material">material</param>
        public CsvMaterial(Model.Material material)
        {
            Ticker = material.Ticker;
            Name = material.Name;
            Weight = material.Weight;
            Volume = material.Volume;
        }
    }
}
