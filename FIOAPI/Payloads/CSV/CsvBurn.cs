﻿using FIOAPI.Payloads.Data;
using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvBurn
    /// </summary>
    public class CsvBurn : ICsvObjectWithUserName
    {
        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [Csv.Index(1)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [Csv.Index(2)]
        public string? PlanetName { get; set; }

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Csv.Index(3)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// DailyWorkforceConsumption
        /// </summary>
        [Csv.Index(4)]
        public double DailyWorkforceConsumption { get; set; }

        /// <summary>
        /// DailyOrderConsumption
        /// </summary>
        [Csv.Index(5)]
        public double DailyOrderConsumption { get; set; }

        /// <summary>
        /// DailyOrderProduction
        /// </summary>
        [Csv.Index(6)]
        public double DailyOrderProduction { get; set; }

        /// <summary>
        /// OverallDelta
        /// </summary>
        [Csv.Index(7)]
        public double OverallDelta { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvBurn() { }

        /// <summary>
        /// CsvBurn Burn data constructor
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="burn"></param>
        /// <param name="burnItem"></param>
        public CsvBurn(string userName, Burn burn, BurnItem burnItem)
        {
            UserName = userName;
            PlanetNaturalId = burn.PlanetNaturalId;
            PlanetName = burn.PlanetName;
            MaterialTicker = burnItem.MaterialTicker;
            DailyWorkforceConsumption = burnItem.WorkforceConsumption;
            DailyOrderConsumption = burnItem.OrderConsumption;
            DailyOrderProduction = burnItem.OrderProduction;
            OverallDelta = burnItem.OverallDelta;
        }
    }
}
