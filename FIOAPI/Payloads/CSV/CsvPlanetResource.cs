﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvPlanet
    /// </summary>
    public class CsvPlanetResource : ICsvObject
    {
        /// <summary>
        /// Key: `PlanetNaturalId-MAT`
        /// </summary>
        [Csv.Index(0)]
        public string Key { get; set; } = null!;

        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [Csv.Index(1)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [Csv.Index(2)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [Csv.Index(3)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Factor
        /// </summary>
        [Csv.Index(4)]
        public double Factor { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvPlanetResource() { }

        /// <summary>
        /// PlanetResource constructor
        /// </summary>
        /// <param name="planetResource"></param>
        public CsvPlanetResource(Model.PlanetResource planetResource)
        {
            var material = EntityCaches.MaterialCache.Get(planetResource.MaterialId);
            var ticker = material != null ? material.Ticker : planetResource.MaterialId;
            var planetNaturalId = planetResource.Planet.NaturalId;

            Key = $"{planetNaturalId}-{ticker}";
            PlanetNaturalId = planetNaturalId;
            Ticker = ticker;
            Type = planetResource.Type;
            Factor = planetResource.Concentration;
        }
    }
}
