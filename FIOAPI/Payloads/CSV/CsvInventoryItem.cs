﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvInventory
    /// </summary>
    public class CsvInventoryItem : ICsvObjectWithUserName
    {
        /// <summary>
        /// NaturalId
        /// </summary>
        [Csv.Index(1)]
        public string? NaturalId { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        [Csv.Index(2)]
        public string? Name { get; set; } = null!;

        /// <summary>
        /// StorageType
        /// </summary>
        [Csv.Index(3)]
        public string StorageType { get; set; } = null!;

        /// <summary>
        /// Ticker
        /// </summary>
        [Csv.Index(4)]
        public string? Ticker { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Csv.Index(5)]
        public int? Amount { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public CsvInventoryItem() { }

        /// <summary>
        /// StorageItem Constructor
        /// </summary>
        /// <param name="storageItem"></param>
        public CsvInventoryItem(Model.StorageItem storageItem)
        {
            UserName = storageItem.Storage.UserNameSubmitted;
            NaturalId = storageItem.Storage.LocationNaturalId;
            Name = storageItem.Storage.LocationName;
            StorageType = storageItem.Storage.Type;
            Ticker = storageItem.MaterialTicker;
            Amount = storageItem.Amount;
        }
    }
}
