﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvBuildingRecipe
    /// </summary>
    public class CsvBuildingRecipe : ICsvObject
    {
        /// <summary>
        /// BUI:INP-INP-INP=>OUT-OUT-OUT
        /// </summary>
        [Csv.Index(0)]
        public string Key { get; set; } = null!;

        /// <summary>
        /// Building ticker
        /// </summary>
        [Csv.Index(1)]
        public string Building { get; set; } = null!;

        /// <summary>
        /// Workforce Level
        /// </summary>
        [Csv.Index(2)]
        public long Duration { get; set; }
    }
}
