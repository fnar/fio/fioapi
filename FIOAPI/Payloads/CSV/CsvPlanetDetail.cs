﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvPlanetDetail
    /// </summary>
    public class CsvPlanetDetail : ICsvObject
    {
        /// <summary>
        /// PlanetId
        /// </summary>
        [Csv.Index(0)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [Csv.Index(1)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [Csv.Index(2)]
        public string? PlanetName { get; set; }

        /// <summary>
        /// Namer
        /// </summary>
        [Csv.Index(3)]
        public string? Namer { get; set; }

        /// <summary>
        /// Nameable
        /// </summary>
        [Csv.Index(4)]
        public bool Nameable { get; set; }


        /// <summary>
        /// Gravity
        /// </summary>
        [Csv.Index(5)]
        public double Gravity { get; set; }

        /// <summary>
        /// MagneticField
        /// </summary>
        [Csv.Index(6)]
        public double MagneticField { get; set; }

        /// <summary>
        /// Mass
        /// </summary>
        [Csv.Index(7)]
        public double Mass { get; set; }

        /// <summary>
        /// SemiMajorAxis
        /// </summary>
        [Csv.Index(8)]
        public double SemiMajorAxis { get; set; }

        /// <summary>
        /// Eccentricity
        /// </summary>
        [Csv.Index(9)]
        public double Eccentricity { get; set; }

        /// <summary>
        /// OrbitIndex
        /// </summary>
        [Csv.Index(10)]
        public int OrbitIndex { get; set; }

        /// <summary>
        /// Pressure
        /// </summary>
        [Csv.Index(11)]
        public double Pressure { get; set; }

        /// <summary>
        /// Radius
        /// </summary>
        [Csv.Index(12)]
        public double Radius { get; set; }

        /// <summary>
        /// Sunlight
        /// </summary>
        [Csv.Index(13)]
        public double Sunlight { get; set; }

        /// <summary>
        /// Surface
        /// </summary>
        [Csv.Index(14)]
        public bool Surface { get; set; }

        /// <summary>
        /// Temperature
        /// </summary>
        [Csv.Index(15)]
        public double Temperature { get; set; }

        /// <summary>
        /// Fertility
        /// </summary>
        [Csv.Index(16)]
        public double? Fertility { get; set; }

        /// <summary>
        /// HasLocalMarket
        /// </summary>
        [Csv.Index(17)]
        public bool HasLocalMarket { get; set; }

        /// <summary>
        /// HasChamberOfCommerce
        /// </summary>
        [Csv.Index(18)]
        public bool HasChamberOfCommerce { get; set; }

        /// <summary>
        /// HasWarehouse
        /// </summary>
        [Csv.Index(19)]
        public bool HasWarehouse { get; set; }

        /// <summary>
        /// HasAdministrationCenter
        /// </summary>
        [Csv.Index(20)]
        public bool HasAdministrationCenter { get; set; }

        /// <summary>
        /// HasShipyard
        /// </summary>
        [Csv.Index(21)]
        public bool HasShipyard { get; set; }

        /// <summary>
        /// CountryCode
        /// </summary>
        [Csv.Index(22)]
        public string? CountryCode { get; set; }

        /// <summary>
        /// CountryName
        /// </summary>
        [Csv.Index(23)]
        public string? CountryName { get; set; }

        /// <summary>
        /// GoverningEntity
        /// </summary>
        [Csv.Index(24)]
        public string? GoverningEntity { get; set; }

        /// <summary>
        /// CurrencyName
        /// </summary>
        [Csv.Index(25)]
        public string? CurrencyName { get; set; }

        /// <summary>
        /// CurrencyCode
        /// </summary>
        [Csv.Index(26)]
        public string? CurrencyCode { get; set; }

        /// <summary>
        /// BaseLocalMarketFee
        /// </summary>
        [Csv.Index(27)]
        public double? BaseLocalMarketFee { get; set; }

        /// <summary>
        /// WarehouseFee
        /// </summary>
        [Csv.Index(28)]
        public double? WarehouseFee { get; set; }

        /// <summary>
        /// EstablishmentFee
        /// </summary>
        [Csv.Index(29)]
        public double? EstablishmentFee { get; set; }

        /// <summary>
        /// PopulationId
        /// </summary>
        [Csv.Index(30)]
        public string? PopulationId { get; set; }

        /// <summary>
        /// COGCProgramStatus
        /// </summary>
        [Csv.Index(31)]
        public string? COGCProgramStatus { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvPlanetDetail() { }

        /// <summary>
        /// Planet Constructor
        /// </summary>
        /// <param name="planet"></param>
        public CsvPlanetDetail(Model.Planet planet)
        {
            PlanetId = planet.PlanetId;
            PlanetNaturalId = planet.NaturalId;
            PlanetName = planet.Name;
            Namer = planet.NamerUserName;
            Nameable = planet.Nameable;
            Gravity = planet.Gravity;
            MagneticField = planet.MagneticField;
            Mass = planet.Mass;
            SemiMajorAxis = planet.SemiMajorAxis;
            Eccentricity = planet.Eccentricity;
            OrbitIndex = planet.OrbitIndex;
            Pressure = planet.Pressure;
            Radius = planet.Radius;
            Sunlight = planet.Sunlight;
            Surface = planet.Surface;
            Temperature = planet.Temperature;
            Fertility = planet.Fertility;
            HasLocalMarket = planet.HasLocalMarket;
            HasChamberOfCommerce = planet.HasChamberOfCommerce;
            HasWarehouse = planet.HasWarehouse;
            HasAdministrationCenter = planet.HasAdministrationCenter;
            HasShipyard = planet.HasShipyard;
            CountryCode = planet.CountryCode;
            CountryName = planet.CountryName;
            GoverningEntity = planet.GoverningEntity;
            CurrencyName = planet.CurrencyName;
            CurrencyCode = planet.CurrencyCode;
            BaseLocalMarketFee = planet.LocalMarketFeeBase;
            WarehouseFee = planet.WarehouseFee;
            EstablishmentFee = planet.EstablishmentFee;
            PopulationId = planet.PopulationId;
            COGCProgramStatus = planet.CurrentCOGCProgram;
        }
    }
}
