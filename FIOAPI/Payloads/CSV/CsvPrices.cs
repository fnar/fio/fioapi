﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvPrices
    /// </summary>
    public class CsvPrices : ICsvObject
    {
        /// <summary>
        /// Ticker
        /// </summary>
        [Csv.Index(0)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// MMBuy
        /// </summary>
        [Csv.Index(1)]
        public double? MMBuy { get; set; }

        /// <summary>
        /// MMSell
        /// </summary>
        [Csv.Index(2)]
        public double? MMSell { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(3)]
        [Csv.Name("AI1-Average")]
        public double? AI1_Average { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(4)]
        [Csv.Name("AI1-Previous")]
        public double? AI1_Previous { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(5)]
        [Csv.Name("AI1-AskAmt")]
        public int? AI1_AskAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(6)]
        [Csv.Name("AI1-AskPrice")]
        public double? AI1_AskPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(7)]
        [Csv.Name("AI1-AskAvail")]
        public int? AI1_AskAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(8)]
        [Csv.Name("AI1-BidAmt")]
        public int? AI1_BidAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(9)]
        [Csv.Name("AI1-BidPrice")]
        public double? AI1_BidPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(10)]
        [Csv.Name("AI1-BidAvail")]
        public int? AI1_BidAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(11)]
        [Csv.Name("CI1-Average")]
        public double? CI1_Average { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(12)]
        [Csv.Name("CI1-Previous")]
        public double? CI1_Previous { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(13)]
        [Csv.Name("CI1-AskAmt")]
        public int? CI1_AskAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(14)]
        [Csv.Name("CI1-AskPrice")]
        public double? CI1_AskPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(15)]
        [Csv.Name("CI1-AskAvail")]
        public int? CI1_AskAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(16)]
        [Csv.Name("CI1-BidAmt")]
        public int? CI1_BidAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(17)]
        [Csv.Name("CI1-BidPrice")]
        public double? CI1_BidPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(18)]
        [Csv.Name("CI1-BidAvail")]
        public int? CI1_BidAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(19)]
        [Csv.Name("CI2-Average")]
        public double? CI2_Average { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(20)]
        [Csv.Name("CI2-Previous")]
        public double? CI2_Previous { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(21)]
        [Csv.Name("CI2-AskAmt")]
        public int? CI2_AskAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(22)]
        [Csv.Name("CI2-AskPrice")]
        public double? CI2_AskPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(23)]
        [Csv.Name("CI2-AskAvail")]
        public int? CI2_AskAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(24)]
        [Csv.Name("CI2-BidAmt")]
        public int? CI2_BidAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(25)]
        [Csv.Name("CI2-BidPrice")]
        public double? CI2_BidPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(26)]
        [Csv.Name("CI2-BidAvail")]
        public int? CI2_BidAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(27)]
        [Csv.Name("NC1-Average")]
        public double? NC1_Average { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(28)]
        [Csv.Name("NC1-Previous")]
        public double? NC1_Previous { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(29)]
        [Csv.Name("NC1-AskAmt")]
        public int? NC1_AskAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(30)]
        [Csv.Name("NC1-AskPrice")]
        public double? NC1_AskPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(31)]
        [Csv.Name("NC1-AskAvail")]
        public int? NC1_AskAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(32)]
        [Csv.Name("NC1-BidAmt")]
        public int? NC1_BidAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(33)]
        [Csv.Name("NC1-BidPrice")]
        public double? NC1_BidPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(34)]
        [Csv.Name("NC1-BidAvail")]
        public int? NC1_BidAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(35)]
        [Csv.Name("NC2-Average")]
        public double? NC2_Average { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(36)]
        [Csv.Name("NC21-Previous")]
        public double? NC2_Previous { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(37)]
        [Csv.Name("NC2-AskAmt")]
        public int? NC2_AskAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(38)]
        [Csv.Name("NC2-AskPrice")]
        public double? NC2_AskPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(39)]
        [Csv.Name("NC2-AskAvail")]
        public int? NC2_AskAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(40)]
        [Csv.Name("NC2-BidAmt")]
        public int? NC2_BidAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(41)]
        [Csv.Name("NC2-BidPrice")]
        public double? NC2_BidPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(42)]
        [Csv.Name("NC2-BidAvail")]
        public int? NC2_BidAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(43)]
        [Csv.Name("IC1-Average")]
        public double? IC1_Average { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(44)]
        [Csv.Name("IC1-Previous")]
        public double? IC1_Previous { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(45)]
        [Csv.Name("IC1-AskAmt")]
        public int? IC1_AskAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(46)]
        [Csv.Name("IC1-AskPrice")]
        public double? IC1_AskPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(47)]
        [Csv.Name("IC1-AskAvail")]
        public int? IC1_AskAvail { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(48)]
        [Csv.Name("IC1-BidAmt")]
        public int? IC1_BidAmt { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(49)]
        [Csv.Name("IC1-BidPrice")]
        public double? IC1_BidPrice { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Csv.Index(50)]
        [Csv.Name("IC1-BidAvail")]
        public int? IC1_BidAvail { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvPrices() { }

        /// <summary>
        /// Constructor that takes all CXEntries for the same ticker
        /// </summary>
        /// <param name="Ticker">Ticker</param>
        /// <param name="MMBuy">MMBuy</param>
        /// <param name="MMSell">MMSell</param>
        /// <param name="AI1">CXEntry from AI1</param>
        /// <param name="CI1">CXEntry from CI1</param>
        /// <param name="CI2">CXEntry from CI2</param>
        /// <param name="NC1">CXEntry from NC1</param>
        /// <param name="NC2">CXEntry from NC2</param>
        /// <param name="IC1">CXEntry from IC1</param>
        public CsvPrices(string Ticker, double? MMBuy, double? MMSell, Model.CXEntry? AI1, Model.CXEntry? CI1, Model.CXEntry? CI2,  Model.CXEntry? NC1, Model.CXEntry? NC2, Model.CXEntry? IC1) 
        {
            this.Ticker = Ticker;
            this.MMBuy = MMBuy;
            this.MMSell = MMSell;

            Model.CXEntry? entry;

            entry = AI1;
            if (entry != null)
            {
                AI1_Average = entry.PriceAverage;
                AI1_Previous = entry.Price;
                AI1_AskAmt = entry.AskCount;
                AI1_AskPrice = entry.Ask;
                AI1_AskAvail = entry.Supply;
                AI1_BidAmt = entry.BidCount;
                AI1_BidPrice = entry.Bid;
                AI1_BidAvail = entry.Demand;
            }
            
            entry = CI1;
            if (entry != null)
            {
                CI1_Average = entry.PriceAverage;
                CI1_Previous = entry.Price;
                CI1_AskAmt = entry.AskCount;
                CI1_AskPrice = entry.Ask;
                CI1_AskAvail = entry.Supply;
                CI1_BidAmt = entry.BidCount;
                CI1_BidPrice = entry.Bid;
                CI1_BidAvail = entry.Demand;
            }

            entry = CI2;
            if (entry != null)
            {
                CI2_Average = entry.PriceAverage;
                CI2_Previous = entry.Price;
                CI2_AskAmt = entry.AskCount;
                CI2_AskPrice = entry.Ask;
                CI2_AskAvail = entry.Supply;
                CI2_BidAmt = entry.BidCount;
                CI2_BidPrice = entry.Bid;
                CI2_BidAvail = entry.Demand;
            }

            entry = NC1;
            if (entry != null)
            {
                NC1_Average = entry.PriceAverage;
                NC1_Previous = entry.Price;
                NC1_AskAmt = entry.AskCount;
                NC1_AskPrice = entry.Ask;
                NC1_AskAvail = entry.Supply;
                NC1_BidAmt = entry.BidCount;
                NC1_BidPrice = entry.Bid;
                NC1_BidAvail = entry.Demand;
            }

            entry = NC2;
            if (entry != null)
            {
                NC2_Average = entry.PriceAverage;
                NC2_Previous = entry.Price;
                NC2_AskAmt = entry.AskCount;
                NC2_AskPrice = entry.Ask;
                NC2_AskAvail = entry.Supply;
                NC2_BidAmt = entry.BidCount;
                NC2_BidPrice = entry.Bid;
                NC2_BidAvail = entry.Demand;
            }

            entry = IC1;
            if (entry != null)
            {
                IC1_Average = entry.PriceAverage;
                IC1_Previous = entry.Price;
                IC1_AskAmt = entry.AskCount;
                IC1_AskPrice = entry.Ask;
                IC1_AskAvail = entry.Supply;
                IC1_BidAmt = entry.BidCount;
                IC1_BidPrice = entry.Bid;
                IC1_BidAvail = entry.Demand;
            }
        }
    }
}
