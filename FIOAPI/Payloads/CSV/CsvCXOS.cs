﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvCXOS
    /// </summary>
    public class CsvCXOS : ICsvObjectWithUserName
    {
        /// <summary>
        /// OrderId
        /// </summary>
        [Csv.Index(1)]
        public string OrderId { get; set; } = null!;

        /// <summary>
        /// ExchangeCode
        /// </summary>
        [Csv.Index(2)]
        public string ExchangeCode { get; set; } = null!;

        /// <summary>
        /// OrderType
        /// </summary>
        [Csv.Index(3)]
        public string OrderType { get; set; } = null!;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Csv.Index(4)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Csv.Index(5)]
        public int Amount { get; set; }

        /// <summary>
        /// InitialAmount
        /// </summary>
        [Csv.Index(6)]
        public int InitialAmount { get; set; }

        /// <summary>
        /// Limit
        /// </summary>
        [Csv.Index(7)]
        public double Limit { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        [Csv.Index(8)]
        public string Currency { get; set; } = null!;

        /// <summary>
        /// Status
        /// </summary>
        [Csv.Index(9)]
        public string Status { get; set; } = null!;

        /// <summary>
        /// CreatedEpochMs
        /// </summary>
        [Csv.Index(10)]
        public long CreatedEpochMs { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public CsvCXOS() { }

        /// <summary>
        /// CXOS constructor
        /// </summary>
        /// <param name="cxos">cxos</param>
        public CsvCXOS(Model.CXOS cxos)
        {
            UserName = cxos.UserNameSubmitted;
            OrderId = cxos.OrderId;
            ExchangeCode = cxos.ExchangeCode;
            OrderType = cxos.OrderType;
            MaterialTicker = cxos.MaterialTicker;
            Amount = cxos.Amount;
            InitialAmount = cxos.InitialAmount;
            Limit = cxos.LimitAmount;
            Currency = cxos.LimitCurrency;
            Status = cxos.Status;
            CreatedEpochMs = cxos.Created.ToEpochMs();
        }
    }
}
