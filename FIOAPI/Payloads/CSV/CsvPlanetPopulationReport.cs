﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvPlanetPopulationReport
    /// </summary>
    public class CsvPlanetPopulationReport : ICsvObject
    {
        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [Csv.Index(0)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [Csv.Index(1)]
        public string? PlanetName { get; set; }

        /// <summary>
        /// ExplorersGraceEnabled
        /// </summary>
        [Csv.Index(2)]
        public bool ExplorersGraceEnabled { get; set; }

        /// <summary>
        /// SimulationPeriod
        /// </summary>
        [Csv.Index(3)]
        public int SimulationPeriod { get; set; }

        /// <summary>
        /// TimestampMs
        /// </summary>
        [Csv.Index(4)]
        public long TimestampMs { get; set; }

        /// <summary>
        /// NextPopulationPioneer
        /// </summary>
        [Csv.Index(5)]
        public int NextPopulationPioneer { get; set; }

        /// <summary>
        /// NextPopulationSettler
        /// </summary>
        [Csv.Index(6)]
        public int NextPopulationSettler { get; set; }

        /// <summary>
        /// NextPopulationTechnician
        /// </summary>
        [Csv.Index(7)]
        public int NextPopulationTechnician { get; set; }

        /// <summary>
        /// NextPopulationEngineer
        /// </summary>
        [Csv.Index(8)]
        public int NextPopulationEngineer { get; set; }

        /// <summary>
        /// NextPopulationScientist
        /// </summary>
        [Csv.Index(9)]
        public int NextPopulationScientist { get; set; }

        /// <summary>
        /// PopulationDifferencePioneer
        /// </summary>
        [Csv.Index(10)]
        public int PopulationDifferencePioneer { get; set; }

        /// <summary>
        /// PopulationDifferenceSettler
        /// </summary>
        [Csv.Index(11)]
        public int PopulationDifferenceSettler { get; set; }

        /// <summary>
        /// PopulationDifferenceTechnician
        /// </summary>
        [Csv.Index(12)]
        public int PopulationDifferenceTechnician { get; set; }

        /// <summary>
        /// PopulationDifferenceEngineer
        /// </summary>
        [Csv.Index(13)]
        public int PopulationDifferenceEngineer { get; set; }

        /// <summary>
        /// PopulationDifferenceScientist
        /// </summary>
        [Csv.Index(14)]
        public int PopulationDifferenceScientist { get; set; }

        /// <summary>
        /// AverageHappinessPioneer
        /// </summary>
        [Csv.Index(15)]
        public double AverageHappinessPioneer { get; set; }

        /// <summary>
        /// AverageHappinessSettler
        /// </summary>
        [Csv.Index(16)]
        public double AverageHappinessSettler { get; set; }

        /// <summary>
        /// AverageHappinessTechnician
        /// </summary>
        [Csv.Index(17)]
        public double AverageHappinessTechnician { get; set; }

        /// <summary>
        /// AverageHappinessEngineer
        /// </summary>
        [Csv.Index(18)]
        public double AverageHappinessEngineer { get; set; }

        /// <summary>
        /// AverageHappinessScientist
        /// </summary>
        [Csv.Index(19)]
        public double AverageHappinessScientist { get; set; }

        /// <summary>
        /// UnemploymentRatePioneer
        /// </summary>
        [Csv.Index(20)]
        public double UnemploymentRatePioneer { get; set; }

        /// <summary>
        /// UnemploymentRateSettler
        /// </summary>
        [Csv.Index(21)]
        public double UnemploymentRateSettler { get; set; }

        /// <summary>
        /// UnemploymentRateTechnician
        /// </summary>
        [Csv.Index(22)]
        public double UnemploymentRateTechnician { get; set; }

        /// <summary>
        /// UnemploymentRateEngineer
        /// </summary>
        [Csv.Index(23)]
        public double UnemploymentRateEngineer { get; set; }

        /// <summary>
        /// UnemploymentRateScientist
        /// </summary>
        [Csv.Index(24)]
        public double UnemploymentRateScientist { get; set; }

        /// <summary>
        /// OpenJobsPioneer
        /// </summary>
        [Csv.Index(25)]
        public double OpenJobsPioneer { get; set; }

        /// <summary>
        /// OpenJobsSettler
        /// </summary>
        [Csv.Index(26)]
        public double OpenJobsSettler { get; set; }

        /// <summary>
        /// OpenJobsTechnician
        /// </summary>
        [Csv.Index(27)]
        public double OpenJobsTechnician { get; set; }

        /// <summary>
        /// OpenJobsEngineer
        /// </summary>
        [Csv.Index(28)]
        public double OpenJobsEngineer { get; set; }

        /// <summary>
        /// OpenJobsScientist
        /// </summary>
        [Csv.Index(29)]
        public double OpenJobsScientist { get; set; }

        /// <summary>
        /// NeedFulfillmentLifeSupport
        /// </summary>
        [Csv.Index(30)]
        public double NeedFulfillmentLifeSupport { get; set; }

        /// <summary>
        /// NeedFulfillmentSafety
        /// </summary>
        [Csv.Index(31)]
        public double NeedFulfillmentSafety { get; set; }

        /// <summary>
        /// NeedFulfillmentHealth
        /// </summary>
        [Csv.Index(32)]
        public double NeedFulfillmentHealth { get; set; }

        /// <summary>
        /// NeedFulfillmentComfort
        /// </summary>
        [Csv.Index(33)]
        public double NeedFulfillmentComfort { get; set; }

        /// <summary>
        /// NeedFulfillmentCulture
        /// </summary>
        [Csv.Index(34)]
        public double NeedFulfillmentCulture { get; set; }

        /// <summary>
        /// NeedFulfillmentEducation
        /// </summary>
        [Csv.Index(35)]
        public double NeedFulfillmentEducation { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public CsvPlanetPopulationReport() { }

        /// <summary>
        /// PlanetPopulationReport constructor
        /// </summary>
        /// <param name="planetPopReport"></param>
        public CsvPlanetPopulationReport(Model.PlanetPopulationReport planetPopReport)
        {
            PlanetNaturalId = planetPopReport.Planet.NaturalId;
            PlanetName = planetPopReport.Planet.Name;
            ExplorersGraceEnabled = planetPopReport.ExplorersGraceEnabled;
            SimulationPeriod = planetPopReport.SimulationPeriod;
            TimestampMs = planetPopReport.ReportTimestamp.ToEpochMs();
            NextPopulationPioneer = planetPopReport.NextPopulationPioneer;
            NextPopulationSettler = planetPopReport.NextPopulationSettler;
            NextPopulationTechnician = planetPopReport.NextPopulationTechnician;
            NextPopulationEngineer = planetPopReport.NextPopulationEngineer;
            NextPopulationScientist = planetPopReport.NextPopulationScientist;
            PopulationDifferencePioneer = planetPopReport.PopulationDifferencePioneer;
            PopulationDifferenceSettler = planetPopReport.PopulationDifferenceSettler;
            PopulationDifferenceTechnician = planetPopReport.PopulationDifferenceTechnician;
            PopulationDifferenceEngineer = planetPopReport.PopulationDifferenceEngineer;
            PopulationDifferenceScientist = planetPopReport.PopulationDifferenceScientist;
            AverageHappinessPioneer = planetPopReport.AverageHappinessPioneer;
            AverageHappinessSettler = planetPopReport.AverageHappinessSettler;
            AverageHappinessTechnician = planetPopReport.AverageHappinessTechnician;
            AverageHappinessEngineer = planetPopReport.AverageHappinessEngineer;
            AverageHappinessScientist = planetPopReport.AverageHappinessScientist;
            UnemploymentRatePioneer = planetPopReport.UnemploymentRatePioneer;
            UnemploymentRateSettler = planetPopReport.UnemploymentRateSettler;
            UnemploymentRateTechnician = planetPopReport.UnemploymentRateTechnician;
            UnemploymentRateEngineer = planetPopReport.UnemploymentRateEngineer;
            UnemploymentRateScientist = planetPopReport.UnemploymentRateScientist;
            OpenJobsPioneer = planetPopReport.OpenJobsPioneer;
            OpenJobsSettler = planetPopReport.OpenJobsSettler;
            OpenJobsTechnician = planetPopReport.OpenJobsTechnician;
            OpenJobsEngineer = planetPopReport.OpenJobsEngineer;
            OpenJobsScientist = planetPopReport.OpenJobsScientist;
            NeedFulfillmentLifeSupport = planetPopReport.NeedFulfillmentLifeSupport;
            NeedFulfillmentSafety = planetPopReport.NeedFulfillmentSafety;
            NeedFulfillmentHealth = planetPopReport.NeedFulfillmentHealth;
            NeedFulfillmentComfort = planetPopReport.NeedFulfillmentComfort;
            NeedFulfillmentCulture = planetPopReport.NeedFulfillmentCulture;
            NeedFulfillmentEducation = planetPopReport.NeedFulfillmentEducation;
        }
    }
}
