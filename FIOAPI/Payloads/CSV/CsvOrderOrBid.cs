﻿using FIOAPI.DB.Model;

using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvOrderOrBid
    /// </summary>
    public class CsvOrderOrBid : ICsvObject
    {
        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Csv.Index(0)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// ExchangeCode
        /// </summary>
        [Csv.Index(1)]
        public string ExchangeCode { get; set; } = null!;

        /// <summary>
        /// CompanyId
        /// </summary>
        [Csv.Index(2)]
        public string CompanyId { get; set; } = null!;

        /// <summary>
        /// CompanyName
        /// </summary>
        [Csv.Index(3)]
        public string CompanyName { get; set; } = null!;

        /// <summary>
        /// CompanyCode
        /// </summary>
        [Csv.Index(4)]
        public string CompanyCode { get; set; } = null!;

        /// <summary>
        /// ItemCount (Empty is infinite)
        /// </summary>
        [Csv.Index(5)]
        public int? ItemCount { get; set; }

        /// <summary>
        /// ItemCost
        /// </summary>
        [Csv.Index(6)]
        public double ItemCost { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvOrderOrBid() { }

        /// <summary>
        /// Constructor from buy order
        /// </summary>
        /// <param name="buyOrder">Buy order</param>
        public CsvOrderOrBid(CXBuyOrder buyOrder)
        {
            MaterialTicker = buyOrder.CXEntry.MaterialTicker;
            ExchangeCode = buyOrder.CXEntry.ExchangeCode;
            CompanyId = buyOrder.CompanyId;
            CompanyName = buyOrder.CompanyName;
            CompanyCode = buyOrder.CompanyCode;
            ItemCount = buyOrder.ItemCount;
            ItemCost = buyOrder.ItemCost;
        }

        /// <summary>
        /// Constructor from sell order
        /// </summary>
        /// <param name="sellOrder">Sell order</param>
        public CsvOrderOrBid(CXSellOrder sellOrder)
        {
            MaterialTicker = sellOrder.CXEntry.MaterialTicker;
            ExchangeCode = sellOrder.CXEntry.ExchangeCode;
            CompanyId = sellOrder.CompanyId;
            CompanyName = sellOrder.CompanyName;
            CompanyCode = sellOrder.CompanyCode;
            ItemCount = sellOrder.ItemCount;
            ItemCost = sellOrder.ItemCost;
        }
    }
}
