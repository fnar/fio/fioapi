﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvShipInventory
    /// </summary>
    public class CsvShipInventory : ICsvObjectWithUserName, ICloneable
    {
        /// <summary>
        /// Default constructor (intentionally empty)
        /// </summary>
        public CsvShipInventory()
        {
            
        }

        /// <summary>
        /// Ship constructor
        /// </summary>
        /// <param name="ship">Ship</param>
        /// <param name="flight">Flight</param>
        /// <param name="storage">Storage</param>
        /// <param name="sfStorage">SF Storage</param>
        /// <param name="ffStorage">FF Storage</param>
        public CsvShipInventory(Model.Ship ship, Model.Flight? flight, Model.Storage? storage, Model.Storage? sfStorage, Model.Storage? ffStorage)
        {
            if (flight != null && ship.FlightId != flight.FlightId)
            {
                throw new ArgumentException($"Ship FlightId {ship.FlightId} does not match Flight FlightId {flight.FlightId}");
            }

            if (storage != null && ship.StoreId != storage.StorageId)
            {
                throw new ArgumentException($"Ship StorageId {ship.StoreId} does not match Storage StorageId {storage.StorageId}");
            }

            if (sfStorage != null && ship.STLFuelStoreId != sfStorage.StorageId)
            {
                throw new ArgumentException($"Ship STLStorageId {ship.STLFuelStoreId} does not match provided SF storage {sfStorage.StorageId}");
            }

            if (ffStorage != null && ship.FTLFuelStoreId != ffStorage.StorageId)
            {
                throw new ArgumentException($"Ship FTLStorageId {ship.FTLFuelStoreId} does not match provided FF storage {ffStorage.StorageId}");
            }

            UserName = ship.UserNameSubmitted;
            ShipId = ship.ShipId;
            ShipName = ship.Name != null ? ship.Name : "";
            ShipRegistration = ship.Registration;
            if (flight != null && flight.FlightSegments.Count > 0)
            {
                Source = flight.FlightSegments.First().OriginNaturalId ?? "";
                Destination = flight.FlightSegments.Last().DestinationNaturalId ?? "";
            }
            Location = ship.CurrentLocationNaturalId ?? "";
            CurrentStorageMass = storage?.WeightLoad ?? -1.0;
            MaxStorageMass = storage?.WeightCapacity ?? -1.0;
            CurrentStorageVolume = storage?.VolumeLoad ?? -1.0;
            MaxStorageVolume = storage?.VolumeCapacity ?? -1.0;
            if (sfStorage != null)
            {
                CurrentSF = (int)(sfStorage.WeightLoad / Constants.STLFuelWeight);
                MaxSF = (int)(sfStorage.WeightCapacity / Constants.STLFuelWeight);
            }
            if (ffStorage != null)
            {
                CurrentFF = (int)(ffStorage.WeightLoad / Constants.FTLFuelWeight);
                MaxFF = (int)(ffStorage.WeightCapacity / Constants.FTLFuelWeight);
            }
            Timestamp = ship.Timestamp.ToEpochMs();
        }

        /// <summary>
        /// ShipId
        /// </summary>
        [Csv.Index(1)]
        public string ShipId { get; set; } = null!;

        /// <summary>
        /// ShipName
        /// </summary>
        [Csv.Index(2)]
        public string ShipName { get; set; } = "";

        /// <summary>
        /// ShipRegistration
        /// </summary>
        [Csv.Index(3)]
        public string ShipRegistration { get; set; } = "";

        /// <summary>
        /// Source
        /// </summary>
        [Csv.Index(4)]
        public string Source { get; set; } = "";

        /// <summary>
        /// Destination
        /// </summary>
        [Csv.Index(5)]
        public string Destination { get; set; } = "";

        /// <summary>
        /// Location
        /// </summary>
        [Csv.Index(6)]
        public string Location { get; set; } = "";

        /// <summary>
        /// CurrentStorageMass
        /// </summary>
        [Csv.Index(7)]
        public double CurrentStorageMass { get; set; } = -1.0;

        /// <summary>
        /// MaxStorageMass
        /// </summary>
        [Csv.Index(8)]
        public double MaxStorageMass { get; set; } = -1.0;

        /// <summary>
        /// CurrentStorageVolume
        /// </summary>
        [Csv.Index(9)]
        public double CurrentStorageVolume { get; set; } = -1.0;

        /// <summary>
        /// MaxStorageVolume
        /// </summary>
        [Csv.Index(10)]
        public double MaxStorageVolume { get; set; } = -1.0;

        /// <summary>
        /// CurrentSF
        /// </summary>
        [Csv.Index(11)]
        public int CurrentSF { get; set; } = -1;

        /// <summary>
        /// MaxSF
        /// </summary>
        [Csv.Index(12)]
        public int MaxSF { get; set; } = -1;

        /// <summary>
        /// CurrentFF
        /// </summary>
        [Csv.Index(13)]
        public int CurrentFF { get; set; } = -1;

        /// <summary>
        /// MaxFF
        /// </summary>
        [Csv.Index(14)]
        public int MaxFF { get; set; } = -1;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Csv.Index(15)]
        public string MaterialTicker { get; set; } = "";

        /// <summary>
        /// MaterialAmount
        /// </summary>
        [Csv.Index(16)]
        public int MaterialAmount { get; set; } = 0;

        /// <summary>
        /// Timestamp
        /// </summary>
        [Csv.Index(17)]
        public long Timestamp { get; set; }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy of this object</returns>
        public object Clone()
        {
            return new CsvShipInventory
            {
                UserName = UserName,
                ShipId = ShipId,
                ShipName = ShipName,
                ShipRegistration = ShipRegistration,
                Source = Source,
                Destination = Destination,
                Location = Location,
                CurrentStorageMass = CurrentStorageMass,
                MaxStorageMass = MaxStorageMass,
                CurrentStorageVolume = CurrentStorageVolume,
                MaxStorageVolume = MaxStorageVolume,
                CurrentSF = CurrentSF,
                MaxSF = MaxSF,
                CurrentFF = CurrentFF,
                MaxFF = MaxFF,
                MaterialTicker = MaterialTicker,
                MaterialAmount = MaterialAmount,
                Timestamp = Timestamp,
            };
        }
    }
}
