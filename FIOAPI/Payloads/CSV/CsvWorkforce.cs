﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvWorkforce
    /// </summary>
    public class CsvWorkforce : ICsvObjectWithUserName
    {
        /// <summary>
        /// SiteId
        /// </summary>
        [Csv.Index(1)]
        public string SiteId { get; set; } = null!;

        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [Csv.Index(2)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [Csv.Index(3)]
        public string? PlanetName { get; set; }

        /// <summary>
        /// PioneerPopulation
        /// </summary>
        [Csv.Index(4)]
        public int PioneerPopulation { get; set; }

        /// <summary>
        /// PioneerCapacity
        /// </summary>
        [Csv.Index(5)]
        public int PioneerCapacity { get; set; }

        /// <summary>
        /// PioneerRequired
        /// </summary>
        [Csv.Index(6)]
        public int PioneerRequired { get; set; }

        /// <summary>
        /// PioneerSatisfaction
        /// </summary>
        [Csv.Index(7)]
        public double PioneerSatisfaction { get; set; }

        /// <summary>
        /// SettlerPopulation
        /// </summary>
        [Csv.Index(8)]
        public int SettlerPopulation { get; set; }

        /// <summary>
        /// SettlerReserve
        /// </summary>
        [Csv.Index(9)]
        public int SettlerReserve { get; set; }

        /// <summary>
        /// SettlerCapacity
        /// </summary>
        [Csv.Index(10)]
        public int SettlerCapacity { get; set; }

        /// <summary>
        /// SettlerRequired
        /// </summary>
        [Csv.Index(11)]
        public int SettlerRequired { get; set; }

        /// <summary>
        /// SettlerSatisfaction
        /// </summary>
        [Csv.Index(12)]
        public double SettlerSatisfaction { get; set; }

        /// <summary>
        /// TechnicianPopulation
        /// </summary>
        [Csv.Index(13)]
        public int TechnicianPopulation { get; set; }

        /// <summary>
        /// TechnicianReserve
        /// </summary>
        [Csv.Index(14)]
        public int TechnicianReserve { get; set; }

        /// <summary>
        /// TechnicianCapacity
        /// </summary>
        [Csv.Index(15)]
        public int TechnicianCapacity { get; set; }

        /// <summary>
        /// TechnicianRequired
        /// </summary>
        [Csv.Index(16)]
        public int TechnicianRequired { get; set; }

        /// <summary>
        /// TechnicianSatisfaction
        /// </summary>
        [Csv.Index(17)]
        public double TechnicianSatisfaction { get; set; }

        /// <summary>
        /// EngineerPopulation
        /// </summary>
        [Csv.Index(18)]
        public int EngineerPopulation { get; set; }

        /// <summary>
        /// EngineerReserve
        /// </summary>
        [Csv.Index(19)]
        public int EngineerReserve { get; set; }

        /// <summary>
        /// EngineerCapacity
        /// </summary>
        [Csv.Index(20)]
        public int EngineerCapacity { get; set; }

        /// <summary>
        /// EngineerRequired
        /// </summary>
        [Csv.Index(21)]
        public int EngineerRequired { get; set; }

        /// <summary>
        /// EngineerSatisfaction
        /// </summary>
        [Csv.Index(22)]
        public double EngineerSatisfaction { get; set; }

        /// <summary>
        /// ScientistPopulation
        /// </summary>
        [Csv.Index(23)]
        public int ScientistPopulation { get; set; }

        /// <summary>
        /// ScientistReserve
        /// </summary>
        [Csv.Index(24)]
        public int ScientistReserve { get; set; }

        /// <summary>
        /// ScientistCapacity
        /// </summary>
        [Csv.Index(25)]
        public int ScientistCapacity { get; set; }

        /// <summary>
        /// ScientistRequired
        /// </summary>
        [Csv.Index(26)]
        public int ScientistRequired { get; set; }

        /// <summary>
        /// ScientistSatisfaction
        /// </summary>
        [Csv.Index(27)]
        public double ScientistSatisfaction { get; set; }

        /// <summary>
        /// LastTickTime - When consumables were last pulled from this site
        /// </summary>
        [Csv.Index(28)]
        public long? LastTickTime { get; set; }

        /// <summary>
        /// Empty Constructor
        /// </summary>
        public CsvWorkforce() { }

        /// <summary>
        /// Workforce constructor
        /// </summary>
        /// <param name="workforce"></param>
        /// <param name="sites"></param>
        public CsvWorkforce(Model.Workforce workforce, List<Model.Site> sites)
        {
            UserName = workforce.UserNameSubmitted;
            SiteId = workforce.SiteId;

            var site = sites.FirstOrDefault(s => s.SiteId == SiteId);
            PlanetNaturalId = (site != null) ? site.LocationNaturalId : SiteId;
            PlanetName = (site != null) ? site.LocationName : SiteId;
            PioneerPopulation = workforce.PioneerPopulation;
            PioneerCapacity = workforce.PioneerCapacity;
            PioneerRequired = workforce.PioneerRequired;
            PioneerSatisfaction = workforce.PioneerSatisfaction;
            SettlerPopulation = workforce.SettlerPopulation;
            SettlerCapacity = workforce.SettlerCapacity;
            SettlerRequired = workforce.SettlerRequired;
            SettlerSatisfaction = workforce.SettlerSatisfaction;
            TechnicianPopulation = workforce.TechnicianPopulation;
            TechnicianCapacity = workforce.TechnicianCapacity;
            TechnicianRequired = workforce.TechnicianRequired;
            TechnicianSatisfaction = workforce.TechnicianSatisfaction;
            EngineerPopulation = workforce.EngineerPopulation;
            EngineerCapacity = workforce.EngineerCapacity;
            EngineerRequired = workforce.EngineerRequired;
            EngineerSatisfaction = workforce.EngineerSatisfaction;
            ScientistPopulation = workforce.ScientistPopulation;
            ScientistCapacity = workforce.ScientistCapacity;
            ScientistRequired = workforce.ScientistRequired;
            ScientistSatisfaction = workforce.ScientistSatisfaction;
            LastTickTime = workforce.LastTickTime?.ToEpochMs();
        }
    }
}
