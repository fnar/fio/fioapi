﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvBuildingWorkforce
    /// </summary>
    public class CsvBuildingWorkforce : ICsvObject
    {
        /// <summary>
        /// BUI-LEVEL
        /// </summary>
        [Csv.Index(0)]
        public string Key { get; set; } = null!;

        /// <summary>
        /// Building ticker
        /// </summary>
        [Csv.Index(1)]
        public string Building { get; set; } = null!;

        /// <summary>
        /// Workforce Level
        /// </summary>
        [Csv.Index(2)]
        public string Level { get; set; } = null!;

        /// <summary>
        /// Capacity
        /// </summary>
        [Csv.Index(3)]
        public int Capacity { get; set; }
    }
}
