﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvSystemLink
    /// </summary>
    public class CsvSystemLink : ICsvObject
    {
        /// <summary>
        /// Left
        /// </summary>
        [Csv.Index(0)]
        public string Left { get; set; } = null!;

        /// <summary>
        /// Right
        /// </summary>
        [Csv.Index(1)]
        public string Right { get; set; } = null!;
    }
}
