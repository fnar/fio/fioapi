﻿using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvPlanet
    /// </summary>
    public class CsvPlanetProductionFee : ICsvObject
    {
        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [Csv.Index(0)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [Csv.Index(1)]
        public string? PlanetName { get; set; } = null!;

        /// <summary>
        /// Category
        /// </summary>
        [Csv.Index(2)]
        public string Category { get; set; } = null!;

        /// <summary>
        /// WorkforceLevel
        /// </summary>
        [Csv.Index(3)]
        public string WorkforceLevel { get; set; } = null!;

        /// <summary>
        /// FeeAmount
        /// </summary>
        [Csv.Index(4)]
        public double? FeeAmount { get; set; }

        /// <summary>
        /// FeeCurrency
        /// </summary>
        [Csv.Index(5)]
        public string? FeeCurrency { get; set; } = null!;

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvPlanetProductionFee() { }

        /// <summary>
        /// PlanetWorkforceFee constructor
        /// </summary>
        /// <param name="planetWorkforceFee"></param>
        public CsvPlanetProductionFee(Model.PlanetWorkforceFee planetWorkforceFee)
        {
            PlanetNaturalId = planetWorkforceFee.Planet.NaturalId;
            PlanetName = planetWorkforceFee.Planet.Name;
            Category = planetWorkforceFee.Category;
            WorkforceLevel = planetWorkforceFee.WorkforceLevel;
            FeeAmount = planetWorkforceFee.Fee;
            FeeCurrency = planetWorkforceFee.Currency;
        }
    }
}
