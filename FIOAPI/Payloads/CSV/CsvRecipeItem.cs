﻿using FIOAPI.DB.Model;
using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Payloads.CSV
{
    /// <summary>
    /// CsvRecipeItem (input or output)
    /// </summary>
    public class CsvRecipeItem : ICsvObject
    {
        /// <summary>
        /// The recipe key
        /// </summary>
        [Csv.Index(0)]
        public string Key { get; set; } = null!;

        /// <summary>
        /// Material ticker
        /// </summary>
        [Csv.Index(1)]
        public string Material { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Csv.Index(2)]
        public int Amount { get; set; }

        /// <summary>
        /// Empty constructor
        /// </summary>
        public CsvRecipeItem() { }

        /// <summary>
        /// Input consturctor
        /// </summary>
        /// <param name="input"></param>
        public CsvRecipeItem(BuildingRecipeInput input)
        {
            Key = input.BuildingRecipe.BuildingRecipeId;
            Material = input.Ticker;
            Amount = input.Amount;
        }

        /// <summary>
        /// Output constructor
        /// </summary>
        /// <param name="output"></param>
        public CsvRecipeItem(BuildingRecipeOutput output)
        {
            Key = output.BuildingRecipe.BuildingRecipeId;
            Material = output.Ticker;
            Amount = output.Amount;
        }
    }
}
