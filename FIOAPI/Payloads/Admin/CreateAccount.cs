﻿
namespace FIOAPI.Payloads.Admin
{
    /// <summary>
    /// The payload for admin account creation
    /// </summary>
    public class CreateAccount : IValidation
    {
        /// <summary>
        /// The UserName
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = "";

        /// <summary>
        /// The password
        /// </summary>
        [StringLength(256, MinimumLength = 3)]
        public string Password { get; set; } = "";

        /// <summary>
        /// If the user should be admin
        /// </summary>
        [DefaultValue(false)]
        public bool Admin { get; set; } = false;

        /// <summary>
        /// If the user should be a bot
        /// </summary>
        [DefaultValue(false)]
        public bool Bot { get; set; } = false;

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (Admin && Bot)
            {
                Errors.Add($"{Context} cannot both be an Admin and a Bot");
            }
        }
    }
}
