﻿#pragma warning disable 1591
using FIOAPI.Payloads;

namespace FIOAPI.Payloads.Material
{
    public class MESG_WORLD_MATERIAL_DATA : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_WORLD_MATERIAL_DATA_PAYLOAD_OUTER payload { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_DATA_PAYLOAD_OUTER : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_WORLD_MATERIAL_DATA_MESSAGE message { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_DATA_MESSAGE : IValidation
    {
        [Equals("WORLD_MATERIAL_DATA")]
        public string messageType { get; set; } = null!;

        public MESG_WORLD_MATERIAL_DATA_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_DATA_PAYLOAD_INNER : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        public bool isResource { get; set; }

        [StringLength(32)]
        public string? resourceType { get; set; } = null;

        public MESG_WORLD_MATERIAL_DATA_INPUT_RECIPE[] inputRecipes { get; set; } = null!;

        public MESG_WORLD_MATERIAL_DATA_OUTPUT_RECIPE[] outputRecipes { get; set; } = null!;

        public MESG_WORLD_MATERIAL_DATA_BUILDING_RECIPE[] buildingRecipes { get; set; } = null!;

        public MESG_WORLD_MATERIAL_DATA_INFRASTRUCTURE_USAGE[] infrastructureUsage { get; set; } = null!;

        public bool cogcUsage { get; set; }

        public bool workforceUsage { get; set; }
    }

    public class MESG_WORLD_MATERIAL_DATA_INPUT_RECIPE : IValidation
    {
        public APEX_COMMON_MATERIAL_AND_AMOUNT[] inputs { get; set; } = null!;

        public APEX_COMMON_MATERIAL_AND_AMOUNT[] outputs { get; set; } = null!;

        public APEX_COMMON_DURATION duration { get; set; } = null!;

        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string reactorId { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_DATA_OUTPUT_RECIPE : IValidation
    {
        public APEX_COMMON_MATERIAL_AND_AMOUNT[] inputs { get; set; } = null!;

        public APEX_COMMON_MATERIAL_AND_AMOUNT[] outputs { get; set; } = null!;

        public APEX_COMMON_DURATION duration { get; set; } = null!;

        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string reactorId { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_DATA_BUILDING_RECIPE : IValidation
    {
        public APEX_COMMON_MATERIAL_AND_AMOUNT[] buildingCosts { get; set; } = null!;

        [APEXID]
        [StringLength(32, MinimumLength = 32)]
        public string reactorId { get; set; } = null!;
    }

    public class MESG_WORLD_MATERIAL_DATA_INFRASTRUCTURE_USAGE : IValidation
    {
        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        [Ticker]
        [StringLength(4, MinimumLength = 1)]
        public string ticker { get; set; } = null!;

        [StringLength(64)]
        public string projectIdentifier { get; set; } = null!;
    }

}
#pragma warning restore 1591