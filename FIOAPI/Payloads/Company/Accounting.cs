﻿#pragma warning disable CS1591 // Missing XML comment for publicly visible type or member
namespace FIOAPI.Payloads.Company
{
    public class MESG_ACCOUNTING_CASH_BALANCES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_ACCOUNTING_CASH_BALANCES_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_ACCOUNTING_CASH_BALANCES_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_ACCOUNTING_CASH_BALANCES_MESSAGE message { get; set; } = null!;
    }

    public class MESG_ACCOUNTING_CASH_BALANCES_MESSAGE : IValidation
    {
        [Equals("ACCOUNTING_CASH_BALANCES")]
        public string messageType { get; set; } = null!;

        public MESG_ACCOUNTING_CASH_BALANCES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_ACCOUNTING_CASH_BALANCES_PAYLOAD_INNER : IValidation
    {
        public APEX_COMMON_CURRENCY ownCurrency { get; set; } = null!;

        public List<MESG_ACCOUNTING_CASH_BALANCES_CURRENCY_ACCOUNT> currencyAccounts { get; set; } = null!;
    }

    public class MESG_ACCOUNTING_CASH_BALANCES_CURRENCY_ACCOUNT : IValidation
    {
        public string category { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int type { get; set; }

        [Range(0, int.MaxValue)]
        public int number { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY bookBalance { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY currencyBalance { get; set; } = null!;
    }
}
#pragma warning restore CS1591 // Missing XML comment for publicly visible type or member