﻿#pragma warning disable 1591
using FIOAPI.Payloads;
using FIOAPI.Payloads.LocalMarket;

namespace FIOAPI.Payloads.Company
{
    // Filler parent classes
    public class PATH_COMPANY : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PATH_COMPANY_PAYLOAD payload { get; set; } = null!;
    }

    public class PATH_COMPANY_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PATH_COMPANY_MESSAGE message { get; set; } = null!;
    }

    public class PATH_COMPANY_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PATH_COMPANY_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PATH_COMPANY_PAYLOAD_INNER : IValidation
    {
        public PATH_COMPANY_BODY body { get; set; } = null!;

        [ContainerLength(2)]
        [Equals("companies", OffsetIndex:0)]
        public List<string> path { get; set; } = null!;
    }

    // json payload file
    public class PATH_COMPANY_BODY : IValidation
    {
        public string name { get; set; } = null!;

        [Uppercase]
        [StringLength(4, MinimumLength = 1)]

        public string code { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP founded { get; set; } = null!;

        public APEX_COMMON_USER user { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE country { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE? corporation { get; set; } = null!;

        public PATH_COMPANY_RATINGREPORT? ratingReport { get; set; } = null!;

        public List<APEX_COMMON_ADDRESS> siteAddresses { get; set; } = null!;

        public List<PATH_COMPANY_REPUTATION> reputation { get; set; } = null!;

        [Range(0, 1000)]
        public int currentRepresentationLevel { get; set; }

        [APEXID]
        public string id { get; set; } = null!;

        public class PATH_COMPANY_RATINGREPORT : IValidation
        {
            [StringLength(7, MinimumLength = 1)]
            public string overallRating { get; set; } = null!;

            [Range(0, 1000)]
            public int contractCount { get; set; }

            public APEX_COMMON_TIMESTAMP? earliestContract { get; set; } = null!;

            public override void CustomValidation(ref List<string> Errors, string Context)
            {
                base.CustomValidation(ref Errors, Context);
                if (!Constants.ValidRatings.Contains(overallRating))
                {
                    Errors.Add($"'{Context}' has an invalid overallRating of '{overallRating}' specified.");
                }
            }
        }

        public class PATH_COMPANY_REPUTATION : IValidation
        {
            public APEX_COMMON_USER_AND_CODE entityId { get; set; } = null!;

            [Range(-1000, 10000)]
            public int reputation { get; set; }
        }

    }
}
#pragma warning restore 1591