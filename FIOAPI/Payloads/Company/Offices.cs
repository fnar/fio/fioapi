﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Company
{
    public class PATH_COMPANIES_OFFICES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PATH_COMPANIES_OFFICES_PAYLOAD payload { get; set; } = null!;
    }

    public class PATH_COMPANIES_OFFICES_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PATH_COMPANIES_OFFICES_MESSAGE message { get; set; } = null!;
    }

    public class PATH_COMPANIES_OFFICES_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PATH_COMPANIES_OFFICES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PATH_COMPANIES_OFFICES_PAYLOAD_INNER : IValidation
    {
        public List<PATH_COMPANIES_OFFICES_BODY> body { get; set; } = null!;

        [ContainerLength(3)]
        [Equals("companies", OffsetIndex:0)]
        [Equals("offices", OffsetIndex:2)]
        public List<string> path { get; set; } = null!;

        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (path.Count != 3)
            {
                Errors.Add($"{Context}.{nameof(path)} doesn't have 3 entries");
            }
            else
            {
                // Verify we have: "companies", followed by an APEXId, followed by "offices"
                if (path[0] != "companies")
                {
                    Errors.Add($"{Context}.{nameof(path)}[0] isn't 'companies'");
                }

                if (path[1].Length != 32)
                {
                    Errors.Add($"{Context}.{nameof(path)}[1] isn't an APEXID");
                }

                if (path[2] != "offices")
                {
                    Errors.Add($"{Context}.{nameof(path)}[2] isn't 'offices'");
                }
            }
        }
    }

    public class PATH_COMPANIES_OFFICES_BODY : IValidation
    {
        [StringLength(20, MinimumLength = 8)] // MEMBER_OF_PARLIAMENT or GOVERNOR
        public string type { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP start { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP end { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;

		public override void CustomValidation(ref List<string> Errors, string Context)
		{
			base.CustomValidation(ref Errors, Context);

            if (type != "MEMBER_OF_PARLIAMENT" && type != "GOVERNOR")
            {
                Errors.Add($"{Context}.type is not 'MEMBER_OF_PARLIAMENT' or 'GOVERNOR'");
            }
		}
	}
}
#pragma warning restore 1591