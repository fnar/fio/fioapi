﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Company
{
    public class ROOT_COMPANY_DATA : IValidation
    {
        [Equals("COMPANY_DATA")]
        public string messageType { get; set; } = null!;

        public ROOT_COMPANY_DATA_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_COMPANY_DATA_PAYLOAD : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string name { get; set; } = null!;

        [Uppercase]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string code { get; set; } = null!;

        [APEXID]
        public string countryId { get; set; } = null!;

        public APEX_COMMON_CURRENCY ownCurrency { get; set; } = null!;

        public List<MESG_COMPANY_DATA_CURRENCY_ACCOUNT> currencyAccounts { get; set; } = null!;

        [StringLength(20, MinimumLength = 1)]
        public string startingProfile { get; set; } = null!;

        public APEX_COMMON_ADDRESS startingLocation { get; set; } = null!;

        public ROOT_COMPANY_DATA_RATING_REPORT ratingReport { get; set; } = null!;

        public MESG_COMPANY_DATA_HQ headquarters { get; set; } = null!;

        public ROOT_COMPANY_DATA_REPRESENTATION representation { get; set; } = null!;
    }


    public class MESG_COMPANY_DATA_CURRENCY_ACCOUNT : IValidation
    {
        [Equals("LIQUID_ASSETS")]
        public string category { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int type { get; set; }

        [Range(0, int.MaxValue)]
        public int number { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY bookBalance { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY currencyBalance { get; set; } = null!;
    }

    public class ROOT_COMPANY_DATA_RATING_REPORT : IValidation
    {
        [StringLength(Constants.RatingLengthMax, MinimumLength = Constants.RatingLengthMin)]
        public string overallRating { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int contractCount { get; set; }

        public APEX_COMMON_TIMESTAMP earliestContract { get; set; } = null!;
    }


    public class ROOT_COMPANY_DATA_REPRESENTATION : IValidation
    {
        [Range(0, int.MaxValue)]
        public int currentLevel { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY costNextLevel { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY contributedNextLevel { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY leftNextLevel { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY contributedTotal { get; set; } = null!;

        // Intentionally ignored
        public object[] contributors { get; set; } = null!;
    }
}
#pragma warning restore 1591