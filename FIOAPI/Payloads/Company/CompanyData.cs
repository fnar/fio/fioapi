﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Company
{
    public class MESG_COMPANY_DATA : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_COMPANY_DATA_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_COMPANY_DATA_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_COMPANY_DATA_MESSAGE message { get; set; } = null!;
    }

    public class MESG_COMPANY_DATA_MESSAGE : IValidation
    {
        [Equals("COMPANY_DATA")]
        public string messageType { get; set; } = null!;

        public MESG_COMPANY_DATA_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_COMPANY_DATA_PAYLOAD_INNER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string name { get; set; } = null!;

        [Uppercase]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string code { get; set; } = null!;

        [APEXID]
        public string countryId { get; set; } = null!;

        [StringLength(20, MinimumLength = 1)]
        public string startingProfile { get; set; } = null!;

        public APEX_COMMON_ADDRESS startingLocation { get; set; } = null!;

        public MESG_COMPANY_DATA_RATING_REPORT ratingReport { get; set; } = null!;

        public MESG_COMPANY_DATA_HQ headquarters { get; set; } = null!;
    }

    public class MESG_COMPANY_DATA_RATING_REPORT : IValidation
    {
        [StringLength(Constants.RatingLengthMax, MinimumLength = Constants.RatingLengthMin)]
        public string overallRating { get; set; } = null!;

        // Intentionally ignore this guy
        public object? subRatings { get; set; }
    }

    public class MESG_COMPANY_DATA_HQ : IValidation
    {
        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        [PositiveNumber]
        public int level { get; set; }

        [PositiveNumber]
        public int basePermits { get; set; }

        [PositiveNumber]
        public int usedBasePermits { get; set; }

        public MESG_COMPANY_DATA_HQ_INVENTORY inventory { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int additionalBasePermits { get; set; }

        [Range(0, 20)]
        public int additionalProductionQueueSlots { get; set; }

        public APEX_COMMON_TIMESTAMP? nextRelocationTime { get; set; } = null!;

        public bool relocationLocked { get; set; }

        public List<MESG_COMPANY_DATA_HQ_EFFICIENCY_GAIN> efficiencyGains { get; set; } = null!;

        public List<MESG_COMPANY_DATA_HQ_EFFICIENCY_GAIN> efficiencyGainsNextLevel { get; set; } = null!;
    }

    public class MESG_COMPANY_DATA_HQ_INVENTORY : IValidation
    {
        public List<MESG_COMPANY_DATA_HQ_INVENTORY_ITEM> items { get; set; } = null!;
    }

    public class MESG_COMPANY_DATA_HQ_INVENTORY_ITEM : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int amount { get; set; }

        [Range(0, int.MaxValue)]
        public int limit { get; set; }
    }


    public class MESG_COMPANY_DATA_HQ_EFFICIENCY_GAIN : IValidation
    {
        [BuildingCategory]
        public string category { get; set; } = null!;

        [Range(0.0, double.MaxValue)]
        public double gain { get; set; }
    }

}
#pragma warning restore 1591