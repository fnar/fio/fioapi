﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.CX
{
    public class CX_PRICES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public CX_PRICES_PAYLOAD payload { get; set; } = null!;
    }

    public class CX_PRICES_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public CX_PRICES_MESSAGE message { get; set; } = null!;
    }

    public class CX_PRICES_MESSAGE : IValidation
    {
        [Equals("COMEX_BROKER_DATA")]
        public string messageType { get; set; } = null!;

        public CX_PRICES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class CX_PRICES_PAYLOAD_INNER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(8, MinimumLength = 5)]
        public string ticker { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE exchange { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public APEX_COMMON_CURRENCY currency { get; set; } = null!;

        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY? price { get; set; }

        public APEX_COMMON_TIMESTAMP? priceTime { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? high { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? allTimeHigh { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? low { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? allTimeLow { get; set; }

        public CX_PRICES_ASK_OR_BID? ask { get; set; }

        public CX_PRICES_ASK_OR_BID? bid { get; set; }

        [Range(0, int.MaxValue)]
        public int? supply { get; set; }

        [Range(0, int.MaxValue)]
        public int? demand { get; set; }

        [Range(0, int.MaxValue)]
        public int? traded { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? volume { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY? priceAverage { get; set; } = null!;

        public CX_PRICES_PRICE_BAND? narrowPriceBand { get; set; } = null!;

        public CX_PRICES_PRICE_BAND? widePriceBand { get; set; } = null!;

        public List<CX_PRICES_ORDER> sellingOrders { get; set; } = null!;

        public List<CX_PRICES_ORDER> buyingOrders { get; set; } = null!;
    }

    public class CX_PRICES_ASK_OR_BID : IValidation
    {
        public APEX_COMMON_AMOUNT_AND_CURRENCY price { get; set; } = null!;

        [PositiveNumber]
        public int amount { get; set; }
    }

    public class CX_PRICES_PRICE_BAND : IValidation
    {
        [PositiveNumber]
        public double low { get; set; }

        [PositiveNumber]
        public double high { get; set; }
    }

    public class CX_PRICES_ORDER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE trader { get; set; } = null!;

        [PositiveNumber]
        public int? amount { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY limit { get; set; } = null!;
    }
}
#pragma warning restore 1591