﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.CX
{
    public class MESG_COMEX_BROKER_PRICES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_COMEX_BROKER_PRICES_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_COMEX_BROKER_PRICES_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_COMEX_BROKER_PRICES_MESSAGE message { get; set; } = null!;
    }

    public class MESG_COMEX_BROKER_PRICES_MESSAGE : IValidation
    {
        [Equals("COMEX_BROKER_PRICES")]
        public string messageType { get; set; } = null!;

        public MESG_COMEX_BROKER_PRICES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_COMEX_BROKER_PRICES_PAYLOAD_INNER : IValidation
    {
        [APEXID]
        public string brokerId { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP from { get; set; } = null!;

        public List<MESG_COMEX_BROKER_PRICES_PRICE> prices { get; set; } = null!;
    }


    public class MESG_COMEX_BROKER_PRICES_PRICE : IValidation
    {
        public string interval { get; set; } = null!;

        public List<MESG_COMEX_BROKER_PRICES_PRICE_DATA> prices { get; set; } = null!;

        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!Constants.ValidCXPCIntervals.Contains(interval))
            {
                Errors.Add($"{Context}.{nameof(interval)} with value '{interval}' is not a valid interval.");
            }
        }
    }

    public class MESG_COMEX_BROKER_PRICES_PRICE_DATA : IValidation
    {
        [APEXTimestamp]
        public long date { get; set; }

        public double open { get; set; }

        public double close { get; set; }

        public double high { get; set; }

        public double low { get; set; }

        public double volume { get; set; }

        public int traded { get; set; }
    }
}
#pragma warning restore 1591
