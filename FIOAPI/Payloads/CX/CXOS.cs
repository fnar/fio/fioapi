﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.CX
{
    public class MESG_COMEX_TRADER_ORDER : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_COMEX_TRADER_ORDER_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_COMEX_TRADER_ORDER_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_COMEX_TRADER_ORDER_MESSAGE message { get; set; } = null!;
    }

    public class MESG_COMEX_TRADER_ORDER_MESSAGE : IValidation
    {
        [Equals("COMEX_TRADER_ORDERS")]
        public string messageType { get; set; } = null!;

        public MESG_COMEX_TRADER_ORDER_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_COMEX_TRADER_ORDER_PAYLOAD_INNER : IValidation
    {
        public List<MESG_COMEX_TRADER_ORDER_ORDER> orders { get; set; } = null!;
    }

    public class MESG_COMEX_TRADER_ORDER_ORDER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE exchange { get; set; } = null!;

        [APEXID]
        public string brokerId { get; set; } = null!;

        [StringLength(7, MinimumLength = 6)]
        public string type { get; set; } = null!;

        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int amount { get; set; }

        [PositiveNumber]
        public int initialAmount { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY limit { get; set; } = null!;

        public string status { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP created { get; set; } = null!;

        public List<MESG_COMEX_TRADER_ORDER_TRADE> trades { get; set; } = null!;
    }

    public class MESG_COMEX_TRADER_ORDER_TRADE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [PositiveNumber]
        public int amount { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY price { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE partner { get; set; } = null!;
    }


}
#pragma warning restore 1591