﻿#pragma warning disable 1591

namespace FIOAPI.Payloads.Global
{
    public class PATH_COMMODITY_EXCHANGES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PATH_COMMODITY_EXCHANGES_PAYLOAD_OUTER payload { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_PAYLOAD_OUTER : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PATH_COMMODITY_EXCHANGES_MESSAGE message { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PATH_COMMODITY_EXCHANGES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_PAYLOAD_INNER : IValidation
    {
        [NotEmpty]
        public List<PATH_COMMODITY_EXCHANGES_BODY> body { get; set; } = null!;

        [ContainerLength(1)]
        [Equals("commodityexchanges", OffsetIndex:0)]
        public string[] path { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_BODY : IValidation
    {
        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        [Ticker]
        [StringLength(3, MinimumLength = 3)]
        public string code { get; set; } = null!;

        public APEX_COMMON_CURRENCY currency { get; set; } = null!;

        public PATH_COMMODITY_EXCHANGES_ADDRESS address { get; set; } = null!;

        public string id { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_OPERATOR : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [Ticker]
        public string code { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        public string _type { get; set; } = null!;

        [APEXID]
        public string _proxy_key { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_ADDRESS : IValidation
    {
        [NotEmpty]
        public List<PATH_COMMODITY_EXCHANGES_LINE> lines { get; set; } = null!;
    }

    public class PATH_COMMODITY_EXCHANGES_LINE : IValidation
    {
        public APEX_COMMON_ADDRESS_ENTITY entity { get; set; } = null!;

        public string type { get; set; } = null!;
    }
}
#pragma warning restore 1591