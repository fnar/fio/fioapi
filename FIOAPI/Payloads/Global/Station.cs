﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Global
{
    public class PATH_STATION : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public PATH_STATION_PAYLOAD_OUTER payload { get; set; } = null!;
    }

    public class PATH_STATION_PAYLOAD_OUTER : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public PATH_STATION_MESSAGE message { get; set; } = null!;
    }

    public class PATH_STATION_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public PATH_STATION_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class PATH_STATION_PAYLOAD_INNER : IValidation
    {
        public PATH_STATION_BODY body { get; set; } = null!;

        [ContainerLength(2)]
        [Equals("stations", OffsetIndex:0)]
        public string[] path { get; set; } = null!;
    }

    public class PATH_STATION_BODY : IValidation
    {
        public string naturalId { get; set; } = null!;

        public string name { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP commissioningTime { get; set; } = null!;

        public PATH_STATION_COMEX comex { get; set; } = null!;

        public string warehouseId { get; set; } = null!;

        public string localMarketId { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE country { get; set; } = null!;

        public APEX_COMMON_CURRENCY currency { get; set; } = null!;

        public PATH_STATION_GOVERNING_ENTITY governingEntity { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;
    }

    public class PATH_STATION_COMEX : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        [Ticker]
        public string code { get; set; } = null!;

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;
    }

    public class PATH_STATION_GOVERNING_ENTITY : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [Ticker]
        public string code { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;
    }
}
#pragma warning restore 1591