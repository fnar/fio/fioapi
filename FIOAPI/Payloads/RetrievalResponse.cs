﻿namespace FIOAPI.Payloads
{
    /// <summary>
    /// An individual response
    /// </summary>
    /// <typeparam name="T">Data type</typeparam>
    public class IndividualUserResponse<T> where T : notnull, new()
    {
        /// <summary>
        /// Any errors with the associated username
        /// </summary>
        public List<string> Errors { get; set; } = new();

        /// <summary>
        /// The data
        /// </summary>
        public T Data { get; set; } = new();
    }

    /// <summary>
    /// A grouped response
    /// </summary>
    /// <typeparam name="T">Data type</typeparam>
    public class RetrievalResponse<T> where T : notnull, new()
    {
        /// <summary>
        /// All responses
        /// </summary>
        public Dictionary<string, IndividualUserResponse<T>> UserNameToData { get; set; } = new();
    }

    /// <summary>
    /// Retrieval Helper
    /// </summary>
    public static class Retrieval
    {
        /// <summary>
        /// Constructs a RetreivalResponse with the inner data type being a list of T
        /// </summary>
        /// <typeparam name="T">The type (of the inner of the list)</typeparam>
        /// <param name="UserToData">UserToData dictionary</param>
        /// <param name="UserToPerm">UserToPerm dictionary</param>
        /// <returns>A RetrievalResponse object</returns>
        public static RetrievalResponse<List<T>> CreateSanitizedResponse<T>(Dictionary<string, List<T>> UserToData, Dictionary<string, Perm> UserToPerm) where T : notnull, new()
        {
            RetrievalResponse<List<T>> Response = new();
            foreach (var kvp in UserToData)
            {
                var UserErrors = new List<string>();
                var User = kvp.Key;
                var Perm = UserToPerm[User];
                var DataList = kvp.Value;

                for (int DataIdx = 0; DataIdx < DataList.Count; ++DataIdx)
                {
                    var Data = DataList[DataIdx]!;
                    if (PermissionSanitizer.ApplyPermissionSanitizations(Perm, ref Data, ref UserErrors))
                    {
                        DataList[DataIdx] = Data;
                    }
                    else
                    {
                        // We can't see the top-level object, so bail
                        break;
                    }
                }

                Response.UserNameToData.Add(User, new IndividualUserResponse<List<T>>
                {
                    Errors = UserErrors,
                    Data = DataList
                });
            }

            // Find any UserNames in UserToPerm that aren't in UserToData and create Error responses for those
            var MissingUsers = UserToPerm.Keys.Except(UserToData.Keys).ToList();
            foreach (var MissingUser in MissingUsers)
            {
                Response.UserNameToData.Add(MissingUser, new IndividualUserResponse<List<T>>
                {
                    Errors = new List<string> { "User or Data not found" },
                });
            }

            return Response;
        }

        /// <summary>
        /// Constructs a RetreivalResponse with the data type being T
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="UserToData">UserToData dictionary</param>
        /// <param name="UserToPerm">UserToPerm dictionary</param>
        /// <returns>A RetrievalResponse object</returns>
        public static RetrievalResponse<T> CreateSanitizedResponse<T>(Dictionary<string, T> UserToData, Dictionary<string, Perm> UserToPerm) where T : notnull, new()
        {
            RetrievalResponse<T> Response = new();
            foreach (var kvp in UserToData)
            {
                var UserErrors = new List<string>();
                var User = kvp.Key;
                var Perm = UserToPerm[User];
                var Data = kvp.Value;
                if (!PermissionSanitizer.ApplyPermissionSanitizations(Perm, ref Data, ref UserErrors))
                {
                    Data = new T();
                }

                Response.UserNameToData.Add(User, new IndividualUserResponse<T>
                {
                    Errors = UserErrors,
                    Data = Data
                });
            }

            // Find any UserNames in UserToPerm that aren't in UserToData and create Error responses for those
            var MissingUsers = UserToPerm.Keys.Except(UserToData.Keys).ToList();
            foreach (var MissingUser in MissingUsers)
            {
                Response.UserNameToData.Add(MissingUser, new IndividualUserResponse<T>
                {
                    Errors = new List<string> { "User or Data not found" },
                });
            }

            return Response;
        }
    }
}
