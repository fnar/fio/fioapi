﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Chat
{
    public class CHANNEL_USER_JOINED : IValidation
    {
        [Equals("CHANNEL_USER_JOINED")]
        public string messageType { get; set; } = null!;

        public CHANNEL_USER_JOINED_PAYLOAD payload { get; set; } = null!;
    }

    public class CHANNEL_USER_JOINED_PAYLOAD : IValidation
    {
        [APEXID]
        public string channelId { get; set; } = null!;

        public CHANNEL_USER_JOINED_USER? user { get; set; } = null;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;

        [APEXID]
        public string messageId { get; set; } = null!;
    }

    public class CHANNEL_USER_JOINED_USER : IValidation
    {
        public APEX_COMMON_USER user { get; set; } = null!;
    }
}
#pragma warning restore 1591