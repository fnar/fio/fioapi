﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Chat
{
    #region MESG_CHANNEL_DATA

    public class MESG_CHANNEL_DATA : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_CHANNEL_DATA_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_CHANNEL_DATA_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;
        public int status { get; set; }
        public MESG_CHANNEL_DATA_MESSAGE message { get; set; } = null!;
    }

    public class MESG_CHANNEL_DATA_MESSAGE : IValidation
    {
        [Equals("CHANNEL_DATA")]
        public string messageType { get; set; } = null!;

        public SHARED_CHANNEL_DATA_PAYLOAD payload { get; set; } = null!;
    }
    #endregion

    #region ROOT_CHANNEL_DATA
    public class ROOT_CHANNEL_DATA : IValidation
    {
        [Equals("CHANNEL_DATA")]
        public string messageType { get; set; } = null!;

        public SHARED_CHANNEL_DATA_PAYLOAD payload { get; set; } = null!;
    }
    #endregion

    #region SHARED_CHANNEL_DATA_PAYLOAD
    public class SHARED_CHANNEL_DATA_PAYLOAD : IValidation
    {
        [APEXID]
        public string channelId { get; set; } = null!;

        [StringLength(10, MinimumLength = 5)]
        public string type { get; set; } = null!;

        [StringLength(10)]
        public string? naturalId { get; set; } = null;

        [StringLength(64)]
        public string? displayName { get; set; } = null;

        public APEX_COMMON_TIMESTAMP creationTime { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP lastActivity { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int userCount { get; set; }

        public int maxMessageLength { get; set; }

        public SHARED_CHANNEL_DATA_PERMISSIONS? permissions { get; set; } = null;

        public object[]? bans { get; set; } = null;
    }


    public class SHARED_CHANNEL_DATA_PERMISSIONS : IValidation
    {
        public bool add { get; set; }
        public bool leave { get; set; }
    }
    #endregion
}
#pragma warning restore 1591