﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Chat
{
    public class CHANNEL_USER_LEFT : IValidation
    {
        [Equals("CHANNEL_USER_LEFT")]
        public string messageType { get; set; } = null!;

        public CHANNEL_USER_LEFT_PAYLOAD payload { get; set; } = null!;
    }

    public class CHANNEL_USER_LEFT_PAYLOAD : IValidation
    {
        [APEXID]
        public string channelId { get; set; } = null!;

        public CHANNEL_USER_LEFT_USER user { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;

        [APEXID]
        public string messageId { get; set; } = null!;
    }

    public class CHANNEL_USER_LEFT_USER : IValidation
    {
        public CHANNEL_USER_LEFT_USER_DETAILS user { get; set; } = null!;
    }

    public class CHANNEL_USER_LEFT_USER_DETAILS : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(32, MinimumLength = 3)]
        public string username { get; set; } = null!;

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;
    }
}
#pragma warning restore 1591