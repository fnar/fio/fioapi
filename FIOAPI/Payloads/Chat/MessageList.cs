﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Chat
{
    public class CHANNEL_MESSAGE_LIST : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public CHANNEL_MESSAGE_LIST_PAYLOAD_OUTER payload { get; set; } = null!;
    }

    public class CHANNEL_MESSAGE_LIST_PAYLOAD_OUTER : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public CHANNEL_MESSAGE_LIST_MESSAGE_OUTER message { get; set; } = null!;
    }

    public class CHANNEL_MESSAGE_LIST_MESSAGE_OUTER : IValidation
    {
        [Equals("CHANNEL_MESSAGE_LIST")]
        public string messageType { get; set; } = null!;

        public CHANNEL_MESSAGE_LIST_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class CHANNEL_MESSAGE_LIST_PAYLOAD_INNER : IValidation
    {
        [APEXID]
        public string channelId { get; set; } = null!;

        public CHANNEL_MESSAGE_LIST_MESSAGE_INNER[] messages { get; set; } = null!;

        public bool hasMore { get; set; }
    }

    public class CHANNEL_MESSAGE_LIST_MESSAGE_INNER : IValidation
    {
        [APEXID]
        public string messageId { get; set; } = null!;

        [StringLength(32, MinimumLength = 2)]
        public string type { get; set; } = null!;

        public APEX_COMMON_USER sender { get; set; } = null!;

        [StringLength(1024)]
        public string? message { get; set; } = null;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;

        [APEXID]
        public string channelId { get; set; } = null!;

        public APEX_COMMON_USER? deletingUser { get; set; } = null;
    }
}
#pragma warning restore 1591