﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Chat
{
    #region MESG_CHANNEL_MESSAGE_ADDED
    public class MESG_CHANNEL_MESSAGE_ADDED : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_CHANNEL_MESSAGE_ADDED_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_CHANNEL_MESSAGE_ADDED_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_CHANNEL_MESSAGE_ADDED_MESSAGE message { get; set; } = null!;
    }

    public class MESG_CHANNEL_MESSAGE_ADDED_MESSAGE : IValidation
    {
        [Equals("CHANNEL_MESSAGE_ADDED")]
        public string messageType { get; set; } = null!;

        public SHARED_MESSAGE_ADDED payload { get; set; } = null!;
    }
    #endregion

    #region ROOT_CHANNEL_MESSAGE_ADDED
    public class ROOT_CHANNEL_MESSAGE_ADDED : IValidation
    {
        [Equals("CHANNEL_MESSAGE_ADDED")]
        public string messageType { get; set; } = null!;

        public SHARED_MESSAGE_ADDED payload { get; set; } = null!;
    }
    #endregion


    #region SHARED_MESSAGE_ADDED
    public class SHARED_MESSAGE_ADDED : IValidation
    {
        [APEXID]
        public string messageId { get; set; } = null!;

        [StringLength(32, MinimumLength = 2)]
        public string type { get; set; } = null!;

        public APEX_COMMON_USER sender { get; set; } = null!;

        [StringLength(1024)]
        public string message { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP time { get; set; } = null!;

        [APEXID]
        public string channelId { get; set; } = null!;

        public APEX_COMMON_USER? deletingUser { get; set; } = null;
    }
    #endregion
}
#pragma warning restore 1591
