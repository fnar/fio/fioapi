﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Storage
{
    public class WAREHOUSE_STORAGES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public WAREHOUSE_STORAGES_PAYLOAD payload { get; set; } = null!;
    }

    public class WAREHOUSE_STORAGES_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public WAREHOUSE_STORAGES_MESSAGES message { get; set; } = null!;
    }

    public class WAREHOUSE_STORAGES_MESSAGES : IValidation
    {
        [Equals("WAREHOUSE_STORAGES")]
        public string messageType { get; set; } = null!;

        public WAREHOUSE_STORAGES_INNER_PAYLOAD payload { get; set; } = null!;
    }

    public class WAREHOUSE_STORAGES_INNER_PAYLOAD : IValidation
    {
        public List<WAREHOUSE_STORAGES_STORAGE> storages { get; set; } = null!;
    }

    public class WAREHOUSE_STORAGES_STORAGE : IValidation
    {
        [APEXID]
        public string warehouseId { get; set; } = null!;

        [APEXID]
        public string storeId { get; set; } = null!;

        [PositiveNumber]
        public int units { get; set; }

        [PositiveNumber]
        public double weightCapacity { get; set; }

        [PositiveNumber]
        public double volumeCapacity { get; set; }

        public APEX_COMMON_TIMESTAMP nextPayment { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY? fee { get; set; }

        public APEX_COMMON_USER_AND_CODE_NULLABLE? feeCollector { get; set; }

        public string status { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;
    }
}
#pragma warning restore 1591