﻿#pragma warning disable 1591
using System.Linq;

namespace FIOAPI.Payloads.Storage
{
    public class STORAGE_STORAGES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public STORAGE_STORAGES_PAYLOAD payload { get; set; } = null!;
    }

    public class STORAGE_STORAGES_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public STORAGE_STORAGES_MESSAGE message { get; set; } = null!;
    }

    public class STORAGE_STORAGES_MESSAGE : IValidation
    {
        [Equals("STORAGE_STORAGES")]
        public string messageType { get; set; } = null!;

        public STORAGE_STORAGES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class STORAGE_STORAGES_PAYLOAD_INNER : IValidation
    {
        public List<STORAGE_STORAGES_STORE> stores { get; set; } = null!;
    }

    public class STORAGE_STORAGES_STORE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string addressableId { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string? name { get; set; }

        [Range(0.0, double.MaxValue)]
        public double weightLoad { get; set; }

        [PositiveNumber]
        public double weightCapacity { get; set; }

        [Range(0.0, double.MaxValue)]
        public double volumeLoad { get; set; }

        [PositiveNumber]
        public double volumeCapacity { get; set; }

        public List<STORAGE_STORAGES_STORE_ITEM> items { get; set; } = null!;

        public bool @fixed { get; set; }            // If it's a fixed store

        public bool tradeStore { get; set; }        // If you can trade from it (only fuel tanks can't)

        public int rank { get; set; }               // Priority?

        public bool locked { get; set; }            // For warehouses

        public string type { get; set; } = null!;   // INVENTORY, WAREHOUSE_STORE, STL_FUEL_STORE, FTL_FUEL_STORE

        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!Constants.StoreTypeEnumToStoreType.Values.Contains(type))
            {
                Errors.Add($"'{Context}.{nameof(type)}' with value '{type}' is not a valid store type");
            }
        }
    }

    public class STORAGE_STORAGES_STORE_ITEM : IValidation
    {
        public STORAGE_STORAGES_STORE_QUANTITY? quantity { get; set; }

        [APEXID]
        public string id { get; set; } = null!;

        public string type { get; set; } = null!;

        [PositiveNumber]
        public double weight { get; set; }

        [PositiveNumber]
        public double volume { get; set; }
    }

    public class STORAGE_STORAGES_STORE_QUANTITY : IValidation
    {
        public STORAGE_STORAGES_AMOUNT_AND_CURRENCY value { get; set; } = null!;

        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [PositiveNumber]
        public int amount { get; set; }
    }

    public class STORAGE_STORAGES_AMOUNT_AND_CURRENCY : IValidation
    {
        public double amount { get; set; }

        [CurrencyCode]
        public string currency { get; set; } = null!;
    }
}
#pragma warning restore 1591