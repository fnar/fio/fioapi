﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Storage
{
    public class ROOT_STORAGE_CHANGE : IValidation
    {
        [Equals("STORAGE_CHANGE")]
        public string messageType { get; set; } = null!;

        public ROOT_STORAGE_CHANGE_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_STORAGE_CHANGE_PAYLOAD
    {
        public List<STORAGE_STORAGES_STORE> stores { get; set; } = null!;
    }
}
#pragma warning restore 1591