﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Map
{
    public class MAP_SYSTEM : IValidation
    {
        public string messageType { get; set; } = null!;

        public MAP_SYSTEM_PAYLOAD payload { get; set; } = null!;
    }

    public class MAP_SYSTEM_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MAP_SYSTEM_MESSAGE message { get; set; } = null!;
    }

    public class MAP_SYSTEM_MESSAGE : IValidation
    {
        [Equals("DATA_DATA")]
        public string messageType { get; set; } = null!;

        public MAP_SYSTEM_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MAP_SYSTEM_PAYLOAD_INNER : IValidation
    {
        public MAP_SYSTEM_BODY body { get; set; } = null!;

        [ContainerLength(2)]
        [Equals("systems", OffsetIndex:0)]
        public List<string> path { get; set; } = null!;
    }

    public class MAP_SYSTEM_BODY : IValidation
    {
        [NaturalID]
        public string naturalId { get; set; } = null!;

        public string name { get; set; } = null!;

        public APEX_COMMON_USER? namer { get; set; }

        [APEXTimestamp]
        public APEX_COMMON_TIMESTAMP? namingDate { get; set; }

        public bool nameable { get; set; }

        public MAP_SYSTEM_STAR star { get; set; } = null!;

        [PositiveNumber]
        public double meteoroidDensity { get; set; }

        [NotEmpty]
        public List<MAP_SYSTEM_PLANET> planets { get; set; } = null!;

        public List<MAP_SYSTEM_CELESTIAL_BODY> celestialBodies { get; set; } = null!;

        [NotEmpty]
        public string[] connections { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE? country { get; set; }

        public APEX_COMMON_CURRENCY? currency { get; set; }

        [APEXID]
        public string id { get; set; } = null!;
    }

    public class MAP_SYSTEM_STAR : IValidation
    {
        [StringLength(1, MinimumLength = 1)]
        public string type { get; set; } = null!;

        [PositiveNumber]
        public double luminosity { get; set; }

        public MAP_SYSTEM_STAR_POSITION position { get; set; } = null!;

        [StringLength(10, MinimumLength = 8)]
        public string sectorId { get; set; } = null!;

        [StringLength(20, MinimumLength = 13)]
        public string subSectorId { get; set; } = null!;

        [PositiveNumber]
        public double mass { get; set; }

        [PositiveNumber]
        public double massSol { get; set; }

        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!sectorId.StartsWith("sector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'sector-'");
            }

            if (!subSectorId.StartsWith("subsector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'subsector-'");
            }
        }
    }

    public class MAP_SYSTEM_STAR_POSITION : IValidation
    {
        public double x { get; set; }

        public double y { get; set; }

        public double z { get; set; }
    }

    public class MAP_SYSTEM_CELESTIAL_BODY : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [NaturalID]
        public string naturalId { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        public MAP_SYSTEM_ORBIT orbit { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;
    }

    public class MAP_SYSTEM_PLANET : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [NaturalID]
        public string naturalId { get; set; } = null!;

        public MAP_SYSTEM_ORBIT orbit { get; set; } = null!;

        [PositiveNumber]
        public double mass { get; set; }

        [PositiveNumber]
        public double massEarth { get; set; }

        public bool surface { get; set; }

        public APEX_COMMON_ADDRESS address { get; set; } = null!;
    }

    public class MAP_SYSTEM_ORBIT : IValidation
    {
        public double semiMajorAxis { get; set; }

        public double eccentricity { get; set; }

        public double inclination { get; set; }

        public double rightAscension { get; set; }

        public double periapsis { get; set; }
    }
}
#pragma warning restore 1591