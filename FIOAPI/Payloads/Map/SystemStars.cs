﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Map
{
    public class SYSTEM_STARS : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public SYSTEM_STARS_PAYLOAD payload { get; set; } = null!;
    }

    public class SYSTEM_STARS_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public SYSTEM_STARS_MESSAGE message { get; set; } = null!;
    }

    public class SYSTEM_STARS_MESSAGE : IValidation
    {
        [Equals("SYSTEM_STARS_DATA")]
        public string messageType { get; set; } = null!;

        public SYSTEM_STARS_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class SYSTEM_STARS_PAYLOAD_INNER : IValidation
    {
        [NotEmpty]
        public List<SYSTEM_STARS_STAR> stars { get; set; } = null!;
    }

    public class SYSTEM_STARS_STAR : IValidation
    {
        [APEXID]
        public string systemId { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public string name { get; set; } = null!;

        [StringLength(1, MinimumLength = 1)]
        public string type { get; set; } = null!;

        public Position position { get; set; } = null!;

        [StringLength(10, MinimumLength = 8)]
        public string sectorId { get; set; } = null!;

        [StringLength(20, MinimumLength = 13)]
        public string subSectorId { get; set; } = null!;

        [NotEmpty]
        public List<string> connections { get; set; } = null!;

        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!sectorId.StartsWith("sector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'sector-'");
            }

            if (!subSectorId.StartsWith("subsector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'subsector-'");
            }
        }
    }

    public class Position : IValidation
    {
        public double x { get; set; }

        public double y { get; set; }

        public double z { get; set; }
    }
}
#pragma warning restore 1591