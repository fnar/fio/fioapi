﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Map
{
    public class MAP_WORLD_SECTORS : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MAP_WORLD_SECTORS_PAYLOAD payload { get; set; } = null!;
    }

    public class MAP_WORLD_SECTORS_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MAP_WORLD_SECTORS_MESSAGE message { get; set; } = null!;
    }

    public class MAP_WORLD_SECTORS_MESSAGE : IValidation
    {
        [Equals("WORLD_SECTORS")]
        public string messageType { get; set; } = null!;

        public MAP_WORLD_SECTORS_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MAP_WORLD_SECTORS_PAYLOAD_INNER : IValidation
    {
        public List<MAP_WORLD_SECTORS_SECTOR> sectors { get; set; } = null!;
    }

    public class MAP_WORLD_SECTORS_SECTOR : IValidation
    {
        [StringLength(10, MinimumLength = 8)]
        public string id { get; set; } = null!;

        public string? name { get; set; }

        public MAP_WORLD_SECTORS_HEX hex { get; set; } = null!;

        public int size { get; set; }

        [NotEmpty]
        public List<MAP_WORLD_SECTORS_SUBSECTOR> subsectors { get; set; } = null!;

        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!id.StartsWith("sector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'sector-'");
            }
        }
    }

    public class MAP_WORLD_SECTORS_HEX : IValidation
    {
        public int q { get; set; }

        public int r { get; set; }

        public int s { get; set; }
    }

    public class MAP_WORLD_SECTORS_SUBSECTOR : IValidation
    {
        [StringLength(20, MinimumLength = 13)]
        public string id { get; set; } = null!;

        [NotEmpty]
        public List<MAP_WORLD_SECTORS_VERTEX> vertices { get; set; } = null!;

        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!id.StartsWith("subsector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'subsector-'");
            }
        }
    }

    public class MAP_WORLD_SECTORS_VERTEX : IValidation
    {
        public double x { get; set; }

        public double y { get; set; }

        public double z { get; set; }
    }
}
#pragma warning restore 1591