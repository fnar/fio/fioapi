﻿
namespace FIOAPI.Payloads.Auth
{
    /// <summary>
    /// SetDiscord
    /// </summary>
    public class SetDiscord : IValidation
    {
        /// <summary>
        /// DiscordName
        /// </summary>
        [StringLength(32, MinimumLength = 0)]
        public string DiscordName { get; set; } = "";

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">ref Errors</param>
        /// <param name="Context">Current context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (DiscordName.Length == 1)
            {
                Errors.Add($"{Context}.{nameof(DiscordName)} must be empty or the length be in the range [2,32]");
            }
        }
    }
}
