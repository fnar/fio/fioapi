﻿namespace FIOAPI.Payloads.Auth
{
    /// <summary>
    /// The response from a login request
    /// </summary>
    public class LoginResponse : IValidation
    {
        /// <summary>
        /// The Jwt Bearer token
        /// </summary>
        /// <remarks>
        /// This token
        /// </remarks>
        /// <example>eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c</example>
        [StringLength(1024)]
        public string Token { get; set; } = "";

        /// <summary>
        /// If users is admin
        /// </summary>
        public bool IsAdministrator { get; set; } = false;
    }
}
