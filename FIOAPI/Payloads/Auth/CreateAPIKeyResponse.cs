﻿namespace FIOAPI.Payloads.Auth
{
    /// <summary>
    /// The CreateAPI Response Payload
    /// </summary>
    public class CreateAPIKeyResponse : IValidation
    {
        /// <summary>
        /// The newly created APIKey
        /// </summary>
        [GuidValid]
        public Guid APIKey { get; set; }
    }
}
