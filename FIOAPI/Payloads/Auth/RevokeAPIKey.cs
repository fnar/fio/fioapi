﻿namespace FIOAPI.Payloads.Auth
{
    /// <summary>
    /// RevokeAPIKey Payload
    /// </summary>
    public class RevokeAPIKey : IValidation
    {
        /// <summary>
        /// Username
        /// </summary>
        /// <example>Saganaki</example>
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = "";

        /// <summary>
        /// Password
        /// </summary>
        /// <example>Hunter2</example>
        [StringLength(256, MinimumLength = 3)]
        public string Password { get; set; } = "";

        /// <summary>
        /// APIKeyToRevoke
        /// </summary>
        /// <Example>a2da80a60f2c44ac84bd2abbe3d39936</Example>
        [GuidValid]
        public Guid APIKeyToRevoke { get; set; }
    }
}
