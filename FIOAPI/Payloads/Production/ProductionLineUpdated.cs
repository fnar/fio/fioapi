﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Production
{
    public class ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED : IValidation
    {
        [Equals("PRODUCTION_PRODUCTION_LINE_UPDATED")]
        public string messageType { get; set; } = null!;

        public ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_PAYLOAD : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string siteId { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public string type { get; set; } = null!;

        [PositiveNumber]
        public int capacity { get; set; }

        [PositiveNumber]
        public int slots { get; set; }

        [Range(0.0, double.MaxValue)]
        public double efficiency { get; set; }

        [Range(0.0, 1.0)]
        public double condition { get; set; }

        public List<ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_WORKFORCE>? workforces { get; set; } = null!;

        public List<ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_ORDER>? orders { get; set; }

        public List<ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_PRODUCTION_TEMPLATE>? productionTemplates { get; set; }

        public List<ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_EFFICIENCY_FACTOR>? efficiencyFactors { get; set; }
    }

    public class ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_WORKFORCE : IValidation
    {
        [WorkforceLevel]
        public string level { get; set; } = null!;

        [Range(0.0, double.MaxValue)]
        public double efficiency { get; set; }
    }

    public class ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_ORDER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string productionLineId { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT>? inputs { get; set; }

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> outputs { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP created { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP? started { get; set; }

        public APEX_COMMON_TIMESTAMP? completion { get; set; }

        public APEX_COMMON_DURATION duration { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP? lastUpdated { get; set; }

        [Range(0.0, 1.0)]
        public double completed { get; set; }

        public bool halted { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? productionFee { get; set; }

        public APEX_COMMON_USER_AND_CODE_NULLABLE? productionFeeCollector { get; set; }

        public bool recurring { get; set; }
    }

    public class ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_PRODUCTION_TEMPLATE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string name { get; set; } = null!;

        public List<ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_FACTOR>? inputFactors { get; set; }

        public List<ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_FACTOR>? outputFactors { get; set; }

        [Range(0.0, double.MaxValue)]
        public double effortFactor { get; set; }

        [Range(0.0, double.MaxValue)]
        public double efficiency { get; set; }

        public APEX_COMMON_DURATION duration { get; set; } = null!;

        public ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_PRODUCTION_FEE_FACTOR productionFeeFactor { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE_NULLABLE? productionFeeCollector { get; set; }
    }

    public class ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_PRODUCTION_FEE_FACTOR : IValidation
    {
        [Range(0.0, double.MaxValue)]
        public double amount { get; set; }

        [CurrencyCode]
        public string currency { get; set; } = null!;
    }

    public class ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_FACTOR : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0.0, double.MaxValue)]
        public double factor { get; set; }
    }

    public class ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED_EFFICIENCY_FACTOR : IValidation
    {
        public string? expertiseCategory { get; set; } = null!;

        public string type { get; set; } = null!;

        public double effectivity { get; set; }

        public double value { get; set; }
    }
}
#pragma warning restore 1591
