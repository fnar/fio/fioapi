﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Production
{
    public class MESG_PRODUCTION_PRODUCTION_LINE : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_PRODUCTION_PRODUCTION_LINE_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_PRODUCTION_PRODUCTION_LINE_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_PRODUCTION_PRODUCTION_LINE_MESSAGE message { get; set; } = null!;
    }

    public class MESG_PRODUCTION_PRODUCTION_LINE_MESSAGE : IValidation
    {
        [Equals("PRODUCTION_PRODUCTION_LINE")]
        public string messageType { get; set; } = null!;

        public MESG_PRODUCTION_PRODUCTION_LINE_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_PRODUCTION_PRODUCTION_LINE_PAYLOAD_INNER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string siteId { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public string type { get; set; } = null!;

        [PositiveNumber]
        public int capacity { get; set; }

        [PositiveNumber]
        public int slots { get; set; }

        [Range(0.0, 5.0)]
        public double efficiency { get; set; }

        [Range(0.0, 1.0)]
        public double condition { get; set; }

        public List<MESG_PRODUCTION_PRODUCTION_LINE_WORKFORCE> workforces { get; set; } = null!;

        public List<MESG_PRODUCTION_PRODUCTION_LINE_ORDER> orders { get; set; } = null!;

        public List<MESG_PRODUCTION_PRODUCTION_LINE_PRODUCTION_TEMPLATE> productionTemplates { get; set; } = null!;

        public List<MESG_PRODUCTION_PRODUCTION_LINE_EFFICIENCY_FACTOR> efficiencyFactors { get; set; } = null!;
    }

    public class MESG_PRODUCTION_PRODUCTION_LINE_WORKFORCE : IValidation
    {
        public string level { get; set; } = null!;

        [Range(0.0, 1.0)]
        public double efficiency { get; set; }
    }

    public class MESG_PRODUCTION_PRODUCTION_LINE_ORDER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string productionLineId { get; set; } = null!;

        public List<MESG_PRODUCTION_PRODUCTION_LINE_INPUT_OR_OUTPUT> inputs { get; set; } = null!;

        public List<MESG_PRODUCTION_PRODUCTION_LINE_INPUT_OR_OUTPUT> outputs { get; set; } = null!;

        [ValidTimestamp]
        public APEX_COMMON_TIMESTAMP created { get; set; } = null!;

        [ValidTimestamp]
        public APEX_COMMON_TIMESTAMP? started { get; set; }

        [ValidTimestamp]
        public APEX_COMMON_TIMESTAMP? completion { get; set; }

        public APEX_COMMON_DURATION duration { get; set; } = null!;

        [ValidTimestamp]
        public APEX_COMMON_TIMESTAMP? lastUpdated { get; set; }

        [Range(0.0, 1.0)]
        public double completed { get; set; }

        public bool halted { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? productionFee { get; set; }

        public APEX_COMMON_USER_AND_CODE_NULLABLE? productionFeeCollector { get; set; }

        public bool recurring { get; set; }
    }

    public class MESG_PRODUCTION_PRODUCTION_LINE_INPUT_OR_OUTPUT : IValidation
    {
        public APEX_COMMON_AMOUNT_AND_CURRENCY value { get; set; } = null!;

        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [PositiveNumber]
        public int amount { get; set; }
    }

    public class MESG_PRODUCTION_PRODUCTION_LINE_PRODUCTION_TEMPLATE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string name { get; set; } = null!;

        public List<MESG_PRODUCTION_PRODUCTION_LINE_PRODUCTION_FACTOR> inputFactors { get; set; } = null!;

        public List<MESG_PRODUCTION_PRODUCTION_LINE_PRODUCTION_FACTOR> outputFactors { get; set; } = null!;

        [Range(0.0, 1.0)]
        public double effortFactor { get; set; }

        [Range(0.0, 5.0)]
        public double efficiency { get; set; }

        public APEX_COMMON_DURATION duration { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY productionFeeFactor { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE_NULLABLE productionFeeCollector { get; set; } = null!;
    }

    public class MESG_PRODUCTION_PRODUCTION_LINE_PRODUCTION_FACTOR : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0.0, double.MaxValue)]
        public double factor { get; set; }
    }

    public class MESG_PRODUCTION_PRODUCTION_LINE_EFFICIENCY_FACTOR : IValidation
    {
        public string? expertiseCategory { get; set; } = null!;

        public string type { get; set; } = null!;

        public int effectivity { get; set; }

        public double value { get; set; }
    }

}
#pragma warning restore 1591