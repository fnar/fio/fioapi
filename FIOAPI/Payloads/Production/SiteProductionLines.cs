﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Production
{
    public class SITE_PRODUCTION_LINES : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public SITE_PRODUCTION_LINES_PAYLOAD payload { get; set; } = null!;
    }

    public class SITE_PRODUCTION_LINES_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public SITE_PRODUCTION_LINES_MESSAGE message { get; set; } = null!;
    }

    public class SITE_PRODUCTION_LINES_MESSAGE : IValidation
    {
        [Equals("PRODUCTION_SITE_PRODUCTION_LINES")]
        public string messageType { get; set; } = null!;

        public SITE_PRODUCTION_LINES_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class SITE_PRODUCTION_LINES_PAYLOAD_INNER : IValidation
    {
        [APEXID]
        public string siteId { get; set; } = null!;

        public List<SITE_PRODUCTION_LINES_PRODUCTION_LINE> productionLines { get; set; } = null!;
    }

    public class SITE_PRODUCTION_LINES_PRODUCTION_LINE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string siteId { get; set; } = null!;

        public APEX_COMMON_ADDRESS address { get; set; } = null!;

        public string type { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int capacity { get; set; }

        [Range(0, int.MaxValue)]
        public int slots { get; set; }

        [Range(0.0, 5.0)]
        public double efficiency { get; set; }

        [Range(0.0, 100.0)]
        public double condition { get; set; }

        public List<SITE_PRODUCTION_LINES_WORKFORCE> workforces { get; set; } = null!;

        public List<SITE_PRODUCTION_LINES_ORDER> orders { get; set; } = null!;

        public List<SITE_PRODUCTION_LINES_PRODUCTION_TEMPLATE> productionTemplates { get; set; } = null!;

        public List<SITE_PRODUCTION_LINES_EFFICIENCY_FACTOR> efficiencyFactors { get; set; } = null!;
    }

    public class SITE_PRODUCTION_LINES_WORKFORCE : IValidation
    {
        [WorkforceLevel]
        public string level { get; set; } = null!;

        [Range(0.0, 5.0)]
        public double efficiency { get; set; }
    }

    public class SITE_PRODUCTION_LINES_ORDER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string productionLineId { get; set; } = null!;

        public List<SITE_PRODUCTION_LINES_OUTPUT>? inputs { get; set; } = null!;

        public List<SITE_PRODUCTION_LINES_ORDER_INPUT> outputs { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP created { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP? started { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP? completion { get; set; }

        public APEX_COMMON_DURATION duration { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP? lastUpdated { get; set; } = null!;

        public double completed { get; set; }

        public bool halted { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY productionFee { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE_NULLABLE productionFeeCollector { get; set; } = null!;

        public bool recurring { get; set; }
    }

    public class SITE_PRODUCTION_LINES_ORDER_INPUT : IValidation
    {
        public APEX_COMMON_AMOUNT_AND_CURRENCY value { get; set; } = null!;

        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [PositiveNumber]
        public int amount { get; set; }
    }

    public class SITE_PRODUCTION_LINES_OUTPUT : IValidation
    {
        public APEX_COMMON_AMOUNT_AND_CURRENCY value { get; set; } = null!;

        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [PositiveNumber]
        public int amount { get; set; }
    }

    public class SITE_PRODUCTION_LINES_PRODUCTION_TEMPLATE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(64, MinimumLength = 3)]
        public string name { get; set; } = null!;

        public List<SITE_PRODUCTION_LINES_FACTOR> inputFactors { get; set; } = null!;

        public List<SITE_PRODUCTION_LINES_FACTOR> outputFactors { get; set; } = null!;

        [PositiveNumber]
        public double effortFactor { get; set; }

        [Range(0.0, 5.0)]
        public double efficiency { get; set; }

        public APEX_COMMON_DURATION duration { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY productionFeeFactor { get; set; } = null!;

        public APEX_COMMON_USER_AND_CODE_NULLABLE productionFeeCollector { get; set; } = null!;
    }

    public class SITE_PRODUCTION_LINES_FACTOR : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [PositiveNumber]
        public int factor { get; set; }
    }

    public class SITE_PRODUCTION_LINES_EFFICIENCY_FACTOR : IValidation
    {
        [BuildingCategory]
        public string? expertiseCategory { get; set; }

        public string type { get; set; } = null!;

        public int effectivity { get; set; }

        [PositiveNumber]
        public double value { get; set; }
    }
}
#pragma warning restore 1591