﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Production
{
    public class ROOT_PRODUCTION_ORDER_UPDATED : IValidation
    {
        [Equals("PRODUCTION_ORDER_UPDATED")]
        public string messageType { get; set; } = null!;

        public ROOT_PRODUCTION_ORDER_UPDATED_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_PRODUCTION_ORDER_UPDATED_PAYLOAD : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string productionLineId { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT>? inputs { get; set; }

        [NotEmpty]
        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> outputs { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP created { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP? started { get; set; }

        public APEX_COMMON_TIMESTAMP? completion { get; set; }

        public APEX_COMMON_DURATION duration { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP? lastUpdated { get; set; }

        [Range(0.0, 1.0)]
        public double completed { get; set; }

        public bool halted { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? productionFee { get; set; }

        public APEX_COMMON_USER_AND_CODE_NULLABLE? productionFeeCollector { get; set; }

        public bool recurring { get; set; }
    }
}
#pragma warning restore 1591
