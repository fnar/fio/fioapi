﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Production
{
    public class MESG_PRODUCTION_ORDER_REMOVED : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_PRODUCTION_ORDER_REMOVED_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_PRODUCTION_ORDER_REMOVED_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public ROOT_PRODUCTION_ORDER_REMOVED message { get; set; } = null!;
    }

    public class ROOT_PRODUCTION_ORDER_REMOVED : IValidation
    {
        [Equals("PRODUCTION_ORDER_REMOVED")]
        public string messageType { get; set; } = null!;

        public ROOT_PRODUCTION_ORDER_REMOVED_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_PRODUCTION_ORDER_REMOVED_PAYLOAD : IValidation
    {
        [APEXID]
        public string orderId { get; set; } = null!;

        [APEXID]
        public string productionLineId { get; set; } = null!;
    }

}
#pragma warning restore 1591
