﻿namespace FIOAPI.Payloads.Group
{
    /// <summary>
    /// Invite response
    /// </summary>
    public class InviteResponse : IValidation
    {
        /// <summary>
        /// GroupId
        /// </summary>
        [Range(1, Model.Group.LargestGroupId)]
        public int GroupId { get; set; }

        /// <summary>
        /// The group name
        /// </summary>
        [StringLength(16, MinimumLength = 3)]
        public string GroupName { get; set; } = null!;

        /// <summary>
        /// If this is an invite for an admin role
        /// </summary>
        public bool Admin { get; set; } = false;

        /// <summary>
        /// The permissions for the group
        /// </summary>
        public Permission.Permissions Permisssions { get; set; } = new();
    }
}
