﻿namespace FIOAPI.Payloads.Group
{
    /// <summary>
    /// UserInvite
    /// </summary>
    public class UserInvite : IValidation
    {
        /// <summary>
        /// The username of the person being invited
        /// </summary>
        
        [StringLength(32, MinimumLength = 3)]
        public string UserName { get; set; } = "";

        /// <summary>
        /// If this is an invite for an admin role
        /// </summary>
        public bool Admin { get; set; } = false;
    }
}
