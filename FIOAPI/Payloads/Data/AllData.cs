﻿namespace FIOAPI.Payloads.Data
{
    /// <summary>
    /// An "AllData" payload - All available data on the user
    /// </summary>
    public class AllData
    {
        /// <summary>
        /// Company
        /// </summary>
        public Model.Company? Company { get; set; }

        /// <summary>
        /// Contracts
        /// </summary>
        public List<Model.Contract>? Contracts { get; set; }

        /// <summary>
        /// CXOSs
        /// </summary>
        public List<Model.CXOS>? CXOSs { get; set; }

        /// <summary>
        /// ProductionLines
        /// </summary>
        public List<Model.ProductionLine>? ProductionLines { get; set; }

        /// <summary>
        /// Ships
        /// </summary>
        public List<Model.Ship>? Ships { get; set; }

        /// <summary>
        /// Flights
        /// </summary>
        public List<Model.Flight>? Flights { get; set; }

        /// <summary>
        /// Sites
        /// </summary>
        public List<Model.Site>? Sites { get; set; }

        /// <summary>
        /// Storages
        /// </summary>
        public List<Model.Storage>? Storages { get; set; }

        /// <summary>
        /// Workforces
        /// </summary>
        public List<Model.Workforce>? Workforces { get; set; }
    }
}
