﻿namespace FIOAPI.Payloads.Data
{
    /// <summary>
    /// BurnItem
    /// </summary>
    [RequiredPermission(Perm.Storage_Items)]
    public class BurnItem
    {
        /// <summary>
        /// MaterialTicker
        /// </summary>
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// MaterialAmount
        /// </summary>
        public int MaterialAmount { get; set; } = 0;

        /// <summary>
        /// WorkforceConsumption
        /// </summary>
        [RequiredPermission(Perm.Sites_Workforces)]
        public double WorkforceConsumption { get; set; } = 0.0;

        /// <summary>
        /// OrderConsumption
        /// </summary>
        [RequiredPermission(Perm.Sites_ProductionLines)]
        public double OrderConsumption { get; set; } = 0.0;

        /// <summary>
        /// OrderProduction
        /// </summary>
        [RequiredPermission(Perm.Sites_ProductionLines)]
        public double OrderProduction { get; set; } = 0.0;

        /// <summary>
        /// OverallDelta
        /// </summary>
        public double OverallDelta
        {
            get
            {
                return OrderProduction - OrderConsumption - WorkforceConsumption;
            }
            set
            {
                // Intentionally empty for json serialization purposes
            }
        }

        /// <summary>
        /// Determines material amount given storage
        /// </summary>
        /// <param name="storage">storage</param>
        /// <exception cref="InvalidOperationException">Didn't set the MaterialTicker on this object</exception>
        public void DetermineMaterialAmount(Model.Storage? storage)
        {
            if (MaterialTicker == null)
            {
                throw new InvalidOperationException($"Must set {nameof(MaterialTicker)} first");
            }

            if (storage != null)
            {
                var item = storage.StorageItems.FirstOrDefault(ps => ps.MaterialTicker == MaterialTicker);
                if (item != null)
                {
                    MaterialAmount = item.Amount!.Value;
                }
            }
        }

        /// <summary>
        /// Determines workforce consumption from the workforce
        /// </summary>
        /// <param name="workforce">workforce</param>
        /// <exception cref="InvalidOperationException">Didn't set the MaterialTicker on this object</exception>
        public void DetermineWorkforceConsumption(Model.Workforce? workforce)
        {
            if (MaterialTicker == null)
            {
                throw new InvalidOperationException($"Must set {nameof(MaterialTicker)} first");
            }

            if (workforce != null)
            {
                WorkforceConsumption = workforce.WorkforceNeeds
                    .Where(wn => wn.MaterialTicker == MaterialTicker)
                    .Sum(wn => wn.UnitsPerInterval)
                    .RoundForDisplay();
            }
        }

        private static readonly long MillisecondsInADay = (long)TimeSpan.FromDays(1.0).TotalMilliseconds;

        /// <summary>
        /// Determines order consumption and production from productionLines
        /// </summary>
        /// <param name="productionLines">productionLines</param>
        /// <exception cref="InvalidOperationException">Didn't set the MaterialTicker on this object</exception>
        public void DetermineOrderConsumptionAndProduction(List<Model.ProductionLine>? productionLines)
        {
            if (MaterialTicker == null)
            {
                throw new InvalidOperationException($"Must set {nameof(MaterialTicker)} first");
            }

            if (productionLines != null)
            {
                // Narrow to productionLines where the MaterialTicker is either an input or output
                productionLines = productionLines
                    .Where(pl => pl.Orders
                        .Any(o => 
                            o.Inputs.Any(i => i.Ticker == MaterialTicker) || 
                            o.Outputs.Any(o => o.Ticker == MaterialTicker)
                            )
                        )
                    .ToList();

                foreach (var productionLine in productionLines)
                {
                    var Orders = productionLine.Orders;

                    bool HasRecurringOrders = productionLine.Orders.Any(o => o.Recurring);
                    if (HasRecurringOrders)
                    {
                        // Only pair down to (only) recurring orders if one exists.
                        // If one doesn't exist, we need to assume the user doesn't have PRO and that *all* orders are recurring
                        Orders = Orders
                            .Where(o => o.Recurring)
                            .ToList();
                    }

                    if (Orders.Count > 0)
                    {
                        // Get the total recurring order time for relevant orders
                        long? TotalRecurringOrderTimeMs = Orders.Where(o => o.DurationMs != null).Sum(o => o.DurationMs);

                        if (TotalRecurringOrderTimeMs != null && TotalRecurringOrderTimeMs > 0)
                        {
                            long ProductionLineTotalTimeMs = MillisecondsInADay * productionLine.Capacity;

                            foreach (var Order in Orders)
                            {
                                double OrderTimePercentage = (double)Order.DurationMs! / (double)TotalRecurringOrderTimeMs;
                                double NumOrdersPerDay = OrderTimePercentage * (ProductionLineTotalTimeMs / (double)Order.DurationMs);

                                var OrderInput = Order.Inputs.FirstOrDefault(oi => oi.Ticker == MaterialTicker);
                                if (OrderInput != null)
                                {
                                    OrderConsumption += OrderInput.Amount * NumOrdersPerDay;
                                }

                                var OrderOutput = Order.Outputs.FirstOrDefault(oo => oo.Ticker == MaterialTicker);
                                if (OrderOutput != null)
                                {
                                    OrderProduction += OrderOutput.Amount * NumOrdersPerDay;
                                }
                            }
                        }
                    }
                }
            }

            OrderConsumption = OrderConsumption.RoundForDisplay();
            OrderProduction = OrderProduction.RoundForDisplay();
        }
    }

    /// <summary>
    /// Burn
    /// </summary>
    public class Burn
    {
        /// <summary>
        /// PlanetId
        /// </summary>
        [RequiredPermission(Perm.Sites_Location)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [RequiredPermission(Perm.Sites_Location)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [RequiredPermission(Perm.Sites_Location)]
        public string? PlanetName { get; set; }

        /// <summary>
        /// WorkforceConsumptionTime
        /// </summary>
        [RequiredPermission(Perm.Sites_Workforces)]
        public DateTime? WorkforceConsumptionTime { get; set; }

        /// <summary>
        /// LastUpdate
        /// </summary>
        [RequiredPermission(Perm.Sites_Workforces)]
        public DateTime? LastUpdate { get; set; }

        /// <summary>
        /// Inventory
        /// </summary>
        [RequiredPermission(Perm.Storage_Information)]
        public List<BurnItem> Inventory { get; set; } = new();
    }
}
