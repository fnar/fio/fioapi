﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Ship
{
    public class MESG_SHIP_FLIGHT_FLIGHTS : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_SHIP_FLIGHT_FLIGHTS_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_SHIP_FLIGHT_FLIGHTS_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_SHIP_FLIGHT_FLIGHTS_MESSAGE message { get; set; } = null!;
    }

    public class MESG_SHIP_FLIGHT_FLIGHTS_MESSAGE : IValidation
    {
        [Equals("SHIP_FLIGHT_FLIGHTS")]
        public string messageType { get; set; } = null!;

        public MESG_SHIP_FLIGHT_FLIGHTS_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_SHIP_FLIGHT_FLIGHTS_PAYLOAD_INNER : IValidation
    {
        public List<MESG_SHIP_FLIGHT_FLIGHTS_FLIGHTS> flights { get; set; } = null!;
    }

    public class MESG_SHIP_FLIGHT_FLIGHTS_FLIGHTS : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string shipId { get; set; } = null!;

        public APEX_COMMON_ADDRESS origin { get; set; } = null!;

        public APEX_COMMON_ADDRESS destination { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP departure { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP arrival { get; set; } = null!;

        public List<MESG_SHIP_FLIGHT_FLIGHTS_SEGMENT> segments { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int currentSegmentIndex { get; set; }

        [Range(0.0, double.MaxValue)]
        public double stlDistance { get; set; }

        [Range(0.0, double.MaxValue)]
        public double ftlDistance { get; set; }

        public bool aborted { get; set; }
    }

    public class MESG_SHIP_FLIGHT_FLIGHTS_SEGMENT : IValidation
    {
        public string type { get; set; } = null!;

        public APEX_COMMON_ADDRESS_WITH_ORBIT origin { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP departure { get; set; } = null!;

        public APEX_COMMON_ADDRESS_WITH_ORBIT destination { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP arrival { get; set; } = null!;

        [Range(0.0, double.MaxValue)]
        public double? stlDistance { get; set; }

        [Range(0.0, double.MaxValue)]
        public double? stlFuelConsumption { get; set; }

        public MESG_SHIP_FLIGHT_FLIGHTS_TRANSFER_ELLIPSE? transferEllipse { get; set; }

        [Range(0.0, double.MaxValue)]
        public double? ftlDistance { get; set; }

        [Range(0.0, double.MaxValue)]
        public double? ftlFuelConsumption { get; set; }

        [Range(0.0, double.MaxValue)]
        public double? damage { get; set; }
    }

    public class MESG_SHIP_FLIGHT_FLIGHTS_TRANSFER_ELLIPSE : IValidation
    {
        public MESG_SHIP_FLIGHT_FLIGHTS_TRANSFER_ELLIPSE_START_POSITION startPosition { get; set; } = null!;

        public MESG_SHIP_FLIGHT_FLIGHTS_TRANSFER_ELLIPSE_TARGET_POSITION targetPosition { get; set; } = null!;

        public MESG_SHIP_FLIGHT_FLIGHTS_TRANSFER_ELLIPSE_CENTER center { get; set; } = null!;

        public double alpha { get; set; }

        public double semiMajorAxis { get; set; }

        public double semiMinorAxis { get; set; }
    }

    public class MESG_SHIP_FLIGHT_FLIGHTS_TRANSFER_ELLIPSE_START_POSITION : IValidation
    {
        public double x { get; set; }

        public double y { get; set; }

        public double z { get; set; }
    }

    public class MESG_SHIP_FLIGHT_FLIGHTS_TRANSFER_ELLIPSE_TARGET_POSITION : IValidation
    {
        public double x { get; set; }

        public double y { get; set; }

        public double z { get; set; }
    }

    public class MESG_SHIP_FLIGHT_FLIGHTS_TRANSFER_ELLIPSE_CENTER : IValidation
    {
        public double x { get; set; }

        public double y { get; set; }

        public double z { get; set; }
    }
}
#pragma warning restore 1591