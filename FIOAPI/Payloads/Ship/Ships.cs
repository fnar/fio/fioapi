﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Ship
{
    public class MESG_SHIP_SHIPS : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_SHIP_SHIPS_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_SHIP_SHIPS_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_SHIP_SHIPS_MESSAGE message { get; set; } = null!;
    }

    public class MESG_SHIP_SHIPS_MESSAGE : IValidation
    {
        [Equals("SHIP_SHIPS")]
        public string messageType { get; set; } = null!;

        public MESG_SHIP_SHIPS_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_SHIP_SHIPS_PAYLOAD_INNER : IValidation
    {
        [NotEmpty]
        public List<MESG_SHIP_SHIPS_SHIPS> ships { get; set; } = null!;
    }

    public class MESG_SHIP_SHIPS_SHIPS : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string idShipStore { get; set; } = null!;

        [APEXID]
        public string idStlFuelStore { get; set; } = null!;

        [APEXID]
        public string idFtlFuelStore { get; set; } = null!;

        [StringLength(32)]
        public string registration { get; set; } = null!;

        [StringLength(32)]
        public string? name { get; set; }

        public APEX_COMMON_TIMESTAMP commissioningTime { get; set; } = null!;

        [StringLength(20)]
        public string blueprintNaturalId { get; set; } = null!;

        public APEX_COMMON_ADDRESS? address { get; set; }

        [APEXID]
        public string? flightId { get; set; }

        [Range(0.0, double.MaxValue)]
        public double acceleration { get; set; }

        [Range(0.0, double.MaxValue)]
        public double thrust { get; set; }

        [Range(0.0, double.MaxValue)]
        public double mass { get; set; }

        [Range(0.0, double.MaxValue)]
        public double operatingEmptyMass { get; set; }

        [Range(0.0, double.MaxValue)]
        public double volume { get; set; }

        [Range(0.0, double.MaxValue)]
        public double? reactorPower { get; set; }

        [Range(0.0, double.MaxValue)]
        public double emitterPower { get; set; }

        [APEXID]
        public string stlFuelStoreId { get; set; } = null!;

        [APEXID]
        public double stlFuelFlowRate { get; set; }

        [APEXID]
        public string ftlFuelStoreId { get; set; } = null!;

        public APEX_COMMON_DURATION operatingTimeStl { get; set; } = null!;

        public APEX_COMMON_DURATION? operatingTimeFtl { get; set; }

        [Range(0.0, 1.0)]
        public double condition { get; set; }

        public APEX_COMMON_TIMESTAMP? lastRepair { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> repairMaterials { get; set; } = null!;

        [StringLength(32)]
        public string status { get; set; } = null!;
    }
}
#pragma warning restore 1591