﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Sites
{
    public class ROOT_SITE_PLATFORM_UPDATED : IValidation
    {
        [Equals("SITE_PLATFORM_UPDATED")]
        public string messageType { get; set; } = null!;

        public ROOT_SITE_PLATFORM_UPDATED_PAYLOAD payload { get; set; } = null!;
    }

    public class ROOT_SITE_PLATFORM_UPDATED_PAYLOAD : IValidation
    {
        [APEXID]
        public string siteId { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;

        public ROOT_SITE_PLATFORM_UPDATED_MODULE module { get; set; } = null!;

        [PositiveNumber]
        public int area { get; set; }

        public APEX_COMMON_TIMESTAMP creationTime { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> reclaimableMaterials { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> repairMaterials { get; set; } = null!;

        public APEX_COMMON_AMOUNT_AND_CURRENCY bookValue { get; set; } = null!;

        [Range(0.3, 1.0)]
        public float condition { get; set; }

        public APEX_COMMON_TIMESTAMP? lastRepair { get; set; } = null!;
    }

    public class ROOT_SITE_PLATFORM_UPDATED_MODULE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [APEXID]
        public string platformId { get; set; } = null!;

        [APEXID]
        public string reactorId { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string reactorName { get; set; } = null!;

        [Ticker]
        public string reactorTicker { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string type { get; set; } = null!;
    }
}
#pragma warning restore 1591