﻿#pragma warning disable 1591
namespace FIOAPI.Payloads
{
    public class APEX_COMMON_MATERIAL : IValidation
    {
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string name { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;

        [Ticker]
        public string ticker { get; set; } = null!;

        [APEXID]
        public string category { get; set; } = null!;

        [Range(0.000001, 1000.0)]
        public double weight { get; set; }

        [Range(0.000001, 1000.0)]
        public double volume { get; set; }

        public bool resource { get; set; }
    }

    public class APEX_COMMON_DURATION : IValidation
    {
        public long millis { get; set; }

        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (millis < 0)
            {
                Errors.Add($"{Context}.{nameof(millis)} is negative.");
            }
        }
    }

    public class APEX_COMMON_MATERIAL_AND_AMOUNT : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int amount { get; set; }
    }

    public class APEX_COMMON_AMOUNT_AND_CURRENCY : IValidation
    {
        public double amount { get; set; }

        [CurrencyCode]
        public string currency { get; set; } = null!;
    }

	public class APEX_COMMON_TIMESTAMP : IValidation
    {
        [APEXTimestamp]
        public long timestamp { get; set; }
    }

    public class APEX_COMMON_ADDRESS_WITH_ORBIT : IValidation
    {
        [NotEmpty]
        public List<APEX_COMMON_ADDRESS_LINE_WITH_ORBIT> lines { get; set; } = null!;
    }

    public class APEX_COMMON_ADDRESS_LINE_WITH_ORBIT : IValidation
    {
        public APEX_COMMON_ADDRESS_ENTITY? entity { get; set; }

        public string type { get; set; } = null!;

        public APEX_COMMON_ADDRESS_ORBIT? orbit { get; set; }
    }

    public class APEX_COMMON_ADDRESS_ORBIT : IValidation
    {
        public double semiMajorAxis { get; set; }

        public double eccentricity { get; set; }

        public double inclination { get; set; }

        public double rightAscension { get; set; }

        public double periapsis { get; set; }
    }

    public class APEX_COMMON_ADDRESS : IValidation
    {
        [NotEmpty]
        public List<APEX_COMMON_ADDRESS_LINE> lines { get; set; } = null!;
    }

    public class APEX_COMMON_ADDRESS_LINE : IValidation
    {
        public APEX_COMMON_ADDRESS_ENTITY? entity { get; set; }

        public string type { get; set; } = null!;
    }

    public class APEX_COMMON_ADDRESS_ENTITY : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [NaturalID]
        public string naturalId { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string name { get; set; } = null!;

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;
    }

    public class APEX_COMMON_USER : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string username { get; set; } = null!;

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;
    }

    public class APEX_COMMON_USER_AND_CODE : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string name { get; set; } = null!;

        [Uppercase]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string? code { get; set; }

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;
    }

	public class APEX_COMMON_USER_AND_CODE_NULLABLE : IValidation
	{
		[APEXID]
		public string? id { get; set; } = null!;

		[StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
		public string? name { get; set; } = null!;

		[Uppercase]
		[StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
		public string? code { get; set; }

		public string _type { get; set; } = null!;

		public string _proxy_key { get; set; } = null!;
	}

	public class APEX_COMMON_CURRENCY : IValidation
    {
        [Range(1, 100)]
        public int numericCode { get; set; }

        [Ticker]
        public string code { get; set; } = null!;

        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string name { get; set; } = null!;

        [Range(0, 100)]
        public int decimals { get; set; }
    }

    public class MaterialTickerAndAmount : IValidation
    {
        [Ticker]
        public string Ticker { get; set; } = null!;

        public int Amount { get; set; }
    }

    /// <summary>
    /// WorkforceLevelAndEfficiency
    /// </summary>
    public class WorkforceLevelAndEfficiency : IValidation
    {
        /// <summary>
        /// Level
        /// </summary>
        [WorkforceLevel]
        public string Level { get; set; } = null!;

        /// <summary>
        /// Efficiency
        /// </summary>
        [Range(0.0, 5.0)]
        public double Efficiency { get; set; }
    }

    /// <summary>
    /// EfficiencyFactor
    /// </summary>
    public class EfficiencyFactor : IValidation
    {
        /// <summary>
        /// ExpertiseCategory
        /// </summary>
        [BuildingCategory]
        public string? ExpertiseCategory { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(64, MinimumLength = 1)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Effectivity
        /// </summary>
        [Range(0.0, 5.0)]
        public double Effectivity { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        [Range(0.0, 5.0)]
        public double Value { get; set; }
    }

    /// <summary>
    /// MaterialAndAmount
    /// </summary>
    public class MaterialAndAmount : IValidation
    {
        /// <summary>
        /// Ticker
        /// </summary>
        [Ticker]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [PositiveNumber]
        public int Amount { get; set; }
    }
}
#pragma warning restore 1591