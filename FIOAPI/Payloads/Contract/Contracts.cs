﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Contract
{
    public class MESG_CONTRACTS_CONTRACTS : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_CONTRACTS_CONTRACTS_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_CONTRACTS_CONTRACTS_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_CONTRACTS_CONTRACTS_MESSAGE message { get; set; } = null!;
    }

    public class MESG_CONTRACTS_CONTRACTS_MESSAGE : IValidation
    {
        [Equals("CONTRACTS_CONTRACTS")]
        public string messageType { get; set; } = null!;

        public MESG_CONTRACTS_CONTRACTS_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_CONTRACTS_CONTRACTS_PAYLOAD_INNER : IValidation
    {
        public List<MESG_CONTRACTS_CONTRACTS_CONTRACT> contracts { get; set; } = null!;
    }

    public class MESG_CONTRACTS_CONTRACTS_CONTRACT : IValidation
    {
        [APEXID]
        public string id { get; set; } = null!;

        [Uppercase]
        [StringLength(8, MinimumLength = 1)]
        public string localId { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP date { get; set; } = null!;

        public string party { get; set; } = null!;

        public MESG_CONTRACTS_CONTRACTS_PARTNER partner { get; set; } = null!;

        public string status { get; set; } = null!;

        public List<MESG_CONTRACTS_CONTRACTS_CONDITION> conditions { get; set; } = null!;

        public APEX_COMMON_TIMESTAMP? extensionDeadline { get; set; }

        public bool canExtend { get; set; }

        public bool canRequestTermination { get; set; }

        public APEX_COMMON_TIMESTAMP? dueDate { get; set; }

        public string? name { get; set; }

        public string? preamble { get; set; }

        public bool terminationSent { get; set; }

        public bool terminationReceived { get; set; }

        public bool agentContract { get; set; }

        public List<string>? relatedContracts { get; set; }

        public string? contractType { get; set; }
    }

    public class MESG_CONTRACTS_CONTRACTS_PARTNER : IValidation
    {
        public string? id { get; set; }

        public string name { get; set; } = null!;

        public string? code { get; set; }

        public string _type { get; set; } = null!;

        public string _proxy_key { get; set; } = null!;

        public string? agentId { get; set; }

        public string? countryId { get; set; }

        public string? countryCode { get; set; }

        public string? type { get; set; } = null!;
    }

    public class MESG_CONTRACTS_CONTRACTS_CONDITION : IValidation
    {
        public APEX_COMMON_AMOUNT_AND_CURRENCY? amount { get; set; }

        public string type { get; set; } = null!;

        [APEXID]
        public string id { get; set; } = null!;

        public string party { get; set; } = null!;

        public int index { get; set; }

        public string status { get; set; } = null!;

        public List<string> dependencies { get; set; } = null!;  // Each is an APEXID

        public APEX_COMMON_DURATION? deadlineDuration { get; set; }

        public APEX_COMMON_TIMESTAMP? deadline { get; set; }

        public APEX_COMMON_MATERIAL_AND_AMOUNT? quantity { get; set; }

        public APEX_COMMON_ADDRESS? address { get; set; }

        [APEXID]
        public string? blockId { get; set; }

        public APEX_COMMON_MATERIAL_AND_AMOUNT? pickedUp { get; set; }

        public double? weight { get; set; }

        public double? volume { get; set; }

        public APEX_COMMON_ADDRESS? destination { get; set; }

        [APEXID]
        public string? shipmentItemId { get; set; }

        [APEXID]
        public string? countryId { get; set; }

        [PositiveNumber]
        public int? reputationChange { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? interest { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? repayment { get; set; }

        public APEX_COMMON_AMOUNT_AND_CURRENCY? total { get; set; }
    }
}
#pragma warning restore 1591