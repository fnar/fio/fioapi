﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Contract
{
    public class MESG_CONTRACTS_CONTRACT : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_CONTRACTS_CONTRACT_PAYLOAD payload { get; set; } = null!;
    }

    public class MESG_CONTRACTS_CONTRACT_PAYLOAD : IValidation
    {
        public string actionId { get; set; } = null!;

        public int status { get; set; }

        public MESG_CONTRACTS_CONTRACT_MESSAGE message { get; set; } = null!;
    }

    public class MESG_CONTRACTS_CONTRACT_MESSAGE : IValidation
    {
        [Equals("CONTRACTS_CONTRACT")]
        public string messageType { get; set; } = null!;

        public MESG_CONTRACTS_CONTRACTS_CONTRACT payload { get; set; } = null!;
    }
}
#pragma warning restore 1591