﻿#pragma warning disable 1591
namespace FIOAPI.Payloads.Building
{
    /// <summary>
    /// APEX Payload for MESG_WORLD_REACTOR_DATA
    /// </summary>
    public class MESG_WORLD_REACTOR_DATA : IValidation
    {
        [Equals("ACTION_COMPLETED")]
        public string messageType { get; set; } = null!;

        public MESG_WORLD_REACTOR_DATA_PAYLOAD_OUTER payload { get; set; } = null!;
    }

    public class MESG_WORLD_REACTOR_DATA_PAYLOAD_OUTER : IValidation
    {
        public string actionId { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int status { get; set; }

        public MESG_WORLD_REACTOR_DATA_MESSAGE message { get; set; } = null!;
    }

    public class MESG_WORLD_REACTOR_DATA_MESSAGE : IValidation
    {
        [Equals("WORLD_REACTOR_DATA")]
        public string messageType { get; set; } = null!;

        public MESG_WORLD_REACTOR_DATA_PAYLOAD_INNER payload { get; set; } = null!;
    }

    public class MESG_WORLD_REACTOR_DATA_PAYLOAD_INNER : IValidation
    {
        public string id { get; set; } = null!;

        [StringLength(64, MinimumLength = 2)]
        public string name { get; set; } = null!;

        [Ticker]
        public string ticker { get; set; } = null!;

        [Range(1, 2000)]
        public int areaCost { get; set; }

        [BuildingCategory]
        [StringLength(32, MinimumLength = 3)]
        public string? expertise { get; set; }

        [NotEmpty]
        public List<MESG_WORLD_REACTOR_DATA_BUILDING_COST> buildingCosts { get; set; } = null!;

        public List<MESG_WORLD_REACTOR_DATA_WORKFORCE_CAPACITY> workforceCapacities { get; set; } = null!;

        public List<MESG_WORLD_REACTOR_DATA_RECIPE> recipes { get; set; } = null!;
    }

    public class MESG_WORLD_REACTOR_DATA_BUILDING_COST : IValidation
    {
        public APEX_COMMON_MATERIAL material { get; set; } = null!;

        [Range(1, 5000)]
        public int amount { get; set; }
    }

    public class MESG_WORLD_REACTOR_DATA_WORKFORCE_CAPACITY : IValidation
    {
        [WorkforceLevel]
        [StringLength(32, MinimumLength = 3)]
        public string level { get; set; } = null!;

        [Range(0, 1000)]
        public int capacity { get; set; }
    }

    public class MESG_WORLD_REACTOR_DATA_RECIPE : IValidation
    {
        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> inputs { get; set; } = null!;

        public List<APEX_COMMON_MATERIAL_AND_AMOUNT> outputs { get; set; } = null!;

        public APEX_COMMON_DURATION duration { get; set; } = null!;
    }
}
#pragma warning restore 1591