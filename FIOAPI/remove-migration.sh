#!/bin/bash

echo Usage: ./remove-migration.sh 
echo Remove the most recent migration for each defined database context

dotnet ef migrations remove --context=SqliteDataContext   --output-dir=Migrations/SqliteMigrations   -- --override-dbtype=sqlite
dotnet ef migrations remove --context=PostgresDataContext --output-dir=Migrations/PostgresMigrations -- --override-dbtype=postgres
