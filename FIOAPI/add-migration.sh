#!/bin/bash
if [ $# -eq 0 ]; then
 echo Usage: ./add-migration.sh MIGRATION_NAME
 echo Creates migration entries for all defined database contexts
 exit 1
fi

# Because it\'s a pooled connection, ensure to tell our program
# what database type this is: "-- --override-dbtype=sqlite"

dotnet ef migrations add $1 --context=SqliteDataContext   --output-dir=Migrations/SqliteMigrations   -- --override-dbtype=sqlite
dotnet ef migrations add $1 --context=PostgresDataContext --output-dir=Migrations/PostgresMigrations -- --override-dbtype=postgres
