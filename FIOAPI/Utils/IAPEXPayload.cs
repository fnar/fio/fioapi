﻿namespace FIOAPI.Utils
{
    // This is only here for reference (for now). Will be removed later.
#if false
    /// <summary>
    /// IAPEXPayload - A required class to inherit from for root payload objects. 
    /// Requires: all `messageType` properties are marked with the `[Equals]` attribute
    /// </summary>
    public interface IAPEXPayload
    {
        /// <summary>
        /// What endpoint this payload is associated with
        /// </summary>
        public string Endpoint { get; }
    }

    /// <summary>
    /// APEXPayloadResolver
    /// </summary>
    public static class APEXPayloadResolver
    {
        internal class PathDetail
        {
            public PathDetail(string Value, int Offset)
            {
                this.Value = Value;
                this.Offset = Offset;
            }

            public string Value { get; set; } = null!;
            public int Offset { get; set; } = -1;

            public override bool Equals(object? obj)
            {
                var other = obj as PathDetail;
                if (other == null)
                {
                    return false;
                }
                else
                {
                    return Value == other.Value && Offset == other.Offset;
                }
            }

            public override int GetHashCode()
            {
                return HashCode.Combine(Value, Offset);
            }
        }

        internal class PayloadDetails
        {
            public List<string> MessageTypes { get; set; } = new();
            public List<PathDetail> PathDetails { get;set; } = new();

            public bool Matches(PayloadDetails details)
            {
                if(MessageTypes.SequenceEqual(details.MessageTypes))
                {
                    foreach (var pathDetail in PathDetails)
                    {
                        
                    }
                }

                return false;
            }

            public override bool Equals(object? obj)
            {
                var other = obj as PayloadDetails;
                if (other == null)
                {
                    return false;
                }
                else
                {
                    return MessageTypes.SequenceEqual(other.MessageTypes) && PathDetails.SequenceEqual(other.PathDetails);
                }
            }

            public override int GetHashCode()
            {
                var HashCode = new HashCode();
                MessageTypes.ForEach(mt => HashCode.Add(mt));
                PathDetails.ForEach(pd => HashCode.Add(pd));
                return HashCode.ToHashCode();
            }
        }

        internal class EndpointDetails
        {
            public string ControllerEndpoint { get; set; } = null!;
            public Type ControllerType { get; set; } = null!;

            public bool MethodRequiresAdmin { get; set; } = false;
            public string MethodEndpoint { get; set; } = null!;
            public string MethodName { get; set; } = null!;
            public MethodInfo MethodInfo { get; set; } = null!;

            public Type PayloadType { get; set; } = null!;
        }

        private static Dictionary<PayloadDetails, EndpointDetails> PayloadDetailsToEndpoint = new();

        private static bool IsInitialized = false;

        private static void WalkPayloadClass(Type PayloadClass, ref PayloadDetails Details)
        {
            var Properties = PayloadClass.GetProperties();
            foreach(var Property in Properties)
            {
                if (Property.PropertyType == typeof(string) && Property.Name == "messageType")
                {
                    var EqualsAttr = Property.GetCustomAttribute<EqualsAttribute>()!;
                    Details.MessageTypes.Add((string)EqualsAttr.Value!);
                }
                else if (Property.Name == "path" /* Property.PropertyType.IsArray && typeof(IEnumerable<string>).IsAssignableFrom(Property.PropertyType) && */) // For now, assume "path" is special
                {
                    var EqualsAttrs = Property.GetCustomAttributes<EqualsAttribute>();
                    foreach (var EqualsAttr in EqualsAttrs)
                    {
                        Details.PathDetails.Add(new PathDetail((string)EqualsAttr.Value!, EqualsAttr.OffsetIndex));
                    }
                }

                if (Property.PropertyType.IsClass && Property.GetCustomAttribute<JsonIgnoreAttribute>() == null)
                {
                    WalkPayloadClass(Property.PropertyType, ref Details);
                }
            }
        }

        /// <summary>
        /// Initialize
        /// </summary>
        public static void Initialize()
        {
            if (!IsInitialized)
            {
                foreach (var APEXPayloadClass in ReflectionHelpers.AllIAPEXPayloadClassTypes)
                {
                    PayloadDetails Details = new();
                    WalkPayloadClass(APEXPayloadClass, ref Details);

                    IAPEXPayload? payload = Activator.CreateInstance(APEXPayloadClass) as IAPEXPayload;
                    if (payload == null)
                    {
                        throw new InvalidOperationException();
                    }

                    EndpointDetails EndpointDet = new();
                    EndpointDet.PayloadType = APEXPayloadClass;

                    var EndpointSplits = payload.Endpoint.Split("/", 2, StringSplitOptions.RemoveEmptyEntries);
                    EndpointDet.ControllerEndpoint = EndpointSplits[0].TrimStart('/').TrimEnd('/');
                    EndpointDet.MethodEndpoint = (EndpointSplits.Length == 2) ? EndpointSplits[1].TrimStart('/').TrimEnd('/') : "";                    

                    EndpointDet.ControllerType = ReflectionHelpers.AllControllerClassTypes
                        .Select(c => new
                        {
                            Type = c,
                            Endpoint = c.GetCustomAttribute<RouteAttribute>()!.Template.TrimStart('/').TrimEnd('/')
                        })
                        .Where(c => c.Endpoint == EndpointDet.ControllerEndpoint)
                        .First().Type;

                    // @TODO: ToLower?
                    EndpointDet.MethodInfo = EndpointDet.ControllerType.GetMethods()
                        .FirstOrDefault(m => m.GetCustomAttribute<HttpPutAttribute>() != null && m.GetCustomAttribute<HttpPutAttribute>()!.Template!.TrimStart('/').TrimEnd('/') == EndpointDet.MethodEndpoint)!;
                    EndpointDet.MethodRequiresAdmin = EndpointDet.MethodInfo.GetCustomAttribute<AuthorizeAttribute>()!.Policy!.StartsWith("adminwrite");

                    EndpointDet.MethodName = EndpointDet.MethodInfo.Name;

                    if (PayloadDetailsToEndpoint.ContainsKey(Details))
                    {
                        System.Diagnostics.Debugger.Break();
                    }
                    PayloadDetailsToEndpoint.Add(Details, EndpointDet);
                }

                IsInitialized = true;
            }
        }

        private const string MessageTypeRegexPattern = "\"messageType\"\\s*:\\s*\"(?<MessageType>[A-Z_]+)\"";
        private static Regex MessageTypeRegex = new Regex(MessageTypeRegexPattern, RegexOptions.Compiled);

        private const string PathsRegexPattern = "\"path\"\\s*:\\s*\\[(?<Paths>[\\s,\"a-zA-Z0-9]+)\\s*\\]";
        private static Regex PathsRegex = new Regex(PathsRegexPattern, RegexOptions.Compiled);
        
        /// <summary>
        /// Resolves and invokes the correct endpoint given a string payload
        /// </summary>
        /// <param name="thisController">This controller</param>
        /// <param name="Payload">payload</param>
        /// <returns>true if resolved and invoked, falseotherwise</returns>
        public static async Task<IActionResult> Resolve(this ControllerBase thisController, string Payload)
        {
            if (!IsInitialized)
            {
                throw new InvalidOperationException();
            }

            var Details = new PayloadDetails();
            MatchCollection mc = MessageTypeRegex.Matches(Payload);
            if (mc.Count == 0)
            {
                return thisController.BadRequest(new List<string> { "Failed to find any messageTypes" });
            }

            PayloadDetails details = new();

            for (int i = 0; i < mc.Count; ++i)
            {
                details.MessageTypes.Add(mc[i]!.Groups["MessageType"].Value);
            }

            mc = PathsRegex.Matches(Payload);
            if (mc.Count > 0) 
            {
                for (int i = 0; i < mc.Count; ++i)
                {
                    var pathMatch = mc[i]!.Groups["Paths"].Value;
                    var pathSplit = pathMatch
                        .Split(',', StringSplitOptions.None)
                        .Select(s => s.Replace(" ", "").TrimStart('"').TrimEnd('"'))
                        .ToList();

                    for (int pathIdx = 0; pathIdx < pathSplit.Count; ++pathIdx)
                    {
                        bool IsAPEXID = (pathSplit[pathIdx].Length == 32) && pathSplit[pathIdx].All(ch => (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f'));
                        if (!IsAPEXID)
                        {
                            details.PathDetails.Add(new PathDetail(pathSplit[pathIdx], pathIdx));
                        }
                    }
                }
            }

            if (PayloadDetailsToEndpoint.TryGetValue(details, out EndpointDetails? endpointDetails))
            {
                if (endpointDetails.MethodRequiresAdmin && !thisController.HasAdminWrite())
                {
                    return thisController.Unauthorized();
                }

                // Invoke
                var controller = (ControllerBase)Activator.CreateInstance(endpointDetails.ControllerType)!;
                controller.ControllerContext = new ControllerContext();
                controller.ControllerContext.HttpContext = new DefaultHttpContext();
                controller.HttpContext.User = thisController.HttpContext.User;

                var jsonDoc = JsonDocument.Parse(Payload);
                var jsonPayload = JsonSerializer.Deserialize(jsonDoc, endpointDetails.PayloadType);

                return await (Task<IActionResult>)endpointDetails.MethodInfo.Invoke(controller, new object[] { jsonPayload })!;
            }

            return thisController.BadRequest();
        }
    }
#endif
}