﻿using System.Timers;
using Timer = System.Timers.Timer;

// @TODO: Interaction with Integration tests?
namespace FIOAPI.Utils
{
    /// <summary>
    /// A timed event base class. Usage:
    /// - Inherit from ITimedEvent
    /// - Override all appropriate methods, abstract properties, and virtual properties
    /// After that, the system will construct a TimedEvent for you at startup and execution in the time-frames you expect.
    /// </summary>
    public abstract class ITimedEvent
    {
        private static List<ITimedEvent> TimedEvents { get; set; } = new List<ITimedEvent>();
        private Timer? timer = null;

        /// <summary>
        /// If the timed event should start ticking at program start (this being false requires you to explicitly enable it)
        /// </summary>
        protected virtual bool ExecuteAtProgramStart
        {
            get; set;
        } = true;

        /// <summary>
        /// The timer interval
        /// </summary>
        protected abstract TimeSpan TimerInterval
        {
            get;
        }

        /// <summary>
        /// If the OnElapsed function should be executed once *prior* to starting the timer
        /// </summary>
        protected virtual bool RunAtTimerStart
        {
            get; set;
        } = true;

        /// <summary>
        /// If it should be run only once
        /// </summary>
        protected virtual bool RunOnce
        {
            get; set;
        } = false;

        /// <summary>
        /// The work your timed event does
        /// </summary>
        /// <param name="source">The source of the timed event (can be the timed event itself or the timer)</param>
        /// <param name="eventTime">When the event time was triggered</param>
        protected abstract void Execute(object source, DateTime eventTime);

        /// <summary>
        /// Start the timed event
        /// </summary>
        public void Start()
        {
            if (RunAtTimerStart)
            {
                Execute(this, DateTime.Now);
            }

            timer = new()
            {
                Interval = TimerInterval.TotalMilliseconds,
                AutoReset = false,
                Enabled = true
            };
            timer.Elapsed += OnElapsed!;
            TimedEvents.Add(this);
        }

        /// <summary>
        /// Stop the timed event
        /// </summary>
        public void Stop()
        {
            if (timer == null)
            {
                throw new InvalidOperationException("Timer not started");
            }

            timer.Enabled = false;
        }

        private void OnElapsed(object source, ElapsedEventArgs e)
        {
            Execute(source, e.SignalTime);

            if (RunOnce)
            {
                TimedEvents.Remove(this);
            }

            // Re-start the timer
            timer!.Start();
        }

        /// <summary>
        /// Finds all ITimedEvent class instances, constructs new instances, and starts them
        /// </summary>
        public static void StartAllEvents()
        {
            var timedEventType = typeof(ITimedEvent);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => p.IsSubclassOf(typeof(ITimedEvent)) && !p.IsAbstract)
                .ToList();

            foreach (var type in types)
            {
                ITimedEvent te = (ITimedEvent)Activator.CreateInstance(type)!;
                if (te.ExecuteAtProgramStart)
                {
                    te.Start();
                    TimedEvents.Add(te);
                }
            }
        }

        /// <summary>
        /// Stops all ITimedEvents (including both startup and runtime timed events)
        /// </summary>
        public static void StopAllEvents()
        {
            TimedEvents.ForEach(te => te.Stop());
            TimedEvents.Clear();
        }
    }
}
