﻿namespace FIOAPI.Utils
{
    /// <summary>
    /// Helper class to clear user data
    /// </summary>
    public static class ClearUserDataHelper
    {
        /// <summary>
        /// Deletes all account data associated with the provided username
        /// **THIS SHOULD ONLY BE USED WHEN DELETING THE ACCOUNT**
        /// </summary>
        /// <param name="UserName">The username (should be lowercase)</param>
        /// <returns>Task</returns>
        public static async Task DeleteUser(string UserName) => await DeleteUserData(UserName, true, true, true, true, true, true, true, true, true, true, true, true, true, DeleteUser: true);

        /// <summary>
        /// Rests specific runtime gameplay data
        /// </summary>
        /// <param name="UserName">The username (lower) to reset the data for</param>
        /// <param name="Company">If Company data should be deleted</param>
        /// <param name="Contracts">If Contracts data should be deleted</param>
        /// <param name="CXOS">If CXOS data should be deleted</param>
        /// <param name="Experts">If Experts data should be deleted</param>
        /// <param name="Flights">If Flights data should be deleted</param>
        /// <param name="ProductionLines">If ProductionLines data should be deleted</param>
        /// <param name="Ships">If Ships data should be deleted</param>
        /// <param name="Sites">If Sites data should be deleted</param>
        /// <param name="Storages">If Storages data should be deleted</param>
        /// <param name="Workforces">If Workforces data should be deleted</param>
        /// <returns>Task</returns>
        public static async Task ResetGameplayData(string UserName, 
                                                  bool Company = true, bool Contracts = true, bool CXOS = true, 
                                                  bool Experts = true, bool Flights = true, bool ProductionLines = true,
                                                  bool Ships = true, bool Sites = true, bool Storages = true, 
                                                  bool Workforces = true)
        {
            await DeleteUserData(UserName, 
                                 Company: Company, Contracts: Contracts, CXOS: CXOS, 
                                 Experts: Experts, Flights: Flights, Groups: false,
                                 Permissions: false,ProductionLines: ProductionLines, Ships: Ships,
                                 Sites: Sites, Storages: Storages, Registrations: false,
                                 Workforces: Workforces);
        }

        private static async Task DeleteUserData(string UserName,
                                                 bool Company, bool Contracts, bool CXOS,
                                                 bool Experts, bool Flights, bool Groups,
                                                 bool Permissions, bool ProductionLines, bool Ships,
                                                 bool Sites, bool Storages, bool Registrations, bool Workforces, bool DeleteUser = false)
        {
            if (UserName.ToLower() != UserName)
            {
                throw new ArgumentException($"{nameof(UserName)} must be lowercase to match database fields");
            }

            using (var writer = DBAccess.GetWriter())
            {
                if (Company)
                {
                    // Company is a bit unique since it's a publicly accessible object with some private data
                    await writer.DB.Companies
                        .Where(c => c.UserName == UserName)
                        .ExecuteUpdateAsync(setter =>
                            setter
                                .SetProperty(c => c.StartingProfile, (string?)null)
                                .SetProperty(c => c.StartingLocationId, (string?)null)
                                .SetProperty(c => c.StartingLocationNaturalId, (string?)null)
                                .SetProperty(c => c.StartingLocationName, (string?)null)
                                .SetProperty(c => c.HeadquartersLocationId, (string?)null)
                                .SetProperty(c => c.HeadquartersLocationNaturalId, (string?)null)
                                .SetProperty(c => c.HeadquartersLocationName, (string?)null)
                                .SetProperty(c => c.HeadquartersLevel, (int?)null)
                                .SetProperty(c => c.TotalBasePermits, (int?)null)
                                .SetProperty(c => c.UsedBasePermits, (int?)null)
                                .SetProperty(c => c.AdditionalBasePermits, (int?)null)
                                .SetProperty(c => c.AdditionalProductionQueueSlots, (int?)null)
                                .SetProperty(c => c.HeadquartersNextRelocationTime, (DateTime?)null)
                                .SetProperty(c => c.HeadquartersRelocationLocked, (bool?)null)
                                .SetProperty(c => c.HeadquartersAgricultureEfficiencyGain, (double?)null)
                                .SetProperty(c => c.HeadquartersChemistryEfficiencyGain, (double?)null)
                                .SetProperty(c => c.HeadquartersConstructionEfficiencyGain, (double?)null)
                                .SetProperty(c => c.HeadquartersElectronicsEfficiencyGain, (double?)null)
                                .SetProperty(c => c.HeadquartersFoodIndustriesEfficiencyGain, (double?)null)
                                .SetProperty(c => c.HeadquartersFuelRefiningEfficiencyGain, (double?)null)
                                .SetProperty(c => c.HeadquartersManufacturingEfficiencyGain, (double?)null)
                                .SetProperty(c => c.HeadquartersMetallurgyEfficiencyGain, (double?)null)
                                .SetProperty(c => c.HeadquartersResourceExtractionEfficiencyGain, (double?)null)
                                .SetProperty(c => c.AICLiquid, (double?)null)
                                .SetProperty(c => c.CISLiquid, (double?)null)
                                .SetProperty(c => c.ECDLiquid, (double?)null)
                                .SetProperty(c => c.ICALiquid, (double?)null)
                                .SetProperty(c => c.NCCLiquid, (double?)null)
                                .SetProperty(c => c.RepresentationCurrentLevel, (int?)null)
                                .SetProperty(c => c.RepresentationCostNextLevel, (double?)null)
                                .SetProperty(c => c.RepresentationContributedNextLevel, (double?)null)
                                .SetProperty(c => c.RepresentationLeftNextLevel, (double?)null)
                                .SetProperty(c => c.RepresentationContributedTotal, (double?)null));
                }

                if (Contracts)
                {
                    await writer.DB.Contracts
                        .Where(u => u.UserNameSubmitted == UserName)
                        .ExecuteDeleteAsync();
                }

                if (CXOS)
                {
                    await writer.DB.CXOSs
                        .Where(c => c.UserNameSubmitted == UserName)
                        .ExecuteDeleteAsync();
                }

                if (Experts)
                {
                    await writer.DB.Experts
                        .Where(e => e.UserNameSubmitted == UserName)
                        .ExecuteDeleteAsync();
                }

                if (Flights)
                {
                    await writer.DB.Flights
                        .Where(f => f.UserNameSubmitted == UserName)
                        .ExecuteDeleteAsync();
                }

                if (Groups)
                {
                    await writer.DB.Groups
                        .Where(u => u.GroupOwner == UserName)
                        .ExecuteDeleteAsync();

                    await writer.DB.GroupAdmins
                        .Where(ga => ga.UserName == UserName)
                        .ExecuteDeleteAsync();

                    await writer.DB.GroupUsers
                        .Where(gu => gu.UserName == UserName)
                        .ExecuteDeleteAsync();

                    await writer.DB.GroupPendingInvites
                        .Where(gpi => gpi.UserName == UserName)
                        .ExecuteDeleteAsync();
                }

                if (Permissions)
                {
                    await writer.DB.Permissions
                        .Where(p => p.GrantorUserName == UserName)
                        .ExecuteDeleteAsync();

                    await writer.DB.Permissions
                        .Where(p => p.GranteeUserName == UserName)
                        .ExecuteDeleteAsync();
                }

                if (ProductionLines)
                {
                    await writer.DB.ProductionLines
                        .Where(pl => pl.UserNameSubmitted == UserName)
                        .ExecuteDeleteAsync();
                }

                if (Ships)
                {
                    await writer.DB.Ships
                        .Where(s => s.UserNameSubmitted == UserName)
                        .ExecuteDeleteAsync();
                }

                if (Sites)
                {
                    await writer.DB.Sites
                        .Where(s => s.UserNameSubmitted == UserName)
                        .ExecuteDeleteAsync();
                }

                if (Storages)
                {
                    await writer.DB.Storages
                        .Where(s => s.UserNameSubmitted == UserName)
                        .ExecuteDeleteAsync();
                }

                if (Registrations)
                {
                    await writer.DB.Registrations
                        .Where(r => r.UserName == UserName)
                        .ExecuteDeleteAsync();
                }

                if (Workforces)
                {
                    await writer.DB.Workforces
                        .Where(w => w.UserNameSubmitted == UserName)
                        .ExecuteDeleteAsync();
                }

                if (DeleteUser)
                {
                    await writer.DB.Users
                        .Where(u => u.UserName == UserName)
                        .ExecuteDeleteAsync();
                }
            }
        }
    }
}
