﻿using System.Collections.Generic;

namespace FIOAPI.Utils
{
#pragma warning disable 1591
    public class APEXPayload
    {
        public Dictionary<string, APEXPayloadDef> apex_payload_defs { get; set; } = new();
    }

    public class APEXPayloadDef
    {
        public string action { get; set; } = null!;

        public bool req_admin { get; set; }
        
        public Dictionary<string, APEXPayloadDef>? payload_events { get; set; }

        public string? suburl { get; set; }

        public List<string>? group_regex_match { get; set; }

        public List<APEXPayloadPath>? paths { get; set; }
    }

    public class APEXPayloadPath
    {
        public int count { get; set; }

        public Dictionary<string, string> matches { get; set; } = null!;

        public bool req_admin { get; set; }

        public string action { get; set;} = null!;

        public string suburl { get; set;} = null!;
    }
#pragma warning restore 1591

    /// <summary>
    /// APEXPayloadHelper
    /// </summary>
    public static class APEXPayloadHelper
    {
        private const string MessageTypeKey = "messageType";
        private const string PathKey = "path";

        private const string SubprocessPayloadIndicator = "ACTION_COMPLETED";
        private const string PathPayloadIndicator = "DATA_DATA";

        // These def names are handled in special manners
        private static Dictionary<string, string> DefNameToSpecialAction = new()
        {
            { "CHANNEL_DATA", "process_chat_special" },
            { "PRODUCTION_PRODUCTION_LINE_UPDATED", "send_payload_delayed_productionlineupdated" },
            { "CHANNEL_MESSAGE_ADDED", "send_chat_if" },
            { "CHANNEL_MESSAGE_DELETED", "send_chat_if" },
            { "CHANNEL_USER_JOINED", "send_chat_if" },
            { "CHANNEL_USER_LEFT", "send_chat_if" },
        };

        /// <summary>
        /// PayloadDefs
        /// </summary>
        public static APEXPayload PayloadDefs 
        { 
            get
            {
                Initialize();
                return _PayloadDefs!;
            }
        }
        private static APEXPayload? _PayloadDefs = null;

        /// <summary>
        /// Initialize the extension payload.
        /// </summary>
        public static void Initialize()
        {
            if (_PayloadDefs != null)
                return;

            _PayloadDefs = new();

            // How this works:
            // 1 - It iterates over all controller classes
            // 2 - For each controller class, it iterates all those methods which are PUTs
            // 3 - It determines if it requires admin via the Authorize attribute
            // 4 - It grabs the body parameter
            // 5 - It inspect the body parameter and looks for any `Equals` attributes and/or `ContainerLength` on the `path` member
            // 6 - It generates the FIOExtension expected payload of message -> endpoint ("suburl")
            foreach (var ControllerClass in ReflectionHelpers.AllControllerClassTypes)
            {
                var ControllerMethods = ControllerClass
                    .GetMethods()
                    .Where(m => m.GetCustomAttribute<HttpPutAttribute>() != null && m.GetCustomAttribute<HttpPutAttribute>()!.Template != null)
                    .ToList();
                var ClassUrl = ControllerClass.GetCustomAttribute<RouteAttribute>()!.Template!.TrimStart('/').TrimEnd('/');
                foreach (var ControllerMethod in ControllerMethods)
                {
                    var PutAttribute = ControllerMethod.GetCustomAttribute<HttpPutAttribute>()!;
                    var MethodUrl = PutAttribute.Template!.TrimStart('/').TrimEnd('/');
                    var SubUrl = $"/{ClassUrl}/{MethodUrl}".TrimEnd('/');

                    var AuthPolicyAttr = ControllerMethod.GetCustomAttribute<AuthorizeAttribute>();
                    bool RequireAdmin = (AuthPolicyAttr != null && AuthPolicyAttr.Policy != null && AuthPolicyAttr.Policy == AuthPolicy.AdminWrite);

                    var FromBodyType = ControllerMethod.GetParameters().FirstOrDefault(param => param.GetCustomAttribute<FromBodyAttribute>() != null);
                    if (FromBodyType != null)
                    {
                        List<string> messageTypes = new();
                        List<Tuple<int, string>> paths = new();
                        int pathCount = 0;
                        WalkPayloadType(FromBodyType.ParameterType, ref messageTypes, ref paths, ref pathCount);

                        bool IsPathPayload = messageTypes.Count == 2 &&
                            messageTypes[0] == SubprocessPayloadIndicator &&
                            messageTypes[1] == PathPayloadIndicator &&
                            paths.Count > 0;

                        bool IsSubprocessPayload = messageTypes.Count == 2 && 
                            paths.Count == 0 && 
                            messageTypes[0] == SubprocessPayloadIndicator &&
                            messageTypes[1] != PathPayloadIndicator;

                        bool IsDirectPayload = messageTypes.Count == 1 &&
                            paths.Count == 0 &&
                            messageTypes[0] != SubprocessPayloadIndicator &&
                            messageTypes[0] != PathPayloadIndicator;

                        if (!IsPathPayload && !IsSubprocessPayload && !IsDirectPayload)
                        {
                            continue;
                        }

                        if (IsDirectPayload)
                        {
                            // Add it directoy as an individual def
                            var defName = messageTypes[0];
                            var defAction = DefNameToSpecialAction.TryGetValue(defName, out var specialAction) ? specialAction : "send_payload";
                            var def = new APEXPayloadDef()
                            {
                                action = defAction,
                                req_admin = RequireAdmin,
                                suburl = SubUrl
                            };

                            _PayloadDefs.apex_payload_defs.Add(defName, def);
                        }

                        if (IsSubprocessPayload || IsPathPayload)
                        {
                            var rootDefName = messageTypes[0];
                            if (!_PayloadDefs.apex_payload_defs.ContainsKey(rootDefName))
                            {
                                _PayloadDefs.apex_payload_defs.Add(rootDefName, new APEXPayloadDef()
                                {
                                    action = "subprocess_payload",
                                    req_admin = false, // top-level is always false
                                    payload_events = new()
                                });
                                _PayloadDefs.apex_payload_defs[rootDefName].payload_events = new();
                            }

                            var defName = messageTypes[1];
                            if (IsSubprocessPayload)
                            {
                                var defAction = DefNameToSpecialAction.TryGetValue(defName, out var specialAction) ? specialAction : "send_payload";

                                var groupRegexMatch = defName == "CHANNEL_DATA" ? new List<string> { ".* Global Site Owners$" } : null; // @TODO: CHANNEL_DATA is a one-off for now

                                _PayloadDefs.apex_payload_defs[rootDefName].payload_events!.Add(defName, new APEXPayloadDef()
                                {
                                    action = defAction,
                                    req_admin = RequireAdmin,
                                    suburl = SubUrl,
                                    group_regex_match = groupRegexMatch
                                });
                            }

                            if (IsPathPayload)
                            {
                                var payload_events = _PayloadDefs.apex_payload_defs[rootDefName].payload_events!;
                                if (!payload_events.ContainsKey(defName))
                                {
                                    payload_events.Add(defName, new APEXPayloadDef()
                                    {
                                        action = "process_by_path",
                                        req_admin = false, // always false here
                                        paths = new()
                                    });
                                }

                                var defAction = DefNameToSpecialAction.TryGetValue(defName, out var specialAction) ? specialAction : "send_payload";
                                payload_events[defName].paths!.Add(new APEXPayloadPath()
                                {
                                    count = pathCount,
                                    req_admin = RequireAdmin,
                                    action = defAction,
                                    suburl = SubUrl,
                                    matches = paths.ToDictionary(p => $"{p.Item1}", p => p.Item2)
                                });
                            }
                        }
                    }
                }    
            }
        }

        private static void WalkPayloadType(Type payloadType, ref List<string> messageTypes, ref List<Tuple<int, string>> paths, ref int pathCount)
        {
            var WalkProperties = payloadType.GetProperties()
                .Where(prop => prop.CanWrite && prop.CanRead)
                .ToList();

            var ReadProperties = WalkProperties
                .Where(prop => prop.GetCustomAttributes<EqualsAttribute>().Any())
                .ToList();

            var MessageTypeProps = ReadProperties
                .Where(prop => prop.Name == MessageTypeKey)
                .ToList();

            if (MessageTypeProps.Count > 1)
            {
                throw new InvalidOperationException();
            }

            if (MessageTypeProps.Count == 1)
            {
                var EqualsValue = (string)MessageTypeProps.First().GetCustomAttribute<EqualsAttribute>()!.Value!;
                messageTypes.Add(EqualsValue);
            }

            var PathProps = ReadProperties
                .Where(prop => prop.Name == PathKey)
                .ToList();
            foreach (var PathProp in PathProps)
            {
                var EqualsAttrs = PathProp
                    .GetCustomAttributes<EqualsAttribute>()
                    .Where(attr => attr.Value is string && attr.OffsetIndex != -1)
                    .ToList();

                var ContainerLengthAttr = PathProp.GetCustomAttribute<ContainerLengthAttribute>();
                if (ContainerLengthAttr != null && EqualsAttrs.Count > 0)
                {
                    pathCount = ContainerLengthAttr.ContainerLength;
                    foreach (var EqualsAttr in EqualsAttrs)
                    {
                        paths.Add(Tuple.Create(EqualsAttr.OffsetIndex, (string)EqualsAttr.Value!));
                    }
                }
            }

            foreach (var WalkProperty in WalkProperties)
            {
                if (WalkProperty.PropertyType.IsClass)
                {
                    WalkPayloadType(WalkProperty.PropertyType, ref messageTypes, ref paths, ref pathCount);
                }
            }
        }
    }
}
