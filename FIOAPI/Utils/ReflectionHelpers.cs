﻿namespace FIOAPI.Utils
{
    /// <summary>
    /// Reflection helpers
    /// </summary>
    public static class ReflectionHelpers
    {
        // This is used by C# internals for Closure/dynamic class generation (including interfaces it appears)
        private const string InternalCompilerClassPrefix = "<>c";

        /// <summary>
        /// AllClassTypes
        /// </summary>
        public static List<Type> AllClassTypes
        {
            get
            {
                return AppDomain.CurrentDomain.GetAssemblies()
                    .SelectMany(t => t.GetTypes())
                    .Where(t => t.IsClass)
                    .Where(t => t.Namespace != null && t.Namespace.StartsWith("FIOAPI"))
                    .Where(t => !t.Name.StartsWith(InternalCompilerClassPrefix))
                    .Where(t => !t.Name.StartsWith("<"))
                    .ToList();
            }
        }

        /// <summary>
        /// AllModelClassTypes
        /// </summary>
        public static List<Type> AllModelClassTypes
        {
            get
            {
                var RandomModelType = typeof(DB.Model.Building);
                var ModelNamespace = RandomModelType.Namespace;
                var FIOAPIAssembly = Assembly.GetAssembly(RandomModelType);
                var ModelClassTypes = FIOAPIAssembly!.GetTypes()
                    .Where(t => t.IsClass)
                    .Where(t => t.Namespace != null && t.Namespace == ModelNamespace)
                    .Where(t => !t.Name.StartsWith(InternalCompilerClassPrefix))
                    .ToList();
                return ModelClassTypes;
            }
        }

        /// <summary>
        /// AllControllerClassTypes
        /// </summary>
        public static List<Type> AllControllerClassTypes
        {
            get
            {
                var RandomControllerType = typeof(AdminController);
                var ControllerNamespace = RandomControllerType.Namespace;
                var FIOAPIAssembly = Assembly.GetAssembly(RandomControllerType);
                var ControllerClassTypes = FIOAPIAssembly!.GetTypes()
                    .Where(t => t.IsClass)
                    .Where(t => !t.IsAbstract)
                    .Where(t => t.Namespace != null && t.Namespace == ControllerNamespace)
                    .Where(t => !t.Name.StartsWith(InternalCompilerClassPrefix))
                    .Where(t => !t.Name.StartsWith("<"))
                    .Where(t => t.GetCustomAttribute<ApiControllerAttribute>() != null && t.GetCustomAttribute<RouteAttribute>() != null)
                    .ToList();
                return ControllerClassTypes;
            }
        }

        /// <summary>
        /// AllCsvClassTypes
        /// </summary>
        public static List<Type> AllCsvClassTypes
        {
            get
            {
                var RandomCsvClassType = typeof(Payloads.CSV.CsvBuilding);
                var CSVNamespace = RandomCsvClassType.Namespace;
                var FIOAPIAssembly = Assembly.GetAssembly(RandomCsvClassType);
                var CsvClassTypes = FIOAPIAssembly!.GetTypes()
                    .Where(t => t.IsClass)
                    .Where(t => t.Namespace != null && t.Namespace == CSVNamespace)
                    .Where(t => !t.Name.StartsWith(InternalCompilerClassPrefix))
                    .ToList();
                return CsvClassTypes;
            }
        }

        /// <summary>
        /// NumericTypes
        /// </summary>
        public static readonly HashSet<Type> NumericTypes = new HashSet<Type>
        {
            typeof(byte), typeof(sbyte), 
            typeof(short), typeof(ushort),
            typeof(int), typeof(uint),
            typeof(long), typeof(ulong),
            typeof(float), typeof(double)
        };
    }
}
