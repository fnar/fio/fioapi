﻿namespace FIOAPI.Utils
{
    /// <summary>
    /// DetailsExceptionConvert
    /// </summary>
    public class DetailedExceptionConverter : JsonConverter<Exception>
    {
        /// <summary>
        /// Read
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="typeToConvert"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public override Exception? Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            return new NotImplementedException();
        }

        /// <summary>
        /// Write
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="options"></param>
        public override void Write(Utf8JsonWriter writer, Exception value, JsonSerializerOptions options)
        {
            writer.WriteStartObject();
            WriteInner(writer, value, options);
            writer.WriteEndObject();
        }

        private void WriteInner(Utf8JsonWriter writer, Exception exception, JsonSerializerOptions options)
        {
            var exceptionType = exception.GetType();
            writer.WriteString("ClassName", exceptionType.FullName);

            if (exception.Message != null)
            {
                writer.WriteString("Message", exception.Message);
            }

            if (exception.StackTrace != null)
            {
                var stackTrace = exception.StackTrace.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                bool Exceeds50Frames = stackTrace.Length > 50;

                writer.WriteStartArray("StackTrace");
                foreach (var stackTraceFrame in stackTrace.Take(50))
                {
                    writer.WriteStringValue(stackTraceFrame.Trim());
                }
                if (Exceeds50Frames)
                {
                    writer.WriteStringValue("[Exceeds 50 Stack Frames]");
                }
                writer.WriteEndArray();
            }

            if (exception.InnerException != null)
            {
                writer.WriteStartObject("InnerException");
                WriteInner(writer, exception.InnerException, options);
                writer.WriteEndObject();
            }
        }
    }
}
