﻿using FIOAPI.DB.Model;

namespace FIOAPI.Utils
{
    /// <summary>
    /// GenericFilter - To proplery handle generics
    /// </summary>
    public class SwaggerGenericFilter : ISchemaFilter
    {
        /// <summary>
        /// Apply
        /// </summary>
        /// <param name="schema"></param>
        /// <param name="context"></param>
        public void Apply(OpenApiSchema schema, SchemaFilterContext context)
        {
            var type = context.Type;
            if (type.IsGenericType == true)
            {
                if (type.GetGenericTypeDefinition() == typeof(RetrievalResponse<>))
                {
                    // Find the "Data" generic type
                    var InnerDataType = type.GetGenericArguments()[0];
                    if (InnerDataType.IsGenericType)
                    {
                        var InnerInnerName = InnerDataType.GetGenericArguments()[0].Name;
                        schema.Title = $"RetrievalResponse<List<{InnerInnerName}>>";
                    }
                    else
                    {
                        schema.Title = $"RetrievalResponse<{InnerDataType.Name}>";
                    }
                }

                if (type.GetGenericTypeDefinition() == typeof(IndividualUserResponse<>))
                {
                    // Find the "Data" generic type
                    var InnerDataType = type.GetGenericArguments()[0];
                    if (InnerDataType.IsGenericType)
                    {
                        var InnerInnerName = InnerDataType.GetGenericArguments()[0].Name;
                        schema.Title = $"IndividualUserResponse<List<{InnerInnerName}>>";
                    }
                    else
                    {
                        schema.Title = $"IndividualUserResponse<{InnerDataType.Name}>";
                    }
                }
            }
        }
    }
}
