﻿#define REQUEST_LOGGER_ENABLED

using System.Diagnostics;

namespace FIOAPI.Utils
{
    /// <summary>
    /// The response from a finished request
    /// </summary>
    public class FinishedRequestPayload
    {
        /// <summary>
        /// The initial state
        /// </summary>
        public InitialState InitialState { get; set; } = null!;

        /// <summary>
        /// All the logged requests
        /// </summary>
        public List<LoggedRequest> LoggedRequests { get; set; } = null!;
    }

    /// <summary>
    /// The initial state of the request logger
    /// </summary>
    public class InitialState
    {
        /// <summary>
        /// User
        /// </summary>
        public Model.User User
        {
            get
            {
                return _User;
            }
            set
            {
                _User = value;
                _User.PasswordHash = SecurePasswordHasher.EmptyHash;
            }
        }
        private Model.User _User = null!;

        // @TODO: Add more initial state
    }

    /// <summary>
    /// The logged request
    /// </summary>
    public class LoggedRequest
    {
        /// <summary>
        /// Method (GET, PUT, POST, DELETE)
        /// </summary>
        public string Method { get; set; } = null!;

        /// <summary>
        /// The path of the endpoint, including query params
        /// </summary>
        public string Path { get; set; } = null!;

        /// <summary>
        /// ContentType
        /// </summary>
        public string? ContentType { get; set; } = null;

        /// <summary>
        /// Content
        /// </summary>
        public string? Content { get; set; } = null;
    }

    /// <summary>
    /// Request Logger - Utility class to store logged requests
    /// </summary>
    public static class RequestLogger
    {
        private const int MaxLoggedRequests = 1000;

#if REQUEST_LOGGER_ENABLED
        private static object RequestLoggerLock = new();
        private static Dictionary<string, InitialState> InitialStates = new();
        private static Dictionary<string, List<LoggedRequest>> LoggedRequests = new();
#endif // REQUEST_LOGGER_ENABLED

        /// <summary>
        /// Starts a RequestLogger session for the provided UserName
        /// </summary>
        /// <param name="UserName">The username to log for</param>
        public static void Start(string UserName)
        {
#if REQUEST_LOGGER_ENABLED
            UserName = UserName.ToLower();

            // Cancel any existing session
            Cancel(UserName);

            // Store initial state
            StoreInitialState(UserName);

            // Add a new List
            LoggedRequests.Add(UserName, new List<LoggedRequest>());
#endif // REQUEST_LOGGER_ENABLED
        }

        /// <summary>
        /// Logs a message
        /// </summary>
        /// <param name="UserName">The username to log for</param>
        /// <param name="context"></param>
        public static async Task Log(string UserName, HttpContext context)
        {
#if REQUEST_LOGGER_ENABLED
            UserName = UserName.ToLower();

            string BodyContent = await context.Request.GetBodyAsync();
            lock(RequestLoggerLock)
            {
                bool HaveActiveSession = HasActiveSession(UserName);
                Debug.Assert(HaveActiveSession);
                if (HaveActiveSession)
                {
                    var LoggedRequest = new LoggedRequest();
                    LoggedRequest.Method = context.Request.Method;
                    LoggedRequest.Path = context.Request.Path;
                    LoggedRequest.ContentType = context.Request.ContentType;
                    LoggedRequest.Content = BodyContent;
                    LoggedRequests[UserName].Add(LoggedRequest);

                    if (LoggedRequests[UserName].Count > MaxLoggedRequests)
                    {
                        // We've hit the maximum threshold, need to cancel
                        Cancel(UserName);
                    }
                }
            }
#endif // REQUEST_LOGGER_ENABLED
        }

        /// <summary>
        /// Finish a RequestLogger session for the provided UserName
        /// </summary>
        /// <param name="UserName">The username to log for</param>
        public static FinishedRequestPayload? Finish(string UserName)
        {
#if REQUEST_LOGGER_ENABLED
            UserName = UserName.ToLower();

            lock(RequestLoggerLock)
            {
                if (HasActiveSession(UserName))
                {
                    FinishedRequestPayload response = new();
                    response.InitialState = InitialStates[UserName];
                    response.LoggedRequests = LoggedRequests[UserName];
                    Cancel(UserName);
                    return response;
                }
                else
                {
                    return null;
                }
            }
#endif // REQUEST_LOGGER_ENABLED
        }

        /// <summary>
        /// Cancels a RequestLoggerSessionf ro the provided UserName
        /// </summary>
        /// <param name="UserName">The username to cancel the session for</param>
        public static void Cancel(string UserName)
        {
#if REQUEST_LOGGER_ENABLED
            UserName = UserName.ToLower();

            lock (RequestLoggerLock)
            {
                InitialStates.Remove(UserName);
                LoggedRequests.Remove(UserName);
            }
#endif // REQUEST_LOGGER_ENABLED
        }

        /// <summary>
        /// Returns if the provided username has an active session
        /// </summary>
        /// <param name="UserName">UserName</param>
        /// <returns>true if there's an active session</returns>
        public static bool HasActiveSession(string UserName)
        {
#if REQUEST_LOGGER_ENABLED
            UserName = UserName.ToLower();

            lock(RequestLoggerLock)
            {
                return InitialStates.ContainsKey(UserName);
            }
#else
            return false;
#endif // REQUEST_LOGGER_ENABLED
        }

        private static void StoreInitialState(string UserName)
        {
            Debug.Assert(UserName == UserName.ToLower());

            var InitialState = new InitialState();
            lock (RequestLoggerLock)
            {
                using (var reader = DBAccess.GetReader())
                {
                    InitialState.User = reader.DB.Users.First(u => u.UserName == UserName);
                    // @TODO: Fill in more
                }

                InitialStates.Add(UserName, InitialState);
            }
        }
    }
}
