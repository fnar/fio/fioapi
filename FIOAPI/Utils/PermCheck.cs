﻿using System;
using System.Net;

namespace FIOAPI.Utils
{
    /// <summary>
    /// Permission enum
    /// </summary>
    [Flags]
    public enum Perm : int
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0,

        /// <summary>
        /// Ship_Information
        /// </summary>
        Ship_Information = 1 << 0,

        /// <summary>
        /// Ship_Repair
        /// </summary>
        Ship_Repair = 1 << 1,

        /// <summary>
        /// Ship_Flight
        /// </summary>
        Ship_Flight = 1 << 2,

        /// <summary>
        /// Ship_Inventory
        /// </summary>
        Ship_Inventory = 1 << 3,

        /// <summary>
        /// Ship_FuelInventory
        /// </summary>
        Ship_FuelInventory = 1 << 4,

        /// <summary>
        /// Sites_Location
        /// </summary>
        Sites_Location = 1 << 5,

        /// <summary>
        /// Sites_Workforces
        /// </summary>
        Sites_Workforces = 1 << 6,

        /// <summary>
        /// Sites_Experts
        /// </summary>
        Sites_Experts = 1 << 7,

        /// <summary>
        /// Sites_Buildings
        /// </summary>
        Sites_Buildings = 1 << 8,

        /// <summary>
        /// Sites_Repair
        /// </summary>
        Sites_Repair = 1 << 9,

        /// <summary>
        /// Sites_Reclaimable
        /// </summary>
        Sites_Reclaimable = 1 << 10,

        /// <summary>
        /// Sites_ProductionLines
        /// </summary>
        Sites_ProductionLines = 1 << 11,

        /// <summary>
        /// Storage_Location
        /// </summary>
        Storage_Location = 1 << 12,

        /// <summary>
        /// Storage_Information
        /// </summary>
        Storage_Information = 1 << 13,

        /// <summary>
        /// Storage_Items
        /// </summary>
        Storage_Items = 1 << 14,

        /// <summary>
        /// Trade_Contract
        /// </summary>
        Trade_Contract = 1 << 15,

        /// <summary>
        /// Trade_CXOS
        /// </summary>
        Trade_CXOS = 1 << 16,

        /// <summary>
        /// Company_Info
        /// </summary>
        Company_Info = 1 << 17,

        /// <summary>
        /// Company_LiquidCurrency
        /// </summary>
        Company_LiquidCurrency = 1 << 18,

        /// <summary>
        /// Company_Headquarters
        /// </summary>
        Company_Headquarters = 1 << 19,

        /// <summary>
        /// Misc_ShipmentTracking
        /// </summary>
        Misc_ShipmentTracking = 1 << 20,

        /// <summary>
        /// PermAll
        /// </summary>
        All = ~0
    }

    /// <summary>
    /// Permissions checker
    /// </summary>
    public static class PermCheck
    {
        /// <summary>
        /// Retrieves the (merged) Perm enum given a requesting user and user to view
        /// </summary>
        /// <param name="ThisUser">Current user</param>
        /// <param name="UserName">The user to view</param>
        /// <returns>A Perm object</returns>
        public static Perm GetPerm(string ThisUser, string UserName)
        {
#if DEBUG
            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(ThisUser, typeof(Model.User), nameof(Model.User.UserName), ref Errors, nameof(ThisUser));
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserName, typeof(Model.User), nameof(Model.User.UserName), ref Errors, nameof(UserName));
            if (Errors.Count > 0)
            {
                throw new ArgumentException(string.Join(Environment.NewLine, Errors));
            }
#endif

            if (ThisUser == UserName)
            {
                return Perm.All;
            }

            var PermissionModels = PermissionCache.GetPermissions(ThisUser, UserName);
            return MergePerms(PermissionModels);
        }

        /// <summary>
        /// Retrieves the (merged) Perm enum for each UserName in Usernames
        /// </summary>
        /// <param name="ThisUser">Current user</param>
        /// <param name="UserNames">The users to view</param>
        /// <returns>A Dictionary of username to perm</returns>
        public static Dictionary<string, Perm> GetPerms(string ThisUser, List<string> UserNames)
        {
#if DEBUG
            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(ThisUser, typeof(Model.User), nameof(Model.User.UserName), ref Errors, nameof(ThisUser));
            for(int UserNameIdx = 0; UserNameIdx < UserNames.Count; ++UserNameIdx)
            {
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserNames[UserNameIdx], typeof(Model.User), nameof(Model.User.UserName), ref Errors, $"{nameof(UserNames)}[{UserNameIdx}]");
            }
            
            if (Errors.Count > 0)
            {
                throw new ArgumentException(string.Join(Environment.NewLine, Errors));
            }
#endif

            Dictionary<string, Perm> Perms = new();
            var UserNameToPermissionModels = PermissionCache.GetManyPermissions(ThisUser, UserNames);
            foreach (var kvp in UserNameToPermissionModels)
            {
                var UserName = kvp.Key;
                var PermissionModels = kvp.Value;

                Perms.Add(UserName, MergePerms(PermissionModels));
            }

            return Perms;
        }

        /// <summary>
        /// If we can see the data for the provided user information.
        /// </summary>
        /// <param name="ThisUser">The current user</param>
        /// <param name="UserName">The user to look information up for</param>
        /// <param name="perm">Permission flags</param>
        /// <returns>True if you can see the data</returns>
        public static bool CanSee(string ThisUser, string UserName, Perm perm)
        {
#if DEBUG
            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(ThisUser, typeof(DB.Model.User), nameof(DB.Model.User.UserName), ref Errors, nameof(ThisUser));
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserName, typeof(DB.Model.User), nameof(DB.Model.User.UserName), ref Errors, nameof(UserName));
            if (Errors.Count > 0)
            {
                throw new ArgumentException(string.Join(Environment.NewLine, Errors));
            }
#endif

            bool CanSee = false;
            var PermissionModels = PermissionCache.GetPermissions(ThisUser, UserName);
            if (PermissionModels.Any())
            {
                var permValues = Enum.GetValues(typeof(Perm))
                    .Cast<Perm>()
                    .Where(v => perm.HasFlag(v))
                    .ToList();

                if (permValues.Count > 0)
                {
                    foreach (var PermissionModel in PermissionModels)
                    {
                        bool PermModelCheck = true;
                        foreach (var permValue in permValues)
                        {
                            switch (permValue)
                            {
                                case Perm.Ship_Information:
                                    PermModelCheck &= PermissionModel.ShipInformation;
                                    break;
                                case Perm.Ship_Repair:
                                    PermModelCheck &= PermissionModel.ShipRepair;
                                    break;
                                case Perm.Ship_Flight:
                                    PermModelCheck &= PermissionModel.ShipFlight;
                                    break;
                                case Perm.Ship_Inventory:
                                    PermModelCheck &= PermissionModel.ShipInventory;
                                    break;
                                case Perm.Ship_FuelInventory:
                                    PermModelCheck &= PermissionModel.ShipFuelInventory;
                                    break;
                                case Perm.Sites_Location:
                                    PermModelCheck &= PermissionModel.SitesLocation;
                                    break;
                                case Perm.Sites_Workforces:
                                    PermModelCheck &= PermissionModel.SitesWorkforces;
                                    break;
                                case Perm.Sites_Experts:
                                    PermModelCheck &= PermissionModel.SitesExperts;
                                    break;
                                case Perm.Sites_Buildings:
                                    PermModelCheck &= PermissionModel.SitesBuildings;
                                    break;
                                case Perm.Sites_Repair:
                                    PermModelCheck &= PermissionModel.SitesRepair;
                                    break;
                                case Perm.Sites_Reclaimable:
                                    PermModelCheck &= PermissionModel.SitesReclaimable;
                                    break;
                                case Perm.Sites_ProductionLines:
                                    PermModelCheck &= PermissionModel.SitesProductionLines;
                                    break;
                                case Perm.Storage_Location:
                                    PermModelCheck &= PermissionModel.StorageLocation;
                                    break;
                                case Perm.Storage_Information:
                                    PermModelCheck &= PermissionModel.StorageInformation;
                                    break;
                                case Perm.Storage_Items:
                                    PermModelCheck &= PermissionModel.StorageItems;
                                    break;
                                case Perm.Trade_Contract:
                                    PermModelCheck &= PermissionModel.TradeContract;
                                    break;
                                case Perm.Trade_CXOS:
                                    PermModelCheck &= PermissionModel.TradeCXOS;
                                    break;
                                case Perm.Company_Info:
                                    PermModelCheck &= PermissionModel.CompanyInfo;
                                    break;
                                case Perm.Company_LiquidCurrency:
                                    PermModelCheck &= PermissionModel.CompanyLiquidCurrency;
                                    break;
                                case Perm.Company_Headquarters:
                                    PermModelCheck &= PermissionModel.CompanyHeadquarters;
                                    break;
                                case Perm.Misc_ShipmentTracking:
                                    PermModelCheck &= PermissionModel.MiscShipmentTracking;
                                    break;
                                default:
                                    throw new InvalidEnumArgumentException("Encountered unknown enum");
                            }
                        }

                        if (PermModelCheck)
                        {
                            CanSee = true;
                            break;
                        }
                    }
                }
            }

            return CanSee;
        }

        private static Perm MergePerms(List<Model.Permission> PermissionModels)
        {
            Perm MergedPerm = Perm.None;
            if (PermissionModels.Any())
            {
                MergedPerm |= (PermissionModels.Any(pm => pm.ShipInformation) ? Perm.Ship_Information : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.ShipRepair) ? Perm.Ship_Repair : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.ShipFlight) ? Perm.Ship_Flight : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.ShipInventory) ? Perm.Ship_Inventory : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.ShipFuelInventory) ? Perm.Ship_FuelInventory : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.SitesLocation) ? Perm.Sites_Location : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.SitesWorkforces) ? Perm.Sites_Workforces : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.SitesExperts) ? Perm.Sites_Experts : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.SitesBuildings) ? Perm.Sites_Buildings : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.SitesRepair) ? Perm.Sites_Repair : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.SitesReclaimable) ? Perm.Sites_Reclaimable : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.SitesProductionLines) ? Perm.Sites_ProductionLines : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.StorageLocation) ? Perm.Storage_Location : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.StorageInformation) ? Perm.Storage_Information : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.StorageItems) ? Perm.Storage_Items : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.TradeContract) ? Perm.Trade_Contract : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.TradeCXOS) ? Perm.Trade_CXOS : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.CompanyInfo) ? Perm.Company_Info : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.CompanyLiquidCurrency) ? Perm.Company_LiquidCurrency : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.CompanyHeadquarters) ? Perm.Company_Headquarters : Perm.None);
                MergedPerm |= (PermissionModels.Any(pm => pm.MiscShipmentTracking) ? Perm.Misc_ShipmentTracking : Perm.None);
            }

            return MergedPerm;
        }
    }
}
