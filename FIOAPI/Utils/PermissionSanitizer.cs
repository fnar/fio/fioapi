﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace FIOAPI.Utils
{
    /// <summary>
    /// PermissionSanitizer
    /// </summary>
    public static class PermissionSanitizer
    {
        private static Dictionary<Type, Perm> Type_To_PermsRequired = new();
        private static Dictionary<Type, Dictionary<string, Perm>> MemberName_To_PermRequired = new();
        private static Dictionary<Type, Dictionary<string, object>> MemberName_To_DefaultValue = new();

        /// <summary>
        /// Initialize
        /// </summary>
        public static void Initialize()
        {
            foreach (var ModelClassType in ReflectionHelpers.AllClassTypes)
            {
                var ClassAttributes = ModelClassType.GetCustomAttributes(true);
                var RequiredPermissionAttr = ClassAttributes.FirstOrDefault(a => a is RequiredPermissionAttribute) as RequiredPermissionAttribute;
                if (RequiredPermissionAttr != null)
                {
                    Type_To_PermsRequired.Add(ModelClassType, RequiredPermissionAttr.perm);
                }

                var PropInfos = ModelClassType.GetProperties();
                foreach (var PropInfo in PropInfos)
                {
                    bool HasBothGetAndSet = PropInfo.GetGetMethod() != null && PropInfo.GetSetMethod() != null;
                    if (!HasBothGetAndSet)
                    {
                        continue;
                    }

                    var PropertyAttributes = PropInfo.GetCustomAttributes(true);
                    RequiredPermissionAttr = PropertyAttributes.FirstOrDefault(a => a is RequiredPermissionAttribute) as RequiredPermissionAttribute;
                    if (RequiredPermissionAttr != null)
                    {
                        if (!MemberName_To_PermRequired.ContainsKey(ModelClassType))
                        {
                            MemberName_To_PermRequired.Add(ModelClassType, new Dictionary<string, Perm>());
                        }

                        MemberName_To_PermRequired[ModelClassType].Add(PropInfo.Name, RequiredPermissionAttr.perm);

                        if (!MemberName_To_DefaultValue.ContainsKey(ModelClassType))
                        {
                            MemberName_To_DefaultValue.Add(ModelClassType, new Dictionary<string, object>());
                        }

                        // Now, determine default objects
                        if (PropInfo.PropertyType == typeof(string))
                        {
                            bool IsAPEXID = PropInfo.PropertyType == typeof(string) && PropertyAttributes.Any(a => a is APEXIDAttribute);
                            if (IsAPEXID)
                            {
                                MemberName_To_DefaultValue[ModelClassType].Add(PropInfo.Name, Constants.InvalidAPEXID);
                                continue;
                            }
                            else
                            {
                                MemberName_To_DefaultValue[ModelClassType].Add(PropInfo.Name, "");
                                continue;
                            }
                        }

                        bool IsNumericType = ReflectionHelpers.NumericTypes.Contains(PropInfo.PropertyType);
                        if (IsNumericType)
                        {
                            MemberName_To_DefaultValue[ModelClassType].Add(PropInfo.Name, Activator.CreateInstance(PropInfo.PropertyType)!);
                            continue;
                        }

                        bool IsGuid = PropInfo.PropertyType == typeof(Guid);
                        if (IsGuid)
                        {
                            MemberName_To_DefaultValue[ModelClassType].Add(PropInfo.Name, Guid.Empty);
                            continue;
                        }

                        bool IsDateTime = PropInfo.PropertyType == typeof(DateTime);
                        if (IsDateTime)
                        {
                            MemberName_To_DefaultValue[ModelClassType].Add(PropInfo.Name, DateTime.MinValue);
                            continue;
                        }

                        bool IsNullableType = Nullable.GetUnderlyingType(PropInfo.PropertyType) != null || PropInfo.IsNullableReferenceType();
                        if (IsNullableType)
                        {
                            // We just set this to null
                            continue;
                        }

                        bool IsVirtual = PropInfo.GetMethod != null && PropInfo.GetMethod.IsVirtual;
                        bool IsList = PropInfo.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)) && PropInfo.PropertyType.GetGenericArguments().Any();
                        if (IsVirtual && !IsList)
                        {
                            // If the object is virtual but not a list, it means it's the "ParentModel" object and we should skip it
                            continue;
                        }

                        if (IsList)
                        {
                            // This is a sub-object list. We special-case these
                            continue;
                        }

                        throw new InvalidOperationException("Unhandled PropertyType encountered!");
                    }
                }
            }
        }

        /// <summary>
        /// If the given permission can see the type
        /// </summary>
        /// <param name="type">The type of the object</param>
        /// <param name="perm">The permission</param>
        /// <returns>true if can see it</returns>
        public static bool CanSeeType(Type type, Perm perm)
        {
            return !Type_To_PermsRequired.ContainsKey(type) || ((Type_To_PermsRequired[type] & perm) > 0);
        }

        /// <summary>
        /// If the given permission can see the property
        /// </summary>
        /// <param name="type"></param>
        /// <param name="propName"></param>
        /// <param name="perm"></param>
        /// <returns>true if the property can be see</returns>
        public static bool CanSeeProperty(Type type, string propName, Perm perm)
        {
            return !MemberName_To_PermRequired.ContainsKey(type) || !MemberName_To_PermRequired[type].ContainsKey(propName) || ((MemberName_To_PermRequired[type][propName] & perm) > 0);
        }

        /// <summary>
        /// Retrieves the default value for the type and propName
        /// </summary>
        /// <param name="type">Type</param>
        /// <param name="propName">Property name</param>
        /// <returns>The default value</returns>
        public static object GetDefaultValue(Type type, string propName)
        {
            return MemberName_To_DefaultValue[type][propName];
        }

        /// <summary>
        /// ApplyPermissionSantiziations - Strips data from the object (and its subobjects) depending on what RequiredParameterAttributes are present
        /// </summary>
        /// <typeparam name="T">Type of object</typeparam>
        /// <param name="perm">The perm</param>
        /// <param name="classObj">the object</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <returns>true if you have required permissions, false otherwise</returns>
        public static bool ApplyPermissionSanitizations<T>(Perm perm, ref T classObj, ref List<string> Errors) where T : notnull
        {
            object obj = classObj;
            return ApplyPermissionSanitizations(perm, ref obj, ref Errors);
        }

        /// <summary>
        /// ApplyPermissionSantiziations - Strips data from the object (and its subobjects) depending on what RequiredParameterAttributes are present
        /// </summary>
        /// <param name="perm">The perm</param>
        /// <param name="obj">the object</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <returns>true if you have required permissions, false otherwise</returns>
        public static bool ApplyPermissionSanitizations(Perm perm, ref object obj, ref List<string> Errors)
        {
            var ObjectType = obj.GetType();
            if (!CanSeeType(ObjectType, perm))
            {
                Errors.Add($"Do not have required permission(s): {Type_To_PermsRequired[ObjectType]}");
                return false;
            }

            ApplyPermissionOnObject(perm, ref obj, ref Errors);
            return true;
        }

        private static void GenerateError(Type ClassType, string PropertyName, Type TargettedClassType, bool ClassRestriction, ref List<string> Errors)
        {
            if (ClassRestriction)
            {
                Errors.Add($"'{ClassType.Name}.{PropertyName}' stripped due to lack of Permission(s): {Type_To_PermsRequired[TargettedClassType]}");
            }
            else
            {
                Errors.Add($"'{ClassType.Name}.{PropertyName}' stripped due to lack of Permission(s): {MemberName_To_PermRequired[TargettedClassType][PropertyName]}");
            }
        }

        private static void ApplyPermissionOnObject(Perm perm, ref object obj, ref List<string> Errors)
        {
            var Type = obj.GetType();
            var PropInfos = Type.GetProperties();
            foreach (var PropInfo in PropInfos)
            {
                bool HasGetterAndSetter = PropInfo.GetGetMethod() != null && PropInfo.GetSetMethod() != null;
                if (HasGetterAndSetter)
                {
                    object propObj = PropInfo.GetValue(obj)!;
                    ApplyPermissionOnPropInfo(perm, Type, PropInfo, ref propObj, ref Errors);
                    PropInfo.SetValue(obj, propObj);
                }
            }
        }

        private static void ApplyPermissionOnPropInfo(Perm perm, Type ParentClassType, PropertyInfo PropInfo, ref object obj, ref List<string> Errors)
        {
            string PropertyName = PropInfo.Name;
            bool IsList = PropInfo.PropertyType.GetInterfaces().Contains(typeof(IEnumerable)) && PropInfo.PropertyType.GetGenericArguments().Any();
            if (IsList)
            {
                Type InnerCollectionType = PropInfo.PropertyType.GetGenericArguments()[0];
                bool bCanSeeType = CanSeeType(InnerCollectionType, perm);
                bool bCanSeeProperty = CanSeeProperty(ParentClassType, PropertyName, perm);
                if (!bCanSeeType || !bCanSeeProperty)
                {
                    // Replace the List with an empty one
                    Type ListType = typeof(List<>);
                    Type ConstructedListType = ListType.MakeGenericType(InnerCollectionType);
                    obj = Activator.CreateInstance(ConstructedListType)!;

                    if (!bCanSeeType)
                    {
                        GenerateError(ParentClassType, PropertyName, InnerCollectionType, !bCanSeeType, ref Errors);
                    }
                    else
                    {
                        GenerateError(ParentClassType, PropertyName, ParentClassType, !bCanSeeType, ref Errors);
                    }
                }
                else
                {
                    if (obj != null)
                    {
                        var enumerable = (IList)obj;
                        int count = enumerable.Count;

                        for (int Idx = 0; Idx < count; ++Idx)
                        {
                            object listObj = enumerable[Idx]!;
                            ApplyPermissionOnObject(perm, ref listObj, ref Errors);
                            enumerable[Idx] = listObj;
                        }
                    }
                }
            }
            else
            {
                if (!CanSeeProperty(ParentClassType, PropertyName, perm))
                {
                    bool IsNullableType = Nullable.GetUnderlyingType(PropInfo.PropertyType) != null || PropInfo.IsNullableReferenceType();
                    if (IsNullableType)
                    {
                        obj = null!;
                    }
                    else
                    {
                        obj = GetDefaultValue(ParentClassType, PropertyName);
                    }

                    GenerateError(ParentClassType, PropertyName, ParentClassType, false, ref Errors);
                }
            }
        }
    }
}
