﻿using System.Globalization;

using CsvHelper.Configuration;
using Csv = CsvHelper.Configuration.Attributes;

namespace FIOAPI.Utils
{
    /// <summary>
    /// ICsvObject
    /// </summary>
    public interface ICsvObject
    { 
    }


    /// <summary>
    /// Interface for implementing a CSVObject
    /// </summary>
    public class ICsvObjectWithUserName : ICsvObject
    {
        /// <summary>
        /// UserName
        /// </summary>
        [Csv.Optional]
        [Csv.Index(0)]
        public string? UserName { get; set; }
    }
}
