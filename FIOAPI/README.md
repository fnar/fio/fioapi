# FIOAPI

## EFCore Tools
- **Initial Install:** `dotnet tool install --global dotnet-ef`
- **Upgrade:** `dotnet tool update --global dotnet-ef`

## First time run
1. Open FIOAPI.sln in VS
2. Ensure FIOAPI is the selected project
3. Ensure FIOAPI builds
4. Set your command line arguments to: `--migrate --run-after-migrate --create-admin-account-username admin --create-admin-acount-password Hunter2`
5. Run

## Documentation
- [Contribution Documentation](CONTRIB.md)
- [Database Documentation](DATABASE.md)
- [Controller Documentation](CONTROLLERS.md)
- [Validation Documentation](VALIDATION.md)