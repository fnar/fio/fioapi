using Microsoft.Extensions.Logging.Console;

namespace FIOAPI
{
    /// <summary>
    /// Program
    /// </summary>
    public class Program
    {
        private const string FIOAPIGuid = "7b595bb6-565c-41d4-af5f-ea05bc5155cc";

        static void ConfigureLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Globals.ConfigRoot)
                .CreateLogger();
        }

        private static WebApplication? app = null;
        private static AutoResetEvent HasStartedEvent = new AutoResetEvent(false);

        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args"></param>
        public static async Task Main(string[] args)
        {
            ConfigureLogger();

            Log.Debug($"Startup Args: {string.Join(", ", args)}");
            var CommandLineParserResult = Parser.Default
                .ParseArguments<CommandLineOptions>(args);

            var CommandLineResult = CommandLineParserResult
                .MapResult((opts) => CommandLineOptions.RunOptions(opts), CommandLineOptions.HandleParseError);
            if (CommandLineResult < 0)
            {
                Environment.Exit(CommandLineResult);
            }

            var CommandLineOpts = CommandLineParserResult.Value;
            if (Globals.ApplyMigration)
            {
                using (var db = FIODBContext.GetNewContext())
                {
                    var pendingMigrations = db.Database.GetPendingMigrations().ToList();
                    if (!pendingMigrations.Any())
                    {
                        Log.Information("No migrations needed to be applied.");
                        if (!Globals.RunAfterMigrate)
                        {
                            Environment.Exit(0);
                        }
                    }
                    else
                    {
                        Log.Information("Applying the following migrations:");
                        pendingMigrations.ForEach(migration =>
                        {
                            Log.Information($"\t {migration}");
                        });

                        db.Database.Migrate();

                        Log.Information("Migration(s) applied successfully.");

                        if (!Globals.RunAfterMigrate)
                        {
                            Environment.Exit(0);
                        }
                    }
                }
            }

            if (!String.IsNullOrEmpty(CommandLineOpts.CreateAdminAccountUserName))
            {
                var AccountPassword = !string.IsNullOrWhiteSpace(CommandLineOpts.CreateAdminAccountPassword) ? CommandLineOpts.CreateAdminAccountPassword : GenerateRandomPassword(16);

                using (var writer = DBAccess.GetWriter())
                {
                    DB.Model.User adminAccount = new();
                    adminAccount.UserName = CommandLineOpts.CreateAdminAccountUserName.ToLowerInvariant();
                    adminAccount.DisplayUserName = CommandLineOpts.CreateAdminAccountUserName;
                    adminAccount.PasswordHash = SecurePasswordHasher.Hash(AccountPassword);
                    adminAccount.IsAdmin = true;

                    List<string> errors = new();
                    adminAccount.Validate(ref errors);
                    if (errors.Count == 0)
                    {
                        var existingUser = writer.DB.Users.FirstOrDefault(u => u.UserName == adminAccount.UserName);
                        if (existingUser != null)
                        {
                            existingUser.DisplayUserName = adminAccount.DisplayUserName;
                            existingUser.PasswordHash = adminAccount.PasswordHash;
                            existingUser.IsAdmin = adminAccount.IsAdmin;
                            //Log.Information($"Modified account '{adminAccount.DisplayUserName}' with password '{AccountPassword}'");
                        }
                        else
                        {
                            writer.DB.Users.Add(adminAccount);
                            //Log.Information($"Created account '{adminAccount.DisplayUserName}' with password '{AccountPassword}'");
                        }
                        writer.DB.SaveChanges();
                    }
                    else
                    {
                        Log.Warning($"Failed to create admin account.  Errors:{Environment.NewLine}{string.Join($"{Environment.NewLine}\t ", errors)}");
                    }
                }
            }

            PermissionSanitizer.Initialize();

            ITimedEvent.StartAllEvents();
            await EntityCaches.Initialize();
            await PermissionCache.Initialize();
            JumpCalculator.Initialize();
            APEXPayloadHelper.Initialize();
            _ = SharedRateLimitAttribute.SharedRateLimits;  // Initialize shared rate limits

            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddCors(o => o.AddDefaultPolicy(policy =>
            {
                policy.AllowAnyOrigin()
                      .WithMethods("GET", "HEAD", "OPTIONS", "POST", "PUT", "DELETE")
                      .WithHeaders(HeaderNames.Origin, HeaderNames.ContentType, HeaderNames.Accept, HeaderNames.Authorization, HeaderNames.Age, "X-Requested-With", "X-FIO-Application")
                      .WithExposedHeaders("Age");
            }));

            builder.Services.AddFIOCustomAuthentication(builder.Configuration);
            builder.Services.AddDbContext<FIODBContext>();
            builder.Services.AddControllers()
                .AddJsonOptions(opts =>
                {
                    opts.JsonSerializerOptions.AllowTrailingCommas = true;
                    opts.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.Never;
                    opts.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                    opts.JsonSerializerOptions.PropertyNamingPolicy = new ExactTypeNameCaseNamingPolicy();
                    opts.JsonSerializerOptions.WriteIndented = true;
                });
            builder.Services.AddSignalR()
                .AddJsonProtocol(options =>
                {
                    options.PayloadSerializerOptions.PropertyNamingPolicy = new ExactTypeNameCaseNamingPolicy();
                });

            if (!CommandLineOpts.Testing)
            {
                builder.Services.AddResponseCaching();
            }
                
            builder.Services.AddRequestDecompression();

            builder.Services.AddControllers(options =>
            {
                options.CacheProfiles.Add("15Mins",
                    new CacheProfile()
                    {
                        Duration = 15 * 60,
                        Location = ResponseCacheLocation.Any
                    });
                options.CacheProfiles.Add("1Hour",
                    new CacheProfile()
                    {
                        Duration = 60 * 60,
                        Location = ResponseCacheLocation.Any
                    });
                options.CacheProfiles.Add("4Hours",
                    new CacheProfile()
                    {
                        Duration = 4 * 60 * 60,
                        Location = ResponseCacheLocation.Any
                    });
                options.CacheProfiles.Add("24Hours",
                    new CacheProfile()
                    {
                        Duration = 24 * 60 * 60,
                        Location = ResponseCacheLocation.Any
                    });
            });

            builder.Services.AddEndpointsApiExplorer();

            builder.Services.AddFIOSwaggerSecurityOptions();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "FIOAPI",
                    Description = "API for FIO Database",
                    Version = "v1"
                });
                options.EnableAnnotations();
                options.SupportNonNullableReferenceTypes();
                options.SchemaFilter<SwaggerGenericFilter>();

                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });

#if DEBUG
            builder.Services.AddLogging(s => s
                .AddConsole()
                .AddFilter(level => level > LogLevel.Debug));
#endif

            app = builder.Build();

            // Enable buffering (so we can read the same request multiple times)
            // This makes it so the HydrationTimeoutFilter actually reads the request
            app.Use((context, next) =>
            {
                context.Request.EnableBuffering();
                return next();
            });

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "FIOAPI v1");
                c.RoutePrefix = string.Empty;
            });

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseExceptionHandler(new ExceptionHandlerOptions()
                {
                    ExceptionHandlingPath = "/Error"
                });
            }

#if DEBUG
            app.UseHttpsRedirection();
#endif

            app.UseRequestDecompression();
            app.UseCors();
            app.UseResponseCaching();
            app.UseAuthentication();
            app.UseAuthorization();

#if false
            if (!Globals.IsTesting)
            {
                app.UseIgnoreRepeatMiddleware();
            }
#endif

            // Request logger needs to happen after authentication/authorization
            app.UseRequestLogger();

            // Exception handler needs to happen at the very end
            app.UseExceptionLogger();

            app.MapControllers();

            Task RunningTask;
            try
            {
                RunningTask = app.RunAsync();
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine($"Failed to start server: {ex.Message}");
                return;
            }

            Console.CancelKeyPress += (s, e) =>
            {
                Console.WriteLine("SIGTERM received.  Shutting down gracefully.");
                app.StopAsync();
            };

            HasStartedEvent.Set();
            await RunningTask;
        }

        /// <summary>
        /// Waits until the app has started
        /// </summary>
        public static void WaitUntilStarted()
        {
            HasStartedEvent.WaitOne();
        }

        /// <summary>
        /// Stops execution
        /// </summary>
        public static void Stop()
        {
            app?.StopAsync();
        }

        private static string GenerateRandomPassword(int length)
        {
            const string validCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*()_-=+/\\'\"<>,.`~";
            StringBuilder res = new StringBuilder();
            while (0 < length--)
            {
                res.Append(validCharacters[RandomNumberGenerator.GetInt32(validCharacters.Length)]);
            }

            return res.ToString();
        }
    }
}