﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class AddUserWarehouseInfo : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "user_warehouses",
                columns: table => new
                {
                    user_warehouse_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    addressable_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    location_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    location_natural_id = table.Column<string>(type: "TEXT", maxLength: 7, nullable: true),
                    location_name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    units = table.Column<int>(type: "INTEGER", nullable: false),
                    weight_capacity = table.Column<double>(type: "REAL", nullable: false),
                    volume_capacity = table.Column<double>(type: "REAL", nullable: false),
                    next_payment = table.Column<DateTime>(type: "TEXT", nullable: true),
                    fee_currency = table.Column<string>(type: "TEXT", maxLength: 3, nullable: true),
                    fee = table.Column<double>(type: "REAL", nullable: true),
                    fee_collector_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: true),
                    fee_collector_code = table.Column<string>(type: "TEXT", maxLength: 4, nullable: true),
                    fee_collector_name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    status = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    user_name_submitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_warehouses", x => x.user_warehouse_id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "user_warehouses");
        }
    }
}
