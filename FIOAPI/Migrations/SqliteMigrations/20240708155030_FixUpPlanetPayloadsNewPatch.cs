﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class FixUpPlanetPayloadsNewPatch : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "governing_company_code",
                table: "planets");

            migrationBuilder.DropColumn(
                name: "governing_company_id",
                table: "planets");

            migrationBuilder.DropColumn(
                name: "governing_company_name",
                table: "planets");

            migrationBuilder.DropColumn(
                name: "governor_user_id",
                table: "planets");

            migrationBuilder.RenameColumn(
                name: "governor_user_name",
                table: "planets",
                newName: "governing_entity");

            migrationBuilder.AddColumn<double>(
                name: "establishment_fee",
                table: "planets",
                type: "REAL",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "establishment_fee",
                table: "planets");

            migrationBuilder.RenameColumn(
                name: "governing_entity",
                table: "planets",
                newName: "governor_user_name");

            migrationBuilder.AddColumn<string>(
                name: "governing_company_code",
                table: "planets",
                type: "TEXT",
                maxLength: 4,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "governing_company_id",
                table: "planets",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "governing_company_name",
                table: "planets",
                type: "TEXT",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "governor_user_id",
                table: "planets",
                type: "TEXT",
                maxLength: 32,
                nullable: true);
        }
    }
}
