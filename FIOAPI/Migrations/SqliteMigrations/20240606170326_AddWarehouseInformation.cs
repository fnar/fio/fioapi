﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.SqliteMigrations
{
    /// <inheritdoc />
    public partial class AddWarehouseInformation : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "local_market_id",
                table: "stations",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "warehouse_id",
                table: "stations",
                type: "TEXT",
                maxLength: 32,
                nullable: true);

            migrationBuilder.CreateTable(
                name: "warehouses",
                columns: table => new
                {
                    warehouse_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    location_id = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    location_name = table.Column<string>(type: "TEXT", maxLength: 64, nullable: true),
                    location_natural_id = table.Column<string>(type: "TEXT", maxLength: 7, nullable: false),
                    user_name_submitted = table.Column<string>(type: "TEXT", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_warehouses", x => x.warehouse_id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "warehouses");

            migrationBuilder.DropColumn(
                name: "local_market_id",
                table: "stations");

            migrationBuilder.DropColumn(
                name: "warehouse_id",
                table: "stations");
        }
    }
}
