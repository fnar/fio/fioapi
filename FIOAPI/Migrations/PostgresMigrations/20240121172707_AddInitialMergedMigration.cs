﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddInitialMergedMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "api_keys",
                columns: table => new
                {
                    api_key_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    key = table.Column<Guid>(type: "uuid", nullable: false),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    application = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    allow_writes = table.Column<bool>(type: "boolean", nullable: false),
                    create_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_api_keys", x => x.api_key_id);
                });

            migrationBuilder.CreateTable(
                name: "buildings",
                columns: table => new
                {
                    building_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    expertise = table.Column<string>(type: "character varying(19)", maxLength: 19, nullable: true),
                    pioneers = table.Column<int>(type: "integer", nullable: false),
                    settlers = table.Column<int>(type: "integer", nullable: false),
                    technicians = table.Column<int>(type: "integer", nullable: false),
                    engineers = table.Column<int>(type: "integer", nullable: false),
                    scientists = table.Column<int>(type: "integer", nullable: false),
                    area_cost = table.Column<int>(type: "integer", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_buildings", x => x.building_id);
                });

            migrationBuilder.CreateTable(
                name: "chat_channels",
                columns: table => new
                {
                    chat_channel_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    type = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    display_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    user_count = table.Column<int>(type: "integer", nullable: false),
                    creation_timestamp = table.Column<long>(type: "bigint", nullable: false),
                    last_activity_timestamp = table.Column<long>(type: "bigint", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_chat_channels", x => x.chat_channel_id);
                });

            migrationBuilder.CreateTable(
                name: "comex_exchanges",
                columns: table => new
                {
                    comex_exchange_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    decimals = table.Column<int>(type: "integer", nullable: false),
                    numeric_code = table.Column<int>(type: "integer", nullable: false),
                    natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_comex_exchanges", x => x.comex_exchange_id);
                });

            migrationBuilder.CreateTable(
                name: "companies",
                columns: table => new
                {
                    company_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    founded = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    country_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    country_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    country_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    corporation_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    corporation_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    corporation_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    current_apex_representation_level = table.Column<int>(type: "integer", nullable: false),
                    overall_rating = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    ratings_earliest_contract = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    rating_contract_count = table.Column<int>(type: "integer", nullable: false),
                    reputation_entity_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    reputation = table.Column<int>(type: "integer", nullable: false),
                    starting_profile = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    starting_location_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    starting_location_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    starting_location_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    headquarters_location_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    headquarters_location_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    headquarters_location_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    headquarters_level = table.Column<int>(type: "integer", nullable: true),
                    total_base_permits = table.Column<int>(type: "integer", nullable: true),
                    used_base_permits = table.Column<int>(type: "integer", nullable: true),
                    additional_base_permits = table.Column<int>(type: "integer", nullable: true),
                    additional_production_queue_slots = table.Column<int>(type: "integer", nullable: true),
                    headquarters_next_relocation_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    headquarters_relocation_locked = table.Column<bool>(type: "boolean", nullable: true),
                    headquarters_agriculture_efficiency_gain = table.Column<double>(type: "double precision", nullable: true),
                    headquarters_chemistry_efficiency_gain = table.Column<double>(type: "double precision", nullable: true),
                    headquarters_construction_efficiency_gain = table.Column<double>(type: "double precision", nullable: true),
                    headquarters_electronics_efficiency_gain = table.Column<double>(type: "double precision", nullable: true),
                    headquarters_food_industries_efficiency_gain = table.Column<double>(type: "double precision", nullable: true),
                    headquarters_fuel_refining_efficiency_gain = table.Column<double>(type: "double precision", nullable: true),
                    headquarters_manufacturing_efficiency_gain = table.Column<double>(type: "double precision", nullable: true),
                    headquarters_metallurgy_efficiency_gain = table.Column<double>(type: "double precision", nullable: true),
                    headquarters_resource_extraction_efficiency_gain = table.Column<double>(type: "double precision", nullable: true),
                    aic_liquid = table.Column<double>(type: "double precision", nullable: true),
                    cis_liquid = table.Column<double>(type: "double precision", nullable: true),
                    ecd_liquid = table.Column<double>(type: "double precision", nullable: true),
                    ica_liquid = table.Column<double>(type: "double precision", nullable: true),
                    ncc_liquid = table.Column<double>(type: "double precision", nullable: true),
                    representation_current_level = table.Column<int>(type: "integer", nullable: true),
                    representation_cost_next_level = table.Column<double>(type: "double precision", nullable: true),
                    representation_contributed_next_level = table.Column<double>(type: "double precision", nullable: true),
                    representation_left_next_level = table.Column<double>(type: "double precision", nullable: true),
                    representation_contributed_total = table.Column<double>(type: "double precision", nullable: true),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_companies", x => x.company_id);
                });

            migrationBuilder.CreateTable(
                name: "contracts",
                columns: table => new
                {
                    contract_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    date = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    party = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    partner_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    partner_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    partner_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    status = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    extension_deadline = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    can_extend = table.Column<bool>(type: "boolean", nullable: false),
                    can_request_termination = table.Column<bool>(type: "boolean", nullable: false),
                    due_date = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    preamble = table.Column<string>(type: "character varying(255)", maxLength: 255, nullable: true),
                    termination_sent = table.Column<bool>(type: "boolean", nullable: false),
                    termination_received = table.Column<bool>(type: "boolean", nullable: false),
                    agent_contract = table.Column<bool>(type: "boolean", nullable: false),
                    related_contracts = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    contract_type = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_contracts", x => x.contract_id);
                });

            migrationBuilder.CreateTable(
                name: "countries",
                columns: table => new
                {
                    country_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    code = table.Column<string>(type: "character varying(2)", maxLength: 2, nullable: false),
                    numeric_code = table.Column<int>(type: "integer", nullable: false),
                    curreny_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    currency_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    currency_decimals = table.Column<int>(type: "integer", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_countries", x => x.country_id);
                });

            migrationBuilder.CreateTable(
                name: "cx_entries",
                columns: table => new
                {
                    cx_entry_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    material_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    exchange_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    exchange_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    currency_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    price = table.Column<double>(type: "double precision", nullable: true),
                    price_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    high = table.Column<double>(type: "double precision", nullable: true),
                    all_time_high = table.Column<double>(type: "double precision", nullable: true),
                    low = table.Column<double>(type: "double precision", nullable: true),
                    all_time_low = table.Column<double>(type: "double precision", nullable: true),
                    ask = table.Column<double>(type: "double precision", nullable: true),
                    ask_count = table.Column<int>(type: "integer", nullable: true),
                    bid = table.Column<double>(type: "double precision", nullable: true),
                    bid_count = table.Column<int>(type: "integer", nullable: true),
                    supply = table.Column<int>(type: "integer", nullable: true),
                    demand = table.Column<int>(type: "integer", nullable: true),
                    traded = table.Column<int>(type: "integer", nullable: true),
                    volume = table.Column<double>(type: "double precision", nullable: true),
                    price_average = table.Column<double>(type: "double precision", nullable: true),
                    narrow_price_band_low = table.Column<double>(type: "double precision", nullable: true),
                    narrow_price_band_high = table.Column<double>(type: "double precision", nullable: true),
                    wide_price_band_low = table.Column<double>(type: "double precision", nullable: true),
                    wide_price_band_high = table.Column<double>(type: "double precision", nullable: true),
                    mm_buy = table.Column<double>(type: "double precision", nullable: true),
                    mm_sell = table.Column<double>(type: "double precision", nullable: true),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_cx_entries", x => x.cx_entry_id);
                });

            migrationBuilder.CreateTable(
                name: "cxo_ss",
                columns: table => new
                {
                    cxos_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    exchange_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    broker_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    order_type = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    initial_amount = table.Column<int>(type: "integer", nullable: false),
                    limit_amount = table.Column<double>(type: "double precision", nullable: false),
                    limit_currency = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    status = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_cxo_ss", x => x.cxos_id);
                });

            migrationBuilder.CreateTable(
                name: "cxp_cs",
                columns: table => new
                {
                    cxpc_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    exchange_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_cxp_cs", x => x.cxpc_id);
                });

            migrationBuilder.CreateTable(
                name: "experts",
                columns: table => new
                {
                    expert_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    location_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    location_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    location_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    total = table.Column<int>(type: "integer", nullable: false),
                    active = table.Column<int>(type: "integer", nullable: false),
                    total_active_cap = table.Column<int>(type: "integer", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_experts", x => x.expert_id);
                });

            migrationBuilder.CreateTable(
                name: "flights",
                columns: table => new
                {
                    flight_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    ship_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    origin_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    origin_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    origin_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    destination_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    destination_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    destination_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    departure = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    arrival = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    current_segment_index = table.Column<int>(type: "integer", nullable: false),
                    stl_distance = table.Column<double>(type: "double precision", nullable: false),
                    ftl_distance = table.Column<double>(type: "double precision", nullable: false),
                    aborted = table.Column<bool>(type: "boolean", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_flights", x => x.flight_id);
                });

            migrationBuilder.CreateTable(
                name: "fx",
                columns: table => new
                {
                    fxid = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    ticker = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    base_currency_numeric_code = table.Column<int>(type: "integer", nullable: false),
                    base_currency_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    base_currency_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    base_currency_decimals = table.Column<int>(type: "integer", nullable: false),
                    quote_currency_numeric_code = table.Column<int>(type: "integer", nullable: false),
                    quote_currency_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    quote_currency_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    quote_currency_decimals = table.Column<int>(type: "integer", nullable: false),
                    decimals = table.Column<int>(type: "integer", nullable: false),
                    open = table.Column<double>(type: "double precision", nullable: false),
                    close = table.Column<double>(type: "double precision", nullable: false),
                    low = table.Column<double>(type: "double precision", nullable: false),
                    high = table.Column<double>(type: "double precision", nullable: false),
                    previous = table.Column<double>(type: "double precision", nullable: false),
                    traded = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    bid = table.Column<double>(type: "double precision", nullable: true),
                    ask = table.Column<double>(type: "double precision", nullable: true),
                    spread = table.Column<double>(type: "double precision", nullable: true),
                    lot_size_amount = table.Column<double>(type: "double precision", nullable: false),
                    lot_currency = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    fees_factor = table.Column<double>(type: "double precision", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_fx", x => x.fxid);
                });

            migrationBuilder.CreateTable(
                name: "groups",
                columns: table => new
                {
                    group_id = table.Column<int>(type: "integer", nullable: false),
                    group_name = table.Column<string>(type: "character varying(16)", maxLength: 16, nullable: false),
                    group_owner = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_groups", x => x.group_id);
                });

            migrationBuilder.CreateTable(
                name: "jump_cache",
                columns: table => new
                {
                    jump_cache_id = table.Column<string>(type: "character varying(65)", maxLength: 65, nullable: false),
                    source_system_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    source_system_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    source_system_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    destination_system_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    destination_system_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    destination_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    overall_distance = table.Column<double>(type: "double precision", nullable: false),
                    jump_count = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_jump_cache", x => x.jump_cache_id);
                });

            migrationBuilder.CreateTable(
                name: "local_market_ads",
                columns: table => new
                {
                    local_market_ad_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    local_market_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    planet_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    planet_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ad_natural_id = table.Column<int>(type: "integer", nullable: false),
                    type = table.Column<string>(type: "character varying(18)", maxLength: 18, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: true),
                    volume = table.Column<double>(type: "double precision", nullable: true),
                    creator_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    creator_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    creator_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    material_amount = table.Column<int>(type: "integer", nullable: true),
                    price = table.Column<double>(type: "double precision", nullable: false),
                    currency = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    fulfillment_days = table.Column<int>(type: "integer", nullable: false),
                    minimum_rating = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    creation_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    expiry = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    origin_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    destination_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_local_market_ads", x => x.local_market_ad_id);
                });

            migrationBuilder.CreateTable(
                name: "material_categories",
                columns: table => new
                {
                    material_category_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    category_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_material_categories", x => x.material_category_id);
                });

            migrationBuilder.CreateTable(
                name: "materials",
                columns: table => new
                {
                    material_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    material_category_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    is_resource = table.Column<bool>(type: "boolean", nullable: false),
                    resource_type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    infrastructure_usage = table.Column<bool>(type: "boolean", nullable: false),
                    cogc_usage = table.Column<bool>(type: "boolean", nullable: false),
                    workforce_usage = table.Column<bool>(type: "boolean", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_materials", x => x.material_id);
                });

            migrationBuilder.CreateTable(
                name: "permissions",
                columns: table => new
                {
                    permission_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    grantor_user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    grantee_user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    group_id = table.Column<int>(type: "integer", nullable: false),
                    ship_information = table.Column<bool>(type: "boolean", nullable: false),
                    ship_repair = table.Column<bool>(type: "boolean", nullable: false),
                    ship_flight = table.Column<bool>(type: "boolean", nullable: false),
                    ship_inventory = table.Column<bool>(type: "boolean", nullable: false),
                    ship_fuel_inventory = table.Column<bool>(type: "boolean", nullable: false),
                    sites_location = table.Column<bool>(type: "boolean", nullable: false),
                    sites_workforces = table.Column<bool>(type: "boolean", nullable: false),
                    sites_experts = table.Column<bool>(type: "boolean", nullable: false),
                    sites_buildings = table.Column<bool>(type: "boolean", nullable: false),
                    sites_repair = table.Column<bool>(type: "boolean", nullable: false),
                    sites_reclaimable = table.Column<bool>(type: "boolean", nullable: false),
                    sites_production_lines = table.Column<bool>(type: "boolean", nullable: false),
                    storage_location = table.Column<bool>(type: "boolean", nullable: false),
                    storage_information = table.Column<bool>(type: "boolean", nullable: false),
                    storage_items = table.Column<bool>(type: "boolean", nullable: false),
                    trade_contract = table.Column<bool>(type: "boolean", nullable: false),
                    trade_cxos = table.Column<bool>(type: "boolean", nullable: false),
                    company_info = table.Column<bool>(type: "boolean", nullable: false),
                    company_liquid_currency = table.Column<bool>(type: "boolean", nullable: false),
                    company_headquarters = table.Column<bool>(type: "boolean", nullable: false),
                    misc_shipment_tracking = table.Column<bool>(type: "boolean", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_permissions", x => x.permission_id);
                });

            migrationBuilder.CreateTable(
                name: "planet_sites",
                columns: table => new
                {
                    planet_site_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    owner_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    owner_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    owner_code = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    type = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    plot_number = table.Column<int>(type: "integer", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_planet_sites", x => x.planet_site_id);
                });

            migrationBuilder.CreateTable(
                name: "planets",
                columns: table => new
                {
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    namer_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    namer_user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    named_timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    nameable = table.Column<bool>(type: "boolean", nullable: false),
                    gravity = table.Column<double>(type: "double precision", nullable: false),
                    magnetic_field = table.Column<double>(type: "double precision", nullable: false),
                    mass = table.Column<double>(type: "double precision", nullable: false),
                    mass_earth = table.Column<double>(type: "double precision", nullable: false),
                    semi_major_axis = table.Column<double>(type: "double precision", nullable: false),
                    eccentricity = table.Column<double>(type: "double precision", nullable: false),
                    inclination = table.Column<double>(type: "double precision", nullable: false),
                    right_ascension = table.Column<double>(type: "double precision", nullable: false),
                    periapsis = table.Column<double>(type: "double precision", nullable: false),
                    orbit_index = table.Column<int>(type: "integer", nullable: false),
                    pressure = table.Column<double>(type: "double precision", nullable: false),
                    radiation = table.Column<double>(type: "double precision", nullable: false),
                    radius = table.Column<double>(type: "double precision", nullable: false),
                    sunlight = table.Column<double>(type: "double precision", nullable: false),
                    temperature = table.Column<double>(type: "double precision", nullable: false),
                    fertility = table.Column<double>(type: "double precision", nullable: true),
                    surface = table.Column<bool>(type: "boolean", nullable: false),
                    plots = table.Column<int>(type: "integer", nullable: false),
                    country_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    country_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    country_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    governor_user_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    governor_user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    governing_company_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    governing_company_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    governing_company_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    currency_numeric_code = table.Column<int>(type: "integer", nullable: true),
                    currency_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: true),
                    currency_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    collection_currency_numeric_code = table.Column<int>(type: "integer", nullable: true),
                    collection_currency_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: true),
                    collection_currency_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    local_market_fee_base = table.Column<double>(type: "double precision", nullable: true),
                    local_market_time_factor = table.Column<double>(type: "double precision", nullable: true),
                    warehouse_fee = table.Column<double>(type: "double precision", nullable: true),
                    population_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    chamber_of_commerce_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    warehouse_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    local_market_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    administration_center_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    shipyard_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    supports_ghost_sites = table.Column<bool>(type: "boolean", nullable: false),
                    current_cogc_program = table.Column<string>(type: "character varying(31)", maxLength: 31, nullable: true),
                    current_cogc_program_end_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_planets", x => x.planet_id);
                });

            migrationBuilder.CreateTable(
                name: "production_lines",
                columns: table => new
                {
                    production_line_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    site_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    location_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    location_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    location_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    building_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    building_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    capacity = table.Column<int>(type: "integer", nullable: false),
                    slots = table.Column<int>(type: "integer", nullable: false),
                    efficiency = table.Column<double>(type: "double precision", nullable: false),
                    condition = table.Column<double>(type: "double precision", nullable: false),
                    raw_workforce_efficiencies = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    raw_efficiency_factors = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_production_lines", x => x.production_line_id);
                });

            migrationBuilder.CreateTable(
                name: "registrations",
                columns: table => new
                {
                    registration_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    display_user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    registration_guid = table.Column<Guid>(type: "uuid", nullable: false),
                    registration_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_registrations", x => x.registration_id);
                });

            migrationBuilder.CreateTable(
                name: "sectors",
                columns: table => new
                {
                    sector_id = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    hex_q = table.Column<int>(type: "integer", nullable: false),
                    hex_r = table.Column<int>(type: "integer", nullable: false),
                    hex_s = table.Column<int>(type: "integer", nullable: false),
                    size = table.Column<int>(type: "integer", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_sectors", x => x.sector_id);
                });

            migrationBuilder.CreateTable(
                name: "ships",
                columns: table => new
                {
                    ship_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    store_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    stl_fuel_store_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    ftl_fuel_store_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    registration = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    commisioning_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    blueprint_natural_id = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    current_location_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    current_location_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    current_location_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    flight_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    acceleration = table.Column<double>(type: "double precision", nullable: false),
                    thrust = table.Column<double>(type: "double precision", nullable: false),
                    mass = table.Column<double>(type: "double precision", nullable: false),
                    operating_empty_mass = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    reactor_power = table.Column<double>(type: "double precision", nullable: true),
                    emitter_power = table.Column<double>(type: "double precision", nullable: false),
                    stl_fuel_flow_rate = table.Column<double>(type: "double precision", nullable: false),
                    operating_time_stl_milliseconds = table.Column<long>(type: "bigint", nullable: false),
                    operating_time_ftl_milliseconds = table.Column<long>(type: "bigint", nullable: false),
                    condition = table.Column<double>(type: "double precision", nullable: false),
                    last_repair = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ships", x => x.ship_id);
                });

            migrationBuilder.CreateTable(
                name: "simulation_data",
                columns: table => new
                {
                    simulation_data_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    simulation_interval = table.Column<int>(type: "integer", nullable: false),
                    flight_stl_factor = table.Column<int>(type: "integer", nullable: false),
                    flight_ftl_factor = table.Column<int>(type: "integer", nullable: false),
                    planetary_motion_factor = table.Column<int>(type: "integer", nullable: false),
                    parsec_length = table.Column<int>(type: "integer", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_simulation_data", x => x.simulation_data_id);
                });

            migrationBuilder.CreateTable(
                name: "sites",
                columns: table => new
                {
                    site_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    location_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    location_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    location_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    founded = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    area = table.Column<int>(type: "integer", nullable: false),
                    invested_permits = table.Column<int>(type: "integer", nullable: false),
                    maximum_permits = table.Column<int>(type: "integer", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_sites", x => x.site_id);
                });

            migrationBuilder.CreateTable(
                name: "stars",
                columns: table => new
                {
                    star_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    system_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    system_natural_id = table.Column<string>(type: "character varying(6)", maxLength: 6, nullable: false),
                    system_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    sector_id = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    sub_sector_id = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    type = table.Column<string>(type: "character varying(1)", maxLength: 1, nullable: false),
                    luminosity = table.Column<double>(type: "double precision", nullable: false),
                    mass = table.Column<double>(type: "double precision", nullable: false),
                    mass_sol = table.Column<double>(type: "double precision", nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_stars", x => x.star_id);
                });

            migrationBuilder.CreateTable(
                name: "stations",
                columns: table => new
                {
                    station_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    currency_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    country_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    country_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    country_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    governing_entity_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    governing_entity_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    governing_entity_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_stations", x => x.station_id);
                });

            migrationBuilder.CreateTable(
                name: "storages",
                columns: table => new
                {
                    storage_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    addressable_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    location_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    location_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    location_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    weight_load = table.Column<double>(type: "double precision", nullable: false),
                    weight_capacity = table.Column<double>(type: "double precision", nullable: false),
                    volume_load = table.Column<double>(type: "double precision", nullable: false),
                    volume_capacity = table.Column<double>(type: "double precision", nullable: false),
                    @fixed = table.Column<bool>(name: "fixed", type: "boolean", nullable: false),
                    trade_store = table.Column<bool>(type: "boolean", nullable: false),
                    rank = table.Column<int>(type: "integer", nullable: false),
                    locked = table.Column<bool>(type: "boolean", nullable: false),
                    type = table.Column<string>(type: "character varying(15)", maxLength: 15, nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_storages", x => x.storage_id);
                });

            migrationBuilder.CreateTable(
                name: "systems",
                columns: table => new
                {
                    system_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    nameable = table.Column<bool>(type: "boolean", nullable: false),
                    namer_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    namer_user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    natural_id = table.Column<string>(type: "character varying(6)", maxLength: 6, nullable: false),
                    type = table.Column<string>(type: "character varying(1)", maxLength: 1, nullable: false),
                    sector_id = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    sub_sector_id = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    position_x = table.Column<double>(type: "double precision", nullable: false),
                    position_y = table.Column<double>(type: "double precision", nullable: false),
                    position_z = table.Column<double>(type: "double precision", nullable: false),
                    meteroid_density = table.Column<double>(type: "double precision", nullable: true),
                    raw_connections = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    raw_planet_ids = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    raw_celestial_body_ids = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: true),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_systems", x => x.system_id);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    user_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    display_user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    password_hash = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    is_admin = table.Column<bool>(type: "boolean", nullable: false),
                    discord_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_users", x => x.user_id);
                });

            migrationBuilder.CreateTable(
                name: "workforce_requirements",
                columns: table => new
                {
                    workforce_requirement_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    level = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_workforce_requirements", x => x.workforce_requirement_id);
                });

            migrationBuilder.CreateTable(
                name: "workforces",
                columns: table => new
                {
                    workforce_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    pioneer_population = table.Column<int>(type: "integer", nullable: false),
                    pioneer_reserve = table.Column<int>(type: "integer", nullable: false),
                    pioneer_capacity = table.Column<int>(type: "integer", nullable: false),
                    pioneer_required = table.Column<int>(type: "integer", nullable: false),
                    pioneer_satisfaction = table.Column<double>(type: "double precision", nullable: false),
                    settler_population = table.Column<int>(type: "integer", nullable: false),
                    settler_reserve = table.Column<int>(type: "integer", nullable: false),
                    settler_capacity = table.Column<int>(type: "integer", nullable: false),
                    settler_required = table.Column<int>(type: "integer", nullable: false),
                    settler_satisfaction = table.Column<double>(type: "double precision", nullable: false),
                    technician_population = table.Column<int>(type: "integer", nullable: false),
                    technician_reserve = table.Column<int>(type: "integer", nullable: false),
                    technician_capacity = table.Column<int>(type: "integer", nullable: false),
                    technician_required = table.Column<int>(type: "integer", nullable: false),
                    technician_satisfaction = table.Column<double>(type: "double precision", nullable: false),
                    engineer_population = table.Column<int>(type: "integer", nullable: false),
                    engineer_reserve = table.Column<int>(type: "integer", nullable: false),
                    engineer_capacity = table.Column<int>(type: "integer", nullable: false),
                    engineer_required = table.Column<int>(type: "integer", nullable: false),
                    engineer_satisfaction = table.Column<double>(type: "double precision", nullable: false),
                    scientist_population = table.Column<int>(type: "integer", nullable: false),
                    scientist_reserve = table.Column<int>(type: "integer", nullable: false),
                    scientist_capacity = table.Column<int>(type: "integer", nullable: false),
                    scientist_required = table.Column<int>(type: "integer", nullable: false),
                    scientist_satisfaction = table.Column<double>(type: "double precision", nullable: false),
                    last_tick_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_workforces", x => x.workforce_id);
                });

            migrationBuilder.CreateTable(
                name: "building_costs",
                columns: table => new
                {
                    building_cost_id = table.Column<string>(type: "character varying(37)", maxLength: 37, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    building_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_building_costs", x => x.building_cost_id);
                    table.ForeignKey(
                        name: "fk_building_costs_buildings_building_id",
                        column: x => x.building_id,
                        principalTable: "buildings",
                        principalColumn: "building_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "building_recipes",
                columns: table => new
                {
                    building_recipe_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    duration_ms = table.Column<long>(type: "bigint", nullable: false),
                    building_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_building_recipes", x => x.building_recipe_id);
                    table.ForeignKey(
                        name: "fk_building_recipes_buildings_building_id",
                        column: x => x.building_id,
                        principalTable: "buildings",
                        principalColumn: "building_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "chat_messages",
                columns: table => new
                {
                    chat_message_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    sender_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    sender_user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    message_text = table.Column<string>(type: "character varying(1024)", maxLength: 1024, nullable: true),
                    message_timestamp = table.Column<long>(type: "bigint", nullable: false),
                    message_deleted = table.Column<bool>(type: "boolean", nullable: false),
                    deleted_by_user_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    deleted_by_user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    chat_channel_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_chat_messages", x => x.chat_message_id);
                    table.ForeignKey(
                        name: "fk_chat_messages_chat_channels_chat_channel_id",
                        column: x => x.chat_channel_id,
                        principalTable: "chat_channels",
                        principalColumn: "chat_channel_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "company_offices",
                columns: table => new
                {
                    company_office_id = table.Column<string>(type: "character varying(65)", maxLength: 65, nullable: false),
                    type = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    start = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    end = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    planet_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    planet_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    company_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_company_offices", x => x.company_office_id);
                    table.ForeignKey(
                        name: "fk_company_offices_companies_company_id",
                        column: x => x.company_id,
                        principalTable: "companies",
                        principalColumn: "company_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "company_planets",
                columns: table => new
                {
                    company_planet_id = table.Column<string>(type: "character varying(65)", maxLength: 65, nullable: false),
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    planet_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    planet_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    company_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_company_planets", x => x.company_planet_id);
                    table.ForeignKey(
                        name: "fk_company_planets_companies_company_id",
                        column: x => x.company_id,
                        principalTable: "companies",
                        principalColumn: "company_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "contract_conditions",
                columns: table => new
                {
                    contract_condition_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    index = table.Column<int>(type: "integer", nullable: false),
                    status = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    dependencies = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    deadline_duration = table.Column<long>(type: "bigint", nullable: true),
                    deadline = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    quantity_material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    quantity_amount = table.Column<int>(type: "integer", nullable: true),
                    address_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    address_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    address_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    block_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    picked_up_material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    picked_up_amount = table.Column<int>(type: "integer", nullable: true),
                    weight = table.Column<double>(type: "double precision", nullable: true),
                    volume = table.Column<double>(type: "double precision", nullable: true),
                    destination_address_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    destination_address_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    destination_address_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    shipment_item_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    country_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    reputation_change = table.Column<int>(type: "integer", nullable: true),
                    loan_interest_amount = table.Column<double>(type: "double precision", nullable: true),
                    loan_interest_currency = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: true),
                    loan_repayment_amount = table.Column<double>(type: "double precision", nullable: true),
                    loan_repayment_currency = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: true),
                    loan_total_amount = table.Column<double>(type: "double precision", nullable: true),
                    loan_total_currency = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: true),
                    contract_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_contract_conditions", x => x.contract_condition_id);
                    table.ForeignKey(
                        name: "fk_contract_conditions_contracts_contract_id",
                        column: x => x.contract_id,
                        principalTable: "contracts",
                        principalColumn: "contract_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "cx_buy_orders",
                columns: table => new
                {
                    cx_buy_order_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    company_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    company_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    company_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    item_count = table.Column<int>(type: "integer", nullable: true),
                    item_cost = table.Column<double>(type: "double precision", nullable: false),
                    cx_entry_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_cx_buy_orders", x => x.cx_buy_order_id);
                    table.ForeignKey(
                        name: "fk_cx_buy_orders_cx_entries_cx_entry_id",
                        column: x => x.cx_entry_id,
                        principalTable: "cx_entries",
                        principalColumn: "cx_entry_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "cx_sell_orders",
                columns: table => new
                {
                    cx_sell_order_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    company_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    company_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    company_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    item_count = table.Column<int>(type: "integer", nullable: true),
                    item_cost = table.Column<double>(type: "double precision", nullable: false),
                    cx_entry_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_cx_sell_orders", x => x.cx_sell_order_id);
                    table.ForeignKey(
                        name: "fk_cx_sell_orders_cx_entries_cx_entry_id",
                        column: x => x.cx_entry_id,
                        principalTable: "cx_entries",
                        principalColumn: "cx_entry_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "cxos_trades",
                columns: table => new
                {
                    cxos_trade_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    price_amount = table.Column<double>(type: "double precision", nullable: false),
                    price_currency = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    partner_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    partner_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    partner_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    cxos_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_cxos_trades", x => x.cxos_trade_id);
                    table.ForeignKey(
                        name: "fk_cxos_trades_cxo_ss_cxos_id",
                        column: x => x.cxos_id,
                        principalTable: "cxo_ss",
                        principalColumn: "cxos_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "cxpc_entries",
                columns: table => new
                {
                    cxpc_entry_id = table.Column<string>(type: "character varying(61)", maxLength: 61, nullable: false),
                    interval = table.Column<string>(type: "character varying(14)", maxLength: 14, nullable: false),
                    date_epoch_ms = table.Column<long>(type: "bigint", nullable: false),
                    open = table.Column<double>(type: "double precision", nullable: false),
                    close = table.Column<double>(type: "double precision", nullable: false),
                    high = table.Column<double>(type: "double precision", nullable: false),
                    low = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    traded = table.Column<int>(type: "integer", nullable: false),
                    cxpc_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_cxpc_entries", x => x.cxpc_entry_id);
                    table.ForeignKey(
                        name: "fk_cxpc_entries_cxp_cs_cxpc_id",
                        column: x => x.cxpc_id,
                        principalTable: "cxp_cs",
                        principalColumn: "cxpc_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "expert_entries",
                columns: table => new
                {
                    expert_entry_id = table.Column<string>(type: "character varying(52)", maxLength: 52, nullable: false),
                    category = table.Column<string>(type: "character varying(19)", maxLength: 19, nullable: false),
                    current = table.Column<int>(type: "integer", nullable: false),
                    limit = table.Column<int>(type: "integer", nullable: false),
                    available = table.Column<int>(type: "integer", nullable: false),
                    efficiency_gain = table.Column<double>(type: "double precision", nullable: false),
                    expert_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_expert_entries", x => x.expert_entry_id);
                    table.ForeignKey(
                        name: "fk_expert_entries_experts_expert_id",
                        column: x => x.expert_id,
                        principalTable: "experts",
                        principalColumn: "expert_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "flight_segments",
                columns: table => new
                {
                    flight_segment_id = table.Column<string>(type: "character varying(36)", maxLength: 36, nullable: false),
                    type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    origin_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    origin_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    origin_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    origin_semi_major_axis = table.Column<double>(type: "double precision", nullable: true),
                    origin_eccentricity = table.Column<double>(type: "double precision", nullable: true),
                    origin_inclination = table.Column<double>(type: "double precision", nullable: true),
                    origin_right_ascension = table.Column<double>(type: "double precision", nullable: true),
                    origin_periapsis = table.Column<double>(type: "double precision", nullable: true),
                    departure = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    arrival = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    destination_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    destination_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    destination_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    destination_semi_major_axis = table.Column<double>(type: "double precision", nullable: true),
                    destination_eccentricity = table.Column<double>(type: "double precision", nullable: true),
                    destination_inclination = table.Column<double>(type: "double precision", nullable: true),
                    destination_right_ascension = table.Column<double>(type: "double precision", nullable: true),
                    destination_periapsis = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_start_position_x = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_start_position_y = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_start_position_z = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_target_position_x = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_target_position_y = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_target_position_z = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_center_position_x = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_center_position_y = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_center_position_z = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_alpha = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_semi_major_axis = table.Column<double>(type: "double precision", nullable: true),
                    transfer_ellipse_semi_minor_axis = table.Column<double>(type: "double precision", nullable: true),
                    stl_distance = table.Column<double>(type: "double precision", nullable: true),
                    ftl_distance = table.Column<double>(type: "double precision", nullable: true),
                    stl_fuel_consumption = table.Column<double>(type: "double precision", nullable: true),
                    ftl_fuel_consumption = table.Column<double>(type: "double precision", nullable: true),
                    damage = table.Column<double>(type: "double precision", nullable: true),
                    flight_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_flight_segments", x => x.flight_segment_id);
                    table.ForeignKey(
                        name: "fk_flight_segments_flights_flight_id",
                        column: x => x.flight_id,
                        principalTable: "flights",
                        principalColumn: "flight_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "fx_buy_orders",
                columns: table => new
                {
                    fx_buy_order_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_company_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    fxid = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_fx_buy_orders", x => x.fx_buy_order_id);
                    table.ForeignKey(
                        name: "fk_fx_buy_orders_fx_fxid",
                        column: x => x.fxid,
                        principalTable: "fx",
                        principalColumn: "fxid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "fx_sell_orders",
                columns: table => new
                {
                    fx_sell_order_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_company_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    fxid = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_fx_sell_orders", x => x.fx_sell_order_id);
                    table.ForeignKey(
                        name: "fk_fx_sell_orders_fx_fxid",
                        column: x => x.fxid,
                        principalTable: "fx",
                        principalColumn: "fxid",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "group_admins",
                columns: table => new
                {
                    group_admin_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    group_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_group_admins", x => x.group_admin_id);
                    table.ForeignKey(
                        name: "fk_group_admins_groups_group_id",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "group_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "group_pending_invites",
                columns: table => new
                {
                    group_pending_invite_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    admin = table.Column<bool>(type: "boolean", nullable: false),
                    group_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_group_pending_invites", x => x.group_pending_invite_id);
                    table.ForeignKey(
                        name: "fk_group_pending_invites_groups_group_id",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "group_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "group_users",
                columns: table => new
                {
                    group_user_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    group_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_group_users", x => x.group_user_id);
                    table.ForeignKey(
                        name: "fk_group_users_groups_group_id",
                        column: x => x.group_id,
                        principalTable: "groups",
                        principalColumn: "group_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "jump_cache_route_jumps",
                columns: table => new
                {
                    jump_cache_route_jump_id = table.Column<string>(type: "character varying(131)", maxLength: 131, nullable: false),
                    source_system_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    source_system_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    source_system_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    destination_system_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    destination_system_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    destination_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    distance = table.Column<double>(type: "double precision", nullable: false),
                    jump_cache_id = table.Column<string>(type: "character varying(65)", maxLength: 65, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_jump_cache_route_jumps", x => x.jump_cache_route_jump_id);
                    table.ForeignKey(
                        name: "fk_jump_cache_route_jumps_jump_cache_jump_cache_id",
                        column: x => x.jump_cache_id,
                        principalTable: "jump_cache",
                        principalColumn: "jump_cache_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "cogc_programs",
                columns: table => new
                {
                    planet_cogc_program_id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    start_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    end_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_cogc_programs", x => x.planet_cogc_program_id);
                    table.ForeignKey(
                        name: "fk_cogc_programs_planets_planet_id",
                        column: x => x.planet_id,
                        principalTable: "planets",
                        principalColumn: "planet_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "planet_population_projects",
                columns: table => new
                {
                    planet_population_project_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    type = table.Column<string>(type: "character varying(26)", maxLength: 26, nullable: false),
                    project_identifier = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    level = table.Column<int>(type: "integer", nullable: false),
                    active_level = table.Column<int>(type: "integer", nullable: false),
                    current_level = table.Column<int>(type: "integer", nullable: false),
                    upgrade_status = table.Column<double>(type: "double precision", nullable: false),
                    upgrade_costs_raw = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_planet_population_projects", x => x.planet_population_project_id);
                    table.ForeignKey(
                        name: "fk_planet_population_projects_planets_planet_id",
                        column: x => x.planet_id,
                        principalTable: "planets",
                        principalColumn: "planet_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "planet_population_reports",
                columns: table => new
                {
                    planet_population_report_id = table.Column<string>(type: "character varying(40)", maxLength: 40, nullable: false),
                    simulation_period = table.Column<int>(type: "integer", nullable: false),
                    explorers_grace_enabled = table.Column<bool>(type: "boolean", nullable: false),
                    report_timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    next_population_pioneer = table.Column<int>(type: "integer", nullable: false),
                    next_population_settler = table.Column<int>(type: "integer", nullable: false),
                    next_population_technician = table.Column<int>(type: "integer", nullable: false),
                    next_population_engineer = table.Column<int>(type: "integer", nullable: false),
                    next_population_scientist = table.Column<int>(type: "integer", nullable: false),
                    population_difference_pioneer = table.Column<int>(type: "integer", nullable: false),
                    population_difference_settler = table.Column<int>(type: "integer", nullable: false),
                    population_difference_technician = table.Column<int>(type: "integer", nullable: false),
                    population_difference_engineer = table.Column<int>(type: "integer", nullable: false),
                    population_difference_scientist = table.Column<int>(type: "integer", nullable: false),
                    average_happiness_pioneer = table.Column<double>(type: "double precision", nullable: false),
                    average_happiness_settler = table.Column<double>(type: "double precision", nullable: false),
                    average_happiness_technician = table.Column<double>(type: "double precision", nullable: false),
                    average_happiness_engineer = table.Column<double>(type: "double precision", nullable: false),
                    average_happiness_scientist = table.Column<double>(type: "double precision", nullable: false),
                    unemployment_rate_pioneer = table.Column<double>(type: "double precision", nullable: false),
                    unemployment_rate_settler = table.Column<double>(type: "double precision", nullable: false),
                    unemployment_rate_technician = table.Column<double>(type: "double precision", nullable: false),
                    unemployment_rate_engineer = table.Column<double>(type: "double precision", nullable: false),
                    unemployment_rate_scientist = table.Column<double>(type: "double precision", nullable: false),
                    open_jobs_pioneer = table.Column<double>(type: "double precision", nullable: false),
                    open_jobs_settler = table.Column<double>(type: "double precision", nullable: false),
                    open_jobs_technician = table.Column<double>(type: "double precision", nullable: false),
                    open_jobs_engineer = table.Column<double>(type: "double precision", nullable: false),
                    open_jobs_scientist = table.Column<double>(type: "double precision", nullable: false),
                    need_fulfillment_life_support = table.Column<double>(type: "double precision", nullable: false),
                    need_fulfillment_safety = table.Column<double>(type: "double precision", nullable: false),
                    need_fulfillment_health = table.Column<double>(type: "double precision", nullable: false),
                    need_fulfillment_comfort = table.Column<double>(type: "double precision", nullable: false),
                    need_fulfillment_culture = table.Column<double>(type: "double precision", nullable: false),
                    need_fulfillment_education = table.Column<double>(type: "double precision", nullable: false),
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_planet_population_reports", x => x.planet_population_report_id);
                    table.ForeignKey(
                        name: "fk_planet_population_reports_planets_planet_id",
                        column: x => x.planet_id,
                        principalTable: "planets",
                        principalColumn: "planet_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "planet_resources",
                columns: table => new
                {
                    planet_resource_id = table.Column<string>(type: "character varying(65)", maxLength: 65, nullable: false),
                    material_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    type = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: false),
                    concentration = table.Column<double>(type: "double precision", nullable: false),
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_planet_resources", x => x.planet_resource_id);
                    table.ForeignKey(
                        name: "fk_planet_resources_planets_planet_id",
                        column: x => x.planet_id,
                        principalTable: "planets",
                        principalColumn: "planet_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "planet_workforce_fees",
                columns: table => new
                {
                    planet_workforce_fee_id = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    category = table.Column<string>(type: "character varying(19)", maxLength: 19, nullable: false),
                    workforce_level = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    fee = table.Column<double>(type: "double precision", nullable: true),
                    currency = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: true),
                    planet_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_planet_workforce_fees", x => x.planet_workforce_fee_id);
                    table.ForeignKey(
                        name: "fk_planet_workforce_fees_planets_planet_id",
                        column: x => x.planet_id,
                        principalTable: "planets",
                        principalColumn: "planet_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "production_line_orders",
                columns: table => new
                {
                    production_line_order_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    raw_inputs = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    raw_outputs = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    started = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    completion = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    duration_ms = table.Column<long>(type: "bigint", nullable: true),
                    last_updated = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    completion_pecentage = table.Column<double>(type: "double precision", nullable: false),
                    halted = table.Column<bool>(type: "boolean", nullable: false),
                    fee_currency_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: true),
                    fee = table.Column<double>(type: "double precision", nullable: true),
                    fee_collector_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    fee_collector_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    fee_collector_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    recurring = table.Column<bool>(type: "boolean", nullable: false),
                    production_line_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_production_line_orders", x => x.production_line_order_id);
                    table.ForeignKey(
                        name: "fk_production_line_orders_production_lines_production_line_id",
                        column: x => x.production_line_id,
                        principalTable: "production_lines",
                        principalColumn: "production_line_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "sub_sectors",
                columns: table => new
                {
                    sub_sector_id = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    vertex0_x = table.Column<double>(type: "double precision", nullable: false),
                    vertex0_y = table.Column<double>(type: "double precision", nullable: false),
                    vertex0_z = table.Column<double>(type: "double precision", nullable: false),
                    vertex1_x = table.Column<double>(type: "double precision", nullable: false),
                    vertex1_y = table.Column<double>(type: "double precision", nullable: false),
                    vertex1_z = table.Column<double>(type: "double precision", nullable: false),
                    vertex2_x = table.Column<double>(type: "double precision", nullable: false),
                    vertex2_y = table.Column<double>(type: "double precision", nullable: false),
                    vertex2_z = table.Column<double>(type: "double precision", nullable: false),
                    sector_id = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_sub_sectors", x => x.sub_sector_id);
                    table.ForeignKey(
                        name: "fk_sub_sectors_sectors_sector_id",
                        column: x => x.sector_id,
                        principalTable: "sectors",
                        principalColumn: "sector_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ship_repair_materials",
                columns: table => new
                {
                    ship_repair_material_id = table.Column<string>(type: "character varying(65)", maxLength: 65, nullable: false),
                    material_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    ship_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_ship_repair_materials", x => x.ship_repair_material_id);
                    table.ForeignKey(
                        name: "fk_ship_repair_materials_ships_ship_id",
                        column: x => x.ship_id,
                        principalTable: "ships",
                        principalColumn: "ship_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "site_buildings",
                columns: table => new
                {
                    site_building_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    area = table.Column<int>(type: "integer", nullable: false),
                    creation_time = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    building_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    building_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    building_type = table.Column<string>(type: "character varying(20)", maxLength: 20, nullable: false),
                    book_value_currency_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    book_value_amount = table.Column<double>(type: "double precision", nullable: false),
                    condition = table.Column<double>(type: "double precision", nullable: false),
                    last_repair = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    raw_reclaimable_materials = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    raw_repair_materials = table.Column<string>(type: "character varying(500)", maxLength: 500, nullable: false),
                    site_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_site_buildings", x => x.site_building_id);
                    table.ForeignKey(
                        name: "fk_site_buildings_sites_site_id",
                        column: x => x.site_id,
                        principalTable: "sites",
                        principalColumn: "site_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "storage_items",
                columns: table => new
                {
                    storage_item_id = table.Column<string>(type: "character varying(65)", maxLength: 65, nullable: false),
                    item_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    material_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    amount = table.Column<int>(type: "integer", nullable: true),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    type = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    value_currency_code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: true),
                    value = table.Column<double>(type: "double precision", nullable: true),
                    storage_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_storage_items", x => x.storage_item_id);
                    table.ForeignKey(
                        name: "fk_storage_items_storages_storage_id",
                        column: x => x.storage_id,
                        principalTable: "storages",
                        principalColumn: "storage_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "workforce_requirement_need",
                columns: table => new
                {
                    workforce_requirement_need_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    essential = table.Column<bool>(type: "boolean", nullable: false),
                    material_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    material_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    units_per100 = table.Column<double>(type: "double precision", nullable: false),
                    workforce_requirement_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_workforce_requirement_need", x => x.workforce_requirement_need_id);
                    table.ForeignKey(
                        name: "fk_workforce_requirement_need_workforce_requirements_workforce",
                        column: x => x.workforce_requirement_id,
                        principalTable: "workforce_requirements",
                        principalColumn: "workforce_requirement_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "workforce_needs",
                columns: table => new
                {
                    workforce_need_id = table.Column<string>(type: "character varying(48)", maxLength: 48, nullable: false),
                    level = table.Column<string>(type: "character varying(10)", maxLength: 10, nullable: false),
                    category = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    essential = table.Column<bool>(type: "boolean", nullable: false),
                    material_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    material_ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    material_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    satisfaction = table.Column<double>(type: "double precision", nullable: false),
                    units_per_interval = table.Column<double>(type: "double precision", nullable: false),
                    units_per100 = table.Column<double>(type: "double precision", nullable: false),
                    workforce_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_workforce_needs", x => x.workforce_need_id);
                    table.ForeignKey(
                        name: "fk_workforce_needs_workforces_workforce_id",
                        column: x => x.workforce_id,
                        principalTable: "workforces",
                        principalColumn: "workforce_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "building_recipe_inputs",
                columns: table => new
                {
                    building_recipe_input_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    building_recipe_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_building_recipe_inputs", x => x.building_recipe_input_id);
                    table.ForeignKey(
                        name: "fk_building_recipe_inputs_building_recipes_building_recipe_id",
                        column: x => x.building_recipe_id,
                        principalTable: "building_recipes",
                        principalColumn: "building_recipe_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "building_recipe_outputs",
                columns: table => new
                {
                    building_recipe_output_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false),
                    id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    weight = table.Column<double>(type: "double precision", nullable: false),
                    volume = table.Column<double>(type: "double precision", nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    building_recipe_id = table.Column<string>(type: "character varying(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_building_recipe_outputs", x => x.building_recipe_output_id);
                    table.ForeignKey(
                        name: "fk_building_recipe_outputs_building_recipes_building_recipe_id",
                        column: x => x.building_recipe_id,
                        principalTable: "building_recipes",
                        principalColumn: "building_recipe_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "planet_population_project_upkeeps",
                columns: table => new
                {
                    planet_population_project_upkeep_id = table.Column<string>(type: "character varying(37)", maxLength: 37, nullable: false),
                    stored = table.Column<int>(type: "integer", nullable: false),
                    store_capacity = table.Column<int>(type: "integer", nullable: false),
                    duration = table.Column<int>(type: "integer", nullable: false),
                    next_tick = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    ticker = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: false),
                    amount = table.Column<int>(type: "integer", nullable: false),
                    current_amount = table.Column<int>(type: "integer", nullable: false),
                    planet_population_project_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_planet_population_project_upkeeps", x => x.planet_population_project_upkeep_id);
                    table.ForeignKey(
                        name: "fk_planet_population_project_upkeeps_planet_population_project",
                        column: x => x.planet_population_project_id,
                        principalTable: "planet_population_projects",
                        principalColumn: "planet_population_project_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "ix_api_keys_key",
                table: "api_keys",
                column: "key");

            migrationBuilder.CreateIndex(
                name: "ix_api_keys_user_name",
                table: "api_keys",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_building_costs_building_id",
                table: "building_costs",
                column: "building_id");

            migrationBuilder.CreateIndex(
                name: "ix_building_recipe_inputs_building_recipe_id",
                table: "building_recipe_inputs",
                column: "building_recipe_id");

            migrationBuilder.CreateIndex(
                name: "ix_building_recipe_outputs_building_recipe_id",
                table: "building_recipe_outputs",
                column: "building_recipe_id");

            migrationBuilder.CreateIndex(
                name: "ix_building_recipes_building_id",
                table: "building_recipes",
                column: "building_id");

            migrationBuilder.CreateIndex(
                name: "ix_buildings_ticker",
                table: "buildings",
                column: "ticker");

            migrationBuilder.CreateIndex(
                name: "ix_chat_messages_chat_channel_id",
                table: "chat_messages",
                column: "chat_channel_id");

            migrationBuilder.CreateIndex(
                name: "ix_chat_messages_message_timestamp",
                table: "chat_messages",
                column: "message_timestamp");

            migrationBuilder.CreateIndex(
                name: "ix_chat_messages_timestamp",
                table: "chat_messages",
                column: "timestamp");

            migrationBuilder.CreateIndex(
                name: "ix_cogc_programs_planet_id",
                table: "cogc_programs",
                column: "planet_id");

            migrationBuilder.CreateIndex(
                name: "ix_comex_exchanges_code",
                table: "comex_exchanges",
                column: "code");

            migrationBuilder.CreateIndex(
                name: "ix_companies_code",
                table: "companies",
                column: "code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_company_offices_company_id",
                table: "company_offices",
                column: "company_id");

            migrationBuilder.CreateIndex(
                name: "ix_company_planets_company_id",
                table: "company_planets",
                column: "company_id");

            migrationBuilder.CreateIndex(
                name: "ix_company_planets_planet_id",
                table: "company_planets",
                column: "planet_id");

            migrationBuilder.CreateIndex(
                name: "ix_company_planets_planet_name",
                table: "company_planets",
                column: "planet_name");

            migrationBuilder.CreateIndex(
                name: "ix_company_planets_planet_natural_id",
                table: "company_planets",
                column: "planet_natural_id");

            migrationBuilder.CreateIndex(
                name: "ix_contract_conditions_contract_id",
                table: "contract_conditions",
                column: "contract_id");

            migrationBuilder.CreateIndex(
                name: "ix_countries_code",
                table: "countries",
                column: "code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_cx_buy_orders_company_code",
                table: "cx_buy_orders",
                column: "company_code");

            migrationBuilder.CreateIndex(
                name: "ix_cx_buy_orders_company_id",
                table: "cx_buy_orders",
                column: "company_id");

            migrationBuilder.CreateIndex(
                name: "ix_cx_buy_orders_company_name",
                table: "cx_buy_orders",
                column: "company_name");

            migrationBuilder.CreateIndex(
                name: "ix_cx_buy_orders_cx_entry_id",
                table: "cx_buy_orders",
                column: "cx_entry_id");

            migrationBuilder.CreateIndex(
                name: "ix_cx_entries_exchange_code",
                table: "cx_entries",
                column: "exchange_code");

            migrationBuilder.CreateIndex(
                name: "ix_cx_entries_material_ticker",
                table: "cx_entries",
                column: "material_ticker");

            migrationBuilder.CreateIndex(
                name: "ix_cx_entries_material_ticker_exchange_code",
                table: "cx_entries",
                columns: new[] { "material_ticker", "exchange_code" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_cx_sell_orders_company_code",
                table: "cx_sell_orders",
                column: "company_code");

            migrationBuilder.CreateIndex(
                name: "ix_cx_sell_orders_company_id",
                table: "cx_sell_orders",
                column: "company_id");

            migrationBuilder.CreateIndex(
                name: "ix_cx_sell_orders_company_name",
                table: "cx_sell_orders",
                column: "company_name");

            migrationBuilder.CreateIndex(
                name: "ix_cx_sell_orders_cx_entry_id",
                table: "cx_sell_orders",
                column: "cx_entry_id");

            migrationBuilder.CreateIndex(
                name: "ix_cxo_ss_exchange_code",
                table: "cxo_ss",
                column: "exchange_code");

            migrationBuilder.CreateIndex(
                name: "ix_cxo_ss_material_ticker",
                table: "cxo_ss",
                column: "material_ticker");

            migrationBuilder.CreateIndex(
                name: "ix_cxos_trades_cxos_id",
                table: "cxos_trades",
                column: "cxos_id");

            migrationBuilder.CreateIndex(
                name: "ix_cxp_cs_exchange_code",
                table: "cxp_cs",
                column: "exchange_code");

            migrationBuilder.CreateIndex(
                name: "ix_cxp_cs_material_ticker",
                table: "cxp_cs",
                column: "material_ticker");

            migrationBuilder.CreateIndex(
                name: "ix_cxp_cs_material_ticker_exchange_code",
                table: "cxp_cs",
                columns: new[] { "material_ticker", "exchange_code" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_cxpc_entries_cxpc_id",
                table: "cxpc_entries",
                column: "cxpc_id");

            migrationBuilder.CreateIndex(
                name: "ix_expert_entries_expert_id",
                table: "expert_entries",
                column: "expert_id");

            migrationBuilder.CreateIndex(
                name: "ix_flight_segments_flight_id",
                table: "flight_segments",
                column: "flight_id");

            migrationBuilder.CreateIndex(
                name: "ix_fx_ticker",
                table: "fx",
                column: "ticker",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_fx_buy_orders_fxid",
                table: "fx_buy_orders",
                column: "fxid");

            migrationBuilder.CreateIndex(
                name: "ix_fx_buy_orders_user_company_code",
                table: "fx_buy_orders",
                column: "user_company_code");

            migrationBuilder.CreateIndex(
                name: "ix_fx_buy_orders_user_name",
                table: "fx_buy_orders",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_fx_sell_orders_fxid",
                table: "fx_sell_orders",
                column: "fxid");

            migrationBuilder.CreateIndex(
                name: "ix_fx_sell_orders_user_company_code",
                table: "fx_sell_orders",
                column: "user_company_code");

            migrationBuilder.CreateIndex(
                name: "ix_fx_sell_orders_user_name",
                table: "fx_sell_orders",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_group_admins_group_id",
                table: "group_admins",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_group_admins_user_name",
                table: "group_admins",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_group_pending_invites_group_id",
                table: "group_pending_invites",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_group_pending_invites_user_name",
                table: "group_pending_invites",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_group_users_group_id",
                table: "group_users",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_group_users_user_name",
                table: "group_users",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_groups_group_name",
                table: "groups",
                column: "group_name");

            migrationBuilder.CreateIndex(
                name: "ix_jump_cache_destination_natural_id",
                table: "jump_cache",
                column: "destination_natural_id");

            migrationBuilder.CreateIndex(
                name: "ix_jump_cache_destination_system_id",
                table: "jump_cache",
                column: "destination_system_id");

            migrationBuilder.CreateIndex(
                name: "ix_jump_cache_destination_system_name",
                table: "jump_cache",
                column: "destination_system_name");

            migrationBuilder.CreateIndex(
                name: "ix_jump_cache_source_system_id",
                table: "jump_cache",
                column: "source_system_id");

            migrationBuilder.CreateIndex(
                name: "ix_jump_cache_source_system_name",
                table: "jump_cache",
                column: "source_system_name");

            migrationBuilder.CreateIndex(
                name: "ix_jump_cache_source_system_natural_id",
                table: "jump_cache",
                column: "source_system_natural_id");

            migrationBuilder.CreateIndex(
                name: "ix_jump_cache_route_jumps_destination_system_id",
                table: "jump_cache_route_jumps",
                column: "destination_system_id");

            migrationBuilder.CreateIndex(
                name: "ix_jump_cache_route_jumps_jump_cache_id",
                table: "jump_cache_route_jumps",
                column: "jump_cache_id");

            migrationBuilder.CreateIndex(
                name: "ix_jump_cache_route_jumps_source_system_id",
                table: "jump_cache_route_jumps",
                column: "source_system_id");

            migrationBuilder.CreateIndex(
                name: "ix_local_market_ads_local_market_id",
                table: "local_market_ads",
                column: "local_market_id");

            migrationBuilder.CreateIndex(
                name: "ix_local_market_ads_planet_id",
                table: "local_market_ads",
                column: "planet_id");

            migrationBuilder.CreateIndex(
                name: "ix_local_market_ads_planet_name",
                table: "local_market_ads",
                column: "planet_name");

            migrationBuilder.CreateIndex(
                name: "ix_local_market_ads_planet_natural_id",
                table: "local_market_ads",
                column: "planet_natural_id");

            migrationBuilder.CreateIndex(
                name: "ix_materials_material_category_id",
                table: "materials",
                column: "material_category_id");

            migrationBuilder.CreateIndex(
                name: "ix_materials_ticker",
                table: "materials",
                column: "ticker");

            migrationBuilder.CreateIndex(
                name: "ix_permissions_grantee_user_name",
                table: "permissions",
                column: "grantee_user_name");

            migrationBuilder.CreateIndex(
                name: "ix_permissions_grantor_user_name",
                table: "permissions",
                column: "grantor_user_name");

            migrationBuilder.CreateIndex(
                name: "ix_permissions_grantor_user_name_grantee_user_name",
                table: "permissions",
                columns: new[] { "grantor_user_name", "grantee_user_name" },
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_permissions_group_id",
                table: "permissions",
                column: "group_id");

            migrationBuilder.CreateIndex(
                name: "ix_planet_population_project_upkeeps_planet_population_project",
                table: "planet_population_project_upkeeps",
                column: "planet_population_project_id");

            migrationBuilder.CreateIndex(
                name: "ix_planet_population_projects_planet_id",
                table: "planet_population_projects",
                column: "planet_id");

            migrationBuilder.CreateIndex(
                name: "ix_planet_population_reports_planet_id",
                table: "planet_population_reports",
                column: "planet_id");

            migrationBuilder.CreateIndex(
                name: "ix_planet_resources_planet_id",
                table: "planet_resources",
                column: "planet_id");

            migrationBuilder.CreateIndex(
                name: "ix_planet_sites_owner_code",
                table: "planet_sites",
                column: "owner_code");

            migrationBuilder.CreateIndex(
                name: "ix_planet_sites_planet_id",
                table: "planet_sites",
                column: "planet_id");

            migrationBuilder.CreateIndex(
                name: "ix_planet_sites_type",
                table: "planet_sites",
                column: "type");

            migrationBuilder.CreateIndex(
                name: "ix_planet_workforce_fees_planet_id",
                table: "planet_workforce_fees",
                column: "planet_id");

            migrationBuilder.CreateIndex(
                name: "ix_planets_name",
                table: "planets",
                column: "name");

            migrationBuilder.CreateIndex(
                name: "ix_planets_natural_id",
                table: "planets",
                column: "natural_id");

            migrationBuilder.CreateIndex(
                name: "ix_planets_population_id",
                table: "planets",
                column: "population_id");

            migrationBuilder.CreateIndex(
                name: "ix_production_line_orders_production_line_id",
                table: "production_line_orders",
                column: "production_line_id");

            migrationBuilder.CreateIndex(
                name: "ix_registrations_user_name",
                table: "registrations",
                column: "user_name");

            migrationBuilder.CreateIndex(
                name: "ix_sectors_name",
                table: "sectors",
                column: "name");

            migrationBuilder.CreateIndex(
                name: "ix_ship_repair_materials_ship_id",
                table: "ship_repair_materials",
                column: "ship_id");

            migrationBuilder.CreateIndex(
                name: "ix_ships_ftl_fuel_store_id",
                table: "ships",
                column: "ftl_fuel_store_id");

            migrationBuilder.CreateIndex(
                name: "ix_ships_stl_fuel_store_id",
                table: "ships",
                column: "stl_fuel_store_id");

            migrationBuilder.CreateIndex(
                name: "ix_ships_store_id",
                table: "ships",
                column: "store_id");

            migrationBuilder.CreateIndex(
                name: "ix_site_buildings_site_id",
                table: "site_buildings",
                column: "site_id");

            migrationBuilder.CreateIndex(
                name: "ix_stars_system_id",
                table: "stars",
                column: "system_id");

            migrationBuilder.CreateIndex(
                name: "ix_stars_system_name",
                table: "stars",
                column: "system_name");

            migrationBuilder.CreateIndex(
                name: "ix_stars_system_natural_id",
                table: "stars",
                column: "system_natural_id");

            migrationBuilder.CreateIndex(
                name: "ix_storage_items_storage_id",
                table: "storage_items",
                column: "storage_id");

            migrationBuilder.CreateIndex(
                name: "ix_sub_sectors_sector_id",
                table: "sub_sectors",
                column: "sector_id");

            migrationBuilder.CreateIndex(
                name: "ix_systems_name",
                table: "systems",
                column: "name");

            migrationBuilder.CreateIndex(
                name: "ix_systems_natural_id",
                table: "systems",
                column: "natural_id");

            migrationBuilder.CreateIndex(
                name: "ix_users_user_name",
                table: "users",
                column: "user_name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "ix_workforce_needs_workforce_id",
                table: "workforce_needs",
                column: "workforce_id");

            migrationBuilder.CreateIndex(
                name: "ix_workforce_requirement_need_workforce_requirement_id",
                table: "workforce_requirement_need",
                column: "workforce_requirement_id");

            migrationBuilder.CreateIndex(
                name: "ix_workforce_requirements_level",
                table: "workforce_requirements",
                column: "level");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "api_keys");

            migrationBuilder.DropTable(
                name: "building_costs");

            migrationBuilder.DropTable(
                name: "building_recipe_inputs");

            migrationBuilder.DropTable(
                name: "building_recipe_outputs");

            migrationBuilder.DropTable(
                name: "chat_messages");

            migrationBuilder.DropTable(
                name: "cogc_programs");

            migrationBuilder.DropTable(
                name: "comex_exchanges");

            migrationBuilder.DropTable(
                name: "company_offices");

            migrationBuilder.DropTable(
                name: "company_planets");

            migrationBuilder.DropTable(
                name: "contract_conditions");

            migrationBuilder.DropTable(
                name: "countries");

            migrationBuilder.DropTable(
                name: "cx_buy_orders");

            migrationBuilder.DropTable(
                name: "cx_sell_orders");

            migrationBuilder.DropTable(
                name: "cxos_trades");

            migrationBuilder.DropTable(
                name: "cxpc_entries");

            migrationBuilder.DropTable(
                name: "expert_entries");

            migrationBuilder.DropTable(
                name: "flight_segments");

            migrationBuilder.DropTable(
                name: "fx_buy_orders");

            migrationBuilder.DropTable(
                name: "fx_sell_orders");

            migrationBuilder.DropTable(
                name: "group_admins");

            migrationBuilder.DropTable(
                name: "group_pending_invites");

            migrationBuilder.DropTable(
                name: "group_users");

            migrationBuilder.DropTable(
                name: "jump_cache_route_jumps");

            migrationBuilder.DropTable(
                name: "local_market_ads");

            migrationBuilder.DropTable(
                name: "material_categories");

            migrationBuilder.DropTable(
                name: "materials");

            migrationBuilder.DropTable(
                name: "permissions");

            migrationBuilder.DropTable(
                name: "planet_population_project_upkeeps");

            migrationBuilder.DropTable(
                name: "planet_population_reports");

            migrationBuilder.DropTable(
                name: "planet_resources");

            migrationBuilder.DropTable(
                name: "planet_sites");

            migrationBuilder.DropTable(
                name: "planet_workforce_fees");

            migrationBuilder.DropTable(
                name: "production_line_orders");

            migrationBuilder.DropTable(
                name: "registrations");

            migrationBuilder.DropTable(
                name: "ship_repair_materials");

            migrationBuilder.DropTable(
                name: "simulation_data");

            migrationBuilder.DropTable(
                name: "site_buildings");

            migrationBuilder.DropTable(
                name: "stars");

            migrationBuilder.DropTable(
                name: "stations");

            migrationBuilder.DropTable(
                name: "storage_items");

            migrationBuilder.DropTable(
                name: "sub_sectors");

            migrationBuilder.DropTable(
                name: "systems");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "workforce_needs");

            migrationBuilder.DropTable(
                name: "workforce_requirement_need");

            migrationBuilder.DropTable(
                name: "building_recipes");

            migrationBuilder.DropTable(
                name: "chat_channels");

            migrationBuilder.DropTable(
                name: "companies");

            migrationBuilder.DropTable(
                name: "contracts");

            migrationBuilder.DropTable(
                name: "cx_entries");

            migrationBuilder.DropTable(
                name: "cxo_ss");

            migrationBuilder.DropTable(
                name: "cxp_cs");

            migrationBuilder.DropTable(
                name: "experts");

            migrationBuilder.DropTable(
                name: "flights");

            migrationBuilder.DropTable(
                name: "fx");

            migrationBuilder.DropTable(
                name: "groups");

            migrationBuilder.DropTable(
                name: "jump_cache");

            migrationBuilder.DropTable(
                name: "planet_population_projects");

            migrationBuilder.DropTable(
                name: "production_lines");

            migrationBuilder.DropTable(
                name: "ships");

            migrationBuilder.DropTable(
                name: "sites");

            migrationBuilder.DropTable(
                name: "storages");

            migrationBuilder.DropTable(
                name: "sectors");

            migrationBuilder.DropTable(
                name: "workforces");

            migrationBuilder.DropTable(
                name: "workforce_requirements");

            migrationBuilder.DropTable(
                name: "buildings");

            migrationBuilder.DropTable(
                name: "planets");
        }
    }
}
