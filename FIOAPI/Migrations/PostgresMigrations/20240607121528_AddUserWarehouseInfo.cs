﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddUserWarehouseInfo : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "user_warehouses",
                columns: table => new
                {
                    user_warehouse_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    addressable_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    location_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    location_natural_id = table.Column<string>(type: "character varying(7)", maxLength: 7, nullable: true),
                    location_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    units = table.Column<int>(type: "integer", nullable: false),
                    weight_capacity = table.Column<double>(type: "double precision", nullable: false),
                    volume_capacity = table.Column<double>(type: "double precision", nullable: false),
                    next_payment = table.Column<DateTime>(type: "timestamp with time zone", nullable: true),
                    fee_currency = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: true),
                    fee = table.Column<double>(type: "double precision", nullable: true),
                    fee_collector_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    fee_collector_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    fee_collector_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    status = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_name_submitted = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    timestamp = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_user_warehouses", x => x.user_warehouse_id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "user_warehouses");
        }
    }
}
