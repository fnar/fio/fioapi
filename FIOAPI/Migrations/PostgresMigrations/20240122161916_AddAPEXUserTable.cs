﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FIOAPI.Migrations.PostgresMigrations
{
    /// <inheritdoc />
    public partial class AddAPEXUserTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "apex_users",
                columns: table => new
                {
                    apex_user_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: false),
                    user_name = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    subscription_level = table.Column<string>(type: "character varying(5)", maxLength: 5, nullable: true),
                    highest_tier = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    company_id = table.Column<string>(type: "character varying(32)", maxLength: 32, nullable: true),
                    company_name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    company_code = table.Column<string>(type: "character varying(4)", maxLength: 4, nullable: true),
                    created_timestamp = table.Column<long>(type: "bigint", nullable: true),
                    team = table.Column<bool>(type: "boolean", nullable: false),
                    moderator = table.Column<bool>(type: "boolean", nullable: false),
                    pioneer = table.Column<bool>(type: "boolean", nullable: false),
                    active_days_per_week = table.Column<int>(type: "integer", nullable: true),
                    last_online_timestamp = table.Column<long>(type: "bigint", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_apex_users", x => x.apex_user_id);
                });

            migrationBuilder.CreateIndex(
                name: "ix_apex_users_company_code",
                table: "apex_users",
                column: "company_code");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "apex_users");
        }
    }
}
