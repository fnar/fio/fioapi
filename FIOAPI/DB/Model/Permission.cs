﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Permission Model
    /// </summary>
    [Index(nameof(GrantorUserName))]
    [Index(nameof(GranteeUserName))]
    [Index(nameof(GroupId))]
    [Index(nameof(GrantorUserName), nameof(GranteeUserName), IsUnique = true)]
    public class Permission : IValidation, ICloneable
    {
        /// <summary>
        /// PermissionsId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int PermissionId { get; set; }

        /// <summary>
        /// The user that is giving permission access
        /// </summary>
        [Lowercase]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string GrantorUserName { get; set; } = null!;

        /// <summary>
        /// The user that is receiving the permission access
        /// </summary>
        [Lowercase]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = 1)]
        public string GranteeUserName { get; set; } = null!;

        /// <summary>
        /// The GroupId for this Permission access object
        /// </summary>
        [Range(0, Group.LargestGroupId)]
        public int GroupId { get; set; }

        /// <summary>
        /// ShipInformation
        /// </summary>
        public bool ShipInformation { get; set; }

        /// <summary>
        /// ShipRepair
        /// </summary>
        public bool ShipRepair { get; set; }

        /// <summary>
        /// ShipFlight
        /// </summary>
        public bool ShipFlight { get; set; }

        /// <summary>
        /// ShipInventory
        /// </summary>
        public bool ShipInventory { get; set; }

        /// <summary>
        /// ShipFuelInventory
        /// </summary>
        public bool ShipFuelInventory { get; set; }

        /// <summary>
        /// SiteLocation
        /// </summary>
        public bool SitesLocation { get; set; }

        /// <summary>
        /// SiteWorkforces
        /// </summary>
        public bool SitesWorkforces { get; set; }

        /// <summary>
        /// SiteExperts
        /// </summary>
        public bool SitesExperts { get; set; }

        /// <summary>
        /// SiteBuilding
        /// </summary>
        public bool SitesBuildings { get; set; }

        /// <summary>
        /// SiteRepair
        /// </summary>
        public bool SitesRepair { get; set; }

        /// <summary>
        /// SiteReclaimable
        /// </summary>
        public bool SitesReclaimable { get; set; }

        /// <summary>
        /// SiteProductionLines
        /// </summary>
        public bool SitesProductionLines { get; set; }

        /// <summary>
        /// StorageLocation
        /// </summary>
        public bool StorageLocation { get; set; }

        /// <summary>
        /// StorageInformation
        /// </summary>
        public bool StorageInformation { get; set; }

        /// <summary>
        /// StorageItems
        /// </summary>
        public bool StorageItems { get; set; }

        /// <summary>
        /// TradeContract
        /// </summary>
        public bool TradeContract { get; set; }

        /// <summary>
        /// TradeCXOS
        /// </summary>
        public bool TradeCXOS { get; set; }

        /// <summary>
        /// CompanyInfo
        /// </summary>
        public bool CompanyInfo { get; set; }

        /// <summary>
        /// CompanyLiquidCurrency
        /// </summary>
        public bool CompanyLiquidCurrency { get; set; }

        /// <summary>
        /// CompanyHeadquarters
        /// </summary>
        public bool CompanyHeadquarters { get; set; }

        /// <summary>
        /// MiscShipmentTracking
        /// </summary>
        public bool MiscShipmentTracking { get; set; }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>Returns an copy of this object</returns>
        public object Clone()
        {
            return new Permission
            {
                PermissionId = PermissionId,
                GrantorUserName = GrantorUserName,
                GranteeUserName = GranteeUserName,
                GroupId = GroupId,
                ShipInformation = ShipInformation,
                ShipRepair = ShipRepair,
                ShipFlight = ShipFlight,
                ShipInventory = ShipInventory,
                ShipFuelInventory = ShipFuelInventory,
                SitesLocation = SitesLocation,
                SitesWorkforces = SitesWorkforces,
                SitesExperts = SitesExperts,
                SitesBuildings = SitesBuildings,
                SitesRepair = SitesRepair,
                SitesReclaimable = SitesReclaimable,
                SitesProductionLines = SitesProductionLines,
                StorageLocation = StorageLocation,
                StorageInformation = StorageInformation,
                StorageItems = StorageItems,
                TradeContract = TradeContract,
                TradeCXOS = TradeCXOS,
                CompanyInfo = CompanyInfo,
                CompanyLiquidCurrency = CompanyLiquidCurrency,
                CompanyHeadquarters = CompanyHeadquarters,
                MiscShipmentTracking = MiscShipmentTracking,
            };
        }

        /// <summary>
        /// Run custom validation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">The context of where this validation is being run</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);
            if (GroupId == 0)
            {
                // Grantor and Grantee cannot be the same
                if (GrantorUserName == GranteeUserName)
                {
                    Errors.Add("Grantee and Grantor are the same");
                }
            }
            else
            {
                // Grantor and Grantee should be the same (name of the group)
                if (GrantorUserName != GranteeUserName)
                {
                    Errors.Add("Invalid Group Permission Setup (Grantor/Grantee)");
                }
            }

            if (GranteeUserName != "*" && GranteeUserName.Length < 3)
            {
                Errors.Add($"{nameof(GranteeUserName)} is fewer than 3 characters");
            }
        }
    }
}
