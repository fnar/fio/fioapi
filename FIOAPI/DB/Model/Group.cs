﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Group Model
    /// </summary>
    [Index(nameof(GroupName))]
    public class Group : IValidation
    {
        /// <summary>
        /// The largest GroupId possible
        /// </summary>
        [NotMapped]
        public const int LargestGroupId = 999999;

        /// <summary>
        /// GroupId
        /// </summary>
        [Key]
        [Range(1, LargestGroupId)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GroupId { get; set; }

        /// <summary>
        /// GroupName
        /// </summary>
        [StringLength(16, MinimumLength = 3)]
        public string GroupName { get; set; } = null!;

        /// <summary>
        /// GroupOwner
        /// </summary>
        [Lowercase]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string GroupOwner { get; set; } = null!;

        /// <summary>
        /// PendingInvites
        /// </summary>
        public virtual List<GroupPendingInvite> PendingInvites { get; set; } = new List<GroupPendingInvite>();

        /// <summary>
        /// Admins
        /// </summary>
        public virtual List<GroupAdmin> Admins { get; set; } = new List<GroupAdmin>();

        /// <summary>
        /// Users
        /// </summary>
        public virtual List<GroupUser> Users { get; set; } = new List<GroupUser>();

        /// <summary>
        /// Retrieves a list of member UserNames
        /// </summary>
        [NotMapped]
        public List<string> MemberUserNames
        {
            get
            {
                return Admins
                    .Select(a => a.UserName)
                    .Concat(Users.Select(u => u.UserName))
                    .Concat(new string[] { GroupOwner })
                    .ToList();
            }
        }
    }
}
