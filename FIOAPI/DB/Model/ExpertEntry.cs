﻿
namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ExpertEntry
    /// </summary>
    public class ExpertEntry : IValidation
    {
        /// <summary>
        /// ExpertEntryId
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(52, MinimumLength = 42)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ExpertEntryId { get; set; } = null!; // {ExpertId}-{ExpertiseCategory}

        /// <summary>
        /// Category
        /// </summary>
        [BuildingCategory]
        [StringLength(Constants.BuildingCategoryLengthMax, MinimumLength = Constants.BuildingCategoryLengthMin)]
        public string Category { get; set; } = null!;

        /// <summary>
        /// Current
        /// </summary>
        [Range(0, 5)]
        public int Current { get; set; }

        /// <summary>
        /// Limit
        /// </summary>
        [Range(0, 5)]
        public int Limit { get; set; }

        /// <summary>
        /// Available
        /// </summary>
        [Range(0, 5)]
        public int Available { get; set; }

        /// <summary>
        /// EfficiencyGain
        /// </summary>
        [Range(0.0, 50.0)]
        public double EfficiencyGain { get; set; }

        /// <summary>
        /// ExpertId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ExpertId { get; set; } = null!;

        /// <summary>
        /// Expert (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual Expert Expert { get; set; } = null!;

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            var ExpertEntryIdSplit = ExpertEntryId.SplitOn("-");
            if (ExpertEntryIdSplit.Count != 2)
            {
                Errors.Add($"{Context}.{nameof(ExpertEntryId)} is not in the correct format.");
            }

            ValidationExtensions.RunValidationsAsIfTypeAndProperty(ExpertEntryIdSplit[0], typeof(Expert), nameof(Expert.ExpertId), ref Errors, Context);
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(ExpertEntryIdSplit[1], typeof(ExpertEntry), nameof(ExpertEntry.Category), ref Errors, Context);
        }
    }
}
