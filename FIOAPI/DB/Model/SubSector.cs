﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// SubSector
    /// </summary>
    public class SubSector : IValidation
    {
        /// <summary>
        /// SubSectorId
        /// </summary>
        [Key]
        [StringLength(20, MinimumLength = 13)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SubSectorId { get; set; } = null!;

        /// <summary>
        /// Vertex0_X
        /// </summary>
        [JsonIgnore]
        public double Vertex0_X { get; set; }

        /// <summary>
        /// Vertex0_Y
        /// </summary>
        [JsonIgnore]
        public double Vertex0_Y { get; set; }

        /// <summary>
        /// Vertex0_Z
        /// </summary>
        [JsonIgnore]
        public double Vertex0_Z { get; set; }

        /// <summary>
        /// Vertex1_X
        /// </summary>
        [JsonIgnore]
        public double Vertex1_X { get; set; }

        /// <summary>
        /// Vertex1_Y
        /// </summary>            
        [JsonIgnore]
        public double Vertex1_Y { get; set; }

        /// <summary>
        /// Vertex1_Z
        /// </summary>           
        [JsonIgnore]
        public double Vertex1_Z { get; set; }

        /// <summary>
        /// Vertex2_X
        /// </summary>
        [JsonIgnore]
        public double Vertex2_X { get; set; }

        /// <summary>
        /// Vertex2_Y
        /// </summary>        
        [JsonIgnore]
        public double Vertex2_Y { get; set; }

        /// <summary>
        /// Vertex2_Z
        /// </summary>       
        [JsonIgnore]
        public double Vertex2_Z { get; set; }

        /// <summary>
        /// Helper to retrieve the vertices
        /// </summary>
        [NotMapped]
        public List<List<double>> Vertices 
        {
            get
            {
                return new()
                {
                    new List<double> { Vertex0_X, Vertex0_Y, Vertex0_Z },
                    new List<double> { Vertex1_X, Vertex1_Y, Vertex1_Z },
                    new List<double> { Vertex2_X, Vertex2_Y, Vertex2_Z }
                };
            }
        }

        /// <summary>
        /// SectorId (parent)
        /// </summary>
        [JsonIgnore]
        [StringLength(10, MinimumLength = 8)]
        public string SectorId { get; set; } = null!;

        /// <summary>
        /// Sector (parent)
        /// </summary>
        [JsonIgnore]
        public virtual Sector Sector { get; set; } = null!;

        /// <summary>
        /// CustomValidate
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!SectorId.StartsWith("sector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'sector-'");
            }

            if (!SubSectorId.StartsWith("subsector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'subsector-'");
            }
        }
    }
}
