﻿using System.Xml.Linq;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// PlanetWorkforceFee
    /// </summary>
    public class PlanetWorkforceFee : IValidation, ICloneable
    {
        /// <summary>
        /// PlanetWorkforceFeeId: {PlanetId}-{ExpertiseCategory}-{WorkforceLevel}
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(64, MinimumLength = 33)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string PlanetWorkforceFeeId { get; set; } = null!;

        /// <summary>
        /// Category - The building/expertise category
        /// </summary>
        [BuildingCategory]
        [StringLength(Constants.BuildingCategoryLengthMax, MinimumLength = Constants.BuildingCategoryLengthMin)]
        public string Category { get; set; } = null!;

        /// <summary>
        /// The tier of worker
        /// </summary>
        [WorkforceLevel]
        [StringLength(Constants.WorkforceLevelLengthMax, MinimumLength = Constants.WorkforceLevelLengthMin)]
        public string WorkforceLevel { get; set; } = null!;

        /// <summary>
        /// Fee
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? Fee { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string? Currency { get; set; } = null!;

        /// <summary>
        /// (Parent) PlanetId
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// (Parent) Planet
        /// </summary>
        [JsonIgnore]
        public virtual Planet Planet { get; set; } = null!;

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new PlanetWorkforceFee()
            {
                PlanetWorkforceFeeId = PlanetWorkforceFeeId,
                Category = Category,
                WorkforceLevel = WorkforceLevel,
                Fee = Fee,
                Currency = Currency,
                PlanetId = PlanetId
            };
        }
    }
}
