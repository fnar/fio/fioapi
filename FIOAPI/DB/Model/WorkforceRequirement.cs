﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// WorkforceRequirement
    /// </summary>
    [Index(nameof(Level))]
    public class WorkforceRequirement : IValidation
    {
        /// <summary>
        /// WorkforceRequirementId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkforceRequirementId { get; set; }

        /// <summary>
        /// The "level" or name of the workforce tier (PIONEER, SETTLER, etc)
        /// </summary>
        [WorkforceLevel]
        [StringLength(Constants.WorkforceLevelLengthMax, MinimumLength = Constants.WorkforceLevelLengthMin)]
        public string Level { get; set; } = null!;

        /// <summary>
        /// The workforce needs for this tier of workers
        /// </summary>
        [NotEmpty]
        public virtual List<WorkforceRequirementNeed> Needs { get; set; } = new List<WorkforceRequirementNeed>();

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }

    /// <summary>
    /// WorkforceRequirementNeed
    /// </summary>
    public class WorkforceRequirementNeed : IValidation
    {
        /// <summary>
        /// WorkforceRequirementNeedId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int WorkforceRequirementNeedId { get; set; }

        /// <summary>
        /// If the need is essential
        /// </summary>
        public bool Essential { get; set; }

        /// <summary>
        /// The name of the material
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string MaterialName { get; set; } = null!;

        /// <summary>
        /// The need's ticker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// The material id
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// The amount consumed per 100 workers
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double UnitsPer100 { get; set; }

        /// <summary>
        /// WorkforceRequirementId
        /// </summary>
        [JsonIgnore]
        public int WorkforceRequirementId { get; set; }

        /// <summary>
        /// WorkforceRequirement
        /// </summary>
        [JsonIgnore]
        public virtual WorkforceRequirement WorkforceRequirement { get; set; } = null!;
    }
}
