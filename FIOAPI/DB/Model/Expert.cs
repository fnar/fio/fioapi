﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Expert
    /// </summary>
    [RequiredPermission(Perm.Sites_Experts)]
    public class Expert : IValidation
    {
        /// <summary>
        /// ExpertId
        /// </summary>
        [Key]
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ExpertId { get; set; } = null!;

        /// <summary>
        /// SiteId
        /// </summary>
        [NotMapped]
        public string SiteId
        {
            get
            {
                return ExpertId;
            }
        }

        /// <summary>
        /// LocationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string LocationId { get; set; } = null!;

        /// <summary>
        /// LocationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string LocationName { get; set; } = null!;

        /// <summary>
        /// LocationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string LocationNaturalId { get; set; } = null!;

        /// <summary>
        /// Total
        /// </summary>
        [Range(0, 50)]
        public int Total { get; set; }

        /// <summary>
        /// Active
        /// </summary>
        [Range(0, 6)]
        public int Active { get; set; }

        /// <summary>
        /// TotalActiveCap
        /// </summary>
        [Range(0, 6)]
        public int TotalActiveCap { get; set; }

        /// <summary>
        /// UserNameSubmitted
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// ExpertEntries
        /// </summary>
        public virtual List<ExpertEntry> ExpertEntries { get; set; } = new();
    }
}
