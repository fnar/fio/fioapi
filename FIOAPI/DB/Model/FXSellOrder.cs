﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// FXSellOrder
    /// </summary>
    [Index(nameof(UserName))]
    [Index(nameof(UserCompanyCode))]
    public class FXSellOrder : IValidation
    {
        /// <summary>
        /// FXSellOrderId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string FXSellOrderId { get; set; } = null!;

        /// <summary>
        /// UserId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string UserId { get; set; } = null!;

        /// <summary>
        /// UserName
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// UserCompanyCode
        /// </summary>
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string UserCompanyCode { get; set; } = null!;

        /// <summary>
        /// FXID
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string FXID { get; set; } = null!;

        /// <summary>
        /// FX
        /// </summary>
        [JsonIgnore]
        public virtual FX FX { get; set; } = null!;
    }
}
