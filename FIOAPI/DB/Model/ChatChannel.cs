﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ChatChannel
    /// </summary>
    public class ChatChannel : IValidation
    {
        /// <summary>
        /// The ChannelId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ChatChannelId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(10, MinimumLength = 5)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// NaturalId
        /// </summary>
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? NaturalId { get; set; } = null;

        /// <summary>
        /// DisplayName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax)]
        public string? DisplayName { get; set; } = null;

        /// <summary>
        /// UserCount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int UserCount { get; set; } = 0;

        /// <summary>
        /// CreationTimestamp
        /// </summary>
        public long CreationTimestamp { get; set; } = 0;

        /// <summary>
        /// LastActivityTimestamp
        /// </summary>
        public long LastActivityTimestamp { get; set; } = 0;

        /// <summary>
        /// Messages
        /// </summary>
        public virtual List<ChatMessage> Messages { get; set; } = new List<ChatMessage>();

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
