﻿using FIOAPI.Caches;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// System
    /// </summary>
    [Index(nameof(Name))]
    [Index(nameof(NaturalId))]
    public class System : IValidation, IAPEXEntity
    {
        /// <summary>
        /// SystemId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SystemId { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? Name { get; set; }

        /// <summary>
        /// Nameable
        /// </summary>
        public bool Nameable { get; set; }

        /// <summary>
        /// NamerId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? NamerId { get; set; }

        /// <summary>
        /// NamerUserName
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string? NamerUserName { get; set; }

        /// <summary>
        /// NaturalId
        /// </summary>
        [NaturalID]
        [CustomStringLength]
        [StringLength(6, MinimumLength = 6)]
        public string NaturalId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// SectorId
        /// </summary>
        [StringLength(10, MinimumLength = 8)]
        public string SectorId { get; set; } = null!;

        /// <summary>
        /// SubSectorId
        /// </summary>
        [StringLength(20, MinimumLength = 13)]
        public string SubSectorId { get; set; } = null!;

        /// <summary>
        /// PositionX
        /// </summary>
        public double PositionX { get; set; }

        /// <summary>
        /// PositionY
        /// </summary>
        public double PositionY { get; set; }

        /// <summary>
        /// PositionZ
        /// </summary>
        public double PositionZ { get; set; }

        /// <summary>
        /// MeteroidDensity
        /// </summary>
        public double? MeteroidDensity { get; set; }

        /// <summary>
        /// A comma-separated list of APEXIDs which represent connections
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string RawConnections { get; set; } = null!;

        /// <summary>
        /// List of connections
        /// </summary>
        [NotMapped]
        public List<string> Connections
        {
            get
            {
                if (_Connections == null)
                {
                    _Connections = RawConnections.SplitOn(",");
                }

                return _Connections;
            }
            set
            {
                RawConnections = String.Join(',', value);
                _Connections = null;
            }
        }
        [NotMapped]
        private List<string>? _Connections;

        /// <summary>
        /// A comma-separated list of APEXIDs which represent PlanetIds
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string? RawPlanetIds { get; set; }

        /// <summary>
        /// List of PlanetIds
        /// </summary>
        [NotMapped]
        public List<string> PlanetIds
        {
            get
            {
                if (_PlanetIds == null)
                {
                    _PlanetIds = (!string.IsNullOrWhiteSpace(RawPlanetIds) ? RawPlanetIds.Split([',']).ToList() : new());
                }

                return _PlanetIds;
            }
            set
            {
                RawPlanetIds = String.Join(',', value);
                _PlanetIds = null;
            }
        }
        [NotMapped]
        private List<string>? _PlanetIds;

        /// <summary>
        /// A comma-separatedlist of APEXIDs which represent celestial bodies (Stations)
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string? RawCelestialBodyIds { get; set; }

        /// <summary>
        /// List of CelestialBodyIds (stations)
        /// </summary>
        [NotMapped]
        public List<string> CelestialBodyIds
        {
            get
            {
                if (_CelestialBodyIds == null)
                {
                    _CelestialBodyIds = (!string.IsNullOrEmpty(RawCelestialBodyIds) ? RawCelestialBodyIds.Split([',']).ToList() : new());
                }

                return _CelestialBodyIds;
            }
            set
            {
                RawCelestialBodyIds = String.Join(',', value);
                _CelestialBodyIds = null;
            }
        }
        [NotMapped]
        private List<string>? _CelestialBodyIds;

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        #region IAPEXEntity
        /// <summary>
        /// EntityId
        /// </summary>
        [NotMapped]
        public string EntityId
        {
            get
            {
                return SystemId;
            }
        }

        /// <summary>
        /// EntityName
        /// </summary>
        [NotMapped]
        public string? EntityName
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// EntityNaturalId
        /// </summary>
        [NotMapped]
        public string EntityFriendlyIdentifier
        {
            get
            {
                return NaturalId;
            }
        }
        #endregion

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!SectorId.StartsWith("sector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'sector-'");
            }

            if (!SubSectorId.StartsWith("subsector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'subsector-'");
            }

            if (Connections.Count == 0)
            {
                Errors.Add($"'{Context}' {nameof(Connections)} count is 0");
            }

            foreach (var Connection in Connections)
            {
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(Connection, typeof(System), nameof(SystemId), ref Errors, Context);
            }

            foreach (var PlanetId in PlanetIds)
            {
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(PlanetId, typeof(Planet), nameof(Planet.PlanetId), ref Errors, Context);
            }

            foreach (var CelestialBodyId in CelestialBodyIds)
            {
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(CelestialBodyId, typeof(Station), nameof(Station.StationId), ref Errors, Context);
            }
        }

        /// <summary>
        /// Clones the object
        /// </summary>
        /// <returns>A copy of the object</returns>
        public object Clone()
        {
            return new System()
            {
                SystemId = SystemId,
                Name = Name,
                Nameable = Nameable,
                NamerId = NamerId,
                NamerUserName = NamerUserName,
                NaturalId = NaturalId,
                Type = Type,
                SectorId = SectorId,
                SubSectorId = SubSectorId,
                PositionX = PositionX,
                PositionY = PositionY,
                PositionZ = PositionZ,
                MeteroidDensity = MeteroidDensity,
                RawConnections = RawConnections,
                RawPlanetIds = RawPlanetIds,
                RawCelestialBodyIds = RawCelestialBodyIds,
                UserNameSubmitted = UserNameSubmitted,
                Timestamp = Timestamp,
            };
        }
    }
}
