﻿using System.Xml.Linq;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// PlanetPopulationReport
    /// </summary>
    public class PlanetPopulationReport : IValidation, ICloneable
    {
        /// <summary>
        /// PlanetPopulationReportId: {PlanetId}-{SimulationPeriod}
        /// </summary>
        [Key]
        [StringLength(40)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string PlanetPopulationReportId { get; set; } = null!;

        /// <summary>
        /// SimulationPeriod
        /// </summary>
        public int SimulationPeriod { get; set; }

        /// <summary>
        /// ExplorersGraceEnabled
        /// </summary>
        public bool ExplorersGraceEnabled { get; set; }

        /// <summary>
        /// Report
        /// </summary>
        public DateTime ReportTimestamp { get; set; }

        /// <summary>
        /// NextPopulationPioneer
        /// </summary>
        public int NextPopulationPioneer { get; set; }

        /// <summary>
        /// NextPopulationSettler
        /// </summary>
        public int NextPopulationSettler { get; set; }

        /// <summary>
        /// NextPopulationTechnician
        /// </summary>
        public int NextPopulationTechnician { get; set; }

        /// <summary>
        /// NextPopulationEngineer
        /// </summary>
        public int NextPopulationEngineer { get; set; }

        /// <summary>
        /// NextPopulationScientist
        /// </summary>
        public int NextPopulationScientist { get; set; }

        /// <summary>
        /// PopulationDifferencePioneer
        /// </summary>
        public int PopulationDifferencePioneer { get; set; }

        /// <summary>
        /// PopulationDifferenceSettler
        /// </summary>
        public int PopulationDifferenceSettler { get; set; }

        /// <summary>
        /// PopulationDifferenceTechnician
        /// </summary>
        public int PopulationDifferenceTechnician { get; set; }

        /// <summary>
        /// PopulationDifferenceEngineer
        /// </summary>
        public int PopulationDifferenceEngineer { get; set; }

        /// <summary>
        /// PopulationDifferenceScientist
        /// </summary>
        public int PopulationDifferenceScientist { get; set; }

        /// <summary>
        /// AverageHappinessPioneer
        /// </summary>
        public double AverageHappinessPioneer { get; set; }

        /// <summary>
        /// AverageHappinessSettler
        /// </summary>
        public double AverageHappinessSettler { get; set; }

        /// <summary>
        /// AverageHappinessTechnician
        /// </summary>
        public double AverageHappinessTechnician { get; set; }

        /// <summary>
        /// AverageHappinessEngineer
        /// </summary>
        public double AverageHappinessEngineer { get; set; }

        /// <summary>
        /// AverageHappinessScientist
        /// </summary>
        public double AverageHappinessScientist { get; set; }

        /// <summary>
        /// UnemploymentRatePioneer
        /// </summary>
        public double UnemploymentRatePioneer { get; set; }

        /// <summary>
        /// UnemploymentRateSettler
        /// </summary>
        public double UnemploymentRateSettler { get; set; }

        /// <summary>
        /// UnemploymentRateTechnician
        /// </summary>
        public double UnemploymentRateTechnician { get; set; }

        /// <summary>
        /// UnemploymentRateEngineer
        /// </summary>
        public double UnemploymentRateEngineer { get; set; }

        /// <summary>
        /// UnemploymentRateScientist
        /// </summary>
        public double UnemploymentRateScientist { get; set; }

        /// <summary>
        /// OpenJobsPioneer
        /// </summary>
        public double OpenJobsPioneer { get; set; }

        /// <summary>
        /// OpenJobsSettler
        /// </summary>
        public double OpenJobsSettler { get; set; }

        /// <summary>
        /// OpenJobsTechnician
        /// </summary>
        public double OpenJobsTechnician { get; set; }

        /// <summary>
        /// OpenJobsEngineer
        /// </summary>
        public double OpenJobsEngineer { get; set; }

        /// <summary>
        /// OpenJobsScientist
        /// </summary>
        public double OpenJobsScientist { get; set; }

        /// <summary>
        /// NeedFulfillmentLifeSupport
        /// </summary>
        public double NeedFulfillmentLifeSupport { get; set; }

        /// <summary>
        /// NeedFulfillmentSafety
        /// </summary>
        public double NeedFulfillmentSafety { get; set; }

        /// <summary>
        /// NeedFulfillmentHealth
        /// </summary>
        public double NeedFulfillmentHealth { get; set; }

        /// <summary>
        /// NeedFulfillmentComfort
        /// </summary>
        public double NeedFulfillmentComfort { get; set; }

        /// <summary>
        /// NeedFulfillmentCulture
        /// </summary>
        public double NeedFulfillmentCulture { get; set; }

        /// <summary>
        /// NeedFulfillmentEducation
        /// </summary>
        public double NeedFulfillmentEducation { get; set; }

        /// <summary>
        /// PlanetId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// Planet (Parent)
        /// </summary>
        [JsonIgnore]
        [NoValidate]
        public Planet Planet { get; set; } = null!;

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new PlanetPopulationReport()
            {
                PlanetPopulationReportId = PlanetPopulationReportId,
                SimulationPeriod = SimulationPeriod,
                ExplorersGraceEnabled = ExplorersGraceEnabled,
                ReportTimestamp = ReportTimestamp,

                NextPopulationPioneer = NextPopulationPioneer,
                NextPopulationSettler = NextPopulationSettler,
                NextPopulationTechnician = NextPopulationTechnician,
                NextPopulationEngineer = NextPopulationEngineer,
                NextPopulationScientist = NextPopulationScientist,

                PopulationDifferencePioneer = PopulationDifferencePioneer,
                PopulationDifferenceSettler = PopulationDifferenceSettler,
                PopulationDifferenceTechnician = PopulationDifferenceTechnician,
                PopulationDifferenceEngineer = PopulationDifferenceEngineer,
                PopulationDifferenceScientist = PopulationDifferenceScientist,

                AverageHappinessPioneer = AverageHappinessPioneer,
                AverageHappinessSettler = AverageHappinessSettler,
                AverageHappinessTechnician = AverageHappinessTechnician,
                AverageHappinessEngineer = AverageHappinessEngineer,
                AverageHappinessScientist = AverageHappinessScientist,

                UnemploymentRatePioneer = UnemploymentRatePioneer,
                UnemploymentRateSettler = UnemploymentRateSettler,
                UnemploymentRateTechnician = UnemploymentRateTechnician,
                UnemploymentRateEngineer = UnemploymentRateEngineer,
                UnemploymentRateScientist = UnemploymentRateScientist,

                OpenJobsPioneer = OpenJobsPioneer,
                OpenJobsSettler = OpenJobsSettler,
                OpenJobsTechnician = OpenJobsTechnician,
                OpenJobsEngineer = OpenJobsEngineer,
                OpenJobsScientist = OpenJobsScientist,

                NeedFulfillmentLifeSupport = NeedFulfillmentLifeSupport,
                NeedFulfillmentSafety = NeedFulfillmentSafety,
                NeedFulfillmentHealth = NeedFulfillmentHealth,
                NeedFulfillmentComfort = NeedFulfillmentComfort,
                NeedFulfillmentCulture = NeedFulfillmentCulture,
                NeedFulfillmentEducation = NeedFulfillmentEducation,
            };
        }
    }
}
