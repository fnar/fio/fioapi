﻿using System.Xml.Linq;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// BuidlingRecipe
    /// </summary>
    public class BuildingRecipe : IValidation, ICloneable
    {
        /// <summary>
        /// BuildingRecipeId
        /// </summary>
        [Key]
        [StringLength(100)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingRecipeId { get; set; } = null!;

        /// <summary>
        /// Duration of the recipe in milliseconds
        /// </summary>
        public long DurationMs { get; set; } = 0;

        /// <summary>
        /// Inputs for a recipe
        /// </summary>
        public virtual List<BuildingRecipeInput> Inputs { get; set; } = new List<BuildingRecipeInput>();

        /// <summary>
        /// Outputs for a recipe
        /// </summary>
        public virtual List<BuildingRecipeOutput> Outputs { get; set; } = new List<BuildingRecipeOutput>();

        /// <summary>
        /// BuildingId
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string BuildingId { get; set; } = null!;

        /// <summary>
        /// Building
        /// </summary>
        [JsonIgnore]
        public virtual Building Building { get; set; } = null!;

        /// <summary>
        /// Retrives the recipe
        /// </summary>
        /// <returns>The standardized RecipeName</returns>
        public string GetRecipeId()
        {
            if (Building == null) throw new ArgumentNullException(nameof(Building));

            var inputs = Inputs.OrderBy(i => i.Ticker).Select(i => $"{i.Amount}x{i.Ticker}");
            var outputs = Outputs.OrderBy(o => o.Ticker).Select(o => $"{o.Amount}x{o.Ticker}");
            return $"{Building.Ticker}:{String.Join("-", inputs)}=>{String.Join("-", outputs)}";
        }

        /// <summary>
        /// Generates the recipe name
        /// </summary>
        public void GenerateRecipeId()
        {
            BuildingRecipeId = GetRecipeId();
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            var copy = new BuildingRecipe();
            copy.BuildingRecipeId = BuildingRecipeId;
            copy.DurationMs = DurationMs;

            Inputs.ForEach(i =>
            {
                var inputCopy = (BuildingRecipeInput)i.Clone();
                inputCopy.BuildingRecipe = copy;
                copy.Inputs.Add(inputCopy);

            });

            Outputs.ForEach(o =>
            {
                var outputCopy = (BuildingRecipeOutput)o.Clone();
                outputCopy.BuildingRecipe = copy;
                copy.Outputs.Add(outputCopy);

            });

            copy.BuildingId = BuildingId;

            return copy;
        }

    }
}
