﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// APEXUser
    /// </summary>
    [Index(nameof(CompanyCode))]
    [Index(nameof(UserName))]
    public class APEXUser : IValidation
    {
        /// <summary>
        /// APEXUserId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string APEXUserId { get; set; } = null!;

        /// <summary>
        /// UserName
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string? UserName { get; set; } = null!;

        /// <summary>
        /// SubscriptionLevel
        /// </summary>
        [StringLength(5, MinimumLength = 3)]
        public string? SubscriptionLevel { get; set; } = null!;

        /// <summary>
        /// HighestTier
        /// </summary>
        [StringLength(32)]
        public string? HighestTier { get; set; }

        /// <summary>
        /// CompanyId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? CompanyId { get; set; } = null!;

        /// <summary>
        /// CompanyName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? CompanyName { get; set; } = null!;

        /// <summary>
        /// CompanyCode
        /// </summary>
        [Uppercase]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string? CompanyCode { get; set; }

        /// <summary>
        /// Liquidated
        /// </summary>
        [NotMapped]
        public bool? Liquidated
        {
            get
            {
                if (CompanyId == null)
                    return null;

                return CompanyCode == null;
            }
        }

        /// <summary>
        /// CreatedTimestamp
        /// </summary>
        public long? CreatedTimestamp { get; set; }

        /// <summary>
        /// Created
        /// </summary>
        [NotMapped]
        public DateTime? Created
        {
            get
            {
                return CreatedTimestamp?.FromUnixTime();
            }
        }

        /// <summary>
        /// Team
        /// </summary>
        public bool Team { get; set; }

        /// <summary>
        /// Moderator
        /// </summary>
        public bool Moderator { get; set; }

        /// <summary>
        /// Pioneer
        /// </summary>
        public bool Pioneer { get; set; }

        /// <summary>
        /// ActiveDaysPerWeek
        /// </summary>
        [Range(0, int.MaxValue)]
        public int? ActiveDaysPerWeek { get; set; }

        /// <summary>
        /// LastOnlineTimestamp
        /// </summary>
        public long? LastOnlineTimestamp { get; set; }

        /// <summary>
        /// When the user was last online
        /// </summary>
        [NotMapped]
        public DateTime? LastOnline
        {
            get
            {
                return LastOnlineTimestamp?.FromUnixTime();
            }
        }
    }
}
