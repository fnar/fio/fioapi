﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Company
    /// </summary>
    [Index(nameof(Code))]   // @TODO: This should be unique, but...old companies
    public class Company : IValidation, IAPEXEntity
    {
        /// <summary>
        /// CompanyId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CompanyId { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Code
        /// </summary>
        [Uppercase]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string Code { get; set; } = null!;

        /// <summary>
        /// Founded
        /// </summary>
        public DateTime Founded { get; set; }

        /// <summary>
        /// UserName
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// CountryId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CountryId { get; set; } = null!;

        /// <summary>
        /// CountryCode
        /// </summary>
        [Uppercase]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string CountryCode { get; set; } = null!;

        /// <summary>
        /// CountryName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string CountryName { get; set; } = null!;

        /// <summary>
        /// CorporationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? CorporationId { get; set; } = null!;

        /// <summary>
        /// CorporationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? CorporationName { get; set; } = null!;

        /// <summary>
        /// CorporationCode
        /// </summary>
        [Uppercase]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string? CorporationCode { get; set; } = null!;

        /// <summary>
        /// CurrentAPEXRepresentationLevel
        /// </summary>
        public int CurrentAPEXRepresentationLevel { get; set; }

        /// <summary>
        /// OverallRating
        /// </summary>
        [StringLength(Constants.RatingLengthMax, MinimumLength = Constants.RatingLengthMin)]
        public string OverallRating { get; set; } = null!;

        /// <summary>
        /// RatingsEarliestContract
        /// </summary>
        public DateTime? RatingsEarliestContract { get; set; }

        /// <summary>
        /// RatingContractCount
        /// </summary>
        [Range(0, 1000)]
        public int RatingContractCount { get; set; }

        /// <summary>
        /// ReputationEntityName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? ReputationEntityName { get; set; } = null!;

        /// <summary>
        /// Reputation
        /// </summary>
        [Range(-1000, 10000)]
        public int Reputation { get; set; }

        /// <summary>
        /// Planets
        /// </summary>
        public virtual List<CompanyPlanet> Planets { get; set; } = new();

        /// <summary>
        /// Offices
        /// </summary>
        public virtual List<CompanyOffice> Offices { get; set; } = new();

        /// <summary>
        /// StartingProfile
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        [RequiredPermission(Perm.Company_Info)]
        public string? StartingProfile { get; set; } = null;

        /// <summary>
        /// StartingLocationId
        /// </summary>
        [APEXID]
        [RequiredPermission(Perm.Company_Info)]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? StartingLocationId { get; set; }

        /// <summary>
        /// StartingLocationNaturalId
        /// </summary>
        [NaturalID]
        [RequiredPermission(Perm.Company_Info)]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? StartingLocationNaturalId { get; set; }

        /// <summary>
        /// StartingLocationName
        /// </summary>
        [RequiredPermission(Perm.Company_Info)]
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? StartingLocationName { get; set; }

        /// <summary>
        /// HeadquartersLocationId
        /// </summary>
        [APEXID]
        [RequiredPermission(Perm.Company_Headquarters)]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? HeadquartersLocationId { get; set; }

        /// <summary>
        /// HeadquartersLocationNaturalId
        /// </summary>
        [NaturalID]
        [RequiredPermission(Perm.Company_Headquarters)]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? HeadquartersLocationNaturalId { get; set; }

        /// <summary>
        /// HeadquartersLocationName
        /// </summary>
        [RequiredPermission(Perm.Company_Headquarters)]
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? HeadquartersLocationName { get; set; }

        /// <summary>
        /// HeadquartersLevel
        /// </summary>
        [PositiveNumber]
        [RequiredPermission(Perm.Company_Headquarters)]
        public int? HeadquartersLevel { get; set; }

        /// <summary>
        /// TotalBasePermits
        /// </summary>
        [PositiveNumber]
        [RequiredPermission(Perm.Company_Headquarters)]
        public int? TotalBasePermits { get; set; }

        /// <summary>
        /// UsedBasePermits
        /// </summary>
        [PositiveNumber]
        [RequiredPermission(Perm.Company_Headquarters)]
        public int? UsedBasePermits { get; set; }

        /// <summary>
        /// AdditionalBasePermits
        /// </summary>
        [Range(0, int.MaxValue)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public int? AdditionalBasePermits { get; set; }

        /// <summary>
        /// AdditionalProductionQueueSlots
        /// </summary>
        [Range(0, 20)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public int? AdditionalProductionQueueSlots { get; set; }

        /// <summary>
        /// HeadquartersNextRelocationTime
        /// </summary>
        [ValidTimestamp]
        [RequiredPermission(Perm.Company_Headquarters)]
        public DateTime? HeadquartersNextRelocationTime { get; set; }

        /// <summary>
        /// HeadquartersRelocationLocked
        /// </summary>
        [RequiredPermission(Perm.Company_Headquarters)]
        public bool? HeadquartersRelocationLocked { get; set; }

        /// <summary>
        /// HeadquartersAgricultureEfficiencyGain
        /// </summary>
        [Range(0.0, 100.0)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public double? HeadquartersAgricultureEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersChemistryEfficiencyGain
        /// </summary>
        [Range(0.0, 100.0)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public double? HeadquartersChemistryEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersConstructionEfficiencyGain
        /// </summary>
        [Range(0.0, 100.0)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public double? HeadquartersConstructionEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersElectronicsEfficiencyGain
        /// </summary>
        [Range(0.0, 100.0)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public double? HeadquartersElectronicsEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersFoodIndustriesEfficiencyGain
        /// </summary>
        [Range(0.0, 100.0)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public double? HeadquartersFoodIndustriesEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersFuelRefiningEfficiencyGain
        /// </summary>
        [Range(0.0, 100.0)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public double? HeadquartersFuelRefiningEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersManufacturingEfficiencyGain
        /// </summary>
        [Range(0.0, 100.0)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public double? HeadquartersManufacturingEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersMetallurgyEfficiencyGain
        /// </summary>
        [Range(0.0, 100.0)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public double? HeadquartersMetallurgyEfficiencyGain { get; set; }

        /// <summary>
        /// HeadquartersResourceExtractionEfficiencyGain
        /// </summary>
        [Range(0.0, 100.0)]
        [RequiredPermission(Perm.Company_Headquarters)]
        public double? HeadquartersResourceExtractionEfficiencyGain { get; set; }

        /// <summary>
        /// AICLiquid
        /// </summary>
        [Range(0.0, double.MaxValue)]
        [RequiredPermission(Perm.Company_LiquidCurrency)]
        public double? AICLiquid { get; set; }

        /// <summary>
        /// CISLiquid
        /// </summary>
        [Range(0.0, double.MaxValue)]
        [RequiredPermission(Perm.Company_LiquidCurrency)]
        public double? CISLiquid { get; set; }

        /// <summary>
        /// ECDLiquid
        /// </summary>
        [Range(0.0, double.MaxValue)]
        [RequiredPermission(Perm.Company_LiquidCurrency)]
        public double? ECDLiquid { get; set; }

        /// <summary>
        /// ICALiquid
        /// </summary>
        [Range(0.0, double.MaxValue)]
        [RequiredPermission(Perm.Company_LiquidCurrency)]
        public double? ICALiquid { get; set; }

        /// <summary>
        /// NCCLiquid
        /// </summary>
        [Range(0.0, double.MaxValue)]
        [RequiredPermission(Perm.Company_LiquidCurrency)]
        public double? NCCLiquid { get; set; }

        /// <summary>
        /// RepresentationCurrentLevel
        /// </summary>
        [Range(0, int.MaxValue)]
        [RequiredPermission(Perm.Company_Info)]
        public int? RepresentationCurrentLevel { get; set; }

        /// <summary>
        /// RepresentationCostNextLevel
        /// </summary>
        [Range(0.0, double.MaxValue)]
        [RequiredPermission(Perm.Company_Info)]
        public double? RepresentationCostNextLevel { get; set; }

        /// <summary>
        /// RepresentationContributedNextLevel
        /// </summary>
        [Range(0.0, double.MaxValue)]
        [RequiredPermission(Perm.Company_Info)]
        public double? RepresentationContributedNextLevel { get; set; }

        /// <summary>
        /// RepresentationLeftNextLevel
        /// </summary>
        [Range(0.0, double.MaxValue)]
        [RequiredPermission(Perm.Company_Info)]
        public double? RepresentationLeftNextLevel { get; set; }

        /// <summary>
        /// RepresentationContributedTotal
        /// </summary>
        [Range(0.0, double.MaxValue)]
        [RequiredPermission(Perm.Company_Info)]
        public double? RepresentationContributedTotal { get; set; }

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        #region IAPEXEntity interface
        /// <summary>
        /// EntityId
        /// </summary>
        [NotMapped]
        public string EntityId => CompanyId;

        /// <summary>
        /// EntityName
        /// </summary>
        [NotMapped]
        public string? EntityName => Name;

        /// <summary>
        /// EntityFriendlyIdentifier
        /// </summary>
        [NotMapped]
        public string EntityFriendlyIdentifier => Code;

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy of this object</returns>
        public object Clone()
        {
            Company c = new()
            {
                CompanyId = CompanyId,
                Name = Name,
                Code = Code,
                Founded = Founded,
                UserName = UserName,
                CountryId = CountryId,
                CountryCode = CountryCode,
                CountryName = CountryName,
                CorporationId = CorporationId,
                CorporationName = CorporationName,
                CorporationCode = CorporationCode,
                CurrentAPEXRepresentationLevel = CurrentAPEXRepresentationLevel,
                OverallRating = OverallRating,
                RatingsEarliestContract = RatingsEarliestContract,
                RatingContractCount = RatingContractCount,
                ReputationEntityName = ReputationEntityName,
                Reputation = Reputation,
                StartingProfile = StartingProfile,
                StartingLocationId = StartingLocationId,
                StartingLocationNaturalId = StartingLocationNaturalId,
                StartingLocationName = StartingLocationName,
                HeadquartersLocationId = HeadquartersLocationId,
                HeadquartersLocationNaturalId = HeadquartersLocationNaturalId,
                HeadquartersLocationName = HeadquartersLocationName,
                HeadquartersLevel = HeadquartersLevel,
                TotalBasePermits = TotalBasePermits,
                UsedBasePermits = UsedBasePermits,
                AdditionalBasePermits = AdditionalBasePermits,
                AdditionalProductionQueueSlots = AdditionalProductionQueueSlots,
                HeadquartersNextRelocationTime = HeadquartersNextRelocationTime,
                HeadquartersRelocationLocked = HeadquartersRelocationLocked,
                HeadquartersAgricultureEfficiencyGain = HeadquartersAgricultureEfficiencyGain,
                HeadquartersChemistryEfficiencyGain = HeadquartersChemistryEfficiencyGain,
                HeadquartersConstructionEfficiencyGain = HeadquartersConstructionEfficiencyGain,
                HeadquartersElectronicsEfficiencyGain = HeadquartersElectronicsEfficiencyGain,
                HeadquartersFoodIndustriesEfficiencyGain = HeadquartersFoodIndustriesEfficiencyGain,
                HeadquartersFuelRefiningEfficiencyGain = HeadquartersFuelRefiningEfficiencyGain,
                HeadquartersManufacturingEfficiencyGain = HeadquartersManufacturingEfficiencyGain,
                HeadquartersMetallurgyEfficiencyGain = HeadquartersMetallurgyEfficiencyGain,
                HeadquartersResourceExtractionEfficiencyGain = HeadquartersResourceExtractionEfficiencyGain,
                AICLiquid = AICLiquid,
                CISLiquid = CISLiquid,
                ECDLiquid = ECDLiquid,
                ICALiquid = ICALiquid,
                NCCLiquid = NCCLiquid,
                RepresentationCurrentLevel = RepresentationCurrentLevel,
                RepresentationCostNextLevel = RepresentationCostNextLevel,
                RepresentationContributedNextLevel = RepresentationContributedNextLevel,
                RepresentationLeftNextLevel = RepresentationLeftNextLevel,
                RepresentationContributedTotal = RepresentationContributedTotal,
                UserNameSubmitted = UserNameSubmitted,
                Timestamp = Timestamp,
            };

            c.Planets = new();
            Planets.ForEach(p =>
            {
                var cp = (CompanyPlanet)p.Clone();
                cp.Company = c;
                c.Planets.Add(cp);
            });

            c.Offices = new();
            Offices.ForEach(o =>
            {
                var co = (CompanyOffice)o.Clone();
                co.Company = c;
                c.Offices.Add(co);
            });

            return c;
        }
        #endregion
    }
}