﻿using FIOAPI.Caches;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Material
    /// </summary>
    [Index(nameof(Ticker))]
    [Index(nameof(MaterialCategoryId))]
    public class Material : IValidation, IAPEXEntity
    {
        /// <summary>
        /// The APEX MaterialId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// Name of the material
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string Name { get; set;} = null!;

        /// <summary>
        /// The material's ticker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// The material's category id
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string MaterialCategoryId { get; set; } = null!;

        /// <summary>
        /// Weight of the material in tons
        /// </summary>
        [Range(0.000001, double.MaxValue)]
        public double Weight { get; set; }

        /// <summary>
        /// Volume of the material in m^3
        /// </summary>
        [Range(0.000001, double.MaxValue)]
        public double Volume { get; set; }

        /// <summary>
        /// If it's a resource extractable from a planet
        /// </summary>
        public bool IsResource { get; set; }

        /// <summary>
        /// The resource type of the material
        /// </summary>
        [StringLength(32)]
        public string? ResourceType { get; set; } = null;

        /// <summary>
        /// If this material is used in infrastructure
        /// </summary>
        public bool InfrastructureUsage { get; set; }

        /// <summary>
        /// If this material is used in COGC
        /// </summary>
        public bool COGCUsage { get; set; }

        /// <summary>
        /// If this material is used for workforces
        /// </summary>
        public bool WorkforceUsage { get; set; }

        /// <summary>
        /// The username that submitted the data
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// The timestamp of the data
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        #region IAPEXEntity Interface
        /// <summary>
        /// EntityId
        /// </summary>
        [NotMapped]
        public string EntityId
        {
            get
            {
                return MaterialId;
            }
        }

        /// <summary>
        /// EntityName
        /// </summary>
        [NotMapped]
        public string EntityName
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// EntityFriendlyIdentifier
        /// </summary>
        [NotMapped]
        public string EntityFriendlyIdentifier
        {
            get
            {
                return Ticker;
            }
        }
        #endregion

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy of this object</returns>
        public object Clone()
        {
            return new Material()
            {
                MaterialId = MaterialId,
                Name = Name,
                Ticker = Ticker,
                MaterialCategoryId = MaterialCategoryId,
                Weight = Weight,
                Volume = Volume,
                IsResource = IsResource,
                ResourceType = ResourceType,
                InfrastructureUsage = InfrastructureUsage,
                COGCUsage = COGCUsage,
                WorkforceUsage = WorkforceUsage,
                UserNameSubmitted = UserNameSubmitted,
                Timestamp = Timestamp
            };
        }
    }
}
