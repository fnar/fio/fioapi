﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Flight
    /// </summary>
    [RequiredPermission(Perm.Ship_Flight)]
    public class Flight : IValidation
    {
        /// <summary>
        /// FlightId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string FlightId { get; set; } = null!;

        /// <summary>
        /// ShipId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ShipId { get; set; } = null!;

        /// <summary>
        /// OriginId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? OriginId { get; set; }

        /// <summary>
        /// OriginName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? OriginName { get; set; }

        /// <summary>
        /// OriginNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? OriginNaturalId { get; set; }

        /// <summary>
        /// DestinationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? DestinationId { get; set; }

        /// <summary>
        /// DestinationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? DestinationName { get; set; }

        /// <summary>
        /// DestinationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? DestinationNaturalId { get; set; }

        /// <summary>
        /// Departure
        /// </summary>
        [ValidTimestamp]
        public DateTime Departure { get; set; }

        /// <summary>
        /// Arrival
        /// </summary>
        [ValidTimestamp]
        public DateTime Arrival { get; set; }

        /// <summary>
        /// CurrentSegmentIndex
        /// </summary>
        [Range(0, int.MaxValue)]
        public int CurrentSegmentIndex { get; set; }

        /// <summary>
        /// STLDistance
        /// </summary>
        public double STLDistance { get; set; }

        /// <summary>
        /// FTLDistance
        /// </summary>
        public double FTLDistance { get; set; }

        /// <summary>
        /// Aborted
        /// </summary>
        public bool Aborted { get; set; }

        /// <summary>
        /// FlightSegments
        /// </summary>
        public virtual List<FlightSegment> FlightSegments { get; set; } = new();

        /// <summary>
        /// The username that submitted the data
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// The timestamp of the data
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
