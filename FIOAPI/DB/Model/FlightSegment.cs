﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// FlightSegment
    /// </summary>
    public class FlightSegment : IValidation
    {
        /// <summary>
        /// FlightSegmentId: {FlightId}-{Index}
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(36, MinimumLength = 34)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string FlightSegmentId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(32)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Origin
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? OriginId { get; set; } = null!;

        /// <summary>
        /// OriginNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? OriginNaturalId { get; set; } = null!;

        /// <summary>
        /// OriginName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? OriginName { get; set; } = null!;

        /// <summary>
        /// OriginSemiMajorAxis
        /// </summary>
        public double? OriginSemiMajorAxis { get; set; }

        /// <summary>
        /// OriginEccentricity
        /// </summary>
        public double? OriginEccentricity { get; set; }

        /// <summary>
        /// OriginInclination
        /// </summary>
        public double? OriginInclination { get; set; }

        /// <summary>
        /// OriginRightAscension
        /// </summary>
        public double? OriginRightAscension { get; set; }

        /// <summary>
        /// OriginPeriapsis
        /// </summary>
        public double? OriginPeriapsis { get; set; }

        /// <summary>
        /// Departure
        /// </summary>
        [ValidTimestamp]
        public DateTime Departure { get; set; }

        /// <summary>
        /// Arrival
        /// </summary>
        [ValidTimestamp]
        public DateTime Arrival { get; set; }

        /// <summary>
        /// DestinationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? DestinationId { get; set; } = null!;

        /// <summary>
        /// DestinationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? DestinationNaturalId { get; set; } = null!;

        /// <summary>
        /// DestinationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? DestinationName { get; set; } = null!;

        /// <summary>
        /// DestinationSemiMajorAxis
        /// </summary>
        public double? DestinationSemiMajorAxis { get; set; }

        /// <summary>
        /// DestinationEccentricity
        /// </summary>
        public double? DestinationEccentricity { get; set; }

        /// <summary>
        /// DestinationInclination
        /// </summary>
        public double? DestinationInclination { get; set; }

        /// <summary>
        /// DestinationRightAscension
        /// </summary>
        public double? DestinationRightAscension { get; set; }

        /// <summary>
        /// DestinationPeriapsis
        /// </summary>
        public double? DestinationPeriapsis { get; set; }

        /// <summary>
        /// TransferEllipseStartPositionX
        /// </summary>
        public double? TransferEllipseStartPositionX { get; set; }

        /// <summary>
        /// TransferEllipseStartPositionY
        /// </summary>
        public double? TransferEllipseStartPositionY { get; set; }

        /// <summary>
        /// TransferEllipseStartPositionZ
        /// </summary>
        public double? TransferEllipseStartPositionZ { get; set; }

        /// <summary>
        /// TransferEllipseTargetPositionX
        /// </summary>
        public double? TransferEllipseTargetPositionX { get; set; }

        /// <summary>
        /// TransferEllipseTargetPositionY
        /// </summary>
        public double? TransferEllipseTargetPositionY { get; set; }

        /// <summary>
        /// TransferEllipseTargetPositionZ
        /// </summary>
        public double? TransferEllipseTargetPositionZ { get; set; }

        /// <summary>
        /// TransferEllipseCenterPositionX
        /// </summary>
        public double? TransferEllipseCenterPositionX { get; set; }

        /// <summary>
        /// TransferEllipseCenterPositionY
        /// </summary>
        public double? TransferEllipseCenterPositionY { get; set; }

        /// <summary>
        /// TransferEllipseCenterPositionZ
        /// </summary>
        public double? TransferEllipseCenterPositionZ { get; set; }

        /// <summary>
        /// TransferEllipseAlpha
        /// </summary>
        public double? TransferEllipseAlpha { get; set; }

        /// <summary>
        /// TransferEllipseSemiMajorAxis
        /// </summary>
        public double? TransferEllipseSemiMajorAxis { get; set; }

        /// <summary>
        /// TransferEllipseSemiMinorAxis
        /// </summary>
        public double? TransferEllipseSemiMinorAxis { get; set; }

        /// <summary>
        /// STLDistance
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? STLDistance { get; set; }

        /// <summary>
        /// FTLDistance
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? FTLDistance { get; set; }

        /// <summary>
        /// STLFuelConsumption
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? STLFuelConsumption { get; set; }

        /// <summary>
        /// FTLFuelConsumption
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? FTLFuelConsumption { get; set; }

        /// <summary>
        /// Damage
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? Damage { get; set; }

        /// <summary>
        /// FlightId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string FlightId { get; set; } = null!;

        /// <summary>
        /// Flight (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual Flight Flight { get; set; } = null!;
    }
}
