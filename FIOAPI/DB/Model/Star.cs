﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Star
    /// </summary>
    [Index(nameof(SystemId))]
    [Index(nameof(SystemNaturalId))]
    [Index(nameof(SystemName))]
    public class Star : IValidation
    {
        /// <summary>
        /// StarId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string StarId { get; set; } = null!;

        /// <summary>
        /// SystemId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string SystemId { get; set; } = null!;

        /// <summary>
        /// SystemNaturalId
        /// </summary>
        [NaturalID]
        [CustomStringLength]
        [StringLength(6, MinimumLength = 6)]
        public string SystemNaturalId { get; set; } = null!;

        /// <summary>
        /// SystemName
        /// </summary>
        [StringLength(64)]
        public string? SystemName { get; set; }

        /// <summary>
        /// SectorId
        /// </summary>
        [StringLength(10, MinimumLength = 8)]
        public string SectorId { get; set; } = null!;

        /// <summary>
        /// SubSectorId
        /// </summary>
        [StringLength(20, MinimumLength = 13)]
        public string SubSectorId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(1, MinimumLength = 1)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Luminosity
        /// </summary>
        public double Luminosity { get; set; }

        /// <summary>
        /// Mass
        /// </summary>
        public double Mass { get; set; }

        /// <summary>
        /// MassSol
        /// </summary>
        public double MassSol { get; set; }

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!SectorId.StartsWith("sector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'sector-'");
            }

            if (!SubSectorId.StartsWith("subsector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'subsector-'");
            }
        }
    }
}
