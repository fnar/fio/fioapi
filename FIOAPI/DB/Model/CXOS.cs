﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// CXOS
    /// </summary>
    [Index(nameof(ExchangeCode))]
    [Index(nameof(MaterialTicker))]
    [RequiredPermission(Perm.Trade_CXOS)]
    public class CXOS : IValidation
    {
        /// <summary>
        /// CXOSId
        /// </summary>
        [Key]
        [APEXID]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CXOSId { get; set; } = null!;

        /// <summary>
        /// OrderId
        /// </summary>
        [NotMapped]
        public string OrderId
        {
            get
            {
                return CXOSId;
            }
        }

        /// <summary>
        /// ExchangeCode
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        public string ExchangeCode { get; set; } = null!;

        /// <summary>
        /// BrokerId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string BrokerId { get; set; } = null!;

        /// <summary>
        /// OrderType
        /// </summary>
        [StringLength(7, MinimumLength = 6)]
        public string OrderType { get; set; } = null!;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Amount { get; set; }

        /// <summary>
        /// InitialAmount
        /// </summary>
        [PositiveNumber]
        public int InitialAmount { get; set; }

        /// <summary>
        /// LimitAmount
        /// </summary>
        [PositiveNumber]
        public double LimitAmount { get; set; }

        /// <summary>
        /// LimitCurrency
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string LimitCurrency { get; set; } = null!;

        /// <summary>
        /// Status
        /// </summary>
        [StringLength(32, MinimumLength = 4)]
        public string Status { get; set; } = null!;

        /// <summary>
        /// Created
        /// </summary>
        [ValidTimestamp]
        public DateTime Created { get; set; }

        /// <summary>
        /// UserNameSubmitted
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Trades
        /// </summary>
        public virtual List<CXOSTrade> Trades { get; set; } = new();
    }
}
