﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ProductionLineOrder
    /// </summary>
    public class ProductionLineOrder : IValidation
    {
        /// <summary>
        /// ProductionLineOrderId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ProductionLineOrderId { get; set; } = null!;

        /// <summary>
        /// RawInputs
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string RawInputs { get; set; } = null!;

        /// <summary>
        /// Inputs
        /// </summary>
        [NotMapped]
        public List<MaterialAndAmount> Inputs
        {
            get
            {
                if (_Inputs == null)
                {
                    _Inputs = RawInputs.SplitOnMultiple(",", "x")
                        .Select(i => new MaterialAndAmount
                        {
                            Amount = int.Parse(i[0]),
                            Ticker = i[1]
                        })
                        .ToList();
                }

                return _Inputs;
            }
        }

        [NotMapped]
        private List<MaterialAndAmount>? _Inputs;

        /// <summary>
        /// RawOutputs
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string RawOutputs { get; set; } = null!;

        /// <summary>
        /// Outputs
        /// </summary>
        [NotMapped]
        public List<MaterialAndAmount> Outputs
        {
            get
            {
                if (_Outputs == null)
                {
                    _Outputs = RawOutputs.SplitOnMultiple(",", "x")
                        .Select(i => new MaterialAndAmount
                        {
                            Amount = int.Parse(i[0]),
                            Ticker = i[1]
                        })
                        .ToList();
                }

                return _Outputs;
            }
        }

        [NotMapped]
        private List<MaterialAndAmount>? _Outputs;

        /// <summary>
        /// Created
        /// </summary>
        [ValidTimestamp]
        public DateTime Created { get; set; }

        /// <summary>
        /// Started
        /// </summary>
        [ValidTimestamp]
        public DateTime? Started { get; set; }

        /// <summary>
        /// Completion
        /// </summary>
        [ValidTimestamp]
        public DateTime? Completion { get; set; }

        /// <summary>
        /// DurationMs
        /// </summary>
        public long? DurationMs { get; set; }

        /// <summary>
        /// LastUpdated
        /// </summary>
        [ValidTimestamp]
        public DateTime? LastUpdated { get; set; }

        /// <summary>
        /// CompletionPercentage
        /// </summary>
        [Range(0.0, 1.0)]
        public double CompletionPecentage { get; set; }

        /// <summary>
        /// Halted
        /// </summary>
        public bool Halted { get; set; }

        /// <summary>
        /// FeeCurrencyCode
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string? FeeCurrencyCode { get; set; }

        /// <summary>
        /// Fee
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? Fee { get; set; }

        /// <summary>
        /// FeeCollectorId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? FeeCollectorId { get; set; }

        /// <summary>
        /// FeeCollectorName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? FeeCollectorName { get; set; }

        /// <summary>
        /// FeeCollectorCode
        /// </summary>
        [Ticker]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string? FeeCollectorCode { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Recurring { get; set; }

        /// <summary>
        /// ProductionLineId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ProductionLineId { get; set; } = null!;

        /// <summary>
        /// ProductionLine (Parent)
        /// </summary>
        [JsonIgnore]
        public ProductionLine ProductionLine { get; set; } = null!;
    }
}
