﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// CXBuyOrder
    /// </summary>
    [Index(nameof(CompanyId))]
    [Index(nameof(CompanyName))]
    [Index(nameof(CompanyCode))]
    public class CXBuyOrder : IValidation
    {
        /// <summary>
        /// CXBuyOrderId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CXBuyOrderId { get; set; } = null!;

        /// <summary>
        /// CompanyId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CompanyId { get; set; } = null!;

        /// <summary>
        /// CompanyName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string CompanyName { get; set; } = null!;

        /// <summary>
        /// CompanyCode
        /// </summary>
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string CompanyCode { get; set; } = null!;

        /// <summary>
        /// ItemCount
        /// </summary>
        [PositiveNumber]
        public int? ItemCount { get; set; }

        /// <summary>
        /// ItemCost
        /// </summary>
        [PositiveNumber]
        public double ItemCost { get; set; }

        /// <summary>
        /// CXEntryId (Parent)
        /// </summary>
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CXEntryId { get; set; } = null!;

        /// <summary>
        /// CXEntry (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual CXEntry CXEntry { get; set; } = null!;
    }
}
