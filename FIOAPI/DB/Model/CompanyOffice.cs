﻿
namespace FIOAPI.DB.Model
{
    /// <summary>
    /// CompanyOffice
    /// </summary>
    public class CompanyOffice : IValidation, ICloneable
    {
        /// <summary>
        /// CompanyOfficeId: {CompanyId}-{StartEpochMs}
        /// </summary>
        [Key]
        [StringLength(65, MinimumLength = 35)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CompanyOfficeId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(8, MinimumLength = 8)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Start
        /// </summary>
        [ValidTimestamp]
        public DateTime Start { get; set; }

        /// <summary>
        /// End
        /// </summary>
        [ValidTimestamp]
        public DateTime End { get; set; }

        /// <summary>
        /// PlanetId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? PlanetName { get; set; } = null!;

        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// CompanyId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CompanyId { get; set; } = null!;

        /// <summary>
        /// Company (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual Company Company { get; set; } = null!;

        #region ICloneable interface
        /// <summary>
        /// Clones the object
        /// </summary>
        /// <returns>A copy of the object</returns>
        public object Clone()
        {
            return new CompanyOffice()
            {
                CompanyOfficeId = CompanyOfficeId,
                Type = Type,
                Start = Start,
                End = End,
                PlanetId = PlanetId,
                PlanetName = PlanetName,
                PlanetNaturalId = PlanetNaturalId,
                CompanyId = CompanyId,
            };
        }
        #endregion

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (Type != "GOVERNOR")
            {
                Errors.Add($"{Context}.{nameof(Type)} is not the text 'GOVERNOR'");
            }
        }
    }
}
