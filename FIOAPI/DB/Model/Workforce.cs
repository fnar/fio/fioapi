﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Workforce
    /// </summary>
    [RequiredPermission(Perm.Sites_Workforces)]
    public class Workforce : IValidation
    {
        /// <summary>
        /// WorkforceId
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string WorkforceId { get; set; } = null!;

        /// <summary>
        /// SiteId
        /// </summary>
        [NotMapped]
        public string SiteId
        {
            get
            {
                return WorkforceId;
            }
        }

        /// <summary>
        /// PioneerPopulation
        /// </summary>
        [Range(0, int.MaxValue)]
        public int PioneerPopulation { get; set; }

        /// <summary>
        /// PioneerReserve
        /// </summary>
        [Range(0, int.MaxValue)]
        public int PioneerReserve { get; set; }

        /// <summary>
        /// PioneerCapacity
        /// </summary>
        [Range(0, int.MaxValue)]
        public int PioneerCapacity { get; set; }

        /// <summary>
        /// PioneerRequired
        /// </summary>
        [Range(0, int.MaxValue)]
        public int PioneerRequired { get; set; }

        /// <summary>
        /// PioneerSatisfaction
        /// </summary>
        [Range(0.0, 1.0)]
        public double PioneerSatisfaction { get; set; }

        /// <summary>
        /// SettlerPopulation
        /// </summary>
        [Range(0, int.MaxValue)]
        public int SettlerPopulation { get; set; }

        /// <summary>
        /// SettlerReserve
        /// </summary>
        [Range(0, int.MaxValue)]
        public int SettlerReserve { get; set; }

        /// <summary>
        /// SettlerCapacity
        /// </summary>
        [Range(0, int.MaxValue)]
        public int SettlerCapacity { get; set; }

        /// <summary>
        /// SettlerRequired
        /// </summary>
        [Range(0, int.MaxValue)]
        public int SettlerRequired { get; set; }

        /// <summary>
        /// SettlerSatisfaction
        /// </summary>
        [Range(0.0, 1.0)]
        public double SettlerSatisfaction { get; set; }

        /// <summary>
        /// TechnicianPopulation
        /// </summary>
        [Range(0, int.MaxValue)]
        public int TechnicianPopulation { get; set; }

        /// <summary>
        /// TechnicianReserve
        /// </summary>
        [Range(0, int.MaxValue)]
        public int TechnicianReserve { get; set; }

        /// <summary>
        /// TechnicianCapacity
        /// </summary>
        [Range(0, int.MaxValue)]
        public int TechnicianCapacity { get; set; }

        /// <summary>
        /// TechnicianRequired
        /// </summary>
        [Range(0, int.MaxValue)]
        public int TechnicianRequired { get; set; }

        /// <summary>
        /// TechnicianSatisfaction
        /// </summary>
        [Range(0.0, 1.0)]
        public double TechnicianSatisfaction { get; set; }

        /// <summary>
        /// EngineerPopulation
        /// </summary>
        [Range(0, int.MaxValue)]
        public int EngineerPopulation { get; set; }

        /// <summary>
        /// EngineerReserve
        /// </summary>
        [Range(0, int.MaxValue)]
        public int EngineerReserve { get; set; }

        /// <summary>
        /// EngineerCapacity
        /// </summary>
        [Range(0, int.MaxValue)]
        public int EngineerCapacity { get; set; }

        /// <summary>
        /// EngineerRequired
        /// </summary>
        [Range(0, int.MaxValue)]
        public int EngineerRequired { get; set; }

        /// <summary>
        /// EngineerSatisfaction
        /// </summary>
        [Range(0.0, 1.0)]
        public double EngineerSatisfaction { get; set; }

        /// <summary>
        /// ScientistPopulation
        /// </summary>
        [Range(0, int.MaxValue)]
        public int ScientistPopulation { get; set; }

        /// <summary>
        /// ScientistReserve
        /// </summary>
        [Range(0, int.MaxValue)]
        public int ScientistReserve { get; set; }

        /// <summary>
        /// ScientistCapacity
        /// </summary>
        [Range(0, int.MaxValue)]
        public int ScientistCapacity { get; set; }

        /// <summary>
        /// ScientistRequired
        /// </summary>
        [Range(0, int.MaxValue)]
        public int ScientistRequired { get; set; }

        /// <summary>
        /// ScientistSatisfaction
        /// </summary>
        [Range(0.0, 1.0)]
        public double ScientistSatisfaction { get; set; }

        /// <summary>
        /// LastTickTime - When consumables were last pulled from this site
        /// </summary>
        [ValidTimestamp]
        public DateTime? LastTickTime { get; set; }

        /// <summary>
        /// UserNameSubmitted
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// WorkforceNeeds
        /// </summary>
        public virtual List<WorkforceNeed> WorkforceNeeds { get; set; } = new List<WorkforceNeed>();
    }
}
