﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// BuildingRecipeMaterial
    /// </summary>
    public class BuildingRecipeOutput : IValidation, ICloneable
    {
        /// <summary>
        /// BuildingRecipeOutputId
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(100)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingRecipeOutputId { get; set; } = null!;

        /// <summary>
        /// Id of the material
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string Id { get; set; } = null!;

        /// <summary>
        /// Name of the material
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Ticker of the material
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// Weight of the material
        /// </summary>
        [Range(0.00001, 1000.0)]
        public double Weight { get; set; } = 0.0;

        /// <summary>
        /// Volume of the material
        /// </summary>
        [Range(0.00001, 1000.0)]
        public double Volume { get; set; } = 0.0;

        /// <summary>
        /// Amount of the material
        /// </summary>
        [Range(1, 5000)]
        public int Amount { get; set; } = 0;

        /// <summary>
        /// BuildingRecipeId
        /// </summary>
        [JsonIgnore]
        [StringLength(100)]
        public string BuildingRecipeId { get; set; } = null!;

        /// <summary>
        /// BuildingRecipe
        /// </summary>
        [JsonIgnore]
        public virtual BuildingRecipe BuildingRecipe { get; set; } = null!;

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new BuildingRecipeOutput()
            {
                BuildingRecipeOutputId = BuildingRecipeOutputId,
                Id = Id,
                Name = Name,
                Ticker = Ticker,
                Weight = Weight,
                Volume = Volume,
                Amount = Amount,
                BuildingRecipeId = BuildingRecipeId,
                // Don't add parent BuildingRecipe
            };
        }
    }
}
