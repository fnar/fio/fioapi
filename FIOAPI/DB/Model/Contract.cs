﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Contract
    /// </summary>
    [RequiredPermission(Perm.Trade_Contract)]
    public class Contract : IValidation
    {
        /// <summary>
        /// ContractId
        /// </summary>
        [Key]
        [APEXID]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ContractId { get; set; } = null!;

        /// <summary>
        /// Date
        /// </summary>
        [ValidTimestamp]
        public DateTime Date { get; set; }

        /// <summary>
        /// Party
        /// </summary>
        [StringLength(32, MinimumLength = 1)]
        public string Party { get; set; } = null!;

        /// <summary>
        /// PartnerId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PartnerId { get; set; } = null!;

        /// <summary>
        /// PartnerCode
        /// </summary>
        [Uppercase]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string? PartnerCode { get; set; }

        /// <summary>
        /// PartnerName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string PartnerName { get; set; } = null!;

        /// <summary>
        /// Status
        /// </summary>
        [StringLength(32, MinimumLength = 1)]
        public string Status { get; set; } = null!;

        /// <summary>
        /// ExtensionDeadline
        /// </summary>
        [ValidTimestamp]
        public DateTime? ExtensionDeadline { get; set; }

        /// <summary>
        /// CanExtend
        /// </summary>
        public bool CanExtend { get; set; }

        /// <summary>
        /// CanRequestTerminiation
        /// </summary>
        public bool CanRequestTermination { get; set; }

        /// <summary>
        /// DueDate
        /// </summary>
        [ValidTimestamp]
        public DateTime? DueDate { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        [StringLength(64)]
        public string? Name { get; set; }

        /// <summary>
        /// Preamble
        /// </summary>
        [StringLength(255)]
        public string? Preamble { get; set; }

        /// <summary>
        /// TerminationSent
        /// </summary>
        public bool TerminationSent { get; set; }

        /// <summary>
        /// TerminationReceived
        /// </summary>
        public bool TerminationReceived { get; set; }

        /// <summary>
        /// AgentContract
        /// </summary>
        public bool AgentContract { get; set; }

        /// <summary>
        /// RelatedContracts (comma separated)
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string RelatedContracts { get; set; } = null!;

        /// <summary>
        /// RelatedContractsList
        /// </summary>
        [NotMapped]
        public List<string> RelatedContractsList
        {
            get
            {
                if (_RelatedContractsList == null)
                {
                    _RelatedContractsList = RelatedContracts.SplitOn(",");
                }

                return _RelatedContractsList;
            }
        }

        [NotMapped]
        private List<string>? _RelatedContractsList = null;

        /// <summary>
        /// ContractType
        /// </summary>
        [StringLength(64, MinimumLength = 2)]
        public string? ContractType { get; set; }

        /// <summary>
        /// UserNameSubmitted
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Conditions
        /// </summary>
        public virtual List<ContractCondition> Conditions { get; set; } = new();
    }
}
