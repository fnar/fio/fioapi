﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// JumpCache
    /// </summary>
    [Index(nameof(SourceSystemId))]
    [Index(nameof(SourceSystemName))]
    [Index(nameof(SourceSystemNaturalId))]
    [Index(nameof(DestinationSystemId))]
    [Index(nameof(DestinationSystemName))]
    [Index(nameof(DestinationNaturalId))]
    public class JumpCache : IValidation
    {
        /// <summary>
        /// JumpCacheId: {SourceSystemId}-{DestinationSystemId}
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(65, MinimumLength = 65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string JumpCacheId { get; set; } = null!;

        /// <summary>
        /// SourceSystemId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string SourceSystemId { get; set; } = null!;

        /// <summary>
        /// SourceSystemName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? SourceSystemName { get; set; } = null!;

        /// <summary>
        /// SourceSystemNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string SourceSystemNaturalId { get; set; } = null!;

        /// <summary>
        /// DestinationSystemId
        /// </summary>
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string DestinationSystemId { get; set; } = null!;

        /// <summary>
        /// DestinationSystemName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? DestinationSystemName { get; set; } = null!;

        /// <summary>
        /// DestinationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string DestinationNaturalId { get; set; } = null!;

        /// <summary>
        /// OverallDistance
        /// </summary>
        [Range(0, double.MaxValue)]
        public double OverallDistance { get; set; }

        /// <summary>
        /// JumpCount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int JumpCount { get; set; }

        /// <summary>
        /// Jumps
        /// </summary>
        public virtual List<JumpCacheRouteJump> Jumps { get; set; } = new();
    }
}
