﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// CXPC
    /// </summary>
    [Index(nameof(MaterialTicker))]
    [Index(nameof(ExchangeCode))]
    [Index(nameof(MaterialTicker), nameof(ExchangeCode), IsUnique = true)]
    public class CXPC : IValidation
    {
        /// <summary>
        /// CXPCId
        /// </summary>
        [Key]
        [APEXID]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CXPCId { get; set; } = null!;

        /// <summary>
        /// BrokerId
        /// </summary>
        [NotMapped]
        public string BrokerId
        {
            get
            {
                return CXPCId;
            }
        }

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// ExchangeCode
        /// </summary>
        [StringLength(3, MinimumLength = 3)]
        public string ExchangeCode { get; set; } = null!;

        /// <summary>
        /// UserNameSubmitted
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Entries
        /// </summary>
        public virtual List<CXPCEntry> Entries { get; set; } = new();
    }
}
