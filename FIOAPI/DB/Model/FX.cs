﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// FX
    /// </summary>
    [Index(nameof(Ticker), IsUnique = true)]
    public class FX : IValidation
    {
        /// <summary>
        /// FXID
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string FXID { get; set; } = null!;

        /// <summary>
        /// The ticker for an FX entry `XXX/YYY`
        /// </summary>
        [FXTicker]
        [StringLength(Constants.FXTickerLengthMax, MinimumLength = Constants.FXTickerLengthMin)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// BaseCurrencyNumericCode
        /// </summary>
        public int BaseCurrencyNumericCode { get; set; }

        /// <summary>
        /// BaseCurrencyCode
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string BaseCurrencyCode { get; set; } = null!;

        /// <summary>
        /// BaseCurrencyName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string BaseCurrencyName { get; set; } = null!;

        /// <summary>
        /// BaseCurrencyDecimals
        /// </summary>
        [Range(1, 100)]
        public int BaseCurrencyDecimals { get; set; }

        /// <summary>
        /// QuoteCurrencyNumericCode
        /// </summary>
        public int QuoteCurrencyNumericCode { get; set; }

        /// <summary>
        /// QuoteCurrencyCode
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string QuoteCurrencyCode { get; set; } = null!;

        /// <summary>
        /// QuoteCurrencyName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string QuoteCurrencyName { get; set; } = null!;

        /// <summary>
        /// QuoteCurrencyDecimals
        /// </summary>
        [Range(1, 100)]
        public int QuoteCurrencyDecimals { get; set; }

        /// <summary>
        /// Decimals
        /// </summary>
        [Range(1, 100)]
        public int Decimals { get; set; }

        /// <summary>
        /// Open
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double Open { get; set; }

        /// <summary>
        /// Close
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double Close { get; set; }

        /// <summary>
        /// Low
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double Low { get; set; }

        /// <summary>
        /// High
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double High { get; set; }

        /// <summary>
        /// Previous
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double Previous { get; set; }

        /// <summary>
        /// traded
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double Traded { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        [Range(0, double.MaxValue)]
        public double Volume { get; set; }

        /// <summary>
        /// BidRate
        /// </summary>
        [Range(0.00001, double.MaxValue)]
        public double? Bid { get; set; }

        /// <summary>
        /// AskRate
        /// </summary>
        [Range(0.00001, double.MaxValue)]
        public double? Ask { get; set; }

        /// <summary>
        /// Spread
        /// </summary>
        [Range(0.00001, double.MaxValue)]
        public double? Spread { get; set; }

        /// <summary>
        /// LotSizeAmount
        /// </summary>
        [Range(0.00001, double.MaxValue)]
        public double LotSizeAmount { get; set; }

        /// <summary>
        /// LotCurrency
        /// </summary>
        [CurrencyCode]
        [StringLength(3, MinimumLength = 3)]
        public string LotCurrency { get; set; } = null!;

        /// <summary>
        /// FeesFactor
        /// </summary>
        [Range(0.00001, 100.0)]
        public double FeesFactor { get; set; }

        /// <summary>
        /// BuyOrders
        /// </summary>
        public virtual List<FXBuyOrder> BuyOrders { get; set; } = new List<FXBuyOrder>();

        /// <summary>
        /// SellOrders
        /// </summary>
        public virtual List<FXSellOrder> SellOrders { get; set; } = new List<FXSellOrder>();

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
