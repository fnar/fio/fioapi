﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// UserModel
    /// </summary>
    [Index(nameof(UserName), IsUnique = true)]
    public class User : IValidation
    {
        /// <summary>
        /// UserId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserId { get; set; }

        /// <summary>
        /// UserName
        /// </summary>
        [Lowercase]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// DisplayUserName
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string DisplayUserName { get; set; } = null!;

        /// <summary>
        /// PasswordHash
        /// </summary>
        [PasswordHash]
        [StringLength(128)]
        public string PasswordHash { get; set; } = null!;

        /// <summary>
        /// IsAdmin
        /// </summary>
        [DefaultValue(false)]
        public bool IsAdmin { get; set; } = false;

        /// <summary>
        /// IsBot
        /// </summary>
        [NotMapped]
        public bool IsBot { get => UserName.EndsWith(" - bot"); }

        /// <summary>
        /// DiscordId
        /// </summary>
        [StringLength(32)]
        [DefaultValue("")]
        public string DiscordName { get; set; } = "";
    }
}
