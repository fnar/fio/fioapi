﻿using System.Xml.Linq;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// APIKey model
    /// </summary>
    [Index(nameof(UserName))]
    [Index(nameof(Key))]
    public class APIKey : IValidation, ICloneable
    {
        /// <summary>
        /// APIKeyId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int APIKeyId { get; set; }

        /// <summary>
        /// The actual Key to use
        /// </summary>
        [GuidValid]
        public Guid Key { get; set; }

        /// <summary>
        /// The username owner
        /// </summary>
        [Lowercase]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// The application name
        /// </summary>
        [StringLength(128)]
        [DefaultValue("")]
        public string Application { get; set; } = "";

        /// <summary>
        /// If this APIKey should allow writes
        /// </summary>
        [DefaultValue(false)]
        public bool AllowWrites { get; set; } = false;

        /// <summary>
        /// The time of APIKey creation in Utc
        /// </summary>
        public DateTime CreateTime { get; set; } = DateTime.UtcNow;

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new APIKey()
            {
                APIKeyId = APIKeyId,
                Key = Key,
                UserName = UserName,
                Application = Application,
                AllowWrites = AllowWrites,
                CreateTime = CreateTime,
            };
        }
    }
}
