﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ShipRepairMaterial
    /// </summary>
    [Index(nameof(ShipId))]
    public class ShipRepairMaterial : IValidation
    {
        /// <summary>
        /// ShipRepairMaterialId: {ShipId}-{MaterialId}
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(65, MinimumLength = 65)]
        public string ShipRepairMaterialId { get; set; } = null!;

        /// <summary>
        /// MaterialId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Amount { get; set; }

        /// <summary>
        /// ShipId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ShipId { get; set; } = null!;

        /// <summary>
        /// Ship (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual Ship Ship { get; set; } = null!;
    }
}
