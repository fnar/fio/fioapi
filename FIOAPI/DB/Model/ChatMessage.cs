﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ChatMessage
    /// </summary>
    [Index(nameof(ChatChannelId))]
    [Index(nameof(MessageTimestamp))]
    [Index(nameof(Timestamp))]
    public class ChatMessage : IValidation
    {
        /// <summary>
        /// ChatMessageId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ChatMessageId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(32, MinimumLength = 2)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// SenderId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string SenderId { get; set; } = null!;

        /// <summary>
        /// SenderUserName
        /// </summary>
        [Lowercase]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string SenderUserName { get; set; } = null!;

        /// <summary>
        /// MessageText
        /// </summary>
        [StringLength(1024)]
        public string? MessageText { get; set; } = null;

        /// <summary>
        /// MessageTimestamp
        /// </summary>
        public long MessageTimestamp { get; set; } = 0;

        /// <summary>
        /// MessageDeleted
        /// </summary>
        public bool MessageDeleted { get; set; } = false;

        /// <summary>
        /// DeletedByUserId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? DeletedByUserId { get; set; } = null;

        /// <summary>
        /// DeletedByUserName
        /// </summary>
        [Lowercase]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string? DeletedByUserName { get; set; } = null;

        /// <summary>
        /// UserNameSubmitted
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// ChatChannelId
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ChatChannelId { get; set; } = null!;

        /// <summary>
        /// ChatChannel
        /// </summary>
        [JsonIgnore]
        public virtual ChatChannel ChatChannel { get; set; } = null!;

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            if (DeletedByUserName != null && DeletedByUserName.Length < 3)
            {
                Errors.Add($"'{Context}' has fewer than 3 characters.");
            }

            base.CustomValidation(ref Errors, Context);
        }
    }
}
