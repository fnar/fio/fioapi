﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ProductionLine
    /// </summary>
    [RequiredPermission(Perm.Sites_ProductionLines)]
    public class ProductionLine : IValidation
    {
        /// <summary>
        /// ProductionLineId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ProductionLineId { get; set; } = null!;

        /// <summary>
        /// SiteId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string SiteId { get; set; } = null!;

        /// <summary>
        /// LocationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string LocationId { get; set; } = null!;

        /// <summary>
        /// LocationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string LocationNaturalId { get; set; } = null!;

        /// <summary>
        /// LocationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string LocationName { get; set; } = null!;

        /// <summary>
        /// BuildingName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string BuildingName { get; set; } = null!;

        /// <summary>
        /// BuildingTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string? BuildingTicker { get; set; }

        /// <summary>
        /// Capacity
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Capacity { get; set; }

        /// <summary>
        /// Slots - Not sure what this represents
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Slots { get; set; }

        /// <summary>
        /// Efficiency
        /// </summary>
        [Range(0, double.MaxValue)]
        public double Efficiency { get; set; }

        /// <summary>
        /// Condition
        /// </summary>
        [Range(0, 1.0)]
        public double Condition { get; set; }

        /// <summary>
        /// RawWorkforceEfficiencies
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string RawWorkforceEfficiencies { get; set; } = null!;

        /// <summary>
        /// WorkforceEfficiencies
        /// </summary>
        [NotMapped]
        public List<WorkforceLevelAndEfficiency> WorkforceEfficiencies
        {
            get
            {
                if (_WorkforceEfficiencies == null)
                {
                    _WorkforceEfficiencies = RawWorkforceEfficiencies
                        .SplitOnMultiple(" ", ":")
                        .Select(s => new WorkforceLevelAndEfficiency
                        {
                            Level = s[0],
                            Efficiency = double.Parse(s[1])
                        })
                        .ToList();
                }

                return _WorkforceEfficiencies;
            }
        }

        [NotMapped]
        private List<WorkforceLevelAndEfficiency>? _WorkforceEfficiencies = null;

        /// <summary>
        /// RawEfficiencyFactors
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string RawEfficiencyFactors { get; set; } = null!;

        /// <summary>
        /// EfficiencyFactors
        /// </summary>
        [NotMapped]
        public List<EfficiencyFactor> EfficiencyFactors
        {
            get
            {
                if (_EfficiencyFactors == null)
                {
                    _EfficiencyFactors = RawEfficiencyFactors.SplitOnMultiple(",", ":", "->", "!")
                        .Select(s => new EfficiencyFactor
                        {
                            ExpertiseCategory = s[0] == " " ? null : s[0],  // Special case: If no expertise category, make it space
                            Type = s[1],
                            Effectivity = double.Parse(s[2]),
                            Value = double.Parse(s[3])
                        })
                        .ToList();
                }

                return _EfficiencyFactors;
            }
        }

        [NotMapped]
        private List<EfficiencyFactor>? _EfficiencyFactors;

        /// <summary>
        /// Orders
        /// </summary>
        public virtual List<ProductionLineOrder> Orders { get; set; } = new();

        /// <summary>
        /// The username that submitted the data
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(32, MinimumLength = 3)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// The timestamp of the data
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
