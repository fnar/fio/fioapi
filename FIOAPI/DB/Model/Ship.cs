﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Ship
    /// </summary>
    [Index(nameof(StoreId))]
    [Index(nameof(STLFuelStoreId))]
    [Index(nameof(FTLFuelStoreId))]
    [RequiredPermission(Perm.Ship_Information)]
    public class Ship : IValidation
    {
        /// <summary>
        /// ShipId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ShipId { get; set; } = null!;

        /// <summary>
        /// StoreId (Cargo)
        /// </summary>
        [APEXID]
        [RequiredPermission(Perm.Ship_Inventory)]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string StoreId { get; set; } = null!;

        /// <summary>
        /// STLFuelStoreId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [RequiredPermission(Perm.Ship_FuelInventory)]
        public string STLFuelStoreId { get; set; } = null!;

        /// <summary>
        /// FTLFuelStoreId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [RequiredPermission(Perm.Ship_FuelInventory)]
        public string FTLFuelStoreId { get; set; } = null!;

        /// <summary>
        /// Registration
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string Registration { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        [StringLength(32)]
        public string? Name { get; set; }

        /// <summary>
        /// CommisioningTime
        /// </summary>
        [ValidTimestamp]
        public DateTime CommisioningTime { get; set; }

        /// <summary>
        /// BlueprintNaturalId
        /// </summary>
        [StringLength(20, MinimumLength = 1)]
        public string BlueprintNaturalId { get; set; } = null!;

        /// <summary>
        /// CurrentLocationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [RequiredPermission(Perm.Ship_Flight)]
        public string? CurrentLocationId { get; set; }

        /// <summary>
        /// CurrentLocationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        [RequiredPermission(Perm.Ship_Flight)]
        public string? CurrentLocationName { get; set; }

        /// <summary>
        /// CurrentLocationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        [RequiredPermission(Perm.Ship_Flight)]
        public string? CurrentLocationNaturalId { get; set; }

        /// <summary>
        /// FlightId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [RequiredPermission(Perm.Ship_Flight)]
        public string? FlightId { get; set; }

        /// <summary>
        /// Acceleration
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double Acceleration { get; set; }

        /// <summary>
        /// Thrust
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double Thrust { get; set; }

        /// <summary>
        /// Mass
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double Mass { get; set; }

        /// <summary>
        /// OperatingEmptyMass
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double OperatingEmptyMass { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double Volume { get; set; }

        /// <summary>
        /// ReactorPower
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? ReactorPower { get; set; }

        /// <summary>
        /// EmitterPower
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double EmitterPower { get; set; }

        /// <summary>
        /// STLFuelFlowRate
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double STLFuelFlowRate { get; set; }

        /// <summary>
        /// OperatingTimeSTL
        /// </summary>
        public long OperatingTimeSTLMilliseconds { get; set; }

        /// <summary>
        /// OperatingTimeFTLMilliseconds
        /// </summary>
        public long OperatingTimeFTLMilliseconds { get; set; }

        /// <summary>
        /// Condition
        /// </summary>
        [Range(0.0, 1.0)]
        public double Condition { get; set; }

        /// <summary>
        /// LastRepair
        /// </summary>
        [ValidTimestamp]
        [RequiredPermission(Perm.Ship_Repair)]
        public DateTime? LastRepair { get; set; }

        /// <summary>
        /// RepairMaterials
        /// </summary>
        [RequiredPermission(Perm.Ship_Repair)]
        public virtual List<ShipRepairMaterial> RepairMaterials { get; set; } = new();

        /// <summary>
        /// The username that submitted the data
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// The timestamp of the data
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
