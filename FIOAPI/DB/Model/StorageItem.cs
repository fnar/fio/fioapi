﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// StorageItem
    /// </summary>
    [RequiredPermission(Perm.Storage_Items)]
    public class StorageItem : IValidation
    {
        /// <summary>
        /// StorageItemId: {StorageId}-{StorageItem.Id}
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(65, MinimumLength = 65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string StorageItemId { get; set; } = null!;

        /// <summary>
        /// ItemId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ItemId { get; set; } = null!;

        /// <summary>
        /// MaterialId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? MaterialId { get; set; }

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string? MaterialTicker { get; set; }

        /// <summary>
        /// Amount
        /// </summary>
        [PositiveNumber]
        public int? Amount { get; set; }

        /// <summary>
        /// Weight
        /// </summary>
        [PositiveNumber]
        public double Weight { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        [PositiveNumber]
        public double Volume { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string Type { get; set;} = null!;

        /// <summary>
        /// ValueCurrencyCode
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string? ValueCurrencyCode { get; set; }

        /// <summary>
        /// Value
        /// </summary>
        public double? Value { get; set; }

        /// <summary>
        /// StorageId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string StorageId { get; set; } = null!;

        /// <summary>
        /// Storage (Parent)
        /// </summary>
        [JsonIgnore]
        public Storage Storage { get; set; } = null!;
    }
}
