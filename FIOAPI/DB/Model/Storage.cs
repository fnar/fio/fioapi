﻿using System.Linq;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Storage
    /// </summary>
    [RequiredPermission(Perm.Storage_Information)]
    public class Storage : IValidation
    {
        /// <summary>
        /// StorageId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string StorageId { get; set; } = null!;

        /// <summary>
        /// AddressableId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [RequiredPermission(Perm.Storage_Location)]
        public string AddressableId { get; set; } = null!;

        /// <summary>
        /// SiteId
        /// </summary>
        [NotMapped]
        public string SiteId
        {
            get
            {
                return AddressableId;
            }
        }

        /// <summary>
        /// Name
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? Name { get; set; }

        /// <summary>
        /// LocationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? LocationId { get; set; }

        /// <summary>
        /// LocationNaturalId
        /// </summary>
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? LocationNaturalId { get; set; }

        /// <summary>
        /// LocationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? LocationName { get; set; }

        /// <summary>
        /// WeightLoad
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double WeightLoad { get; set; }

        /// <summary>
        /// WeightCapacity
        /// </summary>
        [PositiveNumber]
        public double WeightCapacity { get; set; }

        /// <summary>
        /// VolumeLoad
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double VolumeLoad { get; set; }

        /// <summary>
        /// VolumeCapacity
        /// </summary>
        [PositiveNumber]
        public double VolumeCapacity { get; set; }

        /// <summary>
        /// Fixed - If it's a fixed/stationary store
        /// </summary>
        public bool Fixed { get; set; }

        /// <summary>
        /// TradeStore - If you can trade from it. Only fuel tanks cannot.
        /// </summary>
        public bool TradeStore { get; set; }

        /// <summary>
        /// Rank - Usage not yet known
        /// </summary>
        public int Rank { get; set; }

        /// <summary>
        /// Locked - If the store is locked (warehouse not payed)
        /// </summary>
        public bool Locked { get; set; }

        /// <summary>
        /// Type - STORE, WAREHOUSE_STORE, STL_FUEL_STORE, FTL_FUEL_STORE
        /// </summary>
        [StringLength(Constants.StoreTypeLengthMax, MinimumLength = Constants.StoreTypeLengthMin)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// StorageItems
        /// </summary>
        [RequiredPermission(Perm.Storage_Items)]
        public virtual List<StorageItem> StorageItems { get; set; } = new();

        /// <summary>
        /// The username that submitted the data
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// The timestamp of the data
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!Constants.StoreTypeEnumToStoreType.Values.Contains(Type))
            {
                Errors.Add($"'{Context}.{nameof(Type)}' with value '{Type}' is not a valid store type");
            }
        }
    }
}
