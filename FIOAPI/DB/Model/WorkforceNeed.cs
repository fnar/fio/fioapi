﻿
namespace FIOAPI.DB.Model
{
    /// <summary>
    /// WorkforceNeed
    /// </summary>
    [RequiredPermission(Perm.Sites_Workforces)]
    public class WorkforceNeed : IValidation
    {
        /// <summary>
        /// WorkforceNeedId
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(48, MinimumLength = 42)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string WorkforceNeedId { get; set; } = null!; // {WorkforceId}-{Level}-{MaterialTicker}

        /// <summary>
        /// Level
        /// </summary>
        [WorkforceLevel]
        [StringLength(Constants.WorkforceLevelLengthMax, MinimumLength = Constants.WorkforceLevelLengthMin)]
        public string Level { get; set; } = null!;

        /// <summary>
        /// Category
        /// </summary>
        [StringLength(8, MinimumLength = 4)]
        public string Category { get; set; } = null!;

        /// <summary>
        /// Essential
        /// </summary>
        public bool Essential { get; set; }

        /// <summary>
        /// MaterialId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// MaterialName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string MaterialName { get; set; } = null!;

        /// <summary>
        /// Satisfaction
        /// </summary>
        [Range(0.0, 1.0)]
        public double Satisfaction { get; set; }

        /// <summary>
        /// UnitsPerInterval
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double UnitsPerInterval { get; set; }

        /// <summary>
        /// UnitsPer100
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double UnitsPer100 { get; set; }

        /// <summary>
        /// WorkforceId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string WorkforceId { get; set; } = null!;

        /// <summary>
        /// Workforce (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual Workforce Workforce { get; set; } = null!;

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            var WorkforceNeedIdSplits = WorkforceNeedId.SplitOn("-");
            if (WorkforceNeedIdSplits.Count != 3)
            {
                Errors.Add($"{Context} is not formatted properly");
            }

            ValidationExtensions.RunValidationsAsIfTypeAndProperty(WorkforceNeedIdSplits[0], typeof(Site), nameof(Site.SiteId), ref Errors, $"{Context}.{nameof(WorkforceNeedId)}-LeftSide");
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(WorkforceNeedIdSplits[1], typeof(WorkforceNeed), nameof(WorkforceNeed.Level), ref Errors, $"{Context}.{nameof(WorkforceNeedId)}-Middle");
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(WorkforceNeedIdSplits[2], typeof(WorkforceNeed), nameof(WorkforceNeed.MaterialTicker), ref Errors, $"{Context}.{nameof(WorkforceNeedId)}-Right");

            if (!Constants.ValidWorkforceNeedCategories.Contains(Category))
            {
                Errors.Add($"{Context}.{nameof(Category)} is not async valid workforce need category");
            }
        }
    }
}
