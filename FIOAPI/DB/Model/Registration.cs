﻿
namespace FIOAPI.DB.Model
{
    /// <summary>
    /// RegistrationModel
    /// </summary>
    [Index(nameof(UserName))]
    public class Registration : IValidation
    {
        /// <summary>
        /// RegistrationId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int RegistrationId { get; set; }

        /// <summary>
        /// UserName
        /// </summary>
        [Lowercase]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// DisplayUserName
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string DisplayUserName { get; set; } = null!;

        /// <summary>
        /// RegistrationGuid
        /// </summary>
        [GuidValid]
        public Guid RegistrationGuid { get; set; }

        /// <summary>
        /// RegistrationTime
        /// </summary>
        public DateTime RegistrationTime { get; set; }

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (UserName.Contains(' '))
            {
                Errors.Add($"{Context}.{nameof(UserName)} cannot have spaces in it.");
            }
        }
    }
}
