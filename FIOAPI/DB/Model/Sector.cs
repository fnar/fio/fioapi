﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Sector
    /// </summary>
    [Index(nameof(Name))]
    public class Sector : IValidation
    {
        /// <summary>
        /// SectorId
        /// </summary>
        [Key]
        [StringLength(10, MinimumLength = 8)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SectorId { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? Name { get; set; } = null!;

        /// <summary>
        /// HexQ
        /// </summary>
        public int HexQ { get; set; }

        /// <summary>
        /// HexR
        /// </summary>
        public int HexR { get; set; }

        /// <summary>
        /// HexS
        /// </summary>
        public int HexS { get; set; }

        /// <summary>
        /// Size
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// SubSectors
        /// </summary>
        public virtual List<SubSector> SubSectors { get; set; } = new();

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!SectorId.StartsWith("sector-"))
            {
                Errors.Add($"'{Context}' does not start with the string 'sector-'");
            }
        }
    }
}
