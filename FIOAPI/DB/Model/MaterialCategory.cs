﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// MaterialCategory
    /// </summary>
    public class MaterialCategory : IValidation
    {
        /// <summary>
        /// MaterialCategoryId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string MaterialCategoryId { get; set; } = null!;

        /// <summary>
        /// CategoryName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string CategoryName { get; set; } = null!;

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
