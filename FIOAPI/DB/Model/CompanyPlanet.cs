﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// CompanyPlanet
    /// </summary>
    [Index(nameof(PlanetId))]
    [Index(nameof(PlanetName))]
    [Index(nameof(PlanetNaturalId))]
    public class CompanyPlanet : IValidation, ICloneable
    {
        /// <summary>
        /// CompanyPlanetId: {CompanyId}-{PlanetId}
        /// </summary>
        [Key]
        [StringLength(65, MinimumLength = 65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CompanyPlanetId { get; set; } = null!;

        /// <summary>
        /// PlanetId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string PlanetName { get; set; } = null!;

        /// <summary>
        /// PlanetNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// CompanyId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CompanyId { get; set; } = null!;

        /// <summary>
        /// Company (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual Company Company { get; set; } = null!;

        #region ICloneable interface
        /// <summary>
        /// Clones the object
        /// </summary>
        /// <returns>A copy of the object</returns>
        public object Clone()
        {
            return new CompanyPlanet()
            {
                CompanyPlanetId = CompanyPlanetId,
                PlanetId = PlanetId,
                PlanetName = PlanetName,
                PlanetNaturalId = PlanetNaturalId,
                CompanyId = CompanyId
            };
        }
        #endregion
    }
}
