﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// CXOSTrade
    /// </summary>
    [RequiredPermission(Perm.Trade_CXOS)]
    public class CXOSTrade : IValidation
    {
        /// <summary>
        /// CXOSTradeId
        /// </summary>
        [Key]
        [APEXID]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CXOSTradeId { get; set; } = null!;

        /// <summary>
        /// TradeId
        /// </summary>
        [NotMapped]
        public string TradeId
        {
            get
            {
                return CXOSTradeId;
            }
        }

        /// <summary>
        /// Amount
        /// </summary>
        [PositiveNumber]
        public int Amount { get; set; }

        /// <summary>
        /// PriceAmount
        /// </summary>
        [PositiveNumber]
        public double PriceAmount { get; set; }

        /// <summary>
        /// PriceCurrency
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string PriceCurrency { get; set; } = null!;

        /// <summary>
        /// Time
        /// </summary>
        [ValidTimestamp]
        public DateTime Time { get; set; }

        /// <summary>
        /// PartnerId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PartnerId { get; set; } = null!;

        /// <summary>
        /// PartnerCode
        /// </summary>
        [Uppercase]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string? PartnerCode { get; set; }

        /// <summary>
        /// PartnerName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string PartnerName { get; set; } = null!;

        /// <summary>
        /// CXOSId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CXOSId { get; set; } = null!;

        /// <summary>
        /// CXOS (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual CXOS CXOS { get; set; } = null!;
    }
}
