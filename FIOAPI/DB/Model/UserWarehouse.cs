﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// UserWarehouse
    /// </summary>
    [RequiredPermission(Perm.Sites_Location | Perm.Storage_Information | Perm.Storage_Location | Perm.Storage_Information)]
    public class UserWarehouse : IValidation
    {
        /// <summary>
        /// WarehouseId
        /// </summary>
        [Key]
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string UserWarehouseId { get; set; } = null!;

        /// <summary>
        /// AddressableId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string AddressableId { get; set; } = null!;

        /// <summary>
        /// StoreId
        /// </summary>
        [NotMapped]
        public string StoreId => UserWarehouseId;

        /// <summary>
        /// LocationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? LocationId { get; set; }

        /// <summary>
        /// LocationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? LocationNaturalId { get; set; }

        /// <summary>
        /// LocationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? LocationName { get; set; }

        /// <summary>
        /// Units
        /// </summary>
        [PositiveNumber]
        public int Units { get; set; }

        /// <summary>
        /// WeightCapacity
        /// </summary>
        [PositiveNumber]
        public double WeightCapacity { get; set; }

        /// <summary>
        /// VolumeCapacity
        /// </summary>
        [PositiveNumber]
        public double VolumeCapacity { get; set; }

        /// <summary>
        /// NextPayment
        /// </summary>
        [ValidTimestamp]
        public DateTime? NextPayment { get; set; }

        /// <summary>
        /// FeeCurrency
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string? FeeCurrency { get; set; }

        /// <summary>
        /// Fee
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? Fee { get; set; }

        /// <summary>
        /// FeeCollectorId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? FeeCollectorId { get; set; }

        /// <summary>
        /// FeeCollectorCode
        /// </summary>
        [Ticker]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string? FeeCollectorCode { get; set; }

        /// <summary>
        /// FeeCollectorName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? FeeCollectorName { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [StringLength(32)]
        public string Status { get; set; } = null!;

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
