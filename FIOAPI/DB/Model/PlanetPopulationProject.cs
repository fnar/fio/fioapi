﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// PlanetPopulationProject
    /// </summary>
    public class PlanetPopulationProject : IValidation, ICloneable
    {
        /// <summary>
        /// PlanetPopulationProjectId
        /// </summary>
        [Key]
        [APEXID]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetPopulationProjectId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(Constants.PlanetaryInfrastructureProjectLengthMax, MinimumLength = Constants.PlanetaryInfrastructureProjectLengthMin)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// ProjectIdentifier
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string ProjectIdentifier { get; set; } = null!;

        /// <summary>
        /// Level
        /// </summary>
        [Range(0, 10)]
        public int Level { get; set; }

        /// <summary>
        /// ActiveLevel
        /// </summary>
        [Range(-1, 10)]
        public int ActiveLevel { get; set; }

        /// <summary>
        /// CurrentLevel
        /// </summary>
        [Range(0, 10)]
        public int CurrentLevel { get; set; }

        /// <summary>
        /// UpgradeStatus
        /// </summary>
        [Range(0.0, 1.0)]
        public double UpgradeStatus { get; set; }

        /// <summary>
        /// UpgradeCostsRaw
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string UpgradeCostsRaw { get; set; } = "";

        /// <summary>
        /// UpgradeCosts
        /// </summary>
        [NotMapped]
        public List<PlanetaryProjectUpgradeCost> UpgradeCosts
        {
            get
            {
                if (_UpgradeCosts == null)
                {
                    var SplitResult = UpgradeCostsRaw.SplitOnMultiple(",", ":", "_");
                    _UpgradeCosts = SplitResult
                        .Select(sr => new PlanetaryProjectUpgradeCost
                        {
                            Ticker = sr[0],
                            Amount = int.Parse(sr[1]),
                            CurrentAmount = int.Parse(sr[2])
                        })
                        .ToList();
                }

                return _UpgradeCosts;
            }
        }

        [NotMapped]
        [JsonIgnore]
        private List<PlanetaryProjectUpgradeCost>? _UpgradeCosts = null;

        /// <summary>
        /// Upkeeps
        /// </summary>
        public virtual List<PlanetPopulationProjectUpkeep> Upkeeps { get; set; } = new();

        /// <summary>
        /// PlanetId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// Planet (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual Planet Planet { get; set; } = null!;

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            var copy = new PlanetPopulationProject()
            {
                PlanetPopulationProjectId = PlanetPopulationProjectId,
                Type = Type,
                ProjectIdentifier = ProjectIdentifier,
                Level = Level,
                ActiveLevel = ActiveLevel,
                CurrentLevel = CurrentLevel,
                UpgradeStatus = UpgradeStatus,
                UpgradeCostsRaw = UpgradeCostsRaw,
                PlanetId = PlanetId
            };

            Upkeeps.ForEach(u =>
            {
                var uCopy = (PlanetPopulationProjectUpkeep)u.Clone();
                uCopy.PlanetPopulationProject = copy;
                copy.Upkeeps.Add(uCopy);
            });

            return copy;
        }
    }

    /// <summary>
    /// PlanetaryProjectUpgradeCost
    /// </summary>
    [NotMapped]
    public class PlanetaryProjectUpgradeCost
    {
        /// <summary>
        /// Ticker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Range(1, int.MaxValue)]
        public int Amount { get; set; }

        /// <summary>
        /// CurrentAmount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int CurrentAmount { get; set; }
    }
}
