﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// PlanetResource
    /// </summary>
    public class PlanetResource : IValidation, ICloneable
    {
        /// <summary>
        /// PlanetResourceId: {PlanetId}-{MaterialId}
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(65, MinimumLength = 65)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string PlanetResourceId { get; set; } = null!;

        /// <summary>
        /// MaterialId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// Type (resource type)
        /// </summary>
        [ResourceType]
        [StringLength(Constants.PlanetResourceTypeLengthMax, MinimumLength = Constants.PlanetResourceTypeLengthMin)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Resource concentration
        /// </summary>
        [PositiveNumber]
        public double Concentration { get; set; }

        /// <summary>
        /// (Parent) PlanetId
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// (Parent) Planet
        /// </summary>
        [JsonIgnore]
        public virtual Planet Planet { get; set; } = null!;

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new PlanetResource()
            {
                PlanetResourceId = PlanetResourceId,
                MaterialId = MaterialId,
                Type = Type,
                Concentration = Concentration,
                PlanetId = PlanetId
            };
        }
    }
}
