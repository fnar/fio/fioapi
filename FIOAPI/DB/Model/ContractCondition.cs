﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ContractCondition
    /// </summary>
    [RequiredPermission(Perm.Trade_Contract)]
    public class ContractCondition : IValidation
    {
        /// <summary>
        /// ContractConditionId
        /// </summary>
        [Key]
        [APEXID]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ContractConditionId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(32, MinimumLength = 2)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Index
        /// </summary>
        public int Index { get; set; }

        /// <summary>
        /// Status
        /// </summary>
        [StringLength(32, MinimumLength = 2)]
        public string Status { get; set; } = null!;

        /// <summary>
        /// Dependeices (Comma-separated)
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        public string Dependencies { get; set; } = null!;

        /// <summary>
        /// DependenciesList
        /// </summary>
        [NotMapped]
        public List<string> DependenciesList
        {
            get
            {
                if (_DependenciesList == null)
                {
                    _DependenciesList = Dependencies.SplitOn(",");
                }

                return _DependenciesList;
            }
        }

        [NotMapped]
        private List<string>? _DependenciesList;

        /// <summary>
        /// DeadlineDuration
        /// </summary>
        public long? DeadlineDuration { get; set; }

        /// <summary>
        /// Deadline
        /// </summary>
        public DateTime? Deadline { get; set; }

        /// <summary>
        /// QuantityMaterialTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string? QuantityMaterialTicker { get; set; }

        /// <summary>
        /// QuantityAmount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int? QuantityAmount { get; set; }

        /// <summary>
        /// AddressId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? AddressId { get; set; }

        /// <summary>
        /// AddressNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? AddressNaturalId { get; set; }

        /// <summary>
        /// AddressName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? AddressName { get; set; }

        /// <summary>
        /// BlockId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? BlockId { get; set; }

        /// <summary>
        /// PickedUpMaterialTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string? PickedUpMaterialTicker { get; set; }

        /// <summary>
        /// PickedUpAmount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int? PickedUpAmount { get; set; }

        /// <summary>
        /// Weight
        /// </summary>
        [PositiveNumber]
        public double? Weight { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        [PositiveNumber]
        public double? Volume { get; set; }

        /// <summary>
        /// DestinationAddressId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? DestinationAddressId { get; set; }

        /// <summary>
        /// DestinationAddressNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? DestinationAddressNaturalId { get; set; }

        /// <summary>
        /// DestinationAddressName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? DestinationAddressName { get; set; }

        /// <summary>
        /// ShipmentItemId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? ShipmentItemId { get; set; }

        /// <summary>
        /// CountryId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? CountryId { get; set; }

        /// <summary>
        /// ReputationChange
        /// </summary>
        [PositiveNumber]
        public int? ReputationChange { get; set; }

        /// <summary>
        /// LoanInterestAmount
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? LoanInterestAmount { get; set; }

        /// <summary>
        /// LoanInterestCurrency
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string? LoanInterestCurrency { get; set; }

        /// <summary>
        /// LoanRepaymentAmount
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? LoanRepaymentAmount { get; set; }

        /// <summary>
        /// LoanRepaymentCurrency
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string? LoanRepaymentCurrency { get; set; }

        /// <summary>
        /// LoanTotalAmount
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? LoanTotalAmount { get; set; }

        /// <summary>
        /// LoanTotalCurrency
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string? LoanTotalCurrency { get; set; }

        /// <summary>
        /// ContractId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ContractId { get; set; } = null!;

        /// <summary>
        /// Contract (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual Contract Contract { get; set; } = null!;
    }
}
