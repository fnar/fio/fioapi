﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// SiteBuilding
    /// </summary>
    [RequiredPermission(Perm.Sites_Buildings)]
    public class SiteBuilding : IValidation
    {
        /// <summary>
        /// SiteBuildingId (PlatformId)
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SiteBuildingId { get; set; } = null!;

        /// <summary>
        /// Area
        /// </summary>
        [PositiveNumber]
        public int Area { get; set; }

        /// <summary>
        /// CreationTime
        /// </summary>
        [ValidTimestamp]
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// BuildingName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string BuildingName { get; set; } = null!;

        /// <summary>
        /// BuildingTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string BuildingTicker { get; set; } = null!;

        /// <summary>
        /// BuildingType
        /// </summary>
        [StringLength(20, MinimumLength = 4)]
        public string BuildingType { get; set; } = null!;

        /// <summary>
        /// BookValueCurrencyCode
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string BookValueCurrencyCode { get; set; } = null!;

        /// <summary>
        /// BookValueAmount
        /// </summary>
        public double BookValueAmount { get; set; }

        /// <summary>
        /// Condition
        /// </summary>
        [Range(0.0, 1.0)]
        public double Condition { get; set; }

        /// <summary>
        /// LastRepair
        /// </summary>
        [ValidTimestamp]
        [RequiredPermission(Perm.Sites_Repair)]
        public DateTime? LastRepair { get; set; }

        /// <summary>
        /// RawReclaimableMaterials: {Amount}x{Ticker},{Amount}x{Ticker}
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        [RequiredPermission(Perm.Sites_Reclaimable)]
        public string RawReclaimableMaterials { get; set; } = null!;

        /// <summary>
        /// ReclaimableMaterials
        /// </summary>
        [NotMapped]
        public List<MaterialTickerAndAmount> ReclaimableMaterials
        {
            get
            {
                if (_ReclaimableMaterials == null)
                {
                    _ReclaimableMaterials = RawReclaimableMaterials
                        .SplitOnMultiple(",", "x")
                        .Select(s => new MaterialTickerAndAmount
                        {
                            Amount = int.Parse(s[0]),
                            Ticker = s[1]
                        })
                        .ToList();
                }

                return _ReclaimableMaterials;
            }
        }
        private List<MaterialTickerAndAmount>? _ReclaimableMaterials;

        /// <summary>
        /// RawRepairMaterials: {Amount}x{Ticker},{Amount}x{Ticker}
        /// </summary>
        [JsonIgnore]
        [StringLength(500)]
        [RequiredPermission(Perm.Sites_Repair)]
        public string RawRepairMaterials { get; set; } = null!;

        /// <summary>
        /// RepairMaterials
        /// </summary>
        [NotMapped]
        public List<MaterialTickerAndAmount> RepairMaterials
        {
            get
            {
                if (_RepairMaterials == null)
                {
                    _RepairMaterials = RawRepairMaterials.SplitOnMultiple(",", "x")
                        .Select(s => new MaterialTickerAndAmount
                        {
                            Amount = int.Parse(s[0]),
                            Ticker = s[1]
                        })
                        .ToList();
                }

                return _RepairMaterials;
            }
        }
        private List<MaterialTickerAndAmount>? _RepairMaterials;

        /// <summary>
        /// SiteId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string SiteId { get; set; } = null!;

        /// <summary>
        /// Site (Parent)
        /// </summary>
        [JsonIgnore]
        public Site Site { get; set; } = null!;
    }
}
