﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// GroupUser model
    /// </summary>
    [Index(nameof(UserName))]
    [Index(nameof(GroupId))]
    public class GroupUser : IValidation
    {
        /// <summary>
        /// GroupUserId
        /// </summary>
        [Key]
        [JsonIgnore]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int GroupUserId { get; set; }

        /// <summary>
        /// UserName
        /// </summary>
        [Lowercase]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserName { get; set; } = null!;

        /// <summary>
        /// GroupId
        /// </summary>
        [JsonIgnore]
        [Range(0, Group.LargestGroupId)]
        public int GroupId { get; set; }

        /// <summary>
        /// Group
        /// </summary>
        [JsonIgnore]
        public virtual Group Group { get; set; } = null!;
    }
}
