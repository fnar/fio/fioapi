﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Sites
    /// </summary>
    [RequiredPermission(Perm.Sites_Location)]
    public class Site : IValidation
    {
        /// <summary>
        /// SiteId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SiteId { get; set; } = null!;

        /// <summary>
        /// LocationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string LocationId { get; set; } = null!;

        /// <summary>
        /// LocationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string LocationNaturalId { get; set; } = null!;

        /// <summary>
        /// LocationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string LocationName { get; set; } = null!;

        /// <summary>
        /// Founded
        /// </summary>
        [ValidTimestamp]
        public DateTime Founded { get; set; } = DateTime.MinValue;

        /// <summary>
        /// Area
        /// </summary>
        [PositiveNumber]
        public int Area { get; set; }

        /// <summary>
        /// InvestedPermits
        /// </summary>
        [PositiveNumber]
        public int InvestedPermits { get; set; }

        /// <summary>
        /// MaximumPermits
        /// </summary>
        [PositiveNumber]
        public int MaximumPermits { get; set; }

        /// <summary>
        /// Platforms
        /// </summary>
        [RequiredPermission(Perm.Sites_Buildings)]
        public virtual List<SiteBuilding> Buildings { get; set; } = new();

        /// <summary>
        /// The username that submitted the data
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// The timestamp of the data
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
