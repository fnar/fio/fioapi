﻿using FIOAPI.Caches;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Building Model
    /// </summary>
    [Index(nameof(Ticker))]
    public class Building : IValidation, IAPEXEntity
    {
        /// <summary>
        /// BuildingId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingId { get; set; } = null!;

        /// <summary>
        /// The name of the building
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// The ticker of the building
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// The expertise of this building
        /// </summary>
        [StringLength(Constants.BuildingCategoryLengthMax, MinimumLength = Constants.BuildingCategoryLengthMin)]
        public string? Expertise { get; set; } = null;

        /// <summary>
        /// The number of pioneers this building uses
        /// </summary>
        [Range(0, 200)]
        public int Pioneers { get; set; } = 0;

        /// <summary>
        /// The number of settlers this building uses
        /// </summary>
        [Range(0, 200)]
        public int Settlers { get; set; } = 0;

        /// <summary>
        /// The number of technicians this building uses
        /// </summary>
        [Range(0, 200)]
        public int Technicians { get; set; } = 0;

        /// <summary>
        /// The number of engineers this building uses
        /// </summary>
        [Range(0, 200)]
        public int Engineers { get; set; } = 0;

        /// <summary>
        /// The number of scientists this building uses
        /// </summary>
        [Range(0, 200)]
        public int Scientists { get; set; } = 0;

        /// <summary>
        /// The area cost of this building
        /// </summary>
        [Range(1, 2000)]
        public int AreaCost { get; set; } = 0;

        /// <summary>
        /// The costs of the building
        /// </summary>
        public virtual List<BuildingCost> Costs { get; set; } = new List<BuildingCost>();

        /// <summary>
        /// Recipes for the building
        /// </summary>
        public virtual List<BuildingRecipe> Recipes { get; set; } = new List<BuildingRecipe>();

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        #region IAPEXEntity Interface
        /// <summary>
        /// EntityId
        /// </summary>
        [NotMapped]
        public string EntityId
        {
            get
            {
                return BuildingId;
            }
        }

        /// <summary>
        /// EntityName
        /// </summary>
        [NotMapped]
        public string EntityName
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// EntityFriendlyIdentifier
        /// </summary>
        [NotMapped]
        public string EntityFriendlyIdentifier
        {
            get
            {
                return Ticker;
            }
        }
        #endregion

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy of this object</returns>
        public object Clone()
        {
            var copy = new Building();
            copy.BuildingId = BuildingId;
            copy.Name = Name;
            copy.Ticker = Ticker;
            copy.Expertise = Expertise;
            copy.Pioneers = Pioneers;
            copy.Settlers = Settlers;
            copy.Technicians = Technicians;
            copy.Engineers = Engineers;
            copy.Scientists = Scientists;
            copy.AreaCost = AreaCost;

            Costs.ForEach(c =>
            {
                var costCopy = (BuildingCost)c.Clone();
                costCopy.Building = copy;
                copy.Costs.Add(costCopy);
            });

            Recipes.ForEach(r =>
            {
                var recipeCopy = (BuildingRecipe)r.Clone();
                recipeCopy.Building = copy;
                copy.Recipes.Add(recipeCopy);
            });

            copy.UserNameSubmitted = UserNameSubmitted;
            copy.Timestamp = Timestamp;
            return copy;
        }
    }
}
