﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// CX (CXEntry)
    /// </summary>
    [Index(nameof(MaterialTicker))]
    [Index(nameof(ExchangeCode))]
    [Index(nameof(MaterialTicker), nameof(ExchangeCode), IsUnique = true)]
    public class CXEntry : IValidation
    {
        /// <summary>
        /// CXId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CXEntryId { get; set; } = null!;

        /// <summary>
        /// MaterialId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string MaterialId { get; set; } = null!;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string MaterialTicker { get; set; } = null!;

        /// <summary>
        /// ExchangeId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string ExchangeId { get; set; } = null!;

        /// <summary>
        /// ExchangeCode
        /// </summary>
        [Ticker]
        [CustomStringLength]
        [StringLength(3, MinimumLength = 3)]
        public string ExchangeCode { get; set; } = null!;

        /// <summary>
        /// CurrencyCode
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string CurrencyCode { get; set;} = null!;

        /// <summary>
        /// Price
        /// </summary>
        [PositiveNumber]
        public double? Price { get; set; }

        /// <summary>
        /// PriceTime
        /// </summary>
        public DateTime? PriceTime { get; set; }

        /// <summary>
        /// High
        /// </summary>
        [PositiveNumber]
        public double? High { get; set; }

        /// <summary>
        /// AllTimeHigh
        /// </summary>
        [PositiveNumber]
        public double? AllTimeHigh { get; set; }

        /// <summary>
        /// Low
        /// </summary>
        [PositiveNumber]
        public double? Low { get; set; }

        /// <summary>
        /// AllTimeLow
        /// </summary>
        [PositiveNumber]
        public double? AllTimeLow { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [PositiveNumber]
        public double? Ask { get; set; }

        /// <summary>
        /// AskCount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int? AskCount { get; set; }

        /// <summary>
        /// Bid
        /// </summary>
        [PositiveNumber]
        public double? Bid { get; set; }

        /// <summary>
        /// BidCount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int? BidCount { get; set; }

        /// <summary>
        /// Supply
        /// </summary>
        [Range(0, int.MaxValue)]
        public int? Supply { get; set; }

        /// <summary>
        /// Demand
        /// </summary>
        [Range(0, int.MaxValue)]
        public int? Demand { get; set; }

        /// <summary>
        /// Traded
        /// </summary>
        [Range(0, int.MaxValue)]
        public int? Traded { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? Volume { get; set; }

        /// <summary>
        /// PriceAverage
        /// </summary>
        [PositiveNumber]
        public double? PriceAverage { get; set; }

        /// <summary>
        /// NarrowPriceBandLow
        /// </summary>
        [PositiveNumber]
        public double? NarrowPriceBandLow { get; set; }

        /// <summary>
        /// NarrowPriceBandHigh
        /// </summary>
        [PositiveNumber]
        public double? NarrowPriceBandHigh { get; set; }

        /// <summary>
        /// WidePriceBandLow
        /// </summary>
        [PositiveNumber]
        public double? WidePriceBandLow { get; set; }

        /// <summary>
        /// WidePriceBandHigh
        /// </summary>
        [PositiveNumber]
        public double? WidePriceBandHigh { get; set; }

        /// <summary>
        /// MMBuy
        /// </summary>
        [PositiveNumber]
        public double? MMBuy { get; set; }

        /// <summary>
        /// MMSell
        /// </summary>
        [PositiveNumber]
        public double? MMSell { get; set; }

        /// <summary>
        /// UserNameSubmitted
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// BuyOrders
        /// </summary>
        public virtual List<CXBuyOrder> BuyOrders { get; set; } = new();

        /// <summary>
        /// SellOrders
        /// </summary>
        public virtual List<CXSellOrder> SellOrders { get; set; } = new();
    }
}
