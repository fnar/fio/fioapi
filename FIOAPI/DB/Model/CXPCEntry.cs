﻿
namespace FIOAPI.DB.Model
{
    /// <summary>
    /// CXPCEntry
    /// </summary>
    public class CXPCEntry : IValidation
    {
        /// <summary>
        /// CXPCEntryId
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(61, MinimumLength = 54)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CXPCEntryId { get; set; } = null!; // CXPCId-Interval-DateEpochMs

        /// <summary>
        /// Interval
        /// </summary>
        [StringLength(Constants.CXPCIntervalLengthMax, MinimumLength = Constants.CXPCIntervalLengthMin)]
        public string Interval { get; set; } = null!;

        /// <summary>
        /// Date
        /// </summary>
        [NotMapped]
        public DateTime Date
        {
            get
            {
                return DateEpochMs.FromUnixTime();
            }
        }

        /// <summary>
        /// DateEpochMs
        /// </summary>
        [JsonIgnore]
        [ValidTimestamp]
        public long DateEpochMs { get; set; }

        /// <summary>
        /// Open
        /// </summary>
        public double Open { get; set; }

        /// <summary>
        /// Close
        /// </summary>
        public double Close { get; set; }

        /// <summary>
        /// High
        /// </summary>
        public double High { get; set; }

        /// <summary>
        /// Low
        /// </summary>
        public double Low { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        public double Volume { get; set; }

        /// <summary>
        /// Traded
        /// </summary>
        public int Traded { get; set; }

        /// <summary>
        /// CXPCId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CXPCId { get; set; } = null!;

        /// <summary>
        /// CXPC (Parent)
        /// </summary>
        [JsonIgnore]
        public CXPC CXPC { get; set; } = null!;

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (!Constants.ValidCXPCIntervals.Contains(Interval))
            {
                Errors.Add($"{Context}.{nameof(Interval)} has value of '{Interval}' which is not valid.");
            }
        }
    }
}
