﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// LocalMarketAd
    /// </summary>
    [Index(nameof(PlanetId))]
    [Index(nameof(PlanetNaturalId))]
    [Index(nameof(PlanetName))]
    [Index(nameof(LocalMarketId))]
    public class LocalMarketAd : IValidation
    {
        /// <summary>
        /// LocalMarketAdId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string LocalMarketAdId { get; set; } = null!;

        /// <summary>
        /// LocalMarketId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string LocalMarketId { get; set; } = null!;

        /// <summary>
        /// PlanetId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// NaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string PlanetNaturalId { get; set; } = null!;

        /// <summary>
        /// PlanetName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string PlanetName { get; set; } = null!;

        /// <summary>
        /// AdNaturalId
        /// </summary>
        [Range(0, int.MaxValue)]
        public int AdNaturalId { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [Uppercase]
        [StringLength(Constants.LocalMarketTypeLengthMax, MinimumLength = Constants.LocalMarketTypeLengthMin)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// Weight
        /// </summary>
        [Range(0.00001, 100000)]
        public double? Weight { get; set; }

        /// <summary>
        /// Volume
        /// </summary>
        [Range(0.00001, 100000)]
        public double? Volume { get; set; }

        /// <summary>
        /// CreatorId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CreatorId { get; set; } = null!;

        /// <summary>
        /// CreatorName
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string CreatorName { get; set; } = null!;

        /// <summary>
        /// CreatorCode
        /// </summary>
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string CreatorCode { get; set; } = null!;

        /// <summary>
        /// MaterialTicker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string? MaterialTicker { get; set; }

        /// <summary>
        /// MaterialAmount
        /// </summary>
        [Range(1, int.MaxValue)]
        public int? MaterialAmount { get; set; }

        /// <summary>
        /// Price
        /// </summary>
        [Range(0.000001, double.MaxValue)]
        public double Price { get; set; }

        /// <summary>
        /// Currency
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string Currency { get; set; } = null!;

        /// <summary>
        /// FulfillmentDays
        /// </summary>
        [Range(1, 100)]
        public int FulfillmentDays { get; set; }

        /// <summary>
        /// MinimumRating
        /// </summary>
        [StringLength(Constants.RatingLengthMax, MinimumLength = Constants.RatingLengthMin)]
        public string MinimumRating { get; set; } = null!;

        /// <summary>
        /// CreationTime
        /// </summary>
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// Expiry
        /// </summary>
        public DateTime Expiry { get; set; }

        /// <summary>
        /// OriginNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? OriginNaturalId { get; set; }

        /// <summary>
        /// DestinationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string? DestinationNaturalId { get; set; }

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
