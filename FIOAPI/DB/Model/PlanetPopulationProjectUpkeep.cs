﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// PlanetPopulationProjectUpkeep
    /// </summary>
    public class PlanetPopulationProjectUpkeep : IValidation, ICloneable
    {
        /// <summary>
        /// PlanetPopulationProjectUpkeepId
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(37, MinimumLength = 34)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string PlanetPopulationProjectUpkeepId { get; set; } = null!;

        /// <summary>
        /// Stored
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Stored { get; set; }

        /// <summary>
        /// StoreCapacity
        /// </summary>
        [Range(0, int.MaxValue)]
        public int StoreCapacity { get; set; }

        /// <summary>
        /// Duration
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Duration { get; set; }

        /// <summary>
        /// NextTick
        /// </summary>
        [ValidTimestamp]
        public DateTime NextTick { get; set; }

        /// <summary>
        /// Ticker
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string Ticker { get; set; } = null!;

        /// <summary>
        /// Amount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Amount { get; set; }

        /// <summary>
        /// CurrentAmount
        /// </summary>
        [Range(0, int.MaxValue)]
        public int CurrentAmount { get; set; }

        /// <summary>
        /// PlanetPopulationProjectId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetPopulationProjectId { get; set; } = null!;

        /// <summary>
        /// PlanetPopulationProject (Parent)
        /// </summary>
        [JsonIgnore]
        public virtual PlanetPopulationProject PlanetPopulationProject { get; set; } = null!;

        /// <summary>
        /// Cone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new PlanetPopulationProjectUpkeep()
            {
                PlanetPopulationProjectUpkeepId = PlanetPopulationProjectUpkeepId,
                Stored = Stored,
                StoreCapacity = StoreCapacity,
                Duration = Duration,
                NextTick = NextTick,
                Ticker = Ticker,
                Amount = Amount,
                CurrentAmount = CurrentAmount,
                PlanetPopulationProjectId = PlanetPopulationProjectId,
            };
        }
    }
}
