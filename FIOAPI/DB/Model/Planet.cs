﻿using FIOAPI.Caches;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Planet Model
    /// </summary>
    [Index(nameof(NaturalId))]
    [Index(nameof(Name))]
    [Index(nameof(PopulationId))]
    public class Planet : IValidation, IAPEXEntity
    {
        /// <summary>
        /// PlanetId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// NaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string NaturalId { get; set; } = null!;

        /// <summary>
        /// Name
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? Name { get; set; } = null;

        /// <summary>
        /// NamerId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? NamerId { get; set; } = null;

        /// <summary>
        /// NamerUserName
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string? NamerUserName { get; set; } = null;

        /// <summary>
        /// NamedTimestamp
        /// </summary>
        public DateTime? NamedTimestamp { get; set; }

        /// <summary>
        /// Nameable
        /// </summary>
        public bool Nameable { get; set; }

        /// <summary>
        /// Gravity
        /// </summary>
        [PositiveNumber]
        public double Gravity { get; set; }

        /// <summary>
        /// MagneticField
        /// </summary>
        [PositiveNumber]
        public double MagneticField { get; set; }

        /// <summary>
        /// Mass
        /// </summary>
        [PositiveNumber]
        public double Mass { get; set; }

        /// <summary>
        /// MassEarth
        /// </summary>
        [PositiveNumber]
        public double MassEarth { get; set; }

        /// <summary>
        /// SemiMajorAxis
        /// </summary>
        public double SemiMajorAxis { get; set; }

        /// <summary>
        /// Eccentricity
        /// </summary>
        public double Eccentricity { get; set; }

        /// <summary>
        /// Inclination
        /// </summary>
        public double Inclination { get; set; }

        /// <summary>
        /// RightAscension
        /// </summary>
        public double RightAscension { get; set; }

        /// <summary>
        /// Periapsis
        /// </summary>
        public double Periapsis { get; set; }

        /// <summary>
        /// OrbitIndex
        /// </summary>
        [Range(0, int.MaxValue)]
        public int OrbitIndex { get; set; }

        /// <summary>
        /// Pressure
        /// </summary>
        [PositiveNumber]
        public double Pressure { get; set; }

        /// <summary>
        /// Radiation
        /// </summary>
        [PositiveNumber]
        public double Radiation { get; set; }

        /// <summary>
        /// Radius
        /// </summary>
        [PositiveNumber]
        public double Radius { get; set; }

        /// <summary>
        /// Sunlight
        /// </summary>
        [PositiveNumber]
        public double Sunlight { get; set; }

        /// <summary>
        /// Temperature
        /// </summary>
        public double Temperature { get; set; }

        /// <summary>
        /// Fertility
        /// </summary>
        [Range(-1.0, 1.0)]
        public double? Fertility { get; set; }

        /// <summary>
        /// Surface
        /// </summary>
        public bool Surface { get; set; }

        /// <summary>
        /// Plots
        /// </summary>
        [Range(0, int.MaxValue)]
        public int Plots { get; set; }

        /// <summary>
        /// CountryId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? CountryId { get; set; }

        /// <summary>
        /// CountryName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? CountryName { get; set; }

        /// <summary>
        /// CountryCode
        /// </summary>
        [Ticker]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string? CountryCode { get; set; }

        /// <summary>
        /// GoverningEntity
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? GoverningEntity { get; set; }

        /// <summary>
        /// CurrencyNumericCode
        /// </summary>
        public int? CurrencyNumericCode { get; set; }

        /// <summary>
        /// CurrencyCode
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string? CurrencyCode { get; set; }

        /// <summary>
        /// CurrencyName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? CurrencyName { get; set; }

        /// <summary>
        /// CollectionCurrencyNumericCode
        /// </summary>
        public int? CollectionCurrencyNumericCode { get; set; }

        /// <summary>
        /// CollectionCurrencyCode
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string? CollectionCurrencyCode { get; set; }

        /// <summary>
        /// CollectionCurrencyName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? CollectionCurrencyName { get; set; }

        /// <summary>
        /// LocalMarketFeeBase
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? LocalMarketFeeBase { get; set; }

        /// <summary>
        /// LocalMarketTimeFactor
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? LocalMarketTimeFactor { get; set; }

        /// <summary>
        /// EstablishmentFee
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? EstablishmentFee { get; set; }

        /// <summary>
        /// WarehouseFee
        /// </summary>
        [Range(0.0, double.MaxValue)]
        public double? WarehouseFee { get; set; }

        /// <summary>
        /// PopulationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? PopulationId { get; set; }

        /// <summary>
        /// ChamberOfCommerceId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? ChamberOfCommerceId { get; set; }

        /// <summary>
        /// WarehouseId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? WarehouseId { get; set; }

        /// <summary>
        /// LocalMarketId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? LocalMarketId { get; set; }

        /// <summary>
        /// AdministrationCenterId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? AdministrationCenterId { get; set; }

        /// <summary>
        /// ShipyardId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? ShipyardId { get; set; }

        /// <summary>
        /// SupportsGhostSizes
        /// </summary>
        public bool SupportsGhostSites { get; set; }

        /// <summary>
        /// COGCProgram
        /// </summary>
        [StringLength(Constants.COGCProgramLengthMax, MinimumLength = Constants.COGCProgramLengthMin)]
        public string? CurrentCOGCProgram { get; set; }

        /// <summary>
        /// LastCOGCProgramUpdate
        /// </summary>
        public DateTime CurrentCOGCProgramEndTime { get; set; } = DateTime.MinValue;

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        // DEV NOTE: If you add another virtual List here, make sure to update the queries here to `Include` them:
        // * PlanetController.PutCOGC()
        // * EntityCaches.InitializeEntityCaches
        // Basically, we need to ensure the cache is up to date.

        /// <summary>
        /// Resources
        /// </summary>
        public virtual List<PlanetResource> Resources { get; set; } = new();

        /// <summary>
        /// WorkforceFees
        /// </summary>
        public virtual List<PlanetWorkforceFee> WorkforceFees { get; set; } = new();

        /// <summary>
        /// COGCPrograms
        /// </summary>
        public virtual List<PlanetCOGCProgram> COGCPrograms { get; set; } = new();

        /// <summary>
        /// PopulationReports
        /// </summary>
        public virtual List<PlanetPopulationReport> PopulationReports { get; set; } = new();

        /// <summary>
        /// PopulationProjects
        /// </summary>
        public virtual List<PlanetPopulationProject> PopulationProjects { get; set; } = new();

        #region Helper Properties
        /// <summary>
        /// HasShipyard
        /// </summary>
        [NotMapped]
        public bool HasShipyard
        {
            get
            {
                return ShipyardId != null && ShipyardId != Constants.InvalidAPEXID;
            }
            set { }
        }

        /// <summary>
        /// HasAdministrationCenter
        /// </summary>
        [NotMapped]
        public bool HasAdministrationCenter
        {
            get
            {
                return AdministrationCenterId != null && AdministrationCenterId != Constants.InvalidAPEXID;
            }
            set { }
        }

        /// <summary>
        /// HasCOGC
        /// </summary>
        [NotMapped]
        public bool HasChamberOfCommerce
        {
            get
            {
                return ChamberOfCommerceId != null && ChamberOfCommerceId != Constants.InvalidAPEXID;
            }
            set { }
        }

        /// <summary>
        /// HasWarehouse
        /// </summary>
        [NotMapped]
        public bool HasWarehouse
        {
            get
            {
                return WarehouseId != null && WarehouseId != Constants.InvalidAPEXID;
            }
            set { }
        }

        /// <summary>
        /// HasLocalMarket
        /// </summary>
        [NotMapped]
        public bool HasLocalMarket
        {
            get
            {
                return LocalMarketId != null && LocalMarketId != Constants.InvalidAPEXID;
            }
            set { }
        }

        /// <summary>
        /// If planet requires MCG
        /// </summary>
        [NotMapped]
        public bool Rocky
        {
            get
            {
                return Surface;
            }
            set { }
        }

        /// <summary>
        /// If planet requires AEF
        /// </summary>
        [NotMapped]
        public bool Gaseous
        {
            get
            {
                return !Surface;
            }
            set { }
        }

        /// <summary>
        /// If planet requires MGC
        /// </summary>
        [NotMapped]
        public bool LowGravity
        {
            get
            {
                return Gravity < Constants.LowGravityThreshold;
            }
            set { }
        }

        /// <summary>
        /// If planet requires BL
        /// </summary>
        [NotMapped]
        public bool HighGravity
        {
            get
            {
                return Gravity > Constants.HighGravityThreshold;
            }
            set { }
        }

        /// <summary>
        /// If planet requires SEA
        /// </summary>
        [NotMapped]
        public bool LowPressure
        {
            get
            {
                return Pressure < Constants.LowPressureThreshold;
            }
            set { }
        }

        /// <summary>
        /// If planet requires HSE
        /// </summary>
        [NotMapped]
        public bool HighPressure
        {
            get
            {
                return Pressure > Constants.HighPressureThreshold;
            }
            set { }
        }

        /// <summary>
        /// If planet requires INS
        /// </summary>
        [NotMapped]
        public bool LowTemperature
        {
            get
            {
                return Temperature < Constants.LowTemperatureThreshold;
            }
            set { }
        }

        /// <summary>
        /// If planet requires TSH
        /// </summary>
        [NotMapped]
        public bool HighTemperature
        {
            get
            {
                return Temperature > Constants.HighTemperatureThreshold;
            }
            set { }
        }

        /// <summary>
        /// If the planet is fertile
        /// </summary>
        [NotMapped]
        public bool Fertile
        {
            get
            {
                return Fertility != null && Fertility > Constants.NotFertile;
            }
            set { }
        }
        #endregion

        #region IAPEXEntity
        /// <summary>
        /// EntityId
        /// </summary>
        [NotMapped]
        public string EntityId
        {
            get
            {
                return PlanetId;
            }
        }

        /// <summary>
        /// EntityName
        /// </summary>
        [NotMapped]
        public string? EntityName
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// EntityFriendlyIdentifier
        /// </summary>
        [NotMapped]
        public string EntityFriendlyIdentifier
        {
            get
            {
                return NaturalId;
            }
        }
        #endregion

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (CurrentCOGCProgram != null && !Constants.ValidCOGCProgramTypes.Contains(CurrentCOGCProgram))
            {
                Errors.Add($"{Context}.{nameof(CurrentCOGCProgram)} is not a valid COGC program");
            }
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            var copy = new Planet();
            copy.PlanetId = PlanetId;
            copy.NaturalId = NaturalId;
            copy.Name = Name;
            copy.NamerId = NamerId;
            copy.NamerUserName = NamerUserName;
            copy.NamedTimestamp = NamedTimestamp;
            copy.Nameable = Nameable;
            copy.Gravity = Gravity;
            copy.MagneticField = MagneticField;
            copy.Mass = Mass;
            copy.MassEarth = MassEarth;
            copy.SemiMajorAxis = SemiMajorAxis;
            copy.Eccentricity = Eccentricity;
            copy.Inclination = Inclination;
            copy.RightAscension = RightAscension;
            copy.Periapsis = Periapsis;
            copy.OrbitIndex = OrbitIndex;
            copy.Pressure = Pressure;
            copy.Radiation = Radiation;
            copy.Radius = Radius;
            copy.Sunlight = Sunlight;
            copy.Temperature = Temperature;
            copy.Fertility = Fertility;
            copy.Surface = Surface;
            copy.Plots = Plots;
            copy.CountryId = CountryId;
            copy.CountryName = CountryName;
            copy.CountryCode = CountryCode;
            copy.CurrencyNumericCode = CurrencyNumericCode;
            copy.CurrencyCode = CurrencyCode;
            copy.CurrencyName = CurrencyName;
            copy.CollectionCurrencyNumericCode = CollectionCurrencyNumericCode;
            copy.CollectionCurrencyCode = CollectionCurrencyCode;
            copy.CollectionCurrencyName = CollectionCurrencyName;
            copy.LocalMarketFeeBase = LocalMarketFeeBase;
            copy.LocalMarketTimeFactor = LocalMarketTimeFactor;
            copy.WarehouseFee = WarehouseFee;
            copy.PopulationId = PopulationId;
            copy.ChamberOfCommerceId = ChamberOfCommerceId;
            copy.WarehouseId = WarehouseId;
            copy.LocalMarketId = LocalMarketId;
            copy.AdministrationCenterId = AdministrationCenterId;
            copy.ShipyardId = ShipyardId;
            copy.SupportsGhostSites = SupportsGhostSites;
            copy.CurrentCOGCProgram = CurrentCOGCProgram;
            copy.CurrentCOGCProgramEndTime = CurrentCOGCProgramEndTime;
            copy.UserNameSubmitted = UserNameSubmitted;
            copy.Timestamp = Timestamp;

            Resources.ForEach(r =>
            {
                var rCopy = (PlanetResource)r.Clone();
                rCopy.Planet = copy;
                copy.Resources.Add(rCopy);
            });

            WorkforceFees.ForEach(wf =>
            {
                var wfCopy = (PlanetWorkforceFee)wf.Clone();
                wfCopy.Planet = copy;
                copy.WorkforceFees.Add(wfCopy);
            });


            COGCPrograms.ForEach(p =>
            {
                var pCopy = (PlanetCOGCProgram)p.Clone();
                pCopy.Planet = copy;
                copy.COGCPrograms.Add(pCopy);
            });

            PopulationReports.ForEach(pr =>
            {
                var prCopy = (PlanetPopulationReport)pr.Clone();
                prCopy.Planet = copy;
                copy.PopulationReports.Add(prCopy);
            });

            return copy;
        }
    }
}
