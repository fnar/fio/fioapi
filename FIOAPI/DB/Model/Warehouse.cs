﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Warehouse
    /// </summary>
    public class Warehouse : IValidation, IAPEXEntity
    {
        /// <summary>
        /// WarehouseId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string WarehouseId { get; set; } = null!;

        /// <summary>
        /// LocationId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string LocationId { get; set; } = null!;

        /// <summary>
        /// LocationName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? LocationName { get; set;}

        /// <summary>
        /// LocationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string LocationNaturalId { get; set; } = null!;

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        #region IAPEXEntity Interface
        /// <summary>
        /// EntityId
        /// </summary>
        [NotMapped]
        public string EntityId => WarehouseId;

        /// <summary>
        /// EntityName
        /// </summary>
        [NotMapped]
        public string? EntityName => LocationName;

        /// <summary>
        /// EntityFriendlyIdentifier
        /// </summary>
        [NotMapped]
        public string EntityFriendlyIdentifier => LocationNaturalId;

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>Cloned copy</returns>
        public object Clone()
        {
            var copy = new Warehouse();
            copy.WarehouseId = WarehouseId;
            copy.LocationId = LocationId;
            copy.LocationName = LocationName;
            copy.LocationNaturalId = LocationNaturalId;
            copy.UserNameSubmitted = UserNameSubmitted;
            copy.Timestamp = Timestamp;
            return copy;
        }
        #endregion
    }
}
