﻿using System.Xml.Linq;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// PlanetCOGCHistory
    /// </summary>
    public class PlanetCOGCProgram : IValidation, ICloneable
    {
        /// <summary>
        /// PlanetCOGCProgramId: {PlanetId}-{StartTimestamp}
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(64, MinimumLength = 36)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string PlanetCOGCProgramId { get; set; } = null!;

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(32, MinimumLength = 16)]
        public string? Type { get; set; }

        /// <summary>
        /// StartTime
        /// </summary>
        public DateTime StartTime { get; set; }

        /// <summary>
        /// EndTime
        /// </summary>
        public DateTime EndTime { get; set; }

        /// <summary>
        /// PlanetId (Parent)
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// Planet (Parent)
        /// </summary>
        [JsonIgnore]
        public Planet? Planet { get; set; }

        /// <summary>
        /// CustomValidation
        /// </summary>
        /// <param name="Errors">Errors</param>
        /// <param name="Context">Context</param>
        public override void CustomValidation(ref List<string> Errors, string Context)
        {
            base.CustomValidation(ref Errors, Context);

            if (Type != null && !Constants.ValidCOGCProgramTypes.Contains(Type))
            {
                Errors.Add($"{Context}.Type with value '{Type}' is not a valid COGC Program Type");
            }
        }

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new PlanetCOGCProgram()
            {
                PlanetCOGCProgramId = PlanetCOGCProgramId,
                Type = Type,
                StartTime = StartTime,
                EndTime = EndTime,
                PlanetId = PlanetId,
            };
        }
    }
}
