﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// JumpCacheRouteJump
    /// </summary>
    [Index(nameof(SourceSystemId))]
    [Index(nameof(DestinationSystemId))]
    public class JumpCacheRouteJump : IValidation
    {
        /// <summary>
        /// JumpCacheRouteJumpId: JumpCacheId-SourceSystemId-DestinationSystemId
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(131)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string JumpCacheRouteJumpId { get; set; } = null!;

        /// <summary>
        /// SourceSystemId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string SourceSystemId { get; set; } = null!;

        /// <summary>
        /// SourceSystemName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? SourceSystemName { get; set; }

        /// <summary>
        /// SourceSystemNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string SourceSystemNaturalId { get; set; } = null!;

        /// <summary>
        /// DestinationSystemId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string DestinationSystemId { get; set; } = null!;

        /// <summary>
        /// DestinationSystemName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string? DestinationSystemName { get; set; }

        /// <summary>
        /// DestinationNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string DestinationNaturalId { get; set; } = null!;

        /// <summary>
        /// Distance
        /// </summary>
        [Range(0, double.MaxValue)]
        public double Distance { get; set; }

        /// <summary>
        /// JumpCacheId (parent)
        /// </summary>
        [JsonIgnore]
        [StringLength(65, MinimumLength = 65)]
        public string JumpCacheId { get; set; } = null!;

        /// <summary>
        /// JumpCache (parent)
        /// </summary>
        [JsonIgnore]
        public virtual JumpCache JumpCache { get; set; } = null!;
    }
}
