﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// ComexExchange
    /// </summary>
    [Index(nameof(Code))]
    public class ComexExchange : IValidation
    {
        /// <summary>
        /// Exchange Id
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ComexExchangeId { get; set; } = null!;

        /// <summary>
        /// Code
        /// </summary>
        [Ticker]
        [CustomStringLength]
        [StringLength(3, MinimumLength = 3)]
        public string Code { get; set; } = null!;

        /// <summary>
        /// Name of the exchange
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Decimals to show
        /// </summary>
        [Range(1, int.MaxValue)]
        public int Decimals { get; set; }

        /// <summary>
        /// NumericCode
        /// </summary>
        [Range(1, int.MaxValue)]
        public int NumericCode { get; set; }

        /// <summary>
        /// Exchange NaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string NaturalId { get; set; } = null!;

        /// <summary>
        /// SystemId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string SystemId { get; set; } = null!;

        /// <summary>
        /// SystemName
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string SystemName { get; set; } = null!;

        /// <summary>
        /// SystemNaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string SystemNaturalId { get; set; } = null!;

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }
    }
}
