﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// BuildingCost
    /// </summary>
    public class BuildingCost : IValidation, ICloneable
    {
        /// <summary>
        /// BuildingCostId
        /// </summary>
        [Key]
        [JsonIgnore]
        [StringLength(37, MinimumLength = 35)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BuildingCostId { get; set; } = "";

        /// <summary>
        /// The name of the material
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string Name { get; set; } = "";

        /// <summary>
        /// The ticker of the material
        /// </summary>
        [Ticker]
        [StringLength(Constants.TickerLengthMax, MinimumLength = Constants.TickerLengthMin)]
        public string Ticker { get; set; } = "";

        /// <summary>
        /// Weight of the material
        /// </summary>
        [Range(0.00001, 1000.0)]
        public double Weight { get; set; } = 0.0;

        /// <summary>
        /// Volume of the material
        /// </summary>
        [Range(0.00001, 1000.0)]
        public double Volume { get; set; } = 0.0;

        /// <summary>
        /// The amount of the material required
        /// </summary>
        [Range(1, 5000)]
        public int Amount { get; set; } = 0;

        /// <summary>
        /// BuildingId
        /// </summary>
        [APEXID]
        [JsonIgnore]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string BuildingId { get; set; } = null!;

        /// <summary>
        /// Building
        /// </summary>
        [JsonIgnore]
        public virtual Building Building { get; set; } = null!;

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new BuildingCost()
            {
                Name = Name,
                Ticker = Ticker,
                Weight = Weight,
                Volume = Volume,
                Amount = Amount,
                BuildingId = BuildingId,
                // Don't add parent Building
            };
        }
    }
}
