﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// PlanetSite
    /// </summary>
    [Index(nameof(PlanetId))]
    [Index(nameof(OwnerCode))]
    [Index(nameof(Type))]
    public class PlanetSite : IValidation
    {
        /// <summary>
        /// PlanetSiteId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetSiteId { get; set; } = null!;

        /// <summary>
        /// PlanetId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string PlanetId { get; set; } = null!;

        /// <summary>
        /// OwnerId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string OwnerId { get; set; } = null!;

        /// <summary>
        /// OwnerName
        /// </summary>
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string OwnerName { get; set; } = null!;

        /// <summary>
        /// OwnerCode: CompanyCode if player is active, null if liquidated, naturalId if planetary project
        /// </summary>
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string? OwnerCode { get; set; }

        /// <summary>
        /// Type
        /// </summary>
        [StringLength(64)]
        public string Type { get; set; } = null!;

        /// <summary>
        /// PlotNumber: -1 indicates GhostPlot
        /// </summary>
        [Range(-1, int.MaxValue)]
        public int PlotNumber { get; set; } = -1;

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// If this plot is a GhostPlot
        /// </summary>
        [NotMapped]
        public bool GhostPlot
        {
            get
            {
                return PlotNumber == -1;
            }
        }

        /// <summary>
        /// First 8 characters of PlotId
        /// </summary>
        [NotMapped]
        public string SiteId
        {
            get
            {
                return PlanetSiteId.Substring(0, 8);
            }
        }
    }
}
