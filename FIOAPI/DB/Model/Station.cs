﻿using FIOAPI.Caches;

namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Station
    /// </summary>
    public class Station : IValidation, IAPEXEntity
    {
        /// <summary>
        /// StationId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string StationId { get; set; } = null!;

        /// <summary>
        /// NaturalId
        /// </summary>
        [NaturalID]
        [StringLength(Constants.NaturalIDLengthMax, MinimumLength = Constants.NaturalIDLengthMin)]
        public string NaturalId { get; set; } = null!;

        /// <summary>
        /// Station name
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Currency Code
        /// </summary>
        [CurrencyCode]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string CurrencyCode { get; set; } = null!;

        /// <summary>
        /// Country Id
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string CountryId { get; set; } = null!;

        /// <summary>
        /// Country code
        /// </summary>
        [Ticker]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string CountryCode { get; set; } = null!;

        /// <summary>
        /// Country name
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string CountryName { get; set; } = null!;

        /// <summary>
        /// Governing entity id
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string GoverningEntityId { get; set; } = null!;

        /// <summary>
        /// Governing entity code
        /// </summary>
        [Ticker]
        [StringLength(Constants.EntityCodeLengthMax, MinimumLength = Constants.EntityCodeLengthMin)]
        public string GoverningEntityCode { get; set; } = null!;

        /// <summary>
        /// Governing entity name
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string GoverningEntityName { get; set; } = null!;

        /// <summary>
        /// Warehouse Id
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? WarehouseId { get; set; }

        /// <summary>
        /// LocalMarketId
        /// </summary>
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        public string? LocalMarketId { get; set; }

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        #region IAPEXEntity
        /// <summary>
        /// EntityId
        /// </summary>
        [NotMapped]
        public string EntityId
        {
            get
            {
                return StationId;
            }
        }

        /// <summary>
        /// EntityName
        /// </summary>
        [NotMapped]
        public string? EntityName
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// EntityFriendlyIdentifier
        /// </summary>
        [NotMapped]
        public string EntityFriendlyIdentifier
        {
            get
            {
                return NaturalId;
            }
        }
        #endregion

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy</returns>
        public object Clone()
        {
            return new Station()
            {
                StationId = StationId,
                NaturalId = NaturalId,
                Name = Name,
                CurrencyCode = CurrencyCode,
                CountryId = CountryId,
                CountryCode = CountryCode,
                CountryName = CountryName,
                GoverningEntityId = GoverningEntityId,
                GoverningEntityCode = GoverningEntityCode,
                GoverningEntityName = GoverningEntityName
            };
        }
    }
}
