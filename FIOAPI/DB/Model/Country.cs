﻿namespace FIOAPI.DB.Model
{
    /// <summary>
    /// Country
    /// </summary>
    [Index(nameof(Code), IsUnique = true)]
    public class Country : IValidation, IAPEXEntity
    {
        /// <summary>
        /// CountryId
        /// </summary>
        [Key]
        [APEXID]
        [StringLength(Constants.APEXIDLength, MinimumLength = Constants.APEXIDLength)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string CountryId { get; set; } = null!;

        /// <summary>
        /// Name of the country
        /// </summary>
        [StringLength(Constants.EntityNameLengthMax, MinimumLength = Constants.EntityNameLengthMin)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Country code
        /// </summary>
        [Uppercase]
        [StringLength(2, MinimumLength = 2)]
        public string Code { get; set; } = null!;

        /// <summary>
        /// Currency numeric code
        /// </summary>
        [Range(1,5)]
        public int NumericCode { get; set; }

        /// <summary>
        /// Currency code
        /// </summary>
        [Uppercase]
        [StringLength(Constants.CurrencyLengthMax, MinimumLength = Constants.CurrencyLengthMin)]
        public string CurrenyCode { get; set; } = null!;

        /// <summary>
        /// Name of the currency
        /// </summary>
        [StringLength(32, MinimumLength = 3)]
        public string CurrencyName { get; set; } = null!;

        /// <summary>
        /// Decimals to show for this currency
        /// </summary>
        public int CurrencyDecimals { get; set; }

        /// <summary>
        /// The name of the user that submitted this
        /// </summary>
        [Lowercase]
        [JsonIgnore]
        [StringLength(Constants.UserNameLengthMax, MinimumLength = Constants.UserNameLengthMin)]
        public string UserNameSubmitted { get; set; } = null!;

        /// <summary>
        /// Timestamp
        /// </summary>
        [ValidTimestamp]
        public DateTime Timestamp { get; set; }

        #region IAPEXEntity Interface
        /// <summary>
        /// EntityId
        /// </summary>
        [NotMapped]
        public string EntityId
        {
            get
            {
                return CountryId;
            }
        }

        /// <summary>
        /// EntityName
        /// </summary>
        [NotMapped]
        public string? EntityName
        {
            get
            {
                return Name;
            }
        }

        /// <summary>
        /// EntityFriendlyIdentifier
        /// </summary>
        [NotMapped]
        public string EntityFriendlyIdentifier
        {
            get
            {
                return Code;
            }
        }
        #endregion

        /// <summary>
        /// Clone
        /// </summary>
        /// <returns>A copy of this object</returns>
        public object Clone()
        {
            var copy = new Country();
            copy.CountryId = CountryId;
            copy.Name = Name;
            copy.Code = Code;
            copy.NumericCode = NumericCode;
            copy.CurrenyCode = CurrenyCode;
            copy.CurrencyName = CurrencyName;
            copy.CurrencyDecimals = CurrencyDecimals;
            copy.UserNameSubmitted = UserNameSubmitted;
            copy.Timestamp = Timestamp;
            return copy;
        }
    }
}
