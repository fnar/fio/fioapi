﻿using System.Diagnostics;

namespace FIOAPI.DB
{
    /// <summary>
    /// SqliteDataContext
    /// </summary>
    public class SqliteDataContext : FIODBContext
    {
        /// <summary>
        /// OnConfiguring override
        /// </summary>
        /// <param name="options">options</param>
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            LogDebug(options);

            int CommandTimeout = 30;
            if (Debugger.IsAttached)
            {
                CommandTimeout = 300;
            }

            options = options
                .UseSqlite(Globals.DatabaseConnectionString, o => o
                    .UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery)
                    .CommandTimeout(CommandTimeout))
                .UseSnakeCaseNamingConvention();

            if (Globals.DebugDatabase)
            {
                options
                    .EnableSensitiveDataLogging()
                    .LogTo(msg => Log.Logger.Debug(msg));
            }
        }
    }
}
