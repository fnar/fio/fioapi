﻿namespace FIOAPI.DB
{
    /// <summary>
    /// The main DBContext
    /// </summary>
    public class FIODBContext : DbContext
    {
        /// <summary>
        /// APEXUser
        /// </summary>
        public DbSet<Model.APEXUser> APEXUsers { get; set; }

        /// <summary>
        /// APIKeys
        /// </summary>
        public DbSet<Model.APIKey> APIKeys { get; set; }

        /// <summary>
        /// Buildings
        /// </summary>
        public DbSet<Model.Building> Buildings { get; set; }

        /// <summary>
        /// BuildingCosts
        /// </summary>
        public DbSet<Model.BuildingCost> BuildingCosts { get; set; }

        /// <summary>
        /// BuildingRecipes
        /// </summary>
        public DbSet<Model.BuildingRecipe> BuildingRecipes { get; set; }

        /// <summary>
        /// BuildingRecipeInputs
        /// </summary>
        public DbSet<Model.BuildingRecipeInput> BuildingRecipeInputs { get; set; }

        /// <summary>
        /// BuildingRecipeOutputs
        /// </summary>
        public DbSet<Model.BuildingRecipeOutput> BuildingRecipeOutputs { get; set; }

        /// <summary>
        /// ChatChannels
        /// </summary>
        public DbSet<Model.ChatChannel> ChatChannels { get; set; }

        /// <summary>
        /// ChatMessages
        /// </summary>
        public DbSet<Model.ChatMessage> ChatMessages { get; set; }

        /// <summary>
        /// ComexExchanges
        /// </summary>
        public DbSet<Model.ComexExchange> ComexExchanges { get; set; }

        /// <summary>
        /// Contracts
        /// </summary>
        public DbSet<Model.Contract> Contracts { get; set; }

        /// <summary>
        /// ContractConditions
        /// </summary>
        public DbSet<Model.ContractCondition> ContractConditions { get; set; }

        /// <summary>
        /// Companies
        /// </summary>
        public DbSet<Model.Company> Companies { get; set; }

        /// <summary>
        /// CompanyPlanets
        /// </summary>
        public DbSet<Model.CompanyPlanet> CompanyPlanets { get; set; }

        /// <summary>
        /// CompanyOffices
        /// </summary>
        public DbSet<Model.CompanyOffice> CompanyOffices { get; set; }

        /// <summary>
        /// Countries
        /// </summary>
        public DbSet<Model.Country> Countries { get; set; }

        /// <summary>
        /// CXEntries
        /// </summary>
        public DbSet<Model.CXEntry> CXEntries { get; set; }

        /// <summary>
        /// CXBuyOrders
        /// </summary>
        public DbSet<Model.CXBuyOrder> CXBuyOrders { get; set; }

        /// <summary>
        /// CXSellOrders
        /// </summary>
        public DbSet<Model.CXSellOrder> CXSellOrders { get; set; }

        /// <summary>
        /// CXPCs
        /// </summary>
        public DbSet<Model.CXPC> CXPCs { get; set; }

        /// <summary>
        /// CXPCEntries
        /// </summary>
        public DbSet<Model.CXPCEntry> CXPCEntries { get; set; }

        /// <summary>
        /// CXOSs
        /// </summary>
        public DbSet<Model.CXOS> CXOSs { get; set; }

        /// <summary>
        /// CXOSTrades
        /// </summary>
        public DbSet<Model.CXOSTrade> CXOSTrades { get; set; }

        /// <summary>
        /// Experts
        /// </summary>
        public DbSet<Model.Expert> Experts { get; set; }

        /// <summary>
        /// ExpertEntries
        /// </summary>
        public DbSet<Model.ExpertEntry> ExpertEntries { get; set; }

        /// <summary>
        /// Flights
        /// </summary>
        public DbSet<Model.Flight> Flights { get; set; }

        /// <summary>
        /// FlightSegments
        /// </summary>
        public DbSet<Model.FlightSegment> FlightSegments { get; set; }

        /// <summary>
        /// FX
        /// </summary>
        public DbSet<Model.FX> FX { get; set; }

        /// <summary>
        /// FXBuyOrders
        /// </summary>
        public DbSet<Model.FXBuyOrder> FXBuyOrders { get; set; }

        /// <summary>
        /// FXSellOrders
        /// </summary>
        public DbSet<Model.FXSellOrder> FXSellOrders { get; set; }

        /// <summary>
        /// Groups
        /// </summary>
        public DbSet<Model.Group> Groups { get; set; }

        /// <summary>
        /// GroupAdmins
        /// </summary>
        public DbSet<Model.GroupAdmin> GroupAdmins { get; set; }

        /// <summary>
        /// GroupPendingInvites
        /// </summary>
        public DbSet<Model.GroupPendingInvite> GroupPendingInvites { get; set; }

        /// <summary>
        /// GroupUsers
        /// </summary>
        public DbSet<Model.GroupUser> GroupUsers { get; set; }

        /// <summary>
        /// JumpCache
        /// </summary>
        public DbSet<Model.JumpCache> JumpCache { get; set; }

        /// <summary>
        /// JumpCacheRouteJumps
        /// </summary>
        public DbSet<Model.JumpCacheRouteJump> JumpCacheRouteJumps { get; set; }

        /// <summary>
        /// LocalMarketAds
        /// </summary>
        public DbSet<Model.LocalMarketAd> LocalMarketAds { get; set; }

        /// <summary>
        /// Materials
        /// </summary>
        public DbSet<Model.Material> Materials { get; set; }

        /// <summary>
        /// MaterialCategories
        /// </summary>
        public DbSet<Model.MaterialCategory> MaterialCategories { get; set; }

        /// <summary>
        /// Permissions
        /// </summary>
        public DbSet<Model.Permission> Permissions { get; set; }

        /// <summary>
        /// Planets
        /// </summary>
        public DbSet<Model.Planet> Planets { get; set; }

        /// <summary>
        /// ProductionLines
        /// </summary>
        public DbSet<Model.ProductionLine> ProductionLines { get; set; }

        /// <summary>
        /// ProductionLineOrders
        /// </summary>
        public DbSet<Model.ProductionLineOrder> ProductionLineOrders { get; set; }

        /// <summary>
        /// COGCPrograms
        /// </summary>
        public DbSet<Model.PlanetCOGCProgram> COGCPrograms { get; set; }

        /// <summary>
        /// PlanetSites
        /// </summary>
        public DbSet<Model.PlanetSite> PlanetSites { get; set; } 

        /// <summary>
        /// PlanetResources
        /// </summary>
        public DbSet<Model.PlanetResource> PlanetResources { get; set; }

        /// <summary>
        /// PlanetWorkforceFees
        /// </summary>
        public DbSet<Model.PlanetWorkforceFee> PlanetWorkforceFees { get; set; }

        /// <summary>
        /// PlanetPopulationReports
        /// </summary>
        public DbSet<Model.PlanetPopulationReport> PlanetPopulationReports { get; set; }

        /// <summary>
        /// PlanetPopulationProjects
        /// </summary>
        public DbSet<Model.PlanetPopulationProject> PlanetPopulationProjects { get; set; }

        /// <summary>
        /// PlanetProjectUpgradeCosts
        /// </summary>
        public DbSet<Model.PlanetPopulationProjectUpkeep> PlanetPopulationProjectUpkeeps { get; set; }

        /// <summary>
        /// Ships
        /// </summary>
        public DbSet<Model.Ship> Ships { get; set; }

        /// <summary>
        /// ShipRepairMaterials
        /// </summary>
        public DbSet<Model.ShipRepairMaterial> ShipRepairMaterials { get; set; }

        /// <summary>
        /// Sites
        /// </summary>
        public DbSet<Model.Site> Sites { get; set; }

        /// <summary>
        /// SiteBuildings
        /// </summary>
        public DbSet<Model.SiteBuilding> SiteBuildings { get; set; }

        /// <summary>
        /// Sectors
        /// </summary>
        public DbSet<Model.Sector> Sectors { get; set; }

        /// <summary>
        /// SubSectors
        /// </summary>
        public DbSet<Model.SubSector> SubSectors { get; set; }
        
        /// <summary>
        /// Stars
        /// </summary>
        public DbSet<Model.Star> Stars { get; set; }

        /// <summary>
        /// Systems
        /// </summary>
        public DbSet<Model.System> Systems { get; set; }

        /// <summary>
        /// Storages
        /// </summary>
        public DbSet<Model.Storage> Storages { get; set; }

        /// <summary>
        /// StorageItems
        /// </summary>
        public DbSet<Model.StorageItem> StorageItems { get; set; }

        /// <summary>
        /// Users
        /// </summary>
        public DbSet<Model.User> Users { get; set; }

        /// <summary>
        /// Registrations
        /// </summary>
        public DbSet<Model.Registration> Registrations { get; set; }

        /// <summary>
        /// SimulationData
        /// </summary>
        public DbSet<Model.SimulationData> SimulationData { get; set; }

        /// <summary>
        /// Stations
        /// </summary>
        public DbSet<Model.Station> Stations { get; set; }

        /// <summary>
        /// UserWarehouses
        /// </summary>
        public DbSet<Model.UserWarehouse> UserWarehouses { get; set; }

        /// <summary>
        /// Warehouses
        /// </summary>
        public DbSet<Model.Warehouse> Warehouses { get; set; }

        /// <summary>
        /// Workforces
        /// </summary>
        public DbSet<Model.Workforce> Workforces { get; set; }

        /// <summary>
        /// WorkforceNeed
        /// </summary>
        public DbSet<Model.WorkforceNeed> WorkforceNeeds { get; set; }

        /// <summary>
        /// WorkforceRequirements
        /// </summary>
        public DbSet<Model.WorkforceRequirement> WorkforceRequirements { get; set; }

        /// <summary>
        /// If this context should restrict saves (used with DBWriter)
        /// </summary>
        public bool ShouldRestrictSaves { get; set; } = false;

        /// <summary>
        /// Parameterless constructor
        /// </summary>
        public FIODBContext()
        { }

        /// <summary>
        /// DbContextOptions constructor so ASP.Net Core can inject the configuration for tests
        /// </summary>
        /// <param name="options">Options</param>
        public FIODBContext(DbContextOptions<FIODBContext> options)
        : base(options)
        { }


        /// <summary>
        /// Retrieves a new context
        /// </summary>
        /// <param name="bShouldRestrictSaves">If saves should be restricted</param>
        /// <returns>The appropriate DBContext</returns>
        /// <exception cref="InvalidOperationException">If the database type is not defined</exception>
        public static FIODBContext GetNewContext(bool bShouldRestrictSaves = false)
        {
            switch (Globals.DatabaseType)
            {
                case DatabaseType.Postgres:
                    return new PostgresDataContext()
                    {
                        ShouldRestrictSaves = bShouldRestrictSaves
                    };
                case DatabaseType.Sqlite:
                    return new SqliteDataContext()
                    {
                        ShouldRestrictSaves = bShouldRestrictSaves
                    };
                default:
                    throw new InvalidOperationException("No database type defined.");
            }
        }

        /// <summary>
        /// LogDebug DbContext hook
        /// </summary>
        /// <param name="options">options</param>
        public static void LogDebug(DbContextOptionsBuilder options)
        {
            if (Globals.DebugDatabase)
            {
                options.LogTo(Console.WriteLine, LogLevel.Information);
            }
        }

        /// <summary>
        /// ApplyOptions hooks
        /// </summary>
        /// <param name="options">options</param>
        /// <returns>DbContextOptionsBuilder</returns>
        /// <exception cref="InvalidOperationException">If the database type is not defined</exception>
        public static DbContextOptionsBuilder ApplyOptions(DbContextOptionsBuilder options)
        {
            LogDebug(options);
            switch (Globals.DatabaseType)
            {
                case DatabaseType.Postgres:
                    return options.UseNpgsql(Globals.DatabaseConnectionString, o => o.CommandTimeout(Globals.CommandTimeout));
                case DatabaseType.Sqlite:
                    return options.UseSqlite(Globals.DatabaseConnectionString, o => o.CommandTimeout(Globals.CommandTimeout));
                default:
                    throw new InvalidOperationException("No database type defined.");
            }
        }

#region Override DbContext
        /// <summary>
        /// Overrides SaveChanges to enforce save restrictions
        /// </summary>
        /// <returns>Number of changes made</returns>
        public override int SaveChanges() => SaveChanges(true);

        /// <summary>
        /// Overrides SaveChanges to enforce save restrictions
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess">acceptAllChangesOnSuccess</param>
        /// <returns>Number of changes made</returns>
        /// <exception cref="InvalidOperationException">If saves are not permitted</exception>
        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            if (ShouldRestrictSaves)
            {
                throw new InvalidOperationException("Saves not permitted");
            }

            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        /// <summary>
        /// Overrides SaveChangesAsync to enforce save restrictions
        /// </summary>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <returns>Number of changes made</returns>
        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default) => SaveChangesAsync(true, cancellationToken);

        /// <summary>
        /// Overrides SaveChangesAsync to enforce save restrictions
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess">acceptAllChangesOnSuccess</param>
        /// <param name="cancellationToken">cancellationToken</param>
        /// <returns>Number of changes made</returns>
        /// <exception cref="InvalidOperationException">If saves are not permitted</exception>
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            if (ShouldRestrictSaves)
            {
                throw new InvalidOperationException("Saves not permitted");
            }

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }
#endregion
    }
}
