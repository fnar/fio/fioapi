﻿using System.Collections.Generic;
using static FIOAPI.Constants;

namespace FIOAPI
{
    /// <summary>
    /// Constants used across FIOAPI
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// List of valid currencies
        /// </summary>
        public static readonly List<string> ValidCurrencies = new List<string>
        {
            "AIC", 
            "CIS", 
            "ICA", 
            "NCC", 
            "ECD"
        };

        /// <summary>
        /// CurrencyLengthMin
        /// </summary>
        public const int CurrencyLengthMin = 3;

        /// <summary>
        /// CurrencyLengthMax
        /// </summary>
        public const int CurrencyLengthMax = 3;

        /// <summary>
        /// List of valid resource types
        /// </summary>
        public static readonly List<string> ValidPlanetResourceTypes = new List<string>
        {
            "MINERAL", 
            "GASEOUS", 
            "LIQUID",
        };

        /// <summary>
        /// PlanetResourceTypeLengthMin
        /// </summary>
        public const int PlanetResourceTypeLengthMin = 6;

        /// <summary>
        /// PlanetResourceTypeLengthMax
        /// </summary>
        public const int PlanetResourceTypeLengthMax = 7;

        /// <summary>
        /// LocalMarketAdType
        /// </summary>
        public enum LocalMarketAdType
        {
            /// <summary>
            /// Shipping
            /// </summary>
            Shipping,

            /// <summary>
            /// Buying
            /// </summary>
            Buying,
            
            /// <summary>
            /// Selling
            /// </summary>
            Selling
        }

        /// <summary>
        /// List of valid localmarket types
        /// </summary>
        public static readonly Dictionary<LocalMarketAdType, string> ValidLocalMarketTypes = new()
        {
            { LocalMarketAdType.Shipping, "COMMODITY_SHIPPING" },
            { LocalMarketAdType.Buying, "COMMODITY_BUYING" },
            { LocalMarketAdType.Selling, "COMMODITY_SELLING" }
        };

        /// <summary>
        /// LocalMarketTypeLengthMin
        /// </summary>
        public const int LocalMarketTypeLengthMin = 16;

        /// <summary>
        /// LocalMarketTypeLengthMax
        /// </summary>
        public const int LocalMarketTypeLengthMax = 18;

        /// <summary>
        /// List of valid ratings
        /// </summary>
        public static readonly List<string> ValidRatings = new List<string>
        {
            "PENDING", "A", "B", "C", "D", "E", "F", "UNRATED"
        };

        /// <summary>
        /// RatingLengthMin
        /// </summary>
        public const int RatingLengthMin = 1;

        /// <summary>
        /// RatingLengthMax
        /// </summary>
        public const int RatingLengthMax = 7;

        /// <summary>
        /// ValidCXPCIntervals
        /// </summary>
        public static readonly List<string> ValidCXPCIntervals = new List<string>
        {
            "MINUTE_FIVE",
            "MINUTE_FIFTEEN",
            "MINUTE_THIRTY",
            "HOUR_ONE",
            "HOUR_TWO",
            "HOUR_FOUR",
            "HOUR_SIX",
            "HOUR_TWELVE",
            "DAY_ONE",
            "DAY_THREE"
        };

        /// <summary>
        /// CXPCIntervalLengthMin
        /// </summary>
        public const int CXPCIntervalLengthMin = 7;

        /// <summary>
        /// CXPCIntervalLengthMax
        /// </summary>
        public const int CXPCIntervalLengthMax = 14;

        /// <summary>
        /// Enum for Store Types
        /// </summary>
        public enum StoreType
        {
            /// <summary>
            /// Base Inventory
            /// </summary>
            Base,

            /// <summary>
            /// Warehouse Inventory
            /// </summary>
            Warehouse,

            /// <summary>
            /// Ship Inventory
            /// </summary>
            Ship,

            /// <summary>
            /// STL Fuel Inventory
            /// </summary>
            STLFuel,

            /// <summary>
            /// FTL Fuel Inventory
            /// </summary>
            FTLFuel
        }

        /// <summary>
        /// StoreTypeEnumToStoreType
        /// </summary>
        public static readonly Dictionary<StoreType, string> StoreTypeEnumToStoreType = new Dictionary<StoreType, string>
        {
            { StoreType.Base, "STORE" },
            { StoreType.Ship, "SHIP_STORE" },
            { StoreType.Warehouse, "WAREHOUSE_STORE" },
            { StoreType.STLFuel, "STL_FUEL_STORE" },
            { StoreType.FTLFuel, "FTL_FUEL_STORE" }
        };

        /// <summary>
        /// StoreTypeLengthMin
        /// </summary>
        public const int StoreTypeLengthMin = 5;

        /// <summary>
        /// StoreTypeLengthMax
        /// </summary>
        public const int StoreTypeLengthMax = 15;

        /// <summary>
        /// The minimum APEX timestamp which is Jan 1, 2000
        /// </summary>
        public static readonly long MinimumAPEXTimestamp = 946706400000;

        /// <summary>
        /// The maximum APEX timestamp which is Jan 1, 2100
        /// @TODO: Update this when the year 2100 comes around
        /// </summary>
        public static readonly long MaximumAPEXTimestamp = 4102466400000;

        /// <summary>
        /// SupportedPositiveNumberTypes
        /// </summary>
        public static readonly List<string> SupportedPositiveNumberTypes = new()
        {
            nameof(Double),
            nameof(Single),
            nameof(Int32),
            nameof(Int64),
            nameof(Int16),
            nameof(SByte)
        };

        /// <summary>
        /// BuildingCategoryType
        /// </summary>
        public enum BuildingCategoryType
        {
            /// <summary>
            /// Agriculture
            /// </summary>
            Agriculture,

            /// <summary>
            /// Chemistry
            /// </summary>
            Chemistry,

            /// <summary>
            /// Construction
            /// </summary>
            Construction,

            /// <summary>
            /// Electronics
            /// </summary>
            Electronics,

            /// <summary>
            /// FoodIndustries
            /// </summary>
            FoodIndustries,

            /// <summary>
            /// FuelRefining
            /// </summary>
            FuelRefining,

            /// <summary>
            /// Manufacturing
            /// </summary>
            Manufacturing,

            /// <summary>
            /// Metallurgy
            /// </summary>
            Metallurgy,

            /// <summary>
            /// ResourceExtraction
            /// </summary>
            ResourceExtraction
        }

        /// <summary>
        /// ValidBuildingCategories
        /// </summary>
        public static readonly Dictionary<string, BuildingCategoryType> BuildingCategoryToType = new()
        {
            { "AGRICULTURE", BuildingCategoryType.Agriculture },
            { "CHEMISTRY", BuildingCategoryType.Chemistry },
            { "CONSTRUCTION", BuildingCategoryType.Construction },
            { "ELECTRONICS", BuildingCategoryType.Electronics },
            { "FOOD_INDUSTRIES", BuildingCategoryType.FoodIndustries },
            { "FUEL_REFINING", BuildingCategoryType.FuelRefining },
            { "MANUFACTURING", BuildingCategoryType.Manufacturing },
            { "METALLURGY", BuildingCategoryType.Metallurgy },
            { "RESOURCE_EXTRACTION", BuildingCategoryType.ResourceExtraction }
        };

        /// <summary>
        /// BuildingCategoryLengthMin
        /// </summary>
        public const int BuildingCategoryLengthMin = 9;

        /// <summary>
        /// BuildingCategoryLengthMax
        /// </summary>
        public const int BuildingCategoryLengthMax = 19;

        /// <summary>
        /// ValidWorkforceLevels
        /// </summary>
        public static readonly List<string> ValidWorkforceLevels = new()
        {
            "PIONEER",
            "SETTLER",
            "TECHNICIAN",
            "ENGINEER",
            "SCIENTIST"
        };

        /// <summary>
        /// WorkforceLevelLengthMin
        /// </summary>
        public const int WorkforceLevelLengthMin = 7;

        /// <summary>
        /// WorkforceLevelLengthMax
        /// </summary>
        public const int WorkforceLevelLengthMax = 10;

        /// <summary>
        /// ValidCOGCProgramTypes
        /// </summary>
        public static readonly List<string> ValidCOGCProgramTypes = new()
        {
            "WORKFORCE_PIONEERS",
            "WORKFORCE_SETTLERS",
            "WORKFORCE_TECHNICIANS",
            "WORKFORCE_ENGINEERS",
            "WORKFORCE_SCIENTISTS",
            "ADVERTISING_AGRICULTURE",
            "ADVERTISING_CHEMISTRY",
            "ADVERTISING_CONSTRUCTION",
            "ADVERTISING_ELECTRONICS",
            "ADVERTISING_FOOD_INDUSTRIES",
            "ADVERTISING_FUEL_REFINING",
            "ADVERTISING_MANUFACTURING",
            "ADVERTISING_METALLURGY",
            "ADVERTISING_RESOURCE_EXTRACTION"
        };

        /// <summary>
        /// COGCProgramLengthMin
        /// </summary>
        public const int COGCProgramLengthMin = 18;

        /// <summary>
        /// COGCProgramLengthMax
        /// </summary>
        public const int COGCProgramLengthMax = 31;

        /// <summary>
        /// ValidWorkforceNeedCategories
        /// </summary>
        public static readonly List<string> ValidWorkforceNeedCategories = new()
        {
            "FOOD",
            "WATER",
            "HEALTH",
            "CLOTHING",
            "TOOLS"
        };

        /// <summary>
        /// WorkforceNeedCategoryLengthMin
        /// </summary>
        public const int WorkforceNeedCategoryLengthMin = 4;

        /// <summary>
        /// WorkforceNeedCategoryLengthMax
        /// </summary>
        public const int WorkforceNeedCategoryLengthMax = 8;

        /// <summary>
        /// ValidPlanetaryInfrastructureProjectTypes
        /// </summary>
        public static readonly List<string> ValidPlanetaryInfrastructureProjectTypes = new()
        {
            "SAFETY_STATION",
            "SECURITY_DRONE_POST",
            "EMERGENCY_CENTER",
            "INFIRMARY",
            "HOSPITAL",
            "WELLNESS_CENTER",
            "WILDLIFE_PARK",
            "ARCADES",
            "ART_CAFE",
            "ART_GALLERY",
            "THEATER",
            "PLANETARY_BROADCASTING_HUB",
            "LIBRARY",
            "UNIVERSITY"
        };

        /// <summary>
        /// PlanetaryInfrastructureProjectLengthMin
        /// </summary>
        public const int PlanetaryInfrastructureProjectLengthMin = 7;

        /// <summary>
        /// PlanetaryInfrastructureProjectLengthMax
        /// </summary>
        public const int PlanetaryInfrastructureProjectLengthMax = 26;

        /// <summary>
        /// ValidLeaderboardTypes
        /// </summary>
        public static readonly List<string> ValidLeaderboardTypes = new()
        {
            "ARC_LEVEL",
            "BASES",
            "HQ_LEVEL",
            "BUILDINGS",
            "SHIPS",
            "STL_OPERATING_TIME",
            "FTL_OPERATING_TIME",
            "WORKFORCE_TOTAL",
            "WORKFORCE_PIONEER",
            "WORKFORCE_SETTLER",
            "WORKFORCE_TECHNICIAN",
            "WORKFORCE_ENGINEER",
            "WORKFORCE_SCIENTIST",
            "FACTION_REPUTATION_TOTAL",
            "FACTION_REPUTATION_AI",
            "FACTION_REPUTATION_CI",
            "FACTION_REPUTATION_IC",
            "FACTION_REPUTATION_NC",
            "GOVERNOR_TERMS",
            "PRODUCTION"
        };

        /// <summary>
        /// LeaderboardTypesLengthMin
        /// </summary>
        public const int LeaderboardTypesLengthMin = 5;

        /// <summary>
        /// LeaderboardTypesLengthMax
        /// </summary>
        public const int LeaderboardTypesLengthMax = 24;

        /// <summary>
        /// ValidLeaderboardRanges
        /// </summary>
        public static readonly List<string> ValidLeaderboardRanges = new()
        {
            "DAYS_7",
            "DAYS_30",
            "DAYS_90",
            "DAYS_180"
        };

        /// <summary>
        /// LeaderboardRangesLengthMin
        /// </summary>
        public const int LeaderboardRangesLengthMin = 6;

        /// <summary>
        /// LeaderboardRangesLengthMax
        /// </summary>
        public const int LeaderboardRangesLengthMax = 8;

        /// <summary>
        /// PlanetaryProjects
        /// </summary>
        public enum PlanetaryProject
        {
            /// <summary>
            /// AdministrationCenter
            /// </summary>
            AdministrationCenter,

            /// <summary>
            /// ChamberOfGlobalCommerce
            /// </summary>
            ChamberOfGlobalCommerce,

            /// <summary>
            /// Warehouse
            /// </summary>
            LocalMarket,

            /// <summary>
            /// Warehouse
            /// </summary>
            Warehouse,

            /// <summary>
            /// Shipyard
            /// </summary>
            Shipyard,

            /// <summary>
            /// Population Infrastructure
            /// </summary>
            Population
        }

        /// <summary>
        /// FullPermission
        /// </summary>
        public static readonly Model.Permission FullPermission = new()
        {
            ShipInformation = true,
            ShipRepair = true,
            ShipFlight = true,
            ShipInventory = true,
            ShipFuelInventory = true,
            SitesLocation = true,
            SitesWorkforces = true,
            SitesExperts = true,
            SitesBuildings = true,
            SitesRepair = true,
            SitesReclaimable = true,
            SitesProductionLines = true,
            StorageLocation = true,
            StorageInformation = true,
            StorageItems = true,
            TradeContract = true,
            TradeCXOS = true,
            CompanyInfo = true,
            CompanyLiquidCurrency = true,
            CompanyHeadquarters = true,
            MiscShipmentTracking = true,
        };

        /// <summary>
        /// InvalidAPEXID
        /// </summary>
        public static readonly string InvalidAPEXID = "00000000000000000000000000000000";

        /// <summary>
        /// APEXIDLength
        /// </summary>
        public const int APEXIDLength = 32;

        /// <summary>
        /// NaturalIDLengthMin
        /// </summary>
        public const int NaturalIDLengthMin = 3;

        /// <summary>
        /// NaturalIDLengthMax
        /// </summary>
        public const int NaturalIDLengthMax = 7;

        /// <summary>
        /// UserNameLengthMin
        /// </summary>
        public const int UserNameLengthMin = 3;

        /// <summary>
        /// UserNameLengthMax
        /// </summary>
        public const int UserNameLengthMax = 32;

        /// <summary>
        /// EntityNameLengthMin
        /// </summary>
        public const int EntityNameLengthMin = 2;

        /// <summary>
        /// EntityNameLengthMax
        /// </summary>
        public const int EntityNameLengthMax = 64;

        /// <summary>
        /// EntityCodeLengthMin
        /// </summary>
        public const int EntityCodeLengthMin = 1;

        /// <summary>
        /// EntityCodeLengthMax
        /// </summary>
        public const int EntityCodeLengthMax = 4;

        /// <summary>
        /// FXTickerLengthMin
        /// </summary>
        public const int FXTickerLengthMin = 7;

        /// <summary>
        /// FXTickerLengthMax
        /// </summary>
        public const int FXTickerLengthMax = 7;

        /// <summary>
        /// TickerLengthMin
        /// </summary>
        public const int TickerLengthMin = 1;

        /// <summary>
        /// TickerLengthMax
        /// </summary>
        public const int TickerLengthMax = 4;

        /// <summary>
        /// Mapping of type string to PlanetaryProject enum
        /// </summary>
        public static readonly Dictionary<string, PlanetaryProject> TypeToPlanetaryProject = new()
        {
            { "ADM", PlanetaryProject.AdministrationCenter },
            { "COGC", PlanetaryProject.ChamberOfGlobalCommerce },
            { "LOCM", PlanetaryProject.LocalMarket },
            { "WAR", PlanetaryProject.Warehouse },
            { "SHY", PlanetaryProject.Shipyard },
            { "POP", PlanetaryProject.Population }
        };

        /// <summary>
        /// PlanetaryProjectLengthMin
        /// </summary>
        public const int PlanetaryProjectLengthMin = 3;

        /// <summary>
        /// PlanetaryProjectLengthMax
        /// </summary>
        public const int PlanetaryProjectLengthMax = 4;

        /// <summary>
        /// NotFertile
        /// </summary>
        public const double NotFertile = -1.0;

        /// <summary>
        /// LowGravityThreshold
        /// </summary>
        public const double LowGravityThreshold = 0.25;

        /// <summary>
        /// HighGravityThreshold
        /// </summary>
        public const double HighGravityThreshold = 2.5;

        /// <summary>
        /// LowPressureThreshold
        /// </summary>
        public const double LowPressureThreshold = 0.25;

        /// <summary>
        /// HighPressureThreshold
        /// </summary>
        public const double HighPressureThreshold = 2.0;

        /// <summary>
        /// LowTemperatureThreshold
        /// </summary>
        public const double LowTemperatureThreshold = -25.0;

        /// <summary>
        /// HighTemperatureThreshold
        /// </summary>
        public const double HighTemperatureThreshold = 75.0;

        /// <summary>
        /// STLFuelWeight
        /// </summary>
        public const double STLFuelWeight = 0.06;

        /// <summary>
        /// STLFuelVolume
        /// </summary>
        public const double STLFuelVolume = 0.06;

        /// <summary>
        /// FTLFuelWeight
        /// </summary>
        public const double FTLFuelWeight = 0.05;

        /// <summary>
        /// FTLFuelVolume
        /// </summary>
        public const double FTLFuelVolume = 0.01;
    }
}
