﻿namespace FIOAPI.Services
{
    /// <summary>
    /// UserService
    /// </summary>
    public static class UserService
    {
        /// <summary>
        /// GetClaims
        /// </summary>
        /// <param name="user">user</param>
        /// <param name="HasWritePermission">If the auth has write permission</param>
        /// <returns>List of claims</returns>
        public static List<Claim> GetClaims(Model.User? user, bool HasWritePermission = true)
        {
            var claims = new List<Claim>();
            if (user != null)
            {
                claims.Add(new Claim(ClaimTypes.Name, user.DisplayUserName));
                claims.Add(new Claim("username", user.UserName));
                claims.Add(new Claim("usertype", HasWritePermission ? "write" : "read"));
                if (user.IsAdmin)
                {
                    claims.Add(new Claim("admintype", HasWritePermission ? "write" : "read"));
                }

                if (user.IsBot)
                {
                    claims.Add(new Claim("bot", "read"));
                }
            }

            return claims;
        }

        /// <summary>
        /// GetUserClaims
        /// </summary>
        /// <param name="user">user</param>
        /// <param name="HasWritePermissions">If the auth has write permissions</param>
        /// <returns>List of claims</returns>
        public static List<Claim> GetUserClaims(Model.User user, bool HasWritePermissions = true)
        {
            return GetClaims(user, HasWritePermissions);
        }

        /// <summary>
        /// GetUserClaims
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="HasWritePermissions">If the auth has write permissions</param>
        /// <returns>List of claims</returns>
        public static List<Claim> GetUserClaims(string username, bool HasWritePermissions = true)
        {
            return GetClaims(GetUser(username), HasWritePermissions);
        }

        /// <summary>
        /// GetUser
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="db">db</param>
        /// <returns>User?</returns>
        public static Model.User? GetUser(string username, FIODBContext? db = null)
        {
            username = username.ToLowerInvariant();
            if (db == null)
            {
                using (var reader = DBAccess.GetReader())
                {
                    var user = reader.DB.Users.FirstOrDefault(u => u.UserName == username);
                    return user;
                }
            }
            else
            {
                var user = db.Users.FirstOrDefault(u => u.UserName == username);
                return user;
            }
        }

        /// <summary>
        /// IsValidAsync
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="password">password</param>
        /// <returns>true if valid</returns>
        public static async Task<bool> IsValidAsync(string username, string password)
        {
            username = username.ToLowerInvariant();

            using (var reader = DBAccess.GetReader())
            {
                var user = await reader.DB.Users.FirstOrDefaultAsync(u => u.UserName == username);
                if (user != null)
                {
                    return SecurePasswordHasher.Verify(password, user.PasswordHash);
                }

                return false;
            }
        }
    }
}
