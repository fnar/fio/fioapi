﻿namespace FIOAPI.Services
{
    /// <summary>
    /// JwtSettings
    /// </summary>
    public class JwtSettings
    {
        /// <summary>
        /// Issuer
        /// </summary>
        public string Issuer { get; }

        /// <summary>
        /// Audience
        /// </summary>
        public string Audience { get; }

        /// <summary>
        /// Key
        /// </summary>
        public byte[] Key { get; }

        /// <summary>
        /// JwtSettings constructor
        /// </summary>
        /// <param name="Key">Key</param>
        /// <param name="Issuer">Issuer</param>
        /// <param name="Audience">Audience</param>
        public JwtSettings(byte[] Key, string Issuer, string Audience)
        {
            this.Key = Key;
            this.Issuer = Issuer;
            this.Audience = Audience;
        }

        /// <summary>
        /// TokenValidationParameters
        /// </summary>
        public TokenValidationParameters TokenValidationParameters => new TokenValidationParameters
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidIssuer = Issuer,
            ValidAudience = Audience,
            IssuerSigningKey = new SymmetricSecurityKey(Key)
        };

        /// <summary>
        /// FromConfiguration
        /// </summary>
        /// <param name="configuration">configuration</param>
        /// <returns>JwtSettings</returns>
        public static JwtSettings FromConfiguration(IConfiguration configuration)
        {
            // In real life this would come from configuration
            var key = new byte[100];

            var issuser = configuration["jwt:issuer"] ?? "defaultissuer";
            var audience = configuration["jwt:audience"] ?? "defaultaudience";
            var base64Key = configuration["jwt:key"];

            if (!string.IsNullOrEmpty(base64Key))
            {
                key = Convert.FromBase64String(base64Key);
            }
            else
            {
                RandomNumberGenerator.Create().GetBytes(key);
            }

            return new JwtSettings(key, issuser, audience);
        }

    }
}
