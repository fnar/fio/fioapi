﻿using System.Collections.Concurrent;
using System.Net;

namespace FIOAPI.Services
{
    /// <summary>
    /// IgnoreRepeatMiddleware - A middleware to 429 repeat requests within a constant time threshold
    /// </summary>
    public class IgnoreRepeatMiddleware
    {
        internal class RequestSignature
        {
            public string Route { get; set; } = null!;
            public string Source { get; set; } = null!;
            public string Method { get; set; } = null!;
            public string Body { get; set; } = null!;
            int Hash { get; set; }

            public RequestSignature(string Route, string Source, string Method, string Body)
            {
                this.Route = Route;
                this.Method = Method;

                if (SharedRateLimitAttribute.SharedRateLimits.Contains(new SharedRateLimitDef { Method = Method, Route = Route}))
                {
                    this.Source = "SHARED_ENDPOINT";
                }
                else
                {
                    this.Source = Source;
                }
                
                this.Body = Body;

                var hc = new HashCode();
                hc.Add(Route);
                hc.Add(Method);
                hc.Add(Source);
                hc.Add(Body);
                Hash = hc.ToHashCode();
            }

            // Overload equals to compare fields
            public bool RequestSignature_Equals(RequestSignature other)
            {
                if (other == null)
                    return false;
                if (ReferenceEquals(this, other))
                    return true;

                if (Route != other.Route)
                    return false;
                if (Source != other.Source)
                    return false;
                if (Method != other.Method)
                    return false;
                if (Body != other.Body)
                    return false;
                return true;
            }

            public override bool Equals(object? obj)
            {
                var sig = obj as RequestSignature;
                if (sig == null)
                    return false;
                else
                    return RequestSignature_Equals(sig);
            }

            public static bool operator ==(RequestSignature? lhs, RequestSignature? rhs)
            {
                if (lhs == null || rhs == null)
                    return Object.Equals(lhs, rhs);

                return lhs.RequestSignature_Equals(rhs);
            }

            public static bool operator !=(RequestSignature? lhs, RequestSignature? rhs)
            {
                if (lhs == null || rhs == null)
                    return !Object.Equals(lhs, rhs);

                return !(lhs.RequestSignature_Equals(rhs));
            }

            public override int GetHashCode()
            {
                return Hash;
            }
        }

        private const string XForwardedForHeaderkey = "X-Forwarded-For";

        private ConcurrentDictionary<RequestSignature, DateTime> Requests = new();

        private readonly List<string> SupportedMethods = new()
        {
            "POST", "PUT", "DELETE", "PATCH"
        };

        private static readonly TimeSpan IgnoreThreshold = TimeSpan.FromSeconds(1.1);

        private readonly RequestDelegate next;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="next">RequestDelete</param>
        public IgnoreRepeatMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// InvokeAsync
        /// </summary>
        /// <param name="context">HttpContext</param>
        /// <returns>An async task</returns>
        public async Task InvokeAsync(HttpContext context)
        {
            if (SupportedMethods.Contains(context.Request.Method))
            {
                var Now = DateTime.Now;

                string Route = context.Request.Path;
                string Source;

                string? username = context.GetUserName();
                if (username != null)
                {
                    Source = username;
                }
                else
                {
                    if (context.Request.Headers.TryGetValue(XForwardedForHeaderkey, out Microsoft.Extensions.Primitives.StringValues value))
                    {
                        Source = string.Join(";", value!);
                    }
                    else if (context.Connection.RemoteIpAddress != null)
                    {
                        Source = context.Connection.RemoteIpAddress.ToString();
                    }
                    else
                    {
                        Source = "Unknown";
                    }
                }

                string Method = context.Request.Method;
                string Body = await context.Request.GetBodyAsync();

                DateTime NextExpiry = Now.Add(IgnoreThreshold);
                RequestSignature Sig = new(Route, Source, Method, Body);

                DateTime Expiry = Requests.GetOrAdd(Sig, NextExpiry);
                if (Expiry < Now)
                {
                    Requests.Remove(Sig, out Expiry);
                    Expiry = Requests.GetOrAdd(Sig, NextExpiry);
                }
                if (Expiry != NextExpiry)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.TooManyRequests;
                    return;
                }
            }

            await next(context);
        }
    }

    /// <summary>
    /// IgnoreRepeatMiddlewareExtensions
    /// </summary>
    public static class IgnoreRepeatMiddlewareExtensions
    {
        /// <summary>
        /// UseIgnoreRepeatMiddleware
        /// </summary>
        /// <param name="builder">IApplicationBuilder</param>
        /// <returns>IApplicationBuilder</returns>
        public static IApplicationBuilder UseIgnoreRepeatMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<IgnoreRepeatMiddleware>();
        }
    }
}
