﻿using Serilog.Core;

namespace FIOAPI.Services
{
    /// <summary>
    /// APIKeyAuthDefaults
    /// </summary>
    public static class APIKeyAuthDefaults
    {
        /// <summary>
        /// AuthenticationScheme
        /// </summary>
        public const string AuthenticationScheme = "FIOAPIKey";
    }

    /// <summary>
    /// APIKeyAuthOptions
    /// </summary>
    public class APIKeyAuthOptions : AuthenticationSchemeOptions
    {
        /// <summary>
        /// UserInfoEndpoint
        /// </summary>
        public string UserInfoEndpoint { get; set; } = "";
    }

    /// <summary>
    /// APIKeyAuthHandler
    /// </summary>
    public class APIKeyAuthHandler : AuthenticationHandler<APIKeyAuthOptions>
    {
        /// <summary>
        /// APIKeyAuthHandler constructor
        /// </summary>
        /// <param name="options">options</param>
        /// <param name="logger">logger</param>
        /// <param name="encoder">encoder</param>
        public APIKeyAuthHandler(IOptionsMonitor<APIKeyAuthOptions> options, ILoggerFactory logger, UrlEncoder encoder)
            : base(options, logger, encoder)
        {

        }

        /// <summary>
        /// HandleAuthenticateAsync
        /// </summary>
        /// <returns>AuthenticateResult</returns>
        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.TryGetValue(HeaderNames.Authorization, out var authHeaderEntry))
            {
                return AuthenticateResult.NoResult();
            }
            else
            {
                var authHeader = authHeaderEntry.ToString().Trim();
                if (string.IsNullOrEmpty(authHeader) || !authHeader.StartsWith(APIKeyAuthDefaults.AuthenticationScheme))
                {
                    return AuthenticateResult.NoResult();
                }

                string token = authHeader.Substring(APIKeyAuthDefaults.AuthenticationScheme.Length).Trim();

                try
                {
                    return await ValidateTokenAsync(token);
                }
                catch (Exception ex)
                {
                    return AuthenticateResult.Fail(ex.Message);
                }
            }
        }

        /// <summary>
        /// Validates a token
        /// </summary>
        /// <param name="input_token">The APIKey token</param>
        /// <returns>An AuthenticateResult</returns>
        public async Task<AuthenticateResult> ValidateTokenAsync(string input_token)
        {
            Log.Debug($"Validating API Key '{input_token}'");

            if (!Guid.TryParse(input_token, out Guid ApiKeyGuid))
            {
                return AuthenticateResult.NoResult();
                //return AuthenticateResult.Fail($"Invalid APIKey provided.  Failed to parse token '{input_token}'");
            }

            var apiKey = Caches.APIKeyCache.Get(ApiKeyGuid);
            if (apiKey == null)
            {
                using var reader = DBAccess.GetReader();
                apiKey = await Caches.APIKeyCache.CacheAsync(ApiKeyGuid, reader);
                if (apiKey == null)
                {
                    return AuthenticateResult.Fail("Invalid APIKey specified.");
                }
            }

            var claims = UserService.GetClaims(UserService.GetUser(apiKey.UserName), HasWritePermission: apiKey.AllowWrites);
            var identity = new ClaimsIdentity(claims, Scheme.Name);
            var principal = new ClaimsPrincipal(identity);
            var ticket = new AuthenticationTicket(principal, Scheme.Name);
            return AuthenticateResult.Success(ticket);
        }
    }
}
