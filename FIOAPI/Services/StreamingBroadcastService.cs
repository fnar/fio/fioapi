﻿using FIOAPI.DB.Model;

namespace FIOAPI.Services
{
    /// <summary>
    /// BroadcastCXEntryTimedEvent
    /// </summary>
    public class BroadcastCXEntryTimedEvent : ITimedEvent
    {
        /// <summary>
        /// TimerInterval
        /// </summary>
        protected override TimeSpan TimerInterval
        {
            get => TimeSpan.FromSeconds(3.0);
        }

        /// <summary>
        /// Execute
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="eventTime">eventTime</param>
        protected override void Execute(object source, DateTime eventTime)
        {
            var entries = StreamingBroadcastService.GetQueuedCXEntries();
            if (entries.Count > 0)
            {
                // @TODO: Push them out
                int x = 0;
                ++x;
            }
        }
    }

    /// <summary>
    /// StreamingBroadcastService - This may be a stop-gap intermediary
    /// </summary>
    public static class StreamingBroadcastService
    {
        private static List<CXEntry> QueuedCXEntries = new();

        /// <summary>
        /// Pulls all queued CX Entries
        /// </summary>
        /// <returns></returns>
        public static List<CXEntry> GetQueuedCXEntries()
        {
            List<CXEntry> entries = new List<CXEntry>();
            lock(QueuedCXEntries)
            {
                entries.AddRange(QueuedCXEntries);
                QueuedCXEntries.Clear();
            }

            return entries;
        }

        /// <summary>
        /// Pushes a CX Entry to be broadcast
        /// </summary>
        /// <param name="entry">CX entry</param>
        public static void PushCXEntry(CXEntry entry)
        {
            lock(QueuedCXEntries)
            {
                if (!QueuedCXEntries.Contains(entry))
                {
                    QueuedCXEntries.Add(entry);
                }
            }
        }
    }
}
