﻿namespace FIOAPI.Services
{
    /// <summary>
    /// RequestLoggerMiddleware - A mechanism
    /// </summary>
    public class RequestLoggerMiddleware
    {
        private readonly RequestDelegate next;

        /// <summary>
        /// RequestLoggerMiddleware constructor
        /// </summary>
        /// <param name="next">The next delegate to execute</param>
        public RequestLoggerMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        /// <summary>
        /// InvokeAsync
        /// </summary>
        /// <param name="context">The HttpContext</param>
        /// <returns>An async task</returns>
        public async Task InvokeAsync(HttpContext context)
        {
            string? username = context.GetUserName();
            if (username != null && RequestLogger.HasActiveSession(username))
            {
                await RequestLogger.Log(username, context);
            }

            // Invoke the next request in the chain
            await next(context);
        }
    }

    /// <summary>
    /// RequestLoggerMiddlewareExtensions
    /// </summary>
    public static class RequestLoggerMiddlewareExtensions
    {
        /// <summary>
        /// UseRequestLogger
        /// </summary>
        /// <param name="builder">IApplicationBuilder</param>
        /// <returns>IApplicationBuilder</returns>
        public static IApplicationBuilder UseRequestLogger(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RequestLoggerMiddleware>();
        }
    }
}
