﻿namespace FIOAPI.Services
{
    /// <summary>
    /// ExactTypeNameCaseNamingPolicy
    /// </summary>
    public class ExactTypeNameCaseNamingPolicy : JsonNamingPolicy
    {
        /// <summary>
        /// ConvertName override
        /// </summary>
        /// <param name="name">name of the property</param>
        /// <returns>result</returns>
        public override string ConvertName(string name) => name;
    }
}
