﻿using System.Net;

namespace FIOAPI.Services
{
    /// <summary>
    /// Abstract handler for all exceptions.
    /// </summary>
    public class ExceptionLoggerMiddleware
    {
        private readonly RequestDelegate _next;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="next">next</param>
        public ExceptionLoggerMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        /// <summary>
        /// Invoke
        /// </summary>
        /// <param name="context">context</param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch(BadHttpRequestException bex)
            {
                // Ignore this as it can happen when a request is interrupted mid-stream or the server dies, etc etc
                if (bex.Message != "Unexpected end of request content.")
                {
                    ExceptionLogger.LogException(context, bex);
                }
                throw;
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(context, ex);
                throw;
            }
        }
    }

    /// <summary>
    /// ExceptionLoggerMiddlewareExtensions
    /// </summary>
    public static class ExceptionLoggerMiddlewareExtensions
    {
        /// <summary>
        /// UseRequestLogger
        /// </summary>
        /// <param name="builder">IApplicationBuilder</param>
        /// <returns>IApplicationBuilder</returns>
        public static IApplicationBuilder UseExceptionLogger(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionLoggerMiddleware>();
        }
    }
}
