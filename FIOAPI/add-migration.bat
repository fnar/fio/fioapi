REM AddMigration.bat MigrationName
REM Creates migration entries for defined databases

@echo off

REM Because it's a pooled connection, ensure to tell our program
REM what database type this is: "-- --override-dbtype=sqlite"

dotnet ef migrations add %1 --context=SqliteDataContext   --output-dir=Migrations/SqliteMigrations   -- --override-dbtype=sqlite
dotnet ef migrations add %1 --context=PostgresDataContext --output-dir=Migrations/PostgresMigrations -- --override-dbtype=postgres
