﻿namespace FIOAPI.TimedEvents
{
    /// <summary>
    /// ProdLineUpdatedTimedEvent - Works around an issue where PrUn will send us garbage data
    /// </summary>
    public class ProdLineUpdatedTimedEvent : ITimedEvent
    {
        /// <summary>
        /// TimerInterval
        /// </summary>
        protected override TimeSpan TimerInterval
        {
            get => TimeSpan.FromSeconds(3.0);
        }

        /// <summary>
        /// Execute
        /// </summary>
        /// <param name="source"></param>
        /// <param name="eventTime"></param>
        protected override void Execute(object source, DateTime eventTime)
        {
            DelayedProdLineUpdatedHack.Process();
        }
    }

    /// <summary>
    /// DelayedProdLineUpdatedHack - Hack to handle garbage PrUn messages
    /// </summary>
    public static class DelayedProdLineUpdatedHack
    {
        internal static Dictionary<string, List<Model.ProductionLine>> IdToProdLines { get; set; } = new();

        /// <summary>
        /// AddProdLine
        /// </summary>
        /// <param name="prodLine"></param>
        public static void AddProdLine(Model.ProductionLine prodLine)
        {
            lock(IdToProdLines)
            {
                IdToProdLines.AddToList(prodLine.ProductionLineId, prodLine);       
            }
        }

        internal static void Process()
        {
            List<List<Model.ProductionLine>> ProdLinesToProcess = new();

            lock(IdToProdLines)
            {
                DateTime now = DateTime.UtcNow;
                var keys = IdToProdLines.Keys;
                foreach (var key in keys)
                {
                    if (IdToProdLines[key].All(p => now - p.Timestamp > TimeSpan.FromSeconds(2.0)))
                    {
                        ProdLinesToProcess.Add(IdToProdLines[key]);
                        IdToProdLines.Remove(key);
                    }
                }
            }

            List<Model.ProductionLine> ProdLinesToPushToDB = new();
            foreach(var prodLines in ProdLinesToProcess)
            {
                Model.ProductionLine bestProdLine = prodLines.First();
                foreach (var prodLine in prodLines.Skip(1))
                {
                    if (prodLine.Efficiency > bestProdLine.Efficiency)
                    {
                        bestProdLine = prodLine;
                    }
                }

                ProdLinesToPushToDB.Add(bestProdLine);
            }

            using (var writer = DBAccess.GetWriter())
            {
                writer.DB.ProductionLines.UpsertRange(ProdLinesToPushToDB)
                    .On(pl => new { pl.ProductionLineId })
                    .Run();
            }
        }
    }
}
