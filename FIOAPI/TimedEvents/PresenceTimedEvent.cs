﻿using Z.EntityFramework.Plus;

namespace FIOAPI.TimedEvents
{
    /// <summary>
    /// PresenceTimedEvent
    /// </summary>
    public class PresenceTimedEvent : ITimedEvent
    {
        /// <summary>
        /// TimerInterval
        /// </summary>
        protected override TimeSpan TimerInterval
        {
            get
            {   
                if (Globals.IsTesting)
                {
                    // We need a much smaller interval when running integration tests
                    return _TestingTimerInterval;
                }

                return _TimerInterval;
            }
        }
        private static readonly TimeSpan _TestingTimerInterval = TimeSpan.FromMilliseconds(100);
        private static readonly TimeSpan _TimerInterval = TimeSpan.FromMinutes(1.0);

        private static void ExecuteInternal(object source, DateTime eventTime)
        {
            var Ids = Presence.GetIdsAndClear();
            if (Ids.Count > 0)
            {
                List<string> ExistingUserIds = new();

                long NowEpoch = DateTime.UtcNow.ToEpochMs();
                using (var writer = DBAccess.GetWriter())
                {
                    ExistingUserIds = writer.DB.APEXUsers
                        .Where(au => Ids.Contains(au.APEXUserId))
                        .Select(au => au.APEXUserId)
                        .ToList();

                    if (ExistingUserIds.Count > 0)
                    {
                        // Update existing entries
                        int Updates = writer.DB.APEXUsers
                            .Where(au => ExistingUserIds.Contains(au.APEXUserId))
                            .ExecuteUpdate(au => au.SetProperty(p => p.LastOnlineTimestamp, NowEpoch));
                    }

                    // Remove ExistingUserIds from Ids
                    Ids = Ids
                        .Except(ExistingUserIds)
                        .ToList();

                    if (Ids.Count > 0)
                    {
                        // Create entires for all remaining Ids
                        var NewAPEXUsers = Ids.Select(id => new Model.APEXUser
                        {
                            APEXUserId = id,
                            LastOnlineTimestamp = NowEpoch
                        }).ToList();

#if DEBUG
                        List<string> Errors = new();
                        if (!NewAPEXUsers.Validate(ref Errors))
                        {
                            throw new InvalidOperationException(string.Join(Environment.NewLine, Errors));
                        }
#endif

                        writer.DB.APEXUsers.AddRange(NewAPEXUsers);
                        writer.DB.SaveChanges();
                    }
                }
            }
        }

        /// <summary>
        /// Execute
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="eventTime">eventTime</param>
        protected override void Execute(object source, DateTime eventTime)
        {
            ExecuteInternal(source, eventTime);
        }
    }

    /// <summary>
    /// Presence
    /// </summary>
    public static class Presence
    {
        internal static HashSet<string> Ids { get; set; } = new();

        /// <summary>
        /// UpdateEntry
        /// </summary>
        /// <param name="ids">ids</param>
        public static void UpdatePresence(List<string> ids)
        {
            lock(Ids)
            {
                // When we update, we union. The reason is because within the timed event time-frame,
                // a user can enter and then leave.
                Ids.UnionWith(ids);
            }
        }

        internal static List<string> GetIdsAndClear()
        {
            lock(Ids)
            {
                List<string> Copy = Ids.ToList();
                Ids.Clear();    // Clear the list on retrieval
                return Copy;
            }
        }
    }
}
