﻿using Microsoft.AspNetCore.SignalR;

namespace FIOAPI.SignalR
{
    /// <summary>
    /// CXDataHub
    /// </summary>
    [Authorize(Policy = AuthPolicy.Bot)]
    public class CXDataHub : Hub
    {

    }
}
