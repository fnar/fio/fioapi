﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using FIOAPI.Payloads.Planet;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// PlanetController
    /// </summary>
    [ApiController]
    [Route("/planet")]
    public class PlanetController : ControllerBase
    {
        /// <summary>
        /// PUTs the planet payload (PATH_PLANETS) to FIOAPI
        /// </summary>
        /// <param name="PlanetDataPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutPlanet([FromBody] Payloads.Planet.PATH_PLANETS PlanetDataPayload)
        {
            var Errors = new List<string>();
            if (!PlanetDataPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = PlanetDataPayload.payload.message.payload.body;
            if (data != null)
            {
                Model.Planet Planet = new();
                Planet.PlanetId = data.planetId;
                Planet.NaturalId = data.naturalId;
                Planet.Name = data.name;
                Planet.NamerId = data.namer?.id;
                Planet.NamerUserName = data.namer?.username;
                Planet.NamedTimestamp = data.namingDate?.timestamp.FromUnixTime();
                Planet.Nameable = data.nameable;
                Planet.Gravity = data.data.gravity;
                Planet.MagneticField = data.data.magneticField;
                Planet.Mass = data.data.mass;
                Planet.MassEarth = data.data.massEarth;
                Planet.SemiMajorAxis = data.data.orbit.semiMajorAxis;
                Planet.Eccentricity = data.data.orbit.eccentricity;
                Planet.Inclination = data.data.orbit.inclination;
                Planet.RightAscension = data.data.orbit.rightAscension;
                Planet.Periapsis = data.data.orbit.periapsis;
                Planet.OrbitIndex = data.data.orbitIndex;
                Planet.Pressure = data.data.pressure;
                Planet.Radiation = data.data.radiation;
                Planet.Radius = data.data.radius;
                Planet.Sunlight = data.data.sunlight;
                Planet.Temperature = data.data.temperature;
                Planet.Fertility = data.data.fertility;
                Planet.Surface = data.data.surface;
                Planet.Plots = data.data.plots;
                Planet.CountryId = data.country?.id;
                Planet.CountryName = data.country?.name;
                Planet.CountryCode = data.country?.code;
                Planet.GoverningEntity = data.governingEntity;
                Planet.CurrencyNumericCode = data.localRules?.collector?.currency?.numericCode;
                Planet.CurrencyCode = data.localRules?.collector?.currency?.code;
                Planet.CurrencyName = data.localRules?.collector?.currency?.name;
                Planet.CollectionCurrencyNumericCode = data.localRules?.currency?.numericCode;
                Planet.CollectionCurrencyCode = data.localRules?.currency?.code;
                Planet.CollectionCurrencyName = data.localRules?.currency?.name;
                Planet.LocalMarketFeeBase = data.localRules?.localMarketFee.@base;
                Planet.LocalMarketTimeFactor = data.localRules?.localMarketFee.timeFactor;
                Planet.WarehouseFee = data.localRules?.warehouseFee.fee;
                Planet.EstablishmentFee = data.localRules?.siteEstablishmentFee?.fee;
                Planet.PopulationId = data.populationId;
                Planet.SupportsGhostSites = data.supportsGhostSites;
                Planet.UserNameSubmitted = this.GetUserName()!;
                Planet.Timestamp = DateTime.UtcNow;

                foreach (var planet_project in data.projects)
                {
                    if (Constants.TypeToPlanetaryProject.TryGetValue(planet_project.type, out var planetaryProject))
                    {
                        switch(planetaryProject)
                        {
                            case Constants.PlanetaryProject.AdministrationCenter:
                                Planet.AdministrationCenterId = planet_project.entityId;
                                break;
                            case Constants.PlanetaryProject.ChamberOfGlobalCommerce:
                                Planet.ChamberOfCommerceId = planet_project.entityId;
                                break;
                            case Constants.PlanetaryProject.LocalMarket:
                                Planet.LocalMarketId = planet_project.entityId;
                                break;
                            case Constants.PlanetaryProject.Warehouse:
                                Planet.WarehouseId = planet_project.entityId;
                                break;
                            case Constants.PlanetaryProject.Shipyard:
                                Planet.ShipyardId = planet_project.entityId;
                                break;
                            //case Constants.PlanetaryProject.Population:
                            //    Planet.PopulationId = planet_project.entityId;
                            //    break;
                        }
                    }
                }

                var PlanetResources = new List<PlanetResource>();
                foreach (var planet_resource in data.data.resources)
                {
                    var Resource = new PlanetResource();
                    Resource.PlanetResourceId = $"{Planet.PlanetId}-{planet_resource.materialId}";
                    Resource.MaterialId = planet_resource.materialId;
                    Resource.Type = planet_resource.type;
                    Resource.Concentration = planet_resource.factor;
                    Resource.PlanetId = Planet.PlanetId;
                    Resource.Planet = Planet;

                    PlanetResources.Add(Resource);
                }

                Planet.Resources = PlanetResources;

                var PlanetWorkforceFees = new List<PlanetWorkforceFee>();
                if (data.localRules != null && data.localRules.productionFees != null)
                {
                    foreach (var planet_workforce_fee in data.localRules.productionFees.fees)
                    {
                        var WorkforceFee = new PlanetWorkforceFee();
                        WorkforceFee.PlanetWorkforceFeeId = $"{Planet.PlanetId}-{planet_workforce_fee.category}-{planet_workforce_fee.workforceLevel}";
                        WorkforceFee.Category = planet_workforce_fee.category;
                        WorkforceFee.WorkforceLevel = planet_workforce_fee.workforceLevel;
                        WorkforceFee.Fee = planet_workforce_fee.fee?.amount;
                        WorkforceFee.Currency = planet_workforce_fee.fee?.currency;
                        WorkforceFee.PlanetId = Planet.PlanetId;
                        WorkforceFee.Planet = Planet;

                        PlanetWorkforceFees.Add(WorkforceFee);
                    }
                }

                Planet.WorkforceFees = PlanetWorkforceFees;

                if (!Planet.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                Model.Warehouse? warehouse = null;
                if (Planet.WarehouseId != null)
                {
                    warehouse = new();
                    warehouse.WarehouseId = Planet.WarehouseId;
                    warehouse.LocationId = Planet.PlanetId;
                    warehouse.LocationName = Planet.Name;
                    warehouse.LocationNaturalId = Planet.NaturalId;
                    warehouse.Timestamp = Planet.Timestamp;
                    warehouse.UserNameSubmitted = Planet.UserNameSubmitted;

                    if (!warehouse.Validate(ref Errors))
                    {
                        return BadRequest(Errors);
                    }
                }

                bool ShouldUpdatePlanet = FIOHashCache.ShouldUpdate(Planet); // NB: This will always return true right now because Planet at this point doesn't have COGC/POPR/PPRs filled out
                bool ShouldUpdateWarehouse = warehouse != null && FIOHashCache.ShouldUpdate(warehouse);
                if (ShouldUpdatePlanet || ShouldUpdateWarehouse)
                {
                    using (var writer = DBAccess.GetWriter())
                    {
                        if (ShouldUpdatePlanet)
                        {
                            await writer.DB.Planets.Upsert(Planet)
                            .On(p => new { p.PlanetId })
                            .RunAsync();

                            await writer.DB.PlanetResources.UpsertRange(PlanetResources)
                                .On(pr => new { pr.PlanetResourceId })
                                .RunAsync();

                            await writer.DB.PlanetWorkforceFees.UpsertRange(PlanetWorkforceFees)
                                .On(pwf => new { pwf.PlanetWorkforceFeeId })
                                .RunAsync();

                            // Pull COGC/POPR/PPR because we need the PlanetCache to have a full-fledged object in it
                            Planet.COGCPrograms = await writer.DB.COGCPrograms
                                .Where(p => p.PlanetId == Planet.PlanetId)
                                .ToListAsync();

                            Planet.PopulationReports = await writer.DB.PlanetPopulationReports
                                .Where(p => p.PlanetId == Planet.PlanetId)
                                .ToListAsync();

                            Planet.PopulationProjects = await writer.DB.PlanetPopulationProjects
                                .Include(p => p.Upkeeps)
                                .Where(p => p.PlanetId == Planet.PlanetId)
                                .ToListAsync();
                        }

                        if (ShouldUpdateWarehouse)
                        {
                            await writer.DB.Warehouses.Upsert(warehouse!)
                                .On(w => new { w.WarehouseId })
                                .RunAsync();
                        }
                    }

                    if (ShouldUpdatePlanet)
                    {
                        _ = EntityCaches.PlanetCache.UpdateAsync(Planet);
                    }
                    
                    if (ShouldUpdateWarehouse)
                    {
                        _ = EntityCaches.WarehouseCache.UpdateAsync(warehouse!);
                    }
                }

                return Ok();
            }

            return BadRequest();
        }

        /// <summary>
        /// PUTs the planet sites payload (PLANET_SITES) to FIOAPI
        /// </summary>
        /// <param name="PlanetSitesPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("sites")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutPlanetSites([FromBody] Payloads.Planet.PLANET_SITES PlanetSitesPayload)
        {
            List<string> Errors = new();
            if (!PlanetSitesPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            List<PlanetSite> AllPlanetSites = new();

            var data = PlanetSitesPayload.payload.message.payload;
            if (data.body.Count == 0)
            {
                return Ok();
            }

            foreach (var planetSite in data.body)
            {
                PlanetSite PlanetSite = new()
                {
                    PlanetSiteId = planetSite.id,
                    PlanetId = planetSite.planetId,
                    OwnerId = planetSite.entity.id,
                    OwnerName = planetSite.entity.name,
                    OwnerCode = planetSite.entity.code != null ? planetSite.entity.code! : planetSite.entity.naturalId!,
                    Type = planetSite.type,
                    PlotNumber = planetSite.plot,
                    UserNameSubmitted = this.GetUserName()!,
                    Timestamp = DateTime.UtcNow,
                };

                AllPlanetSites.Add(PlanetSite);
            }

            if (!AllPlanetSites.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var PlanetId = data.body[0].planetId;
#if DEBUG
            System.Diagnostics.Debug.Assert(AllPlanetSites.Select(ps => ps.PlanetId).Distinct().Count() == 1);
#endif

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.PlanetSites
                    .Where(ps => ps.PlanetId == PlanetId)
                    .DeleteAsync();

                await writer.DB.AddRangeAsync(AllPlanetSites);
                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// PUTs the planet cogc history payload (PLANET_COGC_HISTORY) to FIOAPI
        /// </summary>
        /// <param name="PlanetCOGCHistoryPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("cogc")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutCOGC([FromBody] Payloads.Planet.PLANET_COGC_HISTORY PlanetCOGCHistoryPayload)
        {
            var Errors = new List<string>();
            if (!PlanetCOGCHistoryPayload.Validate(ref Errors))
            {
                return BadRequest(PlanetCOGCHistoryPayload);
            }

            List<PlanetCOGCProgram> COGCPrograms = new();
            var data = PlanetCOGCHistoryPayload.payload.message.payload.body;
            foreach (var program in data.programs)
            {
                var COGCProgram = new PlanetCOGCProgram()
                {
                    PlanetCOGCProgramId = $"{data.planet.id}-{program.start.timestamp}",
                    Type = program.type,
                    StartTime = program.start.timestamp.FromUnixTime(),
                    EndTime = program.end.timestamp.FromUnixTime(),
                    PlanetId = data.planet.id
                };

                COGCPrograms.Add(COGCProgram);
            }

            if (!COGCPrograms.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            DateTime TimeNow = DateTime.UtcNow;
            var CurrentCOGCProgram = COGCPrograms
                .Where(c => TimeNow >= c.StartTime && TimeNow <= c.EndTime)
                .FirstOrDefault();

            bool ShouldUpdate = false;

            Planet? planet = null;
            using (var writer = DBAccess.GetWriter())
            {
                planet = writer.DB.Planets
                    .Where(p => p.PlanetId == data.planet.id)
                    .Include(p => p.Resources)
                    .Include(p => p.WorkforceFees)
                    .Include(p => p.COGCPrograms)
                    .Include(p => p.PopulationReports)
                    .Include(p => p.PopulationProjects)
                            .ThenInclude(pp => pp.Upkeeps)
                    .FirstOrDefault();
                if (planet != null)
                {
                    if (data.status == "ACTIVE")
                    {
                        if (CurrentCOGCProgram != null)
                        {
                            planet.CurrentCOGCProgram = CurrentCOGCProgram.Type;
                            planet.CurrentCOGCProgramEndTime = CurrentCOGCProgram.EndTime;
                            planet.UserNameSubmitted = this.GetUserName()!;
                            planet.Timestamp = DateTime.UtcNow;
                        }
                        else
                        {
                            planet.CurrentCOGCProgram = "UNKNOWN";
                        }
                    }
                    else
                    {
                        planet.CurrentCOGCProgram = "INACTIVE";
                    }

                    planet.COGCPrograms.Clear();
                    planet.COGCPrograms.AddRange(COGCPrograms);
                    ShouldUpdate = FIOHashCache.ShouldUpdate(planet);
                    if (ShouldUpdate)
                    {
                        await writer.DB.SaveChangesAsync();
                    }
                }
            }

            if (planet != null && ShouldUpdate)
            {
                _ = EntityCaches.PlanetCache.UpdateAsync(planet);
            }

            return Ok();
        }

        /// <summary>
        /// PUTs the planet population report payload (PLANET_POPULATION_REPORT) to FIOAPI
        /// </summary>
        /// <param name="PlanetPopulationReportPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("population")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutPopulationReport([FromBody] Payloads.Planet.PLANET_POPULATION_REPORT PlanetPopulationReportPayload)
        {
            List<string> Errors = new();
            if (!PlanetPopulationReportPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            List<PlanetPopulationReport> PopulationReports = new();
            var data = PlanetPopulationReportPayload.payload.message.payload.body;

            string? PlanetId = null;
            using (var reader = DBAccess.GetReader())
            {
                PlanetId = reader.DB.Planets
                    .Where(p => p.PopulationId == data.id)
                    .Select(p => p.PlanetId)
                    .FirstOrDefault();
            }

            if (PlanetId != null)
            {
                foreach (var report in data.reports)
                {
                    var Report = new PlanetPopulationReport
                    {
                        PlanetPopulationReportId = $"{PlanetId}-{report.simulationPeriod}",
                        SimulationPeriod = report.simulationPeriod,
                        ExplorersGraceEnabled = report.explorersGraceEnabled,
                        ReportTimestamp = report.time.timestamp.FromUnixTime(),

                        NextPopulationPioneer = report.nextPopulation.PIONEER,
                        NextPopulationSettler = report.nextPopulation.SETTLER,
                        NextPopulationTechnician = report.nextPopulation.TECHNICIAN,
                        NextPopulationEngineer = report.nextPopulation.ENGINEER,
                        NextPopulationScientist = report.nextPopulation.SCIENTIST,

                        PopulationDifferencePioneer = report.populationDifference.PIONEER,
                        PopulationDifferenceSettler = report.populationDifference.SETTLER,
                        PopulationDifferenceTechnician = report.populationDifference.TECHNICIAN,
                        PopulationDifferenceEngineer = report.populationDifference.ENGINEER,
                        PopulationDifferenceScientist = report.populationDifference.SCIENTIST,

                        AverageHappinessPioneer = report.averageHappiness.PIONEER,
                        AverageHappinessSettler = report.averageHappiness.SETTLER,
                        AverageHappinessTechnician = report.averageHappiness.TECHNICIAN,
                        AverageHappinessEngineer = report.averageHappiness.ENGINEER,
                        AverageHappinessScientist = report.averageHappiness.SCIENTIST,

                        UnemploymentRatePioneer = report.unemploymentRate.PIONEER,
                        UnemploymentRateSettler = report.unemploymentRate.SETTLER,
                        UnemploymentRateTechnician = report.unemploymentRate.TECHNICIAN,
                        UnemploymentRateEngineer = report.unemploymentRate.ENGINEER,
                        UnemploymentRateScientist = report.unemploymentRate.SCIENTIST,

                        OpenJobsPioneer = report.openJobs.PIONEER,
                        OpenJobsSettler = report.openJobs.SETTLER,
                        OpenJobsTechnician = report.openJobs.TECHNICIAN,
                        OpenJobsEngineer = report.openJobs.ENGINEER,
                        OpenJobsScientist = report.openJobs.SCIENTIST,

                        NeedFulfillmentLifeSupport = report.needFulfillment.LIFE_SUPPORT,
                        NeedFulfillmentSafety = report.needFulfillment.SAFETY,
                        NeedFulfillmentHealth = report.needFulfillment.HEALTH,
                        NeedFulfillmentComfort = report.needFulfillment.COMFORT,
                        NeedFulfillmentCulture = report.needFulfillment.CULTURE,
                        NeedFulfillmentEducation = report.needFulfillment.EDUCATION,

                        PlanetId = PlanetId
                    };

                    PopulationReports.Add(Report);
                }

                if (!PopulationReports.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                bool ShouldUpdate = false;

                Planet? PlanetToUpdate = null;
                using (var writer = DBAccess.GetWriter())
                {
                    // We need to grab the planet from the database explicitly via the writer
                    PlanetToUpdate = writer.DB.Planets
                        .Include(p => p.Resources)
                        .Include(p => p.WorkforceFees)
                        .Include(p => p.COGCPrograms)
                        .Include(p => p.PopulationReports)
                        .Include(p => p.PopulationProjects)
                            .ThenInclude(pp => pp.Upkeeps)
                        .FirstOrDefault(p => p.PlanetId == PlanetId);
                    if (PlanetToUpdate != null)
                    {
                        PopulationReports.ForEach(pr => pr.Planet = PlanetToUpdate);
                        PlanetToUpdate.PopulationReports.Clear();
                        PlanetToUpdate.PopulationReports.AddRange(PopulationReports);

                        ShouldUpdate = FIOHashCache.ShouldUpdate(PlanetToUpdate);
                        if (ShouldUpdate)
                        {
                            await writer.DB.SaveChangesAsync();
                        }
                    }
                }

                if (PlanetToUpdate != null && ShouldUpdate)
                {
                    _ = EntityCaches.PlanetCache.UpdateAsync(PlanetToUpdate);
                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs a planet population project payload (PATH_POPULATION_PROJECTS) to FIOAPI
        /// </summary>
        /// <param name="populationProject">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("populationproject")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutPopulationProject([FromBody] Payloads.Planet.PATH_POPULATION_PROJECTS populationProject)
        {
            List<string> Errors = new();
            if (!populationProject.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = populationProject.payload.message.payload.body;

            string? PlanetId = null;
            using (var cacheReader = EntityCaches.PlanetCache.GetReader())
            {
                var planet = cacheReader.AllEntities.FirstOrDefault(p => p.PopulationId == populationProject.payload.message.payload.path[1]);
                if (planet != null)
                {
                    PlanetId = planet.PlanetId;
                }
            }

            if (PlanetId != null)
            {
                PlanetPopulationProject planetPopulationProject = new();
                planetPopulationProject.PlanetPopulationProjectId = data.id;
                planetPopulationProject.Type = data.type;
                planetPopulationProject.ProjectIdentifier = data.projectIdentifier;
                planetPopulationProject.Level = data.level;
                planetPopulationProject.ActiveLevel = data.activeLevel;
                planetPopulationProject.CurrentLevel = data.currentLevel;
                planetPopulationProject.UpgradeStatus = data.upgradeStatus;
                planetPopulationProject.UpgradeCostsRaw = data.upgradeCosts != null ? string.Join(',', data.upgradeCosts.Select(uc => $"{uc.material.ticker}:{uc.amount}_{uc.currentAmount}")) : "";

                foreach (var upkeep in data.upkeeps)
                {
                    PlanetPopulationProjectUpkeep planetPopulationProjectUpkeep = new();
                    planetPopulationProjectUpkeep.PlanetPopulationProjectUpkeepId = $"{planetPopulationProject.PlanetPopulationProjectId}-{upkeep.material.ticker}";
                    planetPopulationProjectUpkeep.Stored = upkeep.stored;
                    planetPopulationProjectUpkeep.StoreCapacity = upkeep.storeCapacity;
                    planetPopulationProjectUpkeep.Duration = upkeep.duration;
                    planetPopulationProjectUpkeep.NextTick = upkeep.nextTick.timestamp.FromUnixTime();
                    planetPopulationProjectUpkeep.Ticker = upkeep.material.ticker;
                    planetPopulationProjectUpkeep.Amount = upkeep.amount;
                    planetPopulationProjectUpkeep.CurrentAmount = upkeep.currentAmount;
                    planetPopulationProjectUpkeep.PlanetPopulationProjectId = planetPopulationProject.PlanetPopulationProjectId;
                    planetPopulationProjectUpkeep.PlanetPopulationProject = planetPopulationProject;

                    planetPopulationProject.Upkeeps.Add(planetPopulationProjectUpkeep);
                }

                planetPopulationProject.PlanetId = PlanetId;

                var ValidationIgnores = new List<ValidationIgnore>
                {
                    new ValidationIgnore(typeof(PlanetPopulationProject), nameof(planetPopulationProject.Planet))
                };

                if (!planetPopulationProject.Validate(ref Errors, ValidationIgnores: ValidationIgnores))
                {
                    return BadRequest(Errors);
                }

                using (var writer = DBAccess.GetWriter())
                {
                    await writer.DB.PlanetPopulationProjects
                        .Upsert(planetPopulationProject)
                        .On(ppp => ppp.PlanetPopulationProjectId)
                        .RunAsync();

                    await writer.DB.PlanetPopulationProjectUpkeeps
                        .UpsertRange(planetPopulationProject.Upkeeps)
                        .On(pppu => pppu.PlanetPopulationProjectUpkeepId)
                        .RunAsync();
                }
            }
            return Ok();
        }

        /// <summary>
        /// Retrieves all sites for a provided planet identifier
        /// </summary>
        /// <param name="PlanetIdentifier">Planet identifier</param>
        /// <returns>List of planet sites</returns>
        [HttpGet("sites/{PlanetIdentifier}")]
        [AllowAnonymous]
        [Cache(Hours = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.PlanetSite>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "PlanetIdentifier not valid", typeof(string), "plain/text")]
        public async Task<IActionResult> GetSites(string PlanetIdentifier)
        {
            var Planet = EntityCaches.PlanetCache.Get(PlanetIdentifier);
            if (Planet == null)
            {
                return BadRequest("Provided PlanetIdentifier not found");
            }

            List<PlanetSite> PlanetSites = new();
            using (var reader = DBAccess.GetReader())
            {
                PlanetSites = await reader.DB.PlanetSites
                    .Where(ps => ps.PlanetId == Planet.PlanetId)
                    .ToListAsync();
            }

            return Ok(PlanetSites);
        }

        /// <summary>
        /// Retrieves site count. Optional planets can be specified.
        /// </summary>
        /// <param name="planets">Planet names/natural ids/ids</param>
        /// <returns>SiteCounts</returns>
        [HttpGet("sitecount")]
        [AllowAnonymous]
        [Cache(Hours = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<PlanetSiteCount>), "application/json")]
        public async Task<IActionResult> GetSiteCount(
            [FromQuery(Name = "planet")] List<string>? planets)
        {
            bool HavePlanetIdsToSearch = planets != null && planets.Count > 0;

            List<string> planetIdsToSearch = new();
            if (HavePlanetIdsToSearch)
            {
                foreach (var pl in planets!)
                {
                    var res = EntityCaches.PlanetCache.Get(pl);
                    if (res != null)
                    {
                        planetIdsToSearch.Add(res.PlanetId);
                    }
                }
            }

            Dictionary<string, int> planetIdToSiteCount = new();
            using (var reader = DBAccess.GetReader())
            {
                var siteResultsQuery = reader.DB.PlanetSites.AsQueryable();
                if (HavePlanetIdsToSearch)
                {
                    siteResultsQuery = siteResultsQuery.Where(s => planetIdsToSearch.Contains(s.PlanetId));
                }

                var siteResultGroupingQuery = siteResultsQuery.GroupBy(g => g.PlanetId);
                planetIdToSiteCount = await siteResultGroupingQuery.ToDictionaryAsync(g => g.Key, g => g.Count());
            }

            var planetSiteCountResults = planetIdToSiteCount
                .Select(kvp => new PlanetSiteCount
                {
                    PlanetId = kvp.Key,
                    Count = kvp.Value
                }).ToList();
            return Ok(planetSiteCountResults);
        }

        /// <summary>
        /// Retrieves specified planet(s) by name, naturalid, or planet id
        /// </summary>
        /// <param name="planet">Planet(s) to search</param>
        /// <param name="include_resources">If the resources should be returned</param>
        /// <param name="include_workforce_fees">If the workforce fees should be returned</param>
        /// <param name="include_cogc_programs">If COGC data should be returned</param>
        /// <param name="include_population_reports">If population reports should be returned</param>
        /// <param name="include_population_projects">If population projects should be returned</param>
        /// <returns></returns>
        [HttpGet("")]
        [AllowAnonymous]
        [Cache(Hours = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Planet>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Did not specify any planets")]
        public async Task<IActionResult> GetPlanet(
            [FromQuery(Name = "planet")] List<string>? planet,
            [FromQuery(Name = "include_resources")] bool include_resources = true,
            [FromQuery(Name = "include_workforce_fees")] bool include_workforce_fees = false,
            [FromQuery(Name = "include_cogc_programs")] bool include_cogc_programs = false,
            [FromQuery(Name = "include_population_reports")] bool include_population_reports = false,
            [FromQuery(Name = "include_population_projects")] bool include_population_projects = false)
        {
            if (planet == null || planet.Count == 0)
            {
                return BadRequest();
            }

            planet.ForEach(p =>
            {
                p = p.Trim().ToUpper();
            });

            List<Model.Planet> Planets;
            using (var reader = DBAccess.GetReader())
            {
                var PlanetsQuery = reader.DB.Planets.AsQueryable();

                if (include_resources)
                {
                    PlanetsQuery.Include(p => p.Resources);
                }

                if (include_workforce_fees)
                {
                    PlanetsQuery = PlanetsQuery.Include(p => p.WorkforceFees);
                }

                if (include_cogc_programs)
                {
                    PlanetsQuery = PlanetsQuery.Include(p => p.COGCPrograms);
                }

                if (include_population_reports)
                {
                    PlanetsQuery = PlanetsQuery.Include(p => p.PopulationReports);
                }

                if (include_population_projects)
                {
                    PlanetsQuery = PlanetsQuery.Include(p => p.PopulationProjects)
                        .ThenInclude(pp => pp.Upkeeps);
                }

                Planets = await PlanetsQuery
                    .Where(p => planet.Contains(p.PlanetId.ToUpper()) || (p.Name != null && planet.Contains(p.Name.ToUpper())) || planet.Contains(p.NaturalId.ToUpper()))
                    .ToListAsync();
            }

            return Ok(Planets);
        }

        /// <summary>
        /// Retrieves the specified planet by name, naturalid, or planet id
        /// </summary>
        /// <param name="PlanetIdentifier">Name, NaturalId, or Planet id</param>
        /// <param name="include_resources">If the resources should be returned</param>
        /// <param name="include_workforce_fees">If the workforce fees should be returned</param>
        /// <param name="include_cogc_programs">If COGC data should be returned</param>
        /// <param name="include_population_reports">If population reports should be returned</param>
        /// <param name="include_population_projects">If population projects should be returned</param>
        /// <returns>The planet</returns>
        [HttpGet("{PlanetIdentifier}")]
        [AllowAnonymous]
        [Cache(Hours = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(Model.Planet), "application/json")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "Not found")]
        public async Task<IActionResult> GetPlanet(string PlanetIdentifier, 
            [FromQuery(Name = "include_resources")] bool include_resources = true,
            [FromQuery(Name = "include_workforce_fees")] bool include_workforce_fees = false,
            [FromQuery(Name = "include_cogc_programs")] bool include_cogc_programs = false,
            [FromQuery(Name = "include_population_reports")] bool include_population_reports = false,
            [FromQuery(Name = "include_population_projects")] bool include_population_projects = false)
        {
            PlanetIdentifier = PlanetIdentifier.Trim().ToUpper();

            if (string.IsNullOrEmpty(PlanetIdentifier))
            {
                return await GetAllPlanets();
            }

            Model.Planet? Planet = null;
            using (var reader = DBAccess.GetReader())
            {
                var PlanetQuery = reader.DB.Planets.AsQueryable();

                if (include_resources)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.Resources);
                }

                if (include_workforce_fees)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.WorkforceFees);
                }

                if (include_cogc_programs)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.COGCPrograms);
                }

                if (include_population_reports)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.PopulationReports);
                }

                if (include_population_projects)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.PopulationProjects)
                        .ThenInclude(pp => pp.Upkeeps);
                }

                Planet = await PlanetQuery
                    .Where(p => p.PlanetId.ToUpper() == PlanetIdentifier || (p.Name != null && p.Name.ToUpper() == PlanetIdentifier) || p.NaturalId.ToUpper() == PlanetIdentifier)
                    .FirstOrDefaultAsync();
            }

            if (Planet != null)
            {
                return Ok(Planet);
            }

            return NoContent();
        }

        /// <summary>
        /// Retrieves all planets
        /// </summary>
        /// <param name="include_resources">If the resources should be returned</param>
        /// <param name="include_workforce_fees">If the workforce fees should be returned</param>
        /// <param name="include_cogc_programs">If COGC data should be returned</param>
        /// <param name="include_population_reports">If population reports should be returned</param>
        /// <param name="include_population_projects">If population projects should be returned</param>
        /// <returns>All planets</returns>
        [HttpGet("all_planets")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Planet>), "application/json")]
        public async Task<IActionResult> GetAllPlanets(
            [FromQuery(Name = "include_resources")] bool include_resources = true,
            [FromQuery(Name = "include_workforce_fees")] bool include_workforce_fees = false,
            [FromQuery(Name = "include_cogc_programs")] bool include_cogc_programs = false,
            [FromQuery(Name = "include_population_reports")] bool include_population_reports = false,
            [FromQuery(Name = "include_population_projects")] bool include_population_projects = false)
        {
            List<Model.Planet> Planets = new();
            using (var reader = DBAccess.GetReader())
            {
                var PlanetQuery = reader.DB.Planets.AsQueryable();

                if (include_resources)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.Resources);
                }

                if (include_workforce_fees)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.WorkforceFees);
                }

                if (include_cogc_programs)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.COGCPrograms);
                }

                if (include_population_reports)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.PopulationReports);
                }

                if (include_population_projects)
                {
                    PlanetQuery = PlanetQuery.Include(p => p.PopulationProjects)
                        .ThenInclude(pp => pp.Upkeeps);
                }

                Planets = await PlanetQuery.ToListAsync();
            }

            return Ok(Planets);
        }

        /// <summary>
        /// Searches planets
        /// </summary>
        /// <param name="Search">The search payload</param>
        /// <returns>Planet search results</returns>
        [AllowAnonymous]
        [HttpPost("search")]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<PlanetSearchResult>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed to search", typeof(List<string>), "application/json")]
        public async Task<IActionResult> SearchPlanets(
            [FromBody] PlanetSearch Search)
        {
            List<string> Errors = new();
            if (!Search.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var cacheReader = EntityCaches.MaterialCache.GetReader())
            {
                // Verify Search Materials
                if (Search.Materials.Any())
                {
                    // Uppercase all materials
                    Search.Materials = Search.Materials.Select(m => m.ToUpper()).ToList();

                    // Convert all materials into material ids
                    Search.Materials = Search.Materials
                        .Select(sm =>
                        {
                            var mat = cacheReader.AllEntities.FirstOrDefault(m => m.Ticker == sm || m.Name.ToUpper() == sm || m.MaterialId.ToUpper() == sm);
                            return mat != null ? mat.MaterialId : sm;
                        })
                        .ToList();

                    var FailedMaterials = Search.Materials
                        .Where(sm => sm.Length != 32)
                        .Select(sm => $"'{sm}' did not resolve to a material")
                        .ToList();
                    if (FailedMaterials.Any())
                    {
                        return BadRequest(FailedMaterials);
                    }
                }
            }

            // Verify JumpDistanceThresholdSource
            bool DoJumpDistanceThresholdWork = (Search.JumpDistanceThreshold >= 0 && !string.IsNullOrWhiteSpace(Search.JumpDistanceThresholdSource));
            if (DoJumpDistanceThresholdWork)
            {
                var JumpDistanceThresholdSourceSystemId = Search.JumpDistanceThresholdSource!;
                if (!EntityCaches.ConvertToSystemId(ref JumpDistanceThresholdSourceSystemId, out var Error))
                {
                    return BadRequest(new List<string> { Error });
                }
            }

            // Verify DistanceChecks
            bool DoDistanceChecks = Search.DistanceChecks.Count > 0;
            if (DoDistanceChecks)
            {
                if (Search.DistanceChecks.Count > 10)
                {
                    return BadRequest(new List<string> { "Cannot specify more than 10 DistanceChecks" });
                }

                List<string> DistanceCheckSystemIds = Search.DistanceChecks;
                if (!EntityCaches.ConvertToSystemIds(ref DistanceCheckSystemIds, out Errors))
                {
                    return BadRequest(Errors);
                }
            }

            List<PlanetSearchResult> SearchResults = new();
            using (var EntityCacheReader = EntityCaches.PlanetCache.GetReader())
            {
                var AllPlanets = EntityCacheReader.AllEntities;
                var ResultPlanetsQuery = AllPlanets.AsQueryable();

                if (Search.Materials.Count > 0)
                {
                    // Determine all our potential planets
                    var PlanetIdList = AllPlanets.SelectMany(p => p.Resources)
                        .Where(pr => Search.Materials.Contains(pr.MaterialId) && pr.Concentration > Search.ConcentrationThreshold)
                        .GroupBy(pr => pr.PlanetId)
                        .Select(g => new { PlanetId = g.Key, count = g.Count() })
                        .Where(q => q.count >= Search.Materials.Count)
                        .Select(pid => pid.PlanetId)
                        .ToList();

                    ResultPlanetsQuery = ResultPlanetsQuery
                        .Where(p => PlanetIdList.Contains(p.PlanetId));
                }

                if (!Search.IncludeRocky)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => !p.Rocky);
                }
                if (!Search.IncludeGaseous)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => !p.Gaseous);
                }
                if (!Search.IncludeLowGravity)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => !p.LowGravity);
                }
                if (!Search.IncludeHighGravity)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => !p.HighGravity);
                }
                if (!Search.IncludeLowPressure)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => !p.LowPressure);
                }
                if (!Search.IncludeHighPressure)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => !p.HighPressure);
                }
                if (!Search.IncludeLowTemperature)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => !p.LowTemperature);
                }
                if (!Search.IncludeHighTemperature)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => !p.HighTemperature);
                }

                if (Search.MustBeFertile)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => p.Fertile);
                }
                if (Search.MustHaveLocalMarket)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => p.HasLocalMarket);
                }
                if (Search.MustHaveChamberOfCommerce)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => p.HasChamberOfCommerce);
                }
                if (Search.MustHaveWarehouse)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => p.HasWarehouse);
                }
                if (Search.MustHaveAdministrationCenter)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => p.HasAdministrationCenter);
                }
                if (Search.MustHaveShipyard)
                {
                    ResultPlanetsQuery = ResultPlanetsQuery.Where(p => p.HasShipyard);
                }

                SearchResults = ResultPlanetsQuery
                    .Select(p => new PlanetSearchResult(p))
                    .ToList();
            }

            if (DoJumpDistanceThresholdWork || DoDistanceChecks)
            {
                using (var writer = DBAccess.GetWriter())
                {
                    if (DoJumpDistanceThresholdWork)
                    {
                        // Iterate backwards, removing any planet that exceeds the jump distance threshold
                        for (int SearchResultIdx = SearchResults.Count - 1; SearchResultIdx >= 0; --SearchResultIdx)
                        {
                            var JumpCount = await JumpCalculator.GetJumpCount(Search.JumpDistanceThresholdSource!, SearchResults[SearchResultIdx].PlanetId, access: writer);
                            if (JumpCount == -1 || JumpCount > Search.JumpDistanceThreshold)
                            {
                                SearchResults.RemoveAt(SearchResultIdx);
                            }
                        }
                    }

                    if (DoDistanceChecks)
                    {
                        for (int SearchResultIdx = 0; SearchResultIdx < SearchResults.Count; ++SearchResultIdx)
                        {
                            for (int DistanceCheckIdx = 0; DistanceCheckIdx < Search.DistanceChecks.Count; ++DistanceCheckIdx)
                            {
                                var PlanetSystemId = SearchResults[SearchResultIdx].PlanetId;
                                if (EntityCaches.ConvertToSystemId(ref PlanetSystemId, out _))
                                {
                                    SearchResults[SearchResultIdx].DistanceResults.Add(await JumpCalculator.GetJumpCount(PlanetSystemId, Search.DistanceChecks[DistanceCheckIdx], access: writer));
                                }
                                else
                                {
                                    SearchResults[SearchResultIdx].DistanceResults.Add(-1);
                                }
                            }
                        }
                    }
                }
            }

            for(int SearchResultIdx = 0; SearchResultIdx < SearchResults.Count; ++SearchResultIdx)
            {
                var SearchResult = SearchResults[SearchResultIdx];
                var Planet = EntityCaches.PlanetCache.Get(SearchResults[SearchResultIdx].PlanetId);
                if (Planet != null)
                {
                    var LatestReport = Planet.PopulationReports
                        .OrderByDescending(pr => pr.SimulationPeriod)
                        .FirstOrDefault();
                    if (LatestReport != null)
                    {
                        bool HaveJobData = (DateTime.UtcNow < (LatestReport.ReportTimestamp + new TimeSpan(7, 0, 0, 0)));
                        SearchResults[SearchResultIdx].HasJobData = HaveJobData;
                        if (HaveJobData)
                        {
                            SearchResults[SearchResultIdx].TotalPioneers = LatestReport.NextPopulationPioneer;
                            SearchResults[SearchResultIdx].OpenPioneers = (LatestReport.OpenJobsPioneer > 0) ? -(int)LatestReport.OpenJobsPioneer : ((int)(LatestReport.NextPopulationPioneer * LatestReport.UnemploymentRatePioneer));

                            SearchResults[SearchResultIdx].TotalSettlers = LatestReport.NextPopulationSettler;
                            SearchResults[SearchResultIdx].OpenSettlers = (LatestReport.OpenJobsSettler > 0) ? -(int)LatestReport.OpenJobsSettler : ((int)(LatestReport.NextPopulationSettler * LatestReport.UnemploymentRateSettler));

                            SearchResults[SearchResultIdx].TotalTechnicians = LatestReport.NextPopulationTechnician;
                            SearchResults[SearchResultIdx].OpenTechnicians = (LatestReport.OpenJobsTechnician > 0) ? -(int)LatestReport.OpenJobsTechnician : ((int)(LatestReport.NextPopulationTechnician * LatestReport.UnemploymentRateTechnician));

                            SearchResults[SearchResultIdx].TotalEngineers = LatestReport.NextPopulationEngineer;
                            SearchResults[SearchResultIdx].OpenEngineers = (LatestReport.OpenJobsEngineer > 0) ? -(int)LatestReport.OpenJobsEngineer : ((int)(LatestReport.NextPopulationEngineer * LatestReport.UnemploymentRateEngineer));

                            SearchResults[SearchResultIdx].TotalScientists = LatestReport.NextPopulationScientist;
                            SearchResults[SearchResultIdx].OpenScientists = (LatestReport.OpenJobsScientist > 0) ? -(int)LatestReport.OpenJobsScientist : ((int)(LatestReport.NextPopulationScientist * LatestReport.UnemploymentRateScientist));
                        }
                    }
                }
            }          

            return Ok(SearchResults);
        }
    }
}
