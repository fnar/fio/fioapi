﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using FIOAPI.Payloads.Sites;
using System.Diagnostics.CodeAnalysis;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// SitesController
    /// </summary>
    [ApiController]
    [Route("/sites")]
    public class SitesController : ControllerBase
    {
        /// <summary>
        /// PUTs ROOT_SITE_SITES payload
        /// </summary>
        /// <param name="sitesPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("sites")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutSites([FromBody] Payloads.Sites.ROOT_SITE_SITES sitesPayload)
        {
            List<string> Errors = new();
            if (!sitesPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            List<Model.Site> Sites = new();
            var data = sitesPayload.payload;
            foreach (var site in data.sites)
            {
                Model.Site Site = new();
                Site.SiteId = site.siteId;
                Site.LocationId = site.address.lines.Last().entity!.id;
                Site.LocationNaturalId = site.address.lines.Last().entity!.naturalId;
                Site.LocationName = site.address.lines.Last().entity!.name;
                Site.Founded = site.founded.timestamp.FromUnixTime();
                Site.Area = site.area;
                Site.InvestedPermits = site.investedPermits;
                Site.MaximumPermits = site.maximumPermits;

                foreach (var building in site.platforms)
                {
                    Model.SiteBuilding Building = new();
                    Building.SiteBuildingId = building.id;
                    Building.Area = building.area;
                    Building.CreationTime = building.creationTime.timestamp.FromUnixTime();
                    Building.BuildingName = building.module.reactorName;
                    Building.BuildingTicker = building.module.reactorTicker;
                    Building.BuildingType = building.module.type;
                    Building.BookValueCurrencyCode = building.bookValue.currency;
                    Building.BookValueAmount = building.bookValue.amount;
                    Building.Condition = building.condition;
                    Building.LastRepair = building.lastRepair?.timestamp.FromUnixTime();

                    List<string> RawReclaimableList = new();
                    building.reclaimableMaterials.ForEach(rm => RawReclaimableList.Add($"{rm.amount}x{rm.material.ticker}"));
                    Building.RawReclaimableMaterials = string.Join(',', RawReclaimableList);

                    List<string> RawRepairList = new();
                    building.repairMaterials.ForEach(rm => RawRepairList.Add($"{rm.amount}x{rm.material.ticker}"));
                    Building.RawRepairMaterials = string.Join(",", RawRepairList);

                    Building.SiteId = Site.SiteId;
                    Building.Site = Site;
                    Site.Buildings.Add(Building);
                }

                Site.UserNameSubmitted = CurrentUser;
                Site.Timestamp = DateTime.UtcNow;

                Sites.Add(Site);
            }

            if (!Sites.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var AllSiteIds = Sites
                .Select(s => s.SiteId)
                .ToList();

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Sites
                    .Where(s => s.UserNameSubmitted == CurrentUser)
                    .DeleteAsync();

                // Clean up stale experts, workforces, and production lines
                await writer.DB.Experts
                    .Where(e => e.UserNameSubmitted == CurrentUser && !AllSiteIds.Contains(e.ExpertId))
                    .DeleteAsync();
                await writer.DB.Workforces
                    .Where(w => w.UserNameSubmitted == CurrentUser && !AllSiteIds.Contains(w.WorkforceId))
                    .DeleteAsync();
                await writer.DB.ProductionLines
                    .Where(pl => pl.UserNameSubmitted == CurrentUser && !AllSiteIds.Contains(pl.SiteId))
                    .DeleteAsync();

                await writer.DB.Sites
                    .AddRangeAsync(Sites);

                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// PUTs MESG_SITE_SITES payload
        /// </summary>
        /// <param name="msgSitesPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("msg_sites")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMsgSites([FromBody] Payloads.Sites.MESG_SITE_SITES msgSitesPayload)
        {
            List<string> Errors = new();
            if (!msgSitesPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            return await PutSites(msgSitesPayload.payload.message);
        }

        /// <summary>
        /// PUTs ROOT_SITE_PLATFORM_BUILT payload
        /// </summary>
        /// <param name="platformBuiltPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("built")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutBuilt([FromBody] Payloads.Sites.ROOT_SITE_PLATFORM_BUILT platformBuiltPayload)
        {
            List<string> Errors = new();
            if (!platformBuiltPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = platformBuiltPayload.payload;

            SiteBuilding siteBuilding = new();
            siteBuilding.SiteBuildingId = data.id;
            siteBuilding.Area = data.area;
            siteBuilding.CreationTime = data.creationTime.timestamp.FromUnixTime();
            siteBuilding.BuildingName = data.module.reactorName;
            siteBuilding.BuildingTicker = data.module.reactorTicker;
            siteBuilding.BuildingType = data.module.type;
            siteBuilding.BookValueCurrencyCode = data.bookValue.currency;
            siteBuilding.BookValueAmount = data.bookValue.amount;
            siteBuilding.Condition = data.condition;
            siteBuilding.LastRepair = data.lastRepair?.timestamp.FromUnixTime();

            List<string> RawReclaimableList = new();
            data.reclaimableMaterials.ForEach(rm => RawReclaimableList.Add($"{rm.amount}x{rm.material.ticker}"));
            siteBuilding.RawReclaimableMaterials = string.Join(',', RawReclaimableList);

            List<string> RawRepairList = new();
            data.repairMaterials.ForEach(rm => RawRepairList.Add($"{rm.amount}x{rm.material.ticker}"));
            siteBuilding.RawRepairMaterials = string.Join(",", RawRepairList);

            siteBuilding.SiteId = data.siteId;

            List<ValidationIgnore> ignores = new()
            {
                new ValidationIgnore(typeof(SiteBuilding), nameof(SiteBuilding.Site))
            };
            if (!siteBuilding.Validate(ref Errors, ValidationIgnores: ignores))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var site = await writer.DB.Sites
                    .Include(s => s.Buildings)
                    .FirstOrDefaultAsync(s => s.SiteId == siteBuilding.SiteId);
                if (site != null)
                {
                    if (!site.Buildings.Any(b => b.SiteId == siteBuilding.SiteId))
                    {
                        site.Buildings.Add(siteBuilding);

                        await writer.DB.SaveChangesAsync();
                    }
                    
                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs MESG_SITE_PLATFORM_REMOVED payload
        /// </summary>
        /// <param name="platformMsgRemovedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("msg_removed")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMesgRemoved([FromBody] Payloads.Sites.MESG_SITE_PLATFORM_REMOVED platformMsgRemovedPayload)
        {
            List<string> Errors = new();
            if (!platformMsgRemovedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            return await PutRemoved(platformMsgRemovedPayload.payload.message);
        }

        /// <summary>
        /// PUTs ROOT_SITE_PLATFORM_REMOVED payload
        /// </summary>
        /// <param name="platformRemovedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("removed")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutRemoved([FromBody] Payloads.Sites.ROOT_SITE_PLATFORM_REMOVED platformRemovedPayload)
        {
            List<string> Errors = new();
            if (!platformRemovedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = platformRemovedPayload.payload;
            var siteId = data.siteId;
            var buildingId = data.platformId;

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.SiteBuildings
                    .Where(b => b.SiteId == siteId && b.SiteBuildingId == buildingId)
                    .ExecuteDeleteAsync();
            }

            return Ok();
        }

        /// <summary>
        /// PUTs the ROOT_SITE_PLATFORM_UPDATED payload
        /// </summary>
        /// <param name="platformUpdatedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("updated")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutSiteUpdated([FromBody] Payloads.Sites.ROOT_SITE_PLATFORM_UPDATED platformUpdatedPayload)
        {
            List<string> Errors = new();
            if (!platformUpdatedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = platformUpdatedPayload.payload;

            var siteBuilding = new SiteBuilding();
            siteBuilding.SiteBuildingId = data.id;
            siteBuilding.Area = data.area;
            siteBuilding.CreationTime = data.creationTime.timestamp.FromUnixTime();
            siteBuilding.BuildingName = data.module.reactorName;
            siteBuilding.BuildingTicker = data.module.reactorTicker;
            siteBuilding.BuildingType = data.module.type;
            siteBuilding.BookValueCurrencyCode = data.bookValue.currency;
            siteBuilding.BookValueAmount = data.bookValue.amount;
            siteBuilding.Condition = data.condition;
            siteBuilding.LastRepair = data.lastRepair?.timestamp.FromUnixTime();

            List<string> RawReclaimableList = new();
            data.reclaimableMaterials.ForEach(rm => RawReclaimableList.Add($"{rm.amount}x{rm.material.ticker}"));
            siteBuilding.RawReclaimableMaterials = string.Join(',', RawReclaimableList);

            List<string> RawRepairList = new();
            data.repairMaterials.ForEach(rm => RawRepairList.Add($"{rm.amount}x{rm.material.ticker}"));
            siteBuilding.RawRepairMaterials = string.Join(",", RawRepairList);

            siteBuilding.SiteId = data.siteId;

            var ValidationIgnores = new List<ValidationIgnore>
            {
                new ValidationIgnore(typeof(SiteBuilding), nameof(SiteBuilding.Site))
            };

            if (!siteBuilding.Validate(ref Errors, ValidationIgnores: ValidationIgnores))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var existingSite = await writer.DB.Sites
                    .FirstOrDefaultAsync(s => s.SiteId == siteBuilding.SiteId);
                if (existingSite != null)
                {
                    var existingSiteBuilding = await writer.DB.SiteBuildings
                        .FirstOrDefaultAsync(sb => sb.SiteBuildingId == siteBuilding.SiteBuildingId);

                    if (existingSiteBuilding != null)
                    {
                        existingSiteBuilding.BookValueCurrencyCode = siteBuilding.BookValueCurrencyCode;
                        existingSiteBuilding.BookValueAmount = siteBuilding.BookValueAmount;
                        existingSiteBuilding.BuildingName = siteBuilding.BuildingName;
                        existingSiteBuilding.Condition = siteBuilding.Condition;
                        existingSiteBuilding.LastRepair = siteBuilding.LastRepair;
                        existingSiteBuilding.RawReclaimableMaterials = siteBuilding.RawReclaimableMaterials;
                        existingSiteBuilding.RawRepairMaterials = siteBuilding.RawRepairMaterials;
                    }
                    else
                    {
                        await writer.DB.SiteBuildings.AddAsync(siteBuilding);
                    }

                    await writer.DB.SaveChangesAsync();
                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs the MESG_EXPERTS_EXPERTS payload
        /// </summary>
        /// <param name="expertsPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("experts")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutExperts([FromBody] Payloads.Sites.MESG_EXPERTS_EXPERTS expertsPayload)
        {
            List<string> Errors = new();
            if (!expertsPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = expertsPayload.payload.message.payload;
            Expert expert = new()
            {
                ExpertId = data.siteId,
                LocationId = data.address.lines.Last().entity!.id,
                LocationName = data.address.lines.Last().entity!.name,
                LocationNaturalId = data.address.lines.Last().entity!.naturalId,
                Total = data.total,
                Active = data.active,
                TotalActiveCap = data.totalActiveCap,
                UserNameSubmitted = CurrentUser,
                Timestamp = DateTime.UtcNow,
            };

            foreach (var payloadExpert in data.experts)
            {
                expert.ExpertEntries.Add(new ExpertEntry()
                {
                    ExpertEntryId = $"{expert.ExpertId}-{payloadExpert.category}",
                    Category = payloadExpert.category,
                    Current = payloadExpert.entry.current,
                    Limit = payloadExpert.entry.limit,
                    Available = payloadExpert.entry.available,
                    EfficiencyGain = payloadExpert.entry.efficiencyGain,
                    ExpertId = expert.ExpertId,
                    Expert = expert
                });
            }

            if (!expert.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Experts.Upsert(expert)
                    .On(e => new { e.ExpertId })
                    .RunAsync();
            }

            return Ok();
        }

        private static Workforce GenerateWorkforceFromPayload(string siteId, List<MESG_WORKFORCE_WORKFORCES_WORKFORCE> workforces, string currentUser)
        {
            Workforce workforce = new();
            workforce.WorkforceId = siteId;

            var pioneerPayloadData = workforces.FirstOrDefault(w => w.level == "PIONEER");
            var settlerPayloadData = workforces.FirstOrDefault(w => w.level == "SETTLER");
            var technicianPayloadData = workforces.FirstOrDefault(w => w.level == "TECHNICIAN");
            var engineerPayloadData = workforces.FirstOrDefault(w => w.level == "ENGINEER");
            var scientistPayloadData = workforces.FirstOrDefault(w => w.level == "SCIENTIST");

            var GenerateWorkforceNeeds = (List<Payloads.Sites.MESG_WORKFORCE_WORKFORCES_WORKFORCE_NEEDS> Needs, string Level, Workforce Workforce) =>
            {
                return Needs
                    .Select(n => new WorkforceNeed()
                    {
                        WorkforceNeedId = $"{Workforce.WorkforceId}-{Level}-{n.material.ticker}",
                        Level = Level,
                        Category = n.category,
                        Essential = n.essential,
                        MaterialId = n.material.id,
                        MaterialTicker = n.material.ticker,
                        MaterialName = n.material.name,
                        Satisfaction = n.satisfaction,
                        UnitsPerInterval = n.unitsPerInterval,
                        UnitsPer100 = n.unitsPer100,
                        WorkforceId = Workforce.WorkforceId,
                        Workforce = Workforce
                    })
                    .ToList();
            };

            if (pioneerPayloadData != null)
            {
                workforce.PioneerPopulation = pioneerPayloadData.population;
                workforce.PioneerReserve = pioneerPayloadData.reserve;
                workforce.PioneerCapacity = pioneerPayloadData.capacity;
                workforce.PioneerRequired = pioneerPayloadData.required;
                workforce.PioneerSatisfaction = pioneerPayloadData.satisfaction;

                workforce.WorkforceNeeds.AddRange(GenerateWorkforceNeeds(pioneerPayloadData.needs, pioneerPayloadData.level, workforce));
            }

            if (settlerPayloadData != null)
            {
                workforce.SettlerPopulation = settlerPayloadData.population;
                workforce.SettlerReserve = settlerPayloadData.reserve;
                workforce.SettlerCapacity = settlerPayloadData.capacity;
                workforce.SettlerRequired = settlerPayloadData.required;
                workforce.SettlerSatisfaction = settlerPayloadData.satisfaction;

                workforce.WorkforceNeeds.AddRange(GenerateWorkforceNeeds(settlerPayloadData.needs, settlerPayloadData.level, workforce));
            }

            if (technicianPayloadData != null)
            {
                workforce.TechnicianPopulation = technicianPayloadData.population;
                workforce.TechnicianReserve = technicianPayloadData.reserve;
                workforce.TechnicianCapacity = technicianPayloadData.capacity;
                workforce.TechnicianRequired = technicianPayloadData.required;
                workforce.TechnicianSatisfaction = technicianPayloadData.satisfaction;

                workforce.WorkforceNeeds.AddRange(GenerateWorkforceNeeds(technicianPayloadData.needs, technicianPayloadData.level, workforce));
            }

            if (engineerPayloadData != null)
            {
                workforce.EngineerPopulation = engineerPayloadData.population;
                workforce.EngineerReserve = engineerPayloadData.reserve;
                workforce.EngineerCapacity = engineerPayloadData.capacity;
                workforce.EngineerRequired = engineerPayloadData.required;
                workforce.EngineerSatisfaction = engineerPayloadData.satisfaction;

                workforce.WorkforceNeeds.AddRange(GenerateWorkforceNeeds(engineerPayloadData.needs, engineerPayloadData.level, workforce));
            }

            if (scientistPayloadData != null)
            {
                workforce.ScientistPopulation = scientistPayloadData.population;
                workforce.ScientistReserve = scientistPayloadData.reserve;
                workforce.ScientistCapacity = scientistPayloadData.capacity;
                workforce.ScientistRequired = scientistPayloadData.required;
                workforce.ScientistSatisfaction = scientistPayloadData.satisfaction;

                workforce.WorkforceNeeds.AddRange(GenerateWorkforceNeeds(scientistPayloadData.needs, scientistPayloadData.level, workforce));
            }

            workforce.UserNameSubmitted = currentUser;
            workforce.Timestamp = DateTime.UtcNow;

            return workforce;
        }

        /// <summary>
        /// PUTs the WORKFORCE_WORKFORCES payload
        /// </summary>
        /// <param name="workforcesPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("workforces")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutWorkforces([FromBody] Payloads.Sites.MESG_WORKFORCE_WORKFORCES workforcesPayload)
        {
            List<string> Errors = new();
            if (!workforcesPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var payload = workforcesPayload.payload.message.payload;

            Workforce workforce = GenerateWorkforceFromPayload(payload.siteId, payload.workforces, CurrentUser);

            if (!workforce.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            List<Model.WorkforceRequirement>? WorkforceRequirements = null;
            bool HasAdminWrite = this.HasAdminWrite();
            if (HasAdminWrite)
            {
                WorkforceRequirements = new();
                foreach (var wf in payload.workforces)
                {
                    Model.WorkforceRequirement WorkforceRequirement = new();
                    WorkforceRequirement.Level = wf.level;
                    WorkforceRequirement.UserNameSubmitted = this.GetUserName()!;
                    WorkforceRequirement.Timestamp = DateTime.UtcNow;
                    foreach (var need in wf.needs)
                    {
                        if (need.unitsPer100 > 0.0)
                        {
                            Model.WorkforceRequirementNeed WorkforceRequirementNeed = new();
                            WorkforceRequirementNeed.Essential = need.essential;
                            WorkforceRequirementNeed.MaterialName = need.material.name;
                            WorkforceRequirementNeed.MaterialTicker = need.material.ticker;
                            WorkforceRequirementNeed.MaterialId = need.material.id;
                            WorkforceRequirementNeed.UnitsPer100 = need.unitsPer100;
                            WorkforceRequirement.Needs.Add(WorkforceRequirementNeed);
                        }
                    }

                    if (!WorkforceRequirement.Validate(ref Errors))
                    {
                        return BadRequest(Errors);
                    }

                    WorkforceRequirements.Add(WorkforceRequirement);
                }
            }

            using (var writer = DBAccess.GetWriter())
            {
                if (WorkforceRequirements != null)
                {
                    await writer.DB.WorkforceRequirements.DeleteAsync();
                    await writer.DB.WorkforceRequirements.AddRangeAsync(WorkforceRequirements);
                }

                await writer.DB.Workforces
                    .Where(w => w.WorkforceId == workforce.WorkforceId)
                    .DeleteAsync();

                await writer.DB.Workforces.AddAsync(workforce);

                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// PUTs the ROOT_WORKFORCE_WORKFORCES_UPDATED payload
        /// </summary>
        /// <param name="workforcesUpdatedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("workforces/updated")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutWorkforceUpdated([FromBody] Payloads.Sites.ROOT_WORKFORCE_WORKFORCES_UPDATED workforcesUpdatedPayload)
        {
            List<string> Errors = new();
            if (!workforcesUpdatedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var siteId = workforcesUpdatedPayload.payload.siteId;
            var workforces = workforcesUpdatedPayload.payload.workforces;

            Workforce newWorkforce = GenerateWorkforceFromPayload(siteId, workforces, CurrentUser);
            newWorkforce.LastTickTime = DateTime.UtcNow;

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Workforces
                    .Where(w => w.UserNameSubmitted == CurrentUser && w.WorkforceId == newWorkforce.WorkforceId)
                    .DeleteAsync();

                await writer.DB.AddAsync(newWorkforce);

                await writer.DB.SaveChangesAsync();
            }
            
            return Ok();
        }

        /// <summary>
        /// Retrieves Sites for the provided UserName(s)
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group">The group</param>
        /// <param name="include_buildings">If buildings should be included</param>
        /// <returns>List of Sites</returns>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<Site>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetSites(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_buildings")] bool include_buildings = false)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<Model.Site>> UserToSites;
            using (var reader = DBAccess.GetReader())
            {
                var UserToSitesQuery = reader.DB.Sites.AsQueryable();
                if (include_buildings)
                {
                    UserToSitesQuery = UserToSitesQuery.Include(s => s.Buildings);
                }

                UserToSites = await UserToSitesQuery
                    .Where(s => UserToPerm.Keys.Contains(s.UserNameSubmitted))
                    .GroupBy(s => s.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToSites, UserToPerm);
            return Ok(Response);
        }

        /// <summary>
        /// Retrieves Experts for the provided UserName(s)
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group">The group</param>
        /// <returns>List of experts</returns>
        [HttpGet("experts")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<Expert>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetExperts(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<Model.Expert>> UserToExperts;
            using (var reader = DBAccess.GetReader())
            {
                UserToExperts = await reader.DB.Experts
                    .Include(e => e.ExpertEntries)
                    .Where(e => UserToPerm.Keys.Contains(e.UserNameSubmitted))
                    .GroupBy(e => e.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToExperts, UserToPerm);
            return Ok(Response);
        }

        /// <summary>
        /// Retrieves Workforces for the provided UserName(s)
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group">The group</param>
        /// <param name="include_needs">If workforce needs should be included</param>
        /// <returns>List of workforces</returns>
        [HttpGet("workforces")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<Workforce>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetWorkforces(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_needs")] bool include_needs = true)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<Model.Workforce>> UserToWorkforces;
            using (var reader = DBAccess.GetReader())
            {
                var UserToWorkforcesQuery = reader.DB.Workforces.AsQueryable();
                if (include_needs)
                {
                    UserToWorkforcesQuery = UserToWorkforcesQuery.Include(w => w.WorkforceNeeds);
                }

                UserToWorkforces = await UserToWorkforcesQuery
                    .Where(w => UserToPerm.Keys.Contains(w.UserNameSubmitted))
                    .GroupBy(w => w.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToWorkforces, UserToPerm);
            return Ok(Response);
        }
        
    }
}
