﻿using FIOAPI.DB.Model;
using FIOAPI.Payloads.CSV;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// CSVController
    /// </summary>
    [ApiController]
    [Route("/csv")]
    public class CSVController : ControllerBase
    {
        /// <summary>
        /// Retrieves buildings
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV file</returns>
        [HttpGet("buildings")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetBuildings(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<BuildingController>().GetAllBuildings();
            var buildings = result.GetObject<List<Model.Building>?>();
            if (buildings != null)
            {
                var CsvBuildings = buildings
                    .Select(b => new CsvBuilding(b))
                    .OrderBy(b => b.Ticker)
                    .ToList();

                return CsvBuildings.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves building costs
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV file</returns>
        [HttpGet("buildingcosts")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetBuildingCosts(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<BuildingController>().GetAllBuildings(include_costs: true);
            var buildings = result.GetObject<List<Model.Building>?>();
            if (buildings != null)
            {
                var CsvBuildingCosts = buildings
                    .SelectMany(b => b.Costs)
                    .Select(c => new CsvBuildingCost(c))
                    .OrderBy(c => c.Key)
                    .ToList();

                return CsvBuildingCosts.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        private static void PopulateCsvWorkforce(ref List<CsvBuildingWorkforce> CsvBuildingWorkforces, int workforceCount, string buildingTicker, string workforceType)
        {
            if (workforceCount > 0)
            {
                CsvBuildingWorkforces.Add(new CsvBuildingWorkforce()
                {
                    Key = $"{buildingTicker}-{workforceType}",
                    Building = buildingTicker,
                    Level = workforceType,
                    Capacity = workforceCount,
                });
            }
        }

        /// <summary>
        /// Retrieves building workforces
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("buildingworkforces")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetBuildingWorkforces(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<BuildingController>().GetAllBuildings();
            var buildings = result.GetObject<List<Model.Building>?>();
            if (buildings != null)
            {
                List<CsvBuildingWorkforce> CsvBuildingWorkforces = new();
                foreach (var building in buildings)
                {
                    PopulateCsvWorkforce(ref CsvBuildingWorkforces, building.Pioneers, building.Ticker, "PIONEER");
                    PopulateCsvWorkforce(ref CsvBuildingWorkforces, building.Settlers, building.Ticker, "SETTLER");
                    PopulateCsvWorkforce(ref CsvBuildingWorkforces, building.Technicians, building.Ticker, "TECHNICIAN");
                    PopulateCsvWorkforce(ref CsvBuildingWorkforces, building.Engineers, building.Ticker, "ENGINEER");
                    PopulateCsvWorkforce(ref CsvBuildingWorkforces, building.Scientists, building.Ticker, "SCIENTIST");
                }

                return CsvBuildingWorkforces.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves building recipes
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("buildingrecipes")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetBuildingRecipes(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<BuildingController>().GetAllBuildings(include_recipes: true);
            var buildings = result.GetObject<List<Model.Building>?>();
            if (buildings != null)
            {
                List<CsvBuildingRecipe> CsvBuildingRecipes = new();
                foreach (var building in buildings)
                {
                    CsvBuildingRecipes.AddRange(building.Recipes
                        .Select(r => new CsvBuildingRecipe
                        {
                            Key = r.BuildingRecipeId,
                            Building = building.Ticker,
                            Duration = r.DurationMs
                        }));
                }

                return CsvBuildingRecipes
                    .OrderBy(c => c.Key)
                    .ToList()
                    .GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Gets all materials
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("materials")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetMaterials(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            List<string> Empty = new();
            var result = await this.GetController<MaterialController>().GetMaterials(Empty, Empty, Empty, Empty);
            var materials = result.GetObject<List<Model.Material>>();
            if (materials != null)
            {
                var CsvMaterials = materials
                    .Select(m => new CsvMaterial(m))
                    .OrderBy(cm => cm.Ticker)
                    .ToList();
                return CsvMaterials.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Gets all materials
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("prices")]
        [AllowAnonymous]
        [Cache(Minutes = 15)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetPrices(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<CXController>().GetAllPrices();
            var cxEntries = result.GetObject<List<Model.CXEntry>>();
            if (cxEntries != null)
            {
                // Get all tickers
                var tickers = cxEntries
                    .Select(cx => cx.MaterialTicker)
                    .Distinct()
                    .OrderBy(m => m)
                    .ToList();

                List<CsvPrices> CsvPrices = new();
                foreach (var ticker in tickers)
                {
                    var mmbuy = cxEntries.FirstOrDefault(c => c.MaterialTicker == ticker && c.MMBuy != null)?.MMBuy;
                    var mmsell = cxEntries.FirstOrDefault(c => c.MaterialTicker == ticker && c.MMSell != null)?.MMSell;

                    var ai1 = cxEntries.FirstOrDefault(c => c.MaterialTicker == ticker && c.ExchangeCode == "AI1");
                    var ci1 = cxEntries.FirstOrDefault(c => c.MaterialTicker == ticker && c.ExchangeCode == "CI1");
                    var ci2 = cxEntries.FirstOrDefault(c => c.MaterialTicker == ticker && c.ExchangeCode == "CI2");
                    var nc1 = cxEntries.FirstOrDefault(c => c.MaterialTicker == ticker && c.ExchangeCode == "NC1");
                    var nc2 = cxEntries.FirstOrDefault(c => c.MaterialTicker == ticker && c.ExchangeCode == "NC2");
                    var ic1 = cxEntries.FirstOrDefault(c => c.MaterialTicker == ticker && c.ExchangeCode == "IC1");

                    CsvPrices.Add(new CsvPrices(ticker, mmbuy, mmsell, ai1, ci1, ci2, nc1, nc2, ic1));
                }

                return CsvPrices.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves all orders
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("orders")]
        [AllowAnonymous]
        [Cache(Minutes = 15)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetOrders(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<CXController>().GetAllPrices(include_buy_orders: true);
            var cxEntries = result.GetObject<List<Model.CXEntry>>();
            if (cxEntries != null)
            {
                var CsvBuyOrders = cxEntries
                    .SelectMany(cx => cx.BuyOrders)
                    .Select(bo => new CsvOrderOrBid(bo))
                    .OrderBy(bo => bo.ExchangeCode)
                        .ThenBy(bo => bo.MaterialTicker)
                    .ToList();
                return CsvBuyOrders.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves all bids
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("bids")]
        [AllowAnonymous]
        [Cache(Minutes = 15)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetBids(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<CXController>().GetAllPrices(include_sell_orders: true);
            var cxEntries = result.GetObject<List<Model.CXEntry>>();
            if (cxEntries != null)
            {
                var CsvSellOrders = cxEntries
                    .SelectMany(cx => cx.SellOrders)
                    .Select(so => new CsvOrderOrBid(so))
                    .OrderBy(so => so.ExchangeCode)
                        .ThenBy(so => so.MaterialTicker)
                    .ToList();
                return CsvSellOrders.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves all recipe inputs
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("recipeinputs")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetRecipeInputs(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<BuildingController>().GetAllBuildings(include_recipes: true);
            var buildings = result.GetObject<List<Model.Building>>();
            if (buildings != null)
            {
                var CsvInputs = buildings
                    .SelectMany(b => b.Recipes)
                    .SelectMany(r => r.Inputs)
                    .Select(i => new CsvRecipeItem(i))
                    .OrderBy(i => i.Key)
                    .ToList();
                return CsvInputs.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves all recipe outputs
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("recipeoutputs")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetRecipeOutputs(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<BuildingController>().GetAllBuildings(include_recipes: true);
            var buildings = result.GetObject<List<Model.Building>>();
            if (buildings != null)
            {
                var CsvOutputs = buildings
                    .SelectMany(b => b.Recipes)
                    .SelectMany(r => r.Outputs)
                    .Select(i => new CsvRecipeItem(i))
                    .OrderBy(i => i.Key)
                    .ToList();
                return CsvOutputs.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves all planets
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("planets")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetPlanets(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<PlanetController>().GetAllPlanets(include_resources: false);
            var planets = result.GetObject<List<Model.Planet>>();
            if (planets != null)
            {
                var CsvPlanets = planets
                    .Select(p => new CsvPlanet(p))
                    .OrderBy(p => p.PlanetNaturalId)
                    .ToList();
                return CsvPlanets.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves all planet resources
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("planetresources")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetPlanetResources(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<PlanetController>().GetAllPlanets(include_resources: true);
            var planets = result.GetObject<List<Model.Planet>>();
            if (planets != null)
            {
                var CsvPlanets = planets
                    .SelectMany(p => p.Resources)
                    .Select(p => new CsvPlanetResource(p))
                    .OrderBy(p => p.PlanetNaturalId)
                        .ThenBy(p => p.Ticker)
                    .ToList();
                return CsvPlanets.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Get all planet production fees
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("planetproductionfees")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetPlanetProductionFees(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<PlanetController>().GetAllPlanets(include_resources: false);
            var planets = result.GetObject<List<Model.Planet>>();
            if (planets != null)
            {
                var CsvPlanetProductionFees = planets
                    .SelectMany(p => p.WorkforceFees)
                    .Select(wf => new CsvPlanetProductionFee(wf))
                    .OrderBy(wf => wf.PlanetNaturalId)
                        .ThenBy(wf => wf.WorkforceLevel)
                    .ToList();
                return CsvPlanetProductionFees.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves planet detail information
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("planetdetail")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetPlanetDetail(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<PlanetController>().GetAllPlanets(include_resources: false);
            var planets = result.GetObject<List<Model.Planet>>();
            if (planets != null)
            {
                var CsvPlanetDetails = planets
                    .Select(p => new CsvPlanetDetail(p))
                    .OrderBy(p => p.PlanetNaturalId)
                    .ToList();
                return CsvPlanetDetails.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves system information
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("systems")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetSystems(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<MapController>().GetStars();
            var systems = result.GetObject<List<Model.System>>();
            if (systems != null)
            {
                var CsvPlanetDetails = systems
                    .Select(s => new CsvSystem(s))
                    .OrderBy(s => s.NaturalId)
                    .ToList();
                return CsvPlanetDetails.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves system links
        /// </summary>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("systemlinks")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetSystemLinks(
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<MapController>().GetStars();
            var systems = result.GetObject<List<Model.System>>();
            if (systems != null)
            {
                var CsvSystemLinks = new List<CsvSystemLink>();
                foreach (var system in systems)
                {
                    var LeftNaturalId = system.NaturalId;

                    var RightSideNaturalIds = new List<string>();
                    foreach (var connection in system.Connections)
                    {
                        var otherSystem = systems.First(s => s.SystemId == connection);
                        if (otherSystem != null)
                        {
                            RightSideNaturalIds.Add(otherSystem.NaturalId);
                        }
                    }
                    RightSideNaturalIds.Sort();
                    foreach (var RightNaturalId in RightSideNaturalIds)
                    {
                        var link = new CsvSystemLink();

                        link.Left = LeftNaturalId;
                        link.Right = RightNaturalId;

                        CsvSystemLinks.Add(link);
                    }
                }

                var CsvPlanetDetails = systems
                    .Select(s => new CsvSystem(s))
                    .OrderBy(s => s.NaturalId)
                    .ToList();
                return CsvPlanetDetails.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves planet reports
        /// </summary>
        /// <param name="planet">Planet(s) to retrieve</param>
        /// <param name="count">Number of reports to retrieve. Defaults to 5. Specify -1 for all.</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("planetreports")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        public async Task<IActionResult> GetPlanetReports(
            [FromQuery(Name = "planet")] List<string>? planet,
            [FromQuery(Name = "count")] int count = 5,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<PlanetController>().GetPlanet(planet, include_resources: false, include_population_reports: true);
            var planets = result.GetObject<List<Model.Planet>>();
            if (planets != null)
            {
                List<CsvPlanetPopulationReport> CsvPlanetPopulationReports = new();
                foreach (var p in planets)
                {
                    var popReports = (count == -1) ? p.PopulationReports.ToList() : 
                        p.PopulationReports
                            .OrderByDescending(pr => pr.SimulationPeriod)
                            .Take(count)
                            .ToList();

                    foreach (var popReport in popReports)
                    {
                        CsvPlanetPopulationReports.Add(new CsvPlanetPopulationReport(popReport));
                    }
                }

                return CsvPlanetPopulationReports.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves LM Ads
        /// </summary>
        /// <param name="planet">Planet(s) to search</param>
        /// <param name="include_buys">If buys should be included</param>
        /// <param name="include_sells">If sells should be included</param>
        /// <param name="include_shipments">If shipments should be included</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("localmarket")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request - Did not specify planets or any include")]
        public async Task<IActionResult> GetLocalMarket(
            [FromQuery(Name = "planet")] List<string>? planet,
            [FromQuery(Name = "include_buys")] bool include_buys = true,
            [FromQuery(Name = "include_sells")] bool include_sells = true,
            [FromQuery(Name = "include_shipments")] bool include_shipments = true,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var result = await this.GetController<LocalMarketController>().GetLocalMarketAds(planet, include_buys, include_sells, include_shipments);
            var lmAds = result.GetObject<List<Model.LocalMarketAd>>();
            if (lmAds != null)
            {
                var CsvLMAds = lmAds
                    .Select(l => new CsvLMAd(l))
                    .OrderBy(l => l.PlanetNaturalId)
                        .ThenBy(l => l.AdNaturalId)
                    .ToList();

                return CsvLMAds.GetCSVResponse(IncludeHeader: include_header);
            }

            return BadRequest();
        }

        /// <summary>
        /// Retrieves workforce data
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("workforces")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetWorkforce(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var controller = await this.GetController<SitesController>(apikey);
            if (controller != null)
            {
                var workforcesResult = await controller.GetWorkforces(username, group, include_needs: false);
                var workforcesRetrievalResponse = workforcesResult.GetObject<RetrievalResponse<List<Model.Workforce>>>();

                var sitesResult = await controller.GetSites(username, group);
                var sitesRetrievalResponse = sitesResult.GetObject<RetrievalResponse<List<Model.Site>>>();

                List<CsvWorkforce> CsvWorkforces = new();
                if (workforcesRetrievalResponse != null && sitesRetrievalResponse != null)
                {
                    foreach (var kvp in workforcesRetrievalResponse.UserNameToData)
                    {
                        var userName = kvp.Key;
                        var workforces = kvp.Value.Data;
                        var sites = sitesRetrievalResponse.UserNameToData[userName].Data;
                        CsvWorkforces.AddRange(workforces.Select(w => new CsvWorkforce(w, sites)));
                    }

                    return CsvWorkforces.GetCSVResponse(IncludeHeader: include_header);
                }

                return BadRequest();
            }

            return Unauthorized();
        }

        /// <summary>
        /// Retrieves workforce needs
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("workforceneeds")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetWorkforceNeeds(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var controller = await this.GetController<SitesController>(apikey);
            if (controller != null)
            {
                var workforcesResult = await controller.GetWorkforces(username, group);
                var workforcesRetrievalResponse = workforcesResult.GetObject<RetrievalResponse<List<Model.Workforce>>>();

                var sitesResult = await controller.GetSites(username, group);
                var sitesRetrievalResponse = sitesResult.GetObject<RetrievalResponse<List<Model.Site>>>();

                List<CsvWorkforceNeed> CsvWorkforceNeeds = new();
                if (workforcesRetrievalResponse != null && sitesRetrievalResponse != null)
                {
                    foreach (var kvp in workforcesRetrievalResponse.UserNameToData)
                    {
                        var userName = kvp.Key;
                        var workforces = kvp.Value.Data;
                        var sites = sitesRetrievalResponse.UserNameToData[userName].Data;

                        var workforceNeeds = workforces.SelectMany(w => w.WorkforceNeeds);
                        foreach (var workforceNeed in workforceNeeds)
                        {
                            if (workforceNeed.UnitsPerInterval > 0.0)
                            {
                                CsvWorkforceNeeds.Add(new CsvWorkforceNeed(userName, workforceNeed, sites));
                            }
                        }
                    }

                    return CsvWorkforceNeeds.GetCSVResponse(IncludeHeader: include_header);
                }

                return BadRequest();
            }

            return Unauthorized();
        }

        /// <summary>
        /// Retrieves inventory
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_bases">If bases should be included</param>
        /// <param name="include_warehouses">If warehouses should be included</param>
        /// <param name="include_ship_stores">If ship stores should be included</param>
        /// <param name="include_ship_stl_stores">If ship stl fuel stores should be included</param>
        /// <param name="include_ship_ftl_stores">If ship ftl fuel stores should be included</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("inventory")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetInventory(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_bases")] bool include_bases = true,
            [FromQuery(Name = "include_warehouses")] bool include_warehouses = true,
            [FromQuery(Name = "include_ship_stores")] bool include_ship_stores = true,
            [FromQuery(Name = "include_ship_stl_stores")] bool include_ship_stl_stores = false,
            [FromQuery(Name = "include_ship_ftl_stores")] bool include_ship_ftl_stores = false,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var storageController = await this.GetController<StorageController>(apikey);
            if (storageController != null)
            {
                var result = await storageController.GetStorage(username, group, include_bases, include_warehouses, include_ship_stores, include_ship_stl_stores, include_ship_ftl_stores, include_items: true);
                var storageRetrievalResponse = result.GetObject<RetrievalResponse<List<Model.Storage>>>();
                if (storageRetrievalResponse != null)
                {
                    var CsvStorageItems = storageRetrievalResponse.UserNameToData
                        .SelectMany(kvp => kvp.Value.Data
                            .SelectMany(s => s.StorageItems)
                            .Select(si => new CsvInventoryItem(si)))
                        .OrderBy(si => si.NaturalId)
                            .ThenBy(si => si.Ticker)
                        .ToList();

                    return CsvStorageItems.GetCSVResponse(IncludeHeader: include_header);
                }

                return BadRequest();
            }

            return Unauthorized();
        }

        //[HttpGet("infrastructureinfos")] // ?planet=
        //[HttpGet("burnrate")] // APIKey


        /// <summary>
        /// Retrieves reclaimables
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("reclaimables")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetReclaimables(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var sitesController = await this.GetController<SitesController>(apikey);
            if (sitesController != null)
            {
                var result = await sitesController.GetSites(username, group, include_buildings: true);
                var sites = result.GetObject<RetrievalResponse<List<Model.Site>>>();
                if (sites != null)
                {
                    var buildings = sites.UserNameToData
                        .SelectMany(kvp => kvp.Value.Data)
                        .SelectMany(s => s.Buildings)
                        .ToList();

                    List<CsvRepairablesOrReclaimables> CsvReclaimables = new();
                    foreach (var building in buildings)
                    {
                        CsvReclaimables.AddRange(building.ReclaimableMaterials.Select(rm => new CsvRepairablesOrReclaimables(building, rm)));
                    }

                    return CsvReclaimables.GetCSVResponse(IncludeHeader: include_header);
                }

                return BadRequest();
            }

            return Unauthorized();
        }

        /// <summary>
        /// Retrieves repairs
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("repairs")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetRepairs(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var sitesController = await this.GetController<SitesController>(apikey);
            if (sitesController != null)
            {
                var result = await sitesController.GetSites(username, group, include_buildings: true);
                var sites = result.GetObject<RetrievalResponse<List<Model.Site>>>();
                if (sites != null)
                {
                    var buildings = sites.UserNameToData
                        .SelectMany(kvp => kvp.Value.Data)
                        .SelectMany(s => s.Buildings)
                        .ToList();

                    List<CsvRepairablesOrReclaimables> CsvReclaimables = new();
                    foreach (var building in buildings)
                    {
                        CsvReclaimables.AddRange(building.RepairMaterials.Select(rm => new CsvRepairablesOrReclaimables(building, rm)));
                    }

                    return CsvReclaimables.GetCSVResponse(IncludeHeader: include_header);
                }

                return BadRequest();
            }

            return Unauthorized();
        }

        /// <summary>
        /// Retrieves cxos
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("cxos")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetCXOS(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var cxController = await this.GetController<CXController>(apikey);
            if (cxController != null)
            {
                var result = await cxController.GetCXOS(username, group, include_trades: false);
                var cxosEntries = result.GetObject<RetrievalResponse<List<Model.CXOS>>>();
                if (cxosEntries != null)
                {
                    var CsvCXOSEntries = cxosEntries.UserNameToData
                        .SelectMany(kvp => kvp.Value.Data)
                        .Select(cxos => new CsvCXOS(cxos))
                        .OrderByDescending(cxos => cxos.CreatedEpochMs)
                        .ToList();

                    return CsvCXOSEntries.GetCSVResponse(IncludeHeader: include_header);
                }

                return BadRequest();
            }

            return Unauthorized();
        }

        /// <summary>
        /// Retrieves balances
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("balances")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetBalances(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var companyController = await this.GetController<CompanyController>(apikey);
            if (companyController != null)
            {
                var result = await companyController.GetCompany(username, group, include_planets: false, include_offices: false);
                var companiesRetrievalResponse = result.GetObject<RetrievalResponse<Model.Company>>();
                if (companiesRetrievalResponse != null)
                {
                    var companies = companiesRetrievalResponse.UserNameToData.Values.Select(v => v.Data);

                    List<CsvBalance> CsvBalances = new();
                    foreach (var company in companies)
                    {
                        CsvBalances.Add(new CsvBalance { UserName = company.UserName, Currency = "AIC", Amount = company.AICLiquid });
                        CsvBalances.Add(new CsvBalance { UserName = company.UserName, Currency = "CIS", Amount = company.CISLiquid });
                        CsvBalances.Add(new CsvBalance { UserName = company.UserName, Currency = "ECD", Amount = company.ECDLiquid });
                        CsvBalances.Add(new CsvBalance { UserName = company.UserName, Currency = "NCC", Amount = company.NCCLiquid });
                        CsvBalances.Add(new CsvBalance { UserName = company.UserName, Currency = "ICA", Amount = company.ICALiquid });
                    }

                    return CsvBalances.GetCSVResponse(IncludeHeader: include_header);
                }

                return BadRequest();
            }

            return Unauthorized();
        }

        /// <summary>
        /// Retrieves burn
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("burn")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetBurn(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_header")] bool include_header = true
            )
        {
            var dataController = await this.GetController<DataController>(apikey);
            if (dataController != null)
            {
                var result = await dataController.GetBurn(username, group);
                var burnRetrievalResponse = result.GetObject<RetrievalResponse<List<Payloads.Data.Burn>>>();
                if (burnRetrievalResponse != null)
                {
                    List<CsvBurn> CsvBurns = new();
                    foreach (var kvp in burnRetrievalResponse.UserNameToData)
                    {
                        var userName = kvp.Key;
                        var burns = kvp.Value.Data;
                        foreach (var burn in burns)
                        {
                            CsvBurns.AddRange(burn.Inventory.Select(i => new CsvBurn(userName, burn, i)));
                        }
                    }

                    return CsvBurns.GetCSVResponse(IncludeHeader: include_header);
                }
            }

            return Unauthorized();
        }

        /// <summary>
        /// Retrieves sites
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <returns>CSV File</returns>
        [HttpGet("sites")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetSites(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_header")] bool include_header = true)
        {
            var sitesController = await this.GetController<SitesController>(apikey);
            if (sitesController != null)
            {
                var result = await sitesController.GetSites(username, group, include_buildings: true);
                var sites = result.GetObject<RetrievalResponse<List<Model.Site>>>();
                if (sites != null)
                {
                    var CsvSites = sites.UserNameToData
                        .SelectMany(kvp => kvp.Value.Data
                            .SelectMany(s => s.Buildings)
                            .Select(b => new CsvSite(kvp.Key, b)))
                        .OrderBy(b => b.LocationNaturalId)
                        .ToList();

                    return CsvSites.GetCSVResponse(IncludeHeader: include_header);
                }

                return BadRequest();
            }

            return Unauthorized();
        }

        /// <summary>
        /// Retrieve ships
        /// </summary>
        /// <param name="apikey">APIKey to use</param>
        /// <param name="username">UserName(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_header">If the csv header should be present</param>
        /// <param name="include_flight_info">If the flight info should be present</param>
        /// <returns></returns>
        [HttpGet("ships")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK,"Success", typeof(string), "text/csv")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Bad request")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid or unspecified APIKey")]
        public async Task<IActionResult> GetShips(
            [FromQuery(Name = "apikey")] string apikey,
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_header")] bool include_header = true,
            [FromQuery(Name = "include_flight_info")] bool include_flight_info = true)
        {
            var dataController = await this.GetController<DataController>(apikey);
            if (dataController != null)
            {
                var result = await dataController.GetAllData(username, group, include_ship_data: true, include_flight_data: true, include_storage_data: true);
                var allDataResponse = result.GetObject<RetrievalResponse<List<Payloads.Data.AllData>>>();
                if (allDataResponse != null)
                {
                    List<CsvShipInventory> shipInventories = new List<CsvShipInventory>();
                    foreach (var kvp in allDataResponse.UserNameToData)
                    {
                        var dataUserName = kvp.Key;
                        var dataEntries = kvp.Value.Data;
                        foreach (var dataEntry in dataEntries)
                        {
                            // Iterate over every ship and find corresponding data
                            if (dataEntry.Ships != null)
                            {
                                foreach (var ship in dataEntry.Ships)
                                {
                                    var flight = dataEntry.Flights?.FirstOrDefault(f => f.FlightId == ship.FlightId);
                                    var shipStorage = dataEntry.Storages?.FirstOrDefault(s => s.StorageId == ship.StoreId);
                                    var sfStorage = dataEntry.Storages?.FirstOrDefault(s => s.StorageId == ship.STLFuelStoreId);
                                    var ffStorage = dataEntry.Storages?.FirstOrDefault(s => s.StorageId == ship.FTLFuelStoreId);

                                    var shipTemplate = new CsvShipInventory(ship, flight, shipStorage, sfStorage, ffStorage);
                                    if (shipStorage != null)
                                    {
                                        foreach (var storageItem in shipStorage.StorageItems)
                                        {
                                            var shipStorageEntry = (CsvShipInventory)shipTemplate.Clone();
                                            shipStorageEntry.MaterialTicker = storageItem.MaterialTicker ?? storageItem.ItemId;
                                            shipStorageEntry.MaterialAmount = storageItem.Amount ?? 1;
                                            shipInventories.Add(shipStorageEntry);
                                        }
                                    }
                                    else
                                    {
                                        shipInventories.Add(shipTemplate);
                                    }
                                }
                            }
                        }
                    }

                    return shipInventories.GetCSVResponse(IncludeHeader: include_header);
                }

                return BadRequest();
            }

            return Unauthorized();
        }
    }
}
