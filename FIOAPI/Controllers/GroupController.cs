﻿using FIOAPI.DB.Model;
using FIOAPI.Payloads.Group;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// Group endpoint
    /// </summary>
    [ApiController]
    [Route("/group")]
    public class GroupController : ControllerBase
    {
        /// <summary>
        /// The maximum number of groups a user can be owner of
        /// </summary>
        public const int MaxGroupCount = 10;

        /// <summary>
        /// GroupController constructor
        /// </summary>
        public GroupController()
        {

        }

        private static Random rnd = new Random();

        /// <summary>
        /// Creates a group
        /// </summary>
        /// <param name="payload">The create group payload</param>
        [HttpPost("create")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully created group", typeof(CreateResponse), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status406NotAcceptable, "Reached maximum threshold of groups or requested id is already present", typeof(string), "text/plain")]
        public async Task<IActionResult> CreateAsync([FromBody] Create payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            var currentUserName = this.GetUserName()!;

            using (var writer = DBAccess.GetWriter())
            {
                var groupCount = await writer.DB.Groups.Where(g => g.GroupOwner == currentUserName).CountAsync();
                if (groupCount >= MaxGroupCount) 
                {
                    return StatusCode(406, $"Reached maximum threshold of {MaxGroupCount} groups.  Cannot create more.");
                }

                if (payload.RequestedId > 0)
                {
                    if (await writer.DB.Groups.AnyAsync(g => g.GroupId == payload.RequestedId))
                    {
                        return StatusCode(406, $"Requested id {payload.RequestedId} in use.");
                    }
                }

                var inviteUserNames = payload.Invites
                    .Select(i => i.UserName.ToLower());

                // Filter down to only the valid usernames
                var validUserInvites = writer.DB.Users
                    .Where(u => inviteUserNames.Contains(u.UserName))
                    .Select(u => u.UserName)
                    .Distinct()
                    .ToList();

                // Remove self
                validUserInvites.Remove(currentUserName);

                // @TODO: Error on no validUserInvites?

                var PendingInvites = validUserInvites
                    .Select(vui => new Model.GroupPendingInvite
                    {
                        UserName = vui,
                        Admin = payload.Invites.First(i => i.UserName.ToLower() == vui).Admin
                    })
                    .ToList();

                bool NeedToGenerateGroupId = (payload.RequestedId == 0);
                if (NeedToGenerateGroupId)
                {
                    var GroupIds = await writer.DB.Groups.Select(g => g.GroupId).ToListAsync();

                    int GeneratedId = 0;
                    do
                    {
                        GeneratedId = rnd.Next(1, Model.Group.LargestGroupId + 1);
                    } while (GroupIds.Any(g => g == GeneratedId));

                    payload.RequestedId = GeneratedId;
                }

                var Group = new Model.Group
                {
                    GroupId = payload.RequestedId,
                    GroupName = payload.GroupName,
                    GroupOwner = currentUserName,
                };

                PendingInvites.ForEach(pi =>
                {
                    pi.GroupId = payload.RequestedId;
                    pi.Group = Group;
                });

                Group.PendingInvites = PendingInvites;

                Errors.Clear();
                if (!Group.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                writer.DB.Groups.Add(Group);

                var permission = payload.Permissions.ToDBPermission();
                permission.GrantorUserName = $"{payload.GroupName.ToLower()}-{Group.GroupId}";
                permission.GranteeUserName = $"{payload.GroupName.ToLower()}-{Group.GroupId}";
                permission.GroupId = Group.GroupId;

                Errors.Clear();
                if (!permission.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                await writer.DB.Permissions.AddAsync(permission);
                await writer.DB.SaveChangesAsync();

                _ = PermissionCache.OnCreateGroupAsync(Group, permission);

                return Ok(new CreateResponse
                {
                    GroupId = Group.GroupId
                });
            }
        }

        /// <summary>
        /// Retrieves a list of all groups you are a part of
        /// </summary>
        /// <returns>The full list of groups you are a part of</returns>
        [HttpGet("list")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Retrieved list", typeof(List<Model.Group>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        public async Task<IActionResult> ListAsync()
        {
            List<Model.Group> UserGroups;
            using (var reader = DBAccess.GetReader())
            {
                var thisUser = this.GetUserName();
                UserGroups = await reader.DB.Groups
                    .Include(g => g.PendingInvites)
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .Where(g => g.GroupOwner == thisUser || g.Users.Any(u => u.UserName == thisUser) || g.Admins.Any(a => a.UserName == thisUser))
                    .ToListAsync();
            }

            return Ok(UserGroups);
        }

        /// <summary>
        /// Retrieves a list of all groups you own
        /// </summary>
        /// <returns>The full list of groups you own</returns>
        [HttpGet("list/owner")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Retrieved list", typeof(List<Model.Group>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        public async Task<IActionResult> ListOwnerAsync()
        {
            List<Model.Group> UserGroups;
            using (var reader = DBAccess.GetReader())
            {
                UserGroups = await reader.DB.Groups
                    .Include(g => g.PendingInvites)
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .Where(g => g.GroupOwner == this.GetUserName())
                    .ToListAsync();
            }

            return Ok(UserGroups);
        }

        /// <summary>
        /// Retrieves a list of all groups you are an administrator of
        /// </summary>
        /// <returns>The list of groups you are an administrator of</returns>
        [HttpGet("list/admin")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Retrieved list", typeof(List<Model.Group>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        public async Task<IActionResult> ListAdminAsync()
        {
            List<Model.Group> UserGroups;
            using (var reader = DBAccess.GetReader())
            {
                var thisUser = this.GetUserName();
                UserGroups = await reader.DB.Groups
                    .Include(g => g.PendingInvites)
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .Where(g => g.Admins.Any(a => a.UserName == thisUser))
                    .ToListAsync();
            }

            return Ok(UserGroups);
        }

        /// <summary>
        /// Retrieves a list of all groups you are a member of
        /// </summary>
        /// <returns>The list of groups you are a member of</returns>
        [HttpGet("list/member")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Retrieved list", typeof(List<Model.Group>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        public async Task<IActionResult> ListMemberAsync()
        {
            List<Model.Group> UserGroups;
            using (var reader = DBAccess.GetReader())
            {
                var thisUser = this.GetUserName();
                UserGroups = await reader.DB.Groups
                    .Include(g => g.PendingInvites)
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .Where(g => g.Users.Any(u => u.UserName == thisUser))
                    .ToListAsync();
            }

            return Ok(UserGroups);
        }

        /// <summary>
        /// Retrives a group given a GroupId
        /// </summary>
        /// <param name="GroupId">The GroupId</param>
        /// <returns>The group definition</returns>
        [HttpGet("{GroupId:int}")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(Model.Group), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid GroupId")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "You are not a member of this group")]
        public async Task<IActionResult> GetAsync(int GroupId)
        {
            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(GroupId, typeof(DB.Model.Group), nameof(Model.Group.GroupId), ref Errors, nameof(GroupId));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            Model.Group? UserGroup = null;
            using (var reader = DBAccess.GetReader())
            {
                UserGroup = await reader.DB.Groups
                    .Include(g => g.PendingInvites)
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .FirstOrDefaultAsync(g => g.GroupId == GroupId);
            }

            if (UserGroup != null)
            {
                var thisUser = this.GetUserName();
                bool IsGroupOwner = UserGroup.GroupOwner == thisUser;
                bool IsGroupUser = UserGroup.Users.Any(g => g.UserName == thisUser);
                bool IsGroupAdmin = UserGroup.Admins.Any(a => a.UserName == thisUser);
                if (IsGroupOwner || IsGroupUser || IsGroupAdmin)
                {
                    return Ok(UserGroup);
                }
            }

            return Forbid();
        }

        /// <summary>
        /// Retrieves all pending invites
        /// </summary>
        /// <returns>A list of pending invites</returns>
        [HttpGet("list/invite")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Succses", typeof(List<InviteResponse>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        public async Task<IActionResult> ListInvitesAsync()
        {
            var thisUserName = this.GetUserName()!;

            List<InviteResponse> invites;
            using (var reader = DBAccess.GetReader())
            {
                invites = await (from gpi in reader.DB.GroupPendingInvites
                                 join permission in reader.DB.Permissions on gpi.GroupId equals permission.GroupId
                                 join gr in reader.DB.Groups on gpi.GroupId equals gr.GroupId
                                 where gpi.UserName == thisUserName
                                 select new InviteResponse
                                 {
                                     GroupId = gpi.GroupId,
                                     GroupName = gr.GroupName,
                                     Admin = gpi.Admin,
                                     Permisssions = Payloads.Permission.Permissions.FromDBPermission(permission)
                                 }).ToListAsync();
            }

            List<string> Errors = new();
            if (!invites.Validate(ref Errors))
            {
                throw new InvalidOperationException("Query bad");
            }

            return Ok(invites);
        }

        /// <summary>
        /// Accepts a group invite
        /// </summary>
        /// <param name="GroupId">The GroupId to accept the invite to</param>
        /// <returns>HTTP 200 on success</returns>
        [HttpPut("invite/accept/{GroupId:int}")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid GroupId")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "GroupId not found or you do not have an invite from GroupId specified")]
        public async Task<IActionResult> AcceptInviteAsync(int GroupId)
        {
            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(GroupId, typeof(DB.Model.Group), nameof(Model.Group.GroupId), ref Errors, nameof(GroupId));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var thisUser = this.GetUserName()!;
                var pendingInvite = await writer.DB.GroupPendingInvites
                    .FirstOrDefaultAsync(i => i.UserName == thisUser && i.GroupId == GroupId);
                if (pendingInvite != null)
                {
                    var group = await writer.DB.Groups
                        .Include(g => g.PendingInvites)
                        .Include(g => g.Admins)
                        .Include(g => g.Users)
                        .FirstOrDefaultAsync(g => g.GroupId == GroupId);
                    if (group != null)
                    {
                        group.PendingInvites.Remove(pendingInvite);
                        if (pendingInvite.Admin)
                        {
                            var newGroupAdmin = new Model.GroupAdmin()
                            {
                                UserName = thisUser,
                                GroupId = GroupId,
                                Group = group
                            };
                            newGroupAdmin.RunValidation_Throw();
                            group.Admins.Add(newGroupAdmin);
                        }
                        else
                        {
                            var newGroupUser = new Model.GroupUser()
                            {
                                UserName = thisUser,
                                GroupId = group.GroupId,
                                Group = group
                            };
                            newGroupUser.RunValidation_Throw();
                            group.Users.Add(newGroupUser);
                        }

                        await writer.DB.SaveChangesAsync();

                        _ = PermissionCache.OnAddGroupMemberAsync(thisUser, group);
                        return Ok();
                    }
                    else
                    {
                        return Problem(detail:"GroupPendingInvite found but no Group with that GroupId found.  This shouldn't happen.", statusCode: 500);
                    }
                }
                else
                {
                    return NotFound();
                }
            }
        }

        /// <summary>
        /// Rejects a group invite
        /// </summary>
        /// <param name="GroupId">The GroupId to reject the invite to</param>
        /// <returns>HTTP 200 on success</returns>
        [HttpPut("invite/reject/{GroupId:int}")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid GroupId")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "GroupId not found or you do not have an invite from GroupId specified")]
        public async Task<IActionResult> RejectInviteAsync(int GroupId)
        {
            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(GroupId, typeof(DB.Model.Group), nameof(Model.Group.GroupId), ref Errors, nameof(GroupId));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var thisUser = this.GetUserName()!;
                var pendingInvite = await writer.DB.GroupPendingInvites
                    .FirstOrDefaultAsync(i => i.UserName == thisUser && i.GroupId == GroupId);
                if (pendingInvite != null)
                {
                    var group = await writer.DB.Groups
                        .Include(g => g.PendingInvites)
                        .Include(g => g.Admins)
                        .Include(g => g.Users)
                        .FirstOrDefaultAsync(g => g.GroupId == GroupId);
                    if (group != null)
                    {
                        group.PendingInvites.Remove(pendingInvite);
                        await writer.DB.SaveChangesAsync();

                        return Ok();
                    }
                    else
                    {
                        return Problem(detail: "GroupPendingInvite found but no Group with that GroupId found.  This shouldn't happen.", statusCode: 500);
                    }
                }
                else
                {
                    return NotFound();
                }
            }
        }

        /// <summary>
        /// Invites users to a group you own or are admin of
        /// </summary>
        /// <param name="payload">Invite payload</param>
        [HttpPost("invite")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully invited specified users")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation or trying to invite as admin as non-owner", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "GroupId not found or you are not the owner or admin of the GroupId specified")]
        public async Task<IActionResult> InviteAsync([FromBody] Invite payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            var inviteUserNames = payload.Invites
                .Select(i => i.UserName.ToLower())
                .ToList();

            using (var writer = DBAccess.GetWriter())
            {
                var thisUser = this.GetUserName()!;

                var group = await writer.DB.Groups
                    .Include(g => g.PendingInvites)
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .Where(g => g.GroupId == payload.GroupId)
                    .Where(g => g.GroupOwner == thisUser || g.Admins.Any(a => a.UserName == thisUser))
                    .FirstOrDefaultAsync();

                if (group != null)
                {
                    var IsAdmin = group.Admins.Any(a => a.UserName == thisUser);
                    if (IsAdmin && payload.Invites.Any(i => i.Admin))
                    {
                        return BadRequest("You are not permitted to invite an admin unless you are GroupOwner");
                    }

                    // Filter down to only valid usernames
                    var userList = await writer.DB.Users.Select(u => u.UserName).ToListAsync();
                    inviteUserNames = userList.Intersect(inviteUserNames).ToList();

                    // Strip out usernames that meet any of these conditions:
                    // - Already in the group as a member
                    // - Already in the group as an administrator
                    // - Is the group owner
                    // - Already has a pending invite
                    var existingUsers = group.Users
                        .Select(u => u.UserName)
                        .Concat(group.Admins.Select(a => a.UserName))
                        .Concat(group.Users.Select(u => u.UserName))
                        .Concat(group.PendingInvites.Select(i => i.UserName))
                        .Concat(new List<string> { group.GroupOwner });
                    inviteUserNames = inviteUserNames
                        .Except(existingUsers)
                        .ToList();

                    if (!inviteUserNames.Any())
                    {
                        return BadRequest("Users to invite list is empty after filtering out invalid usernames and already present users/invites");
                    }

                    var newPendingInvites = inviteUserNames
                        .Select(iun => new Model.GroupPendingInvite
                        {
                            UserName = iun,
                            Admin = payload.Invites.First(i => i.UserName.ToLower() == iun).Admin,
                            GroupId = group.GroupId,
                            Group = group
                        })
                        .ToList();
                    newPendingInvites.ForEach(npi => npi.RunValidation_Throw());
                    group.PendingInvites.AddRange(newPendingInvites);
                    await writer.DB.SaveChangesAsync();

                    return Ok();
                }
                else
                {
                    return NotFound();
                }
            }
        }

        /// <summary>
        /// Leave a group
        /// </summary>
        /// <param name="GroupId">The GroupId to leave</param>
        /// <returns>200 on success</returns>
        [HttpPut("leave/{GroupId:int}")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully left group")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation or attempting to leave when you are owner")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "GroupId not found or you are not a member of the group")]
        public async Task<IActionResult> LeaveAsync(int GroupId)
        {
            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(GroupId, typeof(DB.Model.Group), nameof(Model.Group.GroupId), ref Errors, nameof(GroupId));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            var thisUser = this.GetUserName()!;
            using (var writer = DBAccess.GetWriter())
            {
                var Group = await writer.DB.Groups
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .Include(g => g.PendingInvites)
                    .Where(g => g.GroupId == GroupId)
                    .FirstOrDefaultAsync();

                if (Group != null)
                {
                    if (Group.GroupOwner == thisUser)
                    {
                        return BadRequest();
                    }

                    var Admin = Group.Admins.FirstOrDefault(a => a.UserName == thisUser);
                    var User = Group.Users.FirstOrDefault(u => u.UserName == thisUser);
                    if (Admin != null || User != null)
                    {
                        if (Admin != null)
                        {
                            Group.Admins.Remove(Admin);
                        }

                        if (User != null)
                        {
                            Group.Users.Remove(User);
                        }

                        await writer.DB.SaveChangesAsync();

                        _ = PermissionCache.OnRemoveGroupMemberAsync(thisUser, Group);

                        return Ok();
                    }
                    else
                    {
                        return NotFound();
                    }
                }

                return NotFound();
            }
        }

        /// <summary>
        /// Kicks a user from the given GroupId
        /// </summary>
        /// <param name="GroupId">The GroupId</param>
        /// <param name="UserName">The UserName to kick</param>
        /// <returns>200 on success</returns>
        [HttpPut("kick/{GroupId:int}/{UserName}")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully invited specified users")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation or attempting to use when you don't have permission", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "GroupId not found, you are not the owner or admin of the GroupId specified, or the user was not found in the group", typeof(string), "text/plain")]
        public async Task<IActionResult> KickAsync(int GroupId, string UserName)
        {
            UserName = UserName.ToLower();

            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(GroupId, typeof(DB.Model.Group), nameof(Model.Group.GroupId), ref Errors, nameof(GroupId));
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserName, typeof(DB.Model.User), nameof(Model.User.UserName), ref Errors, nameof(UserName));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var thisUser = this.GetUserName()!;

                var Group = await writer.DB.Groups
                    .Include(g => g.PendingInvites)
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .Where(g => g.GroupOwner == thisUser || g.Admins.Any(a => a.UserName == thisUser))
                    .FirstOrDefaultAsync();
                if (Group != null)
                {
                    bool bWeAreGroupOwner = Group.GroupOwner == thisUser;
                    bool bWeAreGroupAdmin = Group.Admins.Any(a => a.UserName == thisUser);
                    bool bTargetIsOwner = Group.GroupOwner == UserName;

                    var TargetAdmin = Group.Admins.FirstOrDefault(a => a.UserName == UserName);
                    bool bTargetIsAdmin = (TargetAdmin != null);

                    var TargetUser = Group.Users.FirstOrDefault(a => a.UserName == UserName);
                    bool bTargetIsUser = (TargetUser != null);

                    var TargetPendingInvite = Group.PendingInvites.FirstOrDefault(i => i.UserName == UserName);
                    bool bTargetIsPendingInvite = (TargetPendingInvite != null);
                    bool bTargetIsPendingInviteAndAdmin = bTargetIsPendingInvite && TargetPendingInvite!.Admin;

                    if (bTargetIsOwner)
                    {
                        return BadRequest($"Attempting to kick GroupOwner");
                    }

                    if (!bWeAreGroupOwner && bTargetIsAdmin)
                    {
                        return BadRequest($"Attempting to kick Admin user when you are not GroupOwner");
                    }

                    if (!bWeAreGroupOwner && bTargetIsPendingInviteAndAdmin)
                    {
                        return BadRequest($"Attempting to kick Admin pending invite when you are not GroupOwner");
                    }

                    if (!bWeAreGroupOwner && !bWeAreGroupAdmin)
                    {
                        return BadRequest($"Attempt to kick when you are not an admin or GroupOwner");
                    }

                    if (thisUser == UserName)
                    {
                        return BadRequest($"Can't kick yourself.  Use leave instead");
                    }

                    if (bTargetIsAdmin || bTargetIsUser || bTargetIsPendingInvite)
                    {
                        if (bTargetIsAdmin)
                        {
                            Group.Admins.Remove(TargetAdmin!);
                        }

                        if (bTargetIsUser)
                        {
                            Group.Users.Remove(TargetUser!);
                        }

                        if (bTargetIsPendingInvite)
                        {
                            Group.PendingInvites.Remove(TargetPendingInvite!);
                        }

                        await writer.DB.SaveChangesAsync();

                        _ = PermissionCache.OnRemoveGroupMemberAsync(UserName, Group);

                        return Ok();
                    }
                    else
                    {
                        return NotFound($"User {UserName} was not found in the group");
                    }
                }
                else
                {
                    return NotFound($"GroupId {GroupId} not found or you are not owner/admin of the group");
                }
            }
        }

        /// <summary>
        /// Promotes a the provided UserName in the provided GroupId
        /// </summary>
        /// <param name="GroupId">The GroupId</param>
        /// <param name="UserName">The UserName</param>
        /// <returns>200 on success</returns>
        [HttpPut("promote/{GroupId:int}/{UserName}")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully promoted specified user", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation or invalid operation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "GroupId not found, you are not the owner of the GroupId specified, or the username specified is not in the group", typeof(string), "text/plain")]
        public async Task<IActionResult> PromoteAsync(int GroupId, string UserName)
        {
            UserName = UserName.ToLower();

            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(GroupId, typeof(DB.Model.Group), nameof(Model.Group.GroupId), ref Errors, nameof(GroupId));
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserName, typeof(DB.Model.User), nameof(Model.User.UserName), ref Errors, nameof(UserName));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            var thisUser = this.GetUserName()!;

            using (var writer = DBAccess.GetWriter())
            {
                var Group = await writer.DB.Groups
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .Include(g => g.PendingInvites)
                    .FirstOrDefaultAsync(g => g.GroupOwner == thisUser && g.GroupId == GroupId);
                if (Group != null)
                {
                    if (!Group.Admins.Any(u => u.UserName == UserName))
                    {
                        if (Group.Users.Any(u => u.UserName == UserName))
                        {
                            // Delete from the users list
                            await writer.DB.GroupUsers.Where(gu => gu.GroupId == GroupId && gu.UserName == UserName).DeleteAsync();

                            // Add to the admin list
                            var newGroupAdmin = new Model.GroupAdmin()
                            {
                                UserName = UserName,
                                GroupId = GroupId,
                                Group = Group
                            };
                            newGroupAdmin.RunValidation_Throw();
                            await writer.DB.GroupAdmins.AddAsync(newGroupAdmin);
                            await writer.DB.SaveChangesAsync();

                            return Ok();
                        }
                        else
                        {
                            return NotFound($"User {UserName} is not in the group");
                        }
                    }
                    else
                    {
                        return BadRequest($"User {UserName} is already an administrator");
                    }

                }
                else
                {
                    return NotFound($"GroupId {GroupId} not found or you are not the owner.");
                }
            }
        }

        /// <summary>
        /// Deletes a group you own
        /// </summary>
        /// <param name="GroupId">The GroupId to delete</param>
        [HttpDelete("delete/{GroupId:int}")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully deleted group")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid id", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not authenticated")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "GroupId not found or you are not the owner of the GroupId specified", typeof(string), "text/plain")]
        public async Task<IActionResult> DeleteAsync(int GroupId)
        {
            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(GroupId, typeof(DB.Model.Group), nameof(Model.Group.GroupId), ref Errors, nameof(GroupId));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var GroupToDelete = await writer.DB.Groups
                    .Include(g => g.PendingInvites)
                    .Include(g => g.Admins)
                    .Include(g => g.Users)
                    .FirstOrDefaultAsync(g => g.GroupOwner == this.GetUserName() && g.GroupId == GroupId);
                if (GroupToDelete == null)
                {
                    return NotFound($"GroupId {GroupId} not found or you are not the owner of {GroupId}");
                }

                writer.DB.GroupPendingInvites.RemoveRange(GroupToDelete.PendingInvites);
                writer.DB.GroupAdmins.RemoveRange(GroupToDelete.Admins);
                writer.DB.GroupUsers.RemoveRange(GroupToDelete.Users);
                writer.DB.Groups.Remove(GroupToDelete);
                await writer.DB.SaveChangesAsync();

                _ = PermissionCache.OnDeleteGroupAsync(GroupToDelete);
            }

            return Ok();
        }
    }
}
