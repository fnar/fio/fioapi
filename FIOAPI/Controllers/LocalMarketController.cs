﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using Microsoft.Extensions.Logging.Abstractions;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// LocalMarketController
    /// </summary>
    [ApiController]
    [Route("/localmarket")]
    public class LocalMarketController : ControllerBase
    {
        /// <summary>
        /// Puts the PATH_LOCALMARKET_AD payload
        /// </summary>
        /// <param name="LocalMarketAdData">The payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutLocalMarketAd([FromBody] Payloads.LocalMarket.PATH_LOCALMARKET_AD LocalMarketAdData)
        {
            List<string> Errors = new();
            if (!LocalMarketAdData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = LocalMarketAdData.payload.message.payload;

            if (!data.body.Any())
            {
                return Ok();
            }

            List<LocalMarketAd> LocalMarketAds = new();

            foreach (var ad in data.body)
            {
                LocalMarketAd LMAd = new();
                LMAd.LocalMarketAdId = ad.id;
                LMAd.LocalMarketId = ad.localMarketId;
                LMAd.PlanetId = ad.address.lines.Last().entity!.id;
                LMAd.PlanetNaturalId = ad.address.lines.Last().entity!.naturalId;
                LMAd.PlanetName = ad.address.lines.Last().entity!.name;
                LMAd.AdNaturalId = ad.naturalId;
                LMAd.Type = ad.type;
                LMAd.Weight = ad.cargoWeight;
                LMAd.Volume = ad.cargoVolume;
                LMAd.CreatorId = ad.creator.id;
                LMAd.CreatorName = ad.creator.name;
                LMAd.CreatorCode = ad.creator.code!;
                LMAd.MaterialTicker = ad.quantity?.material.ticker;
                LMAd.MaterialAmount = ad.quantity?.amount;
                LMAd.Price = ad.price.amount;
                LMAd.Currency = ad.price.currency;
                LMAd.FulfillmentDays = ad.advice;
                LMAd.MinimumRating = ad.minimumRating == "PENDING" ? "P" : ad.minimumRating;
                LMAd.CreationTime = ad.creationTime.timestamp.FromUnixTime();
                LMAd.Expiry = ad.expiry.timestamp.FromUnixTime();
                LMAd.OriginNaturalId = ad.origin?.lines.Last().entity!.naturalId;
                LMAd.DestinationNaturalId = ad.destination?.lines.Last().entity!.naturalId;
                LMAd.UserNameSubmitted = this.GetUserName()!;
                LMAd.Timestamp = DateTime.UtcNow;

                if (!LMAd.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                LocalMarketAds.Add(LMAd);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.LocalMarketAds
                    .Where(lma => lma.LocalMarketId == LocalMarketAds.First().LocalMarketId)
                    .DeleteAsync();

                await writer.DB.LocalMarketAds
                    .AddRangeAsync(LocalMarketAds);

                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrives all LM ads
        /// </summary>
        /// <param name="include_buys">If we should emit buys</param>
        /// <param name="include_sells">If we should emit sells</param>
        /// <param name="include_shipments">If we should emit shipments</param>
        /// <returns>A list of local market ads</returns>
        [HttpGet("")]
        [AllowAnonymous]
        [Cache(Minutes = 30)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<LocalMarketAd>), "application/json")]
        public async Task<IActionResult> GetAllLocalMarketAds(
            [FromQuery(Name = "include_buys")] bool include_buys = true,
            [FromQuery(Name = "include_sells")] bool include_sells = true,
            [FromQuery(Name = "include_shipments")] bool include_shipments = true)
        {
            List<LocalMarketAd> Results = new();
            using (var reader = DBAccess.GetReader())
            {
                var LMQuery = reader.DB.LocalMarketAds.AsQueryable();
                if (!include_buys)
                {
                    LMQuery = LMQuery.Where(lma => lma.Type != Constants.ValidLocalMarketTypes[Constants.LocalMarketAdType.Buying]);
                }

                if (!include_sells)
                {
                    LMQuery = LMQuery.Where(lma => lma.Type != Constants.ValidLocalMarketTypes[Constants.LocalMarketAdType.Selling]);
                }

                if (!include_shipments)
                {
                    LMQuery = LMQuery.Where(lma => lma.Type != Constants.ValidLocalMarketTypes[Constants.LocalMarketAdType.Shipping]);
                }

                Results = await LMQuery.ToListAsync();
            }

            return Ok(Results);
        }

        /// <summary>
        /// Retrieves LM ads
        /// </summary>
        /// <param name="LocalMarketIdentifier">The Planet/Station Id/NaturalId/name</param>
        /// <param name="include_buys">If we should emit buys</param>
        /// <param name="include_sells">If we should emit sells</param>
        /// <param name="include_shipments">If we should emit shipments</param>
        /// <returns>OK</returns>
        [HttpGet("{LocalMarketIdentifier}")]
        [AllowAnonymous]
        [Cache(Minutes = 10)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<LocalMarketAd>), "application/json")]
        public async Task<IActionResult> GetLocalMarketAds(
            string LocalMarketIdentifier,
            [FromQuery(Name = "include_buys")] bool include_buys = true,
            [FromQuery(Name = "include_sells")] bool include_sells = true,
            [FromQuery(Name = "include_shipments")] bool include_shipments = true)
        {
            List<LocalMarketAd> Results = new();
            using (var reader = DBAccess.GetReader())
            {
                var LMQuery = reader.DB.LocalMarketAds.AsQueryable();
                if (!include_buys)
                {
                    LMQuery = LMQuery.Where(lma => lma.Type != Constants.ValidLocalMarketTypes[Constants.LocalMarketAdType.Buying]);
                }

                if (!include_sells)
                {
                    LMQuery = LMQuery.Where(lma => lma.Type != Constants.ValidLocalMarketTypes[Constants.LocalMarketAdType.Selling]);
                }

                if (!include_shipments)
                {
                    LMQuery = LMQuery.Where(lma => lma.Type != Constants.ValidLocalMarketTypes[Constants.LocalMarketAdType.Shipping]);
                }

                LMQuery.Where(lma => lma.PlanetId == LocalMarketIdentifier || lma.PlanetNaturalId == LocalMarketIdentifier || lma.PlanetName == LocalMarketIdentifier);

                Results = await LMQuery.ToListAsync();
            }

            return Ok(Results);
        }

        /// <summary>
        /// Retireves local market adds from the provided planet(s)
        /// </summary>
        /// <param name="planet">Planet(s)</param>
        /// <param name="include_buys">If buys should be included</param>
        /// <param name="include_sells">If sells should be included</param>
        /// <param name="include_shipments">If shipments should be included</param>
        /// <returns></returns>
        [HttpGet("planet")]
        [AllowAnonymous]
        [Cache(Minutes = 5)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(LocalMarketAd), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Did not specify any planets or no ad types")]
        public async Task<IActionResult> GetLocalMarketAds(
            [FromQuery(Name = "planet")] List<string>? planet,
            [FromQuery(Name = "include_buys")] bool include_buys = true,
            [FromQuery(Name = "include_sells")] bool include_sells = true,
            [FromQuery(Name = "include_shipments")] bool include_shipments = true)
        {
            if (planet == null || planet.Count == 0)
            {
                return BadRequest();
            }

            planet.ForEach(p =>
            {
                p = p.Trim().ToUpper();
            });

            List<string> ValidAdTypes = new();
            if (include_buys)
            {
                ValidAdTypes.Add(Constants.ValidLocalMarketTypes[Constants.LocalMarketAdType.Buying]);
            }

            if (include_sells)
            {
                ValidAdTypes.Add(Constants.ValidLocalMarketTypes[Constants.LocalMarketAdType.Selling]);
            }

            if (include_shipments)
            {
                ValidAdTypes.Add(Constants.ValidLocalMarketTypes[Constants.LocalMarketAdType.Shipping]);
            }

            if (ValidAdTypes.Count == 0)
            {
                return BadRequest();
            }

            List<LocalMarketAd> LMAds;
            using (var reader = DBAccess.GetReader())
            {
                LMAds = await reader.DB.LocalMarketAds
                    .Where(lma => planet.Contains(lma.PlanetId.ToUpper()) || planet.Contains(lma.PlanetNaturalId.ToUpper()) || planet.Contains(lma.PlanetName.ToUpper()))
                    .Where(lma => ValidAdTypes.Contains(lma.Type))
                    .ToListAsync();
            }

            return Ok(LMAds);
        }

        /// <summary>
        /// Retrives an indvidual LM ad
        /// </summary>
        /// <param name="LocalMarketIdentifier">The Planet/Station Id/NaturalId/name</param>
        /// <param name="LocalMarketAdId">The ad's natural id</param>
        /// <returns></returns>
        [HttpGet("{LocalMarketIdentifier}/{LocalMarketAdId}")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(LocalMarketAd), "application/json")]
        [SwaggerResponse(StatusCodes.Status404NotFound, "No ad found")]
        public async Task<IActionResult> GetLocalMarketAd(string LocalMarketIdentifier, int LocalMarketAdId)
        {
            LocalMarketIdentifier = LocalMarketIdentifier.ToUpper();

            LocalMarketAd? result;
            using (var reader = DBAccess.GetReader())
            {
                result = await reader.DB.LocalMarketAds
                    .Where(lma => lma.PlanetId.ToUpper() == LocalMarketIdentifier || lma.PlanetNaturalId.ToUpper() == LocalMarketIdentifier || lma.PlanetName.ToUpper() == LocalMarketIdentifier)
                    .FirstOrDefaultAsync(lma => lma.AdNaturalId == LocalMarketAdId);
            }

            if (result != null)
            {
                return Ok(result);
            }

            return NotFound();
        }

        /// <summary>
        /// Retrives ads for the provided CompanyIdentifier
        /// </summary>
        /// <param name="CompanyIdentifier">Company Id, Code, or Name</param>
        /// <param name="include_buys">If buys should be included in the result</param>
        /// <param name="include_sells">If sells should be included in the result</param>
        /// <param name="include_shipments">If shipments should be included in the result</param>
        /// <returns>A list of LocalMarketAds created by said company</returns>
        [HttpGet("company/{CompanyIdentifier}")]
        [AllowAnonymous]
        [Cache(Minutes = 10)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<LocalMarketAd>), "application/json")]
        public async Task<IActionResult> GetCompanyAds(
            string CompanyIdentifier,
            [FromQuery(Name = "include_buys")] bool include_buys = true,
            [FromQuery(Name = "include_sells")] bool include_sells = true,
            [FromQuery(Name = "include_shipments")] bool include_shipments = true)
        {
            CompanyIdentifier = CompanyIdentifier.ToUpper();

            List<LocalMarketAd> ads = null!;
            using (var reader = DBAccess.GetReader())
            {
                var query = reader.DB.LocalMarketAds.AsQueryable();
                if (!include_buys)
                {
                    query = query.Where(lma => lma.Type != "COMMODITY_BUYING");
                }
                if (!include_sells)
                {
                    query = query.Where(lma => lma.Type != "COMMODITY_SELLING");
                }
                if (!include_shipments)
                {
                    query = query.Where(lma => lma.Type != "COMMODITY_SHIPPING");
                }

                query = query.Where(lma => lma.CreatorId.ToUpper() == CompanyIdentifier || lma.CreatorCode.ToUpper() == CompanyIdentifier || lma.CreatorName.ToUpper() == CompanyIdentifier);
                ads = await query.ToListAsync();
            }

            return Ok(ads);
        }

        /// <summary>
        /// Retrieve shipping ads provided a given origin and/or destination. If neither are provided, returns all shipping ads.
        /// </summary>
        /// <param name="origin">The natural id of the origin</param>
        /// <param name="destination">The natural id of the destination</param>
        /// <returns>A list of shipping ads</returns>
        [HttpGet("shipping")]
        [AllowAnonymous]
        [Cache(Minutes = 10)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<LocalMarketAd>), "application/json")]
        public async Task<IActionResult> GetShippingAds(
            [FromQuery(Name = "origin")] string? origin = null,
            [FromQuery(Name = "destination")] string? destination = null)
        {
            bool OriginValid = !string.IsNullOrWhiteSpace(origin);
            bool DestinationValid = !string.IsNullOrWhiteSpace(destination);

            List<LocalMarketAd> ads = null!;
            using (var reader = DBAccess.GetReader())
            {
                var query = reader.DB.LocalMarketAds
                    .Where(lma => lma.Type == "COMMODITY_SHIPPING")
                    .AsQueryable();
                if (OriginValid)
                {
                    query = query
                        .Where(lma => lma.OriginNaturalId != null)
                        .Where(lma => lma.OriginNaturalId!.ToUpper() == origin!.ToUpper());
                }

                if (DestinationValid)
                {
                    query = query
                        .Where(lma => lma.DestinationNaturalId != null)
                        .Where(lma => lma.DestinationNaturalId!.ToUpper() == destination!.ToUpper());
                }

                ads = await query.ToListAsync();
            }

            return Ok(ads);
        }
    }
}
