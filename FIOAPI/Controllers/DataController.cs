﻿using FIOAPI.Payloads.Data;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// A controller for retrieving all data
    /// </summary>
    [ApiController]
    [Route("/data")]
    public class DataController : ControllerBase
    {
        /// <summary>
        /// Retrieves all data for the provided usernames(s)/group
        /// </summary>
        /// <param name="username">username(s)</param>
        /// <param name="group">group</param>
        /// <param name="include_company_data">If company data should be retrieved</param>
        /// <param name="include_contract_data">If contract data should be retrieved</param>
        /// <param name="include_cxos_data">If cxos data should be retrieved</param>
        /// <param name="include_productionline_data">If productionline data should be retrieved</param>
        /// <param name="include_ship_data">If ship data should be retrieved</param>
        /// <param name="include_flight_data">If flight data should be retrieved</param>
        /// <param name="include_site_data">If site data should be retrieved</param>
        /// <param name="include_storage_data">If storage data should be retrieved</param>
        /// <param name="include_workforce_data">If workforce data should be retrieved</param>
        /// <returns>All private user data you have access to</returns>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<AllData>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        //[SwaggerResponse(StatusCodes.Status429TooManyRequests, "Re-requested the same payload too soon, please wait 60 seconds between identical requests")] // @TODO - Implement
        public async Task<IActionResult> GetAllData(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_company_data")] bool include_company_data = false,
            [FromQuery(Name = "include_contract_data")] bool include_contract_data = false,
            [FromQuery(Name = "include_cxos_data")] bool include_cxos_data = false,
            [FromQuery(Name = "include_productionline_data")] bool include_productionline_data = false,
            [FromQuery(Name = "include_ship_data")] bool include_ship_data = false,
            [FromQuery(Name = "include_flight_data")] bool include_flight_data = false,
            [FromQuery(Name = "include_site_data")] bool include_site_data = false,
            [FromQuery(Name = "include_storage_data")] bool include_storage_data = false,
            [FromQuery(Name = "include_workforce_data")] bool include_workforce_data = false)
        {
            if (!include_company_data && !include_contract_data &&
                !include_cxos_data && !include_productionline_data &&
                !include_ship_data && !include_flight_data && !include_site_data &&
                !include_storage_data && !include_workforce_data)
            {
                return BadRequest(new List<string> { "No includes specified" });
            }

            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, Model.Company>? UserToCompany = null;
            Task<Dictionary<string, Model.Company>>? UserToCompanyQuery = null;

            Dictionary<string, List<Model.Contract>>? UserToContracts = null;
            Task<Dictionary<string, List<Model.Contract>>>? UserToContractsQuery = null;

            Dictionary<string, List<Model.CXOS>>? UserToCXOSs = null;
            Task<Dictionary<string, List<Model.CXOS>>> ? UserToCXOSQuery = null;

            Dictionary<string, List<Model.ProductionLine>>? UserToProductionLines = null;
            Task<Dictionary<string, List<Model.ProductionLine>>>? UserToProductionLinesQuery = null;

            Dictionary<string, List<Model.Ship>>? UserToShips = null;
            Task<Dictionary<string, List<Model.Ship>>>? UserToShipsQuery = null;

            Dictionary<string, List<Model.Flight>>? UserToFlights = null;
            Task<Dictionary<string, List<Model.Flight>>>? UserToFlightsQuery = null;

            Dictionary<string, List<Model.Site>>? UserToSites = null;
            Task<Dictionary<string, List<Model.Site>>>? UserToSitesQuery = null;

            Dictionary<string, List<Model.Storage>>? UserToStorages = null;
            Task<Dictionary<string, List<Model.Storage>>>? UserToStoragesQuery = null;

            Dictionary<string, List<Model.Workforce>>? UserToWorkforces = null;
            Task<Dictionary<string, List<Model.Workforce>>>? UserToWorkforcesQuery = null;

            using (var reader = DBAccess.GetReader())
            {
                List<Task> TasksToRun = new();

                if (include_company_data)
                {
                    UserToCompanyQuery = reader.DB.Companies
                        .Include(c => c.Planets)
                        .Include(c => c.Offices)
                        .Where(c => UserToPerm.Keys.Contains(c.UserNameSubmitted))
                        .GroupBy(c => c.UserName)
                        .ToDictionaryAsync(g => g.Key, g => g.First());

                    TasksToRun.Add(UserToCompanyQuery);
                }
                
                if(include_contract_data)
                {
                    UserToContractsQuery = reader.DB.Contracts
                        .Include(c => c.Conditions)
                        .Where(c => UserToPerm.Keys.Contains(c.UserNameSubmitted))
                        .GroupBy(c => c.UserNameSubmitted)
                        .ToDictionaryAsync(g => g.Key, g => g.ToList());

                    TasksToRun.Add(UserToContractsQuery);
                }
                
                if (include_cxos_data)
                {
                    UserToCXOSQuery = reader.DB.CXOSs
                        .Include(c => c.Trades)
                        .Where(c => UserToPerm.Keys.Contains(c.UserNameSubmitted))
                        .GroupBy(c => c.UserNameSubmitted)
                        .ToDictionaryAsync(g => g.Key, g => g.ToList());

                    TasksToRun.Add(UserToCXOSQuery);
                }
                
                if (include_productionline_data)
                {
                    UserToProductionLinesQuery = reader.DB.ProductionLines
                        .Include(p => p.Orders)
                        .Where(p => UserToPerm.Keys.Contains(p.UserNameSubmitted))
                        .GroupBy(p => p.UserNameSubmitted)
                        .ToDictionaryAsync(g => g.Key, g => g.ToList());

                    TasksToRun.Add(UserToProductionLinesQuery);
                }
                
                if (include_ship_data)
                {
                    UserToShipsQuery = reader.DB.Ships
                        .Include(s => s.RepairMaterials)
                        .Where(s => UserToPerm.Keys.Contains(s.UserNameSubmitted))
                        .GroupBy(s => s.UserNameSubmitted)
                        .ToDictionaryAsync(g => g.Key, g => g.ToList());

                    TasksToRun.Add(UserToShipsQuery);
                }

                if (include_flight_data)
                {
                    UserToFlightsQuery = reader.DB.Flights
                        .Include(f => f.FlightSegments)
                        .Where(f => UserToPerm.Keys.Contains(f.UserNameSubmitted))
                        .GroupBy(f => f.UserNameSubmitted)
                        .ToDictionaryAsync(g => g.Key, g => g.ToList());

                    TasksToRun.Add(UserToFlightsQuery);
                }
                
                if (include_site_data)
                {
                    UserToSitesQuery = reader.DB.Sites
                        .Include(s => s.Buildings)
                        .Where(s => UserToPerm.Keys.Contains(s.UserNameSubmitted))
                        .GroupBy(s => s.UserNameSubmitted)
                        .ToDictionaryAsync(g => g.Key, g => g.ToList());

                    TasksToRun.Add(UserToSitesQuery);
                }
                
                if (include_storage_data)
                {
                    UserToStoragesQuery = reader.DB.Storages
                        .Include(s => s.StorageItems)
                        .Where(s => UserToPerm.Keys.Contains(s.UserNameSubmitted))
                        .GroupBy(s => s.UserNameSubmitted)
                        .ToDictionaryAsync(g => g.Key, g => g.ToList());

                    TasksToRun.Add(UserToStoragesQuery);
                }

                if (include_workforce_data)
                {
                    UserToWorkforcesQuery = reader.DB.Workforces
                        .Include(w => w.WorkforceNeeds)
                        .Where(w => UserToPerm.Keys.Contains(w.UserNameSubmitted))
                        .GroupBy(w => w.UserNameSubmitted)
                        .ToDictionaryAsync(g => g.Key, g => g.ToList());

                    TasksToRun.Add(UserToWorkforcesQuery);
                }

                await Task.WhenAll(TasksToRun);

                if (UserToCompanyQuery != null)
                {
                    UserToCompany = UserToCompanyQuery.Result;
                }
                
                UserToContracts = UserToContractsQuery?.Result;
                UserToCXOSs = UserToCXOSQuery?.Result;
                UserToProductionLines = UserToProductionLinesQuery?.Result;
                UserToShips = UserToShipsQuery?.Result;
                UserToFlights = UserToFlightsQuery?.Result;
                UserToSites = UserToSitesQuery?.Result;
                UserToStorages = UserToStoragesQuery?.Result;
                UserToWorkforces = UserToWorkforcesQuery?.Result;
            }

            Dictionary<string, AllData> AllDatas = UserToPerm.ToDictionary(
                kvp => kvp.Key, 
                kvp => new AllData()
                {
                    Company = (UserToCompany != null && UserToCompany.ContainsKey(kvp.Key)) ? UserToCompany[kvp.Key] : null,
                    Contracts = (UserToContracts != null && UserToContracts.ContainsKey(kvp.Key)) ? UserToContracts[kvp.Key] : null,
                    CXOSs = (UserToCXOSs != null && UserToCXOSs.ContainsKey(kvp.Key)) ? UserToCXOSs[kvp.Key] : null,
                    ProductionLines = (UserToProductionLines != null && UserToProductionLines.ContainsKey(kvp.Key)) ? UserToProductionLines[kvp.Key] : null,
                    Ships = (UserToShips != null && UserToShips.ContainsKey(kvp.Key)) ? UserToShips[kvp.Key] : null,
                    Flights = (UserToFlights != null && UserToFlights.ContainsKey(kvp.Key)) ? UserToFlights[kvp.Key] : null,
                    Sites = (UserToSites != null && UserToSites.ContainsKey(kvp.Key)) ? UserToSites[kvp.Key] : null,
                    Storages = (UserToStorages != null && UserToStorages.ContainsKey(kvp.Key)) ? UserToStorages[kvp.Key] : null,
                    Workforces = (UserToWorkforces != null && UserToWorkforces.ContainsKey(kvp.Key)) ? UserToWorkforces[kvp.Key] : null
                });

            var Response = Retrieval.CreateSanitizedResponse(AllDatas, UserToPerm);
            return Ok(Response);
        }

        /// <summary>
        /// Retrieves burn data for the provided username(s)/group
        /// </summary>
        /// <param name="username">username(s)</param>
        /// <param name="group">group</param>
        /// <returns>Burn data</returns>
        [HttpGet("burn")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<Burn>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetBurn(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            var allData = await GetAllData(username, group, include_productionline_data: true, include_site_data: true, include_storage_data: true, include_workforce_data: true);
            var allDataRetrievalResponse = allData.GetObject<RetrievalResponse<AllData>>();

            if (allDataRetrievalResponse != null)
            {
                Dictionary<string, List<Burn>> UserNameToBurns = new();
                foreach (var kvp in allDataRetrievalResponse.UserNameToData)
                {
                    var userName = kvp.Key;
                    var data = kvp.Value.Data;

                    if (data.Sites == null) data.Sites = new();
                    if (data.Workforces == null) data.Workforces = new();
                    if (data.Storages == null) data.Storages = new();
                    if (data.ProductionLines == null) data.ProductionLines = new();

                    foreach (var site in data.Sites!)
                    {
                        var planetWorkforce = data.Workforces.FirstOrDefault(w => w.WorkforceId == site.SiteId);
                        var planetStorage = data.Storages.FirstOrDefault(s => s.Type == Constants.StoreTypeEnumToStoreType[Constants.StoreType.Base] && s.AddressableId == site.SiteId);
                        var planetProductionLines = data.ProductionLines.Where(pl => pl.SiteId == site.SiteId).ToList();

                        Burn PlanetBurn = new();
                        PlanetBurn.PlanetId = site.LocationId;
                        PlanetBurn.PlanetNaturalId = site.LocationNaturalId;
                        PlanetBurn.PlanetName = site.LocationName;
                        PlanetBurn.WorkforceConsumptionTime = planetWorkforce?.LastTickTime;
                        PlanetBurn.LastUpdate = planetWorkforce?.Timestamp;

                        if (planetStorage != null)
                        {
                            var TickersToCheck = new List<string>();
                            if (planetWorkforce != null)
                            {
                                TickersToCheck.AddRange(planetWorkforce.WorkforceNeeds
                                    .Where(wn => wn.UnitsPerInterval > 0)   // Only needs we actually...need
                                    .Select(wn => wn.MaterialTicker)
                                    .Distinct());
                            }
                            if (planetProductionLines != null)
                            {
                                TickersToCheck.AddRange(planetProductionLines
                                    .SelectMany(ppl => ppl.Orders)
                                    .SelectMany(ppl => ppl.Inputs.Concat(ppl.Outputs))
                                    .Select(io => io.Ticker)
                                    .Distinct());
                            }

                            foreach (var TickerToCheck in TickersToCheck) 
                            {
                                BurnItem BurnItem = new();
                                BurnItem.MaterialTicker = TickerToCheck;
                                BurnItem.DetermineMaterialAmount(planetStorage);
                                BurnItem.DetermineWorkforceConsumption(planetWorkforce);
                                BurnItem.DetermineOrderConsumptionAndProduction(planetProductionLines);
                                PlanetBurn.Inventory.Add(BurnItem);
                            }
                        }

                        UserNameToBurns.AddToList(userName, PlanetBurn);
                    }
                }

                var Response = Retrieval.CreateSanitizedResponse(UserNameToBurns, UserToPerm);
                return Ok(Response);
            }

            return BadRequest(new List<string> { "Failed to retrieve all data" });
        }
    }
}
