﻿#define USING_PRODUCTION_LINE_UPDATED_BAD_DATA_WORKAROUND

using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using FIOAPI.TimedEvents;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// ProductionLineController
    /// </summary>
    [ApiController]
    [Route("/productionline")]
    public class ProductionLineController : ControllerBase
    {
        /// <summary>
        /// PUTs SITE_PRODUCTION_LINES payload
        /// </summary>
        /// <param name="prodLinesPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutProdLines([FromBody] Payloads.Production.SITE_PRODUCTION_LINES prodLinesPayload)
        {
            List<string> Errors = new();
            if (!prodLinesPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            List<ProductionLine> ProductionLines = new();
            var data = prodLinesPayload.payload.message.payload;
            foreach (var prodLine in data.productionLines)
            {
                ProductionLine ProdLine = new();
                ProdLine.ProductionLineId = prodLine.id;
                ProdLine.SiteId = prodLine.siteId;
                ProdLine.LocationId = prodLine.address.lines.Last().entity!.id;
                ProdLine.LocationNaturalId = prodLine.address.lines.Last().entity!.naturalId;
                ProdLine.LocationName = prodLine.address.lines.Last().entity!.name;
                ProdLine.BuildingName = prodLine.type;
                ProdLine.BuildingTicker = EntityCaches.BuildingCache.Get(ProdLine.BuildingName)?.Ticker;
                ProdLine.Capacity = prodLine.capacity;
                ProdLine.Slots = prodLine.slots;
                ProdLine.Efficiency = prodLine.efficiency;
                ProdLine.Condition = prodLine.condition;
                ProdLine.RawWorkforceEfficiencies = String.Join(' ', prodLine.workforces.Select(w => $"{w.level}:{w.efficiency}"));
                ProdLine.RawEfficiencyFactors = String.Join(',', prodLine.efficiencyFactors.Select(ef => $"{(ef.expertiseCategory != null ? ef.expertiseCategory : " ")}:{ef.type}->{ef.effectivity}!{ef.value}"));
                foreach(var order in prodLine.orders)
                {
                    ProductionLineOrder ProdLineOrder = new();
                    ProdLineOrder.ProductionLineOrderId = order.id;
                    if (order.inputs != null)
                    {
                        ProdLineOrder.RawInputs = string.Join(',', order.inputs.Select(i => $"{i.amount}x{i.material.ticker}"));
                    }
                    
                    ProdLineOrder.RawOutputs = string.Join(",", order.outputs.Select(o => $"{o.amount}x{o.material.ticker}"));
                    ProdLineOrder.Created = order.created.timestamp.FromUnixTime();
                    ProdLineOrder.Started = order.started?.timestamp.FromUnixTime();
                    ProdLineOrder.Completion = order.completion?.timestamp.FromUnixTime();
                    ProdLineOrder.DurationMs = order.duration.millis;
                    ProdLineOrder.LastUpdated = order.lastUpdated?.timestamp.FromUnixTime();
                    ProdLineOrder.CompletionPecentage = order.completed;
                    ProdLineOrder.Halted = order.halted;
                    ProdLineOrder.FeeCurrencyCode = order.productionFee?.currency;
                    ProdLineOrder.Fee = order.productionFee?.amount;
                    ProdLineOrder.FeeCollectorId = order.productionFeeCollector?.id;
                    ProdLineOrder.FeeCollectorName = order.productionFeeCollector?.name;
                    ProdLineOrder.FeeCollectorCode = order.productionFeeCollector?.code;
                    ProdLineOrder.Recurring = order.recurring;
                    ProdLineOrder.ProductionLineId = ProdLine.ProductionLineId;
                    ProdLineOrder.ProductionLine = ProdLine;

                    ProdLine.Orders.Add(ProdLineOrder);
                }
                ProdLine.UserNameSubmitted = CurrentUser;
                ProdLine.Timestamp = DateTime.UtcNow;
                ProductionLines.Add(ProdLine);
            }

            if (!ProductionLines.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            if (ProductionLines.Count > 0)
            {
				using (var writer = DBAccess.GetWriter())
				{
					await writer.DB.ProductionLines
						.Where(s => s.UserNameSubmitted == CurrentUser)
						.Where(s => s.SiteId == ProductionLines.First().SiteId)
						.DeleteAsync();

					await writer.DB.ProductionLines
						.AddRangeAsync(ProductionLines);

					await writer.DB.SaveChangesAsync();
				}
			}

            return Ok();
        }

        /// <summary>
        /// PUTs ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED payload
        /// </summary>
        /// <param name="prodLineUpdatedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("updated")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutProdUpdated([FromBody] Payloads.Production.ROOT_PRODUCTION_PRODUCTION_LINE_UPDATED prodLineUpdatedPayload)
        {
            List<string> Errors = new();
            if (!prodLineUpdatedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var prodLine = prodLineUpdatedPayload.payload;

            ProductionLine ProdLine = new();
            ProdLine.ProductionLineId = prodLine.id;
            ProdLine.SiteId = prodLine.siteId;
            ProdLine.LocationId = prodLine.address.lines.Last().entity!.id;
            ProdLine.LocationNaturalId = prodLine.address.lines.Last().entity!.naturalId;
            ProdLine.LocationName = prodLine.address.lines.Last().entity!.name;
            ProdLine.BuildingName = prodLine.type;
            ProdLine.BuildingTicker = EntityCaches.BuildingCache.Get(ProdLine.BuildingName)?.Ticker;
            ProdLine.Capacity = prodLine.capacity;
            ProdLine.Slots = prodLine.slots;
            ProdLine.Efficiency = prodLine.efficiency;
            ProdLine.Condition = prodLine.condition;
            if (prodLine.workforces != null)
            {
                ProdLine.RawWorkforceEfficiencies = String.Join(' ', prodLine.workforces.Select(w => $"{w.level}:{w.efficiency}"));
            }
            
            if (prodLine.efficiencyFactors != null)
            {
                ProdLine.RawEfficiencyFactors = String.Join(',', prodLine.efficiencyFactors.Select(ef => $"{(ef.expertiseCategory != null ? ef.expertiseCategory : " ")}:{ef.type}->{ef.effectivity}!{ef.value}"));
            }
            
            if (prodLine.orders != null)
            {
                foreach (var order in prodLine.orders)
                {
                    ProductionLineOrder ProdLineOrder = new();
                    ProdLineOrder.ProductionLineOrderId = order.id;
                    if (order.inputs != null)
                    {
                        ProdLineOrder.RawInputs = string.Join(',', order.inputs.Select(i => $"{i.amount}x{i.material.ticker}"));
                    }
                    
                    ProdLineOrder.RawOutputs = string.Join(",", order.outputs.Select(o => $"{o.amount}x{o.material.ticker}"));
                    ProdLineOrder.Created = order.created.timestamp.FromUnixTime();
                    ProdLineOrder.Started = order.started?.timestamp.FromUnixTime();
                    ProdLineOrder.Completion = order.completion?.timestamp.FromUnixTime();
                    ProdLineOrder.DurationMs = order.duration.millis;
                    ProdLineOrder.LastUpdated = order.lastUpdated?.timestamp.FromUnixTime();
                    ProdLineOrder.CompletionPecentage = order.completed;
                    ProdLineOrder.Halted = order.halted;
                    ProdLineOrder.FeeCurrencyCode = order.productionFee?.currency;
                    ProdLineOrder.Fee = order.productionFee?.amount;
                    ProdLineOrder.FeeCollectorId = order.productionFeeCollector?.id;
                    ProdLineOrder.FeeCollectorName = order.productionFeeCollector?.name;
                    ProdLineOrder.FeeCollectorCode = order.productionFeeCollector?.code;
                    ProdLineOrder.Recurring = order.recurring;
                    ProdLineOrder.ProductionLineId = ProdLine.ProductionLineId;
                    ProdLineOrder.ProductionLine = ProdLine;

                    ProdLine.Orders.Add(ProdLineOrder);
                }
            }
            
            ProdLine.UserNameSubmitted = CurrentUser;
            ProdLine.Timestamp = DateTime.UtcNow;

            if (!ProdLine.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

#if USING_PRODUCTION_LINE_UPDATED_BAD_DATA_WORKAROUND
            await Task.CompletedTask;
            DelayedProdLineUpdatedHack.AddProdLine(ProdLine);
#else
            using (var writer = DBAccess.GetWriter())
            {
                var existingProdLine = await writer.DB.ProductionLines
                    .Include(pl => pl.Orders)
                    .FirstOrDefaultAsync(pl => pl.ProductionLineId == ProdLine.ProductionLineId);
                if (existingProdLine != null)
                {
                    existingProdLine.LocationName = ProdLine.LocationName;
                    existingProdLine.Capacity = ProdLine.Capacity;
                    existingProdLine.Slots = ProdLine.Slots;
                    existingProdLine.Efficiency = ProdLine.Efficiency;
                    existingProdLine.Condition = ProdLine.Condition;
                    existingProdLine.RawWorkforceEfficiencies = ProdLine.RawWorkforceEfficiencies;
                    existingProdLine.RawEfficiencyFactors = ProdLine.RawEfficiencyFactors;
                    existingProdLine.Orders = ProdLine.Orders;
                    existingProdLine.UserNameSubmitted = ProdLine.UserNameSubmitted;
                    existingProdLine.Timestamp = ProdLine.Timestamp;
                }
                else
                {
                    await writer.DB.ProductionLines.AddAsync(ProdLine);
                }

                await writer.DB.SaveChangesAsync();
            }
#endif

            return Ok();
        }

        /// <summary>
        /// PUTs MESG_PRODUCTION_PRODUCTION_LINE payload
        /// </summary>
        /// <param name="mesgProdLinePayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("msg_updated")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMesgProdUpdated([FromBody] Payloads.Production.MESG_PRODUCTION_PRODUCTION_LINE mesgProdLinePayload)
        {
            List<string> Errors = new();
            if (!mesgProdLinePayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var prodLine = mesgProdLinePayload.payload.message.payload;

            ProductionLine ProdLine = new();
            ProdLine.ProductionLineId = prodLine.id;
            ProdLine.SiteId = prodLine.siteId;
            ProdLine.LocationId = prodLine.address.lines.Last().entity!.id;
            ProdLine.LocationNaturalId = prodLine.address.lines.Last().entity!.naturalId;
            ProdLine.LocationName = prodLine.address.lines.Last().entity!.name;
            ProdLine.BuildingName = prodLine.type;
            ProdLine.BuildingTicker = EntityCaches.BuildingCache.Get(ProdLine.BuildingName)?.Ticker;
            ProdLine.Capacity = prodLine.capacity;
            ProdLine.Slots = prodLine.slots;
            ProdLine.Efficiency = prodLine.efficiency;
            ProdLine.Condition = prodLine.condition;
            if (prodLine.workforces != null)
            {
                ProdLine.RawWorkforceEfficiencies = String.Join(' ', prodLine.workforces.Select(w => $"{w.level}:{w.efficiency}"));
            }

            if (prodLine.efficiencyFactors != null)
            {
                ProdLine.RawEfficiencyFactors = String.Join(',', prodLine.efficiencyFactors.Select(ef => $"{(ef.expertiseCategory != null ? ef.expertiseCategory : " ")}:{ef.type}->{ef.effectivity}!{ef.value}"));
            }

            if (prodLine.orders != null)
            {
                foreach (var order in prodLine.orders)
                {
                    ProductionLineOrder ProdLineOrder = new();
                    ProdLineOrder.ProductionLineOrderId = order.id;
                    if (order.inputs != null)
                    {
                        ProdLineOrder.RawInputs = string.Join(',', order.inputs.Select(i => $"{i.amount}x{i.material.ticker}"));
                    }

                    ProdLineOrder.RawOutputs = string.Join(",", order.outputs.Select(o => $"{o.amount}x{o.material.ticker}"));
                    ProdLineOrder.Created = order.created.timestamp.FromUnixTime();
                    ProdLineOrder.Started = order.started?.timestamp.FromUnixTime();
                    ProdLineOrder.Completion = order.completion?.timestamp.FromUnixTime();
                    ProdLineOrder.DurationMs = order.duration.millis;
                    ProdLineOrder.LastUpdated = order.lastUpdated?.timestamp.FromUnixTime();
                    ProdLineOrder.CompletionPecentage = order.completed;
                    ProdLineOrder.Halted = order.halted;
                    ProdLineOrder.FeeCurrencyCode = order.productionFee?.currency;
                    ProdLineOrder.Fee = order.productionFee?.amount;
                    ProdLineOrder.FeeCollectorId = order.productionFeeCollector?.id;
                    ProdLineOrder.FeeCollectorName = order.productionFeeCollector?.name;
                    ProdLineOrder.FeeCollectorCode = order.productionFeeCollector?.code;
                    ProdLineOrder.Recurring = order.recurring;
                    ProdLineOrder.ProductionLineId = ProdLine.ProductionLineId;
                    ProdLineOrder.ProductionLine = ProdLine;

                    ProdLine.Orders.Add(ProdLineOrder);
                }
            }

            ProdLine.UserNameSubmitted = CurrentUser;
            ProdLine.Timestamp = DateTime.UtcNow;

            if (!ProdLine.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var existingProdLine = await writer.DB.ProductionLines
                    .Include(pl => pl.Orders)
                    .FirstOrDefaultAsync(pl => pl.ProductionLineId == ProdLine.ProductionLineId);
                if (existingProdLine != null)
                {
                    existingProdLine.LocationName = ProdLine.LocationName;
                    existingProdLine.Capacity = ProdLine.Capacity;
                    existingProdLine.Slots = ProdLine.Slots;
                    existingProdLine.Efficiency = ProdLine.Efficiency;
                    existingProdLine.Condition = ProdLine.Condition;
                    existingProdLine.RawWorkforceEfficiencies = ProdLine.RawWorkforceEfficiencies;
                    existingProdLine.RawEfficiencyFactors = ProdLine.RawEfficiencyFactors;
                    existingProdLine.Orders = ProdLine.Orders;
                    existingProdLine.UserNameSubmitted = ProdLine.UserNameSubmitted;
                    existingProdLine.Timestamp = ProdLine.Timestamp;
                }
                else
                {
                    await writer.DB.ProductionLines.AddAsync(ProdLine);
                }

                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// PUTs ROOT_PRODUCTION_ORDER_ADDED payload
        /// </summary>
        /// <param name="prodOrderAddedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("order/added")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutOrderAdded([FromBody] Payloads.Production.ROOT_PRODUCTION_ORDER_ADDED prodOrderAddedPayload)
        {
            List<string> Errors = new();
            if (!prodOrderAddedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = prodOrderAddedPayload.payload;

            ProductionLineOrder Order = new();
            Order.ProductionLineOrderId = data.id;
            if (data.inputs != null)
            {
                Order.RawInputs = string.Join(',', data.inputs.Select(i => $"{i.amount}x{i.material.ticker}"));
            }
            Order.RawOutputs = string.Join(",", data.outputs.Select(o => $"{o.amount}x{o.material.ticker}"));
            Order.Created = data.created.timestamp.FromUnixTime();
            Order.Started = data.started?.timestamp.FromUnixTime();
            Order.Completion = data.completion?.timestamp.FromUnixTime();
            Order.DurationMs = data.duration?.millis;
            Order.LastUpdated = data.lastUpdated?.timestamp.FromUnixTime();
            Order.CompletionPecentage = data.completed;
            Order.Halted = data.halted;
            Order.FeeCurrencyCode = data.productionFee?.currency;
            Order.Fee = data.productionFee?.amount;
            Order.FeeCollectorId = data.productionFeeCollector?.id;
            Order.FeeCollectorName = data.productionFeeCollector?.name;
            Order.FeeCollectorCode = data.productionFeeCollector?.code;
            Order.Recurring = data.recurring;
            Order.ProductionLineId = data.productionLineId;

            List<ValidationIgnore> ignores = new()
            {
                new ValidationIgnore(typeof(ProductionLineOrder), nameof(ProductionLineOrder.ProductionLine))
            };

            if (!Order.Validate(ref Errors, ValidationIgnores: ignores))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var ProductionLine = await writer.DB.ProductionLines
                    .Include(pl => pl.Orders)
                    .FirstOrDefaultAsync(pl => pl.ProductionLineId == Order.ProductionLineId);
                if (ProductionLine != null)
                {
                    var plo = ProductionLine.Orders.FirstOrDefault(o => o.ProductionLineOrderId == Order.ProductionLineId);
                    if (plo != null)
                    {
                        ProductionLine.Orders.Add(Order);
                        await writer.DB.SaveChangesAsync();
                    }
                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs MESG_PRODUCTION_ORDER_ADDED payload
        /// </summary>
        /// <param name="mesgOrderAddedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("order/msg_added")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMesgOrderAdded([FromBody] Payloads.Production.MESG_PRODUCTION_ORDER_ADDED mesgOrderAddedPayload)
        {
            List<string> Errors = new();
            if (!mesgOrderAddedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            return await PutOrderAdded(mesgOrderAddedPayload.payload.message);
        }

        /// <summary>
        /// PUTs ROOT_PRODUCTION_ORDER_UPDATED payload
        /// </summary>
        /// <param name="prodOrderUpdatedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("order/updated")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutOrderUpdated([FromBody] Payloads.Production.ROOT_PRODUCTION_ORDER_UPDATED prodOrderUpdatedPayload)
        {
            List<string> Errors = new();
            if (!prodOrderUpdatedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = prodOrderUpdatedPayload.payload;

            ProductionLineOrder Order = new();
            Order.ProductionLineOrderId = data.id;
            if (data.inputs != null)
            {
                Order.RawInputs = string.Join(',', data.inputs.Select(i => $"{i.amount}x{i.material.ticker}"));
            }
            Order.RawOutputs = string.Join(",", data.outputs.Select(o => $"{o.amount}x{o.material.ticker}"));
            Order.Created = data.created.timestamp.FromUnixTime();
            Order.Started = data.started?.timestamp.FromUnixTime();
            Order.Completion = data.completion?.timestamp.FromUnixTime();
            Order.DurationMs = data.duration?.millis;
            Order.LastUpdated = data.lastUpdated?.timestamp.FromUnixTime();
            Order.CompletionPecentage = data.completed;
            Order.Halted = data.halted;
            Order.FeeCurrencyCode = data.productionFee?.currency;
            Order.Fee = data.productionFee?.amount;
            Order.FeeCollectorId = data.productionFeeCollector?.id;
            Order.FeeCollectorName = data.productionFeeCollector?.name;
            Order.FeeCollectorCode = data.productionFeeCollector?.code;
            Order.Recurring = data.recurring;
            Order.ProductionLineId = data.productionLineId;

            List<ValidationIgnore> ignores = new()
            {
                new ValidationIgnore(typeof(ProductionLineOrder), nameof(ProductionLineOrder.ProductionLine))
            };

            if (!Order.Validate(ref Errors, ValidationIgnores: ignores))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                var ProductionLine = await writer.DB.ProductionLines
                    .Include(pl => pl.Orders)
                    .FirstOrDefaultAsync(pl => pl.ProductionLineId == Order.ProductionLineId);
                if (ProductionLine != null)
                {
                    var existingOrder = ProductionLine.Orders.FirstOrDefault(o => o.ProductionLineOrderId == Order.ProductionLineOrderId);
                    if (existingOrder != null)
                    {
                        existingOrder.RawInputs = Order.RawInputs;
                        existingOrder.Created = Order.Created;
                        existingOrder.Started = Order.Started;
                        existingOrder.Completion = Order.Completion;
                        existingOrder.DurationMs = Order.DurationMs;
                        existingOrder.LastUpdated = Order.LastUpdated;
                        existingOrder.CompletionPecentage = Order.CompletionPecentage;
                        existingOrder.Halted = Order.Halted;
                        existingOrder.FeeCurrencyCode = Order.FeeCurrencyCode;
                        existingOrder.Fee = Order.Fee;
                        existingOrder.FeeCollectorId = Order.FeeCollectorId;
                        existingOrder.FeeCollectorName = Order.FeeCollectorName;
                        existingOrder.FeeCollectorCode = Order.FeeCollectorCode;
                        existingOrder.Recurring = Order.Recurring;

                        await writer.DB.SaveChangesAsync();
                    }
                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs ROOT_PRODUCTION_ORDER_REMOVED payload
        /// </summary>
        /// <param name="prodOrderRemovedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("order/removed")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutOrderRemoved([FromBody] Payloads.Production.ROOT_PRODUCTION_ORDER_REMOVED prodOrderRemovedPayload)
        {
            List<string> Errors = new();
            if (!prodOrderRemovedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = prodOrderRemovedPayload.payload;
            var orderId = data.orderId;
            var prodLineId = data.productionLineId;

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.ProductionLineOrders
                    .Where(plo => plo.ProductionLineOrderId == orderId)
                    .ExecuteDeleteAsync();
            }

            await Task.CompletedTask;
            return Ok();
        }

        /// <summary>
        /// PUTs MESG_PRODUCTION_ORDER_REMOVED payload
        /// </summary>
        /// <param name="mesgOrderRemovedPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("order/msg_removed")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMesgOrderRemoved([FromBody] Payloads.Production.MESG_PRODUCTION_ORDER_REMOVED mesgOrderRemovedPayload)
        {
            List<string> Errors = new();
            if (!mesgOrderRemovedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            return await PutOrderRemoved(mesgOrderRemovedPayload.payload.message);
        }

        /// <summary>
        /// Retrieves ProductionLines for the provided UserName(s)
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group"></param>
        /// <param name="include_orders">If orders should be included</param>
        /// <returns>List of Sites</returns>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<ProductionLine>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetSites(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_orders")] bool include_orders = false)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<ProductionLine>> UserToSites;
            using (var reader = DBAccess.GetReader())
            {
                var UserToProdLinesQuery = reader.DB.ProductionLines.AsQueryable();
                if (include_orders)
                {
                    UserToProdLinesQuery = UserToProdLinesQuery.Include(pl => pl.Orders);
                }

                UserToSites = await UserToProdLinesQuery
                    .Where(s => UserToPerm.Keys.Contains(s.UserNameSubmitted))
                    .GroupBy(s => s.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToSites, UserToPerm);
            return Ok(Response);
        }
    }
}
