﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// CompanyController
    /// </summary>
    [ApiController]
    [Route("/company")]
    public class CompanyController : ControllerBase
    {
        /// <summary>
        /// PUTs a PATH_COMPANY payload
        /// </summary>
        /// <param name="data_payload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutCompany([FromBody] Payloads.Company.PATH_COMPANY data_payload)
        {
            List<string> Errors = new();
            if (!data_payload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = data_payload.payload.message.payload.body;

            Company company = new();
            company.CompanyId = data.id;
            company.Name = data.name;
            company.Code = data.code;
            company.Founded = data.founded.timestamp.FromUnixTime();
            company.UserName = data.user.username;
            company.CountryId = data.country.id;
            company.CountryCode = data.country.code!;
            company.CountryName = data.country.name;

            if (data.corporation != null)
            {
                company.CorporationId = data.corporation.id;
                company.CorporationName = data.corporation.name;
                company.CorporationCode = data.corporation.code;
            }

            company.CurrentAPEXRepresentationLevel = data.currentRepresentationLevel;
            
            if (data.ratingReport != null)         
            {
                company.OverallRating = data.ratingReport.overallRating;
                company.RatingContractCount = data.ratingReport.contractCount;
                if (data.ratingReport.earliestContract != null) {
                    company.RatingsEarliestContract = data.ratingReport.earliestContract.timestamp.FromUnixTime();
                }
            }

            if (data.reputation != null && data.reputation.Count > 0)
            {
                company.ReputationEntityName = data.reputation.First().entityId.name;
                company.Reputation = data.reputation.First().reputation;
            }

            // fill in the planets list
            company.Planets = new List<CompanyPlanet>();
            if (data.siteAddresses != null)
            {
                foreach(var address in data.siteAddresses)
                {
                    var planetEntity = address.lines.Last().entity!;
                    var planetId = address.lines.Last().entity!.id;

                    CompanyPlanet planet = new CompanyPlanet();
                    planet.CompanyPlanetId = $"{company.CompanyId}-{planetEntity.id}";
                    planet.PlanetId = planetEntity.id;
                    planet.PlanetNaturalId = planetEntity.naturalId;
                    planet.PlanetName = planetEntity.name;
                    planet.CompanyId = company.CompanyId;
                    planet.Company = company;
                    company.Planets.Add(planet);
                }
            }

            company.UserNameSubmitted = this.GetUserName()!;
            company.Timestamp = DateTime.UtcNow;

            if (!company.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            Company? existingCompany;
            using (var writer = DBAccess.GetWriter())
            {
                existingCompany = await writer.DB.Companies
                    .Include(c => c.Planets)
                    .Include(c => c.Offices)
                    .FirstOrDefaultAsync(c => c.CompanyId == company.CompanyId);
                if (existingCompany != null)
                {
                    // Update all non-private fields
                    existingCompany.UserName = company.UserName;
                    existingCompany.CurrentAPEXRepresentationLevel = company.CurrentAPEXRepresentationLevel;
                    existingCompany.OverallRating = company.OverallRating;
                    existingCompany.RatingsEarliestContract = company.RatingsEarliestContract;
                    existingCompany.RatingContractCount = company.RatingContractCount;
                    existingCompany.OverallRating = company.OverallRating;
                    existingCompany.Reputation = company.Reputation;
                    existingCompany.Planets = company.Planets;
                    existingCompany.Offices = company.Offices;
                    existingCompany.UserNameSubmitted = company.UserNameSubmitted;
                    existingCompany.Timestamp = company.Timestamp;
                }
                else
                {
                    await writer.DB.Companies.AddAsync(company);
                }

                await writer.DB.SaveChangesAsync();
            }

            if (existingCompany != null)
            {
                EntityCaches.CompanyCache.Update(existingCompany);
            }
            else
            {
                EntityCaches.CompanyCache.Update(company);
            }

            return Ok();
        }

        /// <summary>
        /// PUTs a ROOT_COMPANY_DATA payload
        /// </summary>
        /// <param name="companyData">Payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("selfroot")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutCompanyRoot([FromBody] Payloads.Company.ROOT_COMPANY_DATA companyData)
        {
            List<string> Errors = new();
            if (!companyData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = companyData.payload;

            Company company = new();
            company.CompanyId = data.id;
            company.Name = data.name;
            company.Code = data.code;
            company.UserName = CurrentUser;
            company.CountryId = data.countryId;
            company.CountryCode = "UNKN";
            company.CountryName = "UNKNOWN";
            company.CorporationId = Constants.InvalidAPEXID;
            company.CorporationName = "UNKNOWN";
            company.CorporationCode = "UNKN";

            if (data.representation != null)
            {
                company.CurrentAPEXRepresentationLevel = data.representation.currentLevel;
                company.RepresentationCurrentLevel = data.representation.currentLevel;
                company.RepresentationCostNextLevel = data.representation.costNextLevel.amount;
                company.RepresentationContributedNextLevel = data.representation.contributedNextLevel.amount;
                company.RepresentationLeftNextLevel = data.representation.leftNextLevel.amount;
                company.RepresentationContributedTotal = data.representation.contributedTotal.amount;
            }

            if (data.ratingReport != null)
            {
                company.OverallRating = data.ratingReport.overallRating;
                company.RatingsEarliestContract = data.ratingReport.earliestContract.timestamp.FromUnixTime();
                company.RatingContractCount = data.ratingReport.contractCount;
            }

            company.StartingProfile = data.startingProfile;
            company.StartingLocationId = data.startingLocation.lines.Last().entity!.id;
            company.StartingLocationName = data.startingLocation.lines.Last().entity!.name;
            company.StartingLocationName = data.startingLocation.lines.Last().entity!.naturalId;

            company.HeadquartersLocationId = data.headquarters.address.lines.Last().entity!.id;
            company.HeadquartersLocationNaturalId = data.headquarters.address.lines.Last().entity!.naturalId;
            company.HeadquartersLocationName = data.headquarters.address.lines.Last().entity!.name;
            company.HeadquartersLevel = data.headquarters.level;
            company.TotalBasePermits = data.headquarters.basePermits;
            company.UsedBasePermits = data.headquarters.usedBasePermits;
            company.AdditionalBasePermits = data.headquarters.additionalProductionQueueSlots;
            company.AdditionalProductionQueueSlots = data.headquarters.additionalProductionQueueSlots;
            company.HeadquartersNextRelocationTime = data.headquarters.nextRelocationTime?.timestamp.FromUnixTime();
            company.HeadquartersRelocationLocked = data.headquarters.relocationLocked;

            foreach (var efficiencyGain in data.headquarters.efficiencyGains)
            {
                var buildingCategoryType = Constants.BuildingCategoryToType[efficiencyGain.category];
                switch (buildingCategoryType)
                {
                    case Constants.BuildingCategoryType.Agriculture:
                        company.HeadquartersAgricultureEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Chemistry:
                        company.HeadquartersChemistryEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Construction:
                        company.HeadquartersConstructionEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Electronics:
                        company.HeadquartersElectronicsEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.FoodIndustries:
                        company.HeadquartersFoodIndustriesEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.FuelRefining:
                        company.HeadquartersFuelRefiningEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Manufacturing:
                        company.HeadquartersManufacturingEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Metallurgy:
                        company.HeadquartersMetallurgyEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.ResourceExtraction:
                        company.HeadquartersResourceExtractionEfficiencyGain = efficiencyGain.gain;
                        break;
                }
            }

            company.UserNameSubmitted = this.GetUserName()!;
            company.Timestamp = DateTime.UtcNow;

            Company? existingCompany;
            using (var writer = DBAccess.GetWriter())
            {
                existingCompany = await writer.DB.Companies
                    .Include(c => c.Planets)
                    .Include(c => c.Offices)
                    .FirstOrDefaultAsync(c => c.Code == company.Code);
                if (existingCompany != null)
                {
                    // Update data we have
                    existingCompany.UserName = company.UserName;
                    existingCompany.CountryId = company.CountryId;
                    existingCompany.OverallRating = company.OverallRating;
                    existingCompany.StartingProfile = company.StartingProfile;
                    existingCompany.StartingLocationId = company.StartingLocationId;
                    existingCompany.StartingLocationName = company.StartingLocationName;
                    existingCompany.StartingLocationName = company.StartingLocationName;

                    existingCompany.CurrentAPEXRepresentationLevel = company.CurrentAPEXRepresentationLevel;
                    existingCompany.RepresentationCurrentLevel = company.RepresentationCurrentLevel;
                    existingCompany.RepresentationCostNextLevel = company.RepresentationCostNextLevel;
                    existingCompany.RepresentationContributedNextLevel = company.RepresentationContributedNextLevel;
                    existingCompany.RepresentationLeftNextLevel = company.RepresentationLeftNextLevel;
                    existingCompany.RepresentationContributedTotal = company.RepresentationContributedTotal;

                    existingCompany.OverallRating = company.OverallRating;
                    existingCompany.RatingsEarliestContract = company.RatingsEarliestContract;
                    existingCompany.RatingContractCount = company.RatingContractCount;

                    existingCompany.HeadquartersLocationId = company.HeadquartersLocationId;
                    existingCompany.HeadquartersLocationNaturalId = company.HeadquartersLocationNaturalId;
                    existingCompany.HeadquartersLocationName = company.HeadquartersLocationName;
                    existingCompany.HeadquartersLevel = company.HeadquartersLevel;
                    existingCompany.TotalBasePermits = company.TotalBasePermits;
                    existingCompany.UsedBasePermits = company.UsedBasePermits;
                    existingCompany.AdditionalBasePermits = company.AdditionalBasePermits;
                    existingCompany.AdditionalProductionQueueSlots = company.AdditionalProductionQueueSlots;
                    existingCompany.HeadquartersNextRelocationTime = company.HeadquartersNextRelocationTime;
                    existingCompany.HeadquartersRelocationLocked = company.HeadquartersRelocationLocked;

                    existingCompany.HeadquartersChemistryEfficiencyGain = company.HeadquartersChemistryEfficiencyGain;
                    existingCompany.HeadquartersConstructionEfficiencyGain = company.HeadquartersConstructionEfficiencyGain;
                    existingCompany.HeadquartersElectronicsEfficiencyGain = company.HeadquartersElectronicsEfficiencyGain;
                    existingCompany.HeadquartersFoodIndustriesEfficiencyGain = company.HeadquartersFoodIndustriesEfficiencyGain;
                    existingCompany.HeadquartersFuelRefiningEfficiencyGain = company.HeadquartersFuelRefiningEfficiencyGain;
                    existingCompany.HeadquartersManufacturingEfficiencyGain = company.HeadquartersManufacturingEfficiencyGain;
                    existingCompany.HeadquartersMetallurgyEfficiencyGain = company.HeadquartersMetallurgyEfficiencyGain;
                    existingCompany.HeadquartersResourceExtractionEfficiencyGain = company.HeadquartersResourceExtractionEfficiencyGain;

                    existingCompany.UserNameSubmitted = CurrentUser;
                    existingCompany.Timestamp = DateTime.UtcNow;
                }
                else
                {
                    await writer.DB.Companies.AddAsync(company);
                }

                await writer.DB.SaveChangesAsync();
            }

            if (existingCompany != null)
            {
                EntityCaches.CompanyCache.Update(existingCompany);
            }
            else
            {
                EntityCaches.CompanyCache.Update(company);
            }

            return Ok();
        }

        /// <summary>
        /// PUTs a MESG_COMPANY_DATA payload
        /// </summary>
        /// <param name="companyData">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("selfmsg")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutCompanySelf([FromBody] Payloads.Company.MESG_COMPANY_DATA companyData)
        {
            List<string> Errors = new();
            if (!companyData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = companyData.payload.message.payload;
            Company company = new();
            company.CompanyId = data.id;
            company.Name = data.name;
            company.Code = data.code;
            company.UserName = CurrentUser;
            company.CountryId = data.countryId;
            company.CountryCode = "UNKN";
            company.CountryName = "UNKNOWN";
            company.CorporationId = Constants.InvalidAPEXID;
            company.CorporationName = "UNKNOWN";
            company.CorporationCode = "UNKN";

            if (data.ratingReport != null)
            {
                company.OverallRating = data.ratingReport.overallRating;
            }

            company.StartingProfile = data.startingProfile;
            company.StartingLocationId = data.startingLocation.lines.Last().entity!.id;
            company.StartingLocationName = data.startingLocation.lines.Last().entity!.name;
            company.StartingLocationName = data.startingLocation.lines.Last().entity!.naturalId;

            company.HeadquartersLocationId = data.headquarters.address.lines.Last().entity!.id;
            company.HeadquartersLocationNaturalId = data.headquarters.address.lines.Last().entity!.naturalId;
            company.HeadquartersLocationName = data.headquarters.address.lines.Last().entity!.name;
            company.HeadquartersLevel = data.headquarters.level;
            company.TotalBasePermits = data.headquarters.basePermits;
            company.UsedBasePermits = data.headquarters.usedBasePermits;
            company.AdditionalBasePermits = data.headquarters.additionalProductionQueueSlots;
            company.AdditionalProductionQueueSlots = data.headquarters.additionalProductionQueueSlots;
            company.HeadquartersNextRelocationTime = data.headquarters.nextRelocationTime?.timestamp.FromUnixTime();
            company.HeadquartersRelocationLocked = data.headquarters.relocationLocked;

            foreach (var efficiencyGain in data.headquarters.efficiencyGains)
            {
                var buildingCategoryType = Constants.BuildingCategoryToType[efficiencyGain.category];
                switch(buildingCategoryType)
                {
                    case Constants.BuildingCategoryType.Agriculture:
                        company.HeadquartersAgricultureEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Chemistry:
                        company.HeadquartersChemistryEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Construction:
                        company.HeadquartersConstructionEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Electronics:
                        company.HeadquartersElectronicsEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.FoodIndustries:
                        company.HeadquartersFoodIndustriesEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.FuelRefining:
                        company.HeadquartersFuelRefiningEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Manufacturing:
                        company.HeadquartersManufacturingEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.Metallurgy:
                        company.HeadquartersMetallurgyEfficiencyGain = efficiencyGain.gain;
                        break;
                    case Constants.BuildingCategoryType.ResourceExtraction:
                        company.HeadquartersResourceExtractionEfficiencyGain = efficiencyGain.gain;
                        break;
                }
            }

            company.UserNameSubmitted = this.GetUserName()!;
            company.Timestamp = DateTime.UtcNow;

            if (!company.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            if (!company.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            Company? existingCompany;
            using (var writer = DBAccess.GetWriter())
            {
				// COLIQing is a thing, so we need to gracefully handle that.
				await writer.DB.Companies
				    .Where(c => c.Code == company.Code && c.CompanyId != company.CompanyId)
					.DeleteAsync();

                existingCompany = await writer.DB.Companies
                    .Include(c => c.Planets)
                    .Include(c => c.Offices)
                    .FirstOrDefaultAsync(c => c.Code == company.Code);
                if (existingCompany != null)
                {
                    // Update data we have
                    existingCompany.UserName = company.UserName;
                    existingCompany.CountryId = company.CountryId;
                    existingCompany.OverallRating = company.OverallRating;
                    existingCompany.StartingProfile = company.StartingProfile;
                    existingCompany.StartingLocationId = company.StartingLocationId;
                    existingCompany.StartingLocationName = company.StartingLocationName;
                    existingCompany.StartingLocationName = company.StartingLocationName;

                    existingCompany.HeadquartersLocationId = company.HeadquartersLocationId;
                    existingCompany.HeadquartersLocationNaturalId = company.HeadquartersLocationNaturalId;
                    existingCompany.HeadquartersLocationName = company.HeadquartersLocationName;
                    existingCompany.HeadquartersLevel = company.HeadquartersLevel;
                    existingCompany.TotalBasePermits = company.TotalBasePermits;
                    existingCompany.UsedBasePermits = company.UsedBasePermits;
                    existingCompany.AdditionalBasePermits = company.AdditionalBasePermits;
                    existingCompany.AdditionalProductionQueueSlots = company.AdditionalProductionQueueSlots;
                    existingCompany.HeadquartersNextRelocationTime = company.HeadquartersNextRelocationTime;
                    existingCompany.HeadquartersRelocationLocked = company.HeadquartersRelocationLocked;

                    existingCompany.HeadquartersChemistryEfficiencyGain = company.HeadquartersChemistryEfficiencyGain;
                    existingCompany.HeadquartersConstructionEfficiencyGain = company.HeadquartersConstructionEfficiencyGain;
                    existingCompany.HeadquartersElectronicsEfficiencyGain = company.HeadquartersElectronicsEfficiencyGain;
                    existingCompany.HeadquartersFoodIndustriesEfficiencyGain = company.HeadquartersFoodIndustriesEfficiencyGain;
                    existingCompany.HeadquartersFuelRefiningEfficiencyGain = company.HeadquartersFuelRefiningEfficiencyGain;
                    existingCompany.HeadquartersManufacturingEfficiencyGain = company.HeadquartersManufacturingEfficiencyGain;
                    existingCompany.HeadquartersMetallurgyEfficiencyGain = company.HeadquartersMetallurgyEfficiencyGain;
                    existingCompany.HeadquartersResourceExtractionEfficiencyGain = company.HeadquartersResourceExtractionEfficiencyGain;

                    existingCompany.UserNameSubmitted = CurrentUser;
                    existingCompany.Timestamp = DateTime.UtcNow;
                }
                else
                {
                    await writer.DB.Companies.AddAsync(company);
                }

                if (existingCompany != null)
                {
                    EntityCaches.CompanyCache.Update(existingCompany);
                }
                else
                {
                    EntityCaches.CompanyCache.Update(company);
                }

                await writer.DB.SaveChangesAsync();
            }
            return Ok();
        }

        /// <summary>
        /// PUTs a PATH_COMPANIES_OFFICES payload
        /// </summary>
        /// <param name="data_payload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("offices")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutCompanyOffices([FromBody] Payloads.Company.PATH_COMPANIES_OFFICES data_payload)
        {
            List<string> Errors = new();
            if (!data_payload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = data_payload.payload.message.payload;
            var Offices = data.body.Select(d => new CompanyOffice
            {
                CompanyOfficeId = $"{data.path[1]}-{d.start.timestamp}",
                Type = d.type,
                Start = d.start.timestamp.FromUnixTime(),
                End = d.end.timestamp.FromUnixTime(),
                PlanetId = d.address.lines.Last().entity!.id,
                PlanetName = d.address.lines.Last().entity!.name,
                PlanetNaturalId = d.address.lines.Last().entity!.naturalId,
                CompanyId = data.path[1],
            }).ToList();

            // @TODO: Add means of ignoring a specific field for Validation (in this case, .Company)

            Company? company;
            using (var writer = DBAccess.GetWriter())
            {
                company = writer.DB.Companies
                    .Include(c => c.Offices)
                    .Include(c => c.Planets)
                    .FirstOrDefault(c => c.CompanyId == data.path[1]);
                if (company != null)
                {
                    Offices.ForEach(o => o.Company = company);

                    company.Offices.Clear();
                    company.Offices.AddRange(Offices);

                    await writer.DB.SaveChangesAsync();
                }
            }

            if (company != null)
            {
                EntityCaches.CompanyCache.Update(company);
            }

            return Ok();
        }

        /// <summary>
        /// PUTs account cash balances
        /// </summary>
        /// <param name="balances">The balances payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("accounting")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutAccounting([FromBody] Payloads.Company.MESG_ACCOUNTING_CASH_BALANCES balances)
        {
            List<string> Errors = new();
            if (!balances.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var thisUser = this.GetUserName()!;

            var currencyData = balances.payload.message.payload.currencyAccounts.Select(ca => ca.currencyBalance).ToList();
            var aic = currencyData.FirstOrDefault(cd => cd.currency == "AIC")?.amount;
            var cis = currencyData.FirstOrDefault(cd => cd.currency == "CIS")?.amount;
            var ecd = currencyData.FirstOrDefault(cd => cd.currency == "ECD")?.amount;
            var ica = currencyData.FirstOrDefault(cd => cd.currency == "ICA")?.amount;
            var ncc = currencyData.FirstOrDefault(cd => cd.currency == "NCC")?.amount;
            
            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Companies
                    .Where(c => c.UserName == thisUser)
                    .ExecuteUpdateAsync(setter =>
                        setter
                            .SetProperty(c => c.AICLiquid, aic)
                            .SetProperty(c => c.CISLiquid, cis)
                            .SetProperty(c => c.ECDLiquid, ecd)
                            .SetProperty(c => c.ICALiquid, ica)
                            .SetProperty(c => c.NCCLiquid, ncc));
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves a list of companies specified via url parameters.
        /// </summary>
        /// <param name="companies">The company code/id/name</param>
        /// <param name="include_planets">If planets should be included in the result</param>
        /// <param name="include_offices">If offices should be included in the result</param>
        /// <returns>A potentially sparse list (null entries if not found) of companies</returns>
        [HttpGet("lookup")]
        [AllowAnonymous]
        [Cache(Minutes = 10)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Company?>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Did not specify any companies or too many (max 10)")]
        public IActionResult GetCompany(
            [FromQuery(Name = "company")] List<string> companies,
            [FromQuery(Name = "include_planets")] bool include_planets = false,
            [FromQuery(Name = "include_offices")] bool include_offices = false)
        {
            if (companies.Count == 0 || companies.Count > 10)
            {
                return BadRequest();
            }

            List<Company?> Results = EntityCaches.CompanyCache.Get(companies);
            if (!include_planets)
            {
                Results.ForEach(r =>
                {
                    r?.Planets.Clear();
                });
            }

            if (!include_offices)
            {
                Results.ForEach(r =>
                {
                    r?.Offices.Clear();
                });
            }

            List<string> Errors = new();
            Results.ForEach(r =>
            {
                if (r != null)
                {
                    PermissionSanitizer.ApplyPermissionSanitizations(Perm.None, ref r, ref Errors);
                }
            });

            return Ok(Results);
        }

#if false
        /// <summary>
        /// Retrieves company data by company id
        /// </summary>
        /// <param name="CompanyId">CompanyId</param>
        /// <param name="include_planets">Whether planets should be included</param>
        /// <param name="include_offices">Whether offices should be included</param>
        /// <returns>OK</returns>
        [HttpGet("id/{CompanyId}")]
        [AllowAnonymous]
        [Cache(Minutes = 10)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(Company), "application/json")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "Company Code not found")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid CompanyCode", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetCompanyById(string CompanyId,
            [FromQuery(Name = "include_planets")] bool include_planets = false,
            [FromQuery(Name = "include_offices")] bool include_offices = false)
        {
            CompanyId = CompanyId.Trim().ToLower();

            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(CompanyId, typeof(Company), nameof(Company.CompanyId), ref Errors, "CompanyCode");
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            Company? Result = new();
            using (var reader = DBAccess.GetReader())
            {
                var CompanyQuery = reader.DB.Companies.AsQueryable();

                if (include_planets)
                {
                    CompanyQuery = CompanyQuery.Include(c => c.Planets);
                }

                if (include_offices)
                {
                    CompanyQuery = CompanyQuery.Include(c => c.Offices);
                }

                Result = await CompanyQuery
                    .Where(c => c.CompanyId == CompanyId)
                    .FirstOrDefaultAsync();
            }

            if (Result != null)
            {
                // Discard any errors because they're expected
                PermissionSanitizer.ApplyPermissionSanitizations(Perm.None, ref Result, ref Errors);
                return Ok(Result);
            }
            else
            {
                return NoContent();
            }
        }

        /// <summary>
        /// Retrieves company data by company code
        /// </summary>
        /// <param name="CompanyCode">CompanyCode</param>
        /// <param name="include_planets">Whether planets should be included</param>
        /// <param name="include_offices">Whether offices should be included</param>
        /// <returns>OK</returns>
        [HttpGet("code/{CompanyCode}")]
        [AllowAnonymous]
        [Cache(Minutes = 10)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(Company), "application/json")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "Company Code not found")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid CompanyCode", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetCompanyByCode(string CompanyCode,
            [FromQuery(Name = "include_planets")] bool include_planets = false,
            [FromQuery(Name = "include_offices")] bool include_offices = false)
        {
            CompanyCode = CompanyCode.Trim().ToUpper();

            List<string> Errors = new();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(CompanyCode, typeof(Company), nameof(Company.Code), ref Errors, "CompanyCode");
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            Company? Result = new();
            using (var reader = DBAccess.GetReader())
            {
                var CompanyQuery = reader.DB.Companies.AsQueryable();

                if (include_planets)
                {
                    CompanyQuery = CompanyQuery.Include(c => c.Planets);
                }

                if (include_offices)
                {
                    CompanyQuery = CompanyQuery.Include(c => c.Offices);
                }

                Result = await CompanyQuery
                    .Where(c => c.Code == CompanyCode)
                    .FirstOrDefaultAsync();
            }

            if (Result != null)
            {
                // Discard any errors because they're expected
                PermissionSanitizer.ApplyPermissionSanitizations(Perm.None, ref Result, ref Errors);
                return Ok(Result);
            }
            else
            {
                return NoContent();
            }
        }
#endif


        /// <summary>
        /// Retrieves company data
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group">The group</param>
        /// <param name="include_planets">Whether planets should be included</param>
        /// <param name="include_offices">Whether offices should be included</param>
        /// <returns></returns>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<Company>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> GetCompany(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_planets")] bool include_planets = false,
            [FromQuery(Name = "include_offices")] bool include_offices = false)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, Company> UserToCompany;
            using (var reader = DBAccess.GetReader())
            {
                var UserToCompanyQuery = reader.DB.Companies.AsQueryable();
                if (include_planets)
                {
                    UserToCompanyQuery.Include(c => c.Planets);
                }

                if (include_offices)
                {
                    UserToCompanyQuery.Include(c => c.Offices);
                }

                UserToCompany = await UserToCompanyQuery
                    .Where(c => UserToPerm.Keys.Contains(c.UserName))
                    .GroupBy(c => c.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.First());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToCompany, UserToPerm);
            return Ok(Response);
        }
    }
}
