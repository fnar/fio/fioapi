﻿namespace FIOAPI.Controllers
{
    /// <summary>
    /// Helper class for defining all AuthPolicies
    /// </summary>
    public static class AuthPolicy
    {
        /// <summary>
        /// UserRead
        /// </summary>
        public const string UserRead = "userread";

        /// <summary>
        /// UserWrite
        /// </summary>
        public const string UserWrite = "userwrite";

        /// <summary>
        /// AdminRead
        /// </summary>
        public const string AdminRead = "adminread";

        /// <summary>
        /// AdminWrite
        /// </summary>
        public const string AdminWrite = "adminwrite";

        /// <summary>
        /// Bot
        /// </summary>
        public const string Bot = "bot";
    }
}
