﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using FIOAPI.TimedEvents;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// APEXUserController
    /// </summary>
    [ApiController]
    [Route("/apexuser")]
    public class APEXUserController : ControllerBase
    {
        /// <summary>
        /// PUTs PATH_USERS payload
        /// </summary>
        /// <param name="userPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutAPEXUser([FromBody] Payloads.User.PATH_USERS userPayload)
        {
            List<string> Errors = new();
            if (!userPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = userPayload.payload.message.payload.body;

            var user = new APEXUser();
            user.APEXUserId = data.id;
            user.UserName = data.username;
            user.SubscriptionLevel = data.subscriptionLevel;
            user.HighestTier = data.highestTier;
            user.CompanyId = data.company?.id;
            user.CompanyName = data.company?.name;
            user.CompanyCode = data.company?.code;
            user.CreatedTimestamp = data.created.timestamp;
            user.Team = data.team;
            user.Moderator = data.moderator;
            user.Pioneer = data.pioneer;
            user.ActiveDaysPerWeek = data.activeDaysPerWeek;

            if (!user.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            if (FIOHashCache.ShouldUpdate(user))
            {
                using (var writer = DBAccess.GetWriter())
                {
                    var existingUser = await writer.DB.APEXUsers
                        .FirstOrDefaultAsync(u => u.APEXUserId == user.APEXUserId);

                    if (existingUser != null)
                    {
                        existingUser.UserName = user.UserName;
                        existingUser.SubscriptionLevel = user.SubscriptionLevel;
                        existingUser.HighestTier = user.HighestTier;
                        existingUser.CompanyId = user.CompanyId;
                        existingUser.CompanyName = user.CompanyName;
                        existingUser.CompanyCode = user.CompanyCode;
                        existingUser.CreatedTimestamp = user.CreatedTimestamp;
                        existingUser.Team = user.Team;
                        existingUser.Moderator = user.Moderator;
                        existingUser.Pioneer = user.Pioneer;
                        existingUser.ActiveDaysPerWeek = user.ActiveDaysPerWeek;
                    }
                    else
                    {
                        await writer.DB.AddAsync(user);
                    }

                    await writer.DB.SaveChangesAsync();
                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs ROOT_PRESENCE_LIST payload
        /// </summary>
        /// <param name="presenceList">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("presence")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public IActionResult PutPresence([FromBody] Payloads.User.ROOT_PRESENCE_LIST presenceList)
        {
            List<string> Errors = new();
            if (!presenceList.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var presenceIds = presenceList.payload.users.Select(u => u.id).ToList();
            Presence.UpdatePresence(presenceIds);

            return Ok();
        }

        /// <summary>
        /// Retrieves user(s) that match the user. (If not present, retrieves all users
        /// </summary>
        /// <param name="user">UserId, UserName, or CompanyCode</param>
        /// <returns></returns>
        [HttpGet("")]
        [AllowAnonymous]
        [Cache(Minutes = 15)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        public async Task<IActionResult> GetUsers(
            [FromQuery(Name = "user")] List<string>? user)
        {
            List<APEXUser> APEXUsers;
            using (var reader = DBAccess.GetReader())
            {
                var APEXUsersQuery = reader.DB.APEXUsers.AsQueryable();
                if (user != null && user.Count > 0)
                {
                    APEXUsersQuery = APEXUsersQuery
                        .Where(u => user.Contains(u.APEXUserId) || (u.CompanyCode != null && user.Contains(u.CompanyCode)) || (u.UserName != null && user.Contains(u.UserName)));
                }

                APEXUsers = await APEXUsersQuery.ToListAsync();
            }

            return Ok(APEXUsers);
        }
    }
}
