﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// MaterialController
    /// </summary>
    [ApiController]
    [Route("/material")]
    public class MaterialController : ControllerBase
    {
        /// <summary>
        /// PUTs the materials payload
        /// </summary>
        /// <returns></returns>
        [HttpPut("")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMaterial([FromBody] Payloads.Material.MESG_WORLD_MATERIAL_DATA MaterialData)
        {
            List<string> Errors = new();
            if (!MaterialData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = MaterialData.payload.message.payload;

            var Material = new Material();
            Material.MaterialId = data.material.id;
            Material.Name = data.material.name;
            Material.Ticker = data.material.ticker;
            Material.MaterialCategoryId = data.material.category;
            Material.Weight = data.material.weight;
            Material.Volume = data.material.volume;
            Material.IsResource = data.isResource;
            Material.ResourceType = data.resourceType;
            Material.InfrastructureUsage = data.infrastructureUsage.Any();
            Material.COGCUsage = data.cogcUsage;
            Material.WorkforceUsage = data.workforceUsage;
            Material.UserNameSubmitted = this.GetUserName()!;
            Material.Timestamp = DateTime.UtcNow;

            if (!Material.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Materials.Upsert(Material)
                    .On(m => m.MaterialId)
                    .RunAsync();
            }

            _ = EntityCaches.MaterialCache.UpdateAsync(Material);

            return Ok();
        }

        /// <summary>
        /// PUTs the material categories payload
        /// </summary>
        /// <returns></returns>
        [HttpPut("category")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMaterialCategories([FromBody] Payloads.Material.MESG_WORLD_MATERIAL_CATEGORIES CategoryData)
        {
            List<string> Errors = new();
            if (!CategoryData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var categories = CategoryData.payload.message.payload.categories;
            var MaterialCategories = categories.Select(c => new MaterialCategory
            {
                MaterialCategoryId = c.id,
                CategoryName = c.name,
                UserNameSubmitted = this.GetUserName()!,
                Timestamp = DateTime.UtcNow,
            }).ToList();

            MaterialCategories.ForEach(mc => mc.Validate(ref Errors));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.MaterialCategories.UpsertRange(MaterialCategories)
                    .On(mc => mc.MaterialCategoryId)
                    .RunAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves materials given the provided parameters. If no parameters given, returns all materials.
        /// </summary>
        /// <param name="id">material id(s)</param>
        /// <param name="ticker">material ticker(s)</param>
        /// <param name="name">material name(s)</param>
        /// <param name="category_id">category id(s)</param>
        /// <returns>Materials that match the given parameters</returns>
        [HttpGet("")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Material>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid GET parameter(s)", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetMaterials(
            [FromQuery(Name = "id")] List<string> id,
            [FromQuery(Name = "ticker")] List<string> ticker,
            [FromQuery(Name = "name")] List<string> name,
            [FromQuery(Name = "category_id")] List<string> category_id
            )
        {
            List<string> Errors = new();
            for (int i = 0; i < id.Count; ++i)
            {
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(id[i], typeof(Material), nameof(Material.MaterialId), ref Errors, $"{nameof(id)}[{i}]");
            }
            
            for (int i = 0; i < ticker.Count; ++i)
            {
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(ticker[i], typeof(Material), nameof(Material.Ticker), ref Errors, $"{nameof(ticker)}[{i}]");
            }

            for (int i = 0; i < name.Count; ++i)
            {
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(name[i], typeof(Material), nameof(Material.Name), ref Errors, $"{nameof(name)}[{i}]");
            }

            for (int i = 0; i < category_id.Count; ++i)
            {
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(category_id[i], typeof(Material), nameof(Material.MaterialCategoryId), ref Errors, $"{nameof(category_id)}[{i}]");
            }

            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            List<Material> materials = new();
            using (var reader = DBAccess.GetReader())
            {
                var mats = reader.DB.Materials.AsQueryable();
                if (id.Count > 0 || ticker.Count > 0 || name.Count > 0 || category_id.Count > 0)
                {
                    mats = mats.Where(m =>
                        id.Contains(m.MaterialId) ||
                        ticker.Contains(m.Ticker) ||
                        name.Contains(m.Name) ||
                        category_id.Contains(m.MaterialCategoryId));
                }
                materials = await mats.ToListAsync();
            }
            
            return Ok(materials);
        }

        /// <summary>
        /// Retrieves a list of materials which are resources (extractable from a planet)
        /// </summary>
        /// <returns>A list of materials which are resources</returns>
        [HttpGet("resources")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.Material>), "application/json")]
        public async Task<IActionResult> GetMaterialResources()
        {
            List<Material> resources = new();
            using (var reader = DBAccess.GetReader())
            {
                resources = await reader.DB.Materials
                    .Where(m => m.IsResource)
                    .ToListAsync();
            }

            return Ok(resources);
        }

        /// <summary>
        /// Retrieves a list of material categories
        /// </summary>
        /// <returns>A list of material categories</returns>
        [HttpGet("categories")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.MaterialCategory>), "application/json")]
        public async Task<IActionResult> GetMaterialCategories()
        {
            List<Model.MaterialCategory> MaterialCategories;
            using (var reader = DBAccess.GetReader())
            {
                MaterialCategories = await reader.DB.MaterialCategories.ToListAsync();
            }
            return Ok(MaterialCategories);
        }
    }
}
