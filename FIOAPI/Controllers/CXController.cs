﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using System.Linq.Expressions;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// CXController
    /// </summary>
    [ApiController]
    [Route("/cx")]
    public class CXController : ControllerBase
    {
        /// <summary>
        /// PUTs CX_PRICES payload to FIOAPI
        /// </summary>
        /// <param name="pricesPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutCXPrices([FromBody] Payloads.CX.CX_PRICES pricesPayload)
        {
            List<string> Errors = new();
            if (!pricesPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var UserName = this.GetUserName()!;

            var data = pricesPayload.payload.message.payload;
            CXEntry Entry = new()
            {
                CXEntryId = data.id,
                MaterialId = data.material.id,
                MaterialTicker = data.material.ticker,
                ExchangeId = data.exchange.id,
                ExchangeCode = data.exchange.code!,
                CurrencyCode = data.currency.code,
                Price = data.price?.amount,
                PriceTime = data.priceTime?.timestamp.FromUnixTime(),
                High = data.high?.amount,
                AllTimeHigh = data.allTimeHigh?.amount,
                Low = data.low?.amount,
                AllTimeLow = data.allTimeLow?.amount,
                Ask = data.ask?.price.amount,
                AskCount = data.ask?.amount,
                Bid = data.bid?.price.amount,
                BidCount = data.bid?.amount,
                Supply = data.supply,
                Demand = data.demand,
                Traded = data.traded,
                Volume = data.volume?.amount,
                PriceAverage = data.priceAverage?.amount,
                NarrowPriceBandLow = data.narrowPriceBand?.low,
                NarrowPriceBandHigh = data.narrowPriceBand?.high,
                WidePriceBandLow = data.widePriceBand?.low,
                WidePriceBandHigh = data.widePriceBand?.high,
                UserNameSubmitted = UserName,
                Timestamp = DateTime.UtcNow,
            };

            Entry.BuyOrders = data.buyingOrders
                .Select(bo => new CXBuyOrder
            {
                CXBuyOrderId = bo.id,
                CompanyId = bo.trader.id,
                CompanyName = bo.trader.name,
                CompanyCode = bo.trader.code!,
                ItemCount = bo.amount,
                ItemCost = bo.limit.amount,
                CXEntryId = Entry.CXEntryId,
                CXEntry = Entry
                }).ToList();

            Entry.SellOrders = data.sellingOrders
                .Select(so => new CXSellOrder
                {
                    CXSellOrderId = so.id,
                    CompanyId = so.trader.id,
                    CompanyName = so.trader.name,
                    CompanyCode = so.trader.code!,
                    ItemCount = so.amount,
                    ItemCost = so.limit.amount,
                    CXEntryId = Entry.CXEntryId,
                    CXEntry = Entry
                })
                .ToList();

            var mmbuy = Entry.BuyOrders.Where(order => order.ItemCount == null).FirstOrDefault();
            if (mmbuy != null)
            {
                Entry.MMBuy = mmbuy.ItemCost;
            }

            var mmsell = Entry.SellOrders.Where(order => order.ItemCount == null).FirstOrDefault();
            if (mmsell != null)
            {
                Entry.MMSell = mmsell.ItemCost;
            }

            if (!Entry.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            if (FIOHashCache.ShouldUpdate(Entry))
            {
                using (var writer = DBAccess.GetWriter())
                {
                    await writer.DB.CXEntries
                        .Upsert(Entry)
                        .RunAsync();

                    await writer.DB.CXBuyOrders
                        .UpsertRange(Entry.BuyOrders)
                        .RunAsync();

                    await writer.DB.CXSellOrders
                        .UpsertRange(Entry.SellOrders)
                        .RunAsync();
                }

                StreamingBroadcastService.PushCXEntry(Entry);
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves all prices
        /// </summary>
        /// <param name="include_buy_orders">If buys should be included</param>
        /// <param name="include_sell_orders">If sells should be included</param>
        /// <returns>All prices</returns>
        [HttpGet("prices")]
        [AllowAnonymous]
        [Cache(Minutes = 15)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<CXEntry>), "application/json")]
        public async Task<IActionResult> GetAllPrices(
            [FromQuery(Name = "include_buy_orders")] bool include_buy_orders = false,
            [FromQuery(Name = "include_sell_orders")] bool include_sell_orders = false)
        {
            List<CXEntry> CXEntries = new();
            using (var reader = DBAccess.GetReader())
            {
                var CXQuery = reader.DB.CXEntries.AsQueryable();

                if (include_buy_orders)
                {
                    CXQuery = CXQuery.Include(cx => cx.BuyOrders);
                }

                if (include_sell_orders)
                {
                    CXQuery = CXQuery.Include(cx => cx.SellOrders);
                }

                CXEntries = await CXQuery.ToListAsync();
            }

            return Ok(CXEntries);
        }
        /// <summary>
        /// Retrieves prices for a single exchange ticker
        /// </summary>
        /// <param name="ExchangeTicker">An exchange ticker in the form XXX.YYY</param>
        /// <param name="include_buy_orders">If buy orders should be included</param>
        /// <param name="include_sell_orders">If sell orders should be included</param>
        /// <returns></returns>
        [HttpGet("prices/{ExchangeTicker}")]
        [AllowAnonymous]
        [Cache(Minutes = 5)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(CXEntry), "application/json")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "ExchangeTicker not found")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid ExchangeTicker provided", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetTickerPrice(string ExchangeTicker,
            [FromQuery(Name = "include_buy_orders")] bool include_buy_orders = false,
            [FromQuery(Name = "include_sell_orders")] bool include_sell_orders = false)
        {
            List<string> Errors = new();
            var ExchangeTickerSplit = ExchangeTicker.Split('.', StringSplitOptions.RemoveEmptyEntries);
            if (ExchangeTickerSplit.Length != 2)
            {
                Errors.Add($"{nameof(ExchangeTicker)} '{ExchangeTicker}' must be of the form XXX.YYY");
                return BadRequest(Errors);
            }

            var MaterialTicker = ExchangeTickerSplit[0];
            var ExchangeCode = ExchangeTickerSplit[1];

            ValidationExtensions.RunValidationsAsIfTypeAndProperty(
                MaterialTicker, 
                typeof(CXEntry), 
                nameof(CXEntry.MaterialTicker), 
                ref Errors, 
                "ExchangeTicker-Left");

            ValidationExtensions.RunValidationsAsIfTypeAndProperty(
                ExchangeCode,
                typeof(CXEntry),
                nameof(CXEntry.ExchangeCode),
                ref Errors,
                "ExchangeTicker-Right");

            if (Errors.Any())
            {
                return BadRequest(Errors);
            }

            CXEntry? Entry;
            using (var reader = DBAccess.GetReader())
            {
                var CXEntryQuery = reader.DB.CXEntries.AsQueryable();

                if (include_buy_orders)
                {
                    CXEntryQuery = CXEntryQuery.Include(cx => cx.BuyOrders);
                }

                if (include_sell_orders)
                {
                    CXEntryQuery = CXEntryQuery.Include(cx => cx.SellOrders);
                }

                Entry = await CXEntryQuery
                    .Where(cx => cx.MaterialTicker == MaterialTicker && cx.ExchangeCode == ExchangeCode)
                    .FirstOrDefaultAsync();
            }

            if (Entry != null)
            {
                return Ok(Entry);
            }

            return NoContent();
        }

        /// <summary>
        /// PUTs MESG_COMEX_BROKER_PRICES payload
        /// </summary>
        /// <param name="cxpcPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("cxpc")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutCXPC([FromBody] Payloads.CX.MESG_COMEX_BROKER_PRICES cxpcPayload)
        {
            List<string> Errors = new();
            if (!cxpcPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = cxpcPayload.payload.message.payload;

            CXEntry? cxEntry;
            using (var reader = DBAccess.GetReader())
            {
                cxEntry = await reader.DB.CXEntries.FirstOrDefaultAsync(cx => cx.CXEntryId == data.brokerId);
            }

            if (cxEntry != null)
            {
                var UserName = this.GetUserName()!;

                CXPC cxpc = new();
                cxpc.CXPCId = data.brokerId;
                cxpc.MaterialTicker = cxEntry.MaterialTicker;
                cxpc.ExchangeCode = cxEntry.ExchangeCode;
                cxpc.UserNameSubmitted = UserName;
                cxpc.Timestamp = DateTime.UtcNow;

                long fromTimestamp = data.from.timestamp;

                foreach (var interval in data.prices)
                {
                    foreach (var price in interval.prices)
                    {
                        CXPCEntry cxpcEntry = new();
                        cxpcEntry.CXPCEntryId = $"{cxpc.CXPCId}-{interval.interval}-{price.date}";
                        cxpcEntry.Interval = interval.interval;
                        cxpcEntry.DateEpochMs = price.date;
                        cxpcEntry.Open = price.open;
                        cxpcEntry.Close = price.close;
                        cxpcEntry.High = price.high;
                        cxpcEntry.Low = price.low;
                        cxpcEntry.Volume = price.volume;
                        cxpcEntry.Traded = price.traded;
                        cxpcEntry.CXPCId = cxpc.CXPCId;
                        cxpcEntry.CXPC = cxpc;

                        cxpc.Entries.Add(cxpcEntry);
                    }
                }

                if (!cxpc.Validate(ref Errors))
                {
                    return BadRequest(Errors);
                }

                using (var writer = DBAccess.GetWriter())
                {
                    var existingCXPC = await writer.DB.CXPCs
                        .Include(cxpc => cxpc.Entries)
                        .FirstOrDefaultAsync(c => c.CXPCId == cxpc.CXPCId);
                    if (existingCXPC != null)
                    {
                        existingCXPC.UserNameSubmitted = cxpc.UserNameSubmitted;
                        existingCXPC.Timestamp = cxpc.Timestamp;

                        existingCXPC.Entries = existingCXPC.Entries
                            .Where(cxpce => cxpce.DateEpochMs <= fromTimestamp) // Keep older
                            .Union(cxpc.Entries)                                // Add newer (updated)
                            .ToList();
                    }
                    else
                    {
                        await writer.DB.CXPCs.AddAsync(cxpc);
                    }

                    await writer.DB.SaveChangesAsync();
                }
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves CXPC data
        /// </summary>
        /// <param name="ticker">[Optional] Ticker parameters in the form XXX.YYY </param>
        /// <param name="fromTime">[Optional] The lower end time of data to retrieve in epoch milliseconds</param>
        /// <param name="toTime">[Optional] The upper end time of data to retrieve in epoch milliseconds</param>
        /// <param name="interval">[Optional] Intervals to include in the response </param>
        /// <returns>A list of CXPC data</returns>
        [HttpGet("cxpc")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<CXPC>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid ticker", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetCXPC(
            [FromQuery(Name = "ticker")] List<string>? ticker = null,
            [FromQuery(Name = "from_time")] long? fromTime = null,
            [FromQuery(Name = "to_time")] long? toTime = null,
            [FromQuery(Name = "interval")] List<string>? interval = null)
        {
            List<Tuple<string, string>> matCodeTickers = new();
            if (ticker != null && ticker.Count > 0)
            {
                ticker = ticker
                    .Select(t => t.ToUpper())
                    .ToList();

                List<string> Errors = new();
                foreach (var t in ticker)
                {
                    var split = t.Split('.', StringSplitOptions.RemoveEmptyEntries);
                    if (split.Length != 2)
                    {
                        Errors.Add($"{nameof(ticker)} '{ticker}' must be of the form XXX.YYY");
                    }
                    else
                    {
                        matCodeTickers.Add(new Tuple<string, string>(split[0], split[1]));
                    }
                }
            }

            List<CXPC> cxpcs;
            using (var reader = DBAccess.GetReader())
            {
                var query = reader.DB.CXPCs
                    .Include(cxpc => cxpc.Entries)
                    .OrderBy(cxpc => cxpc.MaterialTicker)
                    .AsQueryable();

                if (matCodeTickers.Count > 0)
                {
                    var p = Expression.Parameter(typeof(CXPC), null);
                    var parts = new List<Expression>();
                    foreach (var matCodeTicker in matCodeTickers)
                    {
                        // This is a little confusing, but it's equivalent to:
                        // (Row.ticker == ticker && Row.exchangeCode == exchangeCode) || ...
                        // ^ Which you can't do with standard ef core linq (well, at least not server-side)
                        var matValue = Expression.Property(p, nameof(CXPC.MaterialTicker));
                        var matTarget = Expression.Constant(matCodeTicker.Item1);
                        var matTest = Expression.Equal(matValue, matTarget);

                        var codeValue = Expression.Property(p, nameof(CXPC.ExchangeCode));
                        var codeTarget = Expression.Constant(matCodeTicker.Item2);
                        var codeTest = Expression.Equal(codeValue, codeTarget);

                        var part = Expression.AndAlso(matTest, codeTest);
                        parts.Add(part);
                    }

                    var body = parts.Aggregate(Expression.OrElse);
                    var filter = Expression.Lambda<Func<CXPC, bool>>(body, p);

                    query = query.Where(filter);
                }

                cxpcs = await query.ToListAsync();
            }

            foreach (var cxpc in cxpcs)
            {
                var query = cxpc.Entries.AsQueryable();
                if (fromTime != null)
                {
                    query = query.Where(e => e.DateEpochMs >= fromTime);
                }
                if (toTime != null)
                {
                    query = query.Where(e => e.DateEpochMs <= toTime);
                }

                if (interval != null)
                {
                    query = query.Where(e => interval.Contains(e.Interval));
                }

                cxpc.Entries = query.OrderBy(e => e.Interval, new CXPCIntervalComparer())
                            .ThenBy(e => e.DateEpochMs)
                        .ToList();
            }

            return Ok(cxpcs);
        }

        /// <summary>
        /// PUTs a MESG_COMEX_TRADER_ORDER payload
        /// </summary>
        /// <param name="ordersPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("cxos")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutCXOS([FromBody] Payloads.CX.MESG_COMEX_TRADER_ORDER ordersPayload)
        {
            List<string> Errors = new();
            if (!ordersPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = ordersPayload.payload.message.payload;

            List<CXOS> Orders = new();
            foreach (var order in data.orders)
            {
                var CXOSOrder = new CXOS()
                {
                    CXOSId = order.id,
                    ExchangeCode = order.exchange.code!,
                    BrokerId = order.brokerId,
                    OrderType = order.type,
                    MaterialTicker = order.material.ticker,
                    Amount = order.amount,
                    InitialAmount = order.initialAmount,
                    LimitAmount = order.limit.amount,
                    LimitCurrency = order.limit.currency,
                    Status = order.status,
                    Created = order.created.timestamp.FromUnixTime(),
                    UserNameSubmitted = CurrentUser,
                    Timestamp = DateTime.UtcNow
                };
                
                foreach (var trade in order.trades)
                {
                    CXOSOrder.Trades.Add(new CXOSTrade()
                    {
                        CXOSTradeId = trade.id,
                        Amount = trade.amount,
                        PriceAmount = trade.price.amount,
                        PriceCurrency = trade.price.currency,
                        Time = trade.time.timestamp.FromUnixTime(),
                        PartnerId = trade.partner.id,
                        PartnerCode = trade.partner.code,
                        PartnerName = trade.partner.name,
                        CXOSId = CXOSOrder.CXOSId,
                        CXOS = CXOSOrder
                    });
                }

                Orders.Add(CXOSOrder);
            }

            if (!Orders.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.CXOSs
                    .UpsertRange(Orders)
                    .On(o => new { o.CXOSId })
                    .RunAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves CXOS data for the provided UserName(s)
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group">The group</param>
        /// <param name="include_trades">If trade should be included</param>
        /// <returns></returns>
        [HttpGet("cxos")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<CXOS>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetCXOS(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_trades")] bool include_trades = false)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<Model.CXOS>> UserToCXOS;
            using (var reader = DBAccess.GetReader())
            {
                var UserToCXOSQuery = reader.DB.CXOSs.AsQueryable();
                if (include_trades)
                {
                    UserToCXOSQuery = UserToCXOSQuery.Include(c => c.Trades);
                }

                UserToCXOS = await UserToCXOSQuery
                    .Where(s => UserToPerm.Keys.Contains(s.UserNameSubmitted))
                    .GroupBy(s => s.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToCXOS, UserToPerm);
            return Ok(Response);
        }
    }
}
