﻿using FIOAPI.Payloads.Auth;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// Auth endpoint
    /// </summary>
    [ApiController]
    [Route("/auth")]
    public class AuthController : ControllerBase
    {
        private readonly JwtSettings _JwtSettings;

        /// <summary>
        /// The maximum number of API Keys a user can have
        /// </summary>
        public const int MaxAPIKeyCount = 20;
        
        /// <summary>
        /// AuthController constructor
        /// </summary>
        /// <param name="jwtSettings">jwtSettings</param>
        /// <exception cref="ArgumentNullException">if jwtSettings is null</exception>
        public AuthController(JwtSettings jwtSettings)
        {
            _JwtSettings = jwtSettings ?? throw new ArgumentNullException(nameof(jwtSettings));
        }

        /// <summary>
        /// Checks to see if you are logged in
        /// </summary>
        /// <returns>OK if logged in, Unauthorized otherwise</returns>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "You are logged in", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "You are not logged in", typeof(string), "text/plain")]
        public IActionResult Get()
        {
            return this.IsLoggedIn() ? Ok("You are logged in.") : Unauthorized("You are not logged in.");
        }

        /// <summary>
        /// Logs the user in providing a Jwt bearer token
        /// </summary>
        /// <param name="payload">The login payload</param>
        /// <returns>The Jwt bearer token</returns>
        /// <remarks>
        /// <para>The Token result should be used in future requests by adding a header which is:</para>
        /// <br/><br/>
        /// <code>Authorization: Bearer {Token}</code>
        /// </remarks>
        [HttpPost("login")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully logged in", typeof(LoginResponse), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid username and/or password")]
        public async Task<IActionResult> LoginAsync([FromBody] Login payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            bool isValidUser = await UserService.IsValidAsync(payload.UserName, payload.Password);
            if (!isValidUser)
            {
                return Unauthorized();
            }

            bool isAdmin = UserService.GetUser(payload.UserName)!.IsAdmin;

            var claims = UserService.GetUserClaims(payload.UserName);
            var key = new SymmetricSecurityKey(_JwtSettings.Key);
            var signingCreds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                issuer: _JwtSettings.Issuer,
                audience: _JwtSettings.Audience,
                claims: claims,
                expires: DateTime.Now.AddDays(1),
                signingCredentials: signingCreds
                );

            var response = new LoginResponse()
            {
                Token = new JwtSecurityTokenHandler().WriteToken(token),
                IsAdministrator = isAdmin
            };

            response.RunValidation_Throw();
            return Ok(response);
        }

        /// <summary>
        /// Creates an API Key
        /// </summary>
        /// <param name="payload">The CreateAPIKey payload</param>
        /// <returns>The APIKey</returns>
        /// <remarks>
        /// The APIKey result should be used in future requests by adding a header which is:<br/><br/>
        /// <code>Authorization: FIOAPIKey {APIKey}</code>
        /// </remarks>
        [HttpPost("createapikey")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully created APIKey", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid user/pass combination")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status406NotAcceptable, "Reached maximum threshold of APIKeys", typeof(string), "text/plain")]
        public async Task<IActionResult> CreateAPIKeyAsync([FromBody] CreateAPIKey payload) 
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            payload.UserName = payload.UserName.ToLowerInvariant();

            bool isValidUser = await UserService.IsValidAsync(payload.UserName, payload.Password);
            if (!isValidUser)
            {
                return Unauthorized();
            }

            using (var writer = DBAccess.GetWriter())
            {
                var user = UserService.GetUser(payload.UserName, writer.DB);
                if (user == null)
                {
                    return Unauthorized();
                }

                var apiKeyCount = await writer.DB.APIKeys.Where(ak => ak.UserName == payload.UserName).CountAsync();
                if (apiKeyCount >= MaxAPIKeyCount)
                {
                    return StatusCode(406, $"Reached maximum threshold of {MaxAPIKeyCount} API Keys.  Cannot create more.");
                }

                var ApiKey = new DB.Model.APIKey
                {
                    Key = Guid.NewGuid(),
                    UserName = payload.UserName,
                    Application = payload.ApplicationName,
                    AllowWrites = payload.AllowWrites,
                    CreateTime = DateTime.UtcNow
                };
                ApiKey.RunValidation_Throw();

                writer.DB.APIKeys.Add(ApiKey);
                await writer.DB.SaveChangesAsync();

                return Ok(new CreateAPIKeyResponse { APIKey = ApiKey.Key });
            }
        }

        /// <summary>
        /// Revokes an API Key
        /// </summary>
        /// <param name="payload">The RevokeAPIKey payload</param>
        /// <remarks>Revoking an API Key requires using a JWT or writable APIKey (actively logged in)</remarks>
        /// <returns>OK on success</returns>
        [HttpPost("revokeapikey")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully revoked API Key")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "APIKey not found")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid user/pass combination")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        public async Task<IActionResult> RevokeAPIKeyAsync([FromBody] RevokeAPIKey payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            payload.UserName = payload.UserName.ToLowerInvariant();
            bool isValidUser = await UserService.IsValidAsync(payload.UserName, payload.Password);
            if (!isValidUser)
            {
                return Unauthorized();
            }

            using (var writer = DBAccess.GetWriter())
            {
                var user = UserService.GetUser(payload.UserName, writer.DB);
                if (user == null)
                {
                    return Unauthorized();
                }

                var apiKey = await writer.DB.APIKeys.FirstOrDefaultAsync(ak => ak.UserName == payload.UserName && ak.Key == payload.APIKeyToRevoke);
                if (apiKey != null)
                {
                    writer.DB.APIKeys.Remove(apiKey);
                    Caches.APIKeyCache.Remove(apiKey.Key);

                    await writer.DB.SaveChangesAsync();
                    return Ok();
                }
                else
                {
                    return NoContent();
                }
            }
        }

        /// <summary>
        /// Retrieves all APIKeys for the logged in user
        /// </summary>
        /// <returns>A list of API Keys</returns>
        /// <remarks>Note that this requires either a JWT or a writable API key</remarks>
        [HttpGet("listapikeys")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Retrieved API Keys", typeof(List<Model.APIKey>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in or using a read-only APIKey")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        public async Task<IActionResult> ListAPIKeysAsync()
        {
            using (var reader = DBAccess.GetReader())
            {
                var apiKeys = await reader.DB.APIKeys
                    .Where(ak => ak.UserName == this.GetUserName())
                    .ToListAsync();

                return Ok(apiKeys);
            }
        }

        /// <summary>
        /// Changes an account password
        /// </summary>
        /// <param name="payload">The ChangePassword payload</param>
        /// <returns>OK on success</returns>
        /// <remarks>Changing your password requires using a JWT or writable APIKey (actively logged in)</remarks>
        [HttpPost("changepassword")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully changed password", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Invalid password or not logged in", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        public async Task<IActionResult> ChangePasswordAsync([FromBody] ChangePassword payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            using (var writer = DBAccess.GetWriter())
            {
                var user = await writer.DB.Users
                    .FirstOrDefaultAsync(u => u.UserName == this.GetUserName());
                if (user == null)
                {
                    return Unauthorized("Username not found.");
                }

                if (!SecurePasswordHasher.Verify(payload.OldPassword, user.PasswordHash))
                {
                    return Unauthorized("Invalid password.");
                }

                user.PasswordHash = SecurePasswordHasher.Hash(payload.NewPassword);
                await writer.DB.SaveChangesAsync();
            }

            return Ok("Password changed");
        }

        /// <summary>
        /// Runs AutoRegistration (leveraged by extension)
        /// </summary>
        /// <param name="payload">AutoRequestRegister payload</param>
        /// <returns>OK on success</returns>
        /// <remarks>
        /// This is only used by the extension
        /// </remarks>
        [HttpPost("autorequestregister")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully received AutoRequest", typeof(AutoRequestRegisterResponse), "application/json")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "Already a user")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        public async Task<IActionResult> AutoRequestRegisterAsync([FromBody] AutoRequestRegister payload)
        {
            List<string> Errors = new();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            var caseSensitiveUserName = payload.payload.username;
            var userName = caseSensitiveUserName.ToLowerInvariant();
            using(var writer = DBAccess.GetWriter())
            {
                // Check for an existing user first
                var existingUser = await writer.DB.Users.FirstOrDefaultAsync(u => u.UserName == userName);
                if (existingUser == null)
                {
                    bool bModifiedDatabase = false;

                    // Check for an existing registration
                    var registration = await writer.DB.Registrations.FirstOrDefaultAsync(r => r.UserName == userName);
                    if (registration != null) 
                    {
                        // Delete it if it's older than 30 minutes
                        DateTime thirtyMinutesAgo = DateTime.UtcNow.AddMinutes(-30.0);
                        if (registration.RegistrationTime < thirtyMinutesAgo)
                        {
                            writer.DB.Registrations.Remove(registration);
                            bModifiedDatabase = true;
                            registration = null;
                        }
                    }

                    if (registration == null)
                    {
                        registration = new()
                        {
                            UserName = userName,
                            DisplayUserName = caseSensitiveUserName,
                            RegistrationGuid = Guid.NewGuid(),
                            RegistrationTime = DateTime.UtcNow
                        };

                        registration.RunValidation_Throw();
                        writer.DB.Registrations.Add(registration);
                        bModifiedDatabase = true;
                    }

                    if (bModifiedDatabase)
                    {
                        await writer.DB.SaveChangesAsync();
                    }

                    var response = new AutoRequestRegisterResponse()
                    {
                        UserName = caseSensitiveUserName,
                        RegistrationGuid = registration.RegistrationGuid,
                        RegistrationTime = registration.RegistrationTime,
                    };

                    response.RunValidation_Throw();
                    return Ok(response);
                }
            }

            return NoContent();
        }

        /// <summary>
        /// Creates an account given a registration (leveraged by extension/FIOWeb)
        /// </summary>
        /// <param name="payload">Register payload</param>
        /// <returns>OK on success</returns>
        /// <remarks>
        /// This is only used by the extension and FIOWeb
        /// </remarks>
        [HttpPost("register")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully registered and created account")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "No registration found")]
        public async Task<IActionResult> RegisterAsync([FromBody] Register payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            var userName = payload.UserName.ToLowerInvariant();

            using (var writer = DBAccess.GetWriter())
            {
                var registration = await writer.DB.Registrations.FirstOrDefaultAsync(r => r.UserName == userName && r.RegistrationGuid == payload.RegistrationGuid);
                if (registration != null)
                {
                    var userModel = new Model.User()
                    {
                        UserName = registration.UserName,
                        DisplayUserName = registration.DisplayUserName,
                        PasswordHash = SecurePasswordHasher.Hash(payload.Password),
                        IsAdmin = false
                    };
                    userModel.RunValidation_Throw();
                    writer.DB.Users.Add(userModel);
                    writer.DB.Registrations.Remove(registration);
                    await writer.DB.SaveChangesAsync();
                    return Ok();
                }
            }
            
            return Unauthorized();
        }

        /// <summary>
        /// Clears/resets game data
        /// </summary>
        /// <param name="company">if company data should be deleted</param>
        /// <param name="contracts">if contracts data should be deleted</param>
        /// <param name="cxos">if cxos data should be deleted</param>
        /// <param name="experts">if experts data should be deleted</param>
        /// <param name="flights">if flights data should be deleted</param>
        /// <param name="production_lines">if production_lines data should be deleted</param>
        /// <param name="ships">if ships data should be deleted</param>
        /// <param name="sites">if sites data should be deleted</param>
        /// <param name="storages">if storages data should be deleted</param>
        /// <param name="workforces">if workforces data should be deleted</param>
        /// <returns></returns>
        [HttpDelete("resetgamedata")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully cleared game data")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        public async Task<IActionResult> ClearGameData(
            [FromQuery(Name = "company")] bool company = true,
            [FromQuery(Name = "contracts")] bool contracts = true,
            [FromQuery(Name = "cxos")] bool cxos = true,
            [FromQuery(Name = "experts")] bool experts = true,
            [FromQuery(Name = "flights")] bool flights = true,
            [FromQuery(Name = "production_lines")] bool production_lines = true,
            [FromQuery(Name = "ships")] bool ships = true,
            [FromQuery(Name = "sites")] bool sites = true,
            [FromQuery(Name = "storages")] bool storages = true,
            [FromQuery(Name = "workforces")] bool workforces = true)
        {
            var username = this.GetUserName()!;
            await ClearUserDataHelper.ResetGameplayData(UserName: username,
                                                        Company: company, Contracts: contracts, CXOS: cxos,
                                                        Experts: experts, Flights: flights, ProductionLines: production_lines,
                                                        Ships: ships, Sites: sites, Storages: storages, Workforces: workforces);
            return Ok();
        }

        /// <summary>
        /// Deletes your account (requires you be auth'd when deleting)
        /// </summary>
        /// <param name="payload">DeleteAccount payload</param>
        /// <returns>OK on success</returns>
        [HttpPost("deleteaccount")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully deleted account")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Password incorrect")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        public async Task<IActionResult> DeleteAccountAsync([FromBody] DeleteAccount payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            var thisUserName = this.GetUserName()!;

            using (var writer = DBAccess.GetWriter())
            {
                var user = await writer.DB.Users.FirstAsync(u => u.UserName == thisUserName);
                if (!SecurePasswordHasher.Verify(payload.Password, user.PasswordHash))
                {
                    return Unauthorized("Incorrect password");
                }
            }

            await ClearUserDataHelper.DeleteUser(thisUserName);

            return Ok();
        }

        /// <summary>
        /// Gets the discord name for the user
        /// </summary>
        /// <returns>Ok (with discord name string) on success</returns>
        [HttpGet("discord")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully retrieved discord name")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        public async Task<IActionResult> GetDiscord()
        {
            var thisUserName = this.GetUserName()!;

            string? DiscordName;
            using (var reader = DBAccess.GetReader())
            {
                DiscordName = await reader.DB.Users
                    .Where(u => u.UserName == thisUserName)
                    .Select(u => u.DiscordName)
                    .FirstOrDefaultAsync();
            }

            return Ok(DiscordName);
        }

        /// <summary>
        /// Sets the user's discord name (id)
        /// </summary>
        /// <param name="payload">The SetDiscord payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("discord")]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully set discord name")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only APIKey")]
        public async Task<IActionResult> PutDiscord([FromBody] SetDiscord payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            var thisUserName = this.GetUserName()!;

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Users
                    .Where(u => u.UserName == thisUserName)
                    .ExecuteUpdateAsync(b =>
                        b.SetProperty(u => u.DiscordName, payload.DiscordName));
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves a list of all FIO user naems
        /// </summary>
        /// <returns>List of all FIO usernames</returns>
        [HttpGet("users")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetUsers()
        {
            List<string> UserNames;
            using (var reader = DBAccess.GetReader())
            {
                UserNames = await reader.DB.Users
                    .Select(u => u.DisplayUserName)
                    .ToListAsync();
            }

            return Ok(UserNames);
        }
    }
}
