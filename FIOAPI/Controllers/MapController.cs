﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// MapController
    /// </summary>
    [ApiController]
    [Route("/map")]
    public class MapController : ControllerBase
    {
        /// <summary>
        /// PUTs the SYSTEM_STARS payload
        /// </summary>
        /// <param name="SystemStarsData">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("systemstars")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key or not an admin")]
        public async Task<IActionResult> PutSystemStars([FromBody] Payloads.Map.SYSTEM_STARS SystemStarsData)
        {
            List<string> Errors = new();
            if (!SystemStarsData.Validate(ref Errors))
            {
              return BadRequest(Errors);
            }

            List<Model.System> AllSystems = new();

            var data = SystemStarsData.payload.message.payload;
            foreach (var star in data.stars)
            {
                Model.System System = new()
                {
                    SystemId = star.systemId,
                    Name = star.name,
                    NaturalId = star.address.lines.First().entity!.naturalId,
                    Type = star.type,
                    SectorId = star.sectorId,
                    SubSectorId = star.subSectorId,
                    PositionX = star.position.x,
                    PositionY = star.position.y,
                    PositionZ = star.position.z,
                    Connections = star.connections.ToList(),
                    PlanetIds = new List<string>(),
                    CelestialBodyIds = new List<string>(),
                    UserNameSubmitted = this.GetUserName()!,
                    Timestamp = DateTime.UtcNow
                };

                AllSystems.Add(System);
            }

            AllSystems.ForEach(s =>
            {
                s.Validate(ref Errors);
            });

            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Systems
                    .UpsertRange(AllSystems)
                    .On(s => new { s.SystemId })
                    .NoUpdate()
                    .RunAsync();

                AllSystems = await writer.DB.Systems.ToListAsync();
            }

            _ = Task.Run(() =>
            {
                EntityCaches.SystemCache.Initialize(AllSystems);
                JumpCalculator.Initialize();
            });

            return Ok();
        }

        /// <summary>
        /// PUTs the MAP_WORLD_SECTORS payload
        /// </summary>
        /// <param name="SectorsData">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("sectors")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key or not an admin")]
        public async Task<IActionResult> PutSectors([FromBody] Payloads.Map.MAP_WORLD_SECTORS SectorsData)
        {
            List<string> Errors = new();
            if (!SectorsData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            List<Sector> AllSectors = new();

            var data = SectorsData.payload.message.payload;
            foreach (var sector in data.sectors)
            {
                Sector Sector = new();
                Sector.SectorId = sector.id;
                Sector.Name = sector.name;
                Sector.HexQ = sector.hex.q;
                Sector.HexR = sector.hex.r;
                Sector.HexS = sector.hex.s;
                Sector.Size = sector.size;
                Sector.UserNameSubmitted = this.GetUserName()!;
                Sector.Timestamp = DateTime.UtcNow;
                
                foreach (var subsector in sector.subsectors)
                {
                    SubSector SubSector = new();
                    SubSector.SubSectorId = subsector.id;
                    SubSector.Vertex0_X = subsector.vertices[0].x;
                    SubSector.Vertex0_Y = subsector.vertices[0].y;
                    SubSector.Vertex0_Z = subsector.vertices[0].z;

                    SubSector.Vertex1_X = subsector.vertices[1].x;
                    SubSector.Vertex1_Y = subsector.vertices[1].y;
                    SubSector.Vertex1_Z = subsector.vertices[1].z;

                    SubSector.Vertex2_X = subsector.vertices[2].x;
                    SubSector.Vertex2_Y = subsector.vertices[2].y;
                    SubSector.Vertex2_Z = subsector.vertices[2].z;

                    SubSector.SectorId = Sector.SectorId;
                    SubSector.Sector = Sector;

                    Sector.SubSectors.Add(SubSector);
                }

                AllSectors.Add(Sector);
            }

            AllSectors.ForEach(s =>
            {
                s.Validate(ref Errors);
            });

            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            var AllSubSectors = AllSectors
                .SelectMany(s => s.SubSectors)
                .ToList();

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Sectors
                    .UpsertRange(AllSectors)
                    .On(s => new { s.SectorId })
                    .RunAsync();

                await writer.DB.SubSectors
                    .UpsertRange(AllSubSectors)
                    .On(ss => new { ss.SubSectorId })
                    .RunAsync();
            }

            return Ok();
        }

        /// <summary>
        /// PUTs the MAP_SYSTEM payload
        /// </summary>
        /// <param name="SystemData">SystemData</param>
        /// <returns>Ok on success</returns>
        [HttpPut("system")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutSystem([FromBody] Payloads.Map.MAP_SYSTEM SystemData)
        {
            List<string> Errors = new();
            if (!SystemData.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = SystemData.payload.message.payload.body;

            var System = new Model.System()
            {
                SystemId = data.id,
                Name = data.name,
                Nameable = data.nameable,
                NamerId = data.namer?.id,
                NamerUserName = data.namer?.username,
                NaturalId = data.naturalId,
                Type = data.star.type,
                SectorId = data.star.sectorId,
                SubSectorId = data.star.subSectorId,
                PositionX = data.star.position.x,
                PositionY = data.star.position.y,
                PositionZ = data.star.position.z,
                MeteroidDensity = data.meteoroidDensity,
                Connections = data.connections.ToList(),
                PlanetIds = data.planets.Select(p => p.id).ToList(),
                CelestialBodyIds = data.celestialBodies.Select(c => c.id).ToList(),
                UserNameSubmitted = this.GetUserName()!,
                Timestamp = DateTime.UtcNow,
            };

            if (!System.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var Star = new Model.Star()
            {
                StarId = data.id,
                SystemId = data.id,
                SystemNaturalId = data.naturalId,
                SystemName = data.name,
                SectorId = data.star.sectorId,
                SubSectorId = data.star.subSectorId,
                Type = data.star.type,
                Luminosity = data.star.luminosity,
                Mass = data.star.mass,
                MassSol = data.star.massSol,
                UserNameSubmitted = this.GetUserName()!,
                Timestamp = DateTime.UtcNow
            };

            if (!Star.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Systems
                    .Upsert(System)
                    .On(s => new { s.SystemId })
                    .RunAsync();

                await writer.DB.Stars
                    .Upsert(Star)
                    .On(s => new { s.StarId })
                    .RunAsync();
            }

            _ = Caches.EntityCaches.SystemCache.UpdateAsync(System);

            return Ok();
        }

        /// <summary>
        /// Gets sectors by search
        /// </summary>
        /// <param name="SearchIdentifier">SectorId or Name (can be partial name)</param>
        /// <param name="include_subsectors">Whether the payload should include subsectors</param>
        /// <returns>Sectors</returns>
        [HttpGet("sectors/{SearchIdentifier}")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Sector>), "application/json")]
        public async Task<IActionResult> GetSectorSearch(
            string SearchIdentifier,
            [FromQuery(Name = "include_subsectors")] bool include_subsectors = false)
        {
            List<Model.Sector> Sectors = new();

            using(var reader = DBAccess.GetReader())
            {
                var SectorsQuery = reader.DB.Sectors.AsQueryable();

                if (!String.IsNullOrWhiteSpace(SearchIdentifier))
                {
                    SearchIdentifier = SearchIdentifier.Trim().ToUpper();
                    SectorsQuery = SectorsQuery.Where(s => s.SectorId.ToUpper().Contains(SearchIdentifier) || (s.Name != null && s.Name.ToUpper().Contains(SearchIdentifier)));
                }

                if (include_subsectors)
                {
                    SectorsQuery = SectorsQuery.Include(s => s.SubSectors);
                }

                Sectors = await SectorsQuery.ToListAsync();
            }

            return Ok(Sectors);
        }

        /// <summary>
        /// Get all sectors
        /// </summary>
        /// <param name="include_subsectors">Whether the payload should include subsectors</param>
        /// <returns>Sectors</returns>
        [HttpGet("sectors")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Sector>), "application/json")]
        public async Task<IActionResult> GetSectors(
            [FromQuery(Name = "include_subsectors")] bool include_subsectors = false)
        {
            return await GetSectorSearch("", include_subsectors);
        }

        /// <summary>
        /// Gets all systems with a search 
        /// </summary>
        /// <param name="SearchIdentifier">SystemId, Name, or NaturalId (can be partial)</param>
        /// <returns></returns>
        [HttpGet("systems/{SearchIdentifier}")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.System>), "application/json")]
        public async Task<IActionResult> GetSystemsSearch(string SearchIdentifier)
        {
            List<Model.System> Systems = new();

            using (var reader = DBAccess.GetReader())
            {
                var SystemsQuery = reader.DB.Systems.AsQueryable();

                if (!String.IsNullOrWhiteSpace(SearchIdentifier))
                {
                    SearchIdentifier = SearchIdentifier.Trim().ToUpper();
                    SystemsQuery = SystemsQuery.Where(s =>
                        s.SystemId.ToUpper().Contains(SearchIdentifier) ||
                        (s.Name != null && s.Name.ToUpper().Contains(SearchIdentifier)) ||
                        s.NaturalId.ToUpper().Contains(SearchIdentifier));
                }

                Systems = await SystemsQuery.ToListAsync();
            }

            return Ok(Systems);
        }

        /// <summary>
        /// Gets all systems
        /// </summary>
        /// <returns>All systems</returns>
        [HttpGet("systems")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Model.System>), "application/json")]
        public async Task<IActionResult> GetSystems()
        {
            return await GetSystemsSearch("");
        }

        /// <summary>
        /// Get all stars that match the provided SearchIdentifier
        /// </summary>
        /// <param name="SearchIdentifier"></param>
        /// <returns>Stars</returns>
        [HttpGet("stars/{SearchIdentifier}")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Star>), "application/json")]
        public async Task<IActionResult> GetStarsSearch(string SearchIdentifier)
        {
            List<Model.Star> Stars = new();

            using (var reader = DBAccess.GetReader())
            {
                var StarsQuery = reader.DB.Stars.AsQueryable();

                if (!String.IsNullOrWhiteSpace(SearchIdentifier))
                {
                    SearchIdentifier = SearchIdentifier.Trim().ToUpper();
                    StarsQuery = StarsQuery.Where(s =>
                        s.StarId.ToUpper().Contains(SearchIdentifier) ||
                        s.SystemId.ToUpper().Contains(SearchIdentifier) ||
                        s.SystemNaturalId.ToUpper().Contains(SearchIdentifier) ||
                        (s.SystemName != null && s.SystemName.ToUpper().Contains(SearchIdentifier)));
                }

                Stars = await StarsQuery.ToListAsync();
            }

            return Ok(Stars);
        }

        /// <summary>
        /// Get all stars
        /// </summary>
        /// <returns>All stars</returns>
        [HttpGet("stars")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<Star>), "application/json")]
        public async Task<IActionResult> GetStars()
        {
            return await GetStarsSearch("");
        }

        /// <summary>
        /// Retrieves a jump route from a provided source and destination
        /// </summary>
        /// <param name="Source">Source</param>
        /// <param name="Destination">Destination</param>
        /// <returns></returns>
        [HttpGet("jumproute/{Source}/{Destination}")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<JumpCacheRouteJump>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed to resolve source/destination", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetJumpRoute(string Source, string Destination)
        {
            var SourceAndDest = new List<string> { Source, Destination };
            if (!EntityCaches.ConvertToSystemIds(ref SourceAndDest, out var Errors))
            {
                return BadRequest(Errors);
            }

            return Ok(await JumpCalculator.GetRoute(SourceAndDest[0], SourceAndDest[1]));
        }

        /// <summary>
        /// Retrieves the number of jumps for a given source and destination
        /// </summary>
        /// <param name="Source">Source</param>
        /// <param name="Destination">Destination</param>
        /// <returns></returns>
        [HttpGet("jumpcount/{Source}/{Destination}")]
        [AllowAnonymous]
        [Cache(Days = 1)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(int), "application/text")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed to resolve source/destination", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetJumpCount(string Source, string Destination)
        {
            var SourceAndDest = new List<string> { Source, Destination };
            if (!EntityCaches.ConvertToSystemIds(ref SourceAndDest, out var Errors))
            {
                return BadRequest(Errors);
            }

            return Ok(await JumpCalculator.GetJumpCount(SourceAndDest[0], SourceAndDest[1]));
        }
    }
}
