﻿#if DEBUG
using System.Text.Json.Nodes;
using FIOAPI.ControllerFilters;
using FIOAPI.FIODebug;


namespace FIOAPI.Controllers
{
    /// <summary>
    /// Debug controller - This is only used in DEBUG and only with the FIOAPIDebugTamperMonkeyScript
    /// 1) Run the script in browser
    /// 2) Run FIOAPI in Debug
    /// 3) Refresh APEX and click around
    /// 4) Find {root}/FIOAPI/DebugPayloads/
    /// 5) Look at the various payloads
    /// 
    /// There are three flavors of payloads:
    /// - { payload: { message: { payload: { path   // Folders appear as: `PATH_{path[0]}-{path[1}-{path[2]}` (path[n] may be _ if it's an id)
    /// - { payload: { message: { messageType       // Folders appear as: `MESG_{messageType}`
    /// - { messageType                             // Folders appear as: `ROOT_{messageType}`
    /// </summary>
    [ApiController]
    [Route("/debug")]
    public class DebugController : ControllerBase
    {
        private static object IOLock = new object();
        private static string PayloadsPath = "DebugPayloads";
        private static JsonSerializerOptions SerializerOptions = new JsonSerializerOptions()
        {
            PropertyNamingPolicy = new ExactTypeNameCaseNamingPolicy(),
            WriteIndented = true
        };

        private const string HexPattern = @"[0-9a-f]{32}";
        private static Regex HexRegex = new Regex(HexPattern, RegexOptions.Compiled);

        /// <summary>
        /// Posts a payload which is saved and grouped
        /// </summary>
        /// <param name="payload">Payload</param>
        /// <returns>Ok</returns>
        [HttpPost("")]
        [AllowAnonymous]
        [HydrationTimeoutFilter]
        [SwaggerResponse(StatusCodes.Status200OK)]
        public async Task<IActionResult> PostDebug([FromBody] JsonObject payload)
        {
            var json = new FIOJsonObject();
            json.Load(payload);

            var folderHash = json.FolderHash;
            
            bool bSetFolderName = false;
            string FolderName = Path.Combine(PayloadsPath, folderHash);
            string FileName = $"{DateTime.Now:HH-mm-ss-ffffff}.json";
            if (payload.ContainsKey("payload") && payload["payload"] is JsonObject)
            {
                var outerPayloadJsonObj = payload["payload"]!.AsObject();
                if (outerPayloadJsonObj.ContainsKey("message") && outerPayloadJsonObj["message"] is JsonObject)
                {
                    var messageJsonObj = outerPayloadJsonObj["message"]!.AsObject();
                    if (messageJsonObj.ContainsKey("payload") && messageJsonObj["payload"] is JsonObject)
                    {
                        var innerPayloadJsonObj = messageJsonObj["payload"]!.AsObject();
                        if (innerPayloadJsonObj.ContainsKey("path") && innerPayloadJsonObj["path"] is JsonArray)
                        {
                            var pathJsonArray = innerPayloadJsonObj["path"]!.AsArray();

                            if (pathJsonArray.Count > 0)
                            {
                                var fileNameSb = new StringBuilder();
                                foreach (var pathJsonArrayEntry in pathJsonArray)
                                {
                                    var value = pathJsonArrayEntry!.GetValue<string>();
                                    if (!HexRegex.IsMatch(value))
                                    {
                                        fileNameSb.Append(value);
                                        fileNameSb.Append("-");
                                    }
                                    else
                                    {
                                        fileNameSb.Append("_-");
                                    }
                                }

                                if (fileNameSb.Length > 0)
                                {
                                    // Priority 1: _root->payload->message->payload->path
                                    fileNameSb.Remove(fileNameSb.Length - 1, 1);
                                    FolderName = Path.Combine(PayloadsPath, "PATH_" + fileNameSb.ToString());
                                    bSetFolderName = true;
                                }
                            }
                        }
                    }

                    if (!bSetFolderName && messageJsonObj.ContainsKey("messageType") && messageJsonObj["messageType"] is JsonValue)
                    {
                        // Priority 2: _root->payload->message->messageType
                        var messageType = messageJsonObj["messageType"]!.AsValue();
                        var messageValue = messageType!.GetValue<string>();
                        if (!string.IsNullOrWhiteSpace(messageValue))
                        {
                            FolderName = Path.Combine(PayloadsPath, $"MESG_{messageValue}");
                            bSetFolderName = true;
                        }
                    }
                }
            }

            // Priority 3: _root->messageType
            if (!bSetFolderName && payload.ContainsKey("messageType") && payload["messageType"] is JsonValue)
            {
                var messageType = payload["messageType"]!.AsValue();
                var messageValue = messageType!.GetValue<string>();
                if (!string.IsNullOrWhiteSpace(messageValue))
                {
                    FolderName = Path.Combine(PayloadsPath, $"ROOT_{messageValue}");
                    bSetFolderName = true;
                }
            }

            lock (IOLock)
            {
                if (!Directory.Exists(FolderName))
                {
                    Directory.CreateDirectory(FolderName);
                }

                System.IO.File.WriteAllText(Path.Combine(FolderName, FileName), payload.ToJsonString(SerializerOptions));
            }

            await Task.CompletedTask;
            return Ok();
        }

        /// <summary>
        /// Throws an exception to test InternalServerErrors
        /// </summary>
        /// <returns>An exception</returns>
        [HttpGet("exception")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status500InternalServerError)]
        public async Task<IActionResult> GetException()
        {
            await Task.CompletedTask;
            throw new InvalidOperationException("Test Exception");
        }
    }
}
#endif // DEBUG