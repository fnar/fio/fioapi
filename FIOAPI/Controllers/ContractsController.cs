﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using FIOAPI.Payloads.Contract;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// ContractController
    /// </summary>
    [ApiController]
    [Route("/contracts")]
    public class ContractsController : ControllerBase
    {
        private static Contract GenerateContractFromSingleContractPayload(MESG_CONTRACTS_CONTRACTS_CONTRACT contractPayload, string UserName)
        {
            Contract Contract = new();
            Contract.ContractId = contractPayload.id;
            Contract.Date = contractPayload.date.timestamp.FromUnixTime();
            Contract.Party = contractPayload.party;
            Contract.PartnerId = contractPayload.partner.countryId != null ? contractPayload.partner.countryId : contractPayload.partner.id!;
            Contract.PartnerCode = contractPayload.partner.countryCode != null ? contractPayload.partner.countryCode : contractPayload.partner.code;
            Contract.PartnerName = contractPayload.partner.name!;
            Contract.Status = contractPayload.status;
            Contract.ExtensionDeadline = contractPayload.extensionDeadline?.timestamp.FromUnixTime();
            Contract.CanExtend = contractPayload.canExtend;
            Contract.CanRequestTermination = contractPayload.canRequestTermination;
            Contract.DueDate = contractPayload.dueDate?.timestamp.FromUnixTime();
            Contract.Name = contractPayload.name;
            Contract.Preamble = contractPayload.preamble;
            Contract.TerminationSent = contractPayload.terminationSent;
            Contract.TerminationReceived = contractPayload.terminationReceived;
            Contract.AgentContract = contractPayload.agentContract;
            Contract.RelatedContracts = contractPayload.relatedContracts != null ? String.Join(",", contractPayload.relatedContracts) : "";
            Contract.ContractType = contractPayload.contractType;
            Contract.UserNameSubmitted = UserName;
            Contract.Timestamp = DateTime.UtcNow;

            foreach (var condition in contractPayload.conditions)
            {
                ContractCondition Condition = new();
                Condition.ContractConditionId = condition.id;
                Condition.Type = condition.type;
                Condition.Index = condition.index;
                Condition.Status = condition.status;
                Condition.Dependencies = String.Join(",", condition.dependencies);
                Condition.DeadlineDuration = condition.deadlineDuration?.millis;
                Condition.Deadline = condition.deadline?.timestamp.FromUnixTime();
                if (condition.quantity != null)
                {
                    Condition.QuantityMaterialTicker = condition.quantity.material.ticker;
                    Condition.QuantityAmount = condition.quantity.amount;
                }
                if (condition.address != null)
                {
                    Condition.AddressId = condition.address.lines.Last().entity!.id;
                    Condition.AddressNaturalId = condition.address.lines.Last().entity!.naturalId;
                    Condition.AddressName = condition.address.lines.Last().entity!.name;
                }
                Condition.BlockId = condition.blockId;
                if (condition.pickedUp != null)
                {
                    Condition.PickedUpMaterialTicker = condition.pickedUp.material.ticker;
                    Condition.PickedUpAmount = condition.pickedUp.amount;
                }
                Condition.Weight = condition.weight;
                Condition.Volume = condition.volume;
                if (condition.destination != null)
                {
                    Condition.DestinationAddressId = condition.destination.lines.Last().entity!.id;
                    Condition.DestinationAddressNaturalId = condition.destination.lines.Last().entity!.naturalId;
                    Condition.DestinationAddressName = condition.destination.lines.Last().entity!.name;
                }
                Condition.ShipmentItemId = condition.shipmentItemId;
                Condition.CountryId = condition.countryId;
                Condition.ReputationChange = condition.reputationChange;
                Condition.LoanInterestAmount = condition.interest?.amount;
                Condition.LoanInterestCurrency = condition.interest?.currency;
                Condition.LoanRepaymentAmount = condition.repayment?.amount;
                Condition.LoanRepaymentCurrency = condition.repayment?.currency;
                Condition.LoanTotalAmount = condition.total?.amount;
                Condition.LoanTotalCurrency = condition.total?.currency;
                Condition.ContractId = Contract.ContractId;
                Condition.Contract = Contract;

                Contract.Conditions.Add(Condition);
            }

            return Contract;
        }

        /// <summary>
        /// PUTs a MESG_CONTRACTS_CONTRACTS payload
        /// </summary>
        /// <param name="contractsPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutContracts([FromBody] Payloads.Contract.MESG_CONTRACTS_CONTRACTS contractsPayload)
        {
            List<string> Errors = new();
            if (!contractsPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            var data = contractsPayload.payload.message.payload;
            List<Contract> Contracts = new();
            foreach (var contract in data.contracts)
            {
                Contracts.Add(GenerateContractFromSingleContractPayload(contract, CurrentUser));
            }

            if (!Contracts.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            List<string> ContractIds = Contracts.Select(c => c.ContractId).ToList();

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Contracts
                    .Where(c => ContractIds.Contains(c.ContractId))
                    .DeleteAsync();

                await writer.DB.Contracts.AddRangeAsync(Contracts);

                await writer.DB.SaveChangesAsync();
            }

            await Task.CompletedTask;
            return Ok();
        }

        /// <summary>
        /// PUTs a MESG_CONTRACTS_CONTRACT payload
        /// </summary>
        /// <param name="contractPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("contract")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutContract([FromBody] Payloads.Contract.MESG_CONTRACTS_CONTRACT contractPayload)
        {
            List<string> Errors = new();
            if (!contractPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;
            
            var data = contractPayload.payload.message.payload;
            Contract Contract = GenerateContractFromSingleContractPayload(data, CurrentUser);

            if (!Contract.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {

                await writer.DB.Contracts
                    .Where(c => c.ContractId == Contract.ContractId)
                    .DeleteAsync();

                await writer.DB.Contracts.AddAsync(Contract);

                await writer.DB.SaveChangesAsync();
            }

            await Task.CompletedTask;
            return Ok();
        }

        /// <summary>
        /// Retrieves contracts for the given username(s)
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group">The group</param>
        /// <returns>List of contracts</returns>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<Contract>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetContracts(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<Model.Contract>> UserToContracts;
            using (var reader = DBAccess.GetReader())
            {
                UserToContracts = await reader.DB.Contracts
                    .Include(c => c.Conditions)
                    .Where(c => UserToPerm.Keys.Contains(c.UserNameSubmitted))
                    .GroupBy(c => c.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToContracts, UserToPerm);
            return Ok(Response);
        }
    }
}
