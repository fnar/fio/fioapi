﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using System.Collections.Concurrent;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// ChatController
    /// </summary>
    [ApiController]
    [Route("/chat")]
    public class ChatController : ControllerBase
    {
        private static ConcurrentDictionary<string, long> ChannelIdToMostRecentTimestamp = new();

        private const int DeleteMRUCacheSize = 50;
        private static ConcurrentDictionary<string, MRUCache<long, string>> ChannelIdToDeletedMessages = new();

        private bool ShouldUpdateMessage(ChatMessage chatMessage)
        {
            bool bShouldUpdate = false;
            if (chatMessage.MessageDeleted)
            {
                MRUCache<long, string>? DeleteMRUCache;
                if (!ChannelIdToDeletedMessages.TryGetValue(chatMessage.ChatMessageId, out DeleteMRUCache))
                {
                    DeleteMRUCache = new(DeleteMRUCacheSize);
                    DeleteMRUCache.Set(chatMessage.MessageTimestamp, chatMessage.ChatMessageId);
                    ChannelIdToDeletedMessages[chatMessage.ChatMessageId] = DeleteMRUCache;
                    bShouldUpdate = true;
                }
                else
                {
                    bShouldUpdate = DeleteMRUCache.Get(chatMessage.MessageTimestamp) == null;
                    if (bShouldUpdate)
                    {
                        DeleteMRUCache.Set(chatMessage.MessageTimestamp, chatMessage.ChatMessageId);
                    }
                }
            }
            else
            {
                bShouldUpdate = !ChannelIdToMostRecentTimestamp.TryGetValue(chatMessage.ChatMessageId, out var mostRecentTimestamp) || chatMessage.MessageTimestamp > mostRecentTimestamp;
                if (bShouldUpdate)
                {
                    ChannelIdToMostRecentTimestamp[chatMessage.ChatMessageId] = chatMessage.MessageTimestamp;
                }
            }

            return bShouldUpdate;
        }

        private List<ChatMessage> GetMessagesToUpdate(List<ChatMessage> chatMessages)
        {
            if (chatMessages.Count == 0)
            {
                return chatMessages;
            }

            System.Diagnostics.Debug.Assert(chatMessages.Select(cm => cm.ChatChannelId).Distinct().Count() == 1);
            var ChatChannelId = chatMessages[0].ChatMessageId;

            ChannelIdToMostRecentTimestamp.GetOrAdd(ChatChannelId, 0);

            // Handle regular messages
            var RegularMessages = chatMessages.Where(cm => cm.MessageDeleted == false);
            if (ChannelIdToMostRecentTimestamp.TryGetValue(ChatChannelId, out var mostRecentTimestamp))
            {
                RegularMessages = RegularMessages.Where(rm => rm.MessageTimestamp > mostRecentTimestamp);       // Remove any messages that are less than the latest timestamp
                if (RegularMessages.Any())
                {
                    ChannelIdToMostRecentTimestamp[ChatChannelId] = RegularMessages.Max(rm => rm.MessageTimestamp);	// Update the max timestamp
                }
            }

            // Handle deletes
            var DeleteMessages = chatMessages.Where(cm => cm.MessageDeleted == true).OrderBy(cm => cm.MessageTimestamp);
            MRUCache<long, string>? DeleteMRUCache;
            if (!ChannelIdToDeletedMessages.TryGetValue(ChatChannelId, out DeleteMRUCache))
            {
                DeleteMRUCache = new(DeleteMRUCacheSize);
                DeleteMRUCache.SetRange(DeleteMessages.Select(dm => new Tuple<long, string>(dm.MessageTimestamp, dm.ChatMessageId)).ToList());
                ChannelIdToDeletedMessages[ChatChannelId] = DeleteMRUCache;
            }
            else
            {
                lock (DeleteMRUCache.LockObj)
                {
                    DeleteMessages = DeleteMessages
                        .Where(dm => DeleteMRUCache.Get(dm.MessageTimestamp) == null)
                        .OrderBy(a => 1); // No-op sort, basically makes it so we get an OrderedEnumerable back
                }
            }

            return RegularMessages.Concat(DeleteMessages).ToList();
        }

        private async Task<IActionResult> PutSharedChannelDataAsync(Payloads.Chat.SHARED_CHANNEL_DATA_PAYLOAD channelDataPayload)
        {
            // Safety guard against private information
            bool ShouldStoreChannelData = channelDataPayload.type == "PUBLIC" || (channelDataPayload.type == "GROUP" && channelDataPayload.displayName != null && channelDataPayload.displayName.EndsWith("Global Site Owners"));
            if (!ShouldStoreChannelData)
                return Ok();

            var ChatChannel = new Model.ChatChannel();
            ChatChannel.ChatChannelId = channelDataPayload.channelId!;
            ChatChannel.Type = channelDataPayload.type;
            ChatChannel.NaturalId = channelDataPayload.naturalId;
            ChatChannel.DisplayName = channelDataPayload.displayName;
            ChatChannel.CreationTimestamp = channelDataPayload.creationTime.timestamp;
            ChatChannel.LastActivityTimestamp = channelDataPayload.lastActivity.timestamp;
            ChatChannel.UserNameSubmitted = this.GetUserName()!;
            ChatChannel.Timestamp = DateTime.UtcNow;

            var Errors = new List<string>();
            if (!ChatChannel.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.ChatChannels.Upsert(ChatChannel)
                    .On(cc => new { cc.ChatChannelId })
                    .WhenMatched((existingChatChannel, newChatChannel) => new DB.Model.ChatChannel
                    {
                        LastActivityTimestamp = newChatChannel.LastActivityTimestamp,
                        UserCount = newChatChannel.UserCount,
                        UserNameSubmitted = newChatChannel.UserNameSubmitted,
                        Timestamp = newChatChannel.Timestamp
                    })
                    .RunAsync();
            }

            return Ok();
        }

        /// <summary>
        /// PUTs a MESG_CHANNEL_DATA payload
        /// </summary>
        /// <param name="MesgChannelDataPayload">The MESG_CHANNEL_DATA payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("channel")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutChannelAsync([FromBody] Payloads.Chat.MESG_CHANNEL_DATA MesgChannelDataPayload)
        {
            var Errors = new List<string>();
            if (!MesgChannelDataPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            return await PutSharedChannelDataAsync(MesgChannelDataPayload.payload.message.payload);
        }

        /// <summary>
        /// PUTs a ROOT_CHANNEL_DATA payload
        /// </summary>
        /// <param name="RootChannelDataPayload">The ROOT_CHANNEL_DATA payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("channel/root")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutChannelAsync([FromBody] Payloads.Chat.ROOT_CHANNEL_DATA RootChannelDataPayload)
        {
            var Errors = new List<string>();
            if (!RootChannelDataPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            return await PutSharedChannelDataAsync(RootChannelDataPayload.payload);
        }

        private async Task<IActionResult> PutSharedMessageAsync(Payloads.Chat.SHARED_MESSAGE_ADDED SharedMessageAddedPayload)
        {
            ChatMessage chatMessage = new()
            {
                ChatMessageId = SharedMessageAddedPayload.messageId,
                Type = SharedMessageAddedPayload.type,
                SenderId = SharedMessageAddedPayload.sender.id,
                SenderUserName = SharedMessageAddedPayload.sender.username.ToLower(),
                MessageText = SharedMessageAddedPayload.message,
                MessageTimestamp = SharedMessageAddedPayload.time.timestamp,
                MessageDeleted = SharedMessageAddedPayload.deletingUser != null,
                DeletedByUserId = SharedMessageAddedPayload.deletingUser?.id,
                DeletedByUserName = SharedMessageAddedPayload.deletingUser?.username,
                UserNameSubmitted = this.GetUserName()!,
                Timestamp = DateTime.UtcNow,
                ChatChannelId = SharedMessageAddedPayload.channelId
            };

            var Errors = new List<string>();
            if (!chatMessage.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            bool bShouldUpdate = ShouldUpdateMessage(chatMessage);
            if (bShouldUpdate)
            {
                using (var writer = DBAccess.GetWriter())
                {
                    bool ChannelExists = writer.DB.ChatChannels.Any(cc => cc.ChatChannelId == chatMessage.ChatChannelId);
                    if (ChannelExists)
                    {
						await writer.DB.ChatMessages.Upsert(chatMessage)
						    .On(cm => new { cm.ChatMessageId })
						    .WhenMatched((existMsg, newMsg) => new ChatMessage
						    {
						    	Type = newMsg.Type,
						    	MessageText = newMsg.MessageText,
						    	MessageDeleted = newMsg.MessageDeleted,
						    	UserNameSubmitted = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.UserNameSubmitted : existMsg.UserNameSubmitted),
						    	Timestamp = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.Timestamp : existMsg.Timestamp),
						    })
						    .RunAsync();
					}
                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs a CHANNEL_MESSAGE_ADDED_SELF payload
        /// </summary>
        /// <param name="ChannelMessageAddedSelfPayload">The CHANNEL_MESSAGE_ADDED_SELF payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("messageaddedself")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMessageAddedSelfAsync([FromBody] Payloads.Chat.MESG_CHANNEL_MESSAGE_ADDED ChannelMessageAddedSelfPayload)
        {
            var Errors = new List<string>();
            if (!ChannelMessageAddedSelfPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            return await PutSharedMessageAsync(ChannelMessageAddedSelfPayload.payload.message.payload);
        }

        /// <summary>
        /// PUTs a CHANNEL_MESSAGE_ADDED payload
        /// </summary>
        /// <param name="ChannelMessageAddedPayload">The CHANNEL_MESSAGE_ADDED payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("messageadded")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMessageAddedAsync([FromBody] Payloads.Chat.ROOT_CHANNEL_MESSAGE_ADDED ChannelMessageAddedPayload)
        {
            var Errors = new List<string>();
            if (!ChannelMessageAddedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            return await PutSharedMessageAsync(ChannelMessageAddedPayload.payload);
        }

        /// <summary>
        /// PUTs a CHANNEL_MESSAGE_DELETED payload
        /// </summary>
        /// <param name="ChannelMessageDeletedPayload">The CHANNEL_MESSAGE_DELETED payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("messagedeleted")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMessageDeletedAsync([FromBody] Payloads.Chat.CHANNEL_MESSAGE_DELETED ChannelMessageDeletedPayload)
        {
            var Errors = new List<string>();
            if (!ChannelMessageDeletedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var payload = ChannelMessageDeletedPayload.payload;
            ChatMessage chatMessage = new()
            {
                ChatMessageId = payload.messageId,
                Type = payload.type,
                SenderId = payload.sender.id,
                SenderUserName = payload.sender.username.ToLower(),
                MessageText = payload.message,
                MessageTimestamp = payload.time.timestamp,
                MessageDeleted = payload.deletingUser != null,
                DeletedByUserId = payload.deletingUser?.id,
                DeletedByUserName = payload.deletingUser?.username.ToLower(),
                UserNameSubmitted = this.GetUserName()!,
                Timestamp = DateTime.UtcNow,
                ChatChannelId = payload.channelId
            };

            Errors.Clear();
            if (!chatMessage.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            bool bShouldUpdate = ShouldUpdateMessage(chatMessage);
            if (bShouldUpdate)
            {
                using (var writer = DBAccess.GetWriter())
                {
					bool ChannelExists = writer.DB.ChatChannels.Any(cc => cc.ChatChannelId == chatMessage.ChatChannelId);
                    if (ChannelExists)
                    {
						await writer.DB.ChatMessages.Upsert(chatMessage)
						    .On(cm => new { cm.ChatMessageId })
						    .WhenMatched((existMsg, newMsg) => new ChatMessage
						    {
						    	Type = newMsg.Type,
						    	MessageText = newMsg.MessageText,
						    	MessageDeleted = newMsg.MessageDeleted,
						    	UserNameSubmitted = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.UserNameSubmitted : existMsg.UserNameSubmitted),
						    	Timestamp = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.Timestamp : existMsg.Timestamp),
						    })
						    .RunAsync();
					}
                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs a CHANNEL_MESSAGE_LIST payload
        /// </summary>
        /// <param name="ChannelMessageListPayload">The CHANNEL_MESSAGE_LIST payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("messagelist")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutMessageListAsync([FromBody] Payloads.Chat.CHANNEL_MESSAGE_LIST ChannelMessageListPayload)
        {
            var Errors = new List<string>();
            if (!ChannelMessageListPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var payload = ChannelMessageListPayload.payload.message.payload;
            var messages = payload.messages.Select(msg => new ChatMessage
            {
                ChatMessageId = msg.messageId,
                Type = msg.type,
                SenderId = msg.sender.id,
                SenderUserName = msg.sender.username.ToLower(),
                MessageText = msg.message,
                MessageTimestamp = msg.time.timestamp,
                MessageDeleted = msg.deletingUser != null,
                DeletedByUserId = msg.deletingUser?.id,
                DeletedByUserName = msg.deletingUser?.username.ToLower(),
                UserNameSubmitted = this.GetUserName()!,
                Timestamp = DateTime.UtcNow,
                ChatChannelId = msg.channelId
            }).ToList();

            messages.ForEach(msg =>
            {
                msg.Validate(ref Errors);
            });
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            messages = GetMessagesToUpdate(messages);
            if (messages.Count > 0)
            {
                using (var writer = DBAccess.GetWriter())
                {
					bool ChannelExists = writer.DB.ChatChannels.Any(cc => cc.ChatChannelId == messages.First().ChatChannelId);
                    if (ChannelExists)
                    {
						await writer.DB.ChatMessages.UpsertRange(messages)
							.On(cm => new { cm.ChatMessageId })
							.WhenMatched((existMsg, newMsg) => new ChatMessage
							{
								Type = newMsg.Type,
								MessageText = newMsg.MessageText,
								MessageDeleted = newMsg.MessageDeleted,
								UserNameSubmitted = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.UserNameSubmitted : existMsg.UserNameSubmitted),
								Timestamp = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.Timestamp : existMsg.Timestamp),
							})
							.RunAsync();
					}

                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs a CHANNEL_USER_JOINED payload
        /// </summary>
        /// <param name="ChannelUserJoinedPayload">The CHANNEL_USER_JOINED payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("userjoined")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutUserJoinedAsync([FromBody] Payloads.Chat.CHANNEL_USER_JOINED ChannelUserJoinedPayload)
        {
            var Errors = new List<string>();
            if (!ChannelUserJoinedPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var payload = ChannelUserJoinedPayload.payload;
            ChatMessage chatMessage = new()
            {
                ChatMessageId = payload.messageId,
                Type = "JOINED",
                SenderId = payload.user != null && payload.user.user != null ? payload.user.user.id : new String('0', 32),
                SenderUserName = payload.user != null && payload.user.user != null ? payload.user.user.username.ToLower() : "unknownusername",
                MessageText = "",
                MessageTimestamp = payload.time.timestamp,
                MessageDeleted = false,
                DeletedByUserId = null,
                DeletedByUserName = null,
                UserNameSubmitted = this.GetUserName()!,
                Timestamp = DateTime.UtcNow,
                ChatChannelId = payload.channelId
            };

            Errors.Clear();
            if (!chatMessage.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            bool bShouldUpdate = ShouldUpdateMessage(chatMessage);
            if (bShouldUpdate)
            {
                using (var writer = DBAccess.GetWriter())
                {
					bool ChannelExists = writer.DB.ChatChannels.Any(cc => cc.ChatChannelId == chatMessage.ChatChannelId);
                    if (ChannelExists)
                    {
                        await writer.DB.ChatMessages.Upsert(chatMessage)
                            .On(cm => new { cm.ChatMessageId })
                            .WhenMatched((existMsg, newMsg) => new ChatMessage
                            {
                                Type = newMsg.Type,
                                MessageText = newMsg.MessageText,
                                MessageDeleted = newMsg.MessageDeleted,
                                UserNameSubmitted = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.UserNameSubmitted : existMsg.UserNameSubmitted),
                                Timestamp = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.Timestamp : existMsg.Timestamp),
                            })
                            .RunAsync();
                    }
					
                }
            }

            return Ok();
        }

        /// <summary>
        /// PUTs a CHANNEL_USER_LEFT payload
        /// </summary>
        /// <param name="ChannelUserLeftPayload">The CHANNEL_USER_LEFT payload</param>
        /// <returns>OK on success</returns>
        [HttpPut("userleft")]
        [SharedRateLimit]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutUserLeftAsync([FromBody] Payloads.Chat.CHANNEL_USER_LEFT ChannelUserLeftPayload)
        {
            var Errors = new List<string>();
            if (!ChannelUserLeftPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var payload = ChannelUserLeftPayload.payload;
            ChatMessage chatMessage = new()
            {
                ChatMessageId = payload.messageId,
                Type = "LEFT",
                SenderId = payload.user != null && payload.user.user != null ? payload.user.user.id : new String('0', 32),
                SenderUserName = payload.user != null && payload.user.user != null ? payload.user.user.username.ToLower() : "unknownusername",
                MessageText = "",
                MessageTimestamp = payload.time.timestamp,
                MessageDeleted = false,
                DeletedByUserId = null,
                DeletedByUserName = null,
                UserNameSubmitted = this.GetUserName()!,
                Timestamp = DateTime.UtcNow,
                ChatChannelId = payload.channelId
            };

            Errors.Clear();
            if (!chatMessage.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            bool bShouldUpdate = ShouldUpdateMessage(chatMessage);
            if (bShouldUpdate)
            {
                using (var writer = DBAccess.GetWriter())
                {
					bool ChannelExists = writer.DB.ChatChannels.Any(cc => cc.ChatChannelId == chatMessage.ChatChannelId);
                    if (ChannelExists)
                    {
						await writer.DB.ChatMessages.Upsert(chatMessage)
						.On(cm => new { cm.ChatMessageId })
						.WhenMatched((existMsg, newMsg) => new ChatMessage
						{
							Type = newMsg.Type,
							MessageText = newMsg.MessageText,
							MessageDeleted = newMsg.MessageDeleted,
							UserNameSubmitted = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.UserNameSubmitted : existMsg.UserNameSubmitted),
							Timestamp = (existMsg.MessageDeleted == false && newMsg.MessageDeleted == true ? newMsg.Timestamp : existMsg.Timestamp),
						})
						.RunAsync();
					}
                }
            }

            return Ok();
        }

        /// <summary>
        /// Retries a list of channels
        /// </summary>
        /// <param name="types">[Optional] The list of channel types to return</param>
        /// <returns>OK on success</returns>
        [HttpGet("list")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<ChatChannel>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Request failed validation", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetChannelList(
            [FromQuery] List<string>? types)
        {
            List<string> Errors = new();

            if (types != null && types.Count > 0)
            {
                types = types.ConvertAll(t => t.ToLower());
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(types, typeof(ChatChannel), nameof(ChatChannel.Type), ref Errors, nameof(types));
            }

            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            List<ChatChannel> Channels = new();
            using (var reader = DBAccess.GetReader())
            {
                var ChatChannelsQueryable = reader.DB.ChatChannels.AsQueryable();
                if (types != null && types.Count > 0)
                {
                    ChatChannelsQueryable = ChatChannelsQueryable.Where(cc => types.Contains(cc.Type.ToLower()));
                }
                Channels = await ChatChannelsQueryable.ToListAsync();
            }

            return Ok(Channels);
        }

        /// <summary>
        /// Retrieves a list of messages given the queried parameters.
        /// </summary>
        /// <param name="channel_names">[Optional] List of channel names, `&amp;` separated</param>
        /// <param name="channel_ids">[Optional] List of channel ids, `&amp;` separated</param>
        /// <param name="updated_since">[Optional] Only retrieve messages which were updated since this time</param>
        /// <param name="skip">[Optional] Offset or skip this number of messages from the start of the result. Default 0</param>
        /// <param name="top">[Optional] Take this number of messages. Default 1000</param>
        /// <remarks>The maximum number of messages retrievable is 1000</remarks>
        /// <returns>Chat messages</returns>
        [HttpGet("messages")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<ChatMessage>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Request failed validation", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetMessages(
            [FromQuery] List<string>? channel_names,
            [FromQuery] List<string>? channel_ids,
            [FromQuery] DateTime? updated_since,
            [FromQuery] int skip = 0,
            [FromQuery] int top = 1000)
        {
            List<string> Errors = new();

            if (channel_names != null && channel_names.Count > 0)
            {
                channel_names = channel_names.ConvertAll(cn => cn.ToLower());
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(channel_names, typeof(ChatChannel), nameof(ChatChannel.DisplayName), ref Errors, nameof(channel_names));
            }
        
            if (channel_ids != null && channel_ids.Count > 0)
            {
                channel_ids = channel_ids.ConvertAll(ci => ci.ToLower());
                ValidationExtensions.RunValidationsAsIfTypeAndProperty(channel_ids, typeof(ChatChannel), nameof(ChatChannel.ChatChannelId), ref Errors, nameof(channel_ids));
            }

            if (skip < 0)
            {
                Errors.Add($"{nameof(skip)}'s value must be non-negative.");
            }

            if (top <= 0 || top > 1000)
            {
                Errors.Add($"{nameof(top)}'s value of {top} is invalid.  Must be in the [1, 1000] range.");
            }

            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            List<ChatMessage> ChatMessages = new();

            using (var reader = DBAccess.GetReader())
            {
                var ChatMessagesQueryable = reader.DB.ChatMessages.AsQueryable();
                if (channel_names != null && channel_names.Count > 0)
                {
                    ChatMessagesQueryable = ChatMessagesQueryable.Include(cm => cm.ChatChannel);
                    ChatMessagesQueryable = ChatMessagesQueryable.Where(cm =>
                        (cm.ChatChannel.DisplayName != null && channel_names.Contains(cm.ChatChannel.DisplayName.ToLower())) ||
                        (cm.ChatChannel.NaturalId != null && channel_names.Contains(cm.ChatChannel.NaturalId.ToLower())));
                }

                if (channel_ids != null && channel_ids.Count > 0)
                {
                    ChatMessagesQueryable = ChatMessagesQueryable.Where(cm => channel_ids.Contains(cm.ChatChannelId));
                }

                if (updated_since != null)
                {
                    ChatMessagesQueryable = ChatMessagesQueryable.Where(cm => cm.Timestamp > updated_since);
                }

                ChatMessagesQueryable = ChatMessagesQueryable
                    .OrderByDescending(cm => cm.Timestamp)
                    .ThenByDescending(cm => cm.MessageTimestamp);

                ChatMessagesQueryable = ChatMessagesQueryable.Skip((int)skip);
                ChatMessagesQueryable = ChatMessagesQueryable.Take((int)top);

                ChatMessages = await ChatMessagesQueryable
                    .Reverse()      // Output should be oldest first
                    .ToListAsync();
            }

            return Ok(ChatMessages);
        }

        /// <summary>
        /// Gets chat messages of the provided user in the provided channel_id
        /// </summary>
        /// <param name="username"></param>
        /// <param name="channel_id"></param>
        /// <param name="skip">[Optional] Offset or skip this number of messages from the start of the result</param>
        /// <param name="top">[Optional] Take this number of messages</param>
        /// <remarks>This will return a maximum of 1000 chat messages</remarks>
        /// <returns>Chat messages for the provided username</returns>
        [HttpGet("user/{username}/{channel_id}")]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(List<ChatMessage>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Request failed validation", typeof(List<string>), "application/json")]
        public async Task<IActionResult> GetChatForUserInChannel(
            string username, 
            string channel_id,
            [FromQuery] int skip = 0,
            [FromQuery] int top = 1000)
        {
            List<string> Errors = new();

            username = username.ToLower();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(username, typeof(User), nameof(DB.Model.User.UserName), ref Errors, nameof(username));

            channel_id = channel_id.ToLower();
            ValidationExtensions.RunValidationsAsIfTypeAndProperty(channel_id, typeof(ChatChannel), nameof(ChatChannel.ChatChannelId), ref Errors, nameof(channel_id));

            if (skip < 0)
            {
                Errors.Add($"{nameof(skip)}'s value must be non-negative.");
            }

            if (top <= 0 || top > 1000)
            {
                Errors.Add($"{nameof(top)}'s value of {top} is invalid.  Must be in the [1, 1000] range.");
            }

            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            List<ChatMessage> ChatMessages = new();
            using (var reader = DBAccess.GetReader())
            {
                ChatMessages = await reader.DB.ChatMessages
                    .Include(cm => cm.ChatChannel)
                    .Where(cm => cm.SenderUserName == username)
                    .Where(cm => cm.ChatChannelId == channel_id || cm.ChatChannel.NaturalId == channel_id)
                    .OrderBy(cm => cm.MessageTimestamp)
                    .Skip(skip)
                    .Reverse()
                    .Take(top)
                    .ToListAsync();
            }
                
            return Ok(ChatMessages);
        }
    }
}
