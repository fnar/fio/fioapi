﻿using CommandLine;
using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using FIOAPI.Utils;
using System.Linq;
using System.Net;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// ShipController
    /// </summary>
    [ApiController]
    [Route("/ships")]
    public class ShipsController : ControllerBase
    {
        /// <summary>
        /// PUTs MESG_SHIP_SHIPS payload
        /// </summary>
        /// <param name="shipsPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutShips([FromBody] Payloads.Ship.MESG_SHIP_SHIPS shipsPayload)
        {
            List<string> Errors = new();
            if (!shipsPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            List<Model.Ship> Ships = new();
            var data = shipsPayload.payload.message.payload;
            foreach (var ship in data.ships)
            {
                Model.Ship ShipEntry = new();
                ShipEntry.ShipId = ship.id;
                ShipEntry.StoreId = ship.idShipStore;
                ShipEntry.STLFuelStoreId = ship.idStlFuelStore;
                ShipEntry.FTLFuelStoreId = ship.idFtlFuelStore;
                ShipEntry.Registration = ship.registration;
                ShipEntry.Name = ship.name;
                ShipEntry.CommisioningTime = ship.commissioningTime.timestamp.FromUnixTime();
                ShipEntry.BlueprintNaturalId = ship.blueprintNaturalId;
                ShipEntry.CurrentLocationId = ship.address?.lines.Last().entity?.id;
                ShipEntry.CurrentLocationName = ship.address?.lines.Last().entity?.name;
                ShipEntry.CurrentLocationNaturalId = ship.address?.lines.Last().entity?.naturalId;
                ShipEntry.FlightId = ship.flightId;
                ShipEntry.Acceleration = ship.acceleration;
                ShipEntry.Thrust = ship.thrust;
                ShipEntry.Mass = ship.mass;
                ShipEntry.OperatingEmptyMass = ship.operatingEmptyMass;
                ShipEntry.Volume = ship.volume;
                ShipEntry.ReactorPower = ship.reactorPower;
                ShipEntry.EmitterPower = ship.emitterPower;
                ShipEntry.STLFuelFlowRate = ship.stlFuelFlowRate;
                ShipEntry.OperatingTimeSTLMilliseconds = ship.operatingTimeStl.millis;
                ShipEntry.OperatingTimeFTLMilliseconds = ship.operatingTimeFtl != null ? ship.operatingTimeFtl.millis : 0;
                ShipEntry.Condition = ship.condition;
                ShipEntry.LastRepair = ship.lastRepair?.timestamp.FromUnixTime();

                foreach (var repairMat in ship.repairMaterials)
                {
                    ShipRepairMaterial RepairMaterial = new();
                    RepairMaterial.ShipRepairMaterialId = $"{ShipEntry.ShipId}-{repairMat.material.id}";
                    RepairMaterial.MaterialId = repairMat.material.id;
                    RepairMaterial.MaterialTicker = repairMat.material.ticker;
                    RepairMaterial.Amount = repairMat.amount;
                    RepairMaterial.ShipId = ShipEntry.ShipId;
                    RepairMaterial.Ship = ShipEntry;
                    ShipEntry.RepairMaterials.Add(RepairMaterial);
                }

                ShipEntry.UserNameSubmitted = CurrentUser;
                ShipEntry.Timestamp = DateTime.UtcNow;
                Ships.Add(ShipEntry);
            }

            if (!Ships.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Ships
                    .Where(s => s.UserNameSubmitted == CurrentUser)
                    .DeleteAsync();

                await writer.DB.Ships.AddRangeAsync(Ships);

                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// PUTs MESG_SHIP_FLIGHT_FLIGHTS payload
        /// </summary>
        /// <param name="flightsPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("flights")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutFlights([FromBody] Payloads.Ship.MESG_SHIP_FLIGHT_FLIGHTS flightsPayload)
        {
            List<string> Errors = new();
            if (!flightsPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            List<Flight> Flights = new();
            var data = flightsPayload.payload.message.payload;
            foreach (var flight in data.flights)
            {
                Flight Flight = new();
                Flight.FlightId = flight.id;
                Flight.ShipId = flight.shipId;
                Flight.OriginId = flight.origin.lines.Last().entity?.id;
                Flight.OriginName = flight.origin.lines.Last().entity?.name;
                Flight.OriginNaturalId = flight.origin.lines.Last().entity?.naturalId;
                Flight.DestinationId = flight.destination.lines.Last().entity?.id;
                Flight.DestinationName = flight.destination.lines.Last().entity?.name;
                Flight.DestinationNaturalId = flight.destination.lines.Last().entity?.naturalId;
                Flight.Departure = flight.departure.timestamp.FromUnixTime();
                Flight.Arrival = flight.arrival.timestamp.FromUnixTime();
                Flight.CurrentSegmentIndex = flight.currentSegmentIndex;
                Flight.STLDistance = flight.stlDistance;
                Flight.FTLDistance = flight.ftlDistance;
                Flight.Aborted = flight.aborted;
                
                for (int flightSegmentIdx = 0; flightSegmentIdx < flight.segments.Count; ++flightSegmentIdx)
                {
                    var seg = flight.segments[flightSegmentIdx];

                    FlightSegment Segment = new();
                    Segment.FlightSegmentId = $"{Flight.FlightId}-{flightSegmentIdx}";
                    Segment.Type = seg.type;
                    Segment.OriginId = seg.origin.lines.Last().entity?.id;
                    Segment.OriginNaturalId = seg.origin.lines.Last().entity?.naturalId;
                    Segment.OriginName = seg.origin.lines.Last().entity?.name;
                    Segment.OriginSemiMajorAxis = seg.origin.lines.Last().orbit?.semiMajorAxis;
                    Segment.OriginEccentricity = seg.origin.lines.Last().orbit?.eccentricity;
                    Segment.OriginInclination = seg.origin.lines.Last().orbit?.inclination;
                    Segment.OriginRightAscension = seg.origin.lines.Last().orbit?.rightAscension;
                    Segment.OriginPeriapsis = seg.origin.lines.Last().orbit?.periapsis;
                    Segment.Departure = seg.departure.timestamp.FromUnixTime();
                    Segment.Arrival = seg.arrival.timestamp.FromUnixTime();
                    Segment.DestinationId = seg.destination.lines.Last().entity?.id;
                    Segment.DestinationNaturalId = seg.destination.lines.Last().entity?.naturalId;
                    Segment.DestinationName = seg.destination.lines.Last().entity?.name;
                    Segment.DestinationSemiMajorAxis = seg.destination.lines.Last().orbit?.semiMajorAxis;
                    Segment.DestinationEccentricity = seg.destination.lines.Last().orbit?.eccentricity;
                    Segment.DestinationInclination = seg.destination.lines.Last().orbit?.inclination;
                    Segment.DestinationRightAscension = seg.destination.lines.Last().orbit?.rightAscension;
                    Segment.DestinationPeriapsis = seg.destination.lines.Last().orbit?.periapsis;
                    Segment.TransferEllipseStartPositionX = seg.transferEllipse?.startPosition.x;
                    Segment.TransferEllipseStartPositionY = seg.transferEllipse?.startPosition.y;
                    Segment.TransferEllipseStartPositionZ = seg.transferEllipse?.startPosition.z;
                    Segment.TransferEllipseTargetPositionX = seg.transferEllipse?.targetPosition.x;
                    Segment.TransferEllipseTargetPositionY = seg.transferEllipse?.targetPosition.y;
                    Segment.TransferEllipseTargetPositionZ = seg.transferEllipse?.targetPosition.z;
                    Segment.TransferEllipseCenterPositionX = seg.transferEllipse?.center.x;
                    Segment.TransferEllipseCenterPositionY = seg.transferEllipse?.center.y;
                    Segment.TransferEllipseCenterPositionZ = seg.transferEllipse?.center.z;
                    Segment.TransferEllipseAlpha = seg.transferEllipse?.alpha;
                    Segment.TransferEllipseSemiMajorAxis = seg.transferEllipse?.semiMajorAxis;
                    Segment.TransferEllipseSemiMinorAxis = seg.transferEllipse?.semiMinorAxis;
                    Segment.STLDistance = seg.stlDistance;
                    Segment.FTLDistance = seg.ftlDistance;
                    Segment.STLFuelConsumption = seg.stlFuelConsumption;
                    Segment.FTLFuelConsumption = seg.ftlFuelConsumption;
                    Segment.Damage = seg.damage;
                    Segment.FlightId = Flight.FlightId;
                    Segment.Flight = Flight;

                    Flight.FlightSegments.Add(Segment);
                }

                Flight.UserNameSubmitted = CurrentUser;
                Flight.Timestamp = DateTime.UtcNow;

                Flights.Add(Flight);
            }

            if (!Flights.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Flights
                    .Where(f => f.UserNameSubmitted == CurrentUser)
                    .DeleteAsync();

                await writer.DB.Flights.AddRangeAsync(Flights);

                await writer.DB.SaveChangesAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves ships for the provided UserName
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group"></param>
        /// <param name="include_repair_materials">If repair materials should be included</param>
        /// <returns>List of ships</returns>
        [HttpGet()]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<Ship>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetShips(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_repair_materials")] bool include_repair_materials = false)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<Model.Ship>> UserToShips;
            using (var reader = DBAccess.GetReader())
            {
                var UserToShipsQuery = reader.DB.Ships.AsQueryable();
                if (include_repair_materials)
                {
                    UserToShipsQuery = UserToShipsQuery.Include(s => s.RepairMaterials);
                }

                UserToShips = await UserToShipsQuery
                    .Where(s => UserToPerm.Keys.Contains(s.UserNameSubmitted))
                    .GroupBy(s => s.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToShips, UserToPerm);
            return Ok(Response);
        }

        /// <summary>
        /// GetFlights
        /// </summary>
        /// <param name="username">username array</param>
        /// <param name="group">group</param>
        /// <returns>list of flights</returns>
        [HttpGet("flights")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<Ship>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetFlights(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<Flight>> UserToFlights;
            using (var reader = DBAccess.GetReader())
            {
                UserToFlights = await reader.DB.Flights
                    .Where(f => UserToPerm.Keys.Contains(f.UserNameSubmitted))
                    .GroupBy(f => f.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToFlights, UserToPerm);
            return Ok(Response);
        }
    }
}
