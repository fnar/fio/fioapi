﻿using FIOAPI.ControllerFilters;
using FIOAPI.DB.Model;
using Z.EntityFramework.Plus;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// StorageController
    /// </summary>
    [ApiController]
    [Route("/storage")]
    public class StorageController : ControllerBase
    {
        private static Storage GenerateStorageFromStorePayloadobj(Payloads.Storage.STORAGE_STORAGES_STORE store, string CurrentUser)
        {
            Storage Storage = new();
            Storage.StorageId = store.id;
            Storage.AddressableId = store.addressableId;
            Storage.Name = store.name;
            Storage.WeightLoad = store.weightLoad;
            Storage.WeightCapacity = store.weightCapacity;
            Storage.VolumeLoad = store.volumeLoad;
            Storage.VolumeCapacity = store.volumeCapacity;
            Storage.Fixed = store.@fixed;
            Storage.TradeStore = store.tradeStore;
            Storage.Rank = store.rank;
            Storage.Locked = store.locked;
            Storage.Type = store.type;
            Storage.UserNameSubmitted = CurrentUser;
            Storage.Timestamp = DateTime.UtcNow;

            foreach (var storageItem in store.items)
            {
                StorageItem StorageItem = new();
                StorageItem.StorageItemId = $"{Storage.StorageId}-{storageItem.id}";
                StorageItem.ItemId = storageItem.id;
                StorageItem.MaterialId = storageItem.quantity?.material.id;
                StorageItem.MaterialTicker = storageItem.quantity?.material.ticker;
                StorageItem.Amount = storageItem.quantity?.amount;
                StorageItem.Weight = storageItem.weight;
                StorageItem.Volume = storageItem.volume;
                StorageItem.Type = storageItem.type;
                StorageItem.ValueCurrencyCode = storageItem.quantity?.value.currency;
                StorageItem.Value = storageItem.quantity?.value.amount;
                StorageItem.StorageId = Storage.StorageId;
                StorageItem.Storage = Storage;

                Storage.StorageItems.Add(StorageItem);
            }

            return Storage;
        }

        private static async Task WriteStoragesToDB(List<Storage> Storages, string CurrentUser)
        {
            List<string> StorageIds = Storages
                .Select(s => s.StorageId)
                .ToList();

            List<string> AddressableIds = Storages
                .Select(s => s.AddressableId)
                .ToList();

            using (var writer = DBAccess.GetWriter())
            {
                var IdToPlanetId = await writer.DB.PlanetSites
                    .Where(ps => AddressableIds.Contains(ps.PlanetSiteId))
                    .Select(ps => new
                    {
                        id = ps.PlanetSiteId,
                        planet_id = ps.PlanetId
                    }).ToDictionaryAsync(k => k.id, v => v.planet_id);

                var UserShips = await writer.DB.Ships
                    .Where(s => s.UserNameSubmitted == CurrentUser)
                    .ToDictionaryAsync(s => s.ShipId, s => new
                    {
                        s.ShipId,
                        s.Registration,
                        s.Name
                    });

                for (int StorageIdx = 0; StorageIdx < Storages.Count; ++StorageIdx)
                {
                    if (IdToPlanetId.TryGetValue(Storages[StorageIdx].AddressableId, out string? value))
                    {
                        var planet = EntityCaches.PlanetCache.Get(value);
                        if (planet != null)
                        {
                            Storages[StorageIdx].LocationId = planet.PlanetId;
                            Storages[StorageIdx].LocationName = planet.Name;
                            Storages[StorageIdx].LocationNaturalId = planet.NaturalId;
                        }

                        continue;
                    }

                    var userShip = new { ShipId = "", Registration = "", Name = "" };
                    if (UserShips.TryGetValue(Storages[StorageIdx].AddressableId, out userShip!))
                    {
                        Storages[StorageIdx].LocationId = userShip.ShipId;
                        Storages[StorageIdx].LocationName = userShip.Name;
                        Storages[StorageIdx].LocationNaturalId = userShip.Registration;
                        continue;
                    }

                    var warehouse = EntityCaches.WarehouseCache.Get(Storages[StorageIdx].AddressableId);
                    if (warehouse != null)
                    {
                        Storages[StorageIdx].LocationId = warehouse.LocationId;
                        Storages[StorageIdx].LocationName = warehouse.LocationName;
                        Storages[StorageIdx].LocationNaturalId = warehouse.LocationNaturalId;
                    }
                }

                await writer.DB.Storages
                    .Where(s => s.UserNameSubmitted == CurrentUser && StorageIds.Contains(s.StorageId))
                    .DeleteAsync();

                await writer.DB.Storages
                    .AddRangeAsync(Storages);

                await writer.DB.SaveChangesAsync();
            }
        }

        /// <summary>
        /// PUTs STORAGE_STORAGES payload
        /// </summary>
        /// <param name="storagePaylaod">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutStorage([FromBody] Payloads.Storage.STORAGE_STORAGES storagePaylaod)
        {
            List<string> Errors = new();
            if (!storagePaylaod.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            List<Storage> Storages = storagePaylaod.payload.message.payload.stores
                .Select(s => GenerateStorageFromStorePayloadobj(s, CurrentUser))
                .ToList();

            if (!Storages.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            await WriteStoragesToDB(Storages, CurrentUser);

            return Ok();
        }

        /// <summary>
        /// PUTs ROOT_STORAGE_CHANGE payload
        /// </summary>
        /// <param name="storageChangePayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("change")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutStorageChange([FromBody] Payloads.Storage.ROOT_STORAGE_CHANGE storageChangePayload)
        {
            List<string> Errors = new();
            if (!storageChangePayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var CurrentUser = this.GetUserName()!;

            List<Storage> Storages = storageChangePayload.payload.stores
                .Select(s => GenerateStorageFromStorePayloadobj(s, CurrentUser))
                .ToList();

            if (!Storages.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            await WriteStoragesToDB(Storages, CurrentUser);

            await Task.CompletedTask;
            return Ok();
        }

        /// <summary>
        /// PUTs WAREHOUSE_STORAGES payload
        /// </summary>
        /// <param name="warehouseStoragesPayload">The payload</param>
        /// <returns>Ok on success</returns>
        [HttpPut("warehouses")]
        [HydrationTimeoutFilter]
        [Authorize(Policy = AuthPolicy.UserWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Payload accepted")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Payload malformed", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Improperly using a read-only API key")]
        public async Task<IActionResult> PutWarehouses([FromBody] Payloads.Storage.WAREHOUSE_STORAGES warehouseStoragesPayload)
        {
            List<string> Errors = new();
            if (!warehouseStoragesPayload.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            var data = warehouseStoragesPayload.payload.message.payload;
            var userWarehouses = data.storages.Select(w => new UserWarehouse
            {
                UserWarehouseId = w.storeId,
                AddressableId = w.warehouseId,
                Units = w.units,
                WeightCapacity = w.weightCapacity,
                VolumeCapacity = w.volumeCapacity,
                NextPayment = w.nextPayment.timestamp.FromUnixTime(),
                FeeCurrency = w.fee?.currency,
                Fee = w.fee?.amount,
                FeeCollectorId = w.feeCollector?.id,
                FeeCollectorCode = w.feeCollector?.code,
                FeeCollectorName = w.feeCollector?.name,
                Status = w.status,
                UserNameSubmitted = this.GetUserName()!,
                Timestamp = DateTime.UtcNow
            }).ToList();

            for (int userWarehouseIdx = 0; userWarehouseIdx < userWarehouses.Count; ++userWarehouseIdx)
            {
                var warehouse = EntityCaches.WarehouseCache.Get(userWarehouses[userWarehouseIdx].AddressableId);
                if (warehouse != null)
                {
                    userWarehouses[userWarehouseIdx].LocationId = warehouse.LocationId;
                    userWarehouses[userWarehouseIdx].LocationName = warehouse.LocationName;
                    userWarehouses[userWarehouseIdx].LocationNaturalId = warehouse.LocationNaturalId;
                }
            }

            if (!userWarehouses.Validate(ref Errors))
            {
                return BadRequest(Errors);
            }

            if (userWarehouses.Count > 0)
            {
                using (var writer = DBAccess.GetWriter())
                {
                    await writer.DB.UserWarehouses.UpsertRange(userWarehouses)
                        .On(uw => new { uw.UserWarehouseId })
                        .RunAsync();
                }
            }

            return Ok();
        }

        /// <summary>
        /// Retrieves warehouse information for the provided UserName(s) / group
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group">Group</param>
        /// <returns>List of warehouse information</returns>
        [HttpGet("warehouseinfo")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<Storage>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetWarehouseInfo(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group)
        {
            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<UserWarehouse>> UserToUserWarehouse;
            using (var reader = DBAccess.GetReader())
            {
                UserToUserWarehouse = await reader.DB.UserWarehouses
                    .Where(s => UserToPerm.Keys.Contains(s.UserNameSubmitted))
                    .GroupBy(s => s.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToUserWarehouse, UserToPerm);
            return Ok(Response);
        }

        /// <summary>
        /// Retrieves Storages for the provided UserName(s)
        /// </summary>
        /// <param name="username">Provided user(s)</param>
        /// <param name="group">Group</param>
        /// <param name="include_bases">If bases should be included</param>
        /// <param name="include_warehouses">If warehouses should be included</param>
        /// <param name="include_ship_stores">If ship stores should be included</param>
        /// <param name="include_ship_stl_stores">If ship stl fuel stores should be included</param>
        /// <param name="include_ship_ftl_stores">If ship ftl fuel stores should be included</param>
        /// <param name="include_items">If items should be included</param>
        /// <returns>List of Storages</returns>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.UserRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Success", typeof(RetrievalResponse<List<Storage>>), "application/json")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Malformed input", typeof(List<string>), "application/json")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Don't have read access")]
        public async Task<IActionResult> GetStorage(
            [FromQuery(Name = "username")] List<string>? username,
            [FromQuery(Name = "group")] string? group,
            [FromQuery(Name = "include_bases")] bool include_bases = true,
            [FromQuery(Name = "include_warehouses")] bool include_warehouses = true,
            [FromQuery(Name = "include_ship_stores")] bool include_ship_stores = true,
            [FromQuery(Name = "include_ship_stl_stores")] bool include_ship_stl_stores = false,
            [FromQuery(Name = "include_ship_ftl_stores")] bool include_ship_ftl_stores = false,
            [FromQuery(Name = "include_items")] bool include_items = false)
        {
            if (!include_bases && !include_warehouses && !include_ship_stores && !include_ship_stl_stores && !include_ship_ftl_stores)
            {
                return BadRequest("Require at least one storage include");
            }

            List<string> Errors = new();
            Dictionary<string, Perm> UserToPerm;
            if (!this.CheckInputAndGetPerms(username, group, ref Errors, out UserToPerm, out var Result))
            {
                return Result;
            }

            Dictionary<string, List<Storage>> UserToSites;
            using (var reader = DBAccess.GetReader())
            {
                var UserToSitesQuery = reader.DB.Storages.AsQueryable();

                if (include_items)
                {
                    UserToSitesQuery = UserToSitesQuery.Include(s => s.StorageItems);
                }

                UserToSitesQuery = UserToSitesQuery
                    .Where(s =>
                        (include_bases && s.Type == Constants.StoreTypeEnumToStoreType[Constants.StoreType.Base]) ||
                        (include_warehouses && s.Type == Constants.StoreTypeEnumToStoreType[Constants.StoreType.Warehouse]) ||
                        (include_ship_stores && s.Type == Constants.StoreTypeEnumToStoreType[Constants.StoreType.Ship]) ||
                        (include_ship_stl_stores && s.Type == Constants.StoreTypeEnumToStoreType[Constants.StoreType.STLFuel]) ||
                        (include_ship_ftl_stores && s.Type == Constants.StoreTypeEnumToStoreType[Constants.StoreType.FTLFuel]));

                UserToSites = await UserToSitesQuery
                    .Where(s => UserToPerm.Keys.Contains(s.UserNameSubmitted))
                    .GroupBy(s => s.UserNameSubmitted)
                    .ToDictionaryAsync(g => g.Key, g => g.ToList());
            }

            var Response = Retrieval.CreateSanitizedResponse(UserToSites, UserToPerm);
            return Ok(Response);
        }
    }
}
