﻿using FIOAPI.DB.Model;
using FIOAPI.Payloads.Admin;

namespace FIOAPI.Controllers
{
    /// <summary>
    /// Admin endpoint
    /// </summary>
    [ApiController]
    [Route("/admin")]
    public class AdminController : ControllerBase
    {
        /// <summary>
        /// Checks to see if the current user is an administrator
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        [Authorize(Policy = AuthPolicy.AdminRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "You are admin")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "You are not admin")]
        public IActionResult Get()
        {
            return Ok();
        }

        /// <summary>
        /// Checks to see if the provided username is a user
        /// </summary>
        /// <param name="UserName">UserName</param>
        /// <returns>Ok if user is present</returns>
        [HttpGet("isuser/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminRead)]
        [SwaggerResponse(StatusCodes.Status200OK, "Is a valid user")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "Not a valid user")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Invalid UserName", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> IsUser(string UserName)
        {
            List<string> Errors = new();
            UserName = UserName.ToLower();

            ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserName, typeof(User), nameof(Model.User.UserName), ref Errors, nameof(UserName));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            using (var reader = DBAccess.GetReader())
            {
                var IsValidUser = await reader.DB.Users.AnyAsync(u => u.UserName == UserName);
                return IsValidUser ? Ok() : NoContent();
            }
        }

        /// <summary>
        /// Creates an account
        /// </summary>
        /// <param name="payload">The account creation payload</param>
        /// <returns>200 on success</returns>
        [HttpPost("createaccount")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully created account")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin or using read-only api key")]
        public async Task<IActionResult> CreateAccountAsync([FromBody] CreateAccount payload)
        {
            var Errors = new List<string>();
            payload.Validate(ref Errors);
            if (Errors.Count > 0)
            {
                return BadRequest(Errors.GetValidationErrorString());
            }

            if (payload.Bot && !payload.UserName.ToLower().EndsWith(" - bot"))
            {
                payload.UserName += " - Bot";
            }

            var NewUser = new User()
            {
                UserName = payload.UserName.ToLower(),
                DisplayUserName = payload.UserName,
                PasswordHash = SecurePasswordHasher.Hash(payload.Password),
                IsAdmin = payload.Admin
            };
            NewUser.RunValidation_Throw();

            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Users.Upsert(NewUser)
                    .On(u => u.UserName)
                    .RunAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Deletes the account of the given username
        /// </summary>
        /// <remakrs>This will return OK for usernames that don't exist</remakrs>
        /// <param name="UserName">The username to delete</param>
        /// <returns>200 on success</returns>
        [HttpDelete("deleteaccount/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Successfully created account")]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "Failed payload validation", typeof(string), "text/plain")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin or using read-only api key")]
        public async Task<IActionResult> DeleteAccountAsync(string UserName)
        {
            List<string> Errors = new();
            UserName = UserName.ToLower();

            ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserName, typeof(User), nameof(Model.User.UserName), ref Errors, nameof(UserName));
            if (Errors.Count > 0)
            {
                return BadRequest(Errors);
            }

            await ClearUserDataHelper.DeleteUser(UserName);
            return Ok();
        }

        /// <summary>
        /// Clears all CX data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearcxdata")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all CX Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearCXDataAsync()
        {
            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.CXEntries
                    .ExecuteDeleteAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Clears all chat data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearchatdata")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all CX Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearChatChannelsAsync()
        {
            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.ChatChannels
                    .ExecuteDeleteAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Clears all BUI data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearbuidata")]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all BUI Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearBUIDataAsync()
        {
            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Buildings
                    .ExecuteDeleteAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Clears all MAT data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearmatdata")]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all MAT Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearMATDataAsync()
        {
            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Materials
                    .ExecuteDeleteAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Clears all JumpCache data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearjumpcache")]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all JumpCache Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearJumpCacheAsync()
        {
            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.JumpCache
                    .ExecuteDeleteAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Clears all station data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearstationdata")]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all station Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearStationData()
        {
            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Stations
                    .ExecuteDeleteAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Clears all map data
        /// </summary>
        /// <returns>200 on success</returns>
        [HttpDelete("clearmapdata")]
        [SwaggerResponse(StatusCodes.Status200OK, "Deleted all map Data")]
        [SwaggerResponse(StatusCodes.Status401Unauthorized, "Not logged in")]
        [SwaggerResponse(StatusCodes.Status403Forbidden, "Not an admin")]
        public async Task<IActionResult> ClearMapData()
        {
            using (var writer = DBAccess.GetWriter())
            {
                await writer.DB.Systems
                    .ExecuteDeleteAsync();

                await writer.DB.Stars
                    .ExecuteDeleteAsync();

                await writer.DB.Sectors
                    .ExecuteDeleteAsync();
            }

            return Ok();
        }

        /// <summary>
        /// Starts a logging session
        /// </summary>
        /// <param name="UserName">The username to start a logging session for</param>
        /// <returns>OK</returns>
        [HttpPut("requestlogging/start/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Logging started")]
        public IActionResult StartRequestLogging(string UserName)
        {
            RequestLogger.Start(UserName);
            return Ok();
        }

        /// <summary>
        /// Cancels a logging session
        /// </summary>
        /// <param name="UserName">The username to cancel a logging session for</param>
        /// <returns></returns>
        [HttpPut("requestlogging/cancel/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Logging cancelled")]
        public IActionResult CancelRequestLogging(string UserName)
        {
            RequestLogger.Cancel(UserName);
            return Ok();
        }

        /// <summary>
        /// Finish a logging session and return the payload
        /// </summary>
        /// <param name="UserName">The username to finish a logging session for</param>
        /// <returns>The FinishedRequestPayload</returns>
        [HttpPut("requestlogging/finish/{UserName}")]
        [Authorize(Policy = AuthPolicy.AdminWrite)]
        [SwaggerResponse(StatusCodes.Status200OK, "Finish logging and receive the payload", typeof(FinishedRequestPayload), "application/json")]
        [SwaggerResponse(StatusCodes.Status204NoContent, "No active session for user found")]
        public IActionResult FinishRequestLogging(string UserName)
        {
            var result = RequestLogger.Finish(UserName);
            if (result != null)
            {
                return Ok(result);
            }

            return NoContent();
        }
    }
}
