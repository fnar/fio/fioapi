﻿namespace FIOAPI
{
    /// <summary>
    /// ExceptionLogger
    /// </summary>
    public static class ExceptionLogger
    {
        private const string StackFramePattern = @"at FIOAPI\..*\sin\s.+[/\\](?<File>(.+))\.cs:line\s(?<LineNumber>\d+)$";
        private static Regex StackFrameRegex = new Regex(StackFramePattern, RegexOptions.Compiled);

        /// <summary>
        /// Logs an exception to a file
        /// </summary>
        /// <param name="httpContext">HttpContext</param>
        /// <param name="ex">Exception</param>
        public static async void LogException(HttpContext httpContext, Exception ex)
        {
            if (Globals.IsTesting)
            {
                return;
            }

            var UserName = httpContext.GetUserName();

            var Message = ex.Message;
            var StackTrace = ex.StackTrace;
            string? RootPathFolder = null;
            if (StackTrace != null)
            {
                // Try to find a FIOAPI stackframe.
                var StackFrames = StackTrace.Split(new String[] { "\r\n", "\\r\\n", "\n", }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var StackFrame in StackFrames)
                {
                    var StackFrameTrimmed = StackFrame.Trim();
                    MatchCollection mc = StackFrameRegex.Matches(StackFrameTrimmed);
                    if (mc.Count == 1)
                    {
                        RootPathFolder = $"{mc[0].Groups["File"]}_cs_{mc[0].Groups["LineNumber"]}";
                        break;
                    }
                }
            }

            if (RootPathFolder == null)
            {
                // Fallback to Message HashCode
                var HashCode = Message.GetHashCode();
                if (HashCode < 0)
                {
                    HashCode *= -1;
                }

                RootPathFolder = HashCode.ToString();
            }

            string FullBadRequestPath = Path.GetFullPath(Globals.ExceptionsPath);
            Directory.CreateDirectory(FullBadRequestPath);

            // Create the directory for the directory for this RuntimeException grouping if it doesn't exist.  Also add a Message.txt file to the root of it
            string RequestRootPath = Path.Combine(FullBadRequestPath, RootPathFolder);
            if (!Directory.Exists(RequestRootPath))
            {
                Directory.CreateDirectory(RequestRootPath);
                try
                {
                    File.WriteAllText(Path.Combine(RequestRootPath, "Message.txt"), Message);
                }
                catch
                {
                    // Soft-fail.  It means we have multiple writing at the same time
                }
            }

            if (string.IsNullOrWhiteSpace(UserName))
            {
                UserName = "UnknownUserName_";
            }

            // Create the username directory under this path
            string UserNameDirectory = Path.Combine(RequestRootPath, UserName);
            Directory.CreateDirectory(UserNameDirectory);

            string? BodyContent = null;
            try
            {
                BodyContent = await httpContext.Request.GetBodyAsync();
            }
            catch
            {
                // Do nothing
            }

            // Now create the individual BadRequest directory
            DateTime now = DateTime.UtcNow;
            string ThisBadRequestPath = Path.Combine(UserNameDirectory, $"{now.Year:D4}-{now.Month:D2}-{now.Day:d2}_{now.Hour:D2}-{now.Minute:d2}-{now.Second:D2}-{now.Millisecond:D3}");
            Directory.CreateDirectory(ThisBadRequestPath);

            // Serialize the exception out
            JsonSerializerOptions options = new(JsonSerializerOptions.Default);
            options.WriteIndented = true;
            options.Converters.Add(new DetailedExceptionConverter());
            var jsonStr = JsonSerializer.Serialize(ex, options);

            File.WriteAllText(Path.Combine(ThisBadRequestPath, "ExData.json"), jsonStr);
            if (BodyContent != null)
            {
                File.WriteAllText(Path.Combine(ThisBadRequestPath, "Request.json"), BodyContent);
            }
        }
    }
}
