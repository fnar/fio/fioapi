﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to indicate that the provided string is a valid building category.
    /// Building categories are the following:
    /// - AGRICULTURE
    /// - CHEMISTRY
    /// - CONSTRUCTION
    /// - ELECTRONICS
    /// - FOOD_INDUSTRIES
    /// - FUEL_REFINING
    /// - MANUFACTURING
    /// - METALLURGY
    /// - RESOURCE_EXTRACTION
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class BuildingCategoryAttribute : Attribute
    {
        /// <summary>
        /// BuildingCategoryAttribute constructor
        /// </summary>
        public BuildingCategoryAttribute() { }
    }
}
