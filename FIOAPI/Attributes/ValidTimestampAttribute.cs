﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// ValidTimestampAttribute - An attribute to ensure the DateTime is within a valid range
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ValidTimestampAttribute : Attribute
    {
        /// <summary>
        /// ValidTimestampAttribute constructor
        /// </summary>
        public ValidTimestampAttribute() { }
    }
}
