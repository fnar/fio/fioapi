﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to indicate that the string is a FX ticker `XXX/YYY`
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class FXTickerAttribute : Attribute
    {
        /// <summary>
        /// FXTickerAttribute Constructor
        /// </summary>
        public FXTickerAttribute()
        {

        }
    }
}
