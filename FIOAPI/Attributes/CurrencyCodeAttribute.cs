﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to enforce the string is a currency code
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CurrencyCodeAttribute : Attribute
    {
        /// <summary>
        /// CurrencyCodeAttribute Constructor
        /// </summary>
        public CurrencyCodeAttribute()
        {

        }
    }
}
