﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to specify how many entries a container should have
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ContainerLengthAttribute  : Attribute
    {
        /// <summary>
        /// ContainerLength constructor
        /// </summary>
        /// <param name="length">Number of entries</param>
        /// <param name="minimumLength">[Optional] minimum length of entries</param>
        public ContainerLengthAttribute(int length, int minimumLength = -1)
        {
            ContainerLength = length;
            MinimumContainerLength = (minimumLength >= 0) ? minimumLength : ContainerLength;
        }

        /// <summary>
        /// ContainerLength
        /// </summary>
        public int ContainerLength { get; private set; }

        /// <summary>
        /// MinimumContainerLength
        /// </summary>
        public int MinimumContainerLength { get; private set; }
    }
}
