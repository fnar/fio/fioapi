﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to enforce the number is positive
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PositiveNumberAttribute : Attribute
    {
        /// <summary>
        /// PositiveNumberAttribute Constructor
        /// </summary>
        public PositiveNumberAttribute() { }
    }
}
