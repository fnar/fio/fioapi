﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to indicate that the provided string is a valid workforce level.
    /// Workforce levels are the following:
    /// - PIONEER
    /// - SETTLER
    /// - TECHNICIAN
    /// - ENGINEER
    /// - SCIENTIST
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class WorkforceLevelAttribute : Attribute
    {
        /// <summary>
        /// WorkforceLevelAttribute constructor
        /// </summary>
        public WorkforceLevelAttribute() { }
    }
}
