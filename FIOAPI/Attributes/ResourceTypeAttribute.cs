﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute signifying that this string should be a resource type.
    /// Currently supported values:
    /// - LIQUID
    /// - MINERAL
    /// - GASEOUS
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class ResourceTypeAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ResourceTypeAttribute() { }
    }
}
