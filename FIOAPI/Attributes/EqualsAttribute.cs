﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to specify what the ensure the value of a property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class EqualsAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="Value">The expected value</param>
        /// <param name="Type">Optional type for the provided value. Effectively sets the object to (type)value</param>
        /// <param name="OffsetIndex">The offset index into an array or list</param>
        /// <remarks>Note that issues can arise with non-integer/non-double number types and you will have to either specify the type or cast the value explicitly. Example: (long)42</remarks>
        public EqualsAttribute(object? Value, Type? Type = null, int OffsetIndex = -1)
        {
            if (Value != null && Type != null)
            {
                this.Value = Convert.ChangeType(Value, Type);
            }
            else
            {
                this.Value = Value;
            }

            this.OffsetIndex = OffsetIndex;
        }

        /// <summary>
        /// The object's value
        /// </summary>
        public object? Value { get; private set; }

        /// <summary>
        /// The offset index into an array or list
        /// </summary>
        public int OffsetIndex { get; private set; }
    }
}
