﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute signifying that this string should be a natural ID.
    /// A natural id can be either of the following:
    /// - Two uppercase letters, followed by a hyphen, followed by 3 digits, followed by a lowercase letter
    /// - Three uppercase letters (in the case of stations)
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class NaturalIDAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NaturalIDAttribute()
        {

        }
    }
}
