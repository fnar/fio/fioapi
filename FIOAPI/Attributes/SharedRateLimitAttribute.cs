﻿using Microsoft.AspNetCore.Mvc.Routing;

namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to signify that the following endpoint should be rate-limited in a shared manner
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class SharedRateLimitAttribute : Attribute
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public SharedRateLimitAttribute() { }

        /// <summary>
        /// SharedRateLimits
        /// </summary>
        public static HashSet<SharedRateLimitDef> SharedRateLimits
        {
            get
            {
                if (_SharedRateLimits == null)
                {
                    _SharedRateLimits = new HashSet<SharedRateLimitDef>();

                    var ControllerTypes = AppDomain.CurrentDomain.GetAssemblies()
                        .SelectMany(assembly => assembly.GetTypes())
                        .Where(t => t.IsSubclassOf(typeof(ControllerBase)) && t.GetCustomAttributes(typeof(ApiControllerAttribute), true).Any())
                        .ToList();

                    foreach (var ControllerType in ControllerTypes)
                    {
                        var routeAttr = ControllerType.GetCustomAttributes(typeof(RouteAttribute), true).FirstOrDefault() as RouteAttribute;
                        if (routeAttr != null)
                        {
                            string BaseRoute = routeAttr.Template.TrimStart('/').TrimEnd('/');

                            // Now, find each method with a SharedRateLimitAttribute
                            var ControllerMethods = ControllerType.GetMethods()
                                .Where(m => m.GetCustomAttributes(typeof(SharedRateLimitAttribute), true).Any())
                                .ToList();
                            foreach (var ControllerMethod in ControllerMethods)
                            {
                                // Grab the sub-route
                                var httpMethodAttr = ControllerMethod.GetCustomAttributes(true).Where(attr => attr is HttpMethodAttribute).FirstOrDefault() as HttpMethodAttribute;
                                if (httpMethodAttr != null && httpMethodAttr.Template != null)
                                {
                                    var SubRoute = httpMethodAttr.Template.TrimStart('/').TrimEnd('/');

                                    var CombinedRoute = $"{BaseRoute}/{SubRoute}";

                                    if (httpMethodAttr is HttpPostAttribute)
                                    {
                                        _SharedRateLimits.Add(new SharedRateLimitDef { Method = "POST", Route = CombinedRoute });
                                    }
                                    else if (httpMethodAttr is HttpPutAttribute)
                                    {
                                        _SharedRateLimits.Add(new SharedRateLimitDef { Method = "PUT", Route = CombinedRoute });
                                    }
                                    else if (httpMethodAttr is HttpDeleteAttribute)
                                    {
                                        _SharedRateLimits.Add(new SharedRateLimitDef { Method = "DELETE", Route = CombinedRoute });
                                    }
                                    else if (httpMethodAttr is HttpPatchAttribute)
                                    {
                                        _SharedRateLimits.Add(new SharedRateLimitDef { Method = "PATCH", Route = CombinedRoute });
                                    }
                                }
                            }
                        }
                    }
                }

                return _SharedRateLimits;
            }
        }

        private static HashSet<SharedRateLimitDef>? _SharedRateLimits = null;
    }

    /// <summary>
    /// RateLimitDef
    /// </summary>
    public class SharedRateLimitDef
    {
        /// <summary>
        /// Method
        /// </summary>
        public string Method { get; set; } = "POST";

        /// <summary>
        /// Route
        /// </summary>
        public string Route { get; set; } = null!;

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public bool Equals(SharedRateLimitDef other)
        {
            return Method == other.Method && Route == other.Route;
        }

        /// <summary>
        /// Equals
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object? obj)
        {
            var def = obj as SharedRateLimitDef;
            if (def == null)
            {
                return false;
            }
            else
            {
                return Equals(def);
            }
        }

        /// <summary>
        /// GetHashCode
        /// </summary>
        /// <returns>HashCode</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(Method, Route);
        }
    }
}
