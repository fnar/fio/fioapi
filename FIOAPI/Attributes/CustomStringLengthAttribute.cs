﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// CustomStringLengthAttribute - An attribute to signify that we should allow overriding the default string lengths
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class CustomStringLengthAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CustomStringLengthAttribute()
        {

        }
    }
}
