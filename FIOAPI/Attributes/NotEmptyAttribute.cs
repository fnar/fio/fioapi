﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to enforce the string is not the empty string
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class NotEmptyAttribute : Attribute
    {
        /// <summary>
        /// NotEmpty Constructor
        /// </summary>
        public NotEmptyAttribute() { }
    }
}
