﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to signify this property shouldn't be validated.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class NoValidateAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public NoValidateAttribute()
        { }
    }
}
