﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// RequiredPermissionAttribute - An attribute that indicates a class/properties required permission
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Property, AllowMultiple = false)]
    public class RequiredPermissionAttribute : Attribute
    {
        /// <summary>
        /// RequiredPermissionAttribute constructor
        /// </summary>
        /// <param name="perm">Permission enum flag</param>
        public RequiredPermissionAttribute(Perm perm)
        {
            this.perm = perm;
        }

        /// <summary>
        /// The Perm
        /// </summary>
        public Perm perm
        {
            get; private set;
        }
    }
}
