﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// An attribute to enforce the string is a password hash
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class PasswordHashAttribute : Attribute
    {
        /// <summary>
        /// Password hash attribute constructor
        /// </summary>
        public PasswordHashAttribute() { }
    }
}
