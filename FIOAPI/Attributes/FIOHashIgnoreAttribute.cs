﻿namespace FIOAPI.Attributes
{
    /// <summary>
    /// FIOHashIgnoreAttribute - An attribute to use to indicate that the provided field should be ignored for HashCode determination
    /// </summary>
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class FIOHashIgnoreAttribute : Attribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public FIOHashIgnoreAttribute()
        {

        }
    }
}
