﻿using Dijkstra.NET.Graph;
using Dijkstra.NET.ShortestPath;
using FIOAPI.DB.Model;
using System.Data;
using System.Xml.Linq;

namespace FIOAPI
{
    /// <summary>
    /// 
    /// </summary>
    public static class JumpCalculator
    {
        private static object _lock = new();
        private static Dictionary<string, uint>? SystemIdToGraphId;
        private static Graph<string, int>? graph;

        internal class JumpCount : ICloneable
        {
            public int Count { get; set; }

            public object Clone()
            {
                return new JumpCount()
                {
                    Count = Count
                };
            }
        }

        private static MRUCache<string, JumpCount> JumpCountCache = new(65536); // This should be about 6 MiB

        /// <summary>
        /// If the jump calculator is initialized
        /// </summary>
        public static bool IsInitialized
        {
            get
            {
                return graph != null;
            }
        }

        /// <summary>
        /// Populates the JumpCalculator with system data
        /// </summary>
        public static void Initialize()
        {
            var Systems = EntityCaches.SystemCache.AllEntitiesCopy;
            if (Systems.Count == 0)
                return;

            lock (_lock)
            {
                SystemIdToGraphId = new Dictionary<string, uint>();
                graph = new Graph<string, int>();

                foreach (var system in Systems)
                {
                    var sysId = system.SystemId;

                    uint nodeId = 0;
                    if (!SystemIdToGraphId.ContainsKey(sysId))
                    {
                        nodeId = graph.AddNode(sysId);
                        SystemIdToGraphId.Add(sysId, nodeId);
                    }
                    else
                    {
                        nodeId = SystemIdToGraphId[sysId];
                    }

                    foreach (var connection in system.Connections)
                    {
                        uint connectionNodeId;
                        if (!SystemIdToGraphId.ContainsKey(connection))
                        {
                            connectionNodeId = graph.AddNode(connection);
                            SystemIdToGraphId.Add(connection, connectionNodeId);
                        }
                        else
                        {
                            connectionNodeId = SystemIdToGraphId[connection];
                        }

                        var connectingStar = EntityCaches.SystemCache.Get(connection);
                        if (connectingStar != null)
                        {
                            // Distance formula in 3D
                            double distance = Math.Sqrt(Math.Pow((system.PositionX - connectingStar.PositionX), 2) + Math.Pow((system.PositionY - connectingStar.PositionY), 2) + Math.Pow((system.PositionZ - connectingStar.PositionZ), 2));

                            // Since Dijkstra.Graph only takes ints, multiply by 1000 and truncate
                            int dijCost = (int)(distance * 1000.0);
                            graph.Connect(nodeId, connectionNodeId, dijCost, dijCost);
                        }
                    }
                }
            }
        }

        private static JumpCache? BuildJumpCacheRoute(string SourceSystemId, string DestinationSystemId)
        {
            if (!IsInitialized)
                throw new InvalidOperationException("Not initialized");

            JumpCache? JumpCacheResult;
            List<JumpCacheRouteJump> route = new();
            lock(_lock)
            {
                uint SourceNodeId = SystemIdToGraphId![SourceSystemId];
                uint DestinationNodeId = SystemIdToGraphId![DestinationSystemId];

                var routeNodeIds = graph.Dijkstra(SourceNodeId, DestinationNodeId).GetPath().ToList();
                for (int routeNodeIter = 0; routeNodeIter + 1 < routeNodeIds.Count; ++routeNodeIter)
                {
                    var sourceNodeId = routeNodeIds[routeNodeIter];
                    var sourceSysId = SystemIdToGraphId.FirstOrDefault(x => x.Value == sourceNodeId).Key;
                    var sourceSystem = Caches.EntityCaches.SystemCache.Get(sourceSysId);

                    var destNodeId = routeNodeIds[routeNodeIter + 1];
                    var destSysId = SystemIdToGraphId.FirstOrDefault(x => x.Value == destNodeId).Key;
                    var destSystem = Caches.EntityCaches.SystemCache.Get(destSysId);

                    if (sourceSystem == null || destSystem == null)
                    {
                        return null;
                    }

                    JumpCacheRouteJump desc = new JumpCacheRouteJump();
                    desc.SourceSystemId = sourceSysId;
                    desc.SourceSystemName = sourceSystem.Name;
                    desc.SourceSystemNaturalId = sourceSystem.NaturalId;
                    desc.DestinationSystemId = destSysId;
                    desc.DestinationSystemName = destSystem.Name;
                    desc.DestinationNaturalId = destSystem.NaturalId;
                    desc.Distance = Math.Sqrt(Math.Pow((sourceSystem.PositionX - destSystem.PositionX), 2) + Math.Pow((sourceSystem.PositionY - destSystem.PositionY), 2) + Math.Pow((sourceSystem.PositionZ - destSystem.PositionZ), 2));

                    route.Add(desc);
                }

                JumpCacheResult = new JumpCache();
                if (route.Count > 0)
                {
                    var start = route[0];
                    var destination = route[route.Count - 1];

                    JumpCacheResult.JumpCacheId = $"{start.SourceSystemId}-{destination.DestinationSystemId}";
                    JumpCacheResult.SourceSystemId = start.SourceSystemId;
                    JumpCacheResult.SourceSystemName = start.SourceSystemName;
                    JumpCacheResult.SourceSystemNaturalId = start.SourceSystemNaturalId;
                    JumpCacheResult.DestinationSystemId = destination.DestinationSystemId;
                    JumpCacheResult.DestinationSystemName = destination.DestinationSystemName;
                    JumpCacheResult.DestinationNaturalId = destination.DestinationNaturalId;

                    JumpCacheResult.OverallDistance = 0.0;
                    JumpCacheResult.JumpCount = route.Count;

                    foreach (var routeEntry in route)
                    {
                        var jump = new JumpCacheRouteJump();
                        jump.JumpCacheRouteJumpId = $"{JumpCacheResult.JumpCacheId}-{routeEntry.SourceSystemId}-{routeEntry.DestinationSystemId}";
                        jump.SourceSystemId = routeEntry.SourceSystemId;
                        jump.SourceSystemName = routeEntry.SourceSystemName;
                        jump.SourceSystemNaturalId = routeEntry.SourceSystemNaturalId;
                        jump.DestinationSystemId = routeEntry.DestinationSystemId;
                        jump.DestinationSystemName = routeEntry.DestinationSystemName;
                        jump.DestinationNaturalId = routeEntry.DestinationNaturalId;
                        jump.Distance = routeEntry.Distance;
                        jump.JumpCacheId = JumpCacheResult.JumpCacheId;

                        JumpCacheResult.OverallDistance += jump.Distance;

                        JumpCacheResult.Jumps.Add(jump);
                    }

                    List<string> Errors = new();
                    if (!JumpCacheResult.Validate(ref Errors))
                    {
                        throw new InvalidOperationException(string.Join(Environment.NewLine, Errors));
                    }
                }
            }

            return JumpCacheResult;
        }

        /// <summary>
        /// Retrieves the number of jumps between two system ids
        /// </summary>
        /// <param name="Source">Source system id</param>
        /// <param name="Destination">Destination system id</param>
        /// <param name="access">DBAccess object</param>
        /// <returns>number of jumps</returns>
        public static async Task<int> GetJumpCount(string Source, string Destination, DBAccess? access = null)
        {
#if DEBUG
            var SourceAndDest = new List<string> { Source, Destination };
            if (!EntityCaches.ConvertToSystemIds(ref SourceAndDest, out _))
            {
                throw new InvalidOperationException("GetJumpCount not called with SystemIds");
            }
#endif

            var CacheEntryKeyOption1 = $"{Source}{Destination}";
            var CacheEntryKeyOption2 = $"{Destination}{Source}";
            var jc = JumpCountCache.Get(CacheEntryKeyOption1);
            jc ??= JumpCountCache.Get(CacheEntryKeyOption2);
            if (jc != null)
            {
                return jc.Count;
            }

            var route = await GetRoute(Source, Destination, access);
            JumpCountCache.Set(CacheEntryKeyOption1, new JumpCount { Count = route.Count });
            return route.Count;
        }

        /// <summary>
        /// Gets the route given a source system and destination system
        /// </summary>
        /// <param name="Source">The SourceSystemId</param>
        /// <param name="Destination">The DestinationSystemId</param>
        /// <param name="access">DBAccess optional parameter</param>
        /// <returns>A list of JumpCacheRouteJump</returns>
        public static async Task<List<JumpCacheRouteJump>> GetRoute(string Source, string Destination, DBAccess? access = null)
        {
            var SourceAndDest = new List<string> { Source, Destination };
#if DEBUG
            if (!EntityCaches.ConvertToSystemIds(ref SourceAndDest, out _))
            {
                throw new InvalidOperationException("GetRoute not called with SystemIds");
            }
#endif

            var SourceSystemId = SourceAndDest[0];
            var DestinationSystemId = SourceAndDest[1];

            List<JumpCacheRouteJump> Jumps = new();

            if (SourceSystemId == DestinationSystemId)
                return Jumps;

            DBAccess writer = access ?? DBAccess.GetWriter();
            try
            {
                var JumpCacheResult = await writer.DB.JumpCache
                    .Include(jc => jc.Jumps)
                    .Where(jc => jc.SourceSystemId == SourceSystemId)
                    .Where(jc => jc.DestinationSystemId == DestinationSystemId)
                    .FirstOrDefaultAsync();
                if (JumpCacheResult == null)
                {
                    if (writer.DB.ShouldRestrictSaves)
                    {
                        throw new InvalidOperationException("Provided with DBAccess.Reader");
                    }

                    JumpCacheResult = BuildJumpCacheRoute(SourceSystemId, DestinationSystemId);
                    if (JumpCacheResult != null)
                    {
                        await writer.DB.JumpCache.AddAsync(JumpCacheResult);
                        await writer.DB.SaveChangesAsync();
                    }
                }

                if (JumpCacheResult != null)
                {
                    Jumps = JumpCacheResult.Jumps;
                }
            }
            finally
            {
                if (access == null)
                {
                    writer.Dispose();
                }
            }

            return Jumps;
        }
    }
}
