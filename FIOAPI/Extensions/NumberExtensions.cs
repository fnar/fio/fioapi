﻿using System.Collections.Generic;

namespace FIOAPI.Extensions
{
    /// <summary>
    /// Number extensions
    /// </summary>
    public static class NumberExtensions
    {
        const double DOUBLE_SMALL_NUMBER = 0.00001;
        const float FLOAT_SMALL_NUMBER = 0.00001f;

        /// <summary>
        /// If the two values are about equal
        /// </summary>
        /// <param name="left">Left-side number</param>
        /// <param name="right">Right-side number</param>
        /// <returns>true if about equal</returns>
        public static bool AboutEquals(this double left, double right)
        {
            return Math.Abs(left - right) < DOUBLE_SMALL_NUMBER;
        }

        /// <summary>
        /// If the two values are about equal
        /// </summary>
        /// <param name="left">Left-side number</param>
        /// <param name="right">Right-side number</param>
        /// <returns>true if about equal</returns>
        public static bool AboutEquals(this float left, float right)
        {
            return Math.Abs(left - right) < FLOAT_SMALL_NUMBER;
        }

        /// <summary>
        /// Rounds the provided value to the specified number of decimal places
        /// </summary>
        /// <param name="value">The value</param>
        /// <param name="decimalPlaces">Number of decimal places</param>
        /// <returns>The resultant value</returns>
        public static double RoundToDecimalPlaces(this double value, int decimalPlaces)
        {
            return Math.Round(value, decimalPlaces, MidpointRounding.AwayFromZero);
        }

        /// <summary>
        /// Rounds the provided value to the specified number of decimal places
        /// </summary>
        /// <param name="value">The value</param>
        /// <param name="decimalPlaces">Number of decimal places</param>
        /// <returns>The resultant value</returns>
        public static double? RoundToDecimalPlaces(this double? value, int decimalPlaces)
        {
            if (value == null) return null;
            return RoundToDecimalPlaces((double)value, decimalPlaces);
        }

		/// <summary>
		/// Rounds the provided value for display purposes
		/// </summary>
		/// <param name="value">The value></param>
		/// <returns>The resultant value</returns>
		/// <exception cref="InvalidOperationException">Somehow managed to not round for display [shouldn't happen]</exception>
		public static double RoundForDisplay(this double value)
        {
            const int StartDecimalPlaces = 2;
            const int EndDecimalPlaces = 6;

			// First, round to EndDecimalPlaces.  If we're zero, return zero.
			double SixSigValue = Math.Round((double)value, EndDecimalPlaces, MidpointRounding.AwayFromZero);
			if (SixSigValue == 0.0)
			{
				return 0.0;
			}

			// If we're not 0, we try StartDecimalPlaces first.
			// If the result is zero, keep trying up to EndDecimalPlaces.
			for (int DecimalPlaces = StartDecimalPlaces; DecimalPlaces <= EndDecimalPlaces; ++DecimalPlaces)
			{
				var ResValue = Math.Round((double)value, DecimalPlaces, MidpointRounding.AwayFromZero);
				if (ResValue != 0.0)
				{
					return ResValue;
				}
			}

			throw new InvalidOperationException("Somehow managed to not round for display");
		}

		/// <summary>
		/// Rounds the provided value for display purposes
		/// </summary>
		/// <param name="value">The value></param>
		/// <returns>The resultant value</returns>
		/// <exception cref="InvalidOperationException">Somehow managed to not round for display [shouldn't happen]</exception>
		public static double? RoundForDisplay(this double? value)
        {
            if (value == null) return null;
            return RoundForDisplay((double)value);
        }

        /// <summary>
        /// If the provided object can be a whole number (number w/o decimals)
        /// </summary>
        /// <param name="value">The object to check</param>
        /// <returns>true if whole number, false otherwise</returns>
        public static bool IsWholeNumber(this object value)
        {
            return value is sbyte
                || value is byte
                || value is short
                || value is ushort
                || value is int
                || value is uint
                || value is long
                || value is ulong;
        }

        /// <summary>
        /// If the provided object can be a number with decimals
        /// </summary>
        /// <param name="value">The object to check</param>
        /// <returns>true if decimal number, false otherwise</returns>
        public static bool IsDecimalNumber(this object value)
        {
            return IsWholeNumber(value) 
                || value is float
                || value is double
                || value is decimal;
        }
    }
}
