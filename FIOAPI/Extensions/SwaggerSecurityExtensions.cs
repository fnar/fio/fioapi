﻿namespace FIOAPI.Extensions
{
    /// <summary>
    /// SwaggerSecurityExtension
    /// </summary>
    public static class SwaggerSecurityExtensions
    {
        /// <summary>
        /// AddFIOSwaggerSecurityOptions
        /// </summary>
        /// <param name="services">services</param>
        /// <returns>IServiceCollection</returns>
        public static IServiceCollection AddFIOSwaggerSecurityOptions(this IServiceCollection services)
        {
            var jwtSecurityScheme = new OpenApiSecurityScheme()
            {
                Description = "Authorization header using Bearer scheme. Example: <br/><br/>`Authorization: Bearer {token}`<br/></br/>**NOTE:** When authenticating through the documentation page, do not include 'Bearer'.",
                Name = "Authorization",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.Http,
                Scheme = "bearer",
                BearerFormat = "JWT"
            };

            var jwtSecurityRequirement = new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    System.Array.Empty<string>()
                }
            };

            var apiKeySecurityScheme = new OpenApiSecurityScheme()
            {
                Name = "Authorization",
                Description = "Authorization header using FIOAPIKey scheme. <br/>Example: <br/><br/>`Authorization: FIOAPIKey {APIKey}`<br/><br/>**NOTE:** When authenticating through the documentation page, make sure the input is `FIOAPIKey {Key}`.",
                In = ParameterLocation.Header,
                Type = SecuritySchemeType.ApiKey,
                Scheme = APIKeyAuthDefaults.AuthenticationScheme,
            };

            var apiKeySecurityRequirement = new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = APIKeyAuthDefaults.AuthenticationScheme
                        }
                    },
                    System.Array.Empty<string>()
                }
            };

            services.AddSwaggerGen(options =>
            {
                options.AddSecurityDefinition("bearer", jwtSecurityScheme);
                options.AddSecurityDefinition(APIKeyAuthDefaults.AuthenticationScheme, apiKeySecurityScheme);
                options.OperationFilter<SecurityRequirementsOperationFilter>();
            });

            return services;
        }
    }

    internal class SecurityRequirementsOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (context != null && operation != null)
            {
                // AuthenticationSchemes map to scopes
                // for class level authentication schemes
                var classRequiredScopes = context.MethodInfo.DeclaringType!
                        .GetCustomAttributes(true)
                        .OfType<AuthorizeAttribute>()
                        .Distinct();

                // for method level authentication scheme
                var methodRequiredScopes = context.MethodInfo
                        .GetCustomAttributes(true)
                        .OfType<AuthorizeAttribute>()
                        .Distinct();

                var methodAnonymousScopes = context.MethodInfo
                    .GetCustomAttributes(true)
                    .OfType<AllowAnonymousAttribute>()
                    .Distinct();

                bool requireAuth = classRequiredScopes.Any() || methodRequiredScopes.Any();
                if (methodAnonymousScopes.Any())
                {
                    // Explicitly disable auth if we allow anonymous on that specific method
                    requireAuth = false;
                }

                if (requireAuth)
                {
                    operation.Security = new List<OpenApiSecurityRequirement>
                    {
                        new OpenApiSecurityRequirement
                        {
                            {
                                new OpenApiSecurityScheme
                                {
                                    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "bearer" }
                                },
                                new[] { "bearer" }
                            },
                            {
                                new OpenApiSecurityScheme
                                {
                                    Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = APIKeyAuthDefaults.AuthenticationScheme }
                                },
                                new[] { APIKeyAuthDefaults.AuthenticationScheme }
                            }
                        }
                    };
                }
            }
        }
    }
}
