﻿namespace FIOAPI.Extensions
{
    /// <summary>
    /// Helper extensions to retrieve attribute values from properties
    /// </summary>
    public static class AttributeExtensions
    {
        private static AttributeType GetAttribute<AttributeType>(Type type, string Property) where AttributeType : Attribute
        {
            var property = type.GetProperty(Property);
            if (property == null) throw new InvalidOperationException($"Failed to find Property {(Property)} on {type.Name}");

            var sla = property.GetCustomAttribute(typeof(AttributeType)) as AttributeType;
            if (sla == null) throw new InvalidOperationException($"Failed to find {typeof(AttributeType).Name} attribute on {type.Name}.{Property}");

            return sla;
        }

        /// <summary>
        /// Retrieves the minimum and maximum string lengths given a type and property name
        /// </summary>
        /// <param name="type">The type</param>
        /// <param name="Property">The property's name</param>
        /// <param name="MinLength">[Out] MinLength</param>
        /// <param name="MaxLength">[Out] MaxLength</param>
        public static void MinMaxStringLengths(Type type, string Property, out int MinLength, out int MaxLength)
        {
            var sla = GetAttribute<StringLengthAttribute>(type, Property);
            MinLength = sla.MinimumLength;
            MaxLength = sla.MaximumLength;
        }
    }
}
