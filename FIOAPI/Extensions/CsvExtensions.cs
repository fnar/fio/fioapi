﻿using CsvHelper;
using System.Globalization;

namespace FIOAPI.Extensions
{
    /// <summary>
    /// CsvExtensions
    /// </summary>
    public static class CsvExtensions
    {
        private static void WriteCSVHeader<T>(CsvWriter csv, List<T> csvObjects) where T : ICsvObject, new()
        {
            csv.WriteHeader<T>();
            csv.NextRecord();
        }

        private static void WriteCSVOutput<T>(CsvWriter csv, List<T> csvObjects) where T : ICsvObject, new()
        {
            foreach (var csvObject in csvObjects)
            {
                csv.WriteRecord(csvObject);
                csv.NextRecord();
            }
        }

        /// <summary>
        /// ToCSVResponse - Converts a list of ICSVObject to a CSV ContentResult
        /// </summary>
        /// <typeparam name="T">An ICSVObject type</typeparam>
        /// <param name="csvObjects">The CSVObjects</param>
        /// <param name="IncludeHeader">If the csv header should be emitted</param>
        /// <returns>A content result w/ the CSV data</returns>
        public static ActionResult GetCSVResponse<T>(this List<T> csvObjects, bool IncludeHeader = true) where T: ICsvObject, new()
        {
            var sb = new StringBuilder();
            using (var sw = new StringWriter(sb))
            using (var csv = new CsvWriter(sw, CultureInfo.InvariantCulture))
            {
                if (IncludeHeader)
                {
                    WriteCSVHeader(csv, csvObjects);
                }

                WriteCSVOutput(csv, csvObjects);
            }

            return new ContentResult()
            {
                Content = sb.ToString(),
                ContentType = "text/csv",
                StatusCode = 200
            };
        }
    }
}
