﻿namespace FIOAPI.Extensions
{
    /// <summary>
    /// HashCodeExtensions
    /// </summary>
    public static class HashCodeExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        public static Dictionary<Type, PropertyInfo> HashCodeTypeToKeyPropertyInfo { get; private set; } = new();

        static HashCodeExtensions()
        {
            var AllValidationClasses = ReflectionHelpers.AllClassTypes
                .Where(c => c.IsSubclassOf(typeof(IValidation)))
                .ToList();

            foreach (var ValidationClass in AllValidationClasses)
            {
                var KeyProp = ValidationClass
                    .GetProperties()
                    .FirstOrDefault(pi => pi.GetCustomAttribute<KeyAttribute>() != null);
                if (KeyProp != null)
                {
                    HashCodeTypeToKeyPropertyInfo.Add(ValidationClass, KeyProp);
                }
            }
        }

        /// <summary>
        /// Retrieves the HashCode for the provided IFIOHash object
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="obj">The object</param>
        /// <returns>The HashCode</returns>
        public static int GetFIOHashCode<T>(this T obj) where T : IValidation
        {
            HashCode hc = new();
            HashSet<object> ProcessedObjects = new();

            GetFIOHashCode_Internal(obj, ref hc, ref ProcessedObjects);
            return hc.ToHashCode();
        }

        private static void GetFIOHashCode_Internal(this object obj, ref HashCode hc, ref HashSet<object> ProcessedObjects)
        {
            if (ProcessedObjects.Contains(obj))
                return;

            ProcessedObjects.Add(obj);

            var propInfos = obj.GetType().GetProperties();
            foreach (var propInfo in propInfos)
            {
                bool HasFIOHashIgnoreAttribute = propInfo.GetCustomAttribute<FIOHashIgnoreAttribute>() != null;
                bool HasNotMappedAttribute = propInfo.GetCustomAttribute<NotMappedAttribute>() != null;
                bool HasJsonIgnoreAttribute = propInfo.GetCustomAttribute<JsonIgnoreAttribute>() != null;
                bool ShouldIgnore = HasFIOHashIgnoreAttribute || HasNotMappedAttribute || HasJsonIgnoreAttribute;
                if (ShouldIgnore)
                {
                    continue;
                }

                object? subObj = propInfo.GetValue(obj);
                bool IsCollection = subObj is ICollection;
                bool IsArray = propInfo.PropertyType.IsArray;

                if (IsArray || IsCollection)
                {
                    IEnumerable? items = subObj as IEnumerable;
                    if (items == null)
                    {
                        hc.Add(items);
                    }
                    else
                    {
                        var list = items.Cast<object>().ToList();
                        for (var listIdx = 0; listIdx < list.Count; ++listIdx)
                        {
                            if (list[listIdx] is IValidation validationIndexObj)
                            {
                                validationIndexObj.GetFIOHashCode_Internal(ref hc, ref ProcessedObjects);
                            }
                            else
                            {
                                hc.Add(list[listIdx] != null ? list[listIdx].GetHashCode() : 0);
                            }
                        }
                    }
                }
                else
                {
                    if (subObj is IValidation validationObj)
                    {
                        validationObj.GetFIOHashCode_Internal(ref hc, ref ProcessedObjects);
                    }
                    else
                    {
                        hc.Add(subObj != null ? subObj.GetHashCode() : 0);
                    }
                }
            }
        }
    }
}
