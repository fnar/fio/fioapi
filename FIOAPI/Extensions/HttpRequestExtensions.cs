﻿namespace FIOAPI.Extensions
{
    /// <summary>
    /// HttpRequest extensions
    /// </summary>
    public static class HttpRequestExtensions
    {
        /// <summary>
        /// Retrives the HttpRequest body asynchronously
        /// </summary>
        /// <param name="request">The request</param>
        /// <returns>The body string</returns>
        public static async Task<string> GetBodyAsync(this HttpRequest request)
        {
            // Set the stream body position to 0
            request.Body.Position = 0;

            var bodyStr = "";
            using (var reader = new StreamReader(request.Body, Encoding.UTF8, true, 4096, true))
            {
                bodyStr = await reader.ReadToEndAsync();
            }

            // Reset the stream body position to 0
            request.Body.Position = 0;

            return bodyStr;
        }

        /// <summary>
        /// Retrieves the HttpRequest body
        /// </summary>
        /// <param name="request">The request</param>
        /// <returns>The body string</returns>
        public static string GetBody(this HttpRequest request)
        {
            if (request.Body.Length == 0)
                return "";

            // Set the stream body position to 0
            request.Body.Position = 0;

            var bodyStr = "";
            using (var reader = new StreamReader(request.Body, Encoding.UTF8, true, 4096, true))
            {
                bodyStr = reader.ReadToEnd();
            }

            // Reset the stream body position to 0
            request.Body.Position = 0;

            return bodyStr;
        }
    }
}
