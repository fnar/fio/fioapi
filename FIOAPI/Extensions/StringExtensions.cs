﻿namespace FIOAPI.Extensions
{
    /// <summary>
    /// Helper string extensions
    /// </summary>
    public static class StringExtensions
    {
        /// <summary>
        /// Conditionally wraps the given string in quotation marks
        /// </summary>
        /// <param name="str">the string to wrap</param>
        /// <returns>the resultant quoted string</returns>
        public static string QuoteWrap(this string str)
        {
            if (!str.StartsWith("\"") || !str.EndsWith("\""))
            {
                return $"\"{str}\"";
            }

            return str;
        }

        /// <summary>
        /// GetGroupIdFromQueryParam
        /// </summary>
        /// <param name="str">the string to get the groupid for</param>
        /// <param name="GroupId">[out] GroupId</param>
        /// <returns>true if parsed correctly</returns>
        public static bool GetGroupIdFromQueryParam(this string str, out int GroupId)
        {
            return GetGroupParts(str, out _, out GroupId);
        }

        /// <summary>
        /// GetGroupParts
        /// </summary>
        /// <param name="str">the string to get group parts for</param>
        /// <param name="GroupName">[out] GroupName</param>
        /// <param name="GroupId">[out] GroupId</param>
        /// <returns>true if parsed correctly</returns>
        public static bool GetGroupParts(this string str, out string GroupName, out int GroupId)
        {
            GroupName = "";
            GroupId = 0;

            var parts = str.Split('-', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries);
            if (parts.Length == 1)
            {
                if (int.TryParse(parts[0], out GroupId))
                {
                    // Allow just the GroupId
                    return true;
                }
            }
            else if (parts.Length == 2)
            {
                if (int.TryParse(parts[1], out GroupId))
                {
                    GroupName = parts[0];
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// An enum representing the type of NaturalId
        /// </summary>
        public enum NaturalIdType
        {
            /// <summary>
            /// InvalidNaturalId
            /// </summary>
            InvalidNaturalId,

            /// <summary>
            /// Station
            /// </summary>
            Station,

            /// <summary>
            /// System
            /// </summary>
            System,

            /// <summary>
            /// Planet
            /// </summary>
            Planet
        }

        private const string SampleStationNaturalId = "HRT";
        private const string SampleLongNaturalIdSystem = "ZZ-123";
        private const string SampleLongNaturalIdPlanet = "ZZ-123a";

        private static List<int> ValidNaturalIdLengths = new List<int>
        {
            SampleStationNaturalId.Length,
            SampleLongNaturalIdSystem.Length,
            SampleLongNaturalIdPlanet.Length
        };

        private const string LongNaturalIdRegexPattern = @"^[A-Z]{2}-[0-9]{3}(?<PlanetCharacter>[a-z]?)$";
        private static Regex LongNaturalIdRegex = new Regex(LongNaturalIdRegexPattern, RegexOptions.Compiled);
        private static Regex LongNaturalIdRegexIgnoreCase = new Regex(LongNaturalIdRegexPattern, RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Gets the natural id type from a provided string
        /// </summary>
        /// <param name="str">The string</param>
        /// <param name="IgnoreCase">If we should ignore case</param>
        /// <returns>NaturalIdType</returns>
        public static NaturalIdType GetNaturalIdType(this string str, bool IgnoreCase = false)
        {
            if (string.IsNullOrWhiteSpace(str))
            {
                return NaturalIdType.InvalidNaturalId;
            }

            if (!string.IsNullOrWhiteSpace(str) && ValidNaturalIdLengths.Contains(str.Length))
            {
                if (str.Length == SampleStationNaturalId.Length)
                {
                    if (IgnoreCase)
                        str = str.ToUpper();

                    if (str.All(ch => ch >= 'A' && ch <= 'Z'))
                    {
                        return NaturalIdType.Station;
                    }
                }
                else
                {
                    MatchCollection mc = IgnoreCase ? LongNaturalIdRegexIgnoreCase.Matches(str) : LongNaturalIdRegex.Matches(str);
                    if (mc.Count == 1)
                    {
                        if (mc[0].Groups.ContainsKey("PlanetCharacter") && mc[0].Groups["PlanetCharacter"].Value.Length > 0)
                        {
                            return NaturalIdType.Planet;
                        }
                        else
                        {
                            return NaturalIdType.System;
                        }
                    }
                }
            }

            return NaturalIdType.InvalidNaturalId;
        }

        /// <summary>
        /// Splits on sep
        /// </summary>
        /// <param name="str">String to split</param>
        /// <param name="sep">Separator</param>
        /// <returns>A list of strings</returns>
        public static List<string> SplitOn(this string str, string sep)
        {
            return str
                .Split(new string[] { sep }, StringSplitOptions.RemoveEmptyEntries)
                .ToList();
        }

        /// <summary>
        /// Splits on any number of separators
        /// </summary>
        /// <param name="str">The string to split</param>
        /// <param name="groupSep">The main group separator</param>
        /// <param name="internalSeperators">Separators per group</param>
        /// <returns>A List of List of string, where the inner list is limited to internalSeparators count + 1</returns>
        public static List<List<string>> SplitOnMultiple(this string str, string groupSep, params string[] internalSeperators)
        {
            List<List<string>> Result = new();

            var GroupRes = str.Split(new string[] { groupSep }, StringSplitOptions.RemoveEmptyEntries);
            foreach (var Group in GroupRes)
            {
                string GroupStr = Group;
                List<string> GroupList = new();
                foreach (var internalSeperator in internalSeperators)
                {
                    var res = GroupStr.Split(new string[] { internalSeperator }, StringSplitOptions.RemoveEmptyEntries);
                    if (res.Length != 2)
                    {
                        throw new InvalidOperationException("Encountered invalid split result");
                    }

                    GroupList.Add(res[0]);
                    GroupStr = res[1];
                }

                GroupList.Add(GroupStr);
                Result.Add(GroupList);
            }

            return Result;
        }

        /// <summary>
        /// APIKeyValidationResult
        /// </summary>
        public class APIKeyValidationResult
        {
            /// <summary>
            /// If the key is valid
            /// </summary>
            public bool IsValid { get; set; }

            /// <summary>
            /// The username
            /// </summary>
            public string? APIKeyUserName { get; set; }

            /// <summary>
            /// If this key has write access
            /// </summary>
            public bool HasWriteAccess { get; set; }
        }

        /// <summary>
        /// Validates an APIKey and returns the owner username of the APIKey
        /// </summary>
        /// <param name="input_token">The APIKey</param>
        /// <returns>An APIKeyValidationResult</returns>
        public static async Task<APIKeyValidationResult> ValidateAPIKey(this string input_token)
        {
            Model.APIKey? apiKey = null;
            if (Guid.TryParse(input_token, out Guid ApiKeyGuid))
            {
                apiKey = APIKeyCache.Get(ApiKeyGuid);
                if (apiKey == null)
                {
                    using var reader = DBAccess.GetReader();
                    apiKey = await APIKeyCache.CacheAsync(ApiKeyGuid, reader);
                }
            }

            if (apiKey != null)
            {
                return new APIKeyValidationResult
                {
                    IsValid = true,
                    APIKeyUserName = apiKey.UserName,
                    HasWriteAccess = apiKey.AllowWrites
                };
            }
            else
            {
                return new APIKeyValidationResult
                {
                    IsValid = false
                };
            }
        }
    }

    /// <summary>
    /// A comparer for CXPC intervals
    /// </summary>
    public class CXPCIntervalComparer : IComparer<string>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public CXPCIntervalComparer()
        {
        }

        /// <summary>
        /// Compare
        /// </summary>
        /// <param name="lhs">The lhs string</param>
        /// <param name="rhs">The rhs string</param>
        /// <returns>Negative values of "earlier", 0 for equal, positive values for "later"</returns>
        public int Compare(string? lhs, string? rhs)
        {
            ArgumentNullException.ThrowIfNull(lhs);
            ArgumentNullException.ThrowIfNull(rhs);

            int lhsValue = Constants.ValidCXPCIntervals.IndexOf(lhs);
            if (lhsValue == -1)
            {
                throw new ArgumentException($"{nameof(lhs)} with value '{lhs}' is not a valid CXPC interval");
            }

            int rhsValue = Constants.ValidCXPCIntervals.IndexOf(rhs);
            if (rhsValue == -1)
            {
                throw new ArgumentException($"{nameof(rhs)} with value '{rhs}' is not a valid CXPC interval");
            }

            return lhsValue - rhsValue;
        }
    }
}
