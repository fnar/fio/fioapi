﻿using FIOAPI.Controllers;

namespace FIOAPI.Extensions
{
    /// <summary>
    /// ControllerExtensions
    /// </summary>
    public static class ControllerExtensions
    {
        private const int MaxUserNameCount = 10;

        /// <summary>
        /// Validates UserNames/Group input and retrieves Perms for each User
        /// </summary>
        /// <param name="thisController">The controller</param>
        /// <param name="UserNames">List of string usernames</param>
        /// <param name="Group">The group string</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="UserToPerm">[out] Dictionary of user to perm</param>
        /// <param name="Result">[out] ActionResult for failutres</param>
        /// <returns>true if successful, false otherwise</returns>
        public static bool CheckInputAndGetPerms(this ControllerBase thisController, List<string>? UserNames, string? Group, ref List<string> Errors, out Dictionary<string, Perm> UserToPerm, out ActionResult Result)
        {
            Errors.Clear();
            UserToPerm = new();

            var ThisUser = thisController.GetUserName()!;
            bool HaveAnyUserName = UserNames != null && UserNames.Any();
            bool HaveGroup = !string.IsNullOrWhiteSpace(Group);

            if (HaveAnyUserName && HaveGroup)
            {
                Errors.Add("Only username and group can be defined, not both");
                Result = new BadRequestObjectResult(Errors);
                return false;
            }

            if (HaveAnyUserName)
            {
                if (UserNames!.Count > MaxUserNameCount)
                {
                    Errors.Add($"Cannot specify more than {MaxUserNameCount} users");
                    Result = new BadRequestObjectResult(Errors);
                    return false;
                }

                for (int UserNameIdx = 0; UserNameIdx < UserNames!.Count; ++UserNameIdx)
                {
                    UserNames[UserNameIdx] = UserNames[UserNameIdx].ToLower();
                    ValidationExtensions.RunValidationsAsIfTypeAndProperty(UserNames[UserNameIdx], typeof(DB.Model.User), nameof(DB.Model.User.UserName), ref Errors, $"{nameof(UserNames)}[{UserNameIdx}]");
                }

                if (Errors.Count > 0)
                {
                    Result = new BadRequestObjectResult(Errors);
                    return false;
                }
            }
            else if (HaveGroup)
            {
                int GroupId;
                if (!Group!.GetGroupIdFromQueryParam(out GroupId))
                {
                    Errors.Add($"Failed to parse GroupId from {Group}");
                    Result = new BadRequestObjectResult(Errors);
                    return false;
                }

                // Make sure we're apart of this group
                if (!PermissionCache.IsUserMemberOfGroup(ThisUser, GroupId))
                {
                    Result = new ForbidResult();
                    return false;
                }

                UserNames = PermissionCache.GetUsersOfGroup(GroupId);
            }
            else
            {
                UserNames = new List<string> { ThisUser };
            }

            Result = new OkResult();
            UserToPerm = PermCheck.GetPerms(ThisUser, UserNames);

            // If we don't have access to any users specified, forbid
            if (!UserToPerm.Values.Any(p => p != Perm.None))
            {
                Result = new ForbidResult();
                return false;
            }

            return true;
        }

        /// <summary>
        /// Retrieves the type from the action result
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="actionResult">The actionResult</param>
        /// <returns>The type or null on failure</returns>
        public static T? GetObject<T>(this IActionResult? actionResult)
        {
            if (actionResult != null && actionResult is OkObjectResult okObjectActionResult && okObjectActionResult.Value is T result)
            {
                return result;
            }

            return default;
        }

        /// <summary>
        /// Retrieves a controller instance
        /// </summary>
        /// <typeparam name="T">The controller type</typeparam>
        /// <param name="_">Unused</param>
        /// <returns>An instance of the controller</returns>
        public static T GetController<T>(this ControllerBase _) where T: ControllerBase, new()
        {
            return new T();
        }

        /// <summary>
        /// Retrieves a controller instance
        /// </summary>
        /// <typeparam name="T">The controller type</typeparam>
        /// <param name="_">Unused</param>
        /// <param name="apiKey">Optional APIKey (if authenticated endpoint)</param>
        /// <returns>An instance of the controller or null on failure</returns>
        public static async Task<T?> GetController<T>(this ControllerBase _, string apiKey) where T: ControllerBase, new()
        {
            var apiKeyValidationResult = await apiKey.ValidateAPIKey();
            if (!apiKeyValidationResult.IsValid)
            {
                return default;
            }

            var controller = new T();
            var claims = UserService.GetClaims(UserService.GetUser(apiKeyValidationResult.APIKeyUserName!), HasWritePermission: apiKeyValidationResult.HasWriteAccess);
            var identity = new ClaimsIdentity(claims, APIKeyAuthDefaults.AuthenticationScheme);
            var principal = new ClaimsPrincipal(identity);
            controller.ControllerContext = new ControllerContext();
            controller.ControllerContext.HttpContext = new DefaultHttpContext();
            controller.HttpContext.User = principal;

            return controller;
        }
    }
}
