﻿namespace FIOAPI.Extensions
{
    /// <summary>
    /// DictionaryExtensions
    /// </summary>
    public static class DictionaryExtensions
    {
        /// <summary>
        /// AddOrUpdate - Adds the key-value pair if not present, otherwise updates the value
        /// </summary>
        /// <typeparam name="T">Key type</typeparam>
        /// <typeparam name="U">Value type</typeparam>
        /// <param name="thisDict">The dictionary</param>
        /// <param name="Key">Key</param>
        /// <param name="Value">Value</param>
        public static void AddOrUpdate<T, U>(this Dictionary<T, U> thisDict, T Key, U Value) where T : notnull
        {
            if (thisDict.ContainsKey(Key))
            {
                thisDict[Key] = Value;
            }
            else
            {
                thisDict.Add(Key, Value);
            }
        }

        /// <summary>
        /// AddToList - Adds to a Dictionary of T to List of type
        /// </summary>
        /// <typeparam name="T">The key type</typeparam>
        /// <typeparam name="U">An IList of V</typeparam>
        /// <typeparam name="V">The inner type of the value</typeparam>
        /// <param name="thisDict">This dictionary</param>
        /// <param name="Key">The key</param>
        /// <param name="Value">The value</param>
        /// <param name="UniqueValuePerList">If the list should only be unique values</param>
        public static void AddToList<T, U, V>(this Dictionary<T, U> thisDict, T Key, V Value, bool UniqueValuePerList = true) 
            where U : IList<V>, new()
            where T: notnull
        {
            if (thisDict.ContainsKey(Key))
            {
                if (!UniqueValuePerList || !thisDict[Key].Contains(Value))
                {
                    thisDict[Key].Add(Value);
                }
            }
            else
            {
                thisDict.Add(Key, (new U { Value }));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T">The key type</typeparam>
        /// <typeparam name="U">An IList of V</typeparam>
        /// <typeparam name="V">The inner type of the value</typeparam>
        /// <param name="thisDict">This dictionary</param>
        /// <param name="Key">The key</param>
        /// <param name="Value">The value</param>
        /// <param name="RemoveKeyOnEmpty">If the key should be removed from the dictionary if the list is empty</param>
        public static void RemoveFromList<T, U, V>(this Dictionary<T, U> thisDict, T Key, V Value, bool RemoveKeyOnEmpty = true) 
            where U : IList<V>, new()
            where T : notnull
        {
            if (thisDict.ContainsKey(Key))
            {
                thisDict[Key].Remove(Value);
                if (RemoveKeyOnEmpty && !thisDict[Key].Any())
                {
                    thisDict.Remove(Key);
                }
            }
        }
    }
}
