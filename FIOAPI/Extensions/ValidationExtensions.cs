﻿using System.Linq;
using System.Diagnostics;

namespace FIOAPI.Extensions
{
    /// <summary>
    /// Helper PropInfo validation extensions
    /// </summary>
    public static class ValidationExtensions
    {
        /// <summary>
        /// Runs range validation a propInfo
        /// </summary>
        /// <param name="propInfo">PropInfo</param>
        /// <param name="obj">obj</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="Context">The context (name of the variable)</param>
        public static void RunRangeValidationsOnPropInfo(this PropertyInfo propInfo, object? obj, ref List<string> Errors, string Context)
        {
            IComparable? propValue = obj as IComparable;
            if (propValue != null)
            {
                var attrs = propInfo.GetCustomAttributes(true);
                foreach (var attr in attrs)
                {
                    RangeAttribute? rangeAttr = attr as RangeAttribute;
                    if (rangeAttr != null)
                    {
                        var minimum = Convert.ChangeType(rangeAttr.Minimum, propValue.GetType());
                        var maximum = Convert.ChangeType(rangeAttr.Maximum, propValue.GetType());
                        if (propValue.CompareTo(minimum) < 0)
                        {
                            Errors.Add($"'{Context}' is less than the minimum");
                            break;
                        }
                        else if (propValue.CompareTo(maximum) > 0)
                        {
                            Errors.Add($"'{Context}' is greater than the maximum");
                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Runs validation on the given input as if it was the provided type and property
        /// </summary>
        /// <param name="input">The object to run validation on</param>
        /// <param name="type">The type of the object</param>
        /// <param name="PropertyName">The property name</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context</param>
        /// <exception cref="InvalidOperationException">If the type doesn't have the propertyname</exception>
        /// <remarks>You should pretty much always use this over duplicating what the attributes do.</remarks>
        public static void RunValidationsAsIfTypeAndProperty(object input, Type type, string PropertyName, ref List<string> Errors, string Context)
        {
            var propInfo = type.GetProperty(PropertyName);
            if (propInfo == null) throw new InvalidOperationException($"{type.Name} does not have property {PropertyName}");

            if (input is IList || input.GetType().IsArray)
            {
                var enumerable = (IEnumerable)input;
                var list = enumerable.Cast<object>().ToList();
                for(var listIdx = 0; listIdx < list.Count; ++listIdx)
                {
                    var ListContext = $"{Context}[{listIdx}]";
                    propInfo.RunValidationOnEvaluatedPropInfo(list[listIdx], ref Errors, ListContext);
                }
            }
            else
            {
                propInfo.RunValidationOnEvaluatedPropInfo(input, ref Errors, Context);
            }
        }

        /// <summary>
        /// Runs string length validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="MinimumLength">The minimum length (0 if no minimum)</param>
        /// <param name="MaximumLength">The maximum length (0 if no maximum)</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunStringLengthValidation(string? propValue, int MinimumLength, int MaximumLength, ref List<string> Errors, string Context)
        {
            if (propValue == null && MinimumLength != 0)
            {
                Errors.Add($"'{Context}' is null but must be >= {MinimumLength}");
            }
            else if (propValue != null && MinimumLength != 0 && propValue.Length < MinimumLength)
            {
                Errors.Add($"'{Context}' is {propValue} which is length {propValue.Length} but must be >= {MinimumLength}");
            }

            if (propValue != null && propValue.Length > MaximumLength)
            {
                Errors.Add($"'{Context}' is {propValue} which is length {propValue.Length} but must be <= {MaximumLength}");
            }
        }

        /// <summary>
        /// Runs lowercase validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunLowercaseValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null && propValue.ToLowerInvariant() != propValue)
            {
                Errors.Add($"'{Context}' is not lowercase but must be");
            }
        }

        /// <summary>
        /// Runs uppercase validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunUppercaseValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null && propValue.ToUpperInvariant() != propValue)
            {
                Errors.Add($"'{Context}' is not uppercase but must be");
            }
        }

        /// <summary>
        /// Runs password hash validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunPasswordHashValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null && !SecurePasswordHasher.IsHashSupported(propValue))
            {
                Errors.Add($"'{Context}' is not a valid password hash");
            }
        }

        /// <summary>
        /// Runs APEXID validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunAPEXIDValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null && (propValue.Length != 32 || !propValue.All(ch => (ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f'))))
            {
                Errors.Add($"'{Context}' with value '{propValue}' is not a valid APEX ID");
            }
        }

        private const string SampleStationNaturalId = "HRT";
        private const string SampleLongNaturalIdSystem = "ZZ-123";
        private const string SampleLongNaturalIdPlanet = "ZZ-123a";

        

        private const string LongNaturalIdRegexPattern = @"^[A-Z]{2}-[0-9]{3}[a-z]?$";
        private static Regex LongNaturalIdRegex = new Regex(LongNaturalIdRegexPattern, RegexOptions.Compiled);

        /// <summary>
        /// Runs NaturalId validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunNaturalIDValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null)
            {
                var idType = propValue.GetNaturalIdType();
                if (idType == StringExtensions.NaturalIdType.InvalidNaturalId)
                {
                    Errors.Add($"'{Context}' with value '{propValue}' is not a valid NaturalID");
                }
            }
        }

        private const string TickerRegexPattern = "^[A-Z0-9]{0,4}$";
        private static Regex TickerRegex = new Regex(TickerRegexPattern, RegexOptions.Compiled);

        /// <summary>
        /// Runs ticker validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunTickerValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null)
            {
                if (propValue.Length < 1 || propValue.Length > 4)
                {
                    Errors.Add($"'{Context}' is not a valid ticker as it is outside the [1,4] length range.");
                }
                else if (propValue.ToUpper() != propValue)
                {
                    Errors.Add($"'{Context}' is not a valid ticker (not uppercase).");
                }
                else if (!TickerRegex.IsMatch(propValue))
                {
                    Errors.Add($"'{Context}' is not a valid ticker (does not match regex).");
                }
            }
        }

        /// <summary>
        /// Runs currency code validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunCurrencyCodeValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null)
            {
                if (propValue.Length != 3)
                {
                    Errors.Add($"'{Context}' is not a valid currency code as it is not 3 characters");
                }
                else if (propValue.ToUpper() != propValue)
                {
                    Errors.Add($"'{Context}' is not a valid currency code (not uppercase).");
                }
                else if (!Constants.ValidCurrencies.Contains(propValue))
                {
                    Errors.Add($"'{Context}' is not a valid currency code (not in supported currencies list).");
                }
            }
        }

        /// <summary>
        /// Runs resource type validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunResourceTypeValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null)
            {
                if (!Constants.ValidPlanetResourceTypes.Contains(propValue))
                {
                    Errors.Add($"'{Context}' is not a valid resource type");
                }
            }
        }

        /// <summary>
        /// Runs building category validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunBuildingCategoryValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null)
            {
                if (!Constants.BuildingCategoryToType.Keys.Contains(propValue))
                {
                    Errors.Add($"'{Context}' is '{propValue}' which is not a valid building category");
                }
            }
        }

        /// <summary>
        /// Runs workforce level validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunWorkforceLevelValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null)
            {
                if (!Constants.ValidWorkforceLevels.Contains(propValue))
                {
                    Errors.Add($"'{Context}' is not a valid workforce level");
                }
            }
        }

        /// <summary>
        /// Runs fx ticker validation on the provided string
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunFXTickerValidation(string? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null)
            {
                if (propValue.Length != 7)
                {
                    Errors.Add($"'{Context}' is not a valid FX ticker as it is not 7 characters");
                }
                else
                {
                    string leftSide = propValue[0..3];
                    char slash = propValue[3];
                    string rightSide = propValue[^3..^0];

                    if (slash != '/')
                    {
                        Errors.Add($"'{Context}' is not a valid FX ticker pair.");
                    } 
                    else if (!Constants.ValidCurrencies.Contains(leftSide))
                    {
                        Errors.Add($"'{Context}' - left side is not a valid currency");
                    }
                    else if (!Constants.ValidCurrencies.Contains(rightSide))
                    {
                        Errors.Add($"'{Context}' - right side is not a valid currency");
                    }
                    else if (leftSide == rightSide)
                    {
                        Errors.Add($"'{Context}' - cannot have left and right side matching");
                    }
                }
            }
        }

        /// <summary>
        /// Runs apex timestamp validation on the provided long
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunAPEXTimestampValidation(long? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null)
            {
                // @TODO: Update this when the year 2100 comes around
                if (propValue < Constants.MinimumAPEXTimestamp || propValue > Constants.MaximumAPEXTimestamp)
                {
                    Errors.Add($"{Context} - timestamp is outside valid range. [2000, 2100]");
                }
            }
        }

        /// <summary>
        /// Runs datetime timestamp validation on the provided datetime
        /// </summary>
        /// <param name="propValue">The string to run validation on</param>
        /// <param name="Errors">[ref] Errors list</param>
        /// <param name="Context">Context of the object being run on</param>
        public static void RunValidTimestampValidation(DateTime? propValue, ref List<string> Errors, string Context)
        {
            if (propValue != null)
            {
                DateTime dateTime = (DateTime)propValue;
                if (dateTime.ToEpochMs() < Constants.MinimumAPEXTimestamp || dateTime.ToEpochMs() > Constants.MaximumAPEXTimestamp)
                {
                    Errors.Add($"{Context} - timestamp is outside valid range. [2000, 2100]");
                }
            }
        }

        /// <summary>
        /// Runs string validation a propInfo
        /// </summary>
        /// <param name="propInfo">PropInfo</param>
        /// <param name="obj">obj</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="Context">The context (name of the variable)</param>
        public static void RunStringValidationOnPropInfo(this PropertyInfo propInfo, object? obj, ref List<string> Errors, string Context)
        {
            string? propValue = obj as string;

            var attrs = propInfo.GetCustomAttributes(true);
            foreach (var attr in attrs)
            {
                StringLengthAttribute? strLenAttr = attr as StringLengthAttribute;
                if (strLenAttr != null)
                {
                    RunStringLengthValidation(propValue, strLenAttr.MinimumLength, strLenAttr.MaximumLength, ref Errors, Context);
                    continue;
                }

                LowercaseAttribute? lowercaseAttr = attr as LowercaseAttribute;
                if (lowercaseAttr != null)
                {
                    RunLowercaseValidation(propValue, ref Errors, Context);
                    continue;
                }

                UppercaseAttribute? uppercaseAttr = attr as UppercaseAttribute;
                if (uppercaseAttr != null)
                {
                    RunUppercaseValidation(propValue, ref Errors, Context);
                    continue;
                }

                PasswordHashAttribute? passwordHashAttr = attr as PasswordHashAttribute;
                if (passwordHashAttr != null)
                {
                    RunPasswordHashValidation(propValue, ref Errors, Context);
                    continue;
                }

                APEXIDAttribute? apexIdAttr = attr as APEXIDAttribute;
                if (apexIdAttr != null)
                {
                    RunAPEXIDValidation(propValue, ref Errors, Context);
                    continue;
                }

                NaturalIDAttribute? naturalIdAttr = attr as NaturalIDAttribute;
                if (naturalIdAttr != null)
                {
                    RunNaturalIDValidation(propValue, ref Errors, Context);
                    continue;
                }

                TickerAttribute? tickerAttr = attr as TickerAttribute;
                if (tickerAttr != null)
                {
                    RunTickerValidation(propValue, ref Errors, Context);
                    continue;
                }

                CurrencyCodeAttribute? currencyCodeAttr = attr as CurrencyCodeAttribute;
                if (currencyCodeAttr != null)
                {
                    RunCurrencyCodeValidation(propValue, ref Errors, Context);
                    continue;
                }

                FXTickerAttribute? fxTickerAttr = attr as FXTickerAttribute;
                if (fxTickerAttr != null)
                {
                    RunFXTickerValidation(propValue, ref Errors, Context);
                    continue;
                }

                ResourceTypeAttribute? resourceTypeAttr = attr as ResourceTypeAttribute;
                if (resourceTypeAttr != null)
                {
                    RunResourceTypeValidation(propValue, ref Errors, Context);
                    continue;
                }

                BuildingCategoryAttribute? buildingCategoryAttr = attr as BuildingCategoryAttribute;
                if (buildingCategoryAttr != null)
                {
                    RunBuildingCategoryValidation(propValue, ref Errors, Context);
                    continue;
                }

                WorkforceLevelAttribute? workforceLevelAttribute = attr as WorkforceLevelAttribute;
                if (workforceLevelAttribute != null)
                {
                    RunWorkforceLevelValidation(propValue, ref Errors, Context);
                    continue;
                }
            }
        }

        /// <summary>
        /// Runs long validation a propInfo
        /// </summary>
        /// <param name="propInfo">PropInfo</param>
        /// <param name="obj">obj</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="Context">The context (name of the variable)</param>
        public static void RunLongValidationOnPropInfo(this PropertyInfo propInfo, object? obj, ref List<string> Errors, string Context)
        {
            long? propValue = obj as long?;

            var attrs = propInfo.GetCustomAttributes(true);
            foreach (var attr in attrs)
            {
                APEXTimestampAttribute? apexTimestampAttr = attr as APEXTimestampAttribute;
                if (apexTimestampAttr != null)
                {
                    RunAPEXTimestampValidation(propValue, ref Errors, Context);
                    continue;
                }
            }
        }

        /// <summary>
        /// Runs DateTime validation a propInfo
        /// </summary>
        /// <param name="propInfo">PropInfo</param>
        /// <param name="obj">obj</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="Context">The context (name of the variable)</param>
        public static void RunDateTimeValidationOnPropInfo(this PropertyInfo propInfo, object? obj, ref List<string> Errors, string Context)
        {
            DateTime? propValue = obj as DateTime?;

            var attrs = propInfo.GetCustomAttributes(true);
            foreach (var attr in attrs)
            {
                APEXTimestampAttribute? apexTimestampAttr = attr as APEXTimestampAttribute;
                if (apexTimestampAttr != null)
                {
                    RunValidTimestampValidation(propValue, ref Errors, Context);
                    continue;
                }
            }
        }

        /// <summary>
        /// Runs non-nullable object validation on a propInfo. Any object which isn't Nullable boxed (or has the `?` suffix) must not be null.
        /// </summary>
        /// <param name="propInfo"></param>
        /// <param name="obj"></param>
        /// <param name="Errors"></param>
        /// <param name="Context"></param>
        public static void RunNonNullableObjectValidationOnPropInfo(this PropertyInfo propInfo, object? obj, ref List<String> Errors, string Context)
        {
            bool IsNullableType = Nullable.GetUnderlyingType(propInfo.PropertyType) != null || propInfo.IsNullableReferenceType();
            bool IsPrimitiveType = propInfo.PropertyType.IsPrimitive;
            if (!IsNullableType && !IsPrimitiveType)
            {
                if (obj == null)
                {
                    Errors.Add($"'{Context}' is null.");
                }
            }
        }

        /// <summary>
        /// Runs notempty validation a propInfo
        /// </summary>
        /// <param name="propInfo">PropInfo</param>
        /// <param name="obj">obj</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="Context">The context (name of the variable)</param>
        public static void RunContainerValidationsOnPropInfo(this PropertyInfo propInfo, object? obj, ref List<string> Errors, string Context)
        {
            var notEmptyAttr = propInfo.GetCustomAttributes(true)
                .Where(attr => attr is NotEmptyAttribute)
                .Select(attr => (NotEmptyAttribute)attr)
                .FirstOrDefault();

            var containerLengthAttr = propInfo.GetCustomAttributes(true)
                .Where(attr => attr is ContainerLengthAttribute)
                .Select(attr => (ContainerLengthAttribute)attr)
                .FirstOrDefault();

            bool ContainerIsNotNull = false;
            int NumberOfElements = 0;

            if (notEmptyAttr != null || containerLengthAttr != null)
            {
                if (propInfo.PropertyType.IsArray)
                {
                    Array? arr = obj as Array;
                    ContainerIsNotNull = (arr != null);
                    NumberOfElements = ContainerIsNotNull ? arr!.Length : 0;
                }
                else
                {
                    ICollection? collection = obj as ICollection;
                    ContainerIsNotNull = (collection != null);
                    NumberOfElements = ContainerIsNotNull ? collection!.Count : 0;
                }
            }

            if (ContainerIsNotNull)
            {
                if (notEmptyAttr != null)
                {
                    if (NumberOfElements == 0)
                    {
                        Errors.Add($"{Context} has no elements but is expected to not be empty");
                    }
                }

                if (containerLengthAttr != null)
                {
                    if (NumberOfElements < containerLengthAttr.MinimumContainerLength || NumberOfElements > containerLengthAttr.ContainerLength)
                    {
                        Errors.Add($"{Context} is expected to have an element count in the range [{containerLengthAttr.MinimumContainerLength}, {containerLengthAttr.ContainerLength}] but has a count of {NumberOfElements}");
                    }
                }
            }
        }

        /// <summary>
        /// Runs equals validation on a propInfo
        /// </summary>
        /// <param name="propInfo"></param>
        /// <param name="obj"></param>
        /// <param name="Errors"></param>
        /// <param name="Context"></param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void RunEqualsValidationsOnPropInfo(this PropertyInfo propInfo, object? obj, ref List<string> Errors, string Context)
        {
            var equalsAttrs = propInfo.GetCustomAttributes(true)
                .Where(attr => attr is EqualsAttribute)
                .Select(attr => (EqualsAttribute)attr)
                .ToList();
            if (equalsAttrs.Count > 0)
            {
                var PropType = propInfo.PropertyType;
                bool PropIsArray = PropType.IsArray;
                bool PropIsList = PropType.IsGenericType && (PropType.GetGenericTypeDefinition() == typeof(List<>));
                bool PropIsArrayOrContainer = PropIsArray || PropIsList;

                foreach (var equalsAttr in equalsAttrs)
                {
                    bool AttributeThinksItIsArrayOrContainer = equalsAttr.OffsetIndex >= 0;
                    if (AttributeThinksItIsArrayOrContainer != PropIsArrayOrContainer)
                    {
                        Errors.Add($"'{Context}' has an Equals attribute with an OffsetIndex but isn't an array or list");
                        continue;
                    }

                    object? workingObj = obj;
                    string workingContext = Context;

                    if (obj != null)
                    {
                        if (PropIsArray)
                        {
                            Array arr = (Array)obj!;
                            if (equalsAttr.OffsetIndex >= arr.Length)
                            {
                                Errors.Add($"'{Context}' has Equals attribute with OffsetIndex of {equalsAttr.OffsetIndex} which exceeds the array's length");
                                continue;
                            }

                            // Replace obj, PropType, and Context with that of the inner
                            workingObj = arr.GetValue(equalsAttr.OffsetIndex);
                            PropType = workingObj?.GetType()!;
                            workingContext = $"{Context}[{equalsAttr.OffsetIndex}]";
                        }

                        if (PropIsList)
                        {
                            IList list = (IList)obj!;
                            if (equalsAttr.OffsetIndex >= list.Count)
                            {
                                Errors.Add($"'{Context}' has Equals attribute with OffsetIndex of {equalsAttr.OffsetIndex} which exceeds the list's length");
                                continue;
                            }

                            // Replace obj, PropType, and Context with that of the inner
                            workingObj = list[equalsAttr.OffsetIndex];
                            PropType = workingObj?.GetType()!;
                            workingContext = $"{Context}[{equalsAttr.OffsetIndex}]";
                        }
                    }

                    if (equalsAttr.Value == null)
                    {
                        if (workingObj != null)
                        {
                            Errors.Add($"'{Context}' was expected to be null but isn't.");
                        }

                        continue;
                    }

                    if (workingObj == null)
                    {
                        Errors.Add($"'{Context}' is null but is expected to be valid.");
                        continue;
                    }

                    Debug.Assert(workingObj != null && equalsAttr.Value != null);

                    var objTypeValue = Convert.ChangeType(workingObj, PropType);
                    if (objTypeValue == null)
                    {
                        Errors.Add($"'{workingContext}' could not be converted to expected type '{PropType.Name}'.");
                    }
                    else
                    {
                        var equalsAttrType = equalsAttr.Value.GetType();
                        if (equalsAttrType != PropType)
                        {
                            Errors.Add($"'{workingContext}' could not be converted to expected type '{PropType.Name}'.");
                        }
                        else
                        {
                            if (!objTypeValue.Equals(equalsAttr.Value))
                            {
                                Errors.Add($"{workingContext}' does not match the expected value. Value: '{objTypeValue}'. Expected: '{equalsAttr.Value}'.");
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Runs guid validation on a propInfo
        /// </summary>
        /// <param name="propInfo">PropInfo</param>
        /// <param name="obj">obj</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="Context">The context (name of the variable)</param>
        public static void RunGuidValidationsOnPropInfo(this PropertyInfo propInfo, object? obj, ref List<string> Errors, string Context)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

            Guid propValue = (Guid)obj!;

            var attrs = propInfo.GetCustomAttributes(true);
            foreach (var attr in attrs)
            {
                GuidValidAttribute? guidValidAttr = attr as GuidValidAttribute;
                if (guidValidAttr != null)
                {
                    if (propValue == Guid.Empty)
                    {
                        Errors.Add($"'{Context}' is an invalid Guid");
                    }

                    break;
                }
            }
        }

        /// <summary>
        /// Runs positive number validation a propInfo
        /// </summary>
        /// <param name="propInfo">PropInfo</param>
        /// <param name="obj">obj</param>
        /// <param name="Errors">[ref] Errors</param>
        /// <param name="Context">The context (name of the variable)</param>
        public static void RunPositiveNumberValidationsOnPropInfo(this PropertyInfo propInfo, object? obj, ref List<string> Errors, string Context)
        {
            if (obj == null)
            {
                throw new ArgumentNullException(nameof(obj));
            }

#if DEBUG
            if (!Constants.SupportedPositiveNumberTypes.Contains(obj.GetType().Name))
            {
                throw new ArgumentException($"{nameof(obj)} is not a supported number type");
            }
#endif // DEBUG

            var attrs = propInfo.GetCustomAttributes(true);
            foreach (var attr in attrs)
            {
                PositiveNumberAttribute? posNumAttr = attr as PositiveNumberAttribute;
                if (posNumAttr != null)
                {
                    if (obj is double && (double)obj <= 0.0)
                    {
                        Errors.Add($"'{Context}' (double) is not a positive value.");
                        break;
                    }

                    if (obj is float && (float)obj <= 0.0f)
                    {
                        Errors.Add($"'{Context}' (float) is not a positive value.");
                        break;
                    }

                    if (obj is int && (int)obj <= 0)
                    {
                        Errors.Add($"'{Context}' (int) is not a positive value.");
                        break;
                    }

                    if (obj is long && (long)obj <= 0)
                    {
                        Errors.Add($"'{Context}' (long) is not a positive value.");
                        break;
                    }

                    if (obj is short && (short)obj <= 0)
                    {
                        Errors.Add($"'{Context}' (short) is not a positive value.");
                        break;
                    }

                    if (obj is sbyte && (sbyte)obj <= 0)
                    {
                        Errors.Add($"'{Context}' (sbyte) is not a positive value.");
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Helper method to determine if the property is a nullable reference type
        /// </summary>
        /// <param name="propInfo">The PropertyInfo</param>
        /// <returns>true if nullable, false otherwise</returns>
        public static bool IsNullableReferenceType(this PropertyInfo propInfo)
        {
            var nullabilityContext = new NullabilityInfoContext();
            var nullabilityInfo = nullabilityContext.Create(propInfo);
            return nullabilityInfo.WriteState is NullabilityState.Nullable;
        }

        /// <summary>
        /// Runs validation on the provided propInfo and actual object
        /// </summary>
        /// <param name="propInfo">PropertyInformation</param>
        /// <param name="obj">object</param>
        /// <param name="Errors">[ref] List of errors</param>
        /// <param name="Context">Context</param>
        public static void RunValidationOnEvaluatedPropInfo(this PropertyInfo propInfo, object? obj, ref List<string> Errors, string Context)
        {
            bool IsNullableType = Nullable.GetUnderlyingType(propInfo.PropertyType) != null || propInfo.IsNullableReferenceType();
            bool IsPrimitiveType = propInfo.PropertyType.IsPrimitive;

            // If the object is virtual but isn't a collection, that means that this field is a "ParentModel" object, so we shouldn't validate that it's null.
            // Basically, the "ParentModel" object will get auto-filled by EFCore on demand. It's good practice to fill it out, but it isn't required.
            bool IsVirtual = propInfo.GetMethod != null && propInfo.GetMethod.IsVirtual;
            bool IsCollection = obj is ICollection;

            if (!IsNullableType && !IsPrimitiveType && (!IsVirtual && !IsCollection))
            {
                propInfo.RunNonNullableObjectValidationOnPropInfo(obj, ref Errors, Context);
            }

            propInfo.RunEqualsValidationsOnPropInfo(obj, ref Errors, Context);

            if (obj is string)
            {
                propInfo.RunStringValidationOnPropInfo(obj, ref Errors, Context);
                return;
            }

            if (obj is Guid)
            {
                propInfo.RunGuidValidationsOnPropInfo(obj, ref Errors, Context);
                return;
            }

            if (obj is ICollection || propInfo.PropertyType.IsArray)
            {
                propInfo.RunContainerValidationsOnPropInfo(obj, ref Errors, Context);
                return;
            }

            if (obj is long)
            {
                propInfo.RunLongValidationOnPropInfo(obj, ref Errors, Context);
            }
            
            if (obj is IComparable)
            {
                propInfo.RunRangeValidationsOnPropInfo(obj, ref Errors, Context);
            }

            if (obj != null && Constants.SupportedPositiveNumberTypes.Contains(obj.GetType().Name))
            {
                propInfo.RunPositiveNumberValidationsOnPropInfo(obj, ref Errors, Context);
            }

            if (obj is DateTime)
            {
                propInfo.RunDateTimeValidationOnPropInfo(obj, ref Errors, Context);
            }
        }

        /// <summary>
        /// Runs validation on the provided propInfo and PropertyInfo's parent object
        /// </summary>
        /// <param name="propInfo">PropertyInformation</param>
        /// <param name="obj">object</param>
        /// <param name="Errors">[ref] List of errors</param>
        /// <param name="Context">Context</param>
        public static void RunValidationOnPropInfo(this PropertyInfo propInfo, object obj, ref List<string> Errors, string Context) =>
            RunValidationOnEvaluatedPropInfo(propInfo, propInfo.GetValue(obj), ref Errors, Context);
    }
}
