﻿namespace FIOAPI.Extensions
{
    /// <summary>
    /// ListExtensions
    /// </summary>
    public static class ListExtensions
    {
        /// <summary>
        /// Validates a List of T assuming T is derived from IValidation
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="theList">The list</param>
        /// <param name="Errors">ref Errors</param>
        /// <returns>true if no errors</returns>
        public static bool Validate<T>(this List<T> theList, ref List<string> Errors) where T : IValidation
        {
            bool ValidationSuccessful = true;

            if (!typeof(T).IsSubclassOf(typeof(IValidation)))
            {
                throw new ArgumentException("Provided class in List<class> isn't derived from IValidation");
            }

            for (int ValueIdx = 0; ValueIdx < theList.Count; ++ValueIdx)
            {
                var Value = (theList[ValueIdx] as IValidation)!;
                if (!Value.Validate(ref Errors, $"this[{ValueIdx}]"))
                {
                    ValidationSuccessful = false;
                }
            }

            return ValidationSuccessful;
        }

        /// <summary>
        /// Clones a List of T
        /// </summary>
        /// <typeparam name="T">An ICloneable</typeparam>
        /// <param name="theList">List to clone</param>
        /// <returns>A cloned list</returns>
        public static List<T> Clone<T>(this List<T> theList) where T : ICloneable
        {
            List<T> newList = new List<T>();
            theList.ForEach(i =>
            {
                newList.Add((T)i.Clone());
            });
            return newList;
        }
    }
}
