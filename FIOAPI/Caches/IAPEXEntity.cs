﻿namespace FIOAPI.Caches
{
    /// <summary>
    /// IAPEXEntity
    /// <remarks>Also needs to implement ICloneable (including on children classes)</remarks>
    /// </summary>
    public interface IAPEXEntity : ICloneable
    {
        /// <summary>
        /// EntityId
        /// </summary>
        public string EntityId { get; }

        /// <summary>
        /// EntityName
        /// </summary>
        public string? EntityName { get; }

        /// <summary>
        /// EntityFriendlyIdentifier
        /// </summary>
        public string EntityFriendlyIdentifier { get; }
    }
}
