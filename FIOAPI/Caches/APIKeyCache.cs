﻿namespace FIOAPI.Caches
{
    /// <summary>
    /// APIKey Cache
    /// </summary>
    public static class APIKeyCache
    {
        private static MRUCache<Guid, Model.APIKey> MRUCache = new(512);

        /// <summary>
        /// Retreives an APIKey from the cache
        /// </summary>
        /// <param name="Key">The Guid to search for</param>
        /// <returns>The APIKey if present, null otherwise</returns>
        public static Model.APIKey? Get(Guid Key)
        {
            return MRUCache.Get(Key);
        }

        /// <summary>
        /// Sets an Guid-APIKey pair
        /// </summary>
        /// <param name="Key">The Guid</param>
        /// <param name="Value">The APIKey</param>
        public static void Set(Guid Key, Model.APIKey Value)
        {
            MRUCache.Set(Key, Value);
        }

        /// <summary>
        /// Removes a entry
        /// </summary>
        /// <param name="Key">Guid to remove</param>
        /// <returns>true if found, false otherwise</returns>
        public static bool Remove(Guid Key)
        {
            return MRUCache.Remove(Key);
        }

        /// <summary>
        /// Retrieves or caches the provided Guid (async)
        /// </summary>
        /// <param name="Key">The guid</param>
        /// <param name="dbAccess">DBAccess object</param>
        /// <returns>APIKey object if valid, null otherwise</returns>
        public static async Task<Model.APIKey?> GetOrCacheAsync(Guid Key, DBAccess? dbAccess = null)
        {
            var res = Get(Key);
            if (res == null)
            {
                res = await CacheAsync(Key, dbAccess);
            }

            return res;
        }

        /// <summary>
        /// Retrieves or caches the provided Guid
        /// </summary>
        /// <param name="Key">The guid</param>
        /// <param name="dbAccess">DBAccess object</param>
        /// <returns>APIKey object if valid, null otherwise</returns>
        public static Model.APIKey? GetOrCache(Guid Key, DBAccess? dbAccess = null)
        {
            var res = Get(Key);
            if (res == null)
            {
                res = Cache(Key, dbAccess);
            }

            return res;
        }

        /// <summary>
        /// Caches the given APIKey (async)
        /// </summary>
        /// <param name="Key">Guid</param>
        /// <param name="dbAccess">DBAccess object</param>
        /// <returns>APIKey object if valid, null otherwise</returns>
        public static async Task<Model.APIKey?> CacheAsync(Guid Key, DBAccess? dbAccess = null)
        {
            DBAccess reader = dbAccess ?? DBAccess.GetReader();

            var apiKey = await reader.DB.APIKeys
                .Where(ak => ak.Key == Key)
                .FirstOrDefaultAsync();
            if (apiKey != null)
            {
                Set(Key, apiKey);
            }

            if (dbAccess == null)
            {
                reader.Dispose();
            }

            return apiKey;
        }

        /// <summary>
        /// Caches the given APIKey
        /// </summary>
        /// <param name="Key">Guid</param>
        /// <param name="dbAccess">DBAccess object</param>
        /// <returns>APIKey object if valid, null otherwise</returns>
        public static Model.APIKey? Cache(Guid Key, DBAccess? dbAccess = null)
        {
            DBAccess reader = dbAccess ?? DBAccess.GetReader();

            var apiKey = reader.DB.APIKeys
                .Where(ak => ak.Key == Key)
                .FirstOrDefault();
            if (apiKey != null)
            {
                Set(Key, apiKey);
            }

            if (dbAccess == null)
            {
                reader.Dispose();
            }

            return apiKey;
        }
    }
}
