﻿using FIOAPI.DB.Model;

namespace FIOAPI.Caches
{
    /// <summary>
    /// EntityCaches
    /// </summary>
    public static class EntityCaches
    {
        /// <summary>
        /// CompanyCache
        /// </summary>
        public static APEXEntityCache<Company> CompanyCache { get; set; } = new();

        /// <summary>
        /// CountryCache
        /// </summary>
        public static APEXEntityCache<Country> CountryCache { get; set; } = new();

        /// <summary>
        /// PlanetCache
        /// </summary>
        public static APEXEntityCache<Planet> PlanetCache { get; set; } = new();

        /// <summary>
        /// StationCache
        /// </summary>
        public static APEXEntityCache<Station> StationCache { get; set; } = new();

        /// <summary>
        /// SystemCache
        /// </summary>
        public static APEXEntityCache<Model.System> SystemCache { get; set; } = new();

        /// <summary>
        /// BuildingCache
        /// </summary>
        public static APEXEntityCache<Building> BuildingCache { get; set;} = new();

        /// <summary>
        /// MaterialCache
        /// </summary>
        public static APEXEntityCache<Material> MaterialCache { get; set; } = new();

        /// <summary>
        /// WarehouseCache
        /// </summary>
        public static APEXEntityCache<Warehouse> WarehouseCache { get; set; } = new();

        /// <summary>
        /// Initializes all entity caches
        /// </summary>
        public static async Task Initialize()
        {
            using(var reader = DBAccess.GetReader())
            {
                CompanyCache.Initialize(await reader.DB.Companies
                    .Include(c => c.Planets)
                    .Include(c => c.Offices)
                    .ToListAsync());

                CountryCache.Initialize(await reader.DB.Countries.ToListAsync());

                PlanetCache.Initialize(await reader.DB.Planets
                    .Include(p => p.Resources)
                    .Include(P => P.WorkforceFees)
                    .Include(p => p.COGCPrograms)
                    .Include(p => p.PopulationReports)
                    .ToListAsync());

                StationCache.Initialize(await reader.DB.Stations
                    .ToListAsync());

                SystemCache.Initialize(await reader.DB.Systems
                    .ToListAsync());

                BuildingCache.Initialize(await reader.DB.Buildings
                    .Include(b => b.Costs)
                    .Include(b => b.Recipes)
                        .ThenInclude(r => r.Inputs)
                    .Include(b => b.Recipes)
                        .ThenInclude(r => r.Outputs)
                    .ToListAsync());

                MaterialCache.Initialize(await reader.DB.Materials
                    .ToListAsync());

                WarehouseCache.Initialize(await reader.DB.Warehouses
                    .ToListAsync());
            }
        }

        /// <summary>
        /// ConvertToSystemId
        /// </summary>
        /// <param name="SearchValue">A search value to modify [ref]</param>
        /// <param name="Error">Error [ref]</param>
        /// <returns>true on success</returns>
        public static bool ConvertToSystemId(ref string SearchValue, out string Error)
        {
            Error = string.Empty;

            var SearchValues = new List<string> { SearchValue };
            bool Success = ConvertToSystemIds(ref SearchValues, out var Errors);
            SearchValue = SearchValues[0];
            if (!Success)
            {
                Error = Errors.First();
            }

            return Success;
        }

        /// <summary>
        /// ConvertToSystemIds
        /// </summary>
        /// <param name="SearchValues">The SearchValues to modify [ref]</param>
        /// <param name="Errors">Errors [out]</param>
        /// <returns>true if succesful</returns>
        public static bool ConvertToSystemIds(ref List<string> SearchValues, out List<string> Errors)
        {
            Errors = new();
            if (SearchValues.Count > 0)
            {
                var Systems = SystemCache.AllEntitiesCopy;
                for (int SearchValuesIdx = 0; SearchValuesIdx < SearchValues.Count; ++SearchValuesIdx)
                {
                    var SearchValue = SearchValues[SearchValuesIdx];
                    if (string.IsNullOrWhiteSpace(SearchValue))
                    {
                        Errors.Add($"SearchValues[{SearchValuesIdx}] was null or whitespace");
                        continue;
                    }

                    var station = StationCache.Get(SearchValue);
                    if (station != null)
                    {
                        var stationSystem = Systems.FirstOrDefault(s => s.RawCelestialBodyIds != null && s.RawCelestialBodyIds.Contains(station.StationId));
                        if (stationSystem != null)
                        {
                            SearchValues[SearchValuesIdx] = stationSystem.SystemId;
                        }
                        else
                        {
                            Errors.Add($"SearchValues[{SearchValuesIdx}] '{SearchValue}' - found the station but did not resolve what system it belongs to.");
                        }
                        
                        continue;
                    }

                    var system = SystemCache.Get(SearchValue);
                    if (system != null)
                    {
                        SearchValues[SearchValuesIdx] = system.SystemId;
                        continue;
                    }

                    var planet = PlanetCache.Get(SearchValue);
                    if (planet != null)
                    {
                        var planetSystem = Systems.FirstOrDefault(s => s.RawPlanetIds != null && s.RawPlanetIds.Contains(planet.PlanetId));
                        if (planetSystem != null)
                        {
                            SearchValues[SearchValuesIdx] = planetSystem.SystemId;
                        }
                        else
                        {
                            Errors.Add($"SearchValues[{SearchValuesIdx}] '{SearchValue}' - found the planet but did not resolve what system it belongs to.");
                        }
                        
                        continue;
                    }

                    // Failure
                    Errors.Add($"SearchValues[{SearchValuesIdx}] '{SearchValue}' - did not resolve to a planet, system, or station.");
                }
            }

            return Errors.Count == 0;
        }
    }
}
