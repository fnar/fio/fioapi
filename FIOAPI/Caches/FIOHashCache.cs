﻿using System.Collections.Concurrent;
using System.Diagnostics;

namespace FIOAPI.Caches
{
    /// <summary>
    /// FIOHashCache
    /// </summary>
    public static class FIOHashCache
    {
        private static ConcurrentDictionary<Type, Dictionary<string, int>> TypeToHashCache = new();

        static FIOHashCache()
        {
            // Construct each dictionary
            foreach (var type in HashCodeExtensions.HashCodeTypeToKeyPropertyInfo.Keys)
            {
                bool Success = TypeToHashCache.TryAdd(type, new());
                Debug.Assert(Success);
            }
        }

        /// <summary>
        /// Updates the HashCache - Returns false if it already present
        /// </summary>
        /// <typeparam name="T">The type</typeparam>
        /// <param name="obj">The obj to pass in</param>
        /// <returns>true if the database should be updated, false otherwise</returns>
        public static bool ShouldUpdate<T>(T obj) where T : IValidation
        {
            var HashCodeKey = obj.HashCodeKey;
            if (HashCodeKey == null)
            {
#if DEBUG
                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }
                else
                {
                    Debugger.Launch();
                }
#endif
                // Soft-fail in this scenario
                return true;
            }
            var HashCode = obj.HashCode;

            Dictionary<string, int> HashCache = TypeToHashCache[obj.GetType()];
            lock (HashCache)
            {
                if (HashCache.ContainsKey(HashCodeKey) && HashCache[HashCodeKey] == HashCode)
                {
                    return false;
                }

                HashCache.AddOrUpdate(HashCodeKey, HashCode);
                return true;
            }
        }
    }
}
