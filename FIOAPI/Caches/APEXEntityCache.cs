﻿using System.Collections.ObjectModel;

namespace FIOAPI.Caches
{
    /// <summary>
    /// APEXEntityCacheReader
    /// </summary>
    public class APEXEntityCacheReader<T> : IDisposable where T : IAPEXEntity
    {
        private APEXEntityCache<T> cache;

        /// <summary>
        /// Gets AllEntities
        /// </summary>
        public IReadOnlyList<T> AllEntities
        {
            get
            {
                return cache.EntityIdToItems.Values.ToList().AsReadOnly();
            }
        }

        internal APEXEntityCacheReader(APEXEntityCache<T> cache)
        {
            this.cache = cache;
            this.cache._lock.EnterReadLock();
        }

        /// <summary>
        /// Dispose
        /// </summary>
        public void Dispose()
        {
            cache._lock.ExitReadLock();
        }
    }

    /// <summary>
    /// APEXEntityCache - A cache for any time that implements IAPEXEntity
    /// </summary>
    /// <typeparam name="T">The class</typeparam>
    public class APEXEntityCache<T> where T : IAPEXEntity
    {
        internal ReaderWriterLockSlim _lock = new();
        internal Dictionary<string, string> NameToEntityId = new();
        internal Dictionary<string, string> NaturalIdToEntityId = new();
        internal Dictionary<string, T> EntityIdToItems = new();

        /// <summary>
        /// Retrieves an IDisposable reader
        /// </summary>
        /// <returns>An APEXEntityCacheReader</returns>
        public APEXEntityCacheReader<T> GetReader()
        {
            return new APEXEntityCacheReader<T>(this);
        }

        /// <summary>
        /// All entities
        /// <remarks>This returns a copy of all entities (and is likely slow)</remarks>
        /// </summary>
        public ReadOnlyCollection<T> AllEntitiesCopy
        {
            get
            {
                _lock.EnterReadLock();
                try
                {
                    return EntityIdToItems.Values
                        .ToList()
                        .Clone()
                        .AsReadOnly();
                }
                finally
                {
                    _lock.ExitReadLock();
                }
            }
        }

        /// <summary>
        /// Retrieves the provided class given a search string
        /// </summary>
        /// <param name="Search">Search string</param>
        /// <returns>result object</returns>
        public T? Get(string Search)
        {
            _lock.EnterReadLock();
            try
            {
                var res = Get_InLock(Search);
                if (res != null)
                {
                    res = (T)res.Clone();
                }

                return res;
            }
            finally
            {
                _lock.ExitReadLock();
            }
        }

        /// <summary>
        /// Retrieves the provided list of classes given a list of search strings
        /// </summary>
        /// <param name="Searches">Search strings</param>
        /// <returns>A *potentially sparse* (null) list of results</returns>
        public List<T?> Get(List<string> Searches)
        {
            List<T?> results = new List<T?>(Searches.Count);

            _lock.EnterReadLock();
            try
            {
                foreach (var Search in Searches)
                {
                    var res = Get_InLock(Search);
                    if (res != null)
                    {
                        results.Add((T)res.Clone());
                    }
                    else
                    {
                        results.Add(res);
                    }
                }
            }
            finally
            {
                _lock.ExitReadLock();
            }

            return results;
        }

        private T? Get_InLock(string Search)
        {
            string? EntityId = null;
            if (Search.Length == 32)
            {
                if (EntityIdToItems.TryGetValue(Search.ToLower(), out T? result))
                {
                    return result;
                }
            }

            if (NameToEntityId.TryGetValue(Search.ToUpper(), out EntityId))
            {
                return EntityIdToItems[EntityId];
            }

            if (NaturalIdToEntityId.TryGetValue(Search.ToUpper(), out EntityId))
            {
                return EntityIdToItems[EntityId];
            }

            return default;
        }

        /// <summary>
        /// Initializes the cache
        /// </summary>
        /// <param name="Items">All items</param>
        public void Initialize(List<T> Items)
        {
            _lock.EnterWriteLock();
            try
            {
                Items.ForEach(i =>
                {
                    Update_InLock(i);
                });
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Async Update
        /// </summary>
        /// <param name="item">The item to cache</param>
        public async Task UpdateAsync(T item)
        {
            await Task.Run(() => Update(item));
        }

        /// <summary>
        /// Updates a single item in the cache
        /// </summary>
        /// <param name="Item">The item</param>
        public void Update(T Item)
        {
            _lock.EnterWriteLock();
            try
            {
                Update_InLock(Item);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        private void Update_InLock(T Entity)
        {
            var EntityId = ((IAPEXEntity)Entity).EntityId;
            var EntityName = ((IAPEXEntity)Entity).EntityName;
            var EntityFriendlyIdentifier = ((IAPEXEntity)Entity).EntityFriendlyIdentifier;

            if (EntityIdToItems.ContainsKey(EntityId))
            {
                EntityIdToItems[EntityId] = Entity;
            }
            else
            {
                if (EntityName != null)
                {
                    NameToEntityId.AddOrUpdate(EntityName.ToUpper(), EntityId);
                }
                NaturalIdToEntityId.AddOrUpdate(EntityFriendlyIdentifier.ToUpper(), EntityId);
                EntityIdToItems.AddOrUpdate(EntityId.ToLower(), Entity);
            }
        }

        /// <summary>
        /// Deletes an item
        /// </summary>
        /// <param name="item">The item</param>
        public async Task DeleteAsync(T item)
        {
            await Task.Run(() => Delete(item));
        }

        /// <summary>
        /// Deletes an item
        /// </summary>
        /// <param name="Item">The item</param>
        public void Delete(T Item)
        {
            _lock.EnterWriteLock();
            try
            {
                Delete_InLock(Item);
            }
            finally
            {
                _lock.ExitWriteLock();
            }
        }

        private void Delete_InLock(T Entity)
        {
            var EntityId = ((IAPEXEntity)Entity).EntityId;
            var EntityName = ((IAPEXEntity)Entity).EntityName;
            var EntityFriendlyIdentifier = ((IAPEXEntity)Entity).EntityFriendlyIdentifier;

            EntityIdToItems.Remove(EntityId.ToLower());
            NaturalIdToEntityId.Remove(EntityFriendlyIdentifier.ToUpper());
            if (EntityName != null)
            {
                NameToEntityId.Remove(EntityName.ToUpper());
            }
        }
    }
}
