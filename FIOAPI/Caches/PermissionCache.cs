﻿using FIOAPI.DB.Model;
using FIOAPI.Payloads.Permission;
using System.Text.RegularExpressions;

#pragma warning disable 1591
namespace FIOAPI.Caches
{
    /// <summary>
    /// PermissionCache
    /// </summary>
    public static class PermissionCache
    {
        private static ReaderWriterLockSlim _PermissionCacheLock = new();
        private static Dictionary<string, Permission> GrantorGrantee_ToPermission { get; set; } = new();
        private static Dictionary<int, Permission> GroupId_ToPermission { get; set; } = new();
        private static Dictionary<int, List<string>> GroupId_ToMembers { get; set; } = new();
        private static Dictionary<string, List<int>> UserName_ToGroupMembershipIds { get; set; } = new();

        public static async Task Initialize()
        {
            _PermissionCacheLock.EnterWriteLock();

            try
            {
                using (var reader = DBAccess.GetReader())
                {
                    var AllGroups = await reader.DB.Groups.ToListAsync();
                    var AllPermissions = await reader.DB.Permissions.ToListAsync();
                    foreach (var Group in AllGroups)
                    {
                        var GroupId = Group.GroupId;
                        var GroupPermission = AllPermissions.First(p => p.GroupId == GroupId);
                        GroupId_ToPermission.Add(GroupId, GroupPermission);

                        var KeyPrefix = $"$GROUP#{GroupId}$";
                        var GroupMemberUserNames = Group.MemberUserNames;
                        foreach (var GroupMemberUserName in GroupMemberUserNames)
                        {
                            GroupId_ToMembers.AddToList(GroupId, GroupMemberUserName);
                            UserName_ToGroupMembershipIds.AddToList(GroupMemberUserName, GroupId);
                            foreach (var OtherGroupMemberUserName in GroupMemberUserNames)
                            {
                                if (GroupMemberUserName != OtherGroupMemberUserName)
                                {
                                    var Key = $"{KeyPrefix}{GroupMemberUserName}-{OtherGroupMemberUserName}";
                                    GrantorGrantee_ToPermission.Add(Key, (Model.Permission)GroupPermission.Clone());
                                }
                            }
                        }
                    }

                    foreach (var Permission in AllPermissions)
                    {
                        var Key = $"{Permission.GrantorUserName}-{Permission.GranteeUserName}";
                        GrantorGrantee_ToPermission.Add(Key, (Model.Permission)Permission.Clone());
                    }
                }
            }
            finally
            {
                _PermissionCacheLock.ExitWriteLock();
            }
        }

        public static async Task OnAddPermissionAsync(Model.Permission permission)
        {
            await Task.Run(() => OnAddPermission(permission));
        }

        public static void OnAddPermission(Model.Permission permission)
        {
            var Key = $"{permission.GrantorUserName}-{permission.GranteeUserName}";
            var PermissionCopy = (Model.Permission)permission.Clone();

            _PermissionCacheLock.EnterWriteLock();
            try
            {
                GrantorGrantee_ToPermission.AddOrUpdate(Key, PermissionCopy);
            }
            finally
            {
                _PermissionCacheLock.ExitWriteLock();
            }
        }

        public static async Task OnDeletePermissionAsync(Model.Permission permission)
        {
            await Task.Run(() => OnDeletePermission(permission));
        }

        public static void OnDeletePermission(Model.Permission permission)
        {
            var Key = $"{permission.GrantorUserName}-{permission.GranteeUserName}";
            _PermissionCacheLock.EnterWriteLock();
            try
            {
                GrantorGrantee_ToPermission.Remove(Key);
            }
            finally
            {
                _PermissionCacheLock.ExitWriteLock();
            }
        }

        public static async Task OnCreateGroupAsync(Model.Group group, Model.Permission permission)
        {
            await Task.Run(() => OnCreateGroup(group, permission));
        }

        public static void OnCreateGroup(Model.Group group, Model.Permission permission)
        {
            var PermissionCopy = (Model.Permission)permission.Clone();

            _PermissionCacheLock.EnterWriteLock();
            try
            {
                GroupId_ToMembers.AddToList(group.GroupId, group.GroupOwner);
                UserName_ToGroupMembershipIds.AddToList(group.GroupOwner, group.GroupId);
                GroupId_ToPermission.AddOrUpdate(group.GroupId, PermissionCopy);
                
            }
            finally
            {
                _PermissionCacheLock.ExitWriteLock();
            }
        }

        public static async Task OnAddGroupMemberAsync(string UserName, Model.Group group)
        {
            await Task.Run(() => OnAddGroupMember(UserName, group));
        }

        public static void OnAddGroupMember(string UserName, Model.Group group)
        {
            _PermissionCacheLock.EnterWriteLock();
            try
            {
                UserName_ToGroupMembershipIds.AddToList(UserName, group.GroupId);
                var KeyPrefix = $"$GROUP#{group.GroupId}$";

                Permission permission = (Model.Permission)GroupId_ToPermission[group.GroupId].Clone();

                GroupId_ToMembers.AddToList(group.GroupId, UserName);

                // When adding a group member, we need to create the following for every pair:
                // - UserName->GroupMember
                // - GroupMember->UserName
                foreach (var memberUserName in group.MemberUserNames)
                {
                    if (memberUserName != UserName)
                    {
                        // User -> GroupMember
                        var Key = $"{KeyPrefix}{UserName}-{memberUserName}";
                        GrantorGrantee_ToPermission.Add(Key, permission);
                        
                        // GroupMember -> User
                        Key = $"{KeyPrefix}{memberUserName}-{UserName}";
                        GrantorGrantee_ToPermission.Add(Key, permission);
                    }
                }
            }
            finally
            {
                _PermissionCacheLock.ExitWriteLock();
            }
        }

        public static async Task OnRemoveGroupMemberAsync(string UserName, Model.Group group)
        {
            await Task.Run(() => OnRemoveGroupMember(UserName, group));
        }

        public static void OnRemoveGroupMember(string UserName, Model.Group group)
        {
            _PermissionCacheLock.EnterWriteLock();
            try
            {
                GroupId_ToMembers.RemoveFromList(group.GroupId, UserName);
                UserName_ToGroupMembershipIds.RemoveFromList(UserName, group.GroupId);
                var KeyPrefix = $"$GROUP#{group.GroupId}$";
                foreach (var memberUserName in group.MemberUserNames)
                {
                    if (memberUserName != UserName)
                    {
                        // User -> GroupMember
                        var Key = $"{KeyPrefix}{UserName}-{memberUserName}";
                        GrantorGrantee_ToPermission.Remove(Key);

                        // GroupMember -> User
                        Key = $"{KeyPrefix}{memberUserName}-{UserName}";
                        GrantorGrantee_ToPermission.Remove(Key);
                    }
                }
            }
            finally
            {
                _PermissionCacheLock.ExitWriteLock();
            }
        }

        public static async Task OnDeleteGroupAsync(Model.Group group)
        {
            await Task.Run(() => OnDeleteGroup(group));
        }

        public static void OnDeleteGroup(Model.Group group)
        {
            _PermissionCacheLock.EnterWriteLock();
            try
            {
                GroupId_ToMembers.Remove(group.GroupId);

                var MemberUserNames = group.MemberUserNames;
                if (MemberUserNames.Count == 1)
                {
                    // Only the owner, bail
                    return;
                }

                foreach (var MemberUserName in group.MemberUserNames)
                {
                    var KeyPrefix = $"$GROUP#{group.GroupId}$";
                    UserName_ToGroupMembershipIds.RemoveFromList(MemberUserName, group.GroupId);
                    foreach (var OtherMemberUserName in group.MemberUserNames)
                    {
                        if (MemberUserName != OtherMemberUserName)
                        {
                            var Key = $"{KeyPrefix}{MemberUserName}-{OtherMemberUserName}";
                            GrantorGrantee_ToPermission.Remove(Key);
                        }
                    }
                }
            }
            finally
            {
                _PermissionCacheLock.ExitWriteLock();
            }
        }

        public static Dictionary<string, List<Permission>> GetManyPermissions(string CurrentUserName, List<string> UsersToRead)
        {
            if (UsersToRead.Count == 0)
            {
                return new();
            }

            Dictionary<string, List<Permission>> ManyPermissions = new();

            _PermissionCacheLock.EnterReadLock();
            try
            {
                foreach(var UserToRead in UsersToRead)
                {
                    ManyPermissions.Add(UserToRead, GetPermissions_InLock(CurrentUserName, UserToRead));
                }
            }
            finally
            {
                _PermissionCacheLock.ExitReadLock();
            }

            return ManyPermissions;
        }

        private static List<Permission> GetPermissions_InLock(string CurrentUserName, string UserToRead)
        {
            List<Permission> Permissions = new();

            if (CurrentUserName == UserToRead)
            {
                var FullPermission = Constants.FullPermission;
                FullPermission.GrantorUserName = CurrentUserName;
                FullPermission.GranteeUserName = "*";
                return new List<Permission>
                {
                    FullPermission
                };
            }

            // First, check for global permissions
            var Key = $"{UserToRead}-*";
            if (GrantorGrantee_ToPermission.ContainsKey(Key))
            {
                Permissions.Add(GrantorGrantee_ToPermission[Key]);
            }

            // Next, check for an individual permission
            Key = $"{UserToRead}-{CurrentUserName}";
            if (GrantorGrantee_ToPermission.ContainsKey(Key))
            {
                Permissions.Add(GrantorGrantee_ToPermission[Key]);
            }

            List<int>? CurrentUserMembershipIds = UserName_ToGroupMembershipIds.ContainsKey(CurrentUserName) ? UserName_ToGroupMembershipIds[CurrentUserName] : null;
            List<int>? UserToReadMembershipIds = UserName_ToGroupMembershipIds.ContainsKey(UserToRead) ? UserName_ToGroupMembershipIds[UserToRead] : null;
            if (CurrentUserMembershipIds != null && UserToReadMembershipIds != null)
            {
                // Get the intersection of these two lists
                var SharedGroupIds = CurrentUserMembershipIds.Intersect(UserToReadMembershipIds);
                foreach (var SharedGroupId in SharedGroupIds)
                {
                    Permissions.Add(GroupId_ToPermission[SharedGroupId]);
                }
            }

            return Permissions;
        }

        public static List<Model.Permission> GetPermissions(string CurrentUserName, string UserToRead)
        {
            _PermissionCacheLock.EnterReadLock();
            try
            {
                return GetPermissions_InLock(CurrentUserName, UserToRead);
            }
            finally
            {
                _PermissionCacheLock.ExitReadLock();
            }
        }

        public static bool IsUserMemberOfGroup(string UserName, int GroupId)
        {
            return UserName_ToGroupMembershipIds.ContainsKey(UserName) && UserName_ToGroupMembershipIds[UserName].Contains(GroupId);
        }

        public static List<string> GetUsersOfGroup(int GroupId)
        {
            List<string> Users = new();
            if (GroupId_ToMembers.ContainsKey(GroupId))
            {
                Users = GroupId_ToMembers[GroupId];
            }

            return Users;
        }
    }
}
#pragma warning restore 1591