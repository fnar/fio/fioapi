# Testing
- Any new "internal" system should also have tests to verify it functions as expected
- All controllers should have corresponding integration tests to verify they function as expected.

## Running Tests
1. Open FIOAPI.sln
2. `View` menu -> `Test Explorer`
3. Hit the `Run` (green play icon) button or right-click an option and select `Run`

## General Test Writing
Tests are written leveraging the xunit testing framework. In the simplest case, all that is necessary to do is:
1. Create a new class
2. Create a public method on said class
3. Add the `[Test]` attribute to the method
4. Write tests and leverage nunit's `Assert` class to ensure everything works as expected

Example:

```csharp
namespace FIOAPI.Integration.Test.Tests
{
	public class WidgetTest
	{
		[Test]
		public void VerifyId()
		{
			Widget w = new();
			Assert.That(w.Id > 0, Is.True);
		}
	}
}
```

### Ordering Tests
Generally, you should **avoid** requiring test ordering if possible.  However, if it is necessary, you simply leverage the `Order` attribute.  Example:

```csharp
namespace FIOAPI.Integration.Test.Tests
{
	public class WidgetTest
	{
		[Test, Order(1)]
		public void VerifyId()
		{
			Widget w = new();
			Assert.That(w.Id > 0, Is.True);
		}
		
		[Test, Order(0)]
		public void VerifyName()
		{
			Widget w = new();
			Assert.That(!string.IsNullOrWhiteSpace(w.Name), Is.True);
		}
	}
}
```

In the above case, the lower the value of priority, the earlier it runs.  So `VerifyName` will run prior to `VerifyId`

### Integration Tests
Integration tests run an instance of the web server for testing purposes.  As a result, it is **required** that the class be derived from `FIOAPIIntegrationTest`.
```csharp
public class Widget : FIOAPIIntegrationTest
{	
	// ...
}
```

What the above does is ensure that when the test is run, it uses a shared instance of the test web server.

### Test UserAccounts for Integration Tests
If you require an account to test with, leverage the `TestUserAccount` class.  Example usage:
```csharp
var adminAccount = TestUserAccount.MakeAdminAccount();
var userAccount = TestUserAccount.MakeAccount();

var twoAdminAccounts = TestUserAccount.MakeAdminAccounts(2);
var threeUserAccounts = TestUserAccount.MakeAccounts(3);

var user = threeUserAccounts[1];
// Fields:
// user.UserName				- The username in the database (all lowercase)
// user.DisplayUserName			- The display username including casing
// user.Password				- The test account's password
// user.Admin					- If this account is admin
// user.UserWriteAPIKey			- A write API key for the account
// user.UserReadOnlyAPIKey		- A read-only API key for the account

// Methods:
// user.GetReadDef() - Generates a read-only AuthDef object for use with requests
// user.GetWriteDef() - Generates a writable AuthDef object for use with requests

request = new Request(		// Create a new Request
	client, 				// Use the boiler-plate client object
	HttpMethod.Put, 		// The HTTP method to use
	"/permission/grant", 	// The endpoint
	user.GetWriteDef());	// Use a writeable API Key for the request
```

### Request Extension Helpers
There are a few helper extension methods to make testing a little easier. All of these assert that the request object is not null and that the StatusCode is set to the corresponding code.
- `request.AssertOK()`
- `request.AssertBadRequest()`
- `request.AssertNotAcceptable()`
- `request.AssertNotFound()`
- `request.AssertForbidden()`
- `request.AssertUnauthorized()`
- `request.AssertNoContent()`
- `request.AssertHydrationTimeout()`
- `request.AssertStatusCode(481)`